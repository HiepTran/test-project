package com.datawings.app.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import java.util.Locale;

import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.Dashboard;
import com.datawings.app.model.AirlineParam;
import com.datawings.app.model.SysUser;
import com.datawings.app.service.IAirlineParamService;
import com.datawings.app.service.IDashboardService;
import com.datawings.app.service.ISysUserService;

@Controller
@RequestMapping("/secure/")
public class DashboardController {

	@Autowired
	private ISysUserService sysUserService;

	@Autowired
	private IDashboardService dashboardService;

	@Autowired
	private IAirlineParamService airlineParamService;

	@RequestMapping(value = "dashboard", method = RequestMethod.GET)
	public String getDashboard(Model model, HttpServletRequest request,
			HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();

		// Begin langue
		String langueUser = sysUser.getLanguage();
		if (StringUtils.isBlank(langueUser)) {
			langueUser = "fr";
		}
		LocaleResolver localeResolver = RequestContextUtils .getLocaleResolver(request);
		localeResolver.setLocale(request, response, new Locale(langueUser.trim()));
		// End langue

		Date currentDate = dashboardService.getCurrentDate();
		if (currentDate == null)
			return "dashboard";

		String year = DateUtil.date2String(currentDate, "yyyy");
		List<Dashboard> listAwb = dashboardService.getListAwb(year);

		Dashboard saleMonth = new Dashboard();
		Dashboard saleYear = new Dashboard();

		for (Dashboard elm : listAwb) {
			saleYear.setNrtd(saleYear.getNrtd() + elm.getNrtd());
			saleYear.setWeightGross(saleYear.getWeightGross()
					+ elm.getWeightGross());
			saleYear.setWeightCharge(saleYear.getWeightCharge()
					+ elm.getWeightCharge());
			saleYear.setAmount(saleYear.getAmount() + elm.getAmount());

			if (StringUtils.equals(elm.getMonth(),
					DateUtil.date2String(currentDate, "MM/yyyy"))) {
				saleMonth = elm;
			}
		}

		List<Dashboard> listFlown = dashboardService.getListTransport(year);
		Dashboard flownMonth = new Dashboard();
		Dashboard flownYear = new Dashboard();

		for (Dashboard elm : listFlown) {
			flownYear.setNrtd(flownYear.getNrtd() + elm.getNrtd());
			flownYear.setWeightGross(flownYear.getWeightGross()
					+ elm.getWeightGross());
			flownYear.setWeightCharge(flownYear.getWeightCharge()
					+ elm.getWeightCharge());
			flownYear.setAmount(flownYear.getAmount() + elm.getAmount());

			if (StringUtils.equals(elm.getMonth(),
					DateUtil.date2String(currentDate, "MM/yyyy"))) {
				flownMonth = elm;
			}
		}

		List<Date> listMonth = new ArrayList<Date>();
		listMonth = getListMonth(DateUtil.string2Date("01/01/" + year,
				"dd/MM/yyyy"));

		AirlineParam airlineParam = airlineParamService.find(sysUser
				.getAirlineCode());
		if (airlineParam != null) {
			model.addAttribute("cutp", airlineParam.getCurrency());
		}

		model.addAttribute("listFlown", listFlown);
		model.addAttribute("flownMonth", flownMonth);
		model.addAttribute("flownYear", flownYear);
		model.addAttribute("currentDate", currentDate);
		model.addAttribute("saleMonth", saleMonth);
		model.addAttribute("saleYear", saleYear);
		model.addAttribute("listAwb", listAwb);
		model.addAttribute("listMonth", listMonth);

		return "dashboard";
	}

	@RequestMapping(value = "dashboard", method = RequestMethod.POST)
	public String postDashboard(Model model, HttpServletRequest request,
			HttpServletResponse response) {

		return "redirect:/secure/dashboard";
	}

	private List<Date> getListMonth(Date beginDate) {
		List<Date> lstmonth = new ArrayList<Date>();

		lstmonth.add(beginDate);
		Date d = beginDate;
		for (int i = 0; i < 11; i++) {
			d = NextMonth(d);
			lstmonth.add(d);
		}
		return lstmonth;
	}

	public Date NextMonth(Date dais) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dais);
		Calendar calendar2 = Calendar.getInstance();
		calendar2.clear();

		if (calendar.get(Calendar.MONTH) == 12) {
			calendar2.set(calendar.get(Calendar.YEAR) + 1,
					calendar.get(Calendar.MONTH) - 11,
					calendar.get(Calendar.DAY_OF_MONTH));
		} else {
			calendar2.set(calendar.get(Calendar.YEAR),
					calendar.get(Calendar.MONTH) + 1,
					calendar.get(Calendar.DAY_OF_MONTH));
		}

		return calendar2.getTime();
	}
}