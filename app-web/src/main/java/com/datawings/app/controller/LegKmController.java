package com.datawings.app.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.LocaleResolver;

import com.datawings.app.common.DoubleUtil;
import com.datawings.app.filter.LegKmFilter;
import com.datawings.app.model.LegKm;
import com.datawings.app.model.LegKmId;
import com.datawings.app.model.SysUser;
import com.datawings.app.report.LegKmExcel;
import com.datawings.app.service.ILegKmService;
import com.datawings.app.validator.LegKmValidator;
import com.datawings.app.validator.ValidationError;

@Controller
@SessionAttributes({ "legkmfilter" })
@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_SETTING', 'ROLE_SETTING_KM')")
public class LegKmController {

	@Autowired
	private ILegKmService iLegkmService;
	@Autowired
	private LegKmValidator legKmValidator;
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;

	
	@ModelAttribute("legkmfilter")
	public LegKmFilter getLegKmFilter() {
		LegKmFilter filter = new LegKmFilter();
		filter.setPageSize(20);
		return filter;
	}

	@RequestMapping(value = "/secure/legkm", method = RequestMethod.GET)
	public String legKmGet(@ModelAttribute("legkmfilter") LegKmFilter filter, Model model) {
		Integer legKmRowCout = iLegkmService.getLegKmRouCount(filter);
		filter.setRowCount(legKmRowCout);
		return "legkm";
	}

	@RequestMapping(value = "/secure/legkm/loadpage", method = RequestMethod.GET)
	public String legKmLoadpageGet(@ModelAttribute("legkmfilter") LegKmFilter filter, Integer pageNo, Model model) {
		filter.setPage(pageNo);
		List<LegKm> lstLegKm = iLegkmService.getLegKm(filter);
		model.addAttribute("lstLegKm", lstLegKm);
		model.addAttribute("row", (filter.getPage()) * filter.getPageSize());

		SysUser sysUser = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		model.addAttribute("sysUser", sysUser);

		return "legkmloadpage";
	}

	@RequestMapping(value = "/secure/legkm", method = RequestMethod.POST)
	public String legKmPost(@ModelAttribute("legkmfilter") LegKmFilter filter, BindingResult result,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if (StringUtils.equals(action, "GO")) {
			filter.setPage(0);
			return "redirect:/secure/legkm";
		}
		if (StringUtils.equals(action, "RESET")) {
			filter.initLegKmAdd();
			filter.initLegKmEdit();
			filter.setAirportFrom("");
			filter.setAirportTo("");
			filter.setIsocFrom("");
			filter.setIsocTo("");
		}

		return "redirect:/secure/legkm";
	}

	@RequestMapping(value = "/secure/legkm/model/ajax/{isocfrom}/{airportfrom}/{isocto}/{airportto}", method = RequestMethod.GET)
	public String getLegKmAjax(@PathVariable String isocfrom, @PathVariable String airportfrom,
			@PathVariable String isocto, @PathVariable String airportto, Model model) {
		LegKmId legKmID = new LegKmId();
		legKmID.setIsocFrom(isocfrom);
		legKmID.setAirportFrom(airportfrom);
		legKmID.setIsocTo(isocto);
		legKmID.setAirportTo(airportto);
		LegKm legKm = iLegkmService.find(legKmID);
		model.addAttribute("legkm", legKm);
		return "legkmAjax";
	}

	// export excel
	@RequestMapping(value = "/secure/legkm/excel", method = RequestMethod.GET)
	@PreAuthorize(value = "hasAnyRole('ROLE_DW')")
	public void getExcelLegKm(@ModelAttribute("legkmfilter") LegKmFilter filter, Model model,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<LegKm> lst = iLegkmService.fineAll();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		LegKmExcel legKmExcel = new LegKmExcel();
		baos = legKmExcel.exportExcel(lst, filter, messageSource, localeResolver.resolveLocale(request));

		response.setContentType("application/xls");
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + "LEG_KM.xls" + "\";");
		response.setContentLength(baos.size());

		ServletOutputStream out = response.getOutputStream();
		baos.writeTo(out);
		out.flush();
		out.close();
	}

	@RequestMapping(value = "/secure/legkm/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorEdit(@ModelAttribute("legkmfilter") LegKmFilter filter,
			BindingResult result, Model model, Locale locale) {
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = legKmValidator.checkValidatorEditLegKm(filter, result);
		if (errCode > 0) {
			rs.put("errCodeEdit", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for (int i = 0; i < result.getAllErrors().size(); i++) {
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}

	@RequestMapping(value = "/secure/legkm/create/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorCreate(@ModelAttribute("legkmfilter") LegKmFilter filter,
			BindingResult result, Model model, Locale locale) {
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = legKmValidator.checkValidatorCreateLegKm(filter, result);
		if (errCode > 0) {
			rs.put("errCodeCreate", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for (int i = 0; i < result.getAllErrors().size(); i++) {
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}

	@RequestMapping(value = "/secure/legkm/create", method = RequestMethod.POST)
	public String createLegKm(@ModelAttribute("legkmfilter") LegKmFilter filter, BindingResult result,
			HttpServletRequest request, Model model) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if (StringUtils.equals(action, "CREATE")) {
			LegKm newLegkm = new LegKm();
			LegKmId newLegKmId = new LegKmId();

			newLegKmId.setIsocFrom(filter.getLegKmAdd().getId().getIsocFrom().toUpperCase());
			newLegKmId.setAirportFrom(filter.getLegKmAdd().getId().getAirportFrom().toUpperCase());
			newLegKmId.setIsocTo(filter.getLegKmAdd().getId().getIsocTo().toUpperCase());
			newLegKmId.setAirportTo(filter.getLegKmAdd().getId().getAirportTo().toUpperCase());

			newLegkm.setId(newLegKmId);

			newLegkm.setKm(DoubleUtil.round(filter.getLegKmAdd().getKm(), 2));
			newLegkm.setMile(DoubleUtil.round(filter.getLegKmAdd().getMile(), 2));
			iLegkmService.save(newLegkm);
			filter.initLegKmAdd();
		}
		return "redirect:/secure/legkm";
	}

	@RequestMapping(value = "/secure/legkm/delete", method = RequestMethod.POST)
	public String deleteLegKm(@ModelAttribute("legkmfilter") LegKmFilter filter, HttpServletRequest request,
			Model model) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if (StringUtils.equals(action, "DELETE")) {
			String isocfrom = ServletRequestUtils.getStringParameter(request, "isocform", "");
			String airportfrom = ServletRequestUtils.getStringParameter(request, "airportfrom", "");
			String isocto = ServletRequestUtils.getStringParameter(request, "isocto", "");
			String airportto = ServletRequestUtils.getStringParameter(request, "airportto", "");

			LegKmId id = new LegKmId();
			id.setIsocFrom(isocfrom);
			id.setAirportFrom(airportfrom);
			id.setIsocTo(isocto);
			id.setAirportTo(airportto);
			iLegkmService.deleteById(id);
			if (filter.getOffset() > 0 && filter.getOffset() >= filter.getRowCount() - 1) {
				Integer p = filter.getPage();
				filter.setPage(--p);
			}
		}
		return "legkm";
	}

	@RequestMapping(value = "/secure/legkm/modify", method = RequestMethod.POST)
	public String modifyLegKm(@ModelAttribute("legkmfilter") LegKmFilter filter, HttpServletRequest request,
			Model model) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if (StringUtils.equals(action, "MODIFY")) {
			String isocfrom = ServletRequestUtils.getStringParameter(request, "isocfrom", "");
			String airportfrom = ServletRequestUtils.getStringParameter(request, "airportfrom", "");
			String isocto = ServletRequestUtils.getStringParameter(request, "isocto", "");
			String airportto = ServletRequestUtils.getStringParameter(request, "airportto", "");

			LegKmId id = new LegKmId();
			id.setIsocFrom(isocfrom);
			id.setAirportFrom(airportfrom);
			id.setIsocTo(isocto);
			id.setAirportTo(airportto);

			iLegkmService.updateLeg(filter, id);
			filter.initLegKmEdit();
		}
		return "redirect:/secure/legkm";
	}
}
