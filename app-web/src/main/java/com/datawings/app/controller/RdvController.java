package com.datawings.app.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.datawings.app.common.DoubleUtil;
import com.datawings.app.filter.AgentFilter;
import com.datawings.app.filter.RdvFilter;
import com.datawings.app.model.Agent;
import com.datawings.app.model.Awb;
import com.datawings.app.model.CritIsoc;
import com.datawings.app.model.Rdv;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.AwbUtil;
import com.datawings.app.service.IAgentService;
import com.datawings.app.service.IAirlineParamService;
import com.datawings.app.service.IAwbService;
import com.datawings.app.service.ICritIsocService;
import com.datawings.app.service.IRdvService;
import com.datawings.app.service.ISysUserService;
import com.datawings.app.validator.AgentValidator;
import com.datawings.app.validator.RdvValidator;
import com.datawings.app.validator.ValidationError;

@Controller
@SessionAttributes({"rdvFilter", "rdvAddFilter", "agentFilter"})
@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_AWB', 'ROLE_AWB_CAPTURE')")
public class RdvController {

	@Autowired
	private ISysUserService sysUserService;
	
	@Autowired
	private IRdvService rdvService;
	
	@Autowired
	private RdvValidator rdvValidator;
	
	@Autowired
	private IAirlineParamService airlineParamService;
	
	@Autowired
	private ICritIsocService critIsocService;
	
	@Autowired
	private AgentValidator agentValidator;
	
	@Autowired
	private IAgentService agentService;
	
	@Autowired
	private IAwbService awbService;
	
	@Autowired
	private AwbUtil awbUtil;
	
	@Autowired private MessageSource messageSource;
	
	@ModelAttribute("rdvFilter")  
    public RdvFilter getInitRdv() {  
		RdvFilter filter = new RdvFilter();
        return filter;  
    }
	
	@ModelAttribute("rdvAddFilter")  
    public RdvFilter getInitRdvAdd() {  
		RdvFilter filter = new RdvFilter();
        return filter;  
    }
	
	@ModelAttribute("agentFilter")
	public AgentFilter getInitAgent() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		AgentFilter filter = new AgentFilter();
		filter.setIsoc(airlineParamService.find(sysUser.getAirlineCode()).getIsoc());
		return filter;
	}
	
	@RequestMapping(value = "/secure/rdv", method = RequestMethod.GET)
	public String getRdv(@ModelAttribute("rdvFilter") RdvFilter filter, Model model, HttpServletRequest request, HttpServletResponse response) {
		filter.setRowCount(rdvService.getRdvRowsCount(filter));
		
		List<CritIsoc> listIsoc = critIsocService.getAll();
		model.addAttribute("listIsoc", listIsoc);
		return "rdv";
	}
	
	@RequestMapping(value = {"/secure/rdv/loadPage"}, method = RequestMethod.GET)
	public String getListRdvAjax(@ModelAttribute("rdvFilter") RdvFilter filter, Integer pageNo, Model model) {
		filter.setPage(pageNo);
		List<Rdv> listRdv = rdvService.getListRdv(filter);
		for (Rdv elm : listRdv) {
			elm.setNbAwb(awbService.getNbAwbByRdv(elm.getCode()));
		}
		model.addAttribute("row", (filter.getPage()) * filter.getPageSize());
		model.addAttribute("listRdv", listRdv);
		
		return "rdvLoadPage";
	}
	
	@RequestMapping(value = "/secure/rdv", method = RequestMethod.POST)
	public String postRdv(@ModelAttribute("rdvFilter") RdvFilter filter,
			BindingResult result, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if(StringUtils.equals(action, "GO")){
			filter.setPage(0);
			return "redirect:/secure/rdv";
		}else if(StringUtils.equals(action, "RESET")){
			 filter.init();
		}else if(StringUtils.equals(action, "AWB")){
			String rdv = ServletRequestUtils.getStringParameter(request, "id", "");
			return "redirect:/secure/createAwb?codeRdv=" + rdv;
		}else if(StringUtils.equals(action, "DELETE")){
			String rdv = ServletRequestUtils.getStringParameter(request, "id", "");
			rdvService.deleteById(rdv);
		}else if(StringUtils.equals(action, "VIEW")){
			String rdv = ServletRequestUtils.getStringParameter(request, "id", "");
			return "redirect:/secure/rdv/detail?rdvCode=" + rdv;
		}
		return "redirect:/secure/rdv";	
	}
	
	@RequestMapping(value = "/secure/rdv/detail", method = RequestMethod.GET)
	public String getRdvDetail(@ModelAttribute("rdvFilter") RdvFilter filter, Model model, HttpServletRequest request, HttpServletResponse response) {
		String rdvCode = ServletRequestUtils.getStringParameter(request, "rdvCode", "");
		List<Awb> awbs = awbService.getAwbByRdvCode(rdvCode);
		model.addAttribute("awbs", awbs);
		return "rdvDetail";
	}
	
	@RequestMapping(value = "/secure/rdv/detail", method = RequestMethod.POST)
	public String postRdvDetail(@ModelAttribute("rdvFilter") RdvFilter filter, BindingResult result, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		String id = ServletRequestUtils.getStringParameter(request, "id", "");
		String rdvCode = ServletRequestUtils.getStringParameter(request, "rdvCode", "");
		
		if(StringUtils.equals(action, "DETAIL")){
			request.getSession().setAttribute("FROM_PAGE", "RDV");
			request.getSession().setAttribute("ID_DETAIL", rdvCode);
			
			return "redirect:/secure/awb/detail?id="+id;
		}else if(StringUtils.equals(action, "DELETE")){
			awbService.deleteById(id);
			return "redirect:/secure/rdv/detail?rdvCode="+rdvCode;
		}
		
		return "redirect:/secure/rdv";
	}
	
	@RequestMapping(value = "/secure/rdvAdd", method = RequestMethod.POST)
	public String postRdvAdd(@ModelAttribute("rdvAddFilter") RdvFilter filter, BindingResult result, Model model,
			HttpServletRequest request, HttpServletResponse response) {

		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		
		if(StringUtils.equals(action, "ADD")){
			Rdv rdv = awbUtil.processRdv(filter.getAgtn(), filter.getEmission());
			filter.init();
			return "redirect:/secure/createAwb?codeRdv=" + rdv.getCode();
		}
		return "redirect:/secure/rdv";	
	}
	
	@RequestMapping(value = "/secure/addAgent", method = RequestMethod.POST)
	public String postAgentAdd(@ModelAttribute("agentFilter") AgentFilter filter,
			BindingResult result, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if(StringUtils.equals(action, "ADD")){
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			SysUser sysUser = (SysUser) auth.getPrincipal();
			
			Agent agent = new Agent();
			agent.setCode(StringUtils.substring(filter.getAgtnCode(), 0, 10));
			agent.setCheckDigit(StringUtils.substring(filter.getAgtnCode(), 10, 11));
			agent.setName(filter.getName().trim().toUpperCase());
			agent.setAddress1(filter.getAddress1().trim().toUpperCase());
			agent.setKind(filter.getKind());
			agent.setCity(filter.getCity().trim().toUpperCase());
			agent.setZip(filter.getZip().trim().toUpperCase());
			agent.setIsoc(filter.getIsoc());
			agent.setRegion(filter.getRegion().trim().toUpperCase());
			agent.setPhone(filter.getPhone().trim().toUpperCase());
			agent.setFax(filter.getFax().trim().toUpperCase());
			agent.setEmail(filter.getEmail().trim());
			agent.setIataInd(filter.getIataInd());
			agent.setComm(DoubleUtil.convertDouble(filter.getComm()));
			agent.setCreatedBy(sysUser.getUsername());
			agent.setCreatedDate(new Date());
			
			agentService.merge(agent);
			filter.init();
			model.addAttribute("error", true);
		}
		return "rdv";
	}
	
	@RequestMapping(value = "/secure/rdv/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorAgent(@ModelAttribute("rdvAddFilter") RdvFilter filter, BindingResult result, Model model, Locale locale){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = rdvValidator.checkRdv(filter, result);
		if(errCode > 0){
			rs.put("errCodeRdv", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for(int i = 0;i <result.getAllErrors().size();i++){
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null, 
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}
	
	@RequestMapping(value = "/secure/agent/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorAdd(@ModelAttribute("agentFilter") AgentFilter filter, BindingResult result, Model model, Locale locale){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = agentValidator.checkAgent(filter, result);
		if(errCode > 0){
			rs.put("errCodeAgent", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for(int i = 0;i <result.getAllErrors().size();i++){
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null, 
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}
}
