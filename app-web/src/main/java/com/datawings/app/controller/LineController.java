package com.datawings.app.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.LocaleResolver;

import com.datawings.app.filter.LineFilter;
import com.datawings.app.model.Line;
import com.datawings.app.report.LineExcel;
import com.datawings.app.service.ILineService;
import com.datawings.app.validator.LineValidator;
import com.datawings.app.validator.ValidationError;

@Controller
@SessionAttributes({"lineFilter"})
@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_SETTING', 'ROLE_SETTING_LINE')")
public class LineController {

	@Autowired
	private ILineService lineService;
	
	@Autowired
	private LineValidator lineValidator;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@ModelAttribute("lineFilter")  
    public LineFilter getInitLine() {  
		LineFilter filter = new LineFilter();
        return filter;  
    }
	
	public LineFilter getResetLineAdd(LineFilter filter) {
		filter.setOrigExt("");
		filter.setDestExt("");
		filter.setLineExt("");
        return filter;  
    }

	@RequestMapping(value = "/secure/line", method = RequestMethod.GET)
	public String getLine(@ModelAttribute("lineFilter") LineFilter filter, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		List<Line> listLine = lineService.getListLine(filter);
		model.addAttribute("listLine", listLine);
		
		return "line";
	}
	
	@RequestMapping(value = "/secure/line", method = RequestMethod.POST)
	public String postLine(@ModelAttribute("lineFilter") LineFilter filter,
			BindingResult result, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		
		if(StringUtils.equals(action, "GO")){
			return "redirect:/secure/line";
		}else if(StringUtils.equals(action, "RESET")){
			filter.init();
		}else if(StringUtils.equals(action, "ADD")){
			lineValidator.checkLine(filter, result);
			if(result.hasErrors()){
				List<Line> listLine = lineService.getListLine(filter);
				model.addAttribute("listLine", listLine);
				model.addAttribute("error", true);
				return "line";
			}
			Line line = new Line();
			line.setOrac(filter.getOrigExt().toUpperCase());
			line.setDstc(filter.getDestExt().toUpperCase());
			line.setLine(filter.getLineExt().trim().toUpperCase());
			line.setDescripbe(filter.getLineExt().trim().toUpperCase()+ " " + filter.getOrigExt().toUpperCase()+ " " + filter.getDestExt().toUpperCase()+ " " + filter.getOrigExt().toUpperCase());
			lineService.save(line);
			
			getResetLineAdd(filter);
		}else if(StringUtils.equals(action, "DELETE")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			lineService.deleteById(id);
		}else if(StringUtils.equals(action, "UPDATE")){
			lineValidator.checkUpdateLine(filter, result);
			if(result.hasErrors()){
				List<Line> listLine = lineService.getListLine(filter);
				model.addAttribute("listLine", listLine);
				model.addAttribute("errorEdit", true);
				return "line";
			}
			
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			Line line = lineService.find(id);
			line.setLine(filter.getLineEdit().trim().toUpperCase());
			line.setDescripbe(filter.getLineEdit().trim().toUpperCase()+ " " + line.getOrac().toUpperCase()+ " " + line.getDstc().toUpperCase()+ " " + line.getOrac().toUpperCase());
			
			lineService.deleteById(id);
			
			lineService.merge(line);
		}
		return "redirect:/secure/line";
	}
	
	@RequestMapping(value = {"/secure/line/model/{id}"}, method = RequestMethod.GET)
	public String getLine(@PathVariable String id, Model model) {	
		Line line = lineService.find(id);
		model.addAttribute("line", line);
		return "lineEdit";
	}
	@RequestMapping(value = "/secure/line/create/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorCreateLine(@ModelAttribute("lineFilter") LineFilter filter, 
			BindingResult result, Model model, Locale locale){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = lineValidator.checkLine(filter, result);
		if (errCode > 0) {
			rs.put("errCodeCreate", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for (int i = 0; i < result.getAllErrors().size(); i++) {
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}
	
	@RequestMapping(value = "/secure/line/create", method = RequestMethod.POST)
	public String doCreateLine(@ModelAttribute("lineFilter") LineFilter filter, Model model, HttpServletRequest request){
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if(StringUtils.equals(action, "ADD")){
			Line line = new Line();
			line.setOrac(filter.getOrigExt().toUpperCase());
			line.setDstc(filter.getDestExt().toUpperCase());
			line.setLine(filter.getLineExt().trim().toUpperCase());
			line.setDescripbe(filter.getLineExt().trim().toUpperCase()+ " " + filter.getOrigExt().toUpperCase()+ " " + filter.getDestExt().toUpperCase()+ " " + filter.getOrigExt().toUpperCase());
			lineService.save(line);
			
			getResetLineAdd(filter);
		}
		
		return "redirect:/secure/line";
	}
	
	@RequestMapping(value = "/secure/line/edit/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorEditLine(@ModelAttribute("lineFilter") LineFilter filter, 
			BindingResult result, Model model, Locale locale){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = lineValidator.checkUpdateLine(filter, result);
		if (errCode > 0) {
			rs.put("errCodeEdit", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for (int i = 0; i < result.getAllErrors().size(); i++) {
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}
	
	@RequestMapping(value = "/secure/line/edit", method = RequestMethod.POST)
	public String doEditLine(@ModelAttribute("lineFilter") LineFilter filter, Model model, HttpServletRequest request){
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if(StringUtils.equals(action, "UPDATE")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			Line line = lineService.find(id);
			line.setLine(filter.getLineEdit().trim().toUpperCase());
			line.setDescripbe(filter.getLineEdit().trim().toUpperCase()+ " " + line.getOrac().toUpperCase()+ " " + line.getDstc().toUpperCase()+ " " + line.getOrac().toUpperCase());
			
			lineService.deleteById(id);
			
			lineService.merge(line);
		}
		return "redirect:/secure/line";
	}
	
	@RequestMapping(value = "/secure/line/excel", method = RequestMethod.GET)
	public void getExcelLine(@ModelAttribute("lineFilter") LineFilter filter, Model model,
			HttpServletRequest request, HttpServletResponse  response) throws IOException{
		List<Line> lst = lineService.getListLine(filter);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		LineExcel lineExcel = new LineExcel();
		baos = lineExcel.exportExcel(lst, filter, messageSource, localeResolver.resolveLocale(request));
		
		response.setContentType("application/xls");
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ "LINE.xls" + "\";");
		response.setContentLength(baos.size());
		
		ServletOutputStream out = response.getOutputStream();
		baos.writeTo(out);
		out.flush();
		out.close();
	}
}
