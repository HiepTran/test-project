package com.datawings.app.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.LocaleResolver;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.DoubleUtil;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.AwbTaxFilter;
import com.datawings.app.filter.ManifestBean;
import com.datawings.app.filter.ManifestFilter;
import com.datawings.app.model.AwbFlown;
import com.datawings.app.model.AwbFlownTax;
import com.datawings.app.model.Manifest;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.report.AwbFlownExcel;
import com.datawings.app.service.IAwbFlownDetailService;
import com.datawings.app.service.IAwbFlownService;
import com.datawings.app.service.IAwbFlownTaxService;
import com.datawings.app.service.IManifestService;
import com.datawings.app.validator.AwbFlownValidator;
import com.datawings.app.validator.ValidationError;

@Controller
@SessionAttributes({"flownFilter", "awbFilter"})
@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_MANIFEST', 'ROLE_MANIFEST_TRANSPORT')")
public class FlownController {

	@Autowired
	private IManifestService manifestService;
	
	@Autowired
	private IAwbFlownService awbFlownService;
	
	@Autowired
	private IAwbFlownDetailService awbFlownDetailService;
	
	@Autowired
	private IAwbFlownTaxService awbFlownTaxService;
	
	@Autowired
	private AwbFlownValidator awbFlownValidator;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@ModelAttribute("flownFilter")  
    public ManifestFilter getInitCreate() {  
		ManifestFilter filter = new ManifestFilter();
        return filter;  
    } 
	
	@ModelAttribute("awbFilter")  
    public AwbFilter getInit() {  
		AwbFilter filter = new AwbFilter();
		
        return filter;  
    }

	@RequestMapping(value = "/secure/flown", method = RequestMethod.GET)
	public String getCreate(@ModelAttribute("flownFilter") ManifestFilter filter, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		request.getSession().setAttribute("FROM_PAGE", "FLOWN");
		filter.setRowCount(manifestService.getRowsCount(filter));
		
		return "flown";
	}
	
	@RequestMapping(value = "/secure/flown/ajax", method = RequestMethod.GET)
	public String getListLTAAjax(@ModelAttribute("flownFilter") ManifestFilter filter, Integer pageNo, Model model) {
		filter.setPage(pageNo);
		List<ManifestBean> listManifest = manifestService.getManifest(filter);
		model.addAttribute("listManifest", listManifest);
		
		
		model.addAttribute("row", (filter.getPage()) * filter.getPageSize());
		model.addAttribute("listManifest", listManifest);
		return "flownAjax";
	}
	
	@RequestMapping(value = "/secure/flown", method = RequestMethod.POST)
	public String postAwb(@ModelAttribute("flownFilter") ManifestFilter filter,
			BindingResult result, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		
		if(StringUtils.equals(action, "VIEW")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			return "redirect:/secure/flown/detail?id="+id;
		}else if(StringUtils.equals(action, "GO")){
			return "redirect:/secure/flown";
		}else if(StringUtils.equals(action, "RESET")){
			filter.init();
		}
		return "redirect:/secure/flown";
	}
	
	@RequestMapping(value = "/secure/flown/detail", method = RequestMethod.GET)
	public String getAwbDetail(@ModelAttribute("awbFilter") AwbFilter filter, Model model, HttpServletRequest request) {
		request.getSession().setAttribute("FROM_PAGE", "FLOWN");
		String id = ServletRequestUtils.getStringParameter(request, "id", "");
		List<AwbFlown> awbFlowns = awbFlownService.getListAwbFlown(id);
		
		ManifestBean manifest = manifestService.findReturnBean(id);
		
		model.addAttribute("awbFlowns", awbFlowns);
		model.addAttribute("manifest", manifest);
		return "flownDetail";
	}
	
	@RequestMapping(value = "/secure/awb/flown/detail", method = RequestMethod.GET)
	public String getAwbFlownDetail(@ModelAttribute("awbFilter") AwbFilter filter, Model model, HttpServletRequest request) {
	
		String id = ServletRequestUtils.getStringParameter(request, "id", "");
		String idFlown = ServletRequestUtils.getStringParameter(request, "idFlown", "");
		
		AwbFlown awbFlown = awbFlownService.find(Integer.parseInt(idFlown));
		Integer sizeTax = awbFlown.getAwbFlownTax().size();
		Set<AwbFlownTax> setTax = new HashSet<AwbFlownTax>();
		for (int i = 0; i < CargoUtils.NB_TAX - sizeTax; i++) {
			setTax.add(new AwbFlownTax());
		}
		awbFlown.getAwbFlownTax().addAll(setTax);
		
		ManifestBean manifest = manifestService.findReturnBean(id);
		
		model.addAttribute("awbFlown", awbFlown);
		model.addAttribute("manifest", manifest);
		return "awbFlownDetail";
	}
	
	@RequestMapping(value = "/secure/awb/flown/detail", method = RequestMethod.POST)
	public String postAwbDetail(@ModelAttribute("awbFilter") AwbFilter filter, BindingResult result, Model model, HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		String id = ServletRequestUtils.getStringParameter(request, "id", "");
		String idFlown = ServletRequestUtils.getStringParameter(request, "idFlown", "");
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		
		if(StringUtils.equals(action, "BACK")){
			String fromPage = (String) request.getSession().getAttribute("FROM_PAGE");
			
			if(StringUtils.equals(fromPage, "AWB")){
				return "redirect:/secure/awb";
			}else if(StringUtils.equals(fromPage, "FLOWN")){
				return "redirect:/secure/flown/detail?id=" + id;
			}
		}else if(StringUtils.equals(action, "VALIDER")){
			AwbFlown awb = awbFlownService.find(Integer.parseInt(idFlown));
			
			awb.setIsoc(filter.getIsoc().toUpperCase());
			awb.setDateExecute(DateUtil.string2Date(filter.getDateExecute(), "dd/MM/yyyy"));
			awb.setShipName(filter.getShipName());
			awb.setShipAddress1(filter.getShipAddress1());
			awb.setShipAddress2(filter.getShipAddress2());
			awb.setShipAddress3(filter.getShipAddress3());
			awb.setConsigneeName(filter.getConsigneeName());
			awb.setConsigneeAddress1(filter.getConsigneeAddress1());
			awb.setConsigneeAddress2(filter.getConsigneeAddress2());
			awb.setConsigneeAddress3(filter.getConsigneeAddress3());
			awb.setOrig(filter.getOrig().toUpperCase());
			awb.setDest(filter.getDest().toUpperCase());
			awb.setFlightNumber(filter.getFlightNumber());
			awb.setFlightDate(DateUtil.string2Date(filter.getFlightDate(), "dd/MM/yyyy"));
			awb.setCutp(filter.getCutp());
			awb.setChargeIndicator(filter.getChargeIndicator());
			awb.setOtherIndicator(filter.getOtherIndicator());
			awb.setDeclaredCarrige(filter.getDeclaredCarrige().toUpperCase());
			awb.setDeclaredCustom(filter.getDeclaredCustom().toUpperCase());
			awb.setHandlingInfor(filter.getHandlingInfor());
			awb.setDescriptUser(filter.getDescriptUser());
			awb.setKm(DoubleUtil.convertAmount(filter.getKm()));
			
			/*//DETAIL
			Double charge = 0.0;
			for (int i = 0; i < filter.getAwbDetail().size(); i++) {
				AwbDetailFilter elm = filter.getAwbDetail().get(i);
				AwbFlownDetail detail = awbFlownDetailService.find(elm.getAwbDetailId());
				detail.setAmount(DoubleUtil.convertDouble(elm.getAmount(), 2));
				charge += DoubleUtil.convertDouble(elm.getAmount(), 2);
			}
			
			if(StringUtils.equals(awb.getChargeIndicator(), "P")){
				awb.setWeightPp(charge);
				awb.setWeightCc(0.0);
			}else {
				awb.setWeightPp(0.0);
				awb.setWeightCc(charge);
			}*/
			
			//TAX
			Set<AwbFlownTax> taxCu = new HashSet<AwbFlownTax>();
			for (AwbFlownTax elm : awb.getAwbFlownTax()) {
				taxCu.add(elm);
				awbFlownTaxService.deleteId(elm.getAwbFlownTaxId());
			}
			awb.getAwbFlownTax().removeAll(taxCu);
			
			for(AwbTaxFilter elm : filter.getAwbTax()) {
				int retval =  DoubleUtil.convertDouble(elm.getAmount()).compareTo(0.0);
				 
				if(elm.getCode().length() == 3 && retval != 0){
					AwbFlownTax awbFlownTax = new AwbFlownTax();
					awbFlownTax.setManifestNo(awb.getManifestNo());
					awbFlownTax.setLta(awb.getLta());
					awbFlownTax.setCoupon(awb.getCoupon());
					awbFlownTax.setTaxCode(elm.getCode().toUpperCase());
					awbFlownTax.setTaxAmount(DoubleUtil.round(DoubleUtil.convertDouble(elm.getAmount()), 2));
					awbFlownTax.setCreatedBy(awb.getCreatedBy());
					awbFlownTax.setCreatedDate(awb.getCreatedDate());
					awbFlownTax.setModifiedBy(sysUser.getUsername());
					awbFlownTax.setModifiedDate(new Date());
					
					awbFlownTax.setAwbFlownId(awb.getAwbFlownId());
					awbFlownTax.setAwbFlown(awb);
					awb.getAwbFlownTax().add(awbFlownTax);
				}
			}
			
			//TOTAL
			Double agentCharge = 0.0;
			Double carrierCharge = 0.0;
			
			for (AwbFlownTax elm : awb.getAwbFlownTax()) {
				if(StringUtils.equals(StringUtils.substring(elm.getTaxCode(), 2), "A")){
					agentCharge += elm.getTaxAmount();
				}else{
					carrierCharge += elm.getTaxAmount();
				}
			}
			
			if(StringUtils.equals(awb.getOtherIndicator(), "P")){
				awb.setAgentPp(agentCharge);
				awb.setCarrierPp(carrierCharge);
				awb.setAgentCc(0.0);
				awb.setCarrierCc(0.0);
			}else {
				awb.setAgentPp(0.0);
				awb.setCarrierPp(0.0);
				awb.setAgentCc(agentCharge);
				awb.setCarrierCc(carrierCharge);
			}
			
			awb.setTotalPp(awb.getWeightPp() + awb.getAgentPp() + awb.getCarrierPp());
			awb.setTotalCc(awb.getWeightCc() + awb.getAgentCc() + awb.getCarrierCc());
			
			//Comm.
			if(StringUtils.isNotBlank(awb.getAgtnGsa())){
				/*
				 * Comm percent gsa percent
				 * com = (MYC + HT)/commPercent
				 * */
				Double taxMyc = 0.0;
				for (AwbFlownTax elmTax : awb.getAwbFlownTax()) {
					if(StringUtils.equals(elmTax.getTaxCode(), "MYC")){
						taxMyc = elmTax.getTaxAmount();
						break;
					}
				}
				awb.setComm(DoubleUtil.round((taxMyc + awb.getWeightPp() + awb.getWeightCc()) * awb.getCommPercent() / 100, 2));
			}	
			
			//VAT
			awb.setVat(DoubleUtil.round((awb.getTotalPp() + awb.getTotalCc()) * awb.getVatPercent() / 100, 2));
			awb.setNet(DoubleUtil.round(awb.getTotalPp() + awb.getTotalCc() + awb.getVat() - awb.getComm(), 2));
			
			awbFlownService.merge(awb);
		}
		
		return "redirect:/secure/awb/flown/detail?id=" + id + "&idFlown=" + idFlown;
	}
	
	@RequestMapping(value = "/secure/awb/flown/detail/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorAwb(@ModelAttribute("awbFilter") AwbFilter filter, HttpServletRequest request,BindingResult result, Locale locale){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = awbFlownValidator.checkDetail(filter, result);
		if(errCode > 0){
			rs.put("errCode", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for(int i = 0;i <result.getAllErrors().size();i++){
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null, 
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}
	
	@RequestMapping(value = "/secure/flown/detail/excel", method = RequestMethod.GET)
	public void getExcelFlown(Model model, HttpServletRequest request, HttpServletResponse  response) throws IOException {
		
		String id = ServletRequestUtils.getStringParameter(request, "id", "");
		Manifest manifest = manifestService.find(id);
		
		List<AwbFlown> awbFlowns = awbFlownService.getListAwbFlown(id);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		AwbFlownExcel awbFlownExcel = new AwbFlownExcel();
		baos = awbFlownExcel.exportExcel(manifest, awbFlowns, messageSource, localeResolver.resolveLocale(request));
		
		response.setContentType("application/xls");
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ "LTA_FLOWN_" + manifest.getManifestNo() +  ".xls" + "\";");
		response.setContentLength(baos.size());

		ServletOutputStream out = response.getOutputStream();
		baos.writeTo(out);
		out.flush();
		out.close();
	}
	
}
