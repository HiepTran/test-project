package com.datawings.app.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.LocaleResolver;

import com.datawings.app.filter.RoleFilter;
import com.datawings.app.model.SysRole;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.report.RoleExcel;
import com.datawings.app.service.ISysRoleService;
import com.datawings.app.validator.RoleValidator;
import com.datawings.app.validator.ValidationError;

@Controller
@PreAuthorize(value = "hasRole('ROLE_DW')")
public class RoleController {
	@Autowired
	private ISysRoleService sysRoleService;

	@Autowired
	private RoleValidator roleValidator;

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;
	

	@ModelAttribute("rolefilter")
	public RoleFilter getRoleFilter() {
		RoleFilter filter = new RoleFilter();
		return filter;
	}

	@RequestMapping(value = "/secure/role", method = RequestMethod.GET)
	public String getLstRole(@ModelAttribute("rolefilter") RoleFilter filter, Model model) {
		List<SysRole> lstRole = sysRoleService.getRoleOrderByRoleName();
		model.addAttribute("lstRole", lstRole);
		return "role";
	}

	@RequestMapping(value = "/secure/role", method = RequestMethod.POST)
	public String postLstRole(@ModelAttribute("rolefilter") RoleFilter filter, Model model,
			HttpServletRequest request) {
		return "redirect:/secure/role";
	}

	
	@RequestMapping(value = "/secure/role/delete/{id}", method = RequestMethod.POST)
	public String deleteRole(@ModelAttribute("rolefilter") RoleFilter filter, @PathVariable Integer id,
			HttpServletRequest request, Model model) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if (StringUtils.equals(action, "DELETE")) {
			String roleName = ServletRequestUtils.getStringParameter(request, "roleName", "");
			sysRoleService.deleteRoleUser(id);
			sysRoleService.deleteById(id);
			if (!StringUtils.equals(roleName, CargoUtils.ROLE_DW) && !StringUtils.equals(roleName, CargoUtils.ROLE_ADMIN)) {
				Integer idC = id + 1;
				Integer idU = id + 2;
				Integer idD = id + 3;
				sysRoleService.deleteRoleUser(idC);
				sysRoleService.deleteByRoleName(roleName + CargoUtils.ROLE_C);
				sysRoleService.deleteRoleUser(idU);
				sysRoleService.deleteByRoleName(roleName + CargoUtils.ROLE_U);
				sysRoleService.deleteRoleUser(idD);
				sysRoleService.deleteByRoleName(roleName + CargoUtils.ROLE_D);
			}
		}
		return "redirect:/secure/role";
	}

	@RequestMapping(value = "/secure/role/create/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorCreate(@ModelAttribute("rolefilter") RoleFilter filter, 
			BindingResult result, Model model, Locale locale) {
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = roleValidator.checkValidatorRoleAdd(filter, result);
		if (errCode > 0) {
			rs.put("errCodeCreate", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for (int i = 0; i < result.getAllErrors().size(); i++) {
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}

	@RequestMapping(value = "/secure/role/create", method = RequestMethod.POST)
	public String createRole(@ModelAttribute("rolefilter") RoleFilter filter, HttpServletRequest request, Model model) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if (StringUtils.equals(action, "CREATE")) {
			SysRole role = new SysRole();
			SysUser userAuth = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			// add role
			role.setAuthority(filter.getRoleNameAdd().toUpperCase().trim());
			role.setDescription(filter.getDescriptionAdd());
			role.setCreatedBy(userAuth.getUsername());
			role.setCreatedOn(new Date());
			sysRoleService.save(role);

			if (!StringUtils.equals(filter.getRoleNameAdd().toUpperCase(), CargoUtils.ROLE_ADMIN)) {
				
				List<String> lstRoleAdd = new ArrayList<String>();
				lstRoleAdd.add(CargoUtils.ROLE_C);
				lstRoleAdd.add(CargoUtils.ROLE_U);
				lstRoleAdd.add(CargoUtils.ROLE_D);
				
				//add role _C, _U, _D
				for(String r : lstRoleAdd){
					SysRole roleAdd = new SysRole();
					roleAdd.setAuthority(filter.getRoleNameAdd().toUpperCase() + r);
					roleAdd.setDescription(filter.getDescriptionAdd());
					roleAdd.setCreatedBy(userAuth.getUsername());
					roleAdd.setCreatedOn(new Date());
					sysRoleService.save(roleAdd);
				}
			}
			filter.init();
		}
		return "redirect:/secure/role";
	}

	@RequestMapping(value = "/secure/role/modify/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorModify(@ModelAttribute("rolefilter") RoleFilter filter,
			BindingResult result, Model model, Locale locale) {
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = roleValidator.checkValidatorRoleEdit(filter, result);
		if (errCode > 0) {
			rs.put("errCodeModify", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for (int i = 0; i < result.getAllErrors().size(); i++) {
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}

	@RequestMapping(value = "/secure/role/modify/{id}", method = RequestMethod.POST)
	public String modifyRole(@ModelAttribute("rolefilter") RoleFilter filter, @PathVariable Integer id,
			HttpServletRequest request, Model model) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if (StringUtils.equals(action, "MODIFY")) {
			String roleId = ServletRequestUtils.getStringParameter(request, "id", "");
			SysRole role = sysRoleService.find(Integer.parseInt(roleId));
			SysUser userAuth = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			
			role.setAuthority(filter.getRoleNameEdit());
			role.setDescription(filter.getDescriptionEdit());
			role.setModified(userAuth.getUsername());
			role.setModifiedOn(new Date());
			sysRoleService.update(role);
			
			if(!StringUtils.equals(role.getAuthority(), CargoUtils.ROLE_ADMIN) &&
					!StringUtils.equals(role.getAuthority(), CargoUtils.ROLE_DW)){
				Integer roleIdC = Integer.parseInt(roleId) + 1;
				Integer roleIdU = Integer.parseInt(roleId) + 2;
				Integer roleIdD = Integer.parseInt(roleId) + 3;
				
				SysRole roleC = sysRoleService.find(roleIdC);
				roleC.setAuthority(filter.getRoleNameEdit() + CargoUtils.ROLE_C);
				role.setDescription(filter.getDescriptionEdit());
				role.setModified(userAuth.getUsername());
				role.setModifiedOn(new Date());
				sysRoleService.update(roleC);
				
				SysRole roleU = sysRoleService.find(roleIdU);
				roleC.setAuthority(filter.getRoleNameEdit() + CargoUtils.ROLE_U);
				role.setDescription(filter.getDescriptionEdit());
				role.setModified(userAuth.getUsername());
				role.setModifiedOn(new Date());
				sysRoleService.update(roleU);
				
				SysRole roleD = sysRoleService.find(roleIdD);
				roleC.setAuthority(filter.getRoleNameEdit() + CargoUtils.ROLE_D);
				role.setDescription(filter.getDescriptionEdit());
				role.setModified(userAuth.getUsername());
				role.setModifiedOn(new Date());
				sysRoleService.update(roleD);
			}
		}
		return "redirect:/secure/role";
	}

	@RequestMapping(value = "/secure/role/showedit/{roleID}", method = RequestMethod.GET)
	public String getRoleEdit(@ModelAttribute("rolefilter") RoleFilter filter, @PathVariable Integer roleID, Model model) {
		SysRole roles = sysRoleService.find(roleID);
		model.addAttribute("role", roles);
		model.addAttribute("roleId", roleID);
		return "roleEdit";
	}
	
	@RequestMapping(value = "/secure/role/excel", method = RequestMethod.GET)
	@PreAuthorize(value = "hasAnyRole('ROLE_DW')")
	public void getExcelRole(Model model, HttpServletRequest request, HttpServletResponse  response) throws IOException{
		List<SysRole> lst = sysRoleService.getRoleOrderByRoleName();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		RoleExcel roleExcel = new RoleExcel();
		baos = roleExcel.exportExcel(lst, messageSource, localeResolver.resolveLocale(request));
		
		response.setContentType("application/xls");
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ "ROLE.xls" + "\";");
		response.setContentLength(baos.size());
		
		ServletOutputStream out = response.getOutputStream();
		baos.writeTo(out);
		out.flush();
		out.close();
	}
}
