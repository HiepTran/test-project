package com.datawings.app.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.LocaleResolver;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.DoubleUtil;
import com.datawings.app.filter.TaxFilter;
import com.datawings.app.model.CritIsoc;
import com.datawings.app.model.SysUser;
import com.datawings.app.model.Tax;
import com.datawings.app.report.TaxExcel;
import com.datawings.app.service.ICritIsocService;
import com.datawings.app.service.ITaxService;
import com.datawings.app.validator.TaxValidator;

@Controller
@SessionAttributes({ "taxFilter" })
@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_SETTING', 'ROLE_SETTING_CHARGES')")
public class TaxController {
	@Autowired
	private ITaxService taxService;
	
	@Autowired
	private TaxValidator taxValidator;
	
	@Autowired
	private ICritIsocService critIsocService;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@ModelAttribute("taxFilter")
	public TaxFilter getInitTax() {
		TaxFilter filter = new TaxFilter();
		filter.setType("GEN");
		return filter;
	}

	@RequestMapping(value = "/secure/tax", method = RequestMethod.GET)
	public String getTax(@ModelAttribute("taxFilter") TaxFilter filter, Model model, HttpServletRequest request, HttpServletResponse response) {
		List<Tax> listTax = taxService.getListtax(filter);
		List<CritIsoc> listIsoc = critIsocService.getAll();
		
		model.addAttribute("listIsoc", listIsoc);
		model.addAttribute("listTax", listTax);
		return "tax";
	}
	
	@RequestMapping(value = "/secure/tax", method = RequestMethod.POST)
	public String postStockCompagnie(@ModelAttribute("taxFilter") TaxFilter filter,BindingResult result, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		if(StringUtils.equals(action, "GO")){
			return "redirect:/secure/tax";
		}else if(StringUtils.equals(action, "RESET")){
			filter.init();
			filter.setType("GEN");
		}else if(StringUtils.equals(action, "UPDATE")){
			Integer id = ServletRequestUtils.getIntParameter(request, "id", -1);
			taxValidator.checkEdit(filter, result);
			
			if(result.hasErrors()){
				List<Tax> listTax = taxService.getListtax(filter);
				List<CritIsoc> listIsoc = critIsocService.getAll();
				
				model.addAttribute("listIsoc", listIsoc);
				model.addAttribute("listTax", listTax);
				model.addAttribute("errorEdit", true);
				model.addAttribute("id", id);
				return "tax";
			}
			
			Tax tax = taxService.find(id);
			if(tax != null){
				tax.setAmount(DoubleUtil.convertDouble(filter.getAmountEdit()));
				tax.setDescription(filter.getDescriptionEdit());
				
				taxService.merge(tax);
			}
		}else if(StringUtils.equals(action, "ADD")){
			Integer id = ServletRequestUtils.getIntParameter(request, "id", -1);
			Tax tax = taxService.find(id);
			
			taxValidator.checkAdd(filter, tax, result);
			
			if(result.hasErrors()){
				List<Tax> listTax = taxService.getListtax(filter);
				List<CritIsoc> listIsoc = critIsocService.getAll();
				
				model.addAttribute("listIsoc", listIsoc);
				model.addAttribute("listTax", listTax);
				model.addAttribute("errorAdd", true);
				model.addAttribute("id", id);
				return "tax";
			}
			
			Date dateStart = DateUtil.string2Date(filter.getDateFromAdd(), "dd/MM/yyyy");
			tax.setDateEnd(DateUtil.addDay(dateStart, -1));
			tax.setModifiedBy(sysUser.getUsername());
			tax.setModifiedDate(new Date());
			
			Tax taxAdd = new Tax();
			taxAdd.setIsoc(tax.getIsoc());
			taxAdd.setCode(tax.getCode());
			taxAdd.setSubCode(tax.getSubCode());
			taxAdd.setItem(tax.getItem());
			taxAdd.setCutp(tax.getCutp());
			taxAdd.setUnit(tax.getUnit());
			taxAdd.setType(tax.getType());
			taxAdd.setDateStart(dateStart);
			taxAdd.setDateEnd(DateUtil.string2Date("31/12/9999", "dd/MM/yyyy"));
			taxAdd.setAmount(DoubleUtil.convertDouble(filter.getAmountAdd()));
			taxAdd.setDescription(filter.getDescriptionAdd());
			taxAdd.setCreatedBy(sysUser.getUsername());
			taxAdd.setCreatedDate(new Date());
			
			taxService.merge(tax);
			taxService.save(taxAdd);
			
			//init filter
			filter.setDateFromAdd("");
			filter.setAmountAdd("0.0");
			filter.setDescriptionAdd("");
		}else if(StringUtils.equals(action, "ADD_MODAL")){
			taxValidator.checkAddModal(filter, result);
			
			if(result.hasErrors()){
				List<Tax> listTax = taxService.getListtax(filter);
				List<CritIsoc> listIsoc = critIsocService.getAll();
				
				model.addAttribute("listIsoc", listIsoc);
				model.addAttribute("listTax", listTax);
				model.addAttribute("errorAddModal", true);
				return "tax";
			}
			
			Tax tax = new Tax();
			tax.setIsoc(filter.getIsocExt());
			tax.setCode(filter.getCodeExt().toUpperCase());
			tax.setSubCode(filter.getSubCodeExt().toUpperCase());
			tax.setItem(filter.getItemExt().toUpperCase());
			tax.setCutp(filter.getCutpExt());
			tax.setUnit(filter.getUnitExt());
			tax.setType("GEN");
			tax.setDateStart(DateUtil.string2Date(filter.getDateFromExt(), "dd/MM/yyyy"));
			tax.setDateEnd(DateUtil.string2Date("31/12/9999", "dd/MM/yyyy"));
			tax.setAmount(DoubleUtil.convertDouble(filter.getAmountExt()));
			tax.setDescription(filter.getDescriptionExt());
			tax.setCreatedBy(sysUser.getUsername());
			tax.setCreatedDate(new Date());
			
			taxService.save(tax);
			
			filter.setIsocExt("");
			filter.setCodeExt("");
			filter.setSubCodeExt("");
			filter.setItemExt("");
			filter.setCutpExt("");
			filter.setUnitExt("");
			filter.setDateFromExt("");
			filter.setAmountExt("");
			filter.setDescriptionExt("");
			
		}
		return "redirect:/secure/tax";
	}
	
	@RequestMapping(value = {"/secure/tax/model/edit/{id}"}, method = RequestMethod.GET)
	public String getTaxModelEdit(@PathVariable Integer id, Model model) {	
		Tax tax = taxService.find(id);
		model.addAttribute("tax", tax);
		return "taxEdit";
	}
	
	@RequestMapping(value = {"/secure/tax/model/add/{id}"}, method = RequestMethod.GET)
	public String getTaxModelAdd(@PathVariable Integer id, Model model) {	
		Tax tax = taxService.find(id);
		model.addAttribute("tax", tax);
		return "taxAdd";
	}
	
	@RequestMapping(value = "/secure/tax/excel", method = RequestMethod.GET)
	public void getExcelTax(@ModelAttribute("taxFilter") TaxFilter filter, Model model,
			HttpServletRequest request, HttpServletResponse  response) throws IOException{
		List<Tax> lst = taxService.getListtax(filter);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		TaxExcel taxExcel = new TaxExcel();
		baos = taxExcel.exportExcel(lst, filter, messageSource, localeResolver.resolveLocale(request));
		
		response.setContentType("application/xls");
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ "TAX.xls" + "\";");
		response.setContentLength(baos.size());
		
		ServletOutputStream out = response.getOutputStream();
		baos.writeTo(out);
		out.flush();
		out.close();
	}
}
