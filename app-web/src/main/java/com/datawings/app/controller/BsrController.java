package com.datawings.app.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.LocaleResolver;

import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.BsrFilter;
import com.datawings.app.model.Bsr;
import com.datawings.app.model.BsrId;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.report.BsrExcel;
import com.datawings.app.service.IBsrService;
import com.datawings.app.validator.BsrValidator;
import com.datawings.app.validator.ValidationError;

@Controller
@SessionAttributes({ "bsrFilter" })
public class BsrController {
	
	@Autowired
	private IBsrService bsrService;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private BsrValidator bsrValidator;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	
	@ModelAttribute("bsrFilter")
	public BsrFilter getBsrFilter(){
		BsrFilter filter = new BsrFilter();
		filter.setPageSize(100);
		return filter;
	}
	
	@RequestMapping(value = "/secure/bsr", method = RequestMethod.GET)
	public String getBsr(@ModelAttribute("bsrFilter") BsrFilter filter, Model model){
		return "bsr";
	}
	
	@RequestMapping(value = "/secure/bsr/loadpage/errorload.json", method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Object> doCheckValidatorLoad(@ModelAttribute("bsrFilter") BsrFilter filter, BindingResult result, Model model, Locale locale){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = bsrValidator.doValidatorLoad(filter, result);
		
		if(errCode > 0){
			rs.put("errCodeTable", errCode);
		}
		return rs;
	}
	
	
	@RequestMapping(value = "/secure/bsr/loadpage/error.json", method = RequestMethod.GET)
	@ResponseBody
	public HashMap<String, Object> doCheckValidatorTable(@ModelAttribute("bsrFilter") BsrFilter filter, BindingResult result, Model model, Locale locale){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = bsrValidator.doValidatorTable(filter, result);
		
		if(errCode > 0){
			rs.put("errCodeTable", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for(int i = 0; i < result.getAllErrors().size(); i++){
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}
	
	@RequestMapping(value = "/secure/bsr/loadpage", method = RequestMethod.GET)
	public String getBsrLoadPage(@ModelAttribute("bsrFilter") BsrFilter filter, Integer pageNo, Model model, HttpServletRequest request){
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		String column = ServletRequestUtils.getStringParameter(request, "column", "");
		if(StringUtils.equals(action, "SORTBYCURRFROM") && StringUtils.equals(column, "COL_CURR_FROM")){
			filter.setCol(column);
			if(filter.getSorting() == true)
				filter.setSorting(false);
			else
				filter.setSorting(true);
		}
		if(StringUtils.equals(action, "SORTBYDAYSTART") && StringUtils.equals(column, "COL_DAY_START")){
			filter.setCol(column);
			if(filter.getSorting() == true)
				filter.setSorting(false);
			else
				filter.setSorting(true);
		}
		if(StringUtils.equals(column, "DEFAULT")){
			filter.setCol(column);
		}
		Integer rowCount = bsrService.getRowCountBsr(filter);
		filter.setRowCount(rowCount);
		if(pageNo == null)
			pageNo = 0;
		filter.setPage(pageNo);
		
		List<Bsr> lstBsr = bsrService.getBsrs(filter);
		
		model.addAttribute("lstBsr", lstBsr);
		model.addAttribute("row", (filter.getPage()) * filter.getPageSize());
		return "bsrLoadPage";
	}
	
	@RequestMapping(value = "/secure/bsr", method = RequestMethod.POST)
	public String postBsr(@ModelAttribute("bsrFilter")BsrFilter filter, Integer pageNo, Model model, HttpServletRequest request){
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if(StringUtils.equals(action, "RESET")){
			filter.init();
			filter.setPageSize(100);
		}
		return "redirect:/secure/bsr";
	}
	
	@RequestMapping(value = "/secure/bsr/create/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorCreateBsr(@ModelAttribute("bsrFilter") BsrFilter filter, BindingResult result, Model model, Locale locale){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = bsrValidator.doValidatorCreateBsr(filter, result);
		
		if(errCode > 0){
			rs.put("errCodeCreate", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for(int i = 0; i < result.getAllErrors().size(); i++){
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}
	
	@RequestMapping(value = "/secure/bsr/create", method = RequestMethod.POST)
	public String createBsr(@ModelAttribute("bsrFilter") BsrFilter filter, Integer pageNo, HttpServletRequest request, Model model){
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if(StringUtils.equals(action, "CREATE")){
			Bsr newBsr = new Bsr();
			BsrId bsrId = new BsrId();
			bsrId.setCurrFrom(filter.getCurrFromAdd().trim().toUpperCase());
			bsrId.setCurrTo(filter.getCurrToAdd().trim().toUpperCase());
			bsrId.setDateStart(DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"));
			newBsr.setBsrId(bsrId);
			if(StringUtils.equals(filter.getDateEndAdd(), ""))
				filter.setDateEndAdd(CargoUtils.MAX_DATE);
			if(DateUtil.checAfter(DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"), DateUtil.string2Date(filter.getDateEndAdd(), "dd/MM/yyyy")))
				newBsr.setDateEnd(DateUtil.string2Date(filter.getDateEndAdd(), "dd/MM/yyyy"));
			newBsr.setRate(filter.getRateAdd());
			newBsr.setTimeStamp_C(new Date());
			bsrService.save(newBsr);
			filter.initFormAdd();
			filter.setPageSize(100);
		}
		return "redirect:/secure/bsr";
	}
	
	@RequestMapping(value = "/secure/bsr/excel", method = RequestMethod.GET)
	public void getExcelBsr(@ModelAttribute("bsrFilter")BsrFilter filter, Model model, HttpServletRequest request, HttpServletResponse response) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<Bsr> lst = bsrService.getBsrs(filter);
		BsrExcel bsrExcel = new BsrExcel();
		baos = bsrExcel.exportExcel(lst, filter, messageSource, localeResolver.resolveLocale(request));
		
		response.setContentType("application/xls");
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ "Bsr.xls" + "\";");
		response.setContentLength(baos.size());
		
		ServletOutputStream out = response.getOutputStream();
		baos.writeTo(out);
		out.flush();
		out.close();
	}
	
}
