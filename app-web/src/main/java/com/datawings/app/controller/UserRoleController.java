package com.datawings.app.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.LocaleResolver;

import com.datawings.app.filter.UserFilter;
import com.datawings.app.model.AirlineParam;
import com.datawings.app.model.CritIsoc;
import com.datawings.app.model.SysRole;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.report.UserRoleExcel;
import com.datawings.app.service.IAirlineParamService;
import com.datawings.app.service.ICritIsocService;
import com.datawings.app.service.ISysRoleService;
import com.datawings.app.service.ISysUserService;
import com.datawings.app.validator.UserValidator;
import com.datawings.app.validator.ValidationError;

@Controller
@SessionAttributes({ "userFilter" })
@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN')")
public class UserRoleController {
	
	@Autowired
	private ISysUserService sysUserService;
	
	@Autowired
	private ISysRoleService sysRoleService;
	
	@Autowired
	private ICritIsocService critIsocService;
	
	@Autowired 
	IAirlineParamService airlineParamService;
	
	@Autowired
	private UserValidator userValidator;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;

	@ModelAttribute("userFilter")
	public UserFilter userFilter() {
		UserFilter filter = new UserFilter();
		return filter;
	}

	@RequestMapping(value = "/secure/user", method = RequestMethod.GET)
	public String getUserFilter(@ModelAttribute("userFilter") UserFilter userFilter, Model model) throws IOException {
		sysUserService.getRowsCount();
		List<CritIsoc> lstCritIsoc = critIsocService.getAll();
		AirlineParam airlineParam = airlineParamService.findOne();
		model.addAttribute("airlineParam", airlineParam);
		model.addAttribute("lstCritIsoc", lstCritIsoc);
		return "user";
	}

	@RequestMapping(value = "/secure/user/loadpage", method = RequestMethod.GET)
	public String loadPageAjax(@ModelAttribute("userFilter") UserFilter userFilter, Model model) {
		SysUser userAuth = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<SysUser> lstUser = new ArrayList<SysUser>();
		
		for(SysRole r : userAuth.getRoles()){
			if(StringUtils.equals(r.getAuthority(), CargoUtils.ROLE_DW)){
				lstUser = sysUserService.getUsers(userFilter);
			}
			else if(StringUtils.equals(r.getAuthority(), CargoUtils.ROLE_ADMIN)){
				lstUser = sysUserService.getUsersForRoleAdmin(userFilter);
			}
		}
		
		//check if users have role admin then "setCheckAdmin = true" to filter action modify 
		for(SysUser us : lstUser){
			for(SysRole r : us.getRoles()){
				if(StringUtils.equals(r.getAuthority(), CargoUtils.ROLE_ADMIN)){
					us.setCheckAdmin(true);
				}
				if(StringUtils.equals(r.getAuthority(), CargoUtils.ROLE_DW))
					us.setCheckDW(true);
			}
		}
		
		model.addAttribute("lstUser", lstUser);
		
		return "userLoadPage";
	}

	@RequestMapping(value = "/secure/user", method = RequestMethod.POST)
	public String postUserFilter(@ModelAttribute("userFilter") UserFilter userFilter, Model model,
			HttpServletRequest request) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");

		if (StringUtils.equals(action, "GO"))
			return "redirect:/secure/user";
		if(StringUtils.equals(action, "RESET"))
			userFilter.init();
		return "redirect:/secure/user";
	}

	// show from modify user
	@RequestMapping(value = "/secure/user/ajax/{id}", method = RequestMethod.GET)
	public String getUserByID(@PathVariable Integer id, Model model) {
		SysUser user = sysUserService.find(id);
		SysUser prinUser = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<SysRole> lstRole = sysRoleService.findAllRoleUser(prinUser);

		//return list all role user (include roles C, U, D) 
		List<SysRole> lstRoleUser = sysRoleService.findRoleOfUserByUserID(id);
		
		//return list role not include C, U, D to show in table role user
		List<SysRole> rtLstRoleUser = new ArrayList<SysRole>();
		Map<String, Boolean> checkCUD = new HashMap<String, Boolean>();
		for(SysRole rU :  lstRoleUser){
			if(!StringUtils.endsWith(rU.getAuthority(), CargoUtils.ROLE_C) && !StringUtils.endsWith(rU.getAuthority(), CargoUtils.ROLE_U)
					&& !StringUtils.endsWith(rU.getAuthority(), CargoUtils.ROLE_D)){
				rtLstRoleUser.add(rU);
			}
			else 
				checkCUD.put(StringUtils.substring(rU.getAuthority(), 0, rU.getAuthority().length() - 2), true);
		}
		List<CritIsoc> lstCritIsoc = critIsocService.getAll();
		
		model.addAttribute("lstCritIsoc", lstCritIsoc);
		model.addAttribute("lstRole", lstRole);
		model.addAttribute("lstRoleUser", rtLstRoleUser);
		model.addAttribute("lstAllRoleUser", lstRoleUser);
		model.addAttribute("user", user);
		model.addAttribute("userId", id);
		model.addAttribute("checkCUD", checkCUD);
		return "userAjax";
	}

	// do delete User Role
	@RequestMapping(value = "/secure/user/delete/{id}", method = RequestMethod.POST)
	public String deleteUserRole(@ModelAttribute("userFilter") UserFilter filter, @PathVariable Integer id, Model model,
			HttpServletRequest request) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		String idUser = ServletRequestUtils.getStringParameter(request, "idUser", "");
		if (StringUtils.equals(action, "DELETE")) {
			SysUser user = sysUserService.find(Integer.parseInt(idUser));
			SysRole role = sysRoleService.find(id);
			if (role != null && user != null && !StringUtils.equals(filter.getRole(), "ROLE_ADMIN")){
				sysUserService.deleteRoleUserByRoleId(user, role.getSysRoleId());
				Integer idC = role.getSysRoleId() + 1;
				Integer idU = role.getSysRoleId() + 2;
				Integer idD = role.getSysRoleId() + 3;
				sysUserService.deleteRoleUserByRoleId(user, idC);
				sysUserService.deleteRoleUserByRoleId(user, idU);
				sysUserService.deleteRoleUserByRoleId(user, idD);
			}
		}

		// update form user ajax
		SysUser prinUser = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<SysRole> lstRole = sysRoleService.findAllRoleUser(prinUser);
		SysUser user = sysUserService.find(Integer.parseInt(idUser));
		List<SysRole> lstRoleUser = sysRoleService.findRoleOfUserByUserID(Integer.parseInt(idUser));
		List<SysRole> rtLstRoleUser = new ArrayList<SysRole>();
		Map<String, Boolean> checkCUD = new HashMap<String, Boolean>();
		for(SysRole rU :  lstRoleUser){
			if(!StringUtils.endsWith(rU.getAuthority(), CargoUtils.ROLE_C) && !StringUtils.endsWith(rU.getAuthority(), CargoUtils.ROLE_U)
					&& !StringUtils.endsWith(rU.getAuthority(), CargoUtils.ROLE_D)){
				rtLstRoleUser.add(rU);
			}
			else 
				checkCUD.put(StringUtils.substring(rU.getAuthority(), 0, rU.getAuthority().length() - 2), true);
		}
		List<CritIsoc> lstCritIsoc = critIsocService.getAll();
		
		model.addAttribute("lstCritIsoc", lstCritIsoc);
		model.addAttribute("lstRole", lstRole);
		model.addAttribute("lstRoleUser", rtLstRoleUser);
		model.addAttribute("lstAllRoleUser", lstRoleUser);
		model.addAttribute("userId", idUser);
		model.addAttribute("user", user);
		model.addAttribute("checkCUD", checkCUD);
		return "userAjax";
	}

	// do add role user
	@RequestMapping(value = "/secure/user/add/{id}", method = RequestMethod.POST)
	public String addRoleUser(@ModelAttribute("userFilter") UserFilter userFilter, @PathVariable Integer id,
			Model model) {
		if (!StringUtils.equals(userFilter.getRole(), "")) {
			
			//delete old role
			SysUser user = sysUserService.find(id);
			SysRole role = sysRoleService.getRoleByName(userFilter.getRole());
			sysUserService.deleteRoleUserByRoleId(user, role.getSysRoleId());
			Integer idC = role.getSysRoleId() + 1;
			Integer idU = role.getSysRoleId() + 2;
			Integer idD = role.getSysRoleId() + 3;
			sysUserService.deleteRoleUserByRoleId(user, idC);
			sysUserService.deleteRoleUserByRoleId(user, idU);
			sysUserService.deleteRoleUserByRoleId(user, idD);
			
			//add new role
			SysUser userR = sysUserService.find(id);
			userR.getRoles().add(role);

			String strC = userFilter.getRole() + "" + userFilter.getRoleCreate();
			SysRole roleC = sysRoleService.getRoleByName(strC);
			if (roleC != null && !StringUtils.equals(roleC.getAuthority(), role.getAuthority())) {
				userR.getRoles().add(roleC);
			}

			String strU = userFilter.getRole() + "" + userFilter.getRoleUpdate();
			SysRole roleU = sysRoleService.getRoleByName(strU);
			if (roleU != null && !StringUtils.equals(roleU.getAuthority(), role.getAuthority())) {
				userR.getRoles().add(roleU);
			}

			String strD = userFilter.getRole() + "" + userFilter.getRoleDelete();
			SysRole roleD = sysRoleService.getRoleByName(strD);
			if (roleD != null && !StringUtils.equals(roleD.getAuthority(), role.getAuthority())) {
				userR.getRoles().add(roleD);
			}

			sysUserService.merge(userR);
		}
		
		//update form user ajax
		SysUser user = sysUserService.find(id);
		SysUser prinUser = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<SysRole> lstRole = sysRoleService.findAllRoleUser(prinUser);
		List<SysRole> lstRoleUser = sysRoleService.findRoleOfUserByUserID(id);
		List<SysRole> rtLstRoleUser = new ArrayList<SysRole>();
		Map<String, Boolean> checkCUD = new HashMap<String, Boolean>();
		for(SysRole rU :  lstRoleUser){
			if(!StringUtils.endsWith(rU.getAuthority(), CargoUtils.ROLE_C) && !StringUtils.endsWith(rU.getAuthority(), CargoUtils.ROLE_U)
					&& !StringUtils.endsWith(rU.getAuthority(), CargoUtils.ROLE_D)){
				rtLstRoleUser.add(rU);
			}
			else 
				checkCUD.put(StringUtils.substring(rU.getAuthority(), 0, rU.getAuthority().length() - 2), true);
		}
		List<CritIsoc> lstCritIsoc = critIsocService.getAll();
		
		model.addAttribute("lstCritIsoc", lstCritIsoc);
		model.addAttribute("lstRole", lstRole);
		model.addAttribute("lstRoleUser", rtLstRoleUser);
		model.addAttribute("lstAllRoleUser", lstRoleUser);
		model.addAttribute("userId", id);
		model.addAttribute("user", user);
		model.addAttribute("checkCUD", checkCUD);
		userFilter.init();
		return "userAjax";
	}

	// do validator create
	@RequestMapping(value = "/secure/user/create/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorCreate(@ModelAttribute("userFilter") UserFilter userFilter,
			BindingResult result, Model model, Locale locale) {
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = userValidator.doValidatorCreate(userFilter, result);
		if (errCode > 0) {
			rs.put("errCodeCreate", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for (int i = 0; i < result.getAllErrors().size(); i++) {
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}

	// do create
	@RequestMapping(value = "/secure/user/create", method = RequestMethod.POST)
	public String createUser(@ModelAttribute("userFilter") UserFilter userFilter, HttpServletRequest request,
			Model model) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if (StringUtils.equals(action, "CREATE")) {
			SysUser newUser = new SysUser();
			SysUser user = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			newUser.setUsername(userFilter.getUsernameAdd().trim());
			newUser.setPassword(userFilter.getPasswordAdd());
			newUser.setName(userFilter.getNameAdd());
			newUser.setTelephone(userFilter.getTelephoneAdd());
			newUser.setEmail(userFilter.getEmailAdd());
			newUser.setAirlineCode(userFilter.getAirlineCodeAdd());
			newUser.setCountryCode(userFilter.getCountryCodeAdd());
			newUser.setLanguage(userFilter.getLanguageAdd());
			newUser.setLoginDate(null);
			newUser.setLastLoginDate(null);
			newUser.setCreatedBy(user.getUsername());
			newUser.setCreatedOn(new Date());
			newUser.setModifiedBy(user.getUsername());
			newUser.setModifiedOn(new Date());
			newUser.setIsEnable(true);
			sysUserService.save(newUser);
			userFilter.init();
		}
		return "redirect:/secure/user";
	}

	// validator modify user
	@RequestMapping(value = "/secure/user/edit/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorEdit(@ModelAttribute("userFilter") UserFilter filter,
			BindingResult result, Model model, Locale locale) {
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = userValidator.doValidatorEdit(filter, result);
		if (errCode > 0) {
			rs.put("errCodeEdit", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for (int i = 0; i < result.getAllErrors().size(); i++) {
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}

	// do edit user
	@RequestMapping(value = "/secure/user/edit", method = RequestMethod.POST)
	public String editUser(@ModelAttribute("userFilter") UserFilter userFilter, HttpServletRequest request,
			Model model) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if (StringUtils.equals(action, "MODIFY")) {
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			SysUser user = sysUserService.find(Integer.parseInt(id));
			SysUser prinUser = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			user.setUsername(userFilter.getUsernameEdit().trim());
			if(StringUtils.isNotBlank(userFilter.getPasswordEdit()))
				user.setPassword(userFilter.getPasswordEdit());
			user.setName(userFilter.getNameEdit());
			user.setTelephone(userFilter.getTelephoneEdit());
			user.setEmail(userFilter.getEmailEdit());
			user.setLanguage(userFilter.getLanguageEdit());
			user.setAirlineCode(userFilter.getAirlineCodeEdit());
			user.setCountryCode(userFilter.getCountryCodeEdit());
			user.setModifiedBy(prinUser.getUsername());
			user.setModifiedOn(new Date());
			sysUserService.merge(user);
			userFilter.init();
		}

		return "redirect:/secure/user";
	}
	
	//export exel
	@RequestMapping(value = "/secure/user/excel", method = RequestMethod.GET)
	public void getExcelUserRole(@ModelAttribute("userFilter") UserFilter filter, Model model,
			HttpServletRequest request, HttpServletResponse response) throws IOException{
		SysUser user = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<SysUser> lst = sysUserService.getUsersForRoleAdmin(filter);
		UserRoleExcel userExcel = new UserRoleExcel();
		baos = userExcel.exportExcel(lst, filter, user, messageSource, localeResolver.resolveLocale(request));
		
		response.setContentType("application/xls");
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ "User.xls" + "\";");
		response.setContentLength(baos.size());
		
		ServletOutputStream out = response.getOutputStream();
		baos.writeTo(out);
		out.flush();
		out.close();
	}
	
	//change status SysUser
	@RequestMapping(value="/secure/user/changestatus/{userId}", method = RequestMethod.POST)
	public String changeStatus(@ModelAttribute("userFilter") UserFilter userFilter , @PathVariable Integer userId, Model model){
		SysUser user = sysUserService.find(userId);
		SysUser userAuth = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		boolean checkRoleDw = false;
		for(SysRole r : user.getRoles()){
			if(StringUtils.equals(r.getAuthority(), CargoUtils.ROLE_DW)){
				checkRoleDw = true;
				break;
			}
		}

		if(checkRoleDw == false){
			if(user.getIsEnable() == true)
				user.setIsEnable(false);
			else {
				user.setIsEnable(true);
			}
			user.setModifiedBy(userAuth.getUsername());
			user.setModifiedOn(new Date());
			sysUserService.merge(user);	
		}
		//update table user_role
		List<SysUser> lstUser = new ArrayList<SysUser>();
		
		for(SysRole r : userAuth.getRoles()){
			if(StringUtils.equals(r.getAuthority(), CargoUtils.ROLE_DW)){
				lstUser = sysUserService.getUsers(userFilter);
			}
			else if(StringUtils.equals(r.getAuthority(), CargoUtils.ROLE_ADMIN)){
				lstUser = sysUserService.getUsersForRoleAdmin(userFilter);
			}
		}
		
		//check if users have role admin then "setCheckAdmin = true" to filter action modify 
		for(SysUser us : lstUser){
			for(SysRole r : us.getRoles()){
				if(StringUtils.equals(r.getAuthority(), CargoUtils.ROLE_ADMIN)){
					us.setCheckAdmin(true);
				}
				if(StringUtils.equals(r.getAuthority(), CargoUtils.ROLE_DW))
					us.setCheckDW(true);
			}
		}
		
		model.addAttribute("lstUser", lstUser);
		
		return "userLoadPage";
	}
}
