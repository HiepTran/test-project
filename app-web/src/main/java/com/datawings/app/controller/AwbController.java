package com.datawings.app.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.LocaleResolver;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.DoubleUtil;
import com.datawings.app.common.IntegerUtil;
import com.datawings.app.filter.AwbDetailFilter;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.AwbTaxFilter;
import com.datawings.app.model.Awb;
import com.datawings.app.model.AwbDetail;
import com.datawings.app.model.AwbDetailId;
import com.datawings.app.model.AwbFlown;
import com.datawings.app.model.AwbTax;
import com.datawings.app.model.AwbTaxId;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.report.AwbSalesExcel;
import com.datawings.app.service.IAwbFlownService;
import com.datawings.app.service.IAwbService;
import com.datawings.app.validator.AwbValidator;
import com.datawings.app.validator.ValidationError;

@Controller
@SessionAttributes({"awbListFilter", "awbDetailFilter"})
@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_AWB', 'ROLE_AWB_EMISSION')")
public class AwbController {

	@Autowired
	private IAwbService awbService;
	
	@Autowired
	private IAwbFlownService awbFlownService;
	
	@Autowired
	private AwbValidator awbValidator;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@ModelAttribute("awbListFilter")  
    public AwbFilter getInitCreate() {  
		AwbFilter filter = new AwbFilter();
		
        return filter;  
    }
		
	@ModelAttribute("awbDetailFilter")  
    public AwbFilter getInitDetail() {  
		AwbFilter filter = new AwbFilter();
        return filter;  
    } 
	
	@RequestMapping(value = "/secure/awb", method = RequestMethod.GET)
	public String getAwb(@ModelAttribute("awbListFilter") AwbFilter filter, Model model, 
			HttpServletRequest request, HttpServletResponse response) {
		filter.setRowCount(awbService.getLTARowsCount(filter));
		
		return "awb";
	}
	
	@RequestMapping(value = {"/secure/awb/loadPage"}, method = RequestMethod.GET)
	public String getListLTAAjax(@ModelAttribute("awbListFilter") AwbFilter filter, Integer pageNo, Model model) {
		filter.setPage(pageNo);
		List<Awb> listAwb = awbService.getListAwb(filter);
		
		model.addAttribute("row", (filter.getPage()) * filter.getPageSize());
		model.addAttribute("listAwb", listAwb);
		return "awbloadPage";
	}
	
	@RequestMapping(value = "/secure/awb", method = RequestMethod.POST)
	public String postAwb(@ModelAttribute("awbListFilter") AwbFilter filter,
			BindingResult result, Model model, HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		
		if(StringUtils.equals(action, "VIEW")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			request.getSession().setAttribute("FROM_PAGE", "AWB");
			
			return "redirect:/secure/awb/detail?id="+id;
		}else if(StringUtils.equals(action, "RESET")){
			filter.init();
		}else if(StringUtils.equals(action, "GO")){
			filter.setPage(0);
		}else if(StringUtils.equals(action, "DELETE")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			awbService.deleteById(id);
			filter.setPage(0);
		}else if(StringUtils.equals(action, "FLOWN")){
			int id = ServletRequestUtils.getIntParameter(request, "id");
			int idFlown = ServletRequestUtils.getIntParameter(request, "idFlown");
			
			request.getSession().setAttribute("FROM_PAGE", "AWB");
			
			return "redirect:/secure/awb/flown/detail?id=" + id + "&idFlown=" + idFlown;
		}
		return "redirect:/secure/awb";
	}
	
	@RequestMapping(value = "/secure/awb/view", method = RequestMethod.GET)
	public String getAwbModel(Model model, HttpServletRequest request) {
		
		String id = request.getParameter("id");
		Awb awb = awbService.find(id);
		List<AwbFlown> awbFlowns = awbFlownService.getByLta(awb.getLta());
		
		model.addAttribute("awb", awb);
		model.addAttribute("awbFlowns", awbFlowns);
		return "modalAwb";
	}
	
	@RequestMapping(value = "/secure/awb/excel", method = RequestMethod.GET)
	public void getExcelDashboard(@ModelAttribute("awbListFilter") AwbFilter filter,
			Model model, HttpServletRequest request, HttpServletResponse  response) throws IOException {
		
		List<Awb> listAwb = awbService.getExcelAwb(filter);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		AwbSalesExcel awbSalesExcel = new AwbSalesExcel();
		baos = awbSalesExcel.exportExcel(listAwb, filter, messageSource, localeResolver.resolveLocale(request));
		
		response.setContentType("application/xls");
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ "LTA_VENTE.xls" + "\";");
		response.setContentLength(baos.size());

		ServletOutputStream out = response.getOutputStream();
		baos.writeTo(out);
		out.flush();
		out.close();
	}
	
	@RequestMapping(value = "/secure/awb/detail", method = RequestMethod.GET)
	public String getAwbDetail(@ModelAttribute("awbDetailFilter") AwbFilter filter, Model model, HttpServletRequest request) {
	
		String id = ServletRequestUtils.getStringParameter(request, "id", "");
		
		Awb awb = awbService.find(id);
		Integer sizeTax = awb.getAwbTax().size();
		Integer sizeDetail = awb.getAwbDetail().size();
		
		Set<AwbTax> setTax = new HashSet<AwbTax>();
		for (int i = 0; i < CargoUtils.NB_TAX - sizeTax; i++) {
			setTax.add(new AwbTax());
		}
		awb.getAwbTax().addAll(setTax);
		
		Set<AwbDetail> setDetail = new HashSet<AwbDetail>();
		for (int i = 0; i < CargoUtils.NB_DETAIL - sizeDetail; i++) {
			setDetail.add(new AwbDetail());
		}
		awb.getAwbDetail().addAll(setDetail);
		
		model.addAttribute("awb", awb);
		return "awbDetail";
	}
	
	@RequestMapping(value = "/secure/awb/detail/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorAwb(@ModelAttribute("awbDetailFilter") AwbFilter filter, HttpServletRequest request,BindingResult result, Locale locale){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = awbValidator.checkDetail(filter, result);
		if(errCode > 0){
			rs.put("errCode", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for(int i = 0;i <result.getAllErrors().size();i++){
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null, 
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}
	

	@RequestMapping(value = "/secure/awb/detail/errorTax.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorTax(@ModelAttribute("awbDetailFilter") AwbFilter filter, HttpServletRequest request,BindingResult result, Locale locale){
		String id = ServletRequestUtils.getStringParameter(request, "id", "");
		
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = awbValidator.checkDetailTax(id, filter, result);
		if(errCode > 0){
			rs.put("errCodeTax", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for(int i = 0;i <result.getAllErrors().size();i++){
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null, 
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}
	
	@RequestMapping(value = "/secure/awb/detail", method = RequestMethod.POST)
	public String postAwbDetail(@ModelAttribute("awbDetailFilter") AwbFilter filter, BindingResult result, Model model, HttpServletRequest request, HttpServletResponse response) {
	
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if(StringUtils.equals(action, "BACK")){
			String fromPage = (String) request.getSession().getAttribute("FROM_PAGE");
			
			if(StringUtils.equals(fromPage, "AWB")){
				return "redirect:/secure/awb";
			}else if(StringUtils.equals(fromPage, "RDV")){
				String rdv = (String) request.getSession().getAttribute("ID_DETAIL");
				return "redirect:/secure/rdv/detail?rdvCode=" + rdv;
			}
		}else if(StringUtils.equals(action, "VALIDER")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			Awb awb = awbService.find(id);
			
			awb.setStatusControl(CargoUtils.STATUS_F);
			awb.setAgtnGsa(filter.getAgtnGsa().toUpperCase());
			awb.setIsoc(filter.getIsoc().toUpperCase());
			awb.setDateExecute(DateUtil.string2Date(filter.getDateExecute(), "dd/MM/yyyy"));
			awb.setShipName(filter.getShipName().toUpperCase());
			awb.setShipAddress1(filter.getShipAddress1().toUpperCase());
			awb.setShipAddress2(filter.getShipAddress2().toUpperCase());
			awb.setShipAddress3(filter.getShipAddress3().toUpperCase());
			awb.setConsigneeName(filter.getConsigneeName().toUpperCase());
			awb.setConsigneeAddress1(filter.getConsigneeAddress1().toUpperCase());
			awb.setConsigneeAddress2(filter.getConsigneeAddress2().toUpperCase());
			awb.setConsigneeAddress3(filter.getConsigneeAddress3().toUpperCase());
			awb.setOrig(filter.getOrig().toUpperCase());
			awb.setDest(filter.getDest().toUpperCase());
			awb.setFlightNumber(filter.getFlightNumber());
			awb.setFlightDate(DateUtil.string2Date(filter.getFlightDate(), "dd/MM/yyyy"));
			awb.setCutp(filter.getCutp().toUpperCase());
			awb.setChargeIndicator(filter.getChargeIndicator());
			awb.setOtherIndicator(filter.getOtherIndicator());
			awb.setDeclaredCarrige(filter.getDeclaredCarrige().toUpperCase());
			awb.setDeclaredCustom(filter.getDeclaredCustom().toUpperCase());
			awb.setHandlingInfor(filter.getHandlingInfor().toUpperCase());
			awb.setDescriptUser(filter.getDescriptUser());
			awb.setModePayment(filter.getModePayment());
			awb.setKm(DoubleUtil.convertAmount(filter.getKm()));
			if(StringUtils.equals(awb.getModePayment(), "CC")){
				awb.setNbCart(filter.getNbCart());
				awb.setDateExp(filter.getDateExp());
				awb.setNameCart(filter.getNameCart().toUpperCase());
			}
			
			//Detail
			Set<AwbDetail> detailCu = new HashSet<AwbDetail>();
			for (AwbDetail elm : awb.getAwbDetail()) {
				detailCu.add(elm);
			}
			awb.getAwbDetail().removeAll(detailCu);
			
			for (int i = 0; i < filter.getAwbDetail().size(); i++) {
				AwbDetailFilter elm = filter.getAwbDetail().get(i);
				
				int retval =  IntegerUtil.convertInteger(elm.getQuantity());
				 
				if(retval > 0){
					AwbDetail awbDetail = new AwbDetail();
					AwbDetailId awbDetailId = new AwbDetailId();
					awbDetailId.setLta(awb.getLta());
					awbDetailId.setSequence(i+1);
					
					awbDetail.setId(awbDetailId);
					
					awbDetail.setQuantity(IntegerUtil.convertInteger(elm.getQuantity()));
					awbDetail.setWeightIndicator(elm.getWeightIndicator());
					awbDetail.setWeightGross(DoubleUtil.convertDouble(elm.getWeightGross()));
					awbDetail.setRateClass(elm.getRateClass());
					awbDetail.setWeightCharge(DoubleUtil.convertDouble(elm.getWeightCharge()));
					awbDetail.setUnit(DoubleUtil.convertDouble(elm.getUnit()));
					awbDetail.setUnitCass(awb.getUnitCass());
					awbDetail.setAmount(DoubleUtil.round(awbDetail.getWeightCharge() * awbDetail.getUnit(), 2));
					awbDetail.setDescription(elm.getDescription().toUpperCase());
					awbDetail.setCreatedBy(awb.getCreatedBy());
					awbDetail.setCreatedDate(awb.getCreatedDate());
					awbDetail.setModifiedBy(sysUser.getUsername());
					awbDetail.setModifiedDate(new Date());
					
					awbDetail.setAwb(awb);
					awb.getAwbDetail().add(awbDetail);
				}
			}
			
			//TOTAL
			Double weightGross = 0.0;
			Double weightCharge = 0.0;
			Double charge = 0.0;
			for (AwbDetail elm : awb.getAwbDetail()) {
				weightGross += elm.getWeightGross();
				weightCharge += elm.getWeightCharge();
				charge += elm.getAmount();
			}
			
			awb.setWeightGross(weightGross);
			awb.setWeightCharge(weightCharge);
			
			if(StringUtils.equals(awb.getChargeIndicator(), "P")){
				awb.setWeightPp(charge);
				awb.setWeightCc(0.0);
			}else {
				awb.setWeightPp(0.0);
				awb.setWeightCc(charge);
			}
			
			awb.setTotalPp(awb.getWeightPp() + awb.getAgentPp() + awb.getCarrierPp());
			awb.setTotalCc(awb.getWeightCc() + awb.getAgentCc() + awb.getCarrierCc());
			
			//Comm.
			awb.setCommPercent(DoubleUtil.convertAmount(filter.getCommPercent()));
			if(StringUtils.isNotBlank(awb.getAgtnGsa())){
				/*
				 * Comm percent gsa percent
				 * compercent = (MYC + HT)/comm
				 * */
				Double taxMyc = 0.0;
				for (AwbTax elmTax : awb.getAwbTax()) {
					if(StringUtils.equals(elmTax.getId().getTaxCode(), "MYC")){
						taxMyc = elmTax.getTaxAmount();
						break;
					}
				}
				awb.setComm(DoubleUtil.round((taxMyc + awb.getWeightPp() + awb.getWeightCc()) * awb.getCommPercent() / 100, 2));
			}else {
				awb.setComm(DoubleUtil.round(awb.getCommPercent() * (awb.getWeightPp() + awb.getWeightCc()) / 100, 2));
			}			
			
			//VAT
			awb.setVatPercent(DoubleUtil.convertDouble(filter.getVatPercent()));
			awb.setVat(DoubleUtil.round((awb.getTotalPp() + awb.getTotalCc()) * awb.getVatPercent() / 100, 2));
			
			awb.setNet(DoubleUtil.round(awb.getTotalPp() + awb.getTotalCc() + awb.getVat() - awb.getComm(), 2));
			
			//Delete detail
			awbService.deleteAwbDetail(awb.getLta());
			
			awbService.merge(awb);
			
			filter.init();
			return "redirect:/secure/awb/detail?id=" + id;
			
		}else if(StringUtils.equals(action, "TAX")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			Awb awb = awbService.find(id);		
			
			//Tax
			Set<AwbTax> taxCu = new HashSet<AwbTax>();
			for (AwbTax elm : awb.getAwbTax()) {
				taxCu.add(elm);
			}
			awb.getAwbTax().removeAll(taxCu);
			
			for(AwbTaxFilter elm : filter.getAwbTax()) {
				int retval =  DoubleUtil.convertDouble(elm.getAmount()).compareTo(0.0);
				 
				if(elm.getCode().length() == 3 && retval != 0){
					AwbTaxId awbTaxId = new AwbTaxId();
					awbTaxId.setLta(awb.getLta());
					awbTaxId.setTaxCode(elm.getCode().toUpperCase());
					
					AwbTax awbTax = new AwbTax();
					awbTax.setId(awbTaxId);					
					awbTax.setTaxAmount(DoubleUtil.round(DoubleUtil.convertDouble(elm.getAmount()), 2));
					awbTax.setCreatedBy(awb.getCreatedBy());
					awbTax.setCreatedDate(awb.getCreatedDate());
					awbTax.setModifiedBy(sysUser.getUsername());
					awbTax.setModifiedDate(new Date());
					
					awbTax.setAwb(awb);
					awb.getAwbTax().add(awbTax);
				}
			}
			
			//TOTAL
			Double agentCharge = 0.0;
			Double carrierCharge = 0.0;
			
			for (AwbTax elm : awb.getAwbTax()) {
				if(StringUtils.equals(StringUtils.substring(elm.getId().getTaxCode(), 2), "A")){
					agentCharge += elm.getTaxAmount();
				}else{
					carrierCharge += elm.getTaxAmount();
				}
			}
			
			if(StringUtils.equals(awb.getOtherIndicator(), "P")){
				awb.setAgentPp(agentCharge);
				awb.setCarrierPp(carrierCharge);
				awb.setAgentCc(0.0);
				awb.setCarrierCc(0.0);
			}else {
				awb.setAgentPp(0.0);
				awb.setCarrierPp(0.0);
				awb.setAgentCc(agentCharge);
				awb.setCarrierCc(carrierCharge);
			}
			
			awb.setTotalPp(awb.getWeightPp() + awb.getAgentPp() + awb.getCarrierPp());
			awb.setTotalCc(awb.getWeightCc() + awb.getAgentCc() + awb.getCarrierCc());
			
			//Comm.
			if(StringUtils.isNotBlank(awb.getAgtnGsa())){
				/*
				 * Comm percent gsa percent
				 * com = (MYC + HT)/commPercent
				 * */
				Double taxMyc = 0.0;
				for (AwbTax elmTax : awb.getAwbTax()) {
					if(StringUtils.equals(elmTax.getId().getTaxCode(), "MYC")){
						taxMyc = elmTax.getTaxAmount();
						break;
					}
				}
				awb.setComm(DoubleUtil.round((taxMyc + awb.getWeightPp() + awb.getWeightCc()) * awb.getCommPercent() / 100, 2));
			}	
			
			//VAT
			awb.setVat(DoubleUtil.round((awb.getTotalPp() + awb.getTotalCc()) * awb.getVatPercent() / 100, 2));
			
			awb.setNet(DoubleUtil.round(awb.getTotalPp() + awb.getTotalCc() + awb.getVat() - awb.getComm(), 2));
			
			//Delete tax
			awbService.deleteAwbTax(awb.getLta());
			
			awbService.merge(awb);
			
			filter.init();
			return "redirect:/secure/awb/detail?id="+id;
		}
		return "redirect:/secure/awb";
	}
}
