package com.datawings.app.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.datawings.app.common.BeanUtil;
import com.datawings.app.common.DateUtil;
import com.datawings.app.common.DoubleUtil;
import com.datawings.app.common.IntegerUtil;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.ManifestBean;
import com.datawings.app.filter.ManifestFilter;
import com.datawings.app.model.Awb;
import com.datawings.app.model.AwbDetail;
import com.datawings.app.model.AwbFlown;
import com.datawings.app.model.AwbFlownDetail;
import com.datawings.app.model.AwbFlownTax;
import com.datawings.app.model.AwbTax;
import com.datawings.app.model.Manifest;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.process.ManifestManager;
import com.datawings.app.service.IAwbFlownService;
import com.datawings.app.service.IAwbService;
import com.datawings.app.service.IManifestService;
import com.datawings.app.service.ISysUserService;
import com.datawings.app.validator.ManifestValidator;
import com.datawings.app.validator.ValidationError;

@Controller
@SessionAttributes({"manifestFilter", "awbAddFilter"})
@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_MANIFEST', 'ROLE_MANIFEST_CREATION')")
public class ManifestController {

	@Autowired 
	private MessageSource messageSource;
	
	@Autowired
	private ISysUserService sysUserService;
	
	@Autowired
	private IManifestService manifestService;
	
	@Autowired
	private ManifestValidator manifestValidator;
	
	@Autowired
	private IAwbService awbService;
	
	@Autowired
	private IAwbFlownService awbFlownService;
	
	@Autowired
	private ManifestManager manifestManager;
	
	@ModelAttribute("manifestFilter")  
    public ManifestFilter getInitCreate() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		ManifestFilter filter = new ManifestFilter();
		filter.setTacn(sysUser.getAirlineCode());
        return filter;  
    } 
	
	@ModelAttribute("awbAddFilter")  
    public AwbFilter getInitDetail() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		AwbFilter filter = new AwbFilter();
		filter.setTacn(sysUser.getAirlineCode());
		
        return filter;  
    } 
	
	@RequestMapping(value = "/secure/manifest", method = RequestMethod.GET)
	public String getManifest(@ModelAttribute("manifestFilter") ManifestFilter filter, Model model) {
		Integer rowCount = manifestService.getRowsCount(filter);
		filter.setRowCount(rowCount);
		
		return "manifest";
	}

	@RequestMapping(value = "/secure/manifest/loadPage", method = RequestMethod.GET)
	public String getManifestLoadPage(@ModelAttribute("manifestFilter") ManifestFilter filter, Model model) {
		List<ManifestBean> listManifest = manifestService.getManifest(filter);
		model.addAttribute("listManifest", listManifest);
		
		return "manifestLoadPage";
	}
	
	@RequestMapping(value = "/secure/manifest", method = RequestMethod.POST)
	public String postDashboard(@ModelAttribute("manifestFilter") ManifestFilter filter,
			BindingResult result, Model model, HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
	
		if(StringUtils.equals(action, "ADD")){
			Manifest maniFest = new Manifest();
			maniFest.setTacn(filter.getTacn());
			maniFest.setManifestNo(filter.getManifestNo().toUpperCase());
			maniFest.setOrig(filter.getOrig().toUpperCase());
			maniFest.setDest(filter.getDest().toUpperCase());
			maniFest.setFlightNumber(filter.getCarrier().toUpperCase() + filter.getFlightNumber().toUpperCase());
			maniFest.setFlightDate(DateUtil.string2Date(filter.getFlightDate(), "dd/MM/yyyy"));
			if(StringUtils.isNotBlank(filter.getImmat())){
				maniFest.setImmat(filter.getImmat().toUpperCase().trim());
			}
			if(StringUtils.isNotBlank(filter.getEntryNumber())){
				maniFest.setEntryNumber(IntegerUtil.convertInteger(filter.getEntryNumber()));
			}
			maniFest.setCreatedDate(new Date());
			maniFest.setCreatedBy(sysUser.getUsername());
			
			manifestService.save(maniFest);
			
			filter.init();
			filter.setTacn(sysUser.getAirlineCode());
			
			return "redirect:/secure/manifest/add?id="+maniFest.getManifestNo();
			
		}else if(StringUtils.equals(action, "FLOWN")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			request.getSession().setAttribute("SES_FILTER", "FILTER");
			return "redirect:/secure/manifest/add?id="+id;
		}else if(StringUtils.equals(action, "DELETE")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			//Manifest manifest = manifestService.find(id);
			
			manifestService.deleteById(id);
			
		}else if(StringUtils.equals(action, "GO")){
			return "redirect:/secure/manifest";
		}else if(StringUtils.equals(action, "RESET")){
			filter.init();
		}
		
		return "redirect:/secure/manifest";
	}
	
	@RequestMapping(value = "/secure/manifest/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorAdd(@ModelAttribute("manifestFilter") ManifestFilter filter, BindingResult result, Model model, Locale locale){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = manifestValidator.checkManifest(filter, result);
		if(errCode > 0){
			rs.put("errCode", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for(int i = 0;i <result.getAllErrors().size();i++){
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null, 
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}
	
	@RequestMapping(value = "/secure/manifest/add", method = RequestMethod.GET)
	public String getManifestAdd(@ModelAttribute("awbAddFilter") AwbFilter filter, Model model, HttpServletRequest request, HttpServletResponse response) {
	
		String id = ServletRequestUtils.getStringParameter(request, "id", "");
		ManifestBean maniFest = manifestService.findReturnBean(id);
		
		String check = (String) request.getSession().getAttribute("SES_FILTER");
		if(StringUtils.isNotBlank(check)){
			filter.setOrig(maniFest.getOrig());
			filter.setDest(maniFest.getDest());
			request.getSession().setAttribute("SES_FILTER", "");
		}
		
		List<Awb> listAwb = awbService.getListAwbManifest(filter);
		List<AwbFlown> listAwbFlown = awbFlownService.getListAwbManifest(filter, maniFest.getManifestNo());
		
		model.addAttribute("maniFest", maniFest);
		model.addAttribute("listAwb", listAwb);
		model.addAttribute("listAwbFlown", listAwbFlown);
		
		return "manifestAdd";
	}
	
	@RequestMapping(value = "/secure/manifest/add", method = RequestMethod.POST)
	public String postManifestAdd(@ModelAttribute("awbAddFilter") AwbFilter filter,BindingResult result, Model model, 
			HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		
		if(StringUtils.equals(action, "RESET")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			
			filter.setOrig("");
			filter.setDest("");
			filter.setDateExecute("");
			filter.setLta("");
			return "redirect:/secure/manifest/add?id=" + id;
			
		}else if(StringUtils.equals(action, "GO")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			return "redirect:/secure/manifest/add?id=" + id;
			
		}else if(StringUtils.equals(action, "ADD")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			String[] codeAdd = ServletRequestUtils.getStringParameters(request, "ltaChekAdd");
			Manifest manifest = manifestService.find(id);
			if(codeAdd != null){
				manifestManager.flownGeneral(manifest, codeAdd, sysUser);
			}
			return "redirect:/secure/manifest/add?id=" + id;
			
		}else if(StringUtils.equals(action, "ADD_MODAL_AWB_KM")){
			Manifest manifest = manifestService.find(filter.getManifestNo());
			Awb awb = awbService.find(filter.getLtaAdd());	
			manifestManager.flownSplitAwbLeg(manifest, awb, filter, sysUser);
			filter.init();
			
			return "redirect:/secure/manifest/add?id=" + manifest.getManifestNo();
			
		}else if(StringUtils.equals(action, "ADD_MODAL_FLOWN_KM")){
			Manifest manifest = manifestService.find(filter.getManifestNo());
			AwbFlown awbFlown = awbFlownService.find(filter.getId());
			manifestManager.flownSplitFlownLeg(manifest, awbFlown, filter, sysUser);
			filter.init();
			
			return "redirect:/secure/manifest/add?id=" + manifest.getManifestNo();
			
		}else if(StringUtils.equals(action, "ADD_MODAL_AWB_KG")){
			Manifest manifest = manifestService.find(filter.getManifestNo());
			Awb awb = awbService.find(filter.getLtaAdd());
			manifestManager.flownSplitAwbWeight(manifest, awb, filter, sysUser);
			filter.init();
			
			return "redirect:/secure/manifest/add?id=" + manifest.getManifestNo();
			
		}else if(StringUtils.equals(action, "ADD_MODAL_FLOWN_KG")){
			Manifest manifest = manifestService.find(filter.getManifestNo());
			AwbFlown awbFlown = awbFlownService.find(filter.getId());
			manifestManager.flownSplitFlownWeight(manifest, awbFlown, filter, sysUser);
			filter.init();
			
			return "redirect:/secure/manifest/add?id=" + manifest.getManifestNo();
		}
		return "redirect:/secure/manifest";
	}
		
	@RequestMapping(value = "/secure/manifest/awb/km", method = RequestMethod.GET)
	public String getAwbKm(Model model, HttpServletRequest request) {
		
		String manifestNo = request.getParameter("manifest");
		Manifest manifest = manifestService.find(manifestNo);
		
		Double km = awbService.getKm(manifest.getOrig(), manifest.getDest());
		
		String type = request.getParameter("type");
		String id = request.getParameter("id");
		model.addAttribute("km", km);
		model.addAttribute("manifest", manifest);
		
		if(StringUtils.equals(type, "AWB")){
			Awb awb = awbService.find(id);
			
			List<AwbDetail> setAwbDetail = new ArrayList<AwbDetail>();
			
			for(AwbDetail item: awb.getAwbDetail()){
				AwbDetail elm = new AwbDetail();
				BeanUtil.copyProperties(elm, item);
				elm.setAmount(DoubleUtil.round(km * item.getAmount() / awb.getKm(), 2));
				setAwbDetail.add(elm);
			}
			
			Integer sizeTax = awb.getAwbTax().size();
			List<AwbTax> setTax = new ArrayList<AwbTax>();
			
			for(AwbTax item: awb.getAwbTax()){
				AwbTax elm = new AwbTax();
				BeanUtil.copyProperties(elm, item);
				elm.setTaxAmount(DoubleUtil.round(km * item.getTaxAmount() / awb.getKm(), 2));
				setTax.add(elm);
			}
			
			for (int i = 0; i < CargoUtils.NB_TAX - sizeTax; i++) {
				setTax.add(new AwbTax());
			}
			
			model.addAttribute("setAwbDetail", setAwbDetail);
			model.addAttribute("setTax", setTax);
			model.addAttribute("awb", awb);
			
			return "modalAwbKm";
		}else {
			AwbFlown awbFlown = awbFlownService.find(Integer.parseInt(id));
			Awb awb = awbService.find(awbFlown.getLta());
			List<AwbFlown> awbFlowns = awbFlownService.getByLta(awbFlown.getLta());
			List<AwbFlownDetail> setAwbFlownDetail = new ArrayList<AwbFlownDetail>();
			
			for(AwbFlownDetail item: awbFlown.getAwbFlownDetail()){
				AwbFlownDetail elm = new AwbFlownDetail();
				BeanUtil.copyProperties(elm, item);
				elm.setAmount(DoubleUtil.round(km * item.getAmount() / awbFlown.getKm(), 2));
				setAwbFlownDetail.add(elm);
			}
			
			Integer sizeTax = awbFlown.getAwbFlownTax().size();
			List<AwbFlownTax> setTax = new ArrayList<AwbFlownTax>();
			
			for(AwbFlownTax item: awbFlown.getAwbFlownTax()){
				AwbFlownTax elm = new AwbFlownTax();
				BeanUtil.copyProperties(elm, item);
				elm.setTaxAmount(DoubleUtil.round(km * item.getTaxAmount() / awbFlown.getKm(), 2));
				setTax.add(elm);
			}
			
			for (int i = 0; i < CargoUtils.NB_TAX - sizeTax; i++) {
				setTax.add(new AwbFlownTax());
			}
			
			model.addAttribute("setAwbFlownDetail", setAwbFlownDetail);
			model.addAttribute("setTax", setTax);
			model.addAttribute("awbFlown", awbFlown);
			model.addAttribute("awb", awb);
			model.addAttribute("awbFlowns", awbFlowns);
			
			return "modalFlownKm";
		}
	}
	
	@RequestMapping(value = "/secure/manifest/awb/kg", method = RequestMethod.GET)
	public String getAwbKg(Model model, HttpServletRequest request) {
		
		String manifestNo = request.getParameter("manifest");
		Manifest manifest = manifestService.find(manifestNo);
		
		String type = request.getParameter("type");
		String id = request.getParameter("id");
		model.addAttribute("manifest", manifest);
		
		if(StringUtils.equals(type, "AWB")){
			Awb awb = awbService.find(id);
			
			Integer sizeTax = awb.getAwbTax().size();
			List<AwbTax> setTax = new ArrayList<AwbTax>();
			
			for(AwbTax item: awb.getAwbTax()){
				AwbTax elm = new AwbTax();
				BeanUtil.copyProperties(elm, item);
				setTax.add(elm);
			}
			
			for (int i = 0; i < CargoUtils.NB_TAX - sizeTax; i++) {
				setTax.add(new AwbTax());
			}
			
			model.addAttribute("setTax", setTax);
			model.addAttribute("awb", awb);
			
			return "modalAwbKg";
		}else {
			AwbFlown awbFlown = awbFlownService.find(Integer.parseInt(id));
			Awb awb = awbService.find(awbFlown.getLta());
			List<AwbFlown> awbFlowns = awbFlownService.getByLta(awbFlown.getLta());
			Integer sizeTax = awbFlown.getAwbFlownTax().size();
			List<AwbFlownTax> setTax = new ArrayList<AwbFlownTax>();
			
			for(AwbFlownTax item: awbFlown.getAwbFlownTax()){
				AwbFlownTax elm = new AwbFlownTax();
				BeanUtil.copyProperties(elm, item);
				setTax.add(elm);
			}
			
			for (int i = 0; i < CargoUtils.NB_TAX - sizeTax; i++) {
				setTax.add(new AwbFlownTax());
			}
			
			model.addAttribute("setTax", setTax);
			model.addAttribute("awbFlown", awbFlown);
			model.addAttribute("awbFlowns", awbFlowns);
			model.addAttribute("awb", awb);
			
			return "modalFlownKg";
		} 
	}
	
}
