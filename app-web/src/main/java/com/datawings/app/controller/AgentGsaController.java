package com.datawings.app.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.AgentGsaFilter;
import com.datawings.app.model.Agent;
import com.datawings.app.model.AgentGsa;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.service.IAgentGsaService;
import com.datawings.app.service.IAgentService;
import com.datawings.app.validator.AgentGsaValidator;
import com.datawings.app.validator.ValidationError;

@Controller
@SessionAttributes({ "agentGsaFilter" })
@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_SETTING', 'ROLE_SETTING_AGENT_GSA')")
public class AgentGsaController {

	@Autowired
	private IAgentGsaService agentGsaService;

	@Autowired
	private IAgentService agentService;
	
	@Autowired
	private MessageSource messageSource;

	@Autowired
	private AgentGsaValidator agentGsaValidator;

	@ModelAttribute("agentGsaFilter")
	public AgentGsaFilter agentGsaFilter() {
		AgentGsaFilter filter = new AgentGsaFilter();
		filter.setPageSize(10);
		return filter;
	}

	@RequestMapping(value = "/secure/agentGsa", method = RequestMethod.GET)
	public String getAgentGsa(@ModelAttribute("agentGsaFilter") AgentGsaFilter filter, Model model) {
		Integer rowCount = agentGsaService.getAgentGsaRowCount(filter);
		filter.setRowCount(rowCount);
		return "agentGsa";
	}

	@RequestMapping(value = "/secure/agentGsa/loadpage", method = RequestMethod.GET)
	public String getAgentGsaLoadPage(@ModelAttribute("agentGsaFilter") AgentGsaFilter filter, Integer pageNo,
			Model model) {
		filter.setPage(pageNo);
		List<AgentGsa> lstAgentGsa = agentGsaService.getListAgentGsa(filter);

		for (AgentGsa elm : lstAgentGsa) {
			elm.setNumberAgtn(agentGsaService.getCountAgtn(elm.getCode().trim()));
		}
		
		model.addAttribute("row", (filter.getPage()) * filter.getPageSize());
		model.addAttribute("lstAgentGsa", lstAgentGsa);
		return "agentGsaLoadPage";
	}

	@RequestMapping(value = "/secure/agentGsa", method = RequestMethod.POST)
	public String postAgentGsa(@ModelAttribute("agentGsaFilter") AgentGsaFilter filter, Model model,
			HttpServletRequest request) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if (StringUtils.equals(action, "GO")) {
			filter.setPage(0);
			return "redirect:/secure/agentGsa";
		}
		if (StringUtils.equals(action, "RESET")) {
			filter.init();
		}
		if (StringUtils.equals(action, "VIEW")) {
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			return "redirect:/secure/agentGsa/detail?code=" + id;
		}
		if(StringUtils.equals(action, "DELETE")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			agentGsaService.deleteAgentGsaByCode(id.trim());
		}
		return "redirect:/secure/agentGsa";
	}

	@RequestMapping(value = "/secure/agentGsa/create/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorCreate(@ModelAttribute("agentGsaFilter") AgentGsaFilter filter,
			BindingResult result, Model model, Locale locale) {
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = agentGsaValidator.doCreateAgentGsaValidator(filter, result);
		if (errCode > 0) {
			rs.put("errCodeCreate", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for (int i = 0; i < result.getAllErrors().size(); i++) {
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}

	@RequestMapping(value = "/secure/agentGsa/create", method = RequestMethod.POST)
	public String doCreate(@ModelAttribute("agentGsaFilter") AgentGsaFilter filter, HttpServletRequest request,
			Model model) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if (StringUtils.equals(action, "CREATE")) {
			AgentGsa newAgentGsa = new AgentGsa();
			SysUser userAuth = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			newAgentGsa.setCode(filter.getCodeAdd());
			newAgentGsa.setGsaName(filter.getNameAdd());
			newAgentGsa.setComm(filter.getCommAdd());
			newAgentGsa.setDateStart(DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"));
			if (StringUtils.isNotBlank(filter.getDateEndAdd()) && DateUtil.checkDateAsString(filter.getDateEndAdd(), "dd/MM/yyyy")) {
				newAgentGsa.setDateEnd(DateUtil.string2Date(filter.getDateEndAdd(), "dd/MM/yyyy"));
			} else
				newAgentGsa.setDateEnd(DateUtil.string2Date(CargoUtils.MAX_DATE, "dd/MM/yyyy"));
			newAgentGsa.setCreatedBy(userAuth.getUsername());
			newAgentGsa.setCreatedDate(new Date());

			agentGsaService.save(newAgentGsa);

			filter.init();
		}
		return "redirect:/secure/agentGsa";
	}

	@RequestMapping(value = "/secure/agentGsa/detail")
	public String showDetailGsa(@ModelAttribute("agentGsaFilter") AgentGsaFilter filter, Model model,
			HttpServletRequest request) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if (StringUtils.equals(action, "BACK")) {
			return "redirect:/secure/agentGsa";
		} 
		if(StringUtils.equals(action, "DELETE")){
			String id = ServletRequestUtils.getStringParameter(request, "id", "");
			String codeAgentGsa = ServletRequestUtils.getStringParameter(request, "codeAgentGsa", "");
			agentGsaService.deleteAgtnByAgtnCode(id.trim(), codeAgentGsa.trim());
			return "redirect:/secure/agentGsa/detail?code=" + codeAgentGsa;
		}
		else {
			String code = ServletRequestUtils.getStringParameter(request, "code", "");
			AgentGsa agentGsa = agentGsaService.getAgentGsaByCode(code);
			List<AgentGsa> listAgentGsaDetail = agentGsaService.getDetailAgentGsaByCode(code);
			
			model.addAttribute("agentGsa", agentGsa);
			model.addAttribute("listAgentGsaDetail", listAgentGsaDetail);
			model.addAttribute("code", code);
		}
		return "agentGsaDetail";
	}

	@RequestMapping(value = "/secure/agentGsa/ajaxAgentGsa.json", method = RequestMethod.GET)
	@ResponseBody
	public List<String> getListAgentCode(String agtnAddDetail, HttpServletRequest request,
			HttpServletResponse response) {
		List<String> lstAgentCode = agentService.findListAgentCode(agtnAddDetail);
		return lstAgentCode;
	}

	@RequestMapping(value = "/secure/agentGsa/{code}/adddetail/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorAddEdit(@ModelAttribute("agentGsaFilter") AgentGsaFilter filter,
			@PathVariable String code, BindingResult result, Model model, Locale locale) {
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = agentGsaValidator.doValidatorAddDetail(filter, result);
		if (errCode > 0) {
			rs.put("errCodeEdit", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for (int i = 0; i < result.getAllErrors().size(); i++) {
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}

	@RequestMapping(value = "/secure/agentGsa/{code}/adddetail", method = RequestMethod.POST)
	public String doAddDetail(@ModelAttribute("agentGsaFilter") AgentGsaFilter filter, @PathVariable String code,
			HttpServletRequest request, Model model) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if (StringUtils.equals(action, "ADD")) {
			String agentgsa_comm = ServletRequestUtils.getStringParameter(request, "agentgsa_comm", "");
			String agentgsa_dateStart = ServletRequestUtils.getStringParameter(request, "agentgsa_dateStart", "");
			String agentgsa_dateEnd = ServletRequestUtils.getStringParameter(request, "agentgsa_dateEnd", "");
			SysUser authUser = (SysUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			AgentGsa agentGsa = new AgentGsa();
			Agent agent = agentService.find(filter.getAgtnAddDetail());
			agentGsa.setCode(code);
			if(agent != null)
				agentGsa.setGsaName(agent.getName());
			agentGsa.setAgtn(filter.getAgtnAddDetail());
			agentGsa.setComm(Double.parseDouble(agentgsa_comm));
			agentGsa.setDateStart(DateUtil.string2Date(agentgsa_dateStart, "dd/MM/yyyy"));
			agentGsa.setDateEnd(DateUtil.string2Date(agentgsa_dateEnd, "dd/MM/yyyy"));
			agentGsa.setModifiedBy(authUser.getUsername());
			agentGsa.setModifiedDate(new Date());

			agentGsaService.save(agentGsa);
			filter.init();
		}

		return "redirect:/secure/agentGsa/detail?code=" + code;
	}

}
