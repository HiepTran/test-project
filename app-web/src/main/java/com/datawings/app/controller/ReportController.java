package com.datawings.app.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.datawings.app.common.BatchUtil;
import com.datawings.app.common.DateUtil;
import com.datawings.app.common.SystemUtil;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.model.CritIsoc;
import com.datawings.app.model.Report;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.service.ICritIsocService;
import com.datawings.app.service.IReportService;

@Controller
@SessionAttributes({"reportFilter"})
public class ReportController {

	@Autowired
	private IReportService reportService;
	
	@Autowired
	private ICritIsocService critIsocService;
	
	@Autowired
	private MessageSource messageSource;
	
	@ModelAttribute("reportFilter")  
    public ReportFilter getInitCreate() {  
		ReportFilter filter = new ReportFilter();
        return filter;  
    } 
	
	@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_REPORTS', 'ROLE_REPORT_LTA', 'ROLE_REPORT_MANIFEST')")
	@RequestMapping(value = "/secure/report", method = RequestMethod.GET)
	public String getAwb(@ModelAttribute("reportFilter") ReportFilter filter, Model model, HttpServletRequest request,
			HttpServletResponse response) {

		return "report";
	}
	
	@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_REPORTS', 'ROLE_REPORT_LTA', 'ROLE_REPORT_MANIFEST')")
	@RequestMapping(value = "/secure/report", method = RequestMethod.POST)
	public String postAgent(@ModelAttribute("reportFilter") ReportFilter filter, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		if(StringUtils.equals(action, "GO")){
			String type = ServletRequestUtils.getStringParameter(request, "type", "");
			
			if(StringUtils.equals(type, "CASS_SALES")){
				StringBuffer filterReport = new StringBuffer();
				filterReport.append(CargoUtils.COUNTRY + ":");
				if(StringUtils.isNotBlank(filter.getIsoc())){
					filterReport.append(filter.getIsoc() + ";");
				}else {
					filterReport.append(CargoUtils.TOUT + ";");
				}
				
				filterReport.append(CargoUtils.AGENT + ":");
				if(StringUtils.isNotBlank(filter.getAgtn())){
					filterReport.append(filter.getAgtn() + ";");
				}else {
					filterReport.append(CargoUtils.TOUT + ";");
				}
				
				filterReport.append(CargoUtils.DATE_FROM + ":");
				if(StringUtils.isNotBlank(filter.getDateFrom())){
					filterReport.append(filter.getDateFrom() + ";");
				}else {
					filterReport.append(CargoUtils.TOUT + ";");
				}
				
				filterReport.append(CargoUtils.DATE_TO + ":");
				if(StringUtils.isNotBlank(filter.getDateTo())){
					filterReport.append(filter.getDateTo() + ";");
				}else {
					filterReport.append(CargoUtils.TOUT + ";");
				}
				
				filterReport.append(CargoUtils.CHANNEL + ":");
				if(StringUtils.isNotBlank(filter.getChannel())){
					filterReport.append(filter.getChannel() + ";");
				}else {
					filterReport.append(CargoUtils.TOUT + ";");
				}
				
				Report report = new Report();
				report.setName(CargoUtils.CASS_SALES);
				report.setLocal(getLocal());
				report.setFileName("Sales_CASS_" + DateUtil.date2String(new Date(), "yyyyMMdd.HHmmss") + ".xls");
				report.setFilter(filterReport.toString());
				report.setStatus("W");
				report.setType(CargoUtils.SALE);
				report.setCreatedBy(sysUser.getUsername());
				report.setCreatedDate(new Date());
				Integer id = (Integer) reportService.save(report);
				
				String exec = "java -d64 -Xms512M -jar " + getPath() + " REPORT " + id ;
				BatchUtil.run(exec);
				
				filter.init();
			} else if(StringUtils.equals(type, "RMK")){
				//Filter
				StringBuffer filterReport = new StringBuffer();
				filterReport.append(CargoUtils.MONTH + ":");
				if(StringUtils.isNotBlank(filter.getDateFrom())){
					filterReport.append(filter.getDateFrom() + ";");
				}else {
					filterReport.append(CargoUtils.TOUT + ";");
				}
				
				Report report = new Report();
				report.setName(CargoUtils.RMK);
				report.setLocal(getLocal());
				report.setFileName("RMK_" + DateUtil.date2String(new Date(), "yyyyMMdd.HHmmss") + ".xls");
				report.setFilter(filterReport.toString());
				report.setStatus("W");
				report.setType(CargoUtils.FLOWN);
				report.setCreatedBy(sysUser.getUsername());
				
				report.setCreatedDate(new Date());
				Integer id = (Integer) reportService.save(report);
				
				String exec = "java -d64 -Xms512M -jar " + getPath() + " REPORT " + id ;
				BatchUtil.run(exec);
				
			}else if(StringUtils.equals(type, "CA_TRANSPORT")){
				StringBuffer filterReport = new StringBuffer();
				filterReport.append(CargoUtils.DATE_FROM + ":");
				if(StringUtils.isNotBlank(filter.getDateFrom())){
					filterReport.append(filter.getDateFrom() + ";");
				}else {
					filterReport.append(CargoUtils.TOUT + ";");
				}
				
				filterReport.append(CargoUtils.DATE_TO + ":");
				if(StringUtils.isNotBlank(filter.getDateTo())){
					filterReport.append(filter.getDateTo() + ";");
				}else {
					filterReport.append(CargoUtils.TOUT + ";");
				}
				
				filterReport.append(CargoUtils.FLIGHT_NUMBER + ":");
				if(StringUtils.isNotBlank(filter.getFlightNumber())){
					filterReport.append(filter.getFlightNumber() + ";");
				}else {
					filterReport.append(CargoUtils.TOUT + ";");
				}
				
				filterReport.append(CargoUtils.IMMAT + ":");
				if(StringUtils.isNotBlank(filter.getImmat())){
					filterReport.append(filter.getImmat() + ";");
				}else {
					filterReport.append(CargoUtils.TOUT + ";");
				}
				
				filterReport.append(CargoUtils.ORIG + ":");
				if(StringUtils.isNotBlank(filter.getOrig())){
					filterReport.append(filter.getOrig() + ";");
				}else {
					filterReport.append(CargoUtils.TOUT + ";");
				}
				
				filterReport.append(CargoUtils.DEST + ":");
				if(StringUtils.isNotBlank(filter.getDest())){
					filterReport.append(filter.getDest() + ";");
				}else {
					filterReport.append(CargoUtils.TOUT + ";");
				}
				
				Report report = new Report();
				report.setName(CargoUtils.CA_TRANSPORT);
				report.setLocal(getLocal());
				report.setFileName("CA_TRANSPORT_PAR_VOL_" + DateUtil.date2String(new Date(), "yyyyMMdd.HHmmss") + ".xls");
				report.setFilter(filterReport.toString());
				report.setStatus("W");
				report.setType(CargoUtils.FLOWN);
				report.setCreatedBy(sysUser.getUsername());
				report.setCreatedDate(new Date());
				Integer id = (Integer) reportService.save(report);
				
				String exec = "java -d64 -Xms512M -jar " + getPath() + " REPORT " + id ;
				BatchUtil.run(exec);
				
			}
		}
		
		return "redirect:/secure/reportResult";
	}
	
	@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_REPORTS', 'ROLE_REPORT_LTA', 'ROLE_REPORT_MANIFEST')")
	@RequestMapping(value = "/secure/reportResult", method = RequestMethod.GET)
	public String getResult(@ModelAttribute("reportFilter") ReportFilter filter, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		List<Report> reports = reportService.getReports(filter);
		model.addAttribute("reports", reports);
		
		return "reportResult";
	}
	
	@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_REPORTS', 'ROLE_REPORT_LTA', 'ROLE_REPORT_MANIFEST')")
	@RequestMapping(value = "/secure/reportResult", method = RequestMethod.POST)
	public String postResult(@ModelAttribute("reportFilter") ReportFilter filter, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if(StringUtils.equals(action, "DELETE")){
			Integer id = ServletRequestUtils.getIntParameter(request, "id", -1);
			Report report = reportService.find(id);
			File file = new File(report.getLocal() +  report.getFileName());
			try {
				file.delete();
			} catch (Exception e) {
				System.out.println("ERROR DELETE FILE");
			}
			reportService.deleteById(id);
		}
		return "redirect:/secure/reportResult";
	}
	
	@SuppressWarnings("resource")
	@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_REPORTS', 'ROLE_REPORT_LTA', 'ROLE_REPORT_MANIFEST')")
	@RequestMapping(value = "/secure/reportResult/excel", method = RequestMethod.GET)
	public void exportExcel(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String fileName = ServletRequestUtils.getStringParameter(request, "fileName", "");
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		try {
			FileInputStream fin = new FileInputStream(getLocal() + fileName);
			for (int readNum; (readNum = fin.read(buf)) != -1;) {
				baos.write(buf, 0, readNum);
			}
			response.setContentType("application/xls");
			response.setHeader("Expires", "0");
			response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			response.setHeader("Pragma", "public");
			response.setHeader("Content-Disposition", "attachment; filename=\""+ fileName + "\";");
			response.setContentLength(baos.size());
			ServletOutputStream out = response.getOutputStream();
			baos.writeTo(out);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_REPORTS', 'ROLE_REPORT_LTA')")
	@RequestMapping(value = "/secure/report/cassSales", method = RequestMethod.GET)
	public String getReportCassSales(@ModelAttribute("reportFilter") ReportFilter filter, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		filter.init();
		List<CritIsoc> listIsoc = critIsocService.getAll();
		model.addAttribute("listIsoc", listIsoc);
		return "reportCassSales";
	}
	
	@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_REPORTS', 'ROLE_REPORT_MANIFEST')")
	@RequestMapping(value = "/secure/report/rmk", method = RequestMethod.GET)
	public String getReportRmk(@ModelAttribute("reportFilter") ReportFilter filter, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		filter.init();
		filter.setDateFrom(DateUtil.date2String(new Date(), "MM/yyyy"));
		return "reportRmk";
	}
	
	@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_REPORTS', 'ROLE_REPORT_MANIFEST')")
	@RequestMapping(value = "/secure/report/caTransport", method = RequestMethod.GET)
	public String getReportCaTransport(@ModelAttribute("reportFilter") ReportFilter filter, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		filter.init();
		return "reportCaTransport";
	}
	
	private String getPath(){
		String path = "";		
		String pathWindows = messageSource.getMessage("path.windows.bin", null, "", Locale.ENGLISH);
		String pathLinux = messageSource.getMessage("path.linux.bin", null, "", Locale.ENGLISH);
		if (SystemUtil.isWindowsOS()) {
			path = pathWindows;
		} else {
			path = pathLinux;
		}	
		return path;
	}
	
	private String getLocal(){
		String path = "";		
		String pathWindows = messageSource.getMessage("path.windows", null, "", Locale.ENGLISH);
		String pathLinux = messageSource.getMessage("path.linux", null, "", Locale.ENGLISH);
		if (SystemUtil.isWindowsOS()) {
			path = pathWindows;
		} else {
			path = pathLinux;
		}	
		return path;
	}
}
