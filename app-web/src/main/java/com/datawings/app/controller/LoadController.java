package com.datawings.app.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.LocaleResolver;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.DoubleUtil;
import com.datawings.app.filter.AwbGSAMapping;
import com.datawings.app.filter.FileFilter;
import com.datawings.app.loader.AwbGsaLoader;
import com.datawings.app.model.Awb;
import com.datawings.app.model.AwbDetail;
import com.datawings.app.model.AwbTax;
import com.datawings.app.model.CritIsoc;
import com.datawings.app.model.LoadLog;
import com.datawings.app.model.Rdv;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.AwbUtil;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.service.IAwbService;
import com.datawings.app.service.ICritIsocService;
import com.datawings.app.service.ILoadLogService;
import com.datawings.app.validator.LoaderValidator;
import com.datawings.app.validator.ValidationError;

import jxl.read.biff.BiffException;

@Controller
@SessionAttributes({ "fileFilter" })
@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_AWB_LOAD')")
public class LoadController {
	@Autowired
	private ILoadLogService loadLogService;

	@Autowired
	private IAwbService awbService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private LocaleResolver localeResolver;

	@Autowired
	private AwbUtil awbUtil;

	@Autowired
	private ICritIsocService critIsocService;

	@Autowired
	private LoaderValidator loaderValidator;

	@ModelAttribute("fileFilter")
	public FileFilter getInit() {
		FileFilter filter = new FileFilter();
		return filter;
	}

	@RequestMapping(value = "/secure/awb/load", method = RequestMethod.GET)
	public String getAwbLoad(@ModelAttribute("fileFilter") FileFilter filter, Model model, HttpServletRequest request) {
		Integer rowCount = loadLogService.getLoadLogRowCount(filter);
		filter.setRowCount(rowCount);
		List<CritIsoc> critIsoc = critIsocService.getAll();

		model.addAttribute("critIsoc", critIsoc);

		return "loadExcel";
	}

	@RequestMapping(value = "/secure/awb/load/loadpage", method = RequestMethod.GET)
	public String getAwbLoadPage(@ModelAttribute("fileFilter") FileFilter filter, Integer pageNo, Model model) {
		filter.setPage(pageNo);
		List<LoadLog> logs = loadLogService.getAllLoadLog(filter);
		
		model.addAttribute("logs", logs);
		model.addAttribute("row", (filter.getPage()) * filter.getPageSize());
		return "loadExcelLoadPage";
	}

	@RequestMapping(value = "/secure/awb/load/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorChanger(@ModelAttribute("fileFilter") FileFilter filter, HttpServletRequest request,
			BindingResult result, Model model, Locale locale) {
		HashMap<String, Object> rs = new HashMap<String, Object>();
		String fileName = ServletRequestUtils.getStringParameter(request, "fileName", "");
		String fileSize = ServletRequestUtils.getStringParameter(request, "fileSize", "");
		int errCode = 0;
		errCode = loaderValidator.doValidatorChanger(filter, result, fileName, fileSize);
		if (errCode > 0) {
			rs.put("errCode", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for (int i = 0; i < result.getAllErrors().size(); i++) {
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}

	@RequestMapping(value = "/secure/awb/load", method = RequestMethod.POST)
	public String postAwbLoad(@ModelAttribute("fileFilter") FileFilter filter, Model model, HttpServletRequest request,
			HttpServletResponse response) throws BiffException, IOException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();

		if (StringUtils.equals(action, "LOAD")) {
			Integer lignes = 0;
			CommonsMultipartFile file = filter.getFile();

//			String fileXlsOrCsv = file.getOriginalFilename();
//			String chk = fileXlsOrCsv.substring(fileXlsOrCsv.length() - 4, fileXlsOrCsv.length());
			try {
				lignes = loadAwbGsa(file, filter, sysUser);
				LoadLog log = new LoadLog();
				log.setNom(file.getOriginalFilename());
				log.setLine(lignes);
				log.setStatus("OK");
				log.setCreatedBy(sysUser.getUsername());
				log.setCreatedDate(new Date());

				loadLogService.merge(log);

				List<LoadLog> logs = loadLogService.findAll();
				String msgSucess = messageSource.getMessage("load.sucess", null, "!En cours",
						localeResolver.resolveLocale(request));
				List<CritIsoc> critIsoc = critIsocService.getAll();
				filter.init();
				filter.setPageSize(5);
				model.addAttribute("critIsoc", critIsoc);
				model.addAttribute("msgSucess", msgSucess);
				model.addAttribute("logs", logs);
				return "redirect:/secure/awb/load";

			} catch (Exception e) {
				LoadLog log = new LoadLog();
				log.setNom(file.getOriginalFilename());
				log.setLine(lignes);
				log.setStatus("KO");
				log.setCreatedBy(sysUser.getUsername());
				log.setCreatedDate(new Date());

				loadLogService.merge(log);

				List<LoadLog> logs = loadLogService.findAll();
				String msgError = messageSource.getMessage("load.error", null, "!En cours",
						localeResolver.resolveLocale(request));
				List<CritIsoc> critIsoc = critIsocService.getAll();

				model.addAttribute("critIsoc", critIsoc);
				model.addAttribute("msgError", msgError);
				model.addAttribute("logs", logs);
				filter.init();
				filter.setPageSize(5);
				return "redirect:/secure/awb/load";
			}
		}
		return "redirect:/secure/awb/load";
	}

	private Integer loadAwbGsa(CommonsMultipartFile file, FileFilter filter, SysUser sysUser) throws BiffException,
			IOException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		Integer lines = 0;
		AwbGsaLoader loader = new AwbGsaLoader();

		List<AwbGSAMapping> dataList = loader.preDataAwbGsa(file);

		if (dataList != null && dataList.size() > 0) {
			for (AwbGSAMapping elm : dataList) {
				Awb awb = awbService.find(elm.getLta());
				if (awb != null && StringUtils.isBlank(awb.getStatusFlown())) {

					awb.setAgtnGsa(filter.getAgtnGsa());
					awb.setHandlingInfor(elm.getHandlingInfor());
					awb.setDescriptUser(elm.getDescriptUser());
					awb.setDateExecute(elm.getDateExecute());
					awb.setFlightNumber(elm.getFlightNumber());
					awb.setFlightDate(elm.getFlightDate());
					awb.setChargeIndicator(elm.getChargeIndicator());
					awb.setWeightGross(elm.getWeightGross());
					awb.setWeightCharge(elm.getWeightCharge());
					awb.setCutp(elm.getCutp());
					awb.setWeightPp(elm.getWeightPp());
					awb.setWeightCc(elm.getWeightCc());

					awb.setVatPercent(0.0);
					awb.setVat(0.0);
					awb.setComm(Math.abs(elm.getCommGsa()));
					awb.setNet(Math.abs(elm.getNet()));

					if (StringUtils.equals(elm.getChargeIndicator(), "P")) {

						awb.setCarrierPp(elm.getCarrier());
						awb.setCarrierCc(0.0);

						awb.setAgentPp(elm.getAgent());
						awb.setAgentCc(0.0);

						awb.setTotalPp(elm.getNet() - elm.getCommGsa());
						awb.setTotalCc(0.0);

					} else {
						awb.setCarrierPp(0.0);
						awb.setCarrierCc(Math.abs(elm.getCarrier()));

						awb.setAgentPp(0.0);
						awb.setAgentCc(Math.abs(elm.getAgent()));

						awb.setTotalPp(0.0);
						awb.setTotalCc(Math.abs(elm.getNet()) - Math.abs(elm.getCommGsa()));
					}

					awbService.deleteAwbDetail(elm.getLta());
					awbService.deleteAwbTax(elm.getLta());

					// DETAIL
					AwbDetail awbDetail = loader.preDetail(awb, elm, sysUser);
					awbDetail.setAwb(awb);
					awb.getAwbDetail().add(awbDetail);

					// Tax

					Set<AwbTax> setTax = new HashSet<AwbTax>();
					setTax = loader.preAwbTax(awb, elm, sysUser);
					awb.setAwbTax(setTax);

					/*
					 * Comm percent gsa percent compercent = (MYC + HT)/comm
					 */
					if (awb.getComm() > 0.0) {
						Double taxMyc = 0.0;
						for (AwbTax elmTax : setTax) {
							if (StringUtils.equals(elmTax.getId().getTaxCode(), "MYC")) {
								taxMyc = elmTax.getTaxAmount();
								break;
							}
						}
						awb.setCommPercent(
								DoubleUtil.round((taxMyc + awb.getWeightPp() + awb.getWeightCc()) / awb.getComm(), 2));
					}
					awb.setModifiedBy(sysUser.getUsername());
					awb.setModifiedDate(new Date());
					awbService.merge(awb);
				} else {
					awb = new Awb();

					awb.setTacn(elm.getTacn());
					awb.setIsoc(filter.getIsoc());
					awb.setLta(elm.getLta());
					awb.setAgtn(elm.getAgtn());
					awb.setAgtnCode(StringUtils.substring(awb.getAgtn(), 0, 10));
					awb.setCheckDigit(StringUtils.substring(awb.getAgtn(), 10, 11));
					awb.setAgtnIata(StringUtils.substring(awb.getAgtn(), 0, 7));
					awb.setAgtnName(elm.getAgtnName());
					awb.setOrig(elm.getOrig());
					awb.setDest(elm.getDest());
					awb.setOtherIndicator(elm.getChargeIndicator());
					awb.setCutp(elm.getCutp());
					awb.setChannel(CargoUtils.OWN);
					awb.setModePayment("CA");
					awb.setDeclaredCarrige(CargoUtils.NVD);
					awb.setDeclaredCustom(CargoUtils.NCV);

					awb.setHandlingInfor(elm.getHandlingInfor());
					awb.setDescriptUser(elm.getDescriptUser());
					awb.setDateExecute(elm.getDateExecute());
					awb.setFlightNumber(elm.getFlightNumber());
					awb.setFlightDate(elm.getFlightDate());
					awb.setChargeIndicator(elm.getChargeIndicator());
					awb.setWeightIndicator(CargoUtils.WEIGHT_K);
					awb.setWeightGross(elm.getWeightGross());
					awb.setWeightCharge(elm.getWeightCharge());
					awb.setUnitCass(elm.getUnitCass());
					awb.setCutp(elm.getCutp());
					awb.setWeightPp(elm.getWeightPp());
					awb.setWeightCc(elm.getWeightCc());

					awb.setVatPercent(0.0);
					awb.setVat(0.0);
					awb.setComm(Math.abs(elm.getCommGsa()));
					awb.setNet(Math.abs(elm.getNet()));

					// RDV
					Rdv rdv = awbUtil.processRdv(awb.getAgtn(), DateUtil.date2String(new Date(), "dd/MM/yyyy"));
					awb.setRdv(rdv.getRdv());
					awb.setIdRdv(rdv.getCode());

					// GSA
					/*
					 * AgentGsa agentGsa =
					 * agentGsaService.getAgentGsa(awb.getAgtnIata(),
					 * awb.getDateExecute()); if(agentGsa != null){
					 * awb.setAgtnGsa(agentGsa.getCode()); }
					 */
					awb.setAgtnGsa(filter.getAgtnGsa());

					// KM
					Double km = awbService.getKm(awb.getOrig(), awb.getDest());
					awb.setKm(km);

					if (StringUtils.equals(elm.getChargeIndicator(), "P")) {

						awb.setCarrierPp(elm.getCarrier());
						awb.setCarrierCc(0.0);

						awb.setAgentPp(elm.getAgent());
						awb.setAgentCc(0.0);

						awb.setTotalPp(elm.getNet() - elm.getCommGsa());
						awb.setTotalCc(0.0);

					} else {
						awb.setCarrierPp(0.0);
						awb.setCarrierCc(Math.abs(elm.getCarrier()));

						awb.setAgentPp(0.0);
						awb.setAgentCc(Math.abs(elm.getAgent()));

						awb.setTotalPp(0.0);
						awb.setTotalCc(Math.abs(elm.getNet()) - Math.abs(elm.getCommGsa()));
					}

					// DETAIL
					AwbDetail awbDetail = loader.preDetail(awb, elm, sysUser);
					awbDetail.setAwb(awb);
					awb.getAwbDetail().add(awbDetail);

					// Tax

					Set<AwbTax> setTax = new HashSet<AwbTax>();
					setTax = loader.preAwbTax(awb, elm, sysUser);
					awb.setAwbTax(setTax);

					/*
					 * Comm percent gsa percent compercent = (MYC + HT)/comm
					 */
					if (awb.getComm() > 0.0) {
						Double taxMyc = 0.0;
						for (AwbTax elmTax : setTax) {
							if (StringUtils.equals(elmTax.getId().getTaxCode(), "MYC")) {
								taxMyc = elmTax.getTaxAmount();
								break;
							}
						}
						awb.setCommPercent(
								DoubleUtil.round((taxMyc + awb.getWeightPp() + awb.getWeightCc()) / awb.getComm(), 2));
					}

					awb.setCreatedBy(sysUser.getUsername());
					awb.setCreatedDate(new Date());
					awbService.merge(awb);
				}

				lines++;
			}
		}

		return lines;
	}

}
