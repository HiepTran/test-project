package com.datawings.app.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.LocaleResolver;

import com.datawings.app.common.BeanUtil;
import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.StockFilter;
import com.datawings.app.model.StockAgent;
import com.datawings.app.model.StockAgentHist;
import com.datawings.app.model.StockAgentUsed;
import com.datawings.app.model.StockCentral;
import com.datawings.app.model.StockCentralHist;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.report.StockCompagnieExcel;
import com.datawings.app.service.IStockAgentHistService;
import com.datawings.app.service.IStockAgentService;
import com.datawings.app.service.IStockAgentUsedService;
import com.datawings.app.service.IStockCentralHistService;
import com.datawings.app.service.IStockCentralService;
import com.datawings.app.validator.StockAgentValidator;
import com.datawings.app.validator.StockCentralValidator;

@Controller
@SessionAttributes({"stockFilter"})
@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_STOCK', 'ROLE_STOCK_COMPAGNIE')")
public class StockCompagnieController {

	@Autowired
	private IStockCentralHistService stockCentralHistService;
	
	@Autowired
	private IStockCentralService stockCentralService;
	
	@Autowired
	private StockCentralValidator stockCentralValidator;
	
	@Autowired
	private IStockAgentHistService stockAgentHistService;
	
	@Autowired
	private IStockAgentService stockAgentService;
	
	@Autowired
	private IStockAgentUsedService stockAgentUsedService;
	
	@Autowired
	private StockAgentValidator stockAgentValidator;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;
	@ModelAttribute("stockFilter")  
    public StockFilter getInitStock() {  
		StockFilter filter = new StockFilter();
        return filter;  
    } 
	
	@RequestMapping(value = "/secure/stock/compagnie", method = RequestMethod.GET)
	public String getStockCompagnie(@ModelAttribute("stockFilter") StockFilter filter, Model model, HttpServletRequest request,
			HttpServletResponse response) {

		filter.setType(CargoUtils.STOCK_OWN);
		List<StockCentral> stockCentrals = stockCentralService.getStockCentral(filter);
		model.addAttribute("stockCentrals", stockCentrals);
		
		return "stockCompagnie";
	}
	
	@RequestMapping(value = "/secure/stock/compagnie", method = RequestMethod.POST)
	public String postStockCompagnie(@ModelAttribute("stockFilter") StockFilter filter,BindingResult result, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		if(StringUtils.equals(action, "ADD")){
			
			stockCentralValidator.checkStockCentral(filter, result);
			if(result.hasErrors()){
				filter.setType(CargoUtils.STOCK_OWN);
				List<StockCentral> stockCentrals = stockCentralService.getStockCentral(filter);
				model.addAttribute("stockCentrals", stockCentrals);
				
				model.addAttribute("errorAdd", true);
				return "stockCompagnie";
			}
			
			StockCentralHist stockCentralHist = new StockCentralHist();
			stockCentralHist.setSerialFrom(Integer.parseInt(filter.getSerialFrom()));
			stockCentralHist.setQuantity(Integer.parseInt(filter.getQuantity()));
			stockCentralHist.setSerialTo(stockCentralHist.getSerialFrom() + stockCentralHist.getQuantity() - 1);
			stockCentralHist.setQuantity(stockCentralHist.getQuantity());
			stockCentralHist.setComment(filter.getComment());
			stockCentralHist.setType(CargoUtils.STOCK_OWN);
			stockCentralHist.setDateEntry(new Date());
			
			stockCentralHist.setCreatedBy(sysUser.getUsername());
			stockCentralHist.setCreatedDate(new Date());
			
			stockCentralHistService.save(stockCentralHist);
			
			StockCentral stockCentral = new StockCentral();
			BeanUtil.copyProperties(stockCentral, stockCentralHist);
			stockCentral.setDateReplenish(DateUtil.string2Date("01/01/0001", "dd/MM/yyyy"));
			
			stockCentralService.save(stockCentral);
			
			filter.init();
		}else if(StringUtils.equals(action, "DELETE")){
			Integer id = ServletRequestUtils.getIntParameter(request, "id", -1);
			stockCentralHistService.deleteById(id);
			
		}else if(StringUtils.equals(action, "MOVE")){
			Integer id = ServletRequestUtils.getIntParameter(request, "id", -1);
			StockCentral stockCentral = stockCentralService.find(id);
			
			stockCentralValidator.checkStockMove(stockCentral, filter, result);
			if(result.hasErrors()){
				filter.setType(CargoUtils.STOCK_OWN);
				List<StockCentral> stockCentrals = stockCentralService.getStockCentral(filter);
				model.addAttribute("stockCentrals", stockCentrals);
				
				model.addAttribute("id", id);
				model.addAttribute("errorMove", true);
				return "stockCompagnie";
			}
			
			StockAgentHist stockAgentHist = new StockAgentHist();
			stockAgentHist.setAgtn(filter.getAgtn());
			stockAgentHist.setSerialFrom(stockCentral.getSerialFrom());
			stockAgentHist.setSerialTo(stockCentral.getSerialFrom() + Integer.parseInt(filter.getQuantity()) - 1);
			stockAgentHist.setQuantity(Integer.parseInt(filter.getQuantity()));
			stockAgentHist.setQuantityAvailable(Integer.parseInt(filter.getQuantity()));
			stockAgentHist.setDateEntry(new Date());
			stockAgentHist.setType(CargoUtils.STOCK_OWN);
			stockAgentHist.setComment(filter.getComment());
			stockAgentHist.setIdStockCentralHist(stockCentral.getIdStockCentralHist());
			stockAgentHist.setCreatedBy(sysUser.getUsername());
			stockAgentHist.setCreatedDate(new Date());
			
			stockAgentHistService.save(stockAgentHist);
			
			StockAgent stockAgent = new StockAgent();
			BeanUtil.copyProperties(stockAgent, stockAgentHist);
			stockAgent.setDateUsed(DateUtil.string2Date("01/01/0001", "dd/MM/yyyy"));
			
			stockAgentService.save(stockAgent);
			
			
			if(stockCentral.getQuantity() - Integer.parseInt(filter.getQuantity()) == 0){
				stockCentralService.delete(stockCentral);
			}else {
				stockCentral.setSerialFrom(stockCentral.getSerialFrom() + Integer.parseInt(filter.getQuantity()));
				stockCentral.setQuantity(stockCentral.getQuantity() - Integer.parseInt(filter.getQuantity()));
				stockCentral.setDateReplenish(new Date());
				stockCentral.setModifiedBy(sysUser.getUsername());
				stockCentral.setModifiedDate(new Date());
				
				stockCentralService.merge(stockCentral);
			}
			
			filter.init();
			
		}
		return "redirect:/secure/stock/compagnie";
	}
	
	@RequestMapping("/secure/stock/compagnie/model")
	public @ResponseBody StockCentral getStockCentralAjax(Model model, HttpServletRequest request) {
		String id = request.getParameter("id");
		StockCentral data = stockCentralService.find(Integer.parseInt(id));
		return data;
	}
	
	@RequestMapping(value = "/secure/stock/compagnie/agent", method = RequestMethod.GET)
	public String getStockCompagnieAgent(@ModelAttribute("stockFilter") StockFilter filter, Model model, HttpServletRequest request,
			HttpServletResponse response) {

		filter.setType(CargoUtils.STOCK_OWN);
		List<StockAgent> stockAgent = stockAgentService.getStockAgent(filter);
		model.addAttribute("stockAgent", stockAgent);
		
		return "compagnieAgent";
	}
	
	@RequestMapping(value = "/secure/stock/compagnie/agent", method = RequestMethod.POST)
	public String postStockAgent(@ModelAttribute("stockFilter") StockFilter filter,BindingResult result, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		if(StringUtils.equals(action, "CANCEL")){
			Integer id = ServletRequestUtils.getIntParameter(request, "id", -1);
			StockAgent stockAgent = stockAgentService.find(id);
			
			StockAgentUsed stockAgentUsed = new StockAgentUsed();
			
			BeanUtil.copyProperties(stockAgentUsed, stockAgent);
			stockAgentUsed.setStatus("CANCEL");
			stockAgentUsed.setCreatedBy(sysUser.getUsername());
			stockAgentUsed.setCreatedDate(new Date());
			
			stockAgentUsedService.merge(stockAgentUsed);
			
			stockAgentService.delete(stockAgent);
			
		}else if(StringUtils.equals(action, "MOVE")){
			Integer id = ServletRequestUtils.getIntParameter(request, "idAgent", -1);
			StockAgent stockAgent = stockAgentService.find(id);
			
			if(StringUtils.equals(filter.getTypeMove(), "AGENT")){
				stockAgentValidator.checkMoveAgent(stockAgent, filter, result);
				if(result.hasErrors()){
					filter.setType(CargoUtils.STOCK_OWN);
					List<StockAgent> stockAgents = stockAgentService.getStockAgent(filter);
					model.addAttribute("stockAgent", stockAgents);
					model.addAttribute("id", id);
					model.addAttribute("errorMove", true);
					return "compagnieAgent";
				}
				
				List<StockAgent> stockSplit = splitStockAgent(stockAgent, filter, sysUser);
				stockAgentService.delete(stockAgent);
				
				for (StockAgent elm : stockSplit) {
					stockAgentService.save(elm);
				}
				
			}else if(StringUtils.equals(filter.getTypeMove(), "CENTRAL")){
				StockCentral stockCentral = new StockCentral();
				stockCentral.setSerialFrom(stockAgent.getSerialFrom());
				stockCentral.setSerialTo(stockAgent.getSerialTo());
				stockCentral.setQuantity(stockAgent.getQuantity());
				stockCentral.setDateEntry(new Date());
				stockCentral.setIdStockCentralHist(stockAgent.getIdStockCentralHist());
				stockCentral.setType(CargoUtils.STOCK_OWN);
				stockCentral.setComment(filter.getComment());
				stockCentral.setCreatedBy(sysUser.getUsername());
				stockCentral.setCreatedDate(new Date());
				
				stockCentralService.save(stockCentral);
				stockAgentService.delete(stockAgent);
			}
		}
		
		return "redirect:/secure/stock/compagnie/agent";
	}

	@RequestMapping("/secure/stock/compagnie/agent/model")
	public @ResponseBody StockAgent getStockAgentAjax(Model model, HttpServletRequest request) {
		String id = request.getParameter("id");
		StockAgent data = stockAgentService.find(Integer.parseInt(id));
		return data;
	}
	
	private List<StockAgent> splitStockAgent(StockAgent stockAgent, StockFilter filter, SysUser sysUser) {
		List<StockAgent> rs = new ArrayList<StockAgent>();
		
		StockAgent tmp1= new StockAgent();
		tmp1.setSerialFrom(stockAgent.getSerialFrom());
		tmp1.setSerialTo(Integer.parseInt(filter.getSerialFrom()) - 1);
		tmp1.setQuantity(tmp1.getSerialTo() - tmp1.getSerialFrom() + 1);
		
		tmp1.setIdStockCentralHist(stockAgent.getIdStockCentralHist());
		tmp1.setAgtn(stockAgent.getAgtn());
		tmp1.setAgentOrig(stockAgent.getAgentOrig());
		tmp1.setDateEntry(stockAgent.getDateEntry());
		tmp1.setType(CargoUtils.STOCK_OWN);
		tmp1.setComment(stockAgent.getComment());
		tmp1.setDateUsed(DateUtil.string2Date("01/01/0001", "dd/MM/yyyy"));
		tmp1.setCreatedBy(stockAgent.getCreatedBy());
		tmp1.setCreatedDate(stockAgent.getCreatedDate());
		tmp1.setModifiedBy(sysUser.getUsername());
		tmp1.setModifiedDate(new Date());
		
		
		//new
		StockAgent tmp2= new StockAgent();
		tmp2.setSerialFrom(Integer.parseInt(filter.getSerialFrom()));
		tmp2.setSerialTo(Integer.parseInt(filter.getSerialFrom()) + Integer.parseInt(filter.getQuantity()) - 1);
		tmp2.setQuantity(tmp2.getSerialTo() - tmp2.getSerialFrom() + 1);
		
		tmp2.setIdStockCentralHist(stockAgent.getIdStockCentralHist());
		tmp2.setAgtn(filter.getAgtn());
		tmp2.setAgentOrig(stockAgent.getAgtn());
		tmp2.setDateEntry(stockAgent.getDateEntry());
		tmp2.setType(CargoUtils.STOCK_OWN);
		tmp2.setComment(filter.getComment());
		tmp2.setDateUsed(DateUtil.string2Date("01/01/0001", "dd/MM/yyyy"));
		tmp2.setCreatedBy(sysUser.getUsername());
		tmp2.setCreatedDate(new Date());
		
		
		
		StockAgent tmp3= new StockAgent();
		tmp3.setSerialFrom(Integer.parseInt(filter.getSerialFrom()) + Integer.parseInt(filter.getQuantity()));
		tmp3.setSerialTo(stockAgent.getSerialTo());
		tmp3.setQuantity(tmp3.getSerialTo() - tmp3.getSerialFrom() + 1);
		
		tmp3.setIdStockCentralHist(stockAgent.getIdStockCentralHist());
		tmp3.setAgtn(stockAgent.getAgtn());
		tmp3.setAgentOrig(stockAgent.getAgentOrig());
		tmp3.setDateEntry(stockAgent.getDateEntry());
		tmp3.setType(CargoUtils.STOCK_OWN);
		tmp3.setComment(stockAgent.getComment());
		tmp3.setDateUsed(DateUtil.string2Date("01/01/0001", "dd/MM/yyyy"));
		tmp3.setCreatedBy(stockAgent.getCreatedBy());
		tmp3.setCreatedDate(stockAgent.getCreatedDate());
		tmp3.setModifiedBy(sysUser.getUsername());
		tmp3.setModifiedDate(new Date());
		
		if(tmp1.getQuantity() > 0){
			rs.add(tmp1);
		}
		if(tmp2.getQuantity() > 0){
			rs.add(tmp2);
		}
		if(tmp3.getQuantity() > 0){
			rs.add(tmp3);
		}
		
		return rs;
	}
	
	@RequestMapping(value = "/secure/stock/compagnie/excel", method = RequestMethod.GET)
	public void getExcelCompagnieApp(@ModelAttribute("stockFilter") StockFilter filter, Model model, HttpServletRequest request, HttpServletResponse response) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<StockCentral> stockCentrals = stockCentralService.getStockCentral(filter);
		StockCompagnieExcel stockcompagnie = new StockCompagnieExcel();
		
		baos = stockcompagnie.getExcelStockCompagnie(stockCentrals, filter, messageSource, localeResolver.resolveLocale(request));
		response.setContentType("application/xls");
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ "ApprovisonnementCompagnie.xls" + "\";");
		response.setContentLength(baos.size());
		
		ServletOutputStream out = response.getOutputStream();
		baos.writeTo(out);
		out.flush();
		out.close();
	}
	
	@RequestMapping(value = "/secure/stock/compagnie/agent/excel", method = RequestMethod.GET)
	public void getExcelStockAgent(@ModelAttribute("stockFilter") StockFilter filter, Model model, HttpServletRequest request, HttpServletResponse response) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		List<StockAgent> stockAgent = stockAgentService.getStockAgent(filter);
		StockCompagnieExcel stockCompagnie = new StockCompagnieExcel();
		baos = stockCompagnie.getExcelStockAgent(stockAgent, filter, messageSource, localeResolver.resolveLocale(request));
		response.setContentType("application/xls");
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ "StockAgentCompagnie.xls" + "\";");
		response.setContentLength(baos.size());
		
		ServletOutputStream out = response.getOutputStream();
		baos.writeTo(out);
		out.flush();
		out.close();
	}
}
