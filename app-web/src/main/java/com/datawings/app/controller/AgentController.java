package com.datawings.app.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.LocaleResolver;

import com.datawings.app.common.DoubleUtil;
import com.datawings.app.filter.AgentFilter;
import com.datawings.app.model.Agent;
import com.datawings.app.model.CritIsoc;
import com.datawings.app.model.SysUser;
import com.datawings.app.report.AgentExcel;
import com.datawings.app.service.IAgentService;
import com.datawings.app.service.IAirlineParamService;
import com.datawings.app.service.ICritIsocService;
import com.datawings.app.validator.AgentValidator;
import com.datawings.app.validator.ValidationError;

@Controller
@SessionAttributes({ "agentFilterPara" })
@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_SETTING', 'ROLE_SETTING_AGENT')")
public class AgentController {

	@Autowired
	private IAgentService agentService;

	@Autowired
	private ICritIsocService critIsocService;
	
	@Autowired
	private IAirlineParamService airlineParamService;
	
	@Autowired
	private AgentValidator agentValidator;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@ModelAttribute("agentFilterPara")
	public AgentFilter getInitCreate() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		AgentFilter filter = new AgentFilter();
		filter.setPageSize(100);
		filter.setIsoc(airlineParamService.find(sysUser.getAirlineCode()).getIsoc());
		return filter;
	}
	
	@RequestMapping(value = "/secure/agent", method = RequestMethod.GET)
	public String getAgent(@ModelAttribute("agentFilterPara") AgentFilter filter, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		Integer rowCount = agentService.getAgentRowCount(filter);
		filter.setRowCount(rowCount);
		List<String> listIsoc = agentService.getIsoc();
		List<CritIsoc> lstCritIsoc = critIsocService.findAll();
		
		model.addAttribute("listIsoc", listIsoc);	
		model.addAttribute("lstCritIsoc", lstCritIsoc);
		return "agent";
	}
	
	@RequestMapping(value = "/secure/agent/loadpage", method = RequestMethod.GET)
	public String getAgentLoadPage(@ModelAttribute("agentFilterPara") AgentFilter filter, Integer pageNo, Model model){
		filter.setPage(pageNo);
		List<Agent> agents = agentService.getAgent(filter);
		model.addAttribute("agents", agents);
		model.addAttribute("row", (filter.getPage()) * filter.getPageSize());
		return "agentLoadPage";
	}
	
	@RequestMapping(value = "/secure/agent", method = RequestMethod.POST)
	public String postAgent(@ModelAttribute("agentFilterPara") AgentFilter filter, Model model, HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if(StringUtils.equals(action, "UPDATE")){
			String code = ServletRequestUtils.getStringParameter(request, "id", "");
			Agent agent = agentService.find(code);
			agent.setName(filter.getName().trim().toUpperCase());
			agent.setAddress1(filter.getAddress1().trim().toUpperCase());
			agent.setKind(filter.getKind());
			agent.setCity(filter.getCity().trim().toUpperCase());
			agent.setZip(filter.getZip().trim().toUpperCase());
			agent.setIsoc(filter.getIsoc());
			agent.setRegion(filter.getRegion().trim().toUpperCase());
			agent.setPhone(filter.getPhone().trim().toUpperCase());
			agent.setFax(filter.getFax().trim().toUpperCase());
			agent.setEmail(filter.getEmail().trim());
			agent.setIataInd(filter.getIataInd());
			agent.setComm(DoubleUtil.convertDouble(filter.getComm()));
			
			agent.setModifiedBy(sysUser.getUsername());
			agent.setModifiedDate(new Date());
			agentService.merge(agent);
		}else if(StringUtils.equals(action, "DELETE")){
  			String code = ServletRequestUtils.getStringParameter(request, "id", "");
			Agent agent = agentService.find(code);
			agentService.delete(agent);
			if(filter.getOffset() > 0 && filter.getOffset() >= filter.getRowCount() - 1){
				Integer p = filter.getPage();
				filter.setPage(--p);
			}
		}else if(StringUtils.equals(action, "GO")){
			filter.setPage(0);
			return "redirect:/secure/agent";
		}else if(StringUtils.equals(action, "RESET")){
			filter.init();
			filter.setPageSize(100);
		}
		return "redirect:/secure/agent";
	}
	
	@RequestMapping(value = {"/secure/agent/model/ajax/{id}"}, method = RequestMethod.GET)
	public String getAircraft(@PathVariable String id, Model model) {	
		Agent agent = agentService.find(id);
		List<CritIsoc> listIsoc = critIsocService.getAll();
		model.addAttribute("listIsoc", listIsoc);
		model.addAttribute("agent", agent);
		return "agentAjax";
	}
	
	//begin export excel list agent
	@RequestMapping(value = "/secure/agent/excel", method = RequestMethod.GET)
	public void getExcelAgent(@ModelAttribute("agentFilterPara") AgentFilter filter, Model model,
			HttpServletRequest request, HttpServletResponse response) throws IOException{
		List<Agent> lst = agentService.getAgentEX(filter);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		AgentExcel agentExcel = new AgentExcel();
		baos = agentExcel.exportExcel(lst, filter, messageSource, localeResolver.resolveLocale(request));
		
		response.setContentType("application/xls");
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-Disposition", "attachment; filename=\""+ "Agent.xls" + "\";");
		response.setContentLength(baos.size());
		
		ServletOutputStream out = response.getOutputStream();
		baos.writeTo(out);
		out.flush();
		out.close();
	}
	
	@RequestMapping(value = "/secure/agent/error", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidator(@ModelAttribute("agentFilterPara") AgentFilter filter, BindingResult result, Model model, Locale locale){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		
		int errCode = 0;
		errCode = agentValidator.checkAgentValidator(filter, result);
		if(errCode > 0){
			rs.put("errCodeEdit", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for(int i = 0; i< result.getAllErrors().size(); i++){
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null, 
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		
		return rs;
	}
	
	@RequestMapping(value = "/secure/agent/create/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorCreate(@ModelAttribute("agentFilterPara") AgentFilter filter, BindingResult result, Model model, Locale locale){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		
		int errCode = 0;
		errCode = agentValidator.checkValidatorCreateAgent(filter, result);
		if(errCode > 0){
			rs.put("errCodeCreate", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for(int i = 0; i< result.getAllErrors().size(); i++){
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null, 
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		
		return rs;
	}
	
	@RequestMapping(value = "/secure/agent/create", method = RequestMethod.POST)
	public String doCreateAgent(@ModelAttribute("agentFilterPara") AgentFilter filter, Model model, HttpServletRequest request){
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		if(StringUtils.equals(action, "CREATE")){
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			SysUser sysUser = (SysUser) auth.getPrincipal();
			
			Agent agent = new Agent();
			agent.setCode(StringUtils.substring(filter.getAgtnCode(), 0, 10));
			agent.setCheckDigit(StringUtils.substring(filter.getAgtnCode(), 10, 11));
			agent.setName(filter.getName().trim().toUpperCase());
			agent.setAddress1(filter.getAddress1().trim().toUpperCase());
			agent.setKind(filter.getKind());
			agent.setCity(filter.getCity().trim().toUpperCase());
			agent.setZip(filter.getZip().trim().toUpperCase());
			agent.setIsoc(filter.getIsoc());
			agent.setRegion(filter.getRegion().trim().toUpperCase());
			agent.setPhone(filter.getPhone().trim().toUpperCase());
			agent.setFax(filter.getFax().trim().toUpperCase());
			agent.setEmail(filter.getEmail().trim());
			agent.setIataInd(filter.getIataInd());
			agent.setComm(DoubleUtil.convertDouble(filter.getComm()));
			agent.setCreatedBy(sysUser.getUsername());
			agent.setCreatedDate(new Date());
			
			agentService.merge(agent);
			filter.init();
			filter.setPageSize(100);
			filter.setIsoc(airlineParamService.find(sysUser.getAirlineCode()).getIsoc());
		}
		return "redirect:/secure/agent";
	}
}
