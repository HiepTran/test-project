package com.datawings.app.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.datawings.app.model.SysRole;
import com.datawings.app.model.SysUser;
import com.datawings.app.service.ISysUserService;

@Controller
public class LoginController {			
	
	@Autowired
	private ISysUserService sysUserService;
	
	@RequestMapping(value = "/secure/login/success", method = RequestMethod.GET)
	public String loginSuccess(Model model, HttpServletRequest request) {
		//Set date login user
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		sysUser.setLastLoginDate(sysUser.getLoginDate());
		sysUser.setLoginDate(new Date());
		sysUserService.merge(sysUser);
		
		request.getSession().setAttribute("sysUser", sysUser);
		
		List<SysRole> lstRole = getRoleLogin();
		
		if(lstRole != null){
			return "redirect:/secure/dashboard";
		}else{
			return "login";
		}		
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		model.addAttribute("login", "Login Page");
		return "login";
	}

	@RequestMapping(value = "/loginFailed", method = RequestMethod.GET)
	public String loginFailed(Model model) {		
		model.addAttribute("loginFailed", "Login Failed");
		return "login";
	}

	@RequestMapping(value = "/accessdenied", method = RequestMethod.GET)
	public String loginAccessDenied(Model model) {
		return "accessdenied";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(Model model) {
		return "login";
	}
	
	@SuppressWarnings("unchecked")
	final List<SysRole> getRoleLogin(){
		List<SysRole> lstRole = (List<SysRole>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		return lstRole;
	}
}