package com.datawings.app.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.LocaleResolver;

import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.BsrFilter;
import com.datawings.app.model.Awb;
import com.datawings.app.model.Bsr;
import com.datawings.app.model.BsrId;
import com.datawings.app.model.Rdv;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.AwbUtil;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.service.IAirlineParamService;
import com.datawings.app.service.IAwbService;
import com.datawings.app.service.IBsrService;
import com.datawings.app.service.IRdvService;
import com.datawings.app.validator.AwbValidator;
import com.datawings.app.validator.BsrValidator;
import com.datawings.app.validator.ValidationError;

@Controller
@SessionAttributes({"awbFilter", "agentFilter", "bsrFilterCurr"})
@PreAuthorize(value = "hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_AWB', 'ROLE_AWB_CAPTURE')")
public class CreateAwbController {
	
	@Autowired
	private IRdvService rdvService;
	
	@Autowired
	private IAwbService awbService;
	
	@Autowired
	private AwbValidator awbValidator;
	
	@Autowired
	private BsrValidator bsrValidator;
	
	@Autowired
	private IBsrService bsrService;
	
	@Autowired
	private IAirlineParamService airlineParamService;
	
	@Autowired
	private AwbUtil awbUtil;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	@ModelAttribute("awbFilter")  
    public AwbFilter getInitCreate() {  
		AwbFilter filter = new AwbFilter();
		initFilter(filter);
        return filter;  
    }  
	
	@ModelAttribute("bsrFilterCurr")
	public BsrFilter getBsrFilter(){
		BsrFilter bsr = new BsrFilter();
		return bsr;
	}
	
	private void initFilter(AwbFilter filter) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
		
		filter.setTacn(sysUser.getAirlineCode());
		filter.setChargeIndicator(CargoUtils.CHARGE_P);
		filter.setOtherIndicator(CargoUtils.CHARGE_P);
		filter.setCutp(airlineParamService.find(sysUser.getAirlineCode()).getCurrency());
		filter.setWeightIndicator1(CargoUtils.WEIGHT_K);
		filter.setWeightIndicator2(CargoUtils.WEIGHT_K);
		filter.setWeightIndicator3(CargoUtils.WEIGHT_K);
		filter.setWeightIndicator4(CargoUtils.WEIGHT_K);
		filter.setWeightIndicator5(CargoUtils.WEIGHT_K);
		filter.setDeclaredCarrige(CargoUtils.NVD);
		filter.setDeclaredCustom(CargoUtils.NCV);
	}

	@RequestMapping(value = "/secure/createAwb", method = RequestMethod.GET)
	public String getCreate(@ModelAttribute("awbFilter") AwbFilter filter, Model model, HttpServletRequest request, HttpServletResponse response) {
		String codeRdv = ServletRequestUtils.getStringParameter(request, "codeRdv", "");
		request.getSession().setAttribute("codeRDV", codeRdv);
		Rdv rdv = rdvService.find(codeRdv);
		if(rdv == null){
			return "redirect:/secure/rdv";
		}
		model.addAttribute("rdv", rdv);
		return "createAwb";
	}
	
	@RequestMapping(value = "/secure/createAwb", method = RequestMethod.POST)
	public String postDashboard(@ModelAttribute("awbFilter") AwbFilter filter,
			BindingResult result, Model model, HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		SysUser sysUser = (SysUser) auth.getPrincipal();
				
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		String codeRdv = ServletRequestUtils.getStringParameter(request, "codeRdv", "");
		
		if(StringUtils.equals(action, "GO")){
			Rdv rdv = rdvService.find(codeRdv);
			
			Awb awb = new Awb();
			awb.setStatusControl(CargoUtils.STATUS_F);
			awb.setIdRdv(rdv.getCode());
			awb.setRdv(filter.getRdv());
			awb.setChannel(CargoUtils.OWN);
			awb.setCreatedDate(new Date());
			awb.setCreatedBy(sysUser.getUsername());
			
			awbUtil.copyValue(awb, filter);
			awbUtil.processLine(awb);
			awbUtil.processAgent(awb);
			awbUtil.processGsa(awb);
			awbUtil.processDetail(awb, filter);
			awbUtil.processTax(awb, filter);
			awbUtil.processTotal(awb, filter);
			awbUtil.convertLocal(awb);
			
			awbService.merge(awb);
			
			model.addAttribute("message", messageSource.getMessage("awb.awb", null, localeResolver.resolveLocale(request)) + " " + awb.getLta() + " " + messageSource.getMessage("awb.save.success", null, localeResolver.resolveLocale(request)));
			return "createAwb";
			
		}else if (StringUtils.equals(action, "NEW")) {
			/*Awb AwbChk = awbService.find(filter.getLta());
			if(AwbChk == null){
				Rdv rdv = rdvService.find(codeRdv);
				
				Awb awb = new Awb();
				awb.setStatusControl(CargoUtils.STATUS_F);
				awb.setIdRdv(rdv.getCode());
				awb.setRdv(filter.getRdv());
				awb.setChannel(CargoUtils.OWN);
				awb.setCreatedDate(new Date());
				awb.setCreatedBy(sysUser.getUsername());
				
				awbUtil.copyValue(awb, filter);
				awbUtil.processLine(awb);
				awbUtil.convertLocal(awb);
				awbUtil.processAgent(awb);
				awbUtil.processGsa(awb);
				awbUtil.processDetail(awb, filter);
				awbUtil.processTax(awb, filter);
				awbUtil.processTotal(awb, filter);
							
				awbService.merge(awb);
			}*/
			
			filter.init();
			initFilter(filter);
		}
		
		return "redirect:/secure/createAwb?codeRdv="+ codeRdv;
	}
	
	@RequestMapping(value = "/secure/awb/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidator(@ModelAttribute("awbFilter") AwbFilter filter, BindingResult result, Model model, Locale locale){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = 0;
		errCode = awbValidator.checkLTA(filter, result);
		if(errCode > 0){
			rs.put("errCode", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for(int i = 0;i <result.getAllErrors().size();i++){
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null, 
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}
	
	@RequestMapping(value = "/secure/createAwb/curr/error.json", method = RequestMethod.POST)
	@ResponseBody
	public HashMap<String, Object> doValidatorCreateCurr(@ModelAttribute("bsrFilterCurr") BsrFilter filter, BindingResult result, Model model, Locale locale ){
		HashMap<String, Object> rs = new HashMap<String, Object>();
		int errCode = bsrValidator.doValidatorCreateBsr(filter, result);
		
		if(errCode > 0){
			rs.put("errCodeCreate", errCode);
			List<ValidationError> lstErr = new ArrayList<ValidationError>();
			for(int i = 0; i < result.getAllErrors().size(); i++){
				ValidationError elm = new ValidationError();
				elm.setMessage(messageSource.getMessage(result.getAllErrors().get(i).getCode(), null,
						result.getAllErrors().get(i).getDefaultMessage(), locale));
				elm.setPropertyName(StringUtils.substringAfterLast(result.getAllErrors().get(i).getCodes()[0], "."));
				lstErr.add(elm);
			}
			rs.put("lstErr", lstErr);
		}
		return rs;
	}
	
	@RequestMapping(value = "/secure/createAwb/curr", method = RequestMethod.POST)
	public String doCreateCurr(@ModelAttribute("bsrFilterCurr") BsrFilter filter, Model model, HttpServletRequest request){
		String action = ServletRequestUtils.getStringParameter(request, "action", "");
		String codeRdv = (String) request.getSession().getAttribute("codeRDV");
		if(StringUtils.equals(action, "CREATE")){
			Bsr newBsr = new Bsr();
			BsrId bsrId = new BsrId();
			bsrId.setCurrFrom(filter.getCurrFromAdd().trim().toUpperCase());
			bsrId.setCurrTo(filter.getCurrToAdd().trim().toUpperCase());
			bsrId.setDateStart(DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"));
			newBsr.setBsrId(bsrId);
			if(StringUtils.equals(filter.getDateEndAdd(), ""))
				filter.setDateEndAdd(CargoUtils.MAX_DATE);
			if(DateUtil.checAfter(DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"), DateUtil.string2Date(filter.getDateEndAdd(), "dd/MM/yyyy")))
				newBsr.setDateEnd(DateUtil.string2Date(filter.getDateEndAdd(), "dd/MM/yyyy"));
			newBsr.setRate(filter.getRateAdd());
			newBsr.setTimeStamp_C(new Date());
			bsrService.save(newBsr);
			filter.initFormAdd();
		}
		return "redirect:/secure/createAwb?codeRdv="+ codeRdv;
	}
}
