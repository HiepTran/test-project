package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.context.MessageSource;

import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.TaxFilter;
import com.datawings.app.model.Tax;

public class TaxExcel extends AExcelPageEvent {
	Map<String, HSSFCellStyle> styles;
	Map<String, HSSFCellStyle> styles1;
	public TaxExcel() {
		super();
		styles = new HashMap<String, HSSFCellStyle>();
		styles1 = new HashMap<String, HSSFCellStyle>();
	}
	public ByteArrayOutputStream exportExcel(List<Tax> lst, TaxFilter filter, MessageSource messageSource, Locale locale) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		
		//sheet filter
		HSSFSheet sheetFilter = wb.createSheet("TAX");
		sheetFilter.setDisplayGridlines(false);
		sheetFilter.setFitToPage(true);
		sheetFilter.setAutobreaks(true);
		createStyles(wb, styles1);
		
		Integer row = 0;
		Integer col = 0;
		
		writeCell(sheetFilter, row, col, "FILTER", styles1.get("filterStyleCenter"), 1, wb);
		row = row + 1;
		col = 0;
		
		writeCell(sheetFilter, row, col, "Country", styles1.get("filterStyle"));
		if(StringUtils.isNotBlank(filter.getIsoc())){
			writeCell(sheetFilter, row, ++col, filter.getIsoc(), styles1.get("filterStyle"));
		}
		else
			writeCell(sheetFilter, row, ++col, "ALL", styles1.get("filterStyle"));
		row = row + 1;
		col = 0;
		
		writeCell(sheetFilter, row, col, "Tax code", styles1.get("filterStyle"));
		writeCell(sheetFilter, row, ++col, filter.getCode(), styles1.get("filterStyle"));
		row = row + 1;
		col = 0;
		
		writeCell(sheetFilter, row, col, "Subcode", styles1.get("filterStyle"));
		writeCell(sheetFilter, row, ++col, filter.getSubCode(), styles1.get("filterStyle"));
		row = row + 1;
		col = 0;
		
		writeCell(sheetFilter, row, col, "Item", styles1.get("filterStyle"));
		writeCell(sheetFilter, row, ++col, filter.getItem(), styles1.get("filterStyle"));
		
		row = 0;
		col = 0;
		
		//sheet detail
		HSSFSheet sheetDetail = wb.createSheet(messageSource.getMessage("message.detail", null, "", locale));
		sheetDetail.setDisplayGridlines(false);
		sheetDetail.setFitToPage(true);
		sheetDetail.setAutobreaks(true);
		createStyles(wb, styles);
		
		writeCell(sheetDetail, row, col, messageSource.getMessage("tax.country", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("tax.code", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("tax.sub.code", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("tax.item", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("tax.currency", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("tax.unit", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("tax.amount", null, "", locale), styles.get("titleRight"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("tax.date.start", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("tax.date.end", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("tax.description", null, "", locale), styles.get("titleLeft"));
		
		HSSFCellStyle detailLeft = styles.get("detailLeft");
		HSSFCellStyle detailRight = styles.get("detailRight");
		HSSFCellStyle detailColorLeft = styles.get("detailColorLeft");
		HSSFCellStyle detailColorRight = styles.get("detailColorRight");
		HSSFCellStyle styleLeft;
		HSSFCellStyle styleRight;
		
		for(int i = 0; i < lst.size(); i++){
			if (i % 2 == 0) {
				styleLeft = detailLeft;
				styleRight = detailRight;
			} else {
				styleLeft = detailColorLeft;
				styleRight = detailColorRight;
			}
			
			Tax elm = lst.get(i);
			col = 0;
			writeCell(sheetDetail, ++row, col, elm.getIsoc(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getCode(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getSubCode(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getItem(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getCutp(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getUnit(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getAmount(), styleRight);
			writeCell(sheetDetail, row, ++col, DateUtil.date2String(elm.getDateStart(), "dd/MM/yyyy"), styleLeft);
			if(!StringUtils.contains(DateUtil.date2String(elm.getDateEnd(), "dd/MM/yyyy"), "9999")){
				writeCell(sheetDetail, row, ++col, DateUtil.date2String(elm.getDateEnd(), "dd/MM/yyyy"), styleLeft);
			}
			else
				writeCell(sheetDetail, row, ++col, "", styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getDescription(), styleRight);
		}
		
		for (int k = 0; k <= col; k++) {
			sheetDetail.autoSizeColumn((short) k);
			sheetFilter.autoSizeColumn((short) k);
		}

		wb.write(baos);
		return baos;
		
	}
	
	
	
	
}
