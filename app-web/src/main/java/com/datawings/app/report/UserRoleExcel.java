package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.context.MessageSource;

import com.datawings.app.filter.UserFilter;
import com.datawings.app.model.SysRole;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.CargoUtils;

public class UserRoleExcel extends AExcelPageEvent {
	Map<String, HSSFCellStyle> style;
	
	public UserRoleExcel(){
		super();
		style = new HashMap<String, HSSFCellStyle>();
	}
	
	public ByteArrayOutputStream exportExcel(List<SysUser> lstUser, UserFilter filter, SysUser user, MessageSource messageSource, Locale locale) throws IOException{
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		
		HSSFSheet sheet = wb.createSheet("USER");
		sheet.setDisplayGridlines(false);
		sheet.setFitToPage(true);
		sheet.setAutobreaks(true);
		createStyles(wb, style);
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		Integer row = 0;
		Integer col = 0;
		
		writeCell(sheet, row, col, "Liste User", style.get("filterStyleCenter"), 7, wb);
		
		row = row + 1;
		col = 0;
		
		writeCell(sheet, row, col, messageSource.getMessage("user.table.username", null, "", locale), style.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("user.table.password", null, "", locale), style.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("user.table.name", null, "", locale), style.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("user.table.telephone", null, "", locale), style.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("user.table.email", null, "", locale), style.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("user.table.create", null, "", locale), style.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("user.table.modify", null, "", locale), style.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("user.table.active", null, "", locale), style.get("titleLeft"));
		
		HSSFCellStyle detailLeft = style.get("detailLeft");
		HSSFCellStyle detailRight = style.get("detailRight");
		HSSFCellStyle detailColorLeft = style.get("detailColorLeft");
		HSSFCellStyle detailColorRight = style.get("detailColorRight");
		HSSFCellStyle styleLeft;
		HSSFCellStyle styleRight;
		
		for(int i = 0; i < lstUser.size(); i++){
			if(i%2 == 0){
				styleLeft = detailLeft;
				styleRight = detailRight;
			} else {
				styleLeft = detailColorLeft;
				styleRight = detailColorRight;
			}
			
			SysUser elm = lstUser.get(i);
			col = 0;
			writeCell(sheet, ++row, col, elm.getUsername(), styleLeft);
			for(SysRole r : user.getRoles()){
				if(StringUtils.equals(r.getAuthority(), CargoUtils.ROLE_DW)){
					writeCell(sheet, row, ++col, elm.getPassword(), styleLeft);
				}
				else if(StringUtils.equals(r.getAuthority(), CargoUtils.ROLE_ADMIN)) {
					writeCell(sheet, row, ++col, "**********", styleLeft);
				}
			}
			writeCell(sheet, row, ++col, elm.getName(), styleLeft);
			writeCell(sheet, row, ++col, elm.getTelephone(), styleRight);
			writeCell(sheet, row, ++col, elm.getEmail(), styleLeft);
			writeCell(sheet, row, ++col, elm.getCreatedBy() + " " + dateFormat.format(elm.getCreatedOn()), styleLeft);
			writeCell(sheet, row, ++col, elm.getModifiedBy() + " " + dateFormat.format(elm.getModifiedOn()), styleLeft);
			writeCell(sheet, row, ++col, elm.getIsEnable().toString(), styleLeft);
		}
		
		for (int k = 0; k <= col; k++) {
			sheet.autoSizeColumn((short) k);
		}

		wb.write(bao);
		return bao;
	}
}
