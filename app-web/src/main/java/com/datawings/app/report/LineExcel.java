package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.context.MessageSource;

import com.datawings.app.filter.LineFilter;
import com.datawings.app.model.Line;

public class LineExcel extends AExcelPageEvent {
	Map<String, HSSFCellStyle> styles;
	Map<String, HSSFCellStyle> styles1;
	
	
	public LineExcel() {
		super();
		styles = new HashMap<String, HSSFCellStyle>();
		styles1 = new HashMap<String, HSSFCellStyle>();
	}


	public ByteArrayOutputStream exportExcel(List<Line> lst, LineFilter filter, MessageSource messageSource, Locale locale) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		
		//sheet filter
		HSSFSheet sheetFilter = wb.createSheet("TAX");
		sheetFilter.setDisplayGridlines(false);
		sheetFilter.setFitToPage(true);
		sheetFilter.setAutobreaks(true);
		createStyles(wb, styles1);
		
		Integer row = 0;
		Integer col = 0;
		
		writeCell(sheetFilter, row, col, "FILTER", styles1.get("filterStyleCenter"), 1, wb);
		row = row + 1;
		col = 0;
		
		writeCell(sheetFilter, row, col, messageSource.getMessage("line.orig", null, "", locale), styles1.get("filterStyle"));
		writeCell(sheetFilter, row, ++col, filter.getOrig(), styles1.get("filterStyle"));
		row = row + 1;
		col = 0;
		
		writeCell(sheetFilter, row, col, messageSource.getMessage("line.dest", null, "", locale), styles1.get("filterStyle"));
		writeCell(sheetFilter, row, ++col, filter.getDest(), styles1.get("filterStyle"));
		row = row + 1;
		col = 0;
		
		writeCell(sheetFilter, row, col, messageSource.getMessage("line.line", null, "", locale), styles1.get("filterStyle"));
		writeCell(sheetFilter, row, ++col, filter.getLine(), styles1.get("filterStyle"));
		
		row = 0;
		col = 0;
		
		//sheet detail
		HSSFSheet sheetDetail = wb.createSheet(messageSource.getMessage("message.detail", null, "", locale));
		sheetDetail.setDisplayGridlines(false);
		sheetDetail.setFitToPage(true);
		sheetDetail.setAutobreaks(true);
		createStyles(wb, styles);
		
		writeCell(sheetDetail, row, col, messageSource.getMessage("line.orig", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("line.dest", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("line.line", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("line.description", null, "", locale), styles.get("titleLeft"));
		
		HSSFCellStyle detailLeft = styles.get("detailLeft");
//		HSSFCellStyle detailRight = styles.get("detailRight");
		HSSFCellStyle detailColorLeft = styles.get("detailColorLeft");
//		HSSFCellStyle detailColorRight = styles.get("detailColorRight");
		HSSFCellStyle styleLeft;
//		HSSFCellStyle styleRight;
		
		for(int i = 0; i < lst.size(); i++){
			if (i % 2 == 0) {
				styleLeft = detailLeft;
//				styleRight = detailRight;
			} else {
				styleLeft = detailColorLeft;
//				styleRight = detailColorRight;
			}
			Line elm = lst.get(i);
			col = 0;
			writeCell(sheetDetail, ++row, col, elm.getOrac(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getDstc(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getLine(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getDescripbe(), styleLeft);
		}
		for (int k = 0; k <= col; k++) {
			sheetDetail.autoSizeColumn((short) k);
			sheetFilter.autoSizeColumn((short) k);
		}

		wb.write(baos);
		return baos;
	}

}
