package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.context.MessageSource;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.DoubleUtil;
import com.datawings.app.model.AwbFlown;
import com.datawings.app.model.Manifest;

public class AwbFlownExcel extends AExcelPageEvent{

	Map<String, HSSFCellStyle> styles;
	public AwbFlownExcel() {
		super();
		styles = new HashMap<String, HSSFCellStyle>();
	}
	
	
	public ByteArrayOutputStream exportExcel(Manifest manifest, List<AwbFlown> awbFlowns, MessageSource messageSource, Locale locale) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		
		HSSFSheet sheet = wb.createSheet("Lta flown");
		sheet.setDisplayGridlines(false);
		sheet.setFitToPage(true);
		sheet.setAutobreaks(true);
		createStyles(wb, styles);
		
		Integer row = 0;
		Integer col = 0;
		
		HSSFCellStyle filterStyle = styles.get("filterStyle");
		writeCell(sheet, row, col, "LTA FLOWN", filterStyle, 6, wb);
		
		row = row +1;
		col = 0;
		writeCell(sheet, ++row, col, messageSource.getMessage("manifest.owner", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("manifest.no", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("manifest.orig", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("manifest.dest", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("manifest.flight.number", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("manifest.flight.date", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("manifest.immat", null, "", locale), styles.get("titleLeft"));
		
		col = 0;
		writeCell(sheet, ++row, col, manifest.getTacn(), styles.get("detailLeft"));
		writeCell(sheet, row, ++col, manifest.getManifestNo(), styles.get("detailLeft"));
		writeCell(sheet, row, ++col, manifest.getOrig(), styles.get("detailLeft"));
		writeCell(sheet, row, ++col, manifest.getDest(), styles.get("detailLeft"));
		writeCell(sheet, row, ++col, manifest.getFlightNumber() , styles.get("detailLeft"));
		writeCell(sheet, row, ++col, DateUtil.date2String(manifest.getFlightDate(), "dd/MM/yyyy"), styles.get("detailLeft"));
		writeCell(sheet, row, ++col, manifest.getImmat(), styles.get("detailLeft"));

		row = row +1;
		col = 0;
		
		writeCell(sheet, ++row, col, messageSource.getMessage("agent.country", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.awb", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.agent", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.agent.gsa", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.agent.name", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.rdv", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.date.execution", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.orig", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.dest", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.line", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.weight.charge", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, "INDICATOR", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.currency", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.total.weight.charge", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.charge.agent", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.charge.carrier", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.total.weight.charge", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.charge.agent", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.charge.carrier", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.discount", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.com", null, "", locale), styles.get("titleRight"));
		
		HSSFCellStyle detailLeft = styles.get("detailLeft");
		HSSFCellStyle detailRight = styles.get("detailRight");
		HSSFCellStyle detailColorLeft = styles.get("detailColorLeft");
		HSSFCellStyle detailColorRight = styles.get("detailColorRight");
		HSSFCellStyle styleLeft;
		HSSFCellStyle styleRight;
		
		
		for(int i = 0; i< awbFlowns.size(); i++){
			if(i % 2 ==0){
				styleLeft = detailLeft;
				styleRight = detailRight;
			}else {
				styleLeft = detailColorLeft;
				styleRight = detailColorRight;
			}
			
			AwbFlown elm = awbFlowns.get(i);
			col = 0;
			writeCell(sheet, ++row, col, elm.getIsoc(), styleLeft);
			writeCell(sheet, row, ++col, elm.getTacn() + "-" + elm.getLta(), styleLeft);
			writeCell(sheet, row, ++col, elm.getAgtn(), styleLeft);
			writeCell(sheet, row, ++col, elm.getAgtnGsa(), styleLeft);
			writeCell(sheet, row, ++col, elm.getAgtnName(), styleLeft);
			writeCell(sheet, row, ++col, elm.getRdv(), styleLeft);
			writeCell(sheet, row, ++col, DateUtil.date2String(elm.getDateExecute(), "dd/MM/yyyy"), styleLeft);
			writeCell(sheet, row, ++col, elm.getOrig(), styleLeft);
			writeCell(sheet, row, ++col, elm.getDest(), styleLeft);
			writeCell(sheet, row, ++col, elm.getLine(), styleLeft);
			writeCell(sheet, row, ++col, elm.getWeightGross() , styleRight);
			writeCell(sheet, row, ++col, elm.getWeightIndicator() , styleLeft);
			writeCell(sheet, row, ++col, elm.getCutp() , styleLeft);
			
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getWeightPp()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getAgentPp()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getCarrierPp()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getWeightCc()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getAgentCc()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getCarrierCc()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getDiscount()) , styleRight);
			
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getComm()) , styleRight);
		}
		
		for (int k = 0; k <= col; k++) {
			sheet.autoSizeColumn((short) k);
		}
	
		wb.write(baos);
		return baos;
	}
}
