package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.context.MessageSource;

import com.datawings.app.common.DateUtil;
import com.datawings.app.model.SysRole;

public class RoleExcel extends AExcelPageEvent {
	Map<String, HSSFCellStyle> styles;

	public RoleExcel() {
		super();
		styles = new HashMap<String, HSSFCellStyle>();
	}
	
	public ByteArrayOutputStream exportExcel(List<SysRole> lst, MessageSource messageSource, Locale locale) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		
		Integer row = 0;
		Integer col = 0;
		
		//sheet detail
		HSSFSheet sheetDetail = wb.createSheet(messageSource.getMessage("message.detail", null, "", locale));
		sheetDetail.setDisplayGridlines(false);
		sheetDetail.setFitToPage(true);
		sheetDetail.setAutobreaks(true);
		createStyles(wb, styles);
		
		writeCell(sheetDetail, row, col, messageSource.getMessage("role.table.authority", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("role.table.description", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("role.table.create", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("role.table.modify", null, "", locale), styles.get("titleLeft"));
		
		HSSFCellStyle detailLeft = styles.get("detailLeft");
		HSSFCellStyle detailColorLeft = styles.get("detailColorLeft");
		HSSFCellStyle styleLeft;
		
		for(int i = 0; i < lst.size(); i++){
			if (i % 2 == 0) {
				styleLeft = detailLeft;
			} else {
				styleLeft = detailColorLeft;
			}
			
			SysRole elm = lst.get(i);
			
			col = 0;
			writeCell(sheetDetail, ++row, col, elm.getAuthority(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getDescription(), styleLeft);
			if(!StringUtils.equals(elm.getCreatedBy(), ""))
				writeCell(sheetDetail, row, ++col, elm.getCreatedBy() + " - " + DateUtil.date2String(elm.getCreatedOn(), "dd/MM/yyyy"), styleLeft);
			else
				writeCell(sheetDetail, row, ++col, elm.getCreatedBy(), styleLeft);
			if(!StringUtils.equals(elm.getModified(), ""))
				writeCell(sheetDetail, row, ++col, elm.getModified() + " - " + DateUtil.date2String(elm.getModifiedOn(), "dd/MM/yyyy"), styleLeft);
			else
				writeCell(sheetDetail, row, ++col, elm.getModified(), styleLeft);
		}
		
		for (int k = 0; k <= col; k++) {
			sheetDetail.autoSizeColumn((short) k);
		}

		wb.write(baos);
		return baos;
	}
	
}
