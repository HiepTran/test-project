package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.context.MessageSource;

import com.datawings.app.filter.LegKmFilter;
import com.datawings.app.model.LegKm;

public class LegKmExcel extends AExcelPageEvent {
	Map<String, HSSFCellStyle> styles;

	public LegKmExcel() {
		super();
		styles = new HashMap<String, HSSFCellStyle>();
	}

	public ByteArrayOutputStream exportExcel(List<LegKm> lstLegKm, LegKmFilter filter, MessageSource messageSource, Locale locale) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();

		HSSFSheet sheet = wb.createSheet(messageSource.getMessage("message.detail", null, "", locale));
		sheet.setDisplayGridlines(false);
		sheet.setFitToPage(true);
		sheet.setAutobreaks(true);
		createStyles(wb, styles);

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();

		Integer row = 0;
		Integer col = 0;

		HSSFCellStyle filterStyle = styles.get("filterStyle");

		writeCell(sheet, row, col, dateFormat.format(date), filterStyle, 5, wb);
		row = row + 1;
		col = 0;
		writeCell(sheet, row, col, "Liste des distances des segments",styles.get("filterStyleCenter") , 5, wb);
		row = row + 1;
		col = 0;
		writeCell(sheet, row, col, "", filterStyle, 5, wb);
		row = row + 1;
		col = 0;

		// writeCell(sheet, ++row, col, "No", styles.get("titleLeft"));
		writeCell(sheet, row, col, messageSource.getMessage("legkm.isocfrom", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("legkm.isocto", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("legkm.airportfrom", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("legkm.airportto", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("legkm.km", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("legkm.mile", null, "", locale), styles.get("titleRight"));

		HSSFCellStyle detailLeft = styles.get("detailLeft");
		HSSFCellStyle detailRight = styles.get("detailRight");
		HSSFCellStyle detailColorLeft = styles.get("detailColorLeft");
		HSSFCellStyle detailColorRight = styles.get("detailColorRight");
		HSSFCellStyle styleLeft;
		HSSFCellStyle styleRight;

		for (int i = 0; i < lstLegKm.size(); i++) {
			if (i % 2 == 0) {
				styleLeft = detailLeft;
				styleRight = detailRight;
			} else {
				styleLeft = detailColorLeft;
				styleRight = detailColorRight;
			}
			LegKm elm = lstLegKm.get(i);
			col = 0;
			writeCell(sheet, ++row, col, elm.getId().getIsocFrom(), styleLeft);
			writeCell(sheet, row, ++col, elm.getId().getAirportFrom(), styleLeft);
			writeCell(sheet, row, ++col, elm.getId().getIsocTo(), styleLeft);
			writeCell(sheet, row, ++col, elm.getId().getAirportTo(), styleLeft);
			writeCell(sheet, row, ++col, elm.getKm(), styleRight);
			writeCell(sheet, row, ++col, elm.getMile(), styleRight);
		}
		for (int k = 0; k <= col; k++) {
			sheet.autoSizeColumn((short) k);
		}

		wb.write(baos);
		return baos;
	}
}
