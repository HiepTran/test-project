package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.context.MessageSource;

import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.BsrFilter;
import com.datawings.app.model.Bsr;

public class BsrExcel extends AExcelPageEvent {
	Map<String, HSSFCellStyle> styles;
	Map<String, HSSFCellStyle> styles1;
	
	public BsrExcel() {
		super();
		this.styles = new HashMap<String, HSSFCellStyle>();
		this.styles1 = new HashMap<String, HSSFCellStyle>();
	}
	
	public ByteArrayOutputStream exportExcel(List<Bsr> lstBsr, BsrFilter filter, MessageSource messageSource, Locale locale) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		
		//sheet filter
		HSSFSheet sheetFilter = wb.createSheet("BSR");
		sheetFilter.setDisplayGridlines(false);
		sheetFilter.setFitToPage(true);
		sheetFilter.setAutobreaks(true);
		createStyles(wb, styles1);
		
		Integer row = 0;
		Integer col = 0;
		
		writeCell(sheetFilter, row, col, "FILTER", styles1.get("filterStyleCenter"), 1, wb);
		row = row + 1;
		col = 0;
		
		writeCell(sheetFilter, row, col, messageSource.getMessage("bsr.currfrom", null, "", locale), styles1.get("filterStyle"));
		writeCell(sheetFilter, row, ++col, filter.getCurrFrom().trim().toUpperCase(), styles1.get("filterStyle"));
		row = row + 1;
		col = 0;
		
		writeCell(sheetFilter, row, col, messageSource.getMessage("bsr.currto", null, "", locale), styles1.get("filterStyle"));
		writeCell(sheetFilter, row, ++col, filter.getCurrTo().trim().toUpperCase(), styles1.get("filterStyle"));
		row = row + 1;
		col = 0;
		
		writeCell(sheetFilter, row, col, messageSource.getMessage("bsr.startdate", null, "", locale), styles1.get("filterStyle"));
		writeCell(sheetFilter, row, ++col, filter.getDateStart(), styles1.get("filterStyle"));
		row = row + 1;
		col = 0;
		
		writeCell(sheetFilter, row, col, messageSource.getMessage("bsr.enddate", null, "", locale), styles1.get("filterStyle"));
		writeCell(sheetFilter, row, ++col, filter.getDateEnd(), styles1.get("filterStyle"));
		
		row = 0;
		col = 0;
		
		//sheet detail
		HSSFSheet sheetDetail = wb.createSheet(messageSource.getMessage("message.detail", null, "", locale));
		sheetDetail.setDisplayGridlines(false);
		sheetDetail.setFitToPage(true);
		sheetDetail.setAutobreaks(true);
		createStyles(wb, styles);
		
		writeCell(sheetDetail, row, col, messageSource.getMessage("bsr.currfrom", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("bsr.currto", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("bsr.rate", null, "", locale), styles.get("titleRight"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("bsr.startdate", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("bsr.enddate", null, "", locale), styles.get("titleLeft"));
		
		HSSFCellStyle detailLeft = styles.get("detailLeft");
		HSSFCellStyle detailRight = styles.get("detailRight");
		HSSFCellStyle detailColorLeft = styles.get("detailColorLeft");
		HSSFCellStyle detailColorRight = styles.get("detailColorRight");
		HSSFCellStyle styleLeft;
		HSSFCellStyle styleRight;
		
		for(int i = 0; i < lstBsr.size(); i++){
			if (i % 2 == 0) {
				styleLeft = detailLeft;
				styleRight = detailRight;
			} else {
				styleLeft = detailColorLeft;
				styleRight = detailColorRight;
			}
			
			Bsr elm = lstBsr.get(i);
			col = 0;
			
			writeCell(sheetDetail, ++row, col, elm.getBsrId().getCurrFrom(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getBsrId().getCurrTo(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getRate(), styleRight);
			writeCell(sheetDetail, row, ++col, DateUtil.date2String(elm.getBsrId().getDateStart(), "dd/MM/yyyy"), styleLeft);
			writeCell(sheetDetail, row, ++col, DateUtil.date2String(elm.getDateEnd(), "dd/MM/yyyy"), styleLeft);
		}
		
		for (int k = 0; k <= col; k++) {
			sheetDetail.autoSizeColumn((short) k);
			sheetFilter.autoSizeColumn((short) k);
		}
		wb.write(baos);
		return baos;
	}
}
