package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.context.MessageSource;

import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.StockFilter;
import com.datawings.app.model.StockAgent;
import com.datawings.app.model.StockCentral;

public class StockTransitaireExcel extends AExcelPageEvent {
	Map<String, HSSFCellStyle> styles;

	public StockTransitaireExcel() {
		super();
		styles = new HashMap<String, HSSFCellStyle>();
	}

	public ByteArrayOutputStream getExcelStockAgent(List<StockAgent> stockAgent, StockFilter filter, MessageSource messageSource, Locale locale) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		
		HSSFSheet sheetDetail = wb.createSheet(messageSource.getMessage("message.detail", null, "", locale));
		sheetDetail.setFitToPage(true);
		sheetDetail.setAutobreaks(true);
		sheetDetail.setDisplayGridlines(false);
		createStyles(wb, styles);
		
		Integer row = 0;
		Integer col = 0;
		
		writeCell(sheetDetail, row, col, messageSource.getMessage("stock.compagnie.agent", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("stock.compagnie.from", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("stock.compagnie.to", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("stock.compagnie.quantity", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("stock.compagnie.date", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("stock.compagnie.replenish", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("stock.compagnie.agent.orig", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("stock.compagnie.comment", null, "", locale), styles.get("titleLeft"));
		

		HSSFCellStyle detailLeft = styles.get("detailLeft");
//		HSSFCellStyle detailRight = styles.get("detailRight");
		HSSFCellStyle detailColorLeft = styles.get("detailColorLeft");
//		HSSFCellStyle detailColorRight = styles.get("detailColorRight");
		HSSFCellStyle styleLeft;
//		HSSFCellStyle styleRight;
		
		for(int i = 0; i < stockAgent.size(); i++){
			if (i % 2 == 0) {
				styleLeft = detailLeft;
//				styleRight = detailRight;
			} else {
				styleLeft = detailColorLeft;
//				styleRight = detailColorRight;
			}
			
			StockAgent elm = stockAgent.get(i);
			col = 0;
			writeCell(sheetDetail, ++row, col, elm.getAgtn(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getSerialFrom(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getSerialTo(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getQuantity(), styleLeft);
			writeCell(sheetDetail, row, ++col, DateUtil.date2String(elm.getDateEntry(), "dd/MM/yyyy"), styleLeft);
			if(!StringUtils.equals(DateUtil.date2String(elm.getDateUsed(), "dd/MM/yyyy"), "01/01/0001"))
				writeCell(sheetDetail, row, ++col, DateUtil.date2String(elm.getDateUsed(), "dd/MM/yyyy"), styleLeft);
			else
				writeCell(sheetDetail, row, ++col, " ", styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getAgentOrig(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getComment(), styleLeft);
		}
		for (int k = 0; k <= col; k++) {
			sheetDetail.autoSizeColumn((short) k);
		}
		
		wb.write(baos);
		return baos;
	}

	public ByteArrayOutputStream getExcelStockTransitaire(List<StockCentral> lst, StockFilter filter, MessageSource messageSource, Locale locale) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		
		HSSFSheet sheetDetail = wb.createSheet(messageSource.getMessage("message.detail", null, "", locale));
		sheetDetail.setDisplayGridlines(false);
		sheetDetail.setFitToPage(true);
		sheetDetail.setAutobreaks(true);
		createStyles(wb, styles);
		
		Integer row = 0;
		Integer col = 0;
		
		writeCell(sheetDetail, row, col, messageSource.getMessage("stock.compagnie.from", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("stock.compagnie.to", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("stock.compagnie.quantity", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("stock.compagnie.date", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("stock.compagnie.replenish", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("stock.compagnie.comment", null, "", locale), styles.get("titleLeft"));
		
		HSSFCellStyle detailLeft = styles.get("detailLeft");
//		HSSFCellStyle detailRight = styles.get("detailRight");
		HSSFCellStyle detailColorLeft = styles.get("detailColorLeft");
//		HSSFCellStyle detailColorRight = styles.get("detailColorRight");
		HSSFCellStyle styleLeft;
//		HSSFCellStyle styleRight;
		
		for(int i = 0; i < lst.size(); i++){
			if (i % 2 == 0) {
				styleLeft = detailLeft;
//				styleRight = detailRight;
			} else {
				styleLeft = detailColorLeft;
//				styleRight = detailColorRight;
			}
			
			StockCentral elm = lst.get(i);
			col = 0;
			
			writeCell(sheetDetail, ++row, col, elm.getSerialFrom(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getSerialTo(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getQuantity(), styleLeft);
			writeCell(sheetDetail, row, ++col, DateUtil.date2String(elm.getDateEntry(), "dd/MM/yyyy"), styleLeft);
			if(!StringUtils.equals(DateUtil.date2String(elm.getDateReplenish(), "dd/MM/yyyy"), "01/01/0001"))
				writeCell(sheetDetail, row, ++col, DateUtil.date2String(elm.getDateReplenish(), "dd/MM/yyyy"), styleLeft);
			else
				writeCell(sheetDetail, row, ++col, " ", styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getComment(), styleLeft);
		}
		
		for (int k = 0; k <= col; k++) {
			sheetDetail.autoSizeColumn((short) k);
		}
		
		wb.write(baos);
		return baos;
	}
	
}
