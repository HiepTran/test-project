package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.context.MessageSource;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.DoubleUtil;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.model.Awb;
import com.datawings.app.process.CargoUtils;

public class AwbSalesExcel extends AExcelPageEvent{

	Map<String, HSSFCellStyle> styles;
	public AwbSalesExcel() {
		super();
		styles = new HashMap<String, HSSFCellStyle>();
	}
	
	public ByteArrayOutputStream exportExcel(List<Awb> listAwb, AwbFilter filter, MessageSource messageSource, Locale locale) throws IOException {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		
		HSSFSheet sheet = wb.createSheet("Vente des LTAs");
		sheet.setDisplayGridlines(false);
		sheet.setFitToPage(true);
		sheet.setAutobreaks(true);
		createStyles(wb, styles);
		
		Integer row = 0;
		Integer col = 0;
		
		HSSFCellStyle filterStyle = styles.get("filterStyle");
		writeCell(sheet, row, col, "LTA VENTE", filterStyle, 6, wb);
		
		row = row +1;
		col = 0;
		
		writeCell(sheet, ++row, col, messageSource.getMessage("awb.country", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.awb", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.agent", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.agent.gsa", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.agent.name", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.rdv", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.date.execution", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.orig", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.dest", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.weight.charge", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, "INDICATOR", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.currency", null, "", locale), styles.get("titleLeft"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.total.weight.charge", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.charge.agent", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.charge.carrier", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.total.weight.charge", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.charge.agent", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.charge.carrier", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, "% " + messageSource.getMessage("awb.list.com", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.list.com", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.net", null, "", locale), styles.get("titleRight"));
		writeCell(sheet, row, ++col, messageSource.getMessage("awb.channel", null, "", locale), styles.get("titleLeft"));
		
		HSSFCellStyle detailLeft = styles.get("detailLeft");
		HSSFCellStyle detailRight = styles.get("detailRight");
		HSSFCellStyle detailColorLeft = styles.get("detailColorLeft");
		HSSFCellStyle detailColorRight = styles.get("detailColorRight");
		HSSFCellStyle styleLeft;
		HSSFCellStyle styleRight;
		
		for(int i = 0; i< listAwb.size(); i++){
			if(i % 2 ==0){
				styleLeft = detailLeft;
				styleRight = detailRight;
			}else {
				styleLeft = detailColorLeft;
				styleRight = detailColorRight;
			}
			
			Awb elm = listAwb.get(i);
			col = 0;
			writeCell(sheet, ++row, col, elm.getIsoc(), styleLeft);
			writeCell(sheet, row, ++col, elm.getTacn() + "-" + elm.getLta(), styleLeft);
			writeCell(sheet, row, ++col, elm.getAgtn(), styleLeft);
			writeCell(sheet, row, ++col, elm.getAgtnGsa(), styleLeft);
			writeCell(sheet, row, ++col, elm.getAgtnName() , styleLeft);
			writeCell(sheet, row, ++col, elm.getRdv() , styleLeft);
			writeCell(sheet, row, ++col, DateUtil.date2String(elm.getDateExecute(), "dd/MM/yyyy"), styleLeft);
			writeCell(sheet, row, ++col, elm.getOrig() , styleLeft);
			writeCell(sheet, row, ++col, elm.getDest() , styleLeft);
			writeCell(sheet, row, ++col, elm.getWeightGross() , styleRight);
			writeCell(sheet, row, ++col, elm.getWeightIndicator() , styleLeft);
			writeCell(sheet, row, ++col, elm.getCutp() , styleLeft);
			
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getWeightPp()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getAgentPp()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getCarrierPp()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getWeightCc()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getAgentCc()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getCarrierCc()) , styleRight);
			
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getCommPercent()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getComm()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getNet()) , styleRight);
			
			if(StringUtils.equals(elm.getChannel(), CargoUtils.HOT)){
				writeCell(sheet, row, ++col, "CASS" , styleLeft);
			}else {
				writeCell(sheet, row, ++col, "CAPTURE" , styleLeft);
			}
		}
		
		for (int k = 0; k <= col; k++) {
			sheet.autoSizeColumn((short) k);
		}
	
		wb.write(baos);
		return baos;
	}

}
