package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.context.MessageSource;

import com.datawings.app.filter.AgentFilter;
import com.datawings.app.model.Agent;

public class AgentExcel extends AExcelPageEvent {
	Map<String, HSSFCellStyle> styles;
	Map<String, HSSFCellStyle> styles1;

	public AgentExcel() {
		super();
		styles = new HashMap<String, HSSFCellStyle>();
		styles1 = new HashMap<String, HSSFCellStyle>();
	}
	
	/*
	input: List Agent
	output: byte array with list agent
	*/
	
	public ByteArrayOutputStream exportExcel(List<Agent> lstAgent, AgentFilter filter, MessageSource messageSource, Locale locale) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		
		//sheet agent
		HSSFSheet sheetAgent = wb.createSheet(messageSource.getMessage("agent.code", null, "", locale));
		sheetAgent.setDisplayGridlines(false);
		sheetAgent.setFitToPage(true);
		sheetAgent.setAutobreaks(true);
		createStyles(wb, styles1);
		
		Integer row = 0;
		Integer col = 0;
		
		writeCell(sheetAgent, row, col, "FILTER", styles1.get("filterStyleCenter"), 1, wb);
		row = row + 1;
		col = 0;
		
		writeCell(sheetAgent, row, col, messageSource.getMessage("agent.code", null, "", locale), styles1.get("filterStyle"));
		writeCell(sheetAgent, row, ++col, filter.getCodeExt(), styles1.get("filterStyle"));
		row = row + 1;
		col = 0;
		
		writeCell(sheetAgent, row, col, messageSource.getMessage("agent.kind", null, "", locale), styles1.get("filterStyle"));
		if(StringUtils.isNotBlank(filter.getKindExt())){
			writeCell(sheetAgent, row, ++col, filter.getKindExt(), styles1.get("filterStyle"));
		} else{
			writeCell(sheetAgent, row, ++col, "TOUT", styles1.get("filterStyle"));
		}
		row = row + 1;
		col = 0;
		
		writeCell(sheetAgent, row, col, messageSource.getMessage("agent.country", null, "", locale), styles1.get("filterStyle"));
		if(StringUtils.isNotBlank(filter.getIsocExt())){
			writeCell(sheetAgent, row, ++col, filter.getIsocExt(), styles1.get("filterStyle"));
		} else{
			writeCell(sheetAgent, row, ++col, "TOUT", styles1.get("filterStyle"));
		}
		row = row + 1;
		col = 0;
		
		writeCell(sheetAgent, row, col, messageSource.getMessage("agent.iata.ind", null, "", locale), styles1.get("filterStyle"));
		if(StringUtils.isNotBlank(filter.getIataIndExt())){
			writeCell(sheetAgent, row, ++col, filter.getIataIndExt(), styles1.get("filterStyle"));
		} else{
			writeCell(sheetAgent, row, ++col, "TOUT", styles1.get("filterStyle"));
		}
		row = 0;
		col = 0;
		
		//sheet detail
		HSSFSheet sheetDetail = wb.createSheet(messageSource.getMessage("message.detail", null, "", locale));
		sheetDetail.setDisplayGridlines(false);
		sheetDetail.setFitToPage(true);
		sheetDetail.setAutobreaks(true);
		createStyles(wb, styles);
		
		
		writeCell(sheetDetail, row, col, messageSource.getMessage("agent.code", null, "", locale), styles.get("titleRight"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("agent.check.digit", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("agent.name", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("agent.kind", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("agent.address", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("agent.city", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("agent.zip", null, "", locale), styles.get("titleRight"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("agent.country", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("agent.region", null, "", locale), styles.get("titleRight"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("agent.comm", null, "", locale), styles.get("titleRight"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("agent.phone", null, "", locale), styles.get("titleRight"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("agent.fax", null, "", locale), styles.get("titleRight"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("agent.email", null, "", locale), styles.get("titleLeft"));
		writeCell(sheetDetail, row, ++col, messageSource.getMessage("agent.iata.ind", null, "", locale), styles.get("titleRight"));
		
		HSSFCellStyle detailLeft = styles.get("detailLeft");
		HSSFCellStyle detailRight = styles.get("detailRight");
		HSSFCellStyle detailColorLeft = styles.get("detailColorLeft");
		HSSFCellStyle detailColorRight = styles.get("detailColorRight");
		HSSFCellStyle styleLeft;
		HSSFCellStyle styleRight;
		
		for (int i = 0; i < lstAgent.size(); i++) {
			if (i % 2 == 0) {
				styleLeft = detailLeft;
				styleRight = detailRight;
			} else {
				styleLeft = detailColorLeft;
				styleRight = detailColorRight;
			}
			Agent elm = lstAgent.get(i);
			col = 0;
			writeCell(sheetDetail, ++row, col, elm.getCode(), styleRight);
			writeCell(sheetDetail, row, ++col, elm.getCheckDigit(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getName(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getKind(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getAddress1(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getCity(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getZip(), styleRight);
			writeCell(sheetDetail, row, ++col, elm.getIsoc(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getRegion(), styleLeft);
			writeCell(sheetDetail, row, ++col, elm.getComm(), styleRight);
			writeCell(sheetDetail, row, ++col, elm.getPhone(), styleRight);
			writeCell(sheetDetail, row, ++col, elm.getFax(), styleRight);
			writeCell(sheetDetail, row, ++col, elm.getEmail(), styleLeft);
			if(StringUtils.equals(elm.getIataInd(), "1"))
				writeCell(sheetDetail, row, ++col, "Yes", styleLeft);
			else
				writeCell(sheetDetail, row, ++col, "No", styleLeft);
			
		}
		
		for (int k = 0; k <= col; k++) {
			sheetDetail.autoSizeColumn((short) k);
			sheetAgent.autoSizeColumn((short) k);
		}

		wb.write(baos);
		return baos;
	}
}
