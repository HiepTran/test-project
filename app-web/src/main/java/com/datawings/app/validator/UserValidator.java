package com.datawings.app.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.filter.UserFilter;
import com.datawings.app.model.SysUser;
import com.datawings.app.service.ISysUserService;

@Component
public class UserValidator {
	
	@Autowired private ISysUserService isysUserService;
	
	/**
	 * method check validator when create User
	 * @param target the current Object 
	 * @param error the Errors reject when have error
	 * @return 1 if have error or 0 if don't have error
	 */
	public int doValidatorCreate(Object target, Errors error){
		int errCode = 0;
		UserFilter filter = (UserFilter) target;
		if(StringUtils.isBlank(filter.getUsernameAdd())){
			error.rejectValue("usernameAdd", "message.not.empty", "!");
			errCode = 1;
		} else{
			SysUser user = isysUserService.findByUsername(filter.getUsernameAdd());
			if(user != null){
				error.rejectValue("usernameAdd", "message.exist.user", "!");
				errCode = 1;
			}
		}
		
		if(StringUtils.isBlank(filter.getPasswordAdd())){
			error.rejectValue("passwordAdd", "message.not.empty", "!");
			errCode = 1;
		} else if(StringUtils.isBlank(filter.getRetypePasswordAdd())){
			error.rejectValue("retypePasswordAdd", "message.not.empty", "!");
			errCode = 1;
		} else if(!StringUtils.equals(filter.getPasswordAdd(), filter.getRetypePasswordAdd())){
			error.rejectValue("retypePasswordAdd", "message.not.match", "!");
			errCode = 1;
		} 
		return errCode;
	}
	
	/**
	 * method check validator when edit user
	 * @param target the current Object
	 * @param error the Errors reject when have error
	 * @return 1 if have error or 0 if don't have error
	 */
	public int doValidatorEdit(Object target, Errors error){
		int errCode = 0;
		UserFilter filter = (UserFilter) target;
		if(StringUtils.isNotBlank(filter.getPasswordEdit()) || StringUtils.isNotBlank(filter.getRetypePasswordEdit())){
			if(StringUtils.isBlank(filter.getPasswordEdit())){
				error.rejectValue("passwordEdit", "message.not.empty", "!");
				errCode = 1;
			} else if(StringUtils.isBlank(filter.getRetypePasswordEdit())){
				error.rejectValue("retypePasswordEdit", "message.not.empty", "!");
				errCode = 1;
			} else if(!StringUtils.equals(filter.getPasswordEdit(), filter.getRetypePasswordEdit())){
				error.rejectValue("passwordEdit", "message.not.match", "!");
				errCode = 1;
			} 
		}
		return errCode;
	}
	
}
