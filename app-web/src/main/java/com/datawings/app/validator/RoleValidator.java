package com.datawings.app.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.filter.RoleFilter;
import com.datawings.app.model.SysRole;
import com.datawings.app.service.ISysRoleService;

@Component
public class RoleValidator {
	
	@Autowired private ISysRoleService isysRoleService;
	
	/**
	 * method check validator when create or modify role
	 * @param target the current Object 
	 * @param errors the Errors reject when have error 
	 * @return 1 if have error or 0 if don't have error
	 */
	public int checkValidatorRoleAdd(Object target, Errors errors){
		int errCode = 0;
		RoleFilter filter = (RoleFilter) target;
		if(StringUtils.isBlank(filter.getRoleNameAdd())){
			errors.rejectValue("roleNameAdd", "message.not.empty", "!");
			errCode = 1;
		}
		else{
			SysRole role = isysRoleService.findRoleByName(filter.getRoleNameAdd());
			if(role != null){
				errors.rejectValue("roleNameAdd", "message.exist.role", "!");
				errCode = 1;
			}
		}
		if(StringUtils.isBlank(filter.getDescriptionAdd())){
			errors.rejectValue("descriptionAdd", "message.not.empty", "!");
			errCode = 1;
		}
		return errCode;
	}
	
	/**
	 * method check validator when create or modify role
	 * @param target the current Object 
	 * @param errors the Errors reject when have error 
	 * @return 1 if have error or 0 if don't have error
	 */
	public int checkValidatorRoleEdit(Object target, Errors errors){
		int errCode = 0;
		RoleFilter filter = (RoleFilter) target;
		if(StringUtils.isBlank(filter.getRoleNameEdit())){
			errors.rejectValue("roleNameEdit", "message.not.empty", "!");
			errCode = 1;
		}
		if(StringUtils.isBlank(filter.getDescriptionEdit())){
			errors.rejectValue("descriptionEdit", "message.not.empty", "!");
			errCode = 1;
		}
		return errCode;
	}
	
}
