package com.datawings.app.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.filter.FileFilter;

@Component
public class LoaderValidator {
	
	public int doValidatorChanger(Object target, Errors error, String fileName, String fileSize){
		int errCode = 0;
		FileFilter fileFilter = (FileFilter) target;
		
		if(StringUtils.isBlank(fileFilter.getAgtnGsa())){
			errCode = 1;
			error.rejectValue("agtnGsa", "message.not.empty", "!");
		}
		
		if(StringUtils.isBlank(fileName)){
			errCode = 1;
			error.rejectValue("file", "message.no.file", "!");
		}
		else if(Integer.parseInt(fileSize)<=0){
			errCode = 1;
			error.rejectValue("file", "message.error.file", "!");
		}
		else if(!StringUtils.endsWith(fileName, ".xls")){
			errCode = 1;
			error.rejectValue("file", "message.error.file", "!");
		}
		
		return errCode;
	}
}
