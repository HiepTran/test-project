package com.datawings.app.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.filter.LineFilter;
import com.datawings.app.model.Line;
import com.datawings.app.service.ILineService;

@Component
public class LineValidator {

	@Autowired
	private ILineService lineService;

	public int checkLine(Object target, Errors errors) {
		int errCode = 0;
		LineFilter filter = (LineFilter) target;
		
		if(StringUtils.isBlank(filter.getOrigExt())){
			errCode = 1;
			errors.rejectValue("origExt", "message.not.empty", "!");
		}else if(filter.getOrigExt().trim().length() < 3){
			
			errors.rejectValue("origExt", "message.lenght.airport", "!");
		}else if(StringUtils.isBlank(filter.getDestExt())){
			errCode = 1;
			errors.rejectValue("destExt", "message.not.empty", "!");
		}else if(filter.getDestExt().trim().length() < 3){
			errCode = 1;
			errors.rejectValue("destExt", "message.lenght.airport", "!");
		}else if(StringUtils.isBlank(filter.getLineExt())){
			errCode = 1;
			errors.rejectValue("lineExt", "message.not.empty", "!");
		}else {
			Line line = lineService.getLine(filter.getOrigExt(), filter.getDestExt());
			if(line != null){
				errCode = 1;
				errors.rejectValue("origExt", "message.exist.line", "!");
			}
		}
		return errCode;
	}

	public int checkUpdateLine(Object target, Errors errors) {
		int errCode = 0;
		LineFilter filter = (LineFilter) target;
		
		if(StringUtils.isBlank(filter.getLineEdit())){
			errCode = 1;
			errors.rejectValue("lineEdit", "message.not.empty", "!");
		}
		return errCode;
	}
	
	
}
