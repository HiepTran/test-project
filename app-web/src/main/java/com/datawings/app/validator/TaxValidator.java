package com.datawings.app.validator;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.DoubleUtil;
import com.datawings.app.filter.TaxFilter;
import com.datawings.app.model.Tax;
import com.datawings.app.service.ITaxService;

@Component
public class TaxValidator {

	@Autowired
	private ITaxService taxService;
	
	public void checkEdit(Object target, Errors errors) {
		TaxFilter filter = (TaxFilter) target;
		
		if(StringUtils.isBlank(filter.getAmountEdit())){
			errors.rejectValue("amountEdit", "message.not.empty", "!");
		}else if(!DoubleUtil.isDouble(filter.getAmountEdit())){
			errors.rejectValue("amountEdit", "message.double.valid", "!");
		}		
	}

	public void checkAdd(Object target, Tax tax, Errors errors) {
		TaxFilter filter = (TaxFilter) target;
		
		if(StringUtils.isBlank(filter.getDateFromAdd())){
			errors.rejectValue("dateFromAdd", "message.not.empty", "!");
		}else if(!DateUtil.checkDateAsString(filter.getDateFromAdd(), "dd/MM/yyyy")){
			errors.rejectValue("dateFromAdd", "message.date.valid", "!");
		}else {
			Date dateStart = DateUtil.string2Date(filter.getDateFromAdd(), "dd/MM/yyyy");
			if(dateStart.before(tax.getDateStart()) || dateStart.after(tax.getDateEnd())){
				errors.rejectValue("dateFromAdd", "message.not.valid", "!");
			}
		}
		
		if(StringUtils.isBlank(filter.getAmountAdd())){
			errors.rejectValue("amountAdd", "message.not.empty", "!");
		}else if(!DoubleUtil.isDouble(filter.getAmountAdd())){
			errors.rejectValue("amountAdd", "message.double.valid", "!");
		}
	}

	public void checkAddModal(Object target, Errors errors) {
		TaxFilter filter = (TaxFilter) target;

		if(StringUtils.isBlank(filter.getCodeExt())){
			errors.rejectValue("codeExt", "message.not.empty", "!");
		} else if(filter.getCodeExt().trim().length() < 3){
			errors.rejectValue("codeExt", "message.not.valid", "!");
		}else if(StringUtils.isBlank(filter.getDateFromExt())){
			errors.rejectValue("dateFromExt", "message.not.empty", "!");
		}else if(!DateUtil.checkDateAsString(filter.getDateFromExt(), "dd/MM/yyyy")){
			errors.rejectValue("dateFromExt", "message.date.valid", "!");
		}else if(StringUtils.isBlank(filter.getAmountExt())){
			errors.rejectValue("amountExt", "message.not.empty", "!");
		}else if(!DoubleUtil.isDouble(filter.getAmountExt())){
			errors.rejectValue("amountExt", "message.double.valid", "!");
		}else{
			Tax tax = taxService.getTaxValidator(filter);
			if(tax != null){
				errors.rejectValue("typeExt", "message.exist.tax", "!");
			}
		}
	}

}
