package com.datawings.app.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.DoubleUtil;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.AwbTaxFilter;

@Component
public class AwbFlownValidator {

	public int checkDetail(Object target, Errors errors) {
		AwbFilter filter = (AwbFilter) target;
		Integer erroCode = 0;
		
		if(StringUtils.isBlank(filter.getDateExecute())){
			errors.rejectValue("dateExecute", "message.not.empty", "!");
			erroCode = 1;
		}else if(!DateUtil.checkDateAsString(filter.getDateExecute(), "dd/MM/yyyy")){
			errors.rejectValue("dateExecute", "message.date.valid", "!");
			erroCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getOrig())){
			errors.rejectValue("orig", "message.not.empty", "!");
			erroCode = 1;
		}else if(filter.getOrig().trim().length() < 3){
			errors.rejectValue("orig", "message.lenght.three", "!");
			erroCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getDest())){
			errors.rejectValue("dest", "message.not.empty", "!");
			erroCode = 1;
		}else if(filter.getDest().trim().length() < 3){
			errors.rejectValue("dest", "message.lenght.three", "!");
			erroCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getKm())){
			errors.rejectValue("km", "message.not.empty", "!");
			erroCode = 1;
		}else if(!DoubleUtil.isDouble(filter.getKm())){
			errors.rejectValue("km", "message.double.valid", "!");
			erroCode = 1;
		}else if(DoubleUtil.convertDouble(filter.getKm()) <= 0.0){
			errors.rejectValue("km", "message.not.valid", "!");
			erroCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getFlightNumber())){
			errors.rejectValue("flightNumber", "message.not.empty", "!");
			erroCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getFlightDate())){
			errors.rejectValue("flightDate", "message.not.empty", "!");
			erroCode = 1;
		}else if(!DateUtil.checkDateAsString(filter.getFlightDate(), "dd/MM/yyyy")){
			errors.rejectValue("flightDate", "message.date.valid", "!");
			erroCode = 1;
		}
		
		for (AwbTaxFilter elm : filter.getAwbTax()) {
			if(StringUtils.isNotBlank(elm.getCode()) && elm.getCode().length() < 3){
				errors.rejectValue("errorTax", "message.error.tax", "!");
				erroCode = 1;
				break;
			}else if (StringUtils.isNotBlank(elm.getCode()) && !StringUtils.endsWithAny(elm.getCode().toUpperCase(), new String []{"C", "A"})) {
				errors.rejectValue("errorTax", "message.error.tax", "!");
				erroCode = 1;
				break;
			}
			
			if(StringUtils.isNotBlank(elm.getCode()) && !DoubleUtil.isDouble(elm.getAmount())){
				errors.rejectValue("errorTax", "message.error.tax", "!");
				erroCode = 1;
				break;
			}
		}
		
		return erroCode;
	}

}
