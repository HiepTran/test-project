package com.datawings.app.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.RdvFilter;
import com.datawings.app.model.Agent;
import com.datawings.app.model.Rdv;
import com.datawings.app.service.IAgentService;
import com.datawings.app.service.IRdvService;

@Component
public class RdvValidator {

	@Autowired
	private IRdvService rdvService;

	@Autowired
	private IAgentService agentService;
	
	public Integer checkRdv(Object target, Errors errors) {
		RdvFilter filter = (RdvFilter) target;

		Integer erro_code = 0;
		
		if (StringUtils.isBlank(filter.getAgtn())) {
			errors.rejectValue("agtn", "message.not.empty", "!");
			erro_code = 1;
		} else if(filter.getAgtn().trim().length() < 11){
			errors.rejectValue("agtn", "message.lenght.agtn", "!");
			erro_code = 1;
		} else if (StringUtils.isBlank(filter.getEmission())) {
			errors.rejectValue("emission", "message.not.empty", "!");
			erro_code = 1;
		} else if (!DateUtil.checkDateAsString(filter.getEmission(), "dd/MM/yyyy")) {
			errors.rejectValue("emission", "message.date.valid", "!");
			erro_code = 1;
		}else {
			Rdv rdv = rdvService.getRdv(filter);
			if (rdv != null) {
				errors.rejectValue("agtn", "rdv.exist", "!");
				erro_code = 1;
			}
		}
		
		if(StringUtils.isNotBlank(filter.getAgtn()) && filter.getAgtn().trim().length() == 11){
			Agent agent = agentService.find(StringUtils.substring(filter.getAgtn(), 0, 10));
			if (agent == null) {
				errors.rejectValue("agtn", "message.no.agent", "!");
				erro_code = 1;
			}
		}
		
		return erro_code;
	}
}
