package com.datawings.app.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.IntegerUtil;
import com.datawings.app.filter.ManifestFilter;
import com.datawings.app.model.Manifest;
import com.datawings.app.service.IManifestService;

@Component()
public class ManifestValidator {

	@Autowired
	private IManifestService manifestService;
	
	public Integer checkManifest(Object target, Errors errors) {
		
		int errCode = 0;
		ManifestFilter filter = (ManifestFilter) target;
		
		if(StringUtils.isBlank(filter.getTacn())){
			errors.rejectValue("tacn", "message.not.empty", "!");
			errCode = 1;
		}else if(filter.getTacn().trim().length() < 3){
			errors.rejectValue("tacn", "message.lenght.three", "!");
			errCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getManifestNo())){
			errors.rejectValue("manifestNo", "message.not.empty", "!");
			errCode = 1;
		}else{
			Manifest maniFest = manifestService.find(filter.getManifestNo());
			if(maniFest != null){
				errors.rejectValue("manifestNo", "message.exist.manifest", "!");
				errCode = 1;
				return errCode;
			}
		}
		
		if(StringUtils.isBlank(filter.getOrig())){
			errors.rejectValue("orig", "message.not.empty", "!");
			errCode = 1;
		}else if(filter.getOrig().trim().length() < 3){
			errors.rejectValue("orig", "message.lenght.three", "!");
			errCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getDest())){
			errors.rejectValue("dest", "message.not.empty", "!");
			errCode = 1;
		}else if(filter.getDest().trim().length() < 3){
			errors.rejectValue("dest", "message.lenght.three", "!");
			errCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getCarrier())){
			errors.rejectValue("flightNumber", "message.not.valid", "!");
			errCode = 1;
		}else if (StringUtils.isBlank(filter.getFlightNumber())) {
			errors.rejectValue("flightNumber", "message.not.valid", "!");
			errCode = 1;
		}else if(filter.getCarrier().trim().length() < 2){
			errors.rejectValue("flightNumber", "message.not.valid", "!");
			errCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getFlightDate())){
			errors.rejectValue("flightDate", "message.not.empty", "!");
			errCode = 1;
		}else if(!DateUtil.checkDateAsString(filter.getFlightDate(), "dd/MM/yyyy")){
			errCode = 1;
			errors.rejectValue("flightDate", "message.date.valid", "!");
		}
		
		if(StringUtils.isNotBlank(filter.getEntryNumber()) && !IntegerUtil.isInteger(filter.getEntryNumber())){
			errors.rejectValue("entryNumber", "message.integer.valid", "!");
			errCode = 1;
		}
		
		return errCode;
	}
}