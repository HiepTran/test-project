package com.datawings.app.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.filter.LegKmFilter;
import com.datawings.app.model.LegKm;
import com.datawings.app.model.LegKmId;
import com.datawings.app.service.ILegKmService;

@Component
public class LegKmValidator {
	
	@Autowired private ILegKmService iLegKmService;
	
	public int checkValidatorCreateLegKm(Object target, Errors error){
		int errorCode = 0;
		LegKmFilter filter = (LegKmFilter)target;
		if(StringUtils.isBlank(filter.getLegKmAdd().getId().getIsocFrom())){
			error.rejectValue("legKmAdd.id.isocFrom", "message.not.empty", "!isoc from invalid");
			errorCode = 1;
		}else if(filter.getLegKmAdd().getId().getIsocFrom().length() != 2){
			error.rejectValue("legKmAdd.id.isocFrom", "message.lenght.isoc", "!isoc from invalid");
			errorCode = 1;
		}else if(StringUtils.isNotBlank(filter.getLegKmAdd().getId().getIsocFrom())
				&& StringUtils.isNotBlank(filter.getLegKmAdd().getId().getAirportFrom())
				&& StringUtils.isNotBlank(filter.getLegKmAdd().getId().getIsocTo())
				&& StringUtils.isNotBlank(filter.getLegKmAdd().getId().getAirportTo())){
			LegKmId id = new LegKmId();
			id.setIsocFrom(filter.getLegKmAdd().getId().getIsocFrom().toUpperCase()); id.setAirportFrom(filter.getLegKmAdd().getId().getAirportFrom().toUpperCase()); 
			id.setIsocTo(filter.getLegKmAdd().getId().getIsocTo().toUpperCase()); id.setAirportTo(filter.getLegKmAdd().getId().getAirportTo().toUpperCase());
			LegKm legKmNew = iLegKmService.find(id);
			if(legKmNew != null){
				error.rejectValue("legKmAdd.id.isocFrom", "message.exist.legkm", "!legkm already exists");
				errorCode = 1;
			}
		}
		
		if(StringUtils.isBlank(filter.getLegKmAdd().getId().getAirportFrom())){
			error.rejectValue("legKmAdd.id.airportFrom", "message.not.empty", "!airport from invalid");
			errorCode = 1;
		}else if(filter.getLegKmAdd().getId().getAirportFrom().length() != 3){
			error.rejectValue("legKmAdd.id.airportFrom", "message.lenght.airport", "!airport from invalid");
			errorCode = 1;
		}else if(StringUtils.equals(filter.getLegKmAdd().getId().getAirportFrom(), filter.getLegKmAdd().getId().getAirportTo())){
			error.rejectValue("legKmAdd.id.airportFrom", "message.error.legkm", "!invalid airport from, airport to");
			errorCode = 1;
		}
		if(StringUtils.isBlank(filter.getLegKmAdd().getId().getIsocTo())){
			error.rejectValue("legKmAdd.id.isocTo", "message.not.empty", "!isoc to invalid");
			errorCode = 1;
		}else if(filter.getLegKmAdd().getId().getIsocTo().length() != 2){
			error.rejectValue("legKmAdd.id.isocTo", "message.lenght.isoc", "!isoc to invalid");
			errorCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getLegKmAdd().getId().getAirportTo())){
			error.rejectValue("legKmAdd.id.airportTo", "message.not.empty", "!isoc from invalid");
			errorCode = 1;
		}else if(filter.getLegKmAdd().getId().getAirportTo().length() != 3){
			error.rejectValue("legKmAdd.id.airportTo", "message.lenght.airport", "!isoc from invalid");
			errorCode = 1;
		}
		
		if(filter.getLegKmAdd().getKm()<=0){
			error.rejectValue("legKmAdd.km", "message.double.valid", "!km invalid");
			errorCode = 1;
		}
		if(filter.getLegKmAdd().getMile()<=0){
			error.rejectValue("legKmAdd.mile", "message.double.valid", "!mile invalid");
			errorCode = 1;
		}
		return errorCode;
	}
	
	public int checkValidatorEditLegKm(Object target, Errors error){
		int errCode = 0;
		LegKmFilter filter = (LegKmFilter) target;
		
		if(filter.getLegKmEdit().getKm()<=0){
			error.rejectValue("km", "message.double.valid", "!Km invalid");
			errCode = 1;
		}
		if(filter.getLegKmEdit().getMile()<=0){
			error.rejectValue("mile", "message.double.valid", "!Mile invalid");
			errCode = 1;
		}
		return errCode;
	}
}
