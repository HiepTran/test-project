package com.datawings.app.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.common.IntegerUtil;
import com.datawings.app.filter.StockFilter;
import com.datawings.app.model.StockCentral;

@Component
public class StockCentralValidator {

	public void checkStockCentral(Object target, Errors errors) {
		StockFilter filter = (StockFilter) target;
		
		if(StringUtils.isBlank(filter.getSerialFrom())){
			errors.rejectValue("serialFrom", "message.not.empty", "!");
		}else if (filter.getSerialFrom().trim().length() < 7) {
			errors.rejectValue("serialFrom", "message.lenght.seven", "!");
		}else if(!IntegerUtil.isInteger(filter.getSerialFrom())){
			errors.rejectValue("serialFrom", "message.integer.valid", "!");
		}
		
		if(StringUtils.isBlank(filter.getQuantity())){
			errors.rejectValue("quantity", "message.not.empty", "!");
		}else if(!IntegerUtil.isInteger(filter.getQuantity())){
			errors.rejectValue("quantity", "message.integer.valid", "!");
		}
		
	}

	public void checkStockMove(StockCentral stockCentral, Object target, Errors errors) {
		StockFilter filter = (StockFilter) target;
		
		if(StringUtils.isBlank(filter.getAgtn())){
			errors.rejectValue("agtn", "message.not.empty", "!");
		}else if(filter.getAgtn().trim().length() < 11){
			errors.rejectValue("agtn", "message.lenght.agtn", "!");
		}
		
		if(StringUtils.isBlank(filter.getQuantity())){
			errors.rejectValue("quantity", "message.not.empty", "!");
		}else if(!IntegerUtil.isInteger(filter.getQuantity())){
			errors.rejectValue("quantity", "message.integer.valid", "!");
		}else if(stockCentral.getQuantity() < Integer.parseInt(filter.getQuantity())){
			errors.rejectValue("quantity", "stock.compagnie.quantity.bigger", "!");
		}
	}

}
