package com.datawings.app.validator;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.AgentGsaFilter;
import com.datawings.app.model.AgentGsa;
import com.datawings.app.service.IAgentGsaService;

@Component
public class AgentGsaValidator {
	@Autowired 
	IAgentGsaService agentGsaService;
	
	/**
	 * method check validaror create agent gsa
	 * @param target the current Object 
	 * @param error the Errors reject when have error
	 * @return 1 if hava error or 0 if don't have error
	 */
	public int doCreateAgentGsaValidator(Object target, Errors error){
		AgentGsaFilter filter = (AgentGsaFilter) target;
		int errCode = 0;
		
		if(StringUtils.isBlank(filter.getCodeAdd())){
			errCode = 1;
			error.rejectValue("codeAdd", "message.not.empty", "!empty");
		}
		else if(filter.getCodeAdd().trim().length() != 7){
			errCode = 1;
			error.rejectValue("codeAdd", "message.lenght.seven", "!Code length must have 7 characters");
		}
		else if(!StringUtils.isNumeric(filter.getCodeAdd())){
			errCode = 1;
			error.rejectValue("codeAdd", "message.integer.valid", "!Code must be number");
		}
		else{
			List<AgentGsa> lstAgentGsa = agentGsaService.findAll();
			for(AgentGsa agentGsa : lstAgentGsa){
				if(StringUtils.equals(agentGsa.getCode(), filter.getCodeAdd())){
					errCode = 1;
					error.rejectValue("codeAdd", "message.exist.agentgsa", "!already exists");
				}
			}
		}
		
		if(filter.getCommAdd() <= 0){
			errCode = 1;
			error.rejectValue("commAdd", "message.double.valid", "!not valid");
		}
		
		if(StringUtils.isBlank(filter.getDateStartAdd())){
			errCode = 1;
			error.rejectValue("dateStartAdd", "message.not.empty", "!empty");
		}
		else if(!DateUtil.checkDateAsString(filter.getDateStartAdd(), "dd/MM/yyyy")){
			errCode = 1;
			error.rejectValue("dateStartAdd", "message.date.valid", "!not match");
		}
		else{
			Date dateStart = DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy");
			Date dateEnd = DateUtil.string2Date(filter.getDateEndAdd(), "dd/MM/yyyy");
			if(dateEnd != null && dateStart.after(dateEnd)){
				errCode = 1;
				error.rejectValue("dateStartAdd", "message.date.valid", "!not match");
			}
		}
		return errCode;
	}
	
	/**
	 *  method check validator when add detail agent gsa
	 *  @param target the current Object
	 *  @param error the Errors reject when hava error
	 *  @return 1 if have error or 0 if don't have error
	 */
	public int doValidatorAddDetail(Object target, Errors error){
		int errCode = 0;
		AgentGsaFilter filter = (AgentGsaFilter)target;
		if(StringUtils.isBlank(filter.getAgtnAddDetail())){
			errCode = 1;
			error.rejectValue("agtnAddDetail", "message.not.empty", "!empty");
		}
		else if(filter.getAgtnAddDetail().trim().length() != 10){
			errCode = 1;
			error.rejectValue("agtnAddDetail", "message.lenght.ten", "!Agtn length must have 10 characters");
		}
//		else if(!IntegerUtil.isInteger(filter.getAgtnAddDetail().trim())){
//			errCode = 1;
//			error.rejectValue("agtnAddDetail", "message.integer.valid", "!not valid");
//		}
		else{
			List<AgentGsa> lstAgentGsa = agentGsaService.findAll();
			for(AgentGsa agentGsa : lstAgentGsa){
				if(StringUtils.equals(agentGsa.getAgtn(), filter.getAgtnAddDetail())){
					errCode = 1;
					error.rejectValue("agtnAddDetail", "message.exist.agtn", "!already exists");
				}
			}
		}
		
		return errCode;
	}
}
