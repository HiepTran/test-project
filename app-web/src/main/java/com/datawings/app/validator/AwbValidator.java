package com.datawings.app.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.DoubleUtil;
import com.datawings.app.common.IntegerUtil;
import com.datawings.app.filter.AwbDetailFilter;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.AwbTaxFilter;
import com.datawings.app.model.Agent;
import com.datawings.app.model.Awb;
import com.datawings.app.model.Bsr;
import com.datawings.app.service.IAgentService;
import com.datawings.app.service.IAirlineParamService;
import com.datawings.app.service.IAwbService;
import com.datawings.app.service.IBsrService;

@Component("AwbValidator")
public class AwbValidator {

	@Autowired
	private IAwbService awbService;
	
	@Autowired
	private IAgentService agentService;
	
	@Autowired
	private IAirlineParamService airlineParamService;
	
	@Autowired
	private IBsrService bsrService;
	
	public Integer checkLTA(Object target, Errors errors) {
		AwbFilter filter = (AwbFilter) target;
			
		int errCode = 0;
		
		if(StringUtils.isBlank(filter.getLta())){
			errors.rejectValue("lta", "message.not.empty", "!");
			errCode = 1;
		}else if(filter.getLta().trim().length() < 8){
			errors.rejectValue("lta", "message.lenght.lta", "!");
			errCode = 1;
		}else{
			Awb awb = awbService.find(filter.getLta());
			if(awb != null){
				errors.rejectValue("lta", "message.exist.lta", "!");
				errCode = 1;
				return errCode;
			}
		}
		
		if(StringUtils.isBlank(filter.getTacn())){
			errors.rejectValue("tacn", "message.not.empty", "!");
			errCode = 1;
		}else if(filter.getTacn().trim().length() < 3){
			errors.rejectValue("tacn", "message.lenght.three", "!");
			errCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getAgtn())){
			errors.rejectValue("agtn", "message.not.empty", "!");
			errCode = 1;
		}else if(filter.getAgtn().trim().length() < 11){
			errors.rejectValue("agtn", "message.lenght.agtn", "!");
			errCode = 1;
		}else {
			Agent agent = agentService.find(StringUtils.substring(filter.getAgtn(), 0, 10));
			if(agent == null){
				errors.rejectValue("agtn", "message.no.agent", "!");
				errCode = 1;
			}
		}
		
		if(StringUtils.isBlank(filter.getDateExecute())){
			errors.rejectValue("dateExecute", "message.not.empty", "!");
			errCode = 1;
		}else if(!DateUtil.checkDateAsString(filter.getDateExecute(), "dd/MM/yyyy")){
			errors.rejectValue("dateExecute", "message.date.valid", "!");
			errCode = 1;
		}
		else{
			String currency = airlineParamService.findOne().getCurrency();
			Bsr bsrs = bsrService.findBsrByCutpAndDateExt(filter.getCutp(), currency, filter.getDateExecute());
			if(bsrs == null){
				errors.rejectValue("cutp", "message.error.currency", "!");
				errCode = 1;
			}
		}
		
		if(StringUtils.isBlank(filter.getShipName())){
			errors.rejectValue("shipName", "message.not.empty", "!");
			errCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getConsigneeName())){
			errors.rejectValue("consigneeName", "message.not.empty", "!");
			errCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getOrig())){
			errors.rejectValue("orig", "message.not.empty", "!");
			errCode = 1;
		}else if(filter.getOrig().trim().length() < 3){
			errors.rejectValue("orig", "message.lenght.three", "!");
			errCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getDest())){
			errors.rejectValue("dest", "message.not.empty", "!");
			errCode = 1;
		}else if(filter.getDest().trim().length() < 3){
			errors.rejectValue("dest", "message.lenght.three", "!");
			errCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getCarrier())){
			errors.rejectValue("flightNumber", "message.not.valid", "!");
			errCode = 1;
		}else if (StringUtils.isBlank(filter.getFlightNumber())) {
			errors.rejectValue("flightNumber", "message.not.valid", "!");
			errCode = 1;
		}else if(filter.getCarrier().trim().length() < 2){
			errors.rejectValue("flightNumber", "message.not.valid", "!");
			errCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getFlightDate())){
			errors.rejectValue("flightDate", "message.not.empty", "!");
			errCode = 1;
		}else if(!DateUtil.checkDateAsString(filter.getFlightDate(), "dd/MM/yyyy")){
			errors.rejectValue("flightDate", "message.date.valid", "!");
			errCode = 1;
		}
		
		
		//comm
		if(StringUtils.isNotBlank(filter.getCommPercent())){
			if(!DoubleUtil.isDouble(filter.getCommPercent())){
				errors.rejectValue("commPercent", "message.double.valid", "!");
				errCode = 1;
			}
		}
		//vat
		if(StringUtils.isNotBlank(filter.getVatPercent())){
			if(!DoubleUtil.isDouble(filter.getVatPercent())){
				errors.rejectValue("vatPercent", "message.double.valid", "!");
				errCode = 1;
			}
		}
		
		//Tax
		if(StringUtils.isNotBlank(filter.getTaxCode1())){
			if(filter.getTaxCode1().trim().length() < 3){
				errors.rejectValue("taxCode1", "message.lenght.three", "!");
				errCode = 1;
			}else if(!DoubleUtil.isDouble(filter.getTaxValue1())){
				errors.rejectValue("taxValue1", "message.double.valid", "!");
				errCode = 1;
			}
		}
		if(StringUtils.isNotBlank(filter.getTaxCode2())){
			if(filter.getTaxCode2().trim().length() < 3){
				errors.rejectValue("taxCode2", "message.lenght.three", "!");
				errCode = 1;
			}else if(!DoubleUtil.isDouble(filter.getTaxValue2())){
				errors.rejectValue("taxValue2", "message.double.valid", "!");
				errCode = 1;
			}
		}
		if(StringUtils.isNotBlank(filter.getTaxCode3())){
			if(filter.getTaxCode3().trim().length() < 3){
				errors.rejectValue("taxCode3", "message.lenght.three", "!");
				errCode = 1;
			}else if(!DoubleUtil.isDouble(filter.getTaxValue3())){
				errors.rejectValue("taxValue3", "message.double.valid", "!");
				errCode = 1;
			}
		}
		if(StringUtils.isNotBlank(filter.getTaxCode4())){
			if(filter.getTaxCode4().trim().length() < 3){
				errors.rejectValue("taxCode4", "message.lenght.three", "!");
				errCode = 1;
			}else if(!DoubleUtil.isDouble(filter.getTaxValue4())){
				errors.rejectValue("taxValue4", "message.double.valid", "!");
				errCode = 1;
			}
		}
		if(StringUtils.isNotBlank(filter.getTaxCode5())){
			if(filter.getTaxCode5().trim().length() < 3){
				errors.rejectValue("taxCode5", "message.lenght.three", "!");
				errCode = 1;
			}else if(!DoubleUtil.isDouble(filter.getTaxValue5())){
				errors.rejectValue("taxValue5", "message.double.valid", "!");
				errCode = 1;
			}
		}
		if(StringUtils.isNotBlank(filter.getTaxCode6())){
			if(filter.getTaxCode6().trim().length() < 3){
				errors.rejectValue("taxCode6", "message.lenght.three", "!");
				errCode = 1;
			}else if(!DoubleUtil.isDouble(filter.getTaxValue6())){
				errors.rejectValue("taxValue6", "message.double.valid", "!");
				errCode = 1;
			}
		}
		if(StringUtils.isNotBlank(filter.getTaxCode7())){
			if(filter.getTaxCode7().trim().length() < 3){
				errors.rejectValue("taxCode7", "message.lenght.three", "!");
				errCode = 1;
			}else if(!DoubleUtil.isDouble(filter.getTaxValue7())){
				errors.rejectValue("taxValue7", "message.double.valid", "!");
				errCode = 1;
			}
		}
		if(StringUtils.isNotBlank(filter.getTaxCode8())){
			if(filter.getTaxCode8().trim().length() < 3){
				errors.rejectValue("taxCode8", "message.lenght.three", "!");
				errCode = 1;
			}else if(!DoubleUtil.isDouble(filter.getTaxValue8())){
				errors.rejectValue("taxValue8", "message.double.valid", "!");
				errCode = 1;
			}
		}
		
		//Detail
		if(StringUtils.isNotBlank(filter.getQuantity1())){
			if(!IntegerUtil.isInteger(filter.getQuantity1())){
				errors.rejectValue("quantity1", "message.integer.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getWeightGross1())){
				errors.rejectValue("weightGross1", "message.double.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getWeightCharge1())){
				errors.rejectValue("weightCharge1", "message.double.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getUnit1())){
				errors.rejectValue("unit1", "message.double.valid", "!");
				errCode = 1;
			}
		}
		
		if(StringUtils.isNotBlank(filter.getQuantity2())){
			if(!IntegerUtil.isInteger(filter.getQuantity2())){
				errors.rejectValue("quantity2", "message.integer.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getWeightGross2())){
				errors.rejectValue("weightGross2", "message.double.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getWeightCharge2())){
				errors.rejectValue("weightCharge2", "message.double.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getUnit2())){
				errors.rejectValue("unit2", "message.double.valid", "!");
				errCode = 1;
			}
		}
		
		if(StringUtils.isNotBlank(filter.getQuantity3())){
			if(!IntegerUtil.isInteger(filter.getQuantity3())){
				errors.rejectValue("quantity3", "message.integer.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getWeightGross3())){
				errors.rejectValue("weightGross3", "message.double.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getWeightCharge3())){
				errors.rejectValue("weightCharge3", "message.double.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getUnit3())){
				errors.rejectValue("unit3", "message.double.valid", "!");
				errCode = 1;
			}
		}
		
		if(StringUtils.isNotBlank(filter.getQuantity4())){
			if(!IntegerUtil.isInteger(filter.getQuantity4())){
				errors.rejectValue("quantity4", "message.integer.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getWeightGross4())){
				errors.rejectValue("weightGross4", "message.double.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getWeightCharge4())){
				errors.rejectValue("weightCharge4", "message.double.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getUnit4())){
				errors.rejectValue("unit4", "message.double.valid", "!");
				errCode = 1;
			}
		}
		
		if(StringUtils.isNotBlank(filter.getQuantity5())){
			if(!IntegerUtil.isInteger(filter.getQuantity5())){
				errors.rejectValue("quantity5", "message.integer.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getWeightGross5())){
				errors.rejectValue("weightGross5", "message.double.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getWeightCharge5())){
				errors.rejectValue("weightCharge5", "message.double.valid", "!");
				errCode = 1;
			}
			if(!DoubleUtil.isDouble(filter.getUnit5())){
				errors.rejectValue("unit5", "message.double.valid", "!");
				errCode = 1;
			}
		}
		
		if(StringUtils.equals(filter.getModePayment(), "CC")){
			if(StringUtils.isBlank(filter.getNbCart())){
				errors.rejectValue("nbCart", "message.not.empty", "!");
				errCode = 1;
			}
			
			if(StringUtils.isBlank(filter.getDateExp())){
				errors.rejectValue("dateExp", "message.not.empty", "!");
				errCode = 1;
			}else if(!DateUtil.checkDateAsString(filter.getDateExp(), "dd/MM/yyyy")){
				errors.rejectValue("dateExp", "message.date.valid", "!");
				errCode = 1;
			}
			
			if(StringUtils.isBlank(filter.getNameCart())){
				errors.rejectValue("nameCart", "message.not.empty", "!");
				errCode = 1;
			}
		}
		
		return errCode;
	}

	public int checkDetail(Object target, Errors errors) {
		AwbFilter filter = (AwbFilter) target;
		Integer erroCode = 0;
		if(StringUtils.isBlank(filter.getIsoc())){
			errors.rejectValue("isoc", "message.not.empty", "!");
			erroCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getDateExecute())){
			errors.rejectValue("dateExecute", "message.not.empty", "!");
			erroCode = 1;
		}else if(!DateUtil.checkDateAsString(filter.getDateExecute(), "dd/MM/yyyy")){
			errors.rejectValue("dateExecute", "message.date.valid", "!");
			erroCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getOrig())){
			errors.rejectValue("orig", "message.not.empty", "!");
			erroCode = 1;
		}else if(filter.getOrig().trim().length() < 3){
			errors.rejectValue("orig", "message.lenght.three", "!");
			erroCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getDest())){
			errors.rejectValue("dest", "message.not.empty", "!");
			erroCode = 1;
		}else if(filter.getDest().trim().length() < 3){
			errors.rejectValue("dest", "message.lenght.three", "!");
			erroCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getKm())){
			errors.rejectValue("km", "message.not.empty", "!");
			erroCode = 1;
		}else if(!DoubleUtil.isDouble(filter.getKm())){
			errors.rejectValue("km", "message.double.valid", "!");
			erroCode = 1;
		}else if(DoubleUtil.convertDouble(filter.getKm()) <= 0.0){
			errors.rejectValue("km", "message.not.valid", "!");
			erroCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getFlightNumber())){
			errors.rejectValue("flightNumber", "message.not.empty", "!");
			erroCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getFlightDate())){
			errors.rejectValue("flightDate", "message.not.empty", "!");
			erroCode = 1;
		}else if(!DateUtil.checkDateAsString(filter.getFlightDate(), "dd/MM/yyyy")){
			errors.rejectValue("flightDate", "message.date.valid", "!");
			erroCode = 1;
		}
		
		if(StringUtils.isBlank(filter.getCommPercent())){
			errors.rejectValue("commPercent", "message.not.empty", "!");
			erroCode = 1;
		}else if (!DoubleUtil.isDouble(filter.getCommPercent())) {
			errors.rejectValue("commPercent", "message.double.valid", "!");
			erroCode = 1;
		}
		
		if(StringUtils.isNotBlank(filter.getVatPercent())){
			if(!DoubleUtil.isDouble(filter.getVatPercent())){
				errors.rejectValue("vatPercent", "message.double.valid", "!");
				erroCode = 1;
			}
		}
		
		if(StringUtils.equals(filter.getModePayment(), "CC")){
			if(StringUtils.isBlank(filter.getNbCart())){
				errors.rejectValue("nbCart", "message.not.empty", "!");
				erroCode = 1;
			}
			
			if(StringUtils.isBlank(filter.getDateExp())){
				errors.rejectValue("dateExp", "message.not.empty", "!");
				erroCode = 1;
			}else if(!DateUtil.checkDateAsString(filter.getDateExp(), "dd/MM/yyyy")){
				errors.rejectValue("dateExp", "message.date.valid", "!");
				erroCode = 1;
			}
			
			if(StringUtils.isBlank(filter.getNameCart())){
				errors.rejectValue("nameCart", "message.not.empty", "!");
				erroCode = 1;
			}
		}
		
		for (AwbDetailFilter elm : filter.getAwbDetail()) {
			if(!IntegerUtil.isInteger(elm.getQuantity())){
				errors.rejectValue("errorDetail", "message.error.detail", "!");
				erroCode = 1;
				break;
			}
			if(!DoubleUtil.isDouble(elm.getWeightGross())){
				errors.rejectValue("errorDetail", "message.error.detail", "!");
				erroCode = 1;
				break;
			}
			if(!DoubleUtil.isDouble(elm.getWeightCharge())){
				errors.rejectValue("errorDetail", "message.error.detail", "!");
				erroCode = 1;
				break;
			}
			if(!DoubleUtil.isDouble(elm.getUnit())){
				errors.rejectValue("errorDetail", "message.error.detail", "!");
				erroCode = 1;
				break;
			}
		}
		return erroCode;
	}

	public int checkDetailTax(String id, Object target, Errors errors) {
		AwbFilter filter = (AwbFilter) target;
		Integer erroCode = 0;
		
		for (AwbTaxFilter elm : filter.getAwbTax()) {
			if(StringUtils.isNotBlank(elm.getCode()) && elm.getCode().length() < 3){
				errors.rejectValue("errorTax", "message.error.tax", "!");
				erroCode = 1;
				break;
			}else if (StringUtils.isNotBlank(elm.getCode()) && !StringUtils.endsWithAny(elm.getCode().toUpperCase(), new String []{"C", "A"})) {
				errors.rejectValue("errorTax", "message.error.tax", "!");
				erroCode = 1;
				break;
			}
			
			if(StringUtils.isNotBlank(elm.getCode()) && !DoubleUtil.isDouble(elm.getAmount())){
				errors.rejectValue("errorTax", "message.error.tax", "!");
				erroCode = 1;
				break;
			}
		}
		
		//Case total tax awb diff 0.0, check total filter tax diff 0.0 
		Awb awb = awbService.find(id);
		Double totalTax = awb.getAgentPp() + awb.getAgentCc() + awb.getCarrierPp() + awb.getCarrierCc();
		if(totalTax.compareTo(0.0) !=0){
			Double taxAmount = 0.0;
			for(AwbTaxFilter elm : filter.getAwbTax()) {
				int retval =  DoubleUtil.convertDouble(elm.getAmount()).compareTo(0.0);
				if(elm.getCode().length() == 3 &&  retval != 0 ){
					taxAmount += DoubleUtil.round(DoubleUtil.convertDouble(elm.getAmount()), 2);
				}
			}
			
			if(taxAmount == 0.0){
				errors.rejectValue("errorTax", "message.error.tax", "!");
				erroCode = 2;
			}
		}
		return erroCode;
	}
}
