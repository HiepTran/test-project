package com.datawings.app.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.common.IntegerUtil;
import com.datawings.app.filter.StockFilter;
import com.datawings.app.model.Agent;
import com.datawings.app.model.StockAgent;
import com.datawings.app.service.IAgentService;

@Component
public class StockAgentValidator {

	@Autowired
	private IAgentService agentService;
	
	public void checkMoveAgent(StockAgent stockAgent, Object target, Errors errors) {
		StockFilter filter = (StockFilter) target;
		
		if(StringUtils.isBlank(filter.getAgtn())){
			errors.rejectValue("agtn", "message.not.empty", "!");
		}else if(filter.getAgtn().trim().length() < 11){
			errors.rejectValue("agtn", "message.lenght.agtn", "!");
		}else{
			Agent agent = agentService.find(StringUtils.substring(filter.getAgtn(), 0, 10));
			if(agent == null){
				errors.rejectValue("agtn", "message.no.agent", "!");
			}
		}
		
		if(StringUtils.isBlank(filter.getSerialFrom())){
			errors.rejectValue("serialFrom", "message.not.empty", "!");
		}else if (filter.getSerialFrom().trim().length() < 7) {
			errors.rejectValue("serialFrom", "message.lenght.seven", "!");
		}else if(!IntegerUtil.isInteger(filter.getSerialFrom())){
			errors.rejectValue("serialFrom", "message.integer.valid", "!");
		}else if(IntegerUtil.convertInteger(filter.getSerialFrom()) < stockAgent.getSerialFrom() || IntegerUtil.convertInteger(filter.getSerialFrom()) > stockAgent.getSerialTo()){
			errors.rejectValue("serialFrom", "message.integer.valid", "!");
		}
		
		if(StringUtils.isBlank(filter.getQuantity())){
			errors.rejectValue("quantity", "message.not.empty", "!");
		}else if(!IntegerUtil.isInteger(filter.getQuantity())){
			errors.rejectValue("quantity", "message.integer.valid", "!");
		}else if(stockAgent.getQuantity() < Integer.parseInt(filter.getQuantity())){
			errors.rejectValue("quantity", "stock.compagnie.quantity.bigger", "!");
		}else if((Integer.parseInt(filter.getSerialFrom()) + Integer.parseInt(filter.getQuantity()) - 1)  > stockAgent.getSerialTo()){
			errors.rejectValue("quantity", "stock.compagnie.quantity.bigger", "!");
		}
	}

}
