package com.datawings.app.validator;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.BsrFilter;
import com.datawings.app.model.Bsr;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.service.IBsrService;

@Component
public class BsrValidator {
	
	@Autowired
	private IBsrService bsrService;
	
	/**
	 * method check validator create bsr
	 * @param target the Current Object (BsrFilter)
	 * @param error the Errors reject when have error
	 * @return 1 if have error or 0 if don't have error
	 *
	 */
	public int doValidatorCreateBsr(Object target, Errors error){

		int errCode = 0;
		BsrFilter filter = (BsrFilter)target;
		if(StringUtils.isBlank(filter.getCurrFromAdd())){
			errCode = 1;
			error.rejectValue("currFromAdd", "message.not.empty", "!error");
		}
		else if(filter.getCurrFromAdd().trim().length() != 3){
			errCode = 1;
			error.rejectValue("currFromAdd", "message.lenght.three", "!error");
		}
		else if(StringUtils.isBlank(filter.getCurrToAdd())){
			errCode = 1;
			error.rejectValue("currToAdd", "message.not.empty", "!error");
		}
		else if(filter.getCurrToAdd().trim().length() != 3){
			errCode = 1;
			error.rejectValue("currToAdd", "message.lenght.three", "!error");
		}
		else if(StringUtils.equals(filter.getCurrFromAdd(), filter.getCurrToAdd())){
			errCode = 1;
			error.rejectValue("currFromAdd", "message.error.bsr", "!error");
		}
		else if(StringUtils.isBlank(filter.getRateAdd().toString())){
			errCode = 1;
			error.rejectValue("rateAdd", "message.not.empty", "!error");
		}
		else if(filter.getRateAdd()<=0){
			errCode = 1;
			error.rejectValue("rateAdd", "message.double.valid", "!error");
		}
		else if(StringUtils.isBlank(filter.getDateStartAdd())){
			errCode = 1;
			error.rejectValue("dateStartAdd", "message.not.empty", "!error");
		}
		else if(!DateUtil.checkDateAsString(filter.getDateStartAdd(), "dd/MM/yyyy")){
			errCode = 1;
			error.rejectValue("dateStartAdd", "message.date.valid", "!error");
		}
		else{
			List<Bsr> lstBsrs = bsrService.getBsrFilter(filter);
			if(lstBsrs.size() > 1){
				Bsr bsrLast = lstBsrs.get(lstBsrs.size() - 1);
				if(DateUtil.checAfter(bsrLast.getBsrId().getDateStart(), DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"))
						&& DateUtil.checAfter(DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"), bsrLast.getDateEnd())
						&& !DateUtil.equalByDate(bsrLast.getDateEnd(), DateUtil.string2Date(CargoUtils.MAX_DATE, "dd/MM/yyyy"))){
					errCode = 1;
					error.rejectValue("dateStartAdd", "message.exist.day", "!error");
				}
				else if(DateUtil.equalByDate(bsrLast.getDateEnd(), DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"))){
					errCode = 1;
					error.rejectValue("dateStartAdd", "message.exist.day", "!error");
				}
				else{
					lstBsrs.remove(bsrLast);
					if(lstBsrs.size() != 0){
						Bsr startBsr = lstBsrs.get(0);
						Bsr endBsr = lstBsrs.get(lstBsrs.size() - 1);
						
						if(DateUtil.checAfter(startBsr.getBsrId().getDateStart(), DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"))
								&& DateUtil.checAfter(DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"), endBsr.getDateEnd())){
							errCode = 1;
							error.rejectValue("dateStartAdd", "message.exist.day", "!error");
						}
						else{
							Date dateAgo = DateUtils.addDays(startBsr.getBsrId().getDateStart(), -1);
							filter.setDateEndAdd(DateUtil.date2String(dateAgo, "dd/MM/yyyy"));
						}
					}
				}
				
				// update date end of last Bsr to date start add - 1
				if(errCode == 0 && DateUtil.checAfter(bsrLast.getBsrId().getDateStart(), DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"))){
					Date dateAgo = DateUtils.addDays(DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"), -1);
					bsrLast.setDateEnd(dateAgo);
					bsrService.merge(bsrLast);
				}
			}
			else if(lstBsrs.size() == 1){
				Bsr bsrLast = lstBsrs.get(lstBsrs.size() - 1);
				if(DateUtil.checAfter(bsrLast.getBsrId().getDateStart(), DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"))
						&& DateUtil.checAfter(DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"), bsrLast.getDateEnd())
						&& !DateUtil.equalByDate(bsrLast.getDateEnd(), DateUtil.string2Date(CargoUtils.MAX_DATE, "dd/MM/yyyy"))){
					errCode = 1;
					error.rejectValue("dateStartAdd", "message.exist.day", "!error");
				}
				else if(DateUtil.equalByDate(bsrLast.getBsrId().getDateStart(), DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"))){
					errCode = 1;
					error.rejectValue("dateStartAdd", "message.exist.day", "!error");
				}
				if(errCode == 0 && bsrLast.getBsrId().getDateStart().before(DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"))){
					Date dateAgo = DateUtils.addDays(DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"), -1);
					bsrLast.setDateEnd(dateAgo);
					bsrService.merge(bsrLast);
				}
				else if(errCode == 0 && DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy").before(bsrLast.getBsrId().getDateStart())){
					Date dateAgo = DateUtils.addDays(bsrLast.getBsrId().getDateStart(), -1);
					filter.setDateEndAdd(DateUtil.date2String(dateAgo, "dd/MM/yyyy"));
				}
			}
			else if(lstBsrs.size() == 0){
				List<Bsr> lst = bsrService.getBsrFilterByFromTo(filter);
				if(lst.size() > 0){
					Bsr bsrLast = lst.get(lst.size() - 1);
					if(DateUtil.checAfter(bsrLast.getBsrId().getDateStart(), DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"))
							&& DateUtil.checAfter(DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"), bsrLast.getDateEnd())){
						errCode = 1;
						error.rejectValue("dateStartAdd", "message.exist.day", "!error");
					}
					if(errCode == 0 && DateUtil.checAfter(bsrLast.getBsrId().getDateStart(), DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"))){
						Date dateAgo = DateUtils.addDays(DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy"), -1);
						bsrLast.setDateEnd(dateAgo);
						bsrService.merge(bsrLast);
					}
				}
			}
		}
		
		return errCode;
	}
	
	public int doValidatorTable(Object target, Errors error){
		int errCode = 0;
		BsrFilter filter = (BsrFilter) target;
		if(StringUtils.isBlank(filter.getCurrFrom())&&StringUtils.isBlank(filter.getCurrTo())
				&&StringUtils.isBlank(filter.getDateStart())&&StringUtils.isBlank(filter.getDateEnd())){
			errCode = 1;
			error.rejectValue("table", "message.no.criteria","!");
		}
		return errCode;
	}
	
	public int doValidatorLoad(Object target, Errors error){
		int errCode = 0;
		BsrFilter filter = (BsrFilter) target;
		if(StringUtils.isBlank(filter.getCurrFrom())&&StringUtils.isBlank(filter.getCurrTo())
				&&StringUtils.isBlank(filter.getDateStart())&&StringUtils.isBlank(filter.getDateEnd())){
			errCode = 1;
		}
		return errCode;
	}
}
