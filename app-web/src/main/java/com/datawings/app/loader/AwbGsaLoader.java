package com.datawings.app.loader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jxl.Cell;
import jxl.CellType;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.AwbGSAMapping;
import com.datawings.app.model.Awb;
import com.datawings.app.model.AwbDetail;
import com.datawings.app.model.AwbDetailId;
import com.datawings.app.model.AwbTax;
import com.datawings.app.model.AwbTaxId;
import com.datawings.app.model.SysUser;

public class AwbGsaLoader {
	
	private static final Log log = LogFactory.getLog(AwbGsaLoader.class);

	public List<AwbGSAMapping> preDataAwbGsa(CommonsMultipartFile file) throws BiffException, IOException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		
		List<AwbGSAMapping> dataList = new ArrayList<AwbGSAMapping>();
		
		dataList = readFileXLS(file);
		
		return dataList;
	}

	private List<AwbGSAMapping> readFileXLS(CommonsMultipartFile file) throws BiffException, IOException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		WorkbookSettings workbookSettings = new WorkbookSettings();
		workbookSettings.setEncoding("Cp1252");
		Workbook wb = Workbook.getWorkbook(file.getInputStream(), workbookSettings);		
		Sheet sheet = wb.getSheet(0);
		
		List<AwbGSAMapping> rs = new ArrayList<AwbGSAMapping>();
		
		if(sheet != null){
			int rows = sheet.getRows();
			int cols = sheet.getColumns();
			String formatDate = "dd/MM/yyyy";	
			Field[] fields = AwbGSAMapping.class.getDeclaredFields();	
			
			for (int row = 1; row < rows; row++) {
				String code = sheet.getCell(0, row).getContents();
				boolean chk = StringUtils.equals(code, "040");
				
				if(chk){
					AwbGSAMapping elm = new AwbGSAMapping();
					for (int col = 0; col < cols; col++) {
						try {
							String name = fields[col].getName();
							Class<?> t = PropertyUtils.getPropertyType(elm, name);
							if ("String".equalsIgnoreCase(t.getSimpleName())) {
								PropertyUtils.setSimpleProperty(elm, name, sheet.getCell(col, row).getContents());
							}else if ("Double".equalsIgnoreCase(t.getSimpleName())) {
								double chaine = 0;						
								Cell num = sheet.getCell(col, row);
								if (num.getType() == CellType.NUMBER) {
								  NumberCell nc = (NumberCell) num;
								  chaine = nc.getValue();
								} 
								PropertyUtils.setSimpleProperty(elm, name, chaine);
							}else if ("Integer".equalsIgnoreCase(t.getSimpleName())) {
								PropertyUtils.setSimpleProperty(elm, name, Integer.valueOf(sheet.getCell(col, row).getContents()));
							}else if ("Date".equalsIgnoreCase(t.getSimpleName())) {						
								String data = sheet.getCell(col, row).getContents();		
								Date date = DateUtil.string2Date(data, formatDate);
								if(date == null){
									date = DateUtil.string2Date(data, formatDate);
								}																
								PropertyUtils.setSimpleProperty(elm, name, date);
							}  
						} catch (Exception e) {
							log.info("error " + e.getMessage());
						}
					}
					
					elm.setAgtn(StringUtils.replace(elm.getAgtn(), " ", ""));
					rs.add(elm);
				}
				
				
			}
		}
		return rs;
	}

	public AwbDetail preDetail(Awb awb, AwbGSAMapping filter, SysUser sysUser) {
		AwbDetailId awbDetailId = new AwbDetailId();
		awbDetailId.setLta(awb.getLta());
		awbDetailId.setSequence(1);
		
		AwbDetail awbDetail = new AwbDetail();
		awbDetail.setId(awbDetailId);
		awbDetail.setQuantity(filter.getQuantity());
		awbDetail.setWeightIndicator(awb.getWeightIndicator());
		awbDetail.setWeightGross(filter.getWeightGross());
		awbDetail.setRateClass("");
		awbDetail.setWeightCharge(filter.getWeightCharge());
		awbDetail.setUnit(filter.getUnitCass());
		awbDetail.setUnitCass(awb.getUnitCass());
		
		if(StringUtils.equals(filter.getChargeIndicator(), "P")){
			awbDetail.setAmount(filter.getWeightPp());
		}else{
			awbDetail.setAmount(filter.getWeightCc());
		}
		
		awbDetail.setDescription(filter.getDescription());
		awbDetail.setCreatedBy(sysUser.getUsername());
		awbDetail.setCreatedDate(new Date());
		
		return awbDetail;
	}

	public Set<AwbTax> preAwbTax(Awb awb, AwbGSAMapping filter, SysUser sysUser) {
		
		Set<AwbTax> setTax = new HashSet<AwbTax>();
		
		if(filter.getMyc() != 0.0){
			
			AwbTaxId id = new AwbTaxId();
			id.setLta(filter.getLta());
			id.setTaxCode("MYC");
			
			AwbTax myc = new AwbTax();
			myc.setId(id);
			
			myc.setTaxAmount(filter.getMyc());
			myc.setCreatedBy(sysUser.getUsername());
			myc.setCreatedDate(new Date());
			setTax.add(myc);
		}
		
		if(filter.getScc() != 0.0){
			
			AwbTaxId id = new AwbTaxId();
			id.setLta(filter.getLta());
			id.setTaxCode("SCC");
			
			AwbTax scc = new AwbTax();
			scc.setId(id);
			scc.setTaxAmount(filter.getScc());
			scc.setCreatedBy(sysUser.getUsername());
			scc.setCreatedDate(new Date());
			setTax.add(scc);
		}
		
		if(filter.getAwc() != 0.0){
			AwbTaxId id = new AwbTaxId();
			id.setLta(filter.getLta());
			id.setTaxCode("AWC");
			
			AwbTax awc = new AwbTax();
			awc.setId(id);
			awc.setTaxAmount(filter.getAwc());
			awc.setCreatedBy(sysUser.getUsername());
			awc.setCreatedDate(new Date());
			setTax.add(awc);
		}
		
		if(filter.getMcc() != 0.0){
			AwbTaxId id = new AwbTaxId();
			id.setLta(filter.getLta());
			id.setTaxCode("MCC");
			
			AwbTax mcc = new AwbTax();
			mcc.setId(id);
			mcc.setTaxAmount(filter.getMcc());
			mcc.setCreatedBy(sysUser.getUsername());
			mcc.setCreatedDate(new Date());
			setTax.add(mcc);
		}
		
		if(filter.getRac() != 0.0){
			AwbTaxId id = new AwbTaxId();
			id.setLta(filter.getLta());
			id.setTaxCode("RAC");
			
			AwbTax rac = new AwbTax();
			rac.setId(id);
			rac.setTaxAmount(filter.getRac());
			rac.setCreatedBy(sysUser.getUsername());
			rac.setCreatedDate(new Date());
			setTax.add(rac);
		}
		return setTax;
	}

}
