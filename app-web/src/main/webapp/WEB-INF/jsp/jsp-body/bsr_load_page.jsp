<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="row">
	<div class="col-lg-5">
		<label class="label label-success" style="font-size:14px">${bsrFilter.rowCount} <spring:message code="message.result" text="!Result(s)"/></label>
	</div>
	<div class="col-lg-7 text-right">
		<c:if test="${bsrFilter.totalPage > 1}">
			<div class="form-group">
				<label class="control-label" style="font-weight: 500; font-size: 14px;"><spring:message code="button.page" text="!page"></spring:message>&nbsp;</label>
				<input id="PageNoGo" type="text" style="width:50px; height:30px; vertical-align:middle;border-radius:3px; border: 1px solid #e5e6e7; font-size:14px;" value="${bsrFilter.page + 1}">
		        <label class="control-label" style="font-weight: 500; font-size:15px"> /${bsrFilter.totalPage}&nbsp;</label>
		        <button id="go" class="btn btn-success btn-sm text-uppercase" type="button">&nbsp;<spring:message code="button.go.page" text="!Go"/>&nbsp;</button>
		        <div class="btn-group">
			        <c:choose>
				        <c:when test="${bsrFilter.page > 0}">
				        	<a id="prev" class="btn btn-sm btn-white text-uppercase" href="--pageNo"><spring:message code="button.prev" text="!Prev."/></a>
				        </c:when>
				        <c:otherwise>
				         	<a class="btn btn-sm btn-white text-uppercase disabled"><spring:message code="button.prev" text="!Prev."/></a>
				        </c:otherwise>
			        </c:choose>
		                        
		                <c:choose>
				            <c:when test="${bsrFilter.page < (bsrFilter.totalPage - 1)}">
				            	<a id="next" class="btn btn-sm btn-white text-uppercase" href="++pageNo"><spring:message code="button.next" text="!Next"/></a>
				            </c:when>
				            <c:otherwise>
				            	<a class="btn btn-sm btn-white text-uppercase disabled"><spring:message code="button.next" text="!Next"/></a>
				            </c:otherwise>
			            </c:choose>
		          </div>
			</div>
		</c:if>
	</div>
</div>

<table class="table table-striped table-bordered table-hover table-result" style="width:100%;">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th><a onclick="doSort('SORTBYCURRFROM', 'COL_CURR_FROM');">
					<spring:message code="bsr.table.currfrom" text="!Currency from"/>
				</a>
			</th>
			<th><spring:message code="bsr.table.currto" text="!Currency to"/></th>
			<th class="text-right"><spring:message code="bsr.table.rate" text="!Rate"/></th>
			<th><a onclick="doSort('SORTBYDAYSTART', 'COL_DAY_START');">
					<spring:message code="bsr.table.startdate" text="!Start date"/>
				</a>
			</th>
			<th><spring:message code="bsr.table.enddate" text="!End date"/></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${lstBsr}" var="elm" varStatus="stt">
			<tr>
				<td class="text-center">${row + stt.index + 1}</td>
				<td>${fn:trim(elm.bsrId.currFrom)}</td>
				<td>${fn:trim(elm.bsrId.currTo)}</td>
				<td class="text-right">${elm.rate}</td>
				<td><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.bsrId.dateStart}"/></td>
				<c:choose>
					<c:when test="${!elm.dateEnd.toString().contains('9999') }">
						<td><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateEnd}"/></td>
					</c:when>
					<c:otherwise>
						<td> </td>
					</c:otherwise>
				</c:choose>
			</tr>
		</c:forEach>	
	</tbody>
</table>

<script type="text/javascript">

	var pageCount = ${bsrFilter.totalPage};
	
	function doSort(action, column){
		document.forms.namedItem('form').elements['action'].value = action;
		document.forms.namedItem('form').elements['column'].value = column;
		$.ajax({
			url: "<spring:url value='/secure/bsr/loadpage'/>", type: "GET",
			data: $('#form').serialize(),
	        cache : false,
	        success: function (response) {
	        	$("#_results").html(response);
	        }
		});
	}
	
	$(function () {
		$("#prev").click(function () {
		    var href = $(this).attr("href");
		    eval("pageNo = (pageCount + " + href + " ) % pageCount");
		    fnLoadPage();
		    return false;
		});
		
		$("#next").click(function () {
		    var href = $(this).attr("href");
		    eval("pageNo = (pageCount + " + href + " ) % pageCount");
		    fnLoadPage();
		    return false;
		});
		$("#go").click(function () {
		    var href = $("#PageNoGo").val() - 1;
		
		    if (href < 1 || isNaN(href)) {
		        eval("pageNo = (pageCount + " + 0 + " ) % pageCount");
		    } else if (href > pageCount - 1) {
		        eval("pageNo = (pageCount + " + (pageCount - 1) + " ) % pageCount");
		    } else {
		        eval("pageNo = (pageCount + " + href + " ) % pageCount");
		    }
		    fnLoadPage();
		    return false;
		});
		
	});
		
	function fnLoadPage() {
		$.ajax({
			url : "<spring:url value='/secure/bsr/loadpage'/>",
		    data: { pageNo: pageNo },
		    cache : false,
		    success: function (response) {
		        $("#_results").html(response);
		    }
		});
	};
	
	$(function () {
 	 	$('.table-result').dataTable({
 	 		"dom": 'T<"clear">lfrtip',
 	        "sDom": "<'row'<'col-sm-4'><'col-sm-4 text-right'>r>" + "t" + "<'row'<'col-sm-4'><'col-sm-4'p>>",
 			"bSort": false,
 			"paginate": false
 	   	}); 	 	
	});
</script>