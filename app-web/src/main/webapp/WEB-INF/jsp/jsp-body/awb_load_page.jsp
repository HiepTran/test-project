<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="row">
    <div class="col-lg-5">
        <label class="label label-success" style="font-size:14px">${awbListFilter.rowCount} <spring:message code="message.result" text="!Result(s)"/></label>
    </div>
    <div class="col-lg-7">
        <div class="row text-right">
            <div class="col-lg-12">
	            <c:if test="${awbListFilter.totalPage > 1}">
	            	<div class="form-group">
		                <label class="control-label" style="font-weight: 500; font-size:14px;"><spring:message code="button.page" text="!Page"/>&nbsp;</label>
		                <input id="PageNoGo" type="text" style="width:50px; height:30px; vertical-align:middle;border-radius:3px; border: 1px solid #e5e6e7; font-size:14px;" value="${awbListFilter.page + 1}">
		                <label class="control-label" style="font-weight: 500; font-size:15px"> /${awbListFilter.totalPage}&nbsp;</label>
		                <button id="go" class="btn btn-success btn-sm text-uppercase" type="button">&nbsp;<spring:message code="button.go.page" text="!Go"/>&nbsp;</button>
		                <div class="btn-group">
			                <c:choose>
				                <c:when test="${awbListFilter.page > 0}">
				                	<a id="prev" class="btn btn-sm btn-white text-uppercase" href="--pageNo"><spring:message code="button.prev" text="!Prev."/></a>
				                </c:when>
				                <c:otherwise>
				                	<a class="btn btn-sm btn-white text-uppercase disabled"><spring:message code="button.prev" text="!Prev."/></a>
				                </c:otherwise>
			                </c:choose>
		                        
		                    <c:choose>
				                <c:when test="${awbListFilter.page < (awbListFilter.totalPage - 1)}">
				                	<a id="next" class="btn btn-sm btn-white text-uppercase" href="++pageNo"><spring:message code="button.next" text="!Next"/></a>
				                </c:when>
				                <c:otherwise>
				                	<a class="btn btn-sm btn-white text-uppercase disabled"><spring:message code="button.next" text="!Next"/></a>
				                </c:otherwise>
			                </c:choose>
		                </div>
		            </div>
	            </c:if>
            </div>
        </div>
    </div>
</div>

<table class="table table-striped table-bordered table-hover table-lta" style="width:100%;">
	<thead>
		<tr>
			<th rowspan="2" class="text-center" style="vertical-align:middle;">#</th>
			<th rowspan="2" class="text-center" style="vertical-align:middle;"><spring:message code="commom.action" text="!Action"/></th>
			<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.awb" text="!"/></th>
			<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.agent" text="!"/></th>
			<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.rdv" text="!"/></th>
			<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.date.execution" text="!"/></th>
			<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.orig" text="!"/></th>
			<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.dest" text="!"/></th>
			<th rowspan="2" class="text-right" style="vertical-align:middle;"><spring:message code="awb.list.weight.charge" text="!"/></th>
			<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.currency" text="!"/></th>
			<th colspan="3" class="text-center"><spring:message code="awb.prepaid" text="!"/></th>
			<th colspan="3" class="text-center"><spring:message code="awb.collect" text="!"/></th>
			<th rowspan="2" class="text-right" style="vertical-align:middle;"><spring:message code="awb.list.com" text="!"/></th>
			<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.channel" text="!"/></th>
		</tr>
		
		<tr>
			<th class="text-right"><spring:message code="awb.list.charge" text="!"/></th>
			<th class="text-right text-nowrap"><spring:message code="awb.list.charge.agent" text="!"/></th>
			<th class="text-right text-nowrap"><spring:message code="awb.list.charge.carrier" text="!"/></th>
			<th class="text-right"><spring:message code="awb.list.charge" text="!"/></th>
			<th class="text-right text-nowrap"><spring:message code="awb.list.charge.agent" text="!"/></th>
			<th class="text-right text-nowrap"><spring:message code="awb.list.charge.carrier" text="!"/></th>
		</tr>
	</thead>
	<tbody>
	  	<c:set var="formatPattern" value="#,##0.00"/>
		
		<c:forEach items="${listAwb}" var="elm" varStatus="stt">
			<c:choose>
				<c:when test="${elm.cutp eq 'XAF'}">
					<tr>
						<td class="text-center">${row + (stt.index + 1)}</td>
						<td class="text-center text-nowrap">
							<a title="Voir" onclick="modalAwb('${elm.lta}')" data-toggle="modal" data-target="#formAwb">
						 		<i class="fa fa-2x fa-search"></i>
           				 	</a>
							<a onclick="doEdit('VIEW','${elm.lta}')" title="<spring:message code="message.modify" text="!View"/>"><i class="fa fa-2x fa-edit"></i></a>
							<a onclick="ConformDelete('${elm.lta}')" title="<spring:message code="button.delete" text="!Delete"/>"><i class="fa fa-2x fa-trash-o"></i></a>
						</td>
						<td class="text-nowrap text-center">
							${elm.tacn}-${elm.lta}
							<c:if test="${elm.statusFlown eq 'F' }">
								<span class="label label-success text-uppercase">
									${elm.statusFlown}
								</span>
							</c:if>
						</td>
						<td>${elm.agtn}</td>
						<td>${elm.rdv}</td>
						<td><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateExecute}" /></td>
						<td>${elm.orig}</td>
						<td>${elm.dest}</td>
						<td class="text-right text-nowrap">${elm.weightGross} ${elm.weightIndicator}</td>
						<td>${elm.cutp}</td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightPp}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentPp}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierPp}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightCc}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentCc}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierCc}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.comm}"/></td>
						<td><spring:message code="awb.list.${elm.channel}" text="!"/></td>
					</tr>
				</c:when>
					
				<c:otherwise>
					<tr>
						<td rowspan="2" class="text-center">${row + (stt.index + 1)}</td>
						<td rowspan="2" class="text-center text-nowrap">
							<a title="Voir" onclick="modalAwb('${elm.lta}')" data-toggle="modal" data-target="#formAwb">
						 		<i class="fa fa-2x fa-search"></i>
           				 	</a>
							<a onclick="doEdit('VIEW','${elm.lta}')" title="<spring:message code="message.modify" text=""/>"><i class="fa fa-2x fa-edit"></i></a>
							<a onclick="ConformDelete('${elm.lta}')" title="<spring:message code="button.delete" text="!Delete"/>"><i class="fa fa-2x fa-trash-o"></i></a>
						</td>
						<td rowspan="2" class="text-nowrap text-center">
							${elm.tacn}-${elm.lta}
							<c:if test="${elm.statusFlown eq 'F' }">
								<span class="label label-success text-uppercase">
									${elm.statusFlown}
								</span>
							</c:if>
						</td>
						<td rowspan="2">${elm.agtn}</td>
						<td rowspan="2">${elm.rdv}</td>
						<td rowspan="2"><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateExecute}" /></td>
						<td rowspan="2">${elm.orig}</td>
						<td rowspan="2">${elm.dest}</td>
						<td rowspan="2" class="text-right text-nowrap">${elm.weightGross} <spring:message code="commom.${elm.weightIndicator}" text="!"/></td>
						<td>${elm.cutp}</td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightPp}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentPp}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierPp}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightCc}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentCc}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierCc}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.comm}"/></td>
						<td rowspan="2"><spring:message code="awb.list.${elm.channel}" text="!"/></td>
					</tr>
					
					<tr>
						<td style="display: none;"></td>
						<td style="display: none;"></td>
						<td style="display: none;"></td>
						<td style="display: none;"></td>
						<td style="display: none;"></td>
						<td style="display: none;"></td>
						<td style="display: none;"></td>
						<td style="display: none;"></td>
						<td style="display: none;"></td>
						<td style="display: none;"></td>
						<td>${elm.cutpLc}</td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightPpLc}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentPpLc}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierPpLc}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightCcLc}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentCcLc}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierCcLc}"/></td>
						<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.commLc}"/></td>
						<td style="display: none;"></td>
					</tr>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</tbody>
</table>

<script type="text/javascript">
	var pageCount = ${awbListFilter.totalPage};
 	$(function () {
 	 	$('.table-lta').dataTable({
 	 		"dom": 'T<"clear">lfrtip',
 	        "sDom": "<'row'<'col-sm-6 text-left'><'col-sm-6 text-right'>r>" + "t" + "<'row'<'col-sm-6'><'col-sm-6'p>>",
 			"bSort": false,
 			"paginate": false,
 			"scrollY": true,
 			"scrollY": '420px',
 	        "scrollCollapse": true,
 	        "scrollX": true
 	   	}); 
 	 	
	    $("#prev").click(function () {
	        var href = $(this).attr("href");
	        eval("pageNo = (pageCount + " + href + " ) % pageCount");
	        fnLoadPage();
	        return false;
	    });
	
	    $("#next").click(function () {
	        var href = $(this).attr("href");
	        eval("pageNo = (pageCount + " + href + " ) % pageCount");
	        fnLoadPage();
	        return false;
	    });
	
	    $("#go").click(function () {
	        var href = $("#PageNoGo").val() - 1;
	
	        if (href < 1 || isNaN(href)) {
	            eval("pageNo = (pageCount + " + 0 + " ) % pageCount");
	        } else if (href > pageCount - 1) {
	            eval("pageNo = (pageCount + " + (pageCount - 1) + " ) % pageCount");
	        } else {
	            eval("pageNo = (pageCount + " + href + " ) % pageCount");
	        }
	        fnLoadPage();
	        return false;
	    });
	});
	
 	function fnLoadPage() {
	    $.ajax({
	    	url : "<spring:url value='/secure/awb/loadPage'/>",
	        data: { pageNo: pageNo },
	        cache : false,
	        success: function (response) {
	            $("#_results").html(response);
	        }
	    });
	};
</script>