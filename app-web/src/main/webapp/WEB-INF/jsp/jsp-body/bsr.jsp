<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<style type="text/css">
	.datepicker { 
		z-index: 9999 !important;
	}
</style>

<script type="text/javascript">
	document.getElementById("param").className = "active";
	document.getElementById("param_bsr").className = "active";

	var pageNo = ${bsrFilter.page};
	
	$(document).ready(function(){
	    $('.input-group.date').datepicker({
			 todayBtn: "linked",
	         keyboardNavigation: false,
	         forceParse: false,
	         calendarWeeks: true,
	         autoclose: true,
			 format: "dd/mm/yyyy"
	    });
	    
	    $.ajax({
	    	url : "<spring:url value='/secure/bsr/loadpage/errorload.json'/>", type: "GET",
	        data: $('#form').serialize(),
	        cache : false,
	        success: function (result) {
	        	if(result.errCodeTable != 1){
	        		doShowTable();
				}
	        }
	    });
	});
	
	function doValidatorShowTable(search){
	    $.ajax({
	    	url : "<spring:url value='/secure/bsr/loadpage/error.json'/>", type: "GET",
	        data: $('#form').serialize(),
	        cache : false,
	        success: function (result) {
	        	if(result.errCodeTable > 0){
					$('#err_table').html('');
					var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	for(var i=0;i<len;i++)
	               	{
	                   $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	}
		           	$("#_results").html('');
				}
				else{
					$('#err_table').html('');
					doShowTable(search);
				}
	        }
	    });
	};
	
	function doShowTable(search){
		document.forms.namedItem('form').elements['column'].value = search;
		$.ajax({
			url : "<spring:url value='/secure/bsr/loadpage'/>", type: "GET",
	        data: $('#form').serialize(),
	        cache : false,
	        success: function (response) {
	        	$("#_results").html(response);
	        }
		});
	}
	
	
	function doCreateForm(){
		$.ajax({
			url: "<spring:url value='/secure/bsr/create/error.json'/>",
			type: "POST",
			cache: false,
			data: $('#form-add').serialize(),
			success: function(result){
				if(result.errCodeCreate > 0){
					$('#err_currFromAdd').html('');
					$('#err_currToAdd').html('');
					$('#err_rateAdd').html('');
					$('#err_dateStartAdd').html('');
					var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	for(var i=0;i<len;i++)
	               	{
	                   $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	}
				}
				else{
					$('#form-add').submit();
				}
			}
		});
	}
</script>

<div id="body">
	<form:form id="form" name="form" commandName="bsrFilter">
		<input type="hidden" name="action">
		<input type="hidden" name="column">
		<div class="row">
			<div class="col-sm-3">
				<div class="form-group">
					<label><spring:message code="bsr.currfrom" text="!Currency from"/></label>
					<form:input path="currFrom" cssClass="form-control textfield" maxlength="3" style='text-transform:uppercase'/>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label><spring:message code="bsr.currto" text="!Currency to"/></label>
					<form:input path="currTo" cssClass="form-control text-field" maxlength="3" style='text-transform:uppercase'/>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label><spring:message code="bsr.startdate" text="!Start date"/></label>
					<div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <form:input path="dateStart" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
				</div>
			</div>
			
			<div class="col-sm-3">
				<div class="form-group">
					<label><spring:message code="bsr.enddate" text="!End date"/></label>
					<div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <form:input path="dateEnd" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-sm-4">
				<button type="button" class="btn btn-w-m btn-success text-uppercase" data-toggle="modal" data-target="#formCreate">
					<i class="fa fa-plus-square "></i> <spring:message code="button.create" text="!Create"/>
				</button>
				
			</div>
			<div class="col-sm-8 text-right">
				<button onclick="javascript:doSubmit('RESET');" class="btn btn-w-m btn-default text-uppercase"><i class="fa fa-undo"></i>
					<spring:message code="button.reset" text="!"></spring:message>
				</button>
				<button onclick="doValidatorShowTable('DEFAULT');" type="button" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-search"></i> <spring:message code="button.go" text="!"/>
				</button>	
				<a href="<c:url value="/secure/bsr/excel"></c:url>" target="_blank" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-file-excel-o"></i> <spring:message code="button.excel" text="!"/>
				</a>
			</div>
		</div>
		<label id="err_table" class="text-danger"></label>
		&nbsp;
		<div class="row">
			<div class="col-sm-12">
				<div id="_results"></div>
			</div>
		</div>
	</form:form>
	
	<div class="modal inmodal" id="formCreate" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" style="width:800px">
			<div class="modal-content animated bounceInRight">
				<div class="modal-body">
				<form:form id="form-add" name="form-add" method="post" commandName="bsrFilter" action="bsr/create">
					<input type="hidden" name="action" value="CREATE">	
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label><spring:message code="bsr.convertfrom" text="!Convert from"/></label>
								<form:input path="currFromAdd" cssClass="form-control textfield" maxlength="3" style='text-transform:uppercase'/>
								<label id="err_currFromAdd" class="text-danger"></label>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label><spring:message code="bsr.convertto" text="!to"/></label>
								<form:input path="currToAdd" cssClass="form-control textfield" maxlength="3" style='text-transform:uppercase'/>
								<label id="err_currToAdd" class="text-danger"></label>
							</div>
							
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label><spring:message code="bsr.rate" text="!Roe"/></label>
								<form:input path="rateAdd" cssClass="form-control textfield text-right"/>
								<label id="err_rateAdd" class="text-danger"></label>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label><spring:message code="bsr.appticationdate" text="!Application date	"/></label>
								<div class="input-group date">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			                        <form:input path="dateStartAdd" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
								</div>
								<label id="err_dateStartAdd" class="text-danger"></label>
							</div>
						</div>
					</div>
					</form:form>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<button id ="add" type="button" onclick="doCreateForm();" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-plus-square "></i> <spring:message code="button.create" text="!Create"/>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>