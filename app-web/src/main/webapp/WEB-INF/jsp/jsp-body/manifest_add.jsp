<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<style type="text/css">
	.checkbox {
	    padding-bottom: 5px;
	}
</style>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>


<script type="text/javascript">
	document.getElementById("manifest").className = "active";
	document.getElementById("createManifest").className = "active";

	$(document).ready(function() {
		$('.input-group.date').datepicker({
			todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
			format: "dd/mm/yyyy"
	    });

 	 	$('.table-result').dataTable({
 	 		"dom": 'T<"clear">lfrtip',
 	        "sDom": "<'row'<'col-sm-6 text-left'><'col-sm-6 text-right'>r>" + "t" + "<'row'<'col-sm-6'><'col-sm-6'p>>",
 			"bSort": false,
 			"paginate": false,
 	        "scrollX": true
 	   	});

 	 	$("#fromManifest").css({
	        'margin-left': function () {
	            return -($(this).width() / 2);
	        }
	    });
	});

	function modalKm(id, manifest, type) {
		var url = "<c:url value='/secure/manifest/awb/km?id='/>" + id + "&manifest=" + manifest + "&type=" + type;
		jQuery.ajax({
			cache: false,
	     	type:"GET",
	     	url:url,
	     	success:function(html){			    	
   	 			jQuery("#modalAwb").html(html);
			}
		});
	};

	function modalKg(id, manifest, type) {
		var url = "<c:url value='/secure/manifest/awb/kg?id='/>" + id + "&manifest=" + manifest + "&type=" + type;
		jQuery.ajax({
			cache: false,
	     	type:"GET",
	     	url:url,
	     	success:function(html){			    	
   	 			jQuery("#modalAwb").html(html);
			}
		});
	};
	
		
	function checkUncheckAdd(bool){
		var size = 0;
		try{
			size = document.forms[0].elements['ltaChekAdd'].length;
			document.forms[0].elements['ltaChekAdd'].checked=bool;
			for(i=0; i<size;i++){
				document.forms[0].elements['ltaChekAdd'][i].checked=bool;
			}
		}catch(err){}
	};

	function doAdd() {
 		document.forms[0].elements['action'].value='ADD';
	 	document.forms[0].submit();
        
    };
</script>

<div id="body">
	<form:form id="form" name="form" method="post" commandName="awbAddFilter">
		<input type="hidden" name="action">
	
		<label><spring:message code="manifest.title" text="!"/></label>
		<div class="row">
			<div class="col-lg-12">
				<table class="table table-striped table-bordered table-hover table-manifest" style="width:100%;">
					<thead>
						<tr>
							<th><spring:message code="manifest.owner" text="!"/></th>
							<th><spring:message code="manifest.no" text="!"/></th>
							<th><spring:message code="manifest.orig" text="!"/></th>
							<th><spring:message code="manifest.dest" text="!"/></th>
							<th><spring:message code="manifest.flight.number" text="!"/></th>
							<th><spring:message code="manifest.flight.date" text="!"/></th>
							<th><spring:message code="manifest.immat" text="!"/></th>
							<th class="text-right"><spring:message code="manifest.entry.number" text="!"/></th>
							<th class="text-right"><spring:message code="manifest.reality.number" text="!"/></th>
							<th class="text-right"><spring:message code="manifest.rest" text="!"/></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>${maniFest.tacn}</td>
							<td class="text-nowrap">${maniFest.manifestNo}</td>
							<td>${maniFest.orig}</td>
							<td>${maniFest.dest}</td>
							<td>${maniFest.flightNumber}</td>
							<td><fmt:formatDate pattern="dd/MM/yyyy" value="${maniFest.flightDate}" /></td>
							<td>${maniFest.immat}</td>
							<td class="text-right">${maniFest.entryNumber}</td>
							<td class="text-right">${maniFest.awbFlownCount}</td>
							<c:choose>
								<c:when test="${maniFest.entryNumber == 0}">
									<td class="text-right">N/A</td>
								</c:when>
								<c:when test="${maniFest.entryNumber != 0 &&  (maniFest.entryNumber - maniFest.awbFlownCount) !=0}">
									<td class="text-right text-danger">${maniFest.entryNumber - maniFest.awbFlownCount}</td>
								</c:when>
								<c:otherwise>
									<td class="text-right">${maniFest.entryNumber - maniFest.awbFlownCount}</td>
								</c:otherwise>
							</c:choose>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="row">&nbsp;</div>
		<label><spring:message code="manifest.filter" text=""/></label>
		<div class="form-group">
			<div class="row">
			 	<div class="col-lg-3">
				 	<label><spring:message code="manifest.lta" text="!"/></label>
				 	<form:input path="lta" maxlength="8" class="form-control textfield"/>
				 </div>
				<div class="col-lg-3">
				 	<label><spring:message code="manifest.orig" text="!"/></label>
				 	<form:input path="orig" maxlength="3" class="form-control textfield text-uppercase"/>
				 </div>
				 <div class="col-lg-3">
				 	<label><spring:message code="manifest.dest" text="!"/></label>
				 	<form:input path="dest" maxlength="3" class="form-control textfield text-uppercase"/>
				 </div>
				<div class="col-lg-3">
				 	<label><spring:message code="awb.date.execution" text="!"/></label>
				 	<div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <form:input path="dateExecute" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
				 </div>
			 </div>
		 </div>
		 <div class="form-group">
		 	<div class="row">
		 		<div class="col-lg-6">
					<a href="<c:url value="/secure/manifest"/>" class="btn btn-w-m btn-default text-uppercase"><spring:message code="button.back" text="!"/></a>
				</div>
				<div class="col-lg-6 text-right">
					<a onclick="doSubmit('RESET');" class="btn btn-w-m btn-default text-uppercase"><i class="fa fa-undo"></i>&nbsp;<spring:message code="button.reset" text="!"/></a>
				 	<a onclick="doSubmit('GO');" class="btn btn-w-m btn-success text-uppercase"><i class="fa fa-check"></i>&nbsp;<spring:message code="button.go" text="!"/></a>
				</div>
			</div>
		 </div>
		
		<div class="row">
			<div class="col-lg-3">
				<a onclick="doAdd();" class="btn btn-w-m btn-success text-uppercase"><i class="fa fa-plus-square-o"></i>&nbsp;<spring:message code="button.flown." text="Ajouter"/><br><spring:message code="button.flown." text="au manifeste"/></a>
			</div>
			<div class="col-lg-6 text-center">
				<br><label><spring:message code="manifest.search.result" text=""/></label>
			</div>
		</div>
		
		<div class="row">&nbsp;</div>
		<div class="row">
			<div class="col-lg-12">		
				<table class="table table-striped table-bordered table-hover table-result" style="width:100%;">
					<thead>
						<tr>
							<th rowspan="2" class="text-center" style="vertical-align:middle;">#</th>
							<th rowspan="2" style="vertical-align:middle;" class="text-center">
								<div class="checkbox checkbox-primary">
	                               <input type="checkbox" name="chkAdd" onclick="javascript:checkUncheckAdd(this.checked);">
	                               <label></label>
	                           	</div>
							</th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.awb" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;" class="text-center"><spring:message code="awb.coupon" text="!Cp"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.agent" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.date.execution" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.orig" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.dest" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="manifest.line" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;" class="text-right"><spring:message code="awb.list.weight.charge" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.currency" text="!"/></th>
							<th colspan="3" class="text-center"><spring:message code="awb.prepaid" text="!"/></th>
							<th colspan="3" class="text-center"><spring:message code="awb.collect" text="!"/></th>
							<th rowspan="2" class="text-right" style="vertical-align:middle;"><spring:message code="awb.list.com" text="!"/></th>
						</tr>
						
						<tr>
							<th class="text-right"><spring:message code="awb.list.charge" text="!"/></th>
							<th class="text-right"><spring:message code="awb.list.charge.agent" text="!"/></th>
							<th class="text-right"><spring:message code="awb.list.charge.carrier" text="!"/></th>
							<th class="text-right"><spring:message code="awb.list.charge" text="!"/></th>
							<th class="text-right"><spring:message code="awb.list.charge.agent" text="!"/></th>
							<th class="text-right"><spring:message code="awb.list.charge.carrier" text="!"/></th>
						</tr>
					</thead>
					<tbody>
						<c:set var="formatPattern" value="#,##0.00"/>
						<c:set var="rows" value="0"></c:set>
						
						<c:forEach items="${listAwbFlown}" var="elm" varStatus="stt">
							<tr class="text-danger">
								<td class="text-center">${row + (stt.index + 1)}</td>
								<td class="text-center text-nowrap">
									<c:choose>
										<c:when test="${elm.orig !=  maniFest.orig || elm.dest !=  maniFest.dest}">
											<a title="Split Km" onclick="modalKm('${elm.awbFlownId}', '${maniFest.manifestNo}','FLOWN')" data-toggle="modal" data-target="#fromManifest">
										 		<i class="fa fa-2x fa-pencil-square-o"></i>
	                    				 	</a>
										</c:when>
										<c:otherwise>
											<a title="Split Kg" onclick="modalKg('${elm.awbFlownId}', '${maniFest.manifestNo}','FLOWN')" data-toggle="modal" data-target="#fromManifest">
										 		<i class="fa fa-2x fa-pencil-square-o"></i>
	                    				 	</a>
										</c:otherwise>
									</c:choose>
                    				 <div class="checkbox checkbox-primary checkbox-inline">
	                                   <input title="Choose" type="checkbox" id="ltaChekAdd" name="ltaChekAdd" value="${elm.awbFlownId};FLOWN">
	                                   <label></label>
	                               	</div>
								</td>
								<td class="text-nowrap">${elm.tacn}-${elm.lta}</td>
								<td class="text-center">${elm.coupon}</td>
								<td>${elm.agtn}</td>
								<td><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateExecute}" /></td>
								<td>${elm.orig}</td>
								<td>${elm.dest}</td>
								<td>${elm.line}</td>
								<td class="text-right text-nowrap"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightCharge}"/> ${elm.weightIndicator}</td>
								<td>${elm.cutp}</td>
								<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightPp}"/></td>
								<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentPp}"/></td>
								<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierPp}"/></td>
								<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightCc}"/></td>
								<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentCc}"/></td>
								<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierCc}"/></td>
								<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.comm}"/></td>
							</tr>
							<c:set var="rows" value="${row + (stt.index + 1)}"></c:set>
						</c:forEach>
						
						
						<c:forEach items="${listAwb}" var="elm" varStatus="stt">
						<c:choose>
							<c:when test="${elm.orig !=  maniFest.orig || elm.dest !=  maniFest.dest}">
								<tr class="text-danger">
									<td class="text-center">${rows + (stt.index + 1)}</td>
									<td class="text-center text-nowrap">
										 <a title="Split Km" onclick="modalKm('${elm.lta}', '${maniFest.manifestNo}','AWB')" data-toggle="modal" data-target="#fromManifest">
									 		<i class="fa fa-2x fa-pencil-square-o"></i>
	                    				 </a>
	                    				 <div class="checkbox checkbox-primary checkbox-inline">
		                                   <input title="Choose" type="checkbox" id="ltaChekAdd" name="ltaChekAdd" value="${elm.lta};AWB">
		                                   <label></label>
		                               	</div>
									</td>
									<td class="text-nowrap">${elm.tacn}-${elm.lta}</td>
									<td class="text-center"></td>
									<td>${elm.agtn}</td>
									<td><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateExecute}" /></td>
									<td>${elm.orig}</td>
									<td>${elm.dest}</td>
									<td>${elm.line}</td>
									<td class="text-right text-nowrap">${elm.weightCharge} ${elm.weightIndicator}</td>
									<td>${elm.cutp}</td>
									<td class="text-right">${elm.weightPp}</td>
									<td class="text-right">${elm.agentPp}</td>
									<td class="text-right">${elm.carrierPp}</td>
									<td class="text-right">${elm.weightCc}</td>
									<td class="text-right">${elm.agentCc}</td>
									<td class="text-right">${elm.carrierCc}</td>
									<td class="text-right">${elm.comm}</td>
								</tr>
							</c:when>
							<c:otherwise>
								<tr>
									<td class="text-center">${rows + (stt.index + 1)}</td>
									<td class="text-center text-nowrap">
										<a title="Split Kg" onclick="modalKg('${elm.lta}', '${maniFest.manifestNo}','AWB')" data-toggle="modal" data-target="#fromManifest">
									 		<i class="fa fa-2x fa-pencil-square-o"></i>
	                    				 </a>
										<div class="checkbox checkbox-primary checkbox-inline">
		                                   <input title="Choose" type="checkbox" id="ltaChekAdd" name="ltaChekAdd" value="${elm.lta};AWB">
		                                   <label></label>
		                               	</div>
									</td>
									<td class="text-nowrap">${elm.tacn}-${elm.lta}</td>
									<td class="text-center"></td>
									<td>${elm.agtn}</td>
									<td><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateExecute}" /></td>
									<td>${elm.orig}</td>
									<td>${elm.dest}</td>
									<td>${elm.line}</td>
									<td class="text-right text-nowrap">${elm.weightCharge} ${elm.weightIndicator}</td>
									<td>${elm.cutp}</td>
									<td class="text-right">${elm.weightPp}</td>
									<td class="text-right">${elm.agentPp}</td>
									<td class="text-right">${elm.carrierPp}</td>
									<td class="text-right">${elm.weightCc}</td>
									<td class="text-right">${elm.agentCc}</td>
									<td class="text-right">${elm.carrierCc}</td>
									<td class="text-right">${elm.comm}</td>
								</tr>
							</c:otherwise>
						</c:choose>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-6">
				<a onclick="doAdd();" class="btn btn-w-m btn-success text-uppercase"><i class="fa fa-plus-square-o"></i>&nbsp;<spring:message code="button.flown." text="Ajouter"/><br><spring:message code="button.flown." text="au manifeste"/></a>
			</div>
		</div>
		<div class="row">&nbsp;</div>
	</form:form>
	
	<div class="row">
		<div class="col-lg-12">
			<!-- Modal create Manifest -->
			<div class="modal inmodal" id="fromManifest" tabindex="-1" role="dialog" aria-hidden="true">
				<div id="modalAwb"></div>
			</div>
		</div>
	</div>
</div>