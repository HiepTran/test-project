<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="row">
    <div class="col-lg-5">
        <label class="label label-success" style="font-size:14px">${manifestFilter.rowCount} <spring:message code="message.result" text="!Result(s)"/></label>
    </div>
    <div class="col-lg-7">
        <div class="row text-right">
            <div class="col-lg-12">
	            <c:if test="${manifestFilter.totalPage > 1}">
	            	<div class="form-group">
		                <label class="control-label" style="font-weight: 500; font-size:14px;"><spring:message code="button.page" text="!Page"/>&nbsp;</label>
		                <input id="PageNoGo" type="text" style="width:50px; height:30px; vertical-align:middle;border-radius:3px; border: 1px solid #e5e6e7; font-size:14px;" value="${manifestFilter.page + 1}">
		                <label class="control-label" style="font-weight: 500; font-size:15px"> /${manifestFilter.totalPage}&nbsp;</label>
		                <button id="go" class="btn btn-success btn-sm text-uppercase" type="button">&nbsp;<spring:message code="button.go.page" text="!Go"/>&nbsp;</button>
		                <div class="btn-group">
			                <c:choose>
				                <c:when test="${manifestFilter.page > 0}">
				                	<a id="prev" class="btn btn-sm btn-white text-uppercase" href="--pageNo"><spring:message code="button.prev" text="!Prev."/></a>
				                </c:when>
				                <c:otherwise>
				                	<a class="btn btn-sm btn-white text-uppercase disabled"><spring:message code="button.prev" text="!Prev."/></a>
				                </c:otherwise>
			                </c:choose>
		                        
		                    <c:choose>
				                <c:when test="${manifestFilter.page < (manifestFilter.totalPage - 1)}">
				                	<a id="next" class="btn btn-sm btn-white text-uppercase" href="++pageNo"><spring:message code="button.next" text="!Next"/></a>
				                </c:when>
				                <c:otherwise>
				                	<a class="btn btn-sm btn-white text-uppercase disabled"><spring:message code="button.next" text="!Next"/></a>
				                </c:otherwise>
			                </c:choose>
		                </div>
		            </div>
	            </c:if>
            </div>
        </div>
    </div>
</div>
&nbsp;
<table class="table table-striped table-bordered table-hover table-manifest" style="width:100%;">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center"><spring:message code="commom.action" text="!Action"/></th>
			<th><spring:message code="manifest.owner" text="!"/></th>
			<th><spring:message code="manifest.no" text="!"/></th>
			<th><spring:message code="manifest.orig" text="!"/></th>
			<th><spring:message code="manifest.dest" text="!"/></th>
			<th><spring:message code="manifest.flight.number" text="!"/></th>
			<th><spring:message code="manifest.flight.date" text="!"/></th>
			<th><spring:message code="manifest.immat" text="!"/></th>
			<th class="text-right"><spring:message code="manifest.entry.number" text="!"/></th>
			<th class="text-right"><spring:message code="manifest.reality.number" text="!"/></th>
			<th class="text-right"><spring:message code="manifest.rest" text="!"/></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${listManifest}" var="elm" varStatus="stt">
			<tr id="${elm.manifestNo}" >
				<td class="text-center">${row + (stt.index + 1)}</td>
				<td class="text-center text-nowrap">
					<a  onclick="doEdit('FLOWN','${elm.manifestNo}')" title="<spring:message code="message.add" text="!Add"/>"><i class="fa fa-2x fa-plus-square-o"></i></a>
					<c:choose>
						<c:when test="${elm.awbFlownCount > 0}">
							<i class="fa fa-2x fa-trash-o"></i>
						</c:when>
						<c:otherwise>
							<a onclick="javascript:ConformDelete('${elm.manifestNo}');"  title="<spring:message code="button.delete" text="!Delete"/>"><i class="fa fa-2x fa-trash-o"></i></a>
						</c:otherwise>
					</c:choose>
				</td>
				<td>${elm.tacn}</td>
				<td class="text-nowrap">${elm.manifestNo}</td>
				<td>${elm.orig}</td>
				<td>${elm.dest}</td>
				<td>${elm.flightNumber}</td>
				<td><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.flightDate}" /></td>
				<td>${elm.immat}</td>
				<td class="text-right">${elm.entryNumber}</td>
				<td class="text-right">${elm.awbFlownCount}</td>
				<c:choose>
					<c:when test="${elm.entryNumber == 0}">
						<td class="text-right">N/A</td>
					</c:when>
					<c:when test="${elm.entryNumber != 0 &&  (elm.entryNumber - elm.awbFlownCount) !=0}">
						<td class="text-right text-danger">${elm.entryNumber - elm.awbFlownCount}</td>
					</c:when>
					<c:otherwise>
						<td class="text-right">${elm.entryNumber - elm.awbFlownCount}</td>
					</c:otherwise>
				</c:choose>
			</tr>
		</c:forEach>
	</tbody>
</table>

<script type="text/javascript">
 	$(function () {
 	 	$('.table-manifest').dataTable({
 	 		"dom": 'T<"clear">lfrtip',
 	        "sDom": "<'row'<'col-sm-6 text-left'><'col-sm-6 text-right'>r>" + "t" + "<'row'<'col-sm-6'><'col-sm-6'p>>",
 			"bSort": false,
 			"paginate": false
   		}); 
 	});
</script>