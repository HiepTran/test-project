<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>

<style type="text/css">
     .datepicker { 
		z-index: 9999 !important;
	}
</style>

<script type="text/javascript">
	document.getElementById("param").className = "active";
	document.getElementById("param_agentgsa").className = "active";
	
	var pageNo = ${agentGsaFilter.page};
	
	$(function(){
	    $.ajax({
	    	url : "<spring:url value='/secure/agentGsa/loadpage'/>",
	        data: { pageNo: pageNo },
	        cache : false,
	        success: function (response) {
	            $("#_results").html(response);
	        }
	    });
	    
	    $('.input-group.date').datepicker({
			 todayBtn: "linked",
	         keyboardNavigation: false,
	         forceParse: false,
	         calendarWeeks: true,
	         autoclose: true,
			 format: "dd/mm/yyyy"
	    });
	});
	function doCreateForm(){
		$.ajax({
			url: "<spring:url value='/secure/agentGsa/create/error.json'/>", type: "POST",
			cache: false,
			data: $('#form-add').serialize(),
			success: function(result){
				if(result.errCodeCreate > 0){
		           	 $('#err_codeAdd').html('');
		           	 $('#err_commAdd').html('');
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++)
	               	 {
	                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
	        	}else{
					$('#form-add').submit();
	        	}
			}
		});
	}
	
	function ConformDelete(action, agentgsaCode){
		 swal({
	            title: "<spring:message code="message.delete" text="!"/>" + " [" + agentgsaCode + "] ?",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "<spring:message code="button.delete" text="!"/>",
	            cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
	            closeOnConfirm: false
	        }, function () {
	       	 	document.forms[0].elements['action'].value='DELETE';
				 document.forms[0].elements['id'].value=agentgsaCode;
				 document.forms[0].submit();
	        });
	}

</script>

<div id="body">
	<form:form id="form" name="form" method="post" commandName="agentGsaFilter">
	<input name="action" type="hidden"> 
	<input name="id" type="hidden">
	
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label><spring:message code="agentgsa.code" text="!Code"/></label>
					<form:input path="codeFilter" type="text" maxlength="7" cssClass="form-control textfield"/>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-sm-4">
				<button type="button" class="btn btn-w-m btn-success text-uppercase" data-toggle="modal" data-target="#formCreate">
				<i class="fa fa-plus-square"> <spring:message code="button.create" text="!"/></i>
				</button>
			</div>
			<div class="col-sm-8 text-right">
				<button onclick="javascript:doSubmit('RESET')" type="button" class="btn btn-w-m btn-default text-uppercase">
					<i class="fa fa-undo"> <spring:message code="button.reset"/></i>
				</button>
				<button onclick="javascript:doSubmit('GO');" type="button" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-search"></i> <spring:message code="button.go" text="!"/>
				</button>
			</div>
		</div>
		&nbsp;
		<div class="row">
			<div class="col-sm-12">
				<div id="_results"></div>
			</div>
		</div>
	</form:form>
	
	<div class="modal inmodal" id="formCreate" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" style="width:800px">
			<div class="modal-content animated bounceInRight">
				<div class="modal-body">
				<form:form id="form-add" name="form-add" method="post" commandName="agentGsaFilter" action="agentGsa/create">
					<input type="hidden" name="action" value="CREATE">	
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="agentgsa.code" text="!Code"/></label>
								<form:input path="codeAdd" type="text" maxlength="7" cssClass="form-control textfield"/>
								<label id="err_codeAdd" class="text-danger"></label>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="agentgsa.name" text="!Name"/></label>
								<form:input path="nameAdd" cssClass="form-control textfield disablecopypaste"/>
								<label id="err_nameAdd" class="text-danger"></label>
							</div>
							
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="agentgsa.comm" text="!Comm"/></label>
								<form:input path="commAdd"  cssClass="form-control textfield text-right"/>
								<label id="err_commAdd" class="text-danger"></label>
							</div>
						</div>
						
					</div>
					
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="agentgsa.datestart" text="!Date start"/></label>
								<div class="input-group date">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			                        <form:input path="dateStartAdd" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
								</div>
								<label id="err_dateStartAdd" class="text-danger"></label>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="agentgsa.dateend" text="!Date end"/></label>
								<div class="input-group date">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			                        <form:input path="dateEndAdd" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
								</div>
								<label id="err_dateEndAdd" class="text-danger"></label>
							</div>
						</div>
					</div>
					</form:form>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<button id ="add" type="button" onclick="doCreateForm();" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-plus-square "></i> <spring:message code="button.create" text="!Create"/>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>