<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="row">
	<div class="col-lg-5">
		<label class="label label-success" style="font-size:14px">${legkmfilter.rowCount} <spring:message code="message.result" text="!Result(s)"/></label>
	</div>
	<div class="col-lg-7 text-right">
		<c:if test="${legkmfilter.totalPage > 1}">
			<div class="form-group">
		    	<label class="control-label" style="font-weight: 500; font-size:14px;"><spring:message code="button.page" text="!Page"/>&nbsp;</label>
			   	<input id="PageNoGo" type="text" style="width:50px; height:30px; vertical-align:middle;border-radius:3px; border: 1px solid #e5e6e7; font-size:14px;" value="${legkmfilter.page + 1}">
			   	<label class="control-label" style="font-weight: 500; font-size:15px"> /${legkmfilter.totalPage}&nbsp;</label>
			   	<button id="go" class="btn btn-success btn-sm text-uppercase" type="button">&nbsp;<spring:message code="button.go.page" text="!Go"/>&nbsp;</button>
				<div class="btn-group">
					 <c:choose>
						<c:when test="${legkmfilter.page > 0}">
							<a id="prev" class="btn btn-sm btn-white text-uppercase" href="--pageNo"><spring:message code="button.prev" text="!Prev."/></a>
						</c:when>
					  	<c:otherwise>
					  		<a class="btn btn-sm btn-white text-uppercase disabled"><spring:message code="button.prev" text="!Prev."/></a>
					  	</c:otherwise>
					 </c:choose>
        
				    <c:choose>
						<c:when test="${legkmfilter.page < (legkmfilter.totalPage - 1)}">
				  		<a id="next" class="btn btn-sm btn-white text-uppercase" href="++pageNo"><spring:message code="button.next" text="!Next"/></a>
				  	</c:when>
				  	<c:otherwise>
				  		<a class="btn btn-sm btn-white text-uppercase disabled"><spring:message code="button.next" text="!Next"/></a>
				  	</c:otherwise>
				 	</c:choose>
				</div>
			</div>
		 </c:if>
	</div>
</div>

<table class="table table-striped table-bordered table-hover table-manifest" style="width:100%;">
	<thead>
		<tr>
			<th class="text-center">#</th>
				<th class="text-center"><spring:message code="commom.action" text="!"/></th>
				<th><spring:message code="legkm.isocfrom" text="!Isoc from"/></th>
				<th><spring:message code="legkm.airportfrom" text="!Airport from"/></th>
				<th><spring:message code="legkm.isocto" text="!Isoc to"/></th>
				<th><spring:message code="legkm.airportto" text="!Airport to"/></th>
				<th class="text-right"><spring:message code="legkm.km" text="!Km"/></th>
				<th class="text-right"><spring:message code="legkm.mile" text="!Mile"/></th>
			</tr>
			</thead>
			<tbody>
				<c:set var="formatNumber" value="#,##0.00"/>
				<c:forEach items="${lstLegKm}" var="elm" varStatus="stt">
					<tr id="${stt.index }">
						<td class="text-center">${row + stt.index + 1}</td>
						<td class="text-center text-nowrap">
							<a id='a-edit-${stt.index}' onclick='bfShow("edit-box","a-edit-${stt.index}", "${stt.index}");
								showForm("edit-box", "${elm.id.isocFrom}", "${elm.id.airportFrom }", "${elm.id.isocTo }", "${elm.id.airportTo }")' 
								title="<spring:message code="legkm.table.actionedit" text="!Edit"/>"><i class="fa fa-2x fa-edit"></i>
							</a>
							<a onclick="ConformDelete('${elm.id.isocFrom}', '${elm.id.airportFrom }', '${elm.id.isocTo }', '${elm.id.airportTo }');"
								title="<spring:message code="legkm.table.delete" text="!Delete"/>"><i class="fa fa-2x fa-trash-o"></i>
							</a>
						</td>
							<td>${elm.id.isocFrom}</td>
							<td>${elm.id.airportFrom }</td>
							<td>${elm.id.isocTo }</td>
							<td>${elm.id.airportTo }</td>
							<td class="text-right"><fmt:formatNumber pattern="${formatNumber}" value="${elm.km }"/></td>
							<td class="text-right"><fmt:formatNumber pattern="${formatNumber}" value="${elm.mile }"/></td>
					</tr>
			</c:forEach>
		</tbody>
</table>
			
<script type="text/javascript">

	var pageCount = ${legkmfilter.totalPage};
	
	function showForm(divId, isocfrom, airportfrom, isocto, airportto) {
	    $.ajax({
	        url: "<spring:url value='/secure/legkm/model/ajax/'/>" + isocfrom +"/" + airportfrom + "/" + isocto + "/" +airportto,
	        cache: false,
	        success: function (reponse) {
	            $("div#edit-result").html(reponse);
	        }
	    });
	    show(divId);
	}
	
	$(function () {
		$("#prev").click(function () {
		    var href = $(this).attr("href");
		    eval("pageNo = (pageCount + " + href + " ) % pageCount");
		    fnLoadPage();
		    return false;
		});
		
		$("#next").click(function () {
		    var href = $(this).attr("href");
		    eval("pageNo = (pageCount + " + href + " ) % pageCount");
		    fnLoadPage();
		    return false;
		});
		$("#go").click(function () {
		    var href = $("#PageNoGo").val() - 1;
		
		    if (href < 1 || isNaN(href)) {
		        eval("pageNo = (pageCount + " + 0 + " ) % pageCount");
		    } else if (href > pageCount - 1) {
		        eval("pageNo = (pageCount + " + (pageCount - 1) + " ) % pageCount");
		    } else {
		        eval("pageNo = (pageCount + " + href + " ) % pageCount");
		    }
		    fnLoadPage();
		    return false;
		});
		
	});
		
	function fnLoadPage() {
		$.ajax({
			url : "<spring:url value='/secure/legkm/loadpage'/>",
		    data: { pageNo: pageNo },
		    cache : false,
		    success: function (response) {
		        $("#_results").html(response);
		    }
		});
	};
</script>	