<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style type="text/css">
	.datepicker { 
		z-index: 9999 !important;
	}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$('.input-group.date').datepicker({
			todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "dd/mm/yyyy"
		});
	});

	function doReport() {
		document.forms[0].elements['action'].value = "GO";
		document.forms[0].elements['type'].value = "CA_TRANSPORT";
		document.forms[0].submit();
	}
	function doReset() {
		$('#dateFrom').val('');
		$('#dateTo').val('');
		$('#flightNumber').val('');
		$('#immat').val('');
		$('#orig').val('');
		$('#dest').val('');
	}
</script>

<form:form name="form" id="form" method="post" commandName="reportFilter" action="report">
	<input type="hidden" name="action">
	<input type="hidden" name="type">

	<div class="form-group">
		<div class="row">
			<div class="col-lg-6">
				<label><spring:message code="report.ca.transport.date.from" text="!" /></label>
				<div class="input-group date">
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					<form:input path="dateFrom" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy" />
				</div>
			</div>
			<div class="col-lg-6">
				<label><spring:message code="report.ca.transport.date.to" text="!" /></label>
				<div class="input-group date">
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					<form:input path="dateTo" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy" />
				</div>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<div class="row">
			<div class="col-lg-6">
				<label><spring:message code="report.ca.transport.flight.number" text="!" /></label>
				<form:input path="flightNumber" type="text" cssClass="form-control textfield"/>
			</div>
			<div class="col-lg-6">
				<label><spring:message code="report.ca.transport.immat" text="!" /></label>
				<form:input path="immat" type="text" cssClass="form-control textfield"/>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<div class="row">
			<div class="col-lg-6">
				<label><spring:message code="report.ca.transport.orig" text="!" /></label>
				<form:input path="orig" type="text" cssClass="form-control textfield"/>
			</div>
			<div class="col-lg-6">
				<label><spring:message code="report.ca.transport.dest" text="!" /></label>
				<form:input path="dest" type="text" cssClass="form-control textfield"/>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<div class="row">
			<div class="col-lg-12 text-right">
				<a onclick="doReset()" class="btn btn-w-m btn-default text-uppercase" style="color: #fff;">
					<i class="fa fa-undo"></i> <spring:message code="button.reset" text="!" /></a> 
				<a onclick="doReport()" class="btn btn-w-m btn-success text-uppercase" style="color: #fff;">
					<i class="fa fa-check"></i> <spring:message code="button.go" text="!" />
				</a>
			</div>
		</div>
	</div>
</form:form>