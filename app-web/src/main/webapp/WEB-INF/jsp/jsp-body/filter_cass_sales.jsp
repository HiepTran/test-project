<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style type="text/css">
	.chosen-container-single .chosen-single {
		background: #fff;
		border-radius: 3px;
		color: #444;
		display: block;
		height: 23px;
		line-height: 24px;
		overflow: hidden;
		padding: 0 0 0 8px;
		position: relative;
		text-decoration: none;
		white-space: nowrap;
		box-shadow: none;
		border: 1px solid #e5e6e7;
	}
	
	.datepicker { 
		z-index: 9999 !important;
	}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$('.input-group.date').datepicker({
			todayBtn : "linked",
			keyboardNavigation : false,
			forceParse : false,
			calendarWeeks : true,
			autoclose : true,
			format : "dd/mm/yyyy"
		});

		$(".chosen-select").chosen({
	    	  "disable_search": true
		});

	});

	function doReport() {
		document.forms[0].elements['action'].value = "GO";
		document.forms[0].elements['type'].value = "CASS_SALES";
		document.forms[0].submit();
	}
	function doReset() {
		$('#dateFrom').val('');
		$('#dateTo').val('');
		$('#agtn').val('');
		$('#isoc').val('');
		$('#isoc').trigger('chosen:updated');
		$('#channel').val('');
		$('#channel').trigger('chosen:updated');
	}
</script>


<form:form name="form" id="form" method="post" commandName="reportFilter" action="report">
	<input type="hidden" name="action">
	<input type="hidden" name="type">

	<div class="form-group">
		<div class="row">
			<div class="col-lg-4">
				<label><spring:message code="report.country" text="!" /></label>
				<form:select path="isoc" class="chosen-select" cssStyle="width:100%;">
					<option value=""><spring:message code="commom.all" text="!" /></option>
					<c:forEach items="${listIsoc}" var="elm">
						<option value="${elm.isoc}"
							<c:if test="${elm.isoc == reportFilter.isoc}" >selected="selected"</c:if>>
							<spring:message code="isoc.${elm.isoc}" text="notFound" />
						</option>
					</c:forEach>
				</form:select>
			</div>
			<div class="col-lg-4">
				<label><spring:message code="report.agtn" text="!" /></label>
				<form:input path="agtn" type="text" cssClass="form-control textfield" />
			</div>
			<div class="col-lg-4">
				<label><spring:message code="awb.channel" text="!" /></label>
				<form:select path="channel" cssClass="chosen-select" cssStyle="width:100%;">
					<option value=""
						<c:if test="${reportFilter.channel == ''}" >selected="selected"</c:if>>
						<spring:message code="commom.all" text="!" />
					</option>
					<option value="HOT"
						<c:if test="${reportFilter.channel == 'HOT'}" >selected="selected"</c:if>>
						<spring:message code="awb.list.HOT" text="!" />
					</option>
					<option value="OWN"
						<c:if test="${reportFilter.channel == 'OWN'}" >selected="selected"</c:if>>
						<spring:message code="awb.list.OWN" text="!" />
					</option>
				</form:select>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<div class="row">
			<div class="col-lg-4">
				<label><spring:message code="report.date.from" text="!" /></label>
				<div class="input-group date">
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					<form:input path="dateFrom" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy" />
				</div>
			</div>
			<div class="col-lg-4">
				<label><spring:message code="report.date.to" text="!" /></label>
				<div class="input-group date">
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					<form:input path="dateTo" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy" />
				</div>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<div class="row">
			<div class="col-lg-12 text-right">
				<a onclick="doReset()" class="btn btn-w-m btn-default text-uppercase" style="color: #fff;">
					<i class="fa fa-undo"></i> <spring:message code="button.reset" text="!" /></a> 
				<a onclick="doReport()" class="btn btn-w-m btn-success text-uppercase" style="color: #fff;">
					<i class="fa fa-check"></i> <spring:message code="button.go" text="!" />
				</a>
			</div>
		</div>
	</div>
</form:form>