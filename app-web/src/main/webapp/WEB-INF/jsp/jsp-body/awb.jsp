<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>

<style type="text/css">
	.chosen-container-single .chosen-single {
         background: none;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
</style>
<script type="text/javascript">
	document.getElementById("lta").className = "active";
	document.getElementById("listLTA").className = "active";

	var pageNo = ${awbListFilter.page};
	$(function () {
	    $.ajax({
	    	url : "<spring:url value='/secure/awb/loadPage'/>",
	        data: { pageNo: pageNo },
	        cache : false,
	        success: function (response) {
	            $("#_results").html(response);
	        }
	    });
	    
	    $(".chosen-select").chosen({
	    	  "disable_search": true
  		});
  		
	    $('.input-group.date').datepicker({
			todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
			format: "dd/mm/yyyy"
	    });

	    $("#formAwb").css({
	        'margin-left': function () { //Horizontal centering
	            return -($(this).width() / 2);
	        }
	    });
	});

	function ConformDelete(id) {
        swal({
            title: "<spring:message code="message.delete" text="!"/>" + " [" + id + "] ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<spring:message code="button.delete" text="!"/>",
            cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
            closeOnConfirm: false
        }, function () {
       	 	document.forms[0].elements['action'].value='DELETE';
			 document.forms[0].elements['id'].value=id;
			 document.forms[0].submit();
        });
    };

    function modalAwb(id) {
		var url = "<c:url value='/secure/awb/view?id='/>" + id;
		jQuery.ajax({
			cache: false,
	     	type:"GET",
	     	url:url,
	     	success:function(html){			    	
   	 			jQuery("#modalAwb").html(html);
			}
		});
	};
</script>
	
<div id="body">
	<form:form name="form" id="form" method="post" commandName="awbListFilter" action="awb">
	<input type="hidden" name="action">
	<input type="hidden" name="id">
	
		<div class="row">
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="awb.awb" text="!"/></label>
					<form:input path="lta" type="text" cssClass="form-control textfield"/>
 				</div>
			</div>
			
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="awb.list.date.from" text="!"/></label>
					<div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <form:input path="dateFrom" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
				</div>
			</div>
			
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="awb.list.date.to" text="!"/></label>
					<div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <form:input path="dateTo" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
				</div>
			</div>
			
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="awb.channel" text="!Nb vol"/></label>
					<form:select path="channel" cssClass="chosen-select" cssStyle="width:100%;">
						<option value="" <c:if test="${awbListFilter.channel == ''}" >selected="selected"</c:if>>
							<spring:message code="commom.all" text="!"/>						
						</option>
						<option value="HOT" <c:if test="${awbListFilter.channel == 'HOT'}" >selected="selected"</c:if>>
							<spring:message code="awb.list.HOT" text="!"/>						
						</option>
						<option value="OWN" <c:if test="${awbListFilter.channel == 'OWN'}" >selected="selected"</c:if>>
							<spring:message code="awb.list.OWN" text="!"/>						
						</option>
					</form:select>
 				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="awb.agent" text="!"/></label>
					<form:input path="agtn" type="text" cssClass="form-control textfield"/>
 				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="awb.rdv" text="!"/></label>
					<form:input path="rdv" type="text" cssClass="form-control textfield text-uppercase"/>
 				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="awb.list.orig" text="!"/></label>
					<form:input path="orig" type="text" cssClass="form-control textfield text-uppercase"/>
 				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="awb.list.dest" text="!"/></label>
					<form:input path="dest" type="text" cssClass="form-control textfield text-uppercase"/>
 				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12 text-right">
				<button onclick="javascript:doSubmit('RESET');" class="btn btn-w-m btn-default text-uppercase">
					<i class="fa fa-undo"></i> <spring:message code="button.reset" text="!"/>
				</button>
				<button onclick="javascript:doSubmit('GO');" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-check"></i> <spring:message code="button.go" text="!"/>
				</button>
				<a href="<c:url value="/secure/awb/excel"></c:url>" target="_blank" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-file-excel-o"></i> <spring:message code="button.excel" text="!"/>
				</a>
			</div>
		</div>
	
		&nbsp;
		<div class="row">
			<div class="col-lg-12">
				<div id="_results"></div>
			</div>
		</div>
	</form:form>
	
	<div class="row">
		<div class="col-lg-12">
			<!-- Modal create Manifest -->
			<div class="modal inmodal" id="formAwb" tabindex="-1" role="dialog" aria-hidden="true">
				<div id="modalAwb"></div>
			</div>
		</div>
	</div>
</div>