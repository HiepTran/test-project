<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style type="text/css">
	.chosen-container-single .chosen-single {
         background: none;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
	.datepicker { 
		z-index: 9999 !important;
	}
	.chosen-container {
        width: 100% !important;
    }
</style>

<script type="text/javascript">
	document.getElementById("param").className = "active";
	document.getElementById("param_tax").className = "active";

	$(document).ready(function() {
		 $('#add').click(function(){
		    	$('#form-add').submit();
		    });

		    if(${errorAddModal == true}){
				$('#formTax').modal("show");
			}
			
		$('.chosen-select').chosen();
		
		$('.input-group.date').datepicker({
			todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
			format: "dd/mm/yyyy"
	    });

		$('.table-result').dataTable({
 	 		"dom": 'T<"clear">lfrtip',
 	        "sDom": "<'row'<'col-sm-6 text-left'><'col-sm-6 text-right'>r>" + "t" + "<'row'<'col-sm-6'><'col-sm-6'p>>",
 			"bSort": false,
 			"paginate": false
 	   	});

		<c:if test="${errorAdd == true}">
			bfShow("add-box","a-add-${id}", ${id}); showFormAdd("add-box", ${id});

			setTimeout(function() {
		        toastr.options = {
		            closeButton: true,
		            progressBar: true,
		            showMethod: 'slideDown',
		            timeOut: 20000
		        };
		        toastr.error('<spring:message code="tax.error" text="!"/>');	
		    }, 1300);
		</c:if>

		<c:if test="${errorEdit == true}">
			bfShow("edit-box","a-edit-${id}", ${id}); showForm("edit-box", ${id});
	
			setTimeout(function() {
		        toastr.options = {
		            closeButton: true,
		            progressBar: true,
		            showMethod: 'slideDown',
		            timeOut: 20000
		        };
		        toastr.error('<spring:message code="tax.error" text="!"/>');	
		    }, 1300);
		</c:if>
		
	});

	function showForm(divId, id) {
 	    $.ajax({
 	        url: "<spring:url value='/secure/tax/model/edit/'/>" + id,
 	        cache: false,
 	        success: function (reponse) {
 	            $("div#edit-result").html(reponse);
 	        }
 	    });
 	    show(divId);
 	}

	function showFormAdd(divId, id) {
 	    $.ajax({
 	        url: "<spring:url value='/secure/tax/model/add/'/>" + id,
 	        cache: false,
 	        success: function (reponse) {
 	            $("div#add-result").html(reponse);
 	        }
 	    });
 	    show(divId);
 	}
 	
</script>

<div id="body">
	<form:form id="form" name="form" method="post" commandName="taxFilter" action="tax">
		<input name="action" type="hidden">
		<input name="id" type="hidden">
		
		<!-- modify -->
		<div id="edit-box" class="boxcss" style="display: block;">
			<div id="edit-result"></div>
	    </div>
    
	    <!-- add -->
		<div id="add-box" class="boxcss" style="display: block;">
			<div id="add-result"></div>
	    </div>
    
		<div class="row">
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="tax.country" text="!"/></label>
					<form:select path="isoc" class="chosen-select" cssStyle="width:100%">
						<option value="" <c:if test="${'' == taxFilter.isoc}">selected="selected"</c:if>><spring:message code="commom.all" text="!"/></option>
				 		<c:forEach items="${listIsoc}" var="elm">
							<option value="${elm.isoc}"
								<c:if test="${elm.isoc == taxFilter.isoc}" >selected="selected"</c:if>>${elm.isoc}
							</option>
						</c:forEach>
					</form:select>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="tax.code" text="!"/></label>
					<form:input path="code" maxlength="3" type="text" cssClass="form-control textfield text-uppercase"/>
				</div>
			</div>
			
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="tax.sub.code" text="!"/></label>
					<form:input path="subCode" type="text" cssClass="form-control textfield text-uppercase"/>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="tax.item" text="!"/></label>
					<form:input path="item" type="text" cssClass="form-control textfield text-uppercase"/>
				</div>
			</div>
		</div>
		
		<%-- <div class="row">
			<div class="col-lg-3 date">
				<div class="form-group">
					<label><spring:message code="tax.date.from" text="!Date execution"/></label> 
					<div class="input-group date">
	                       <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                       <form:input path="dateFrom" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
				</div>
			</div>
			
			<div class="col-lg-3 date">
				<div class="form-group">
					<label><spring:message code="tax.date.to" text="!Date execution"/></label> 
					<div class="input-group date">
	                       <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                       <form:input path="dateTo" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
				</div>
			</div>
		</div> --%>
		
		<div class="row">
			<div class="col-lg-4">
				<button type="button" class="btn btn-w-m btn-success text-uppercase" data-toggle="modal" data-target="#formTax">
					<i class="fa fa-plus-square"></i> <spring:message code="button.create" text="!"/>
				</button>
			</div>
			
			<div class="col-lg-8 text-right">
				<button onclick="javascript:doSubmit('RESET');" class="btn btn-w-m btn-default text-uppercase">
					<i class="fa fa-undo"></i> <spring:message code="button.reset" text="!"/>
				</button>
				<button onclick="javascript:doSubmit('GO');" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-check"></i> <spring:message code="button.go" text="!"/>
				</button>
				<a href="<c:url value="/secure/tax/excel"></c:url>" target="_blank" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-file-excel-o"></i> <spring:message code="button.excel" text="!"/>
				</a>
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">
		    <div class="col-lg-6">
		        <label class="label label-success" style="font-size:14px">${fn:length(listTax)} <spring:message code="message.result" text="!Result(s)"/></label>
		    </div> 
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">
		    <div class="col-lg-12">
				<table class="table table-striped table-bordered table-hover table-result" style="width:100%;">
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th class="text-center"><spring:message code="commom.action" text="!"/></th>
							<th><spring:message code="tax.country" text="!"/></th>
							<th><spring:message code="tax.code" text="!"/></th>
							<th><spring:message code="tax.sub.code" text="!"/></th>
							<th><spring:message code="tax.item" text="!"/></th>
							<th><spring:message code="tax.currency" text="!"/></th>
							<th><spring:message code="tax.unit" text="!"/></th>
							<th class="text-right"><spring:message code="tax.amount" text="!"/></th>
							<th><spring:message code="tax.date.start" text="!"/></th>
							<th><spring:message code="tax.date.end" text="!"/></th>
							<th><spring:message code="tax.description" text="!"/></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${listTax}" var="elm" varStatus="stt">
							<fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateEnd}" var="dateEnd"/>
						
							<tr id="${elm.id}">
								<td class="text-center">${stt.index + 1}</td>
								<td class="text-center text-nowrap">
									<c:choose>
										<c:when test="${dateEnd == '31/12/9999' }">
											<a id='a-add-${elm.id}' onclick='bfShow("add-box","a-add-${elm.id}", "${elm.id}"); showFormAdd("add-box", "${elm.id}")' 
												title="<spring:message code="message.add" text="!Edit"/>"><i class="fa fa-2x fa-plus-square-o"></i>
											</a>
										</c:when>
										<c:otherwise>
											<i style="color: #e7eaec" class="fa fa-2x fa-plus-square-o"></i>
										</c:otherwise>
									</c:choose>
									<a id='a-edit-${elm.id}' onclick='bfShow("edit-box","a-edit-${elm.id}", "${elm.id}"); showForm("edit-box", "${elm.id}")' 
										title="<spring:message code="message.modify" text="!Edit"/>"><i class="fa fa-2x fa-edit"></i>
									</a>
								</td>
								<td>${elm.isoc}</td>
								<td>${elm.code}</td>
								<td>${elm.subCode}</td>
								<td>${elm.item}</td>
								<td>${elm.cutp}</td>
								<td><spring:message code="tax.${elm.unit}" text="!"/></td>
								<td class="text-right"><fmt:formatNumber pattern="#,##0.00" value="${elm.amount}"/></td>
								<td><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateStart}" /></td>
								<td>
									<c:if test="${dateEnd != '31/12/9999' }">
										<fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateEnd}"/>
									</c:if>
								</td>
								<td>${elm.description}</td>
							</tr>
						</c:forEach>	
					</tbody>
				</table>
			</div>
		</div>
	</form:form>
	
	<div class="modal inmodal" id="formTax" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" style="margin-left: 300px;">
			<div class="modal-content animated bounceInRight" style="width: 950px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
					<h5 class="modal-title"><spring:message code="tax.title" text="!"/></h5>
				</div>
				<div class="modal-body">
				<form:form class="form-horizontal" id="form-add" name="form-add" method="post" commandName="taxFilter">
					<input type="hidden" name="action" value="ADD_MODAL">
					
						<div class="form-group">
							<div class="row">
								<div class="col-lg-12">
								 	<form:errors path="typeExt" cssClass="text-danger"/>
								 </div>
							 </div>
						 </div>
						 
						<div class="form-group">
							<div class="row">
							 	<div class="col-lg-3">
								 	<label><spring:message code="tax.country" text="!"/></label>
								 	<form:select path="isocExt" cssClass="chosen-select" cssStyle="width:100%;">
								 		<c:forEach items="${listIsoc}" var="elm">
											<option value="${elm.isoc}"
												<c:if test="${elm.isoc == taxFilter.isocExt}" >selected="selected"</c:if>>${elm.isoc}
											</option>
										</c:forEach>
									</form:select>
								 </div>
								<div class="col-lg-3">
								 	<label><spring:message code="tax.code" text="!"/></label>
								 	<form:input path="codeExt" maxlength="3" class="form-control textfield text-uppercase"/>
								 	<form:errors path="codeExt" cssClass="text-danger"/>
								 </div>
								 <div class="col-lg-3">
								 	<label><spring:message code="tax.sub.code" text="!"/></label>
								 	<form:input path="subCodeExt" class="form-control textfield text-uppercase"/>
								 	<form:errors path="subCodeExt" cssClass="text-danger"/>
								 </div>
								 <div class="col-lg-3">
								 	<label><spring:message code="tax.item" text="!"/></label>
								 	<form:input path="itemExt" class="form-control textfield text-uppercase"/>
								 	<form:errors path="itemExt" cssClass="text-danger"/>
								 </div>
							 </div>
						 </div>
						 
						 <div class="form-group">
							<div class="row">
							 	<div class="col-lg-3">
								 	<label><spring:message code="tax.currency" text="!"/></label>
								 	<form:select path="cutpExt" cssClass="chosen-select" cssStyle="width:100%;">
										<option value="CHF" <c:if test="${'CHF' == taxFilter.cutpExt}">selected="selected"</c:if>>CHF</option>
										<option value="EUR" <c:if test="${'EUR' == taxFilter.cutpExt}">selected="selected"</c:if>>EUR</option>
										<option value="NGN" <c:if test="${'NGN' == taxFilter.cutpExt}">selected="selected"</c:if>>NGN</option>
										<option value="USD" <c:if test="${'USD' == taxFilter.cutpExt}">selected="selected"</c:if>>USD</option>
										<option value="XAF" <c:if test="${'XAF' == taxFilter.cutpExt}">selected="selected"</c:if>>XAF</option>
										<option value="XOF" <c:if test="${'XOF' == taxFilter.cutpExt}">selected="selected"</c:if>>XOF</option>
									</form:select>
								 </div>
								<div class="col-lg-3">
								 	<label><spring:message code="tax.unit" text="!"/></label>
								 	<form:select path="unitExt" cssClass="chosen-select" cssStyle="width:100%;">
										<option value="A" <c:if test="${'A' == taxFilter.unitExt}">selected="selected"</c:if>>
											<spring:message code="tax.A" text="!"/>
										</option>
										<option value="K" <c:if test="${'K' == taxFilter.unitExt}">selected="selected"</c:if>>
											<spring:message code="tax.K" text="!"/>
										</option>
										<option value="P" <c:if test="${'P' == taxFilter.unitExt}">selected="selected"</c:if>>
											<spring:message code="tax.P" text="!"/>
										</option>
									</form:select>
								 </div>
								 <div class="col-lg-3">
								 	<label><spring:message code="tax.date.start" text="!"/></label>
								 	<div class="input-group date">
				                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                        <form:input path="dateFromExt" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
									</div>
								 	<form:errors path="dateFromExt" cssClass="text-danger"/>
								 </div>
								 <div class="col-lg-3">
								 	<label><spring:message code="tax.amount" text="!"/></label>
									<form:input path="amountExt" class="form-control textfield text-right"/>
									<form:errors path="amountExt" cssClass="text-danger"/>
								 </div>
							 </div>
						 </div>
						 
						 <div class="form-group">
							<div class="row">
								 <div class="col-lg-12">
								 	<label><spring:message code="tax.description" text="!"/></label>
									<form:input path="descriptionExt" class="form-control textfield"/>
								 </div>
							 </div>
						 </div>
						 
					</form:form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<button id ="add" type="button" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-plus-square "></i> <spring:message code="button.create" text="!Create"/>
					</button>
				</div>
			</div>
		</div>
	</div>	
</div>