<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style>
	.chosen-container-single .chosen-single {
		background: none;
		border-radius: 3px;
		color: #444;
		display: block;
		height: 23px;
		line-height: 24px;
		overflow: hidden;
		padding: 0 0 0 8px;
		position: relative;
		text-decoration: none;
		white-space: nowrap;
		box-shadow: none;
		border: 1px solid #e5e6e7;
	}
	
	.datepicker { 
		z-index: 9999 !important;
	}
</style>

<script type="text/javascript">
	$(document).ready(function() {
		$('.input-group.date').datepicker({
			minViewMode: 1,
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            todayHighlight: true,
			format: "mm/yyyy"
		});

		$('.chosen-select').chosen();

	});

	function doReport() {
		document.forms[0].elements['action'].value = "GO";
		document.forms[0].elements['type'].value = "RMK";
		document.forms[0].submit();
	}
	function doReset() {
		$('#dateFrom').val('');
	}
</script>

<form:form name="form" id="form" method="post" commandName="reportFilter" action="report">
	<input type="hidden" name="action">
	<input type="hidden" name="type">
		
	<div class="form-group">
		<div class="row">
			<div class="col-lg-4">
				<label><spring:message code="report.month" text="!" /></label>
				<div class="input-group date">
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					<form:input path="dateFrom" type="text" cssClass="form-control textfield" placeholder="mm/yyyy" />
				</div>
			</div>
		</div>
	</div>
			
	<div class="form-group">
		<div class="row">
			<div class="col-lg-12 text-right">
				<a onclick="doReset()" class="btn btn-w-m btn-default text-uppercase" style="color: #fff;">
					<i class="fa fa-undo"></i> <spring:message code="button.reset" text="!" /></a> 
				<a onclick="doReport()" class="btn btn-w-m btn-success text-uppercase" style="color: #fff;">
					<i class="fa fa-check"></i> <spring:message code="button.go" text="!" />
				</a>
			</div>
		</div>
	</div>
</form:form>