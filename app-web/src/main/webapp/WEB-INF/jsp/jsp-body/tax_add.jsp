<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript">
	function doEditForm(action, id){
		document.forms[1].elements['action'].value=action;
		document.forms[1].elements['id'].value=id;
		document.forms[1].submit();
	}

	$(document).ready(function() {
		$('.input-group.date').datepicker({
			todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
			format: "dd/mm/yyyy"
	    });
	});
</script>

<form:form name="form_modify" id="form_modify" method="post" commandName="taxFilter" action="tax">
	<input name="action" type="hidden">
	<input name="id" type="hidden">
	
	<div class="panel panel-success" style="margin-top:9px;">
         <div class="panel-heading" style="padding: 4px;">
             <div class="row">
                 <div class="col-lg-11">
                     <h3><spring:message code="message.add" text="!"/> ${tax.code }</h3>
                 </div>
                 <div class="col-lg-1 text-right">
                     <i class="fa fa-2x fa-times-circle" onclick="javascript:hide('add-box');" style="cursor:pointer;"></i>
                 </div>
             </div>
         </div>
		
         <div class="panel-body" style="padding:5px;">
           	<div class="form-group">
				<div class="row">
					<div class="col-lg-3">
						<label><spring:message code="tax.country" text="!"/></label>
						<input value="${tax.isoc}" readonly="readonly" class="form-control textfield"/>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="tax.code" text="!"/></label>
						<input value="${tax.code}" readonly="readonly" class="form-control textfield"/>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="tax.sub.code" text="!"/></label>
						<input value="${tax.subCode}" readonly="readonly" class="form-control textfield"/>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="tax.item" text="!"/></label>
					 	<input value="${tax.item}" readonly="readonly" class="form-control textfield"/>
					</div>	
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-lg-3">
						<label><spring:message code="tax.currency" text="!"/></label>
						<input value="${tax.cutp}" readonly="readonly" class="form-control textfield"/>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="tax.unit" text="!"/></label>
						<spring:message code="tax.${tax.unit}" text="!" var ="unitEdit"/>
						<input value="${unitEdit}" readonly="readonly" class="form-control textfield"/>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="tax.date.start" text="!"/></label>
						<fmt:formatDate pattern="dd/MM/yyyy" value="${tax.dateStart}" var="dateStart" />
						<input value="${dateStart}" readonly="readonly" class="form-control textfield"/>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="tax.amount" text="!"/></label>
						<input value="${tax.amount}" readonly="readonly" class="form-control textfield text-right"/>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-lg-12">
						<label><spring:message code="tax.add.new" text="!"/></label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-lg-3">
						<label><spring:message code="tax.date.start" text="!"/></label>
						<div class="input-group date">
	                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                        <form:input path="dateFromAdd" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
						</div>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="tax.amount" text="!"/></label>
						<form:input path="amountAdd" class="form-control textfield text-right"/>
					</div>
					<div class="col-lg-6">
						<label><spring:message code="tax.description" text="!"/></label>
						<form:input path="descriptionAdd" class="form-control textfield"/>
					</div>
				</div>
			</div>	 
			<div class="row">
				<div class="col-lg-12 text-right">
		  			<a onclick="doEditForm('ADD','${tax.id}')" class="btn btn-w-m btn-success text-uppercase" type="submit">
		  				<i class="fa fa-plus-square"></i> <spring:message code="button.add" text="!Go"/>
		  			</a>
				</div>
			</div>
         </div>
     </div>
</form:form>