<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>

<style type="text/css">
	.chosen-container-single .chosen-single {
         background: #fff;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
     
     .chosen-container {
        width: 100% !important;
    }
</style>

<script type="text/javascript">
	document.getElementById("userrole").className = "active";
	document.getElementById("userrole_user").className = "active";
	
	function fnLoadPage() {
		$.ajax({
			url : "<spring:url value='/secure/user/loadpage'/>",
		    cache : false,
		    success: function (response) {
		        $("#_results").html(response);
		    }
		});
	};
	
	function doCreateForm(){
		$.ajax({
			url: "<spring:url value='/secure/user/create/error.json'/>",
			type: "POST",
			cache: false,
			data: $('#form-add').serialize(),
			success: function(result){
				if(result.errCodeCreate > 0){
		           	 $('#err_usernameAdd').html('');
		           	 $('#err_passwordAdd').html('');
		           	 $('#err_retypePasswordAdd').html('');
		           	 $('#err_airlineCodeAdd').html('');
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++)
	               	 {
	                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
	        	}else{
					$('#form-add').submit();
	        	}
			}
		});
	}
	
	$(function(){
	    $.ajax({
	    	url : "<spring:url value='/secure/user/loadpage'/>",
	        cache : false,
	        success: function (response) {
	            $("#_results").html(response);
	        }
	    });

	    $(".chosen-select").chosen({
	    	  "disable_search": true
		});
	});
	
	
	function doEditUser(id) {
 	    $.ajax({ 
 	     	url: "<spring:url value='/secure/user/ajax/'/>" + id,
 	        cache: false,
 	        success: function (reponse) {
 	            $("div#modify-result").html(reponse);
 	        }
 	    });
 	}
	
	function doAddButton(userId){
		$.ajax({
			url : "<spring:url value='/secure/user/add/" + userId +"'/>",
			type : "POST",
			cache: false,
			data : $('#form_add_roleuser').serialize(),
			success: function(reponse){
				$("div#modify-result").html(reponse);
				fnLoadPage();
			}
		});
	}
	
	function ConformDelete(roleId, userId, roleName) {
	    swal({
	        title: "<spring:message code="message.delete" text="!"/>" + " [" + roleName +"] ?",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "<spring:message code="button.delete" text="!"/>",
	        cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
	        closeOnConfirm: false
	    }, function () {
	    	 $('.sweet-alert').hide();
        	 $('.sweet-overlay').hide();
        	 document.forms[2].elements['action'].value="DELETE";
			 document.forms[2].elements['idUser'].value=userId;
        	 doDelete(roleId);
	    });
	};
	
	function doDelete(roleId){
		$.ajax({
			url: "<spring:url value='/secure/user/delete/" + roleId +"'/>",
			type: "POST",
			cache: false,
			data: $('#form_add_roleuser').serialize(),
			success: function(reponse){
				$("div#modify-result").html(reponse);
				fnLoadPage();
			}
		});
	}
</script>

<div id="body">
<form:form id="form" name="form" method="post" modelAttribute="userFilter">
<input name="action" type="hidden"> 
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label><spring:message code="user.usename" text="!Username"/></label>
					<form:input path="username" cssClass="form-control"/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<button id="btnCreate" type="button" class="btn btn-w-m btn-success text-uppercase" data-toggle="modal" data-target="#formCreateUser">
				<i class="fa fa-plus-square"> <spring:message code="button.create" text="!Create User"/></i>
				</button>
			</div>
			<div class="col-sm-8 text-right">
				<button id="btnReset" onclick="javascript:doSubmit('RESET')" type="button" class="btn btn-w-m btn-default text-uppercase">
					<i class="fa fa-undo"> <spring:message code="button.reset"/></i>
				</button>
				<button id="btnSearch" onclick="javascript:doSubmit('GO');" type="button" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-search"></i> <spring:message code="button.go" text="!"/>
				</button>
				
				<a href="<c:url value="/secure/user/excel"></c:url>" target="_blank" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-file-excel-o"></i> <spring:message code="button.excel" text="!"/>
				</a>		
			</div>
		</div>	
		<div class="col-sm-12">
			&nbsp;
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div id="_results"></div>
			</div>
		</div>
</form:form>

<div class="modal inmodal" id="formEdit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" >
		<div class="modal-content animated bounceInRight" style="width: 900px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
				<h5 class="modal-title"><spring:message code="user.table.modify" text="!"/></h5>
			</div>
			<div class="modal-body">
				<div id="modify-result"></div>
			</div>
		</div>
	</div>
</div>
	
<div class="modal inmodal" id="formCreateUser" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" style="width:800px">
			<div class="modal-content animated bounceInRight">
				<div class="modal-body">
				<form:form id="form-add" name="form-add" method="post" commandName="userFilter" action="user/create">
					<input type="hidden" name="action" value="CREATE">	
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="user.usename" text="!User name"/></label>
								<form:input path="usernameAdd" type="text" cssClass="form-control textfield"/>
								<label id="err_usernameAdd" class="text-danger"></label>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="user.password" text="!Password"/></label>
								<form:password path="passwordAdd" cssClass="form-control textfield disablecopypaste"/>
								<label id="err_passwordAdd" class="text-danger"></label>
							</div>
							
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="user.retypepassword" text="!Confirm password"/></label>
								<form:password path="retypePasswordAdd"  cssClass="form-control textfield disablecopypaste"/>
								<label id="err_retypePasswordAdd" class="text-danger"></label>
							</div>
						</div>
						
					</div>
					
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="user.name" text="!Name"/></label>
								<form:input path="nameAdd" cssClass="form-control textfield "/>
								<label id="err_nameAdd" class="text-danger"></label>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="user.email" text="!Email"/></label>
								<form:input path="emailAdd" type="text" cssClass="form-control textfield "/>
								<label id="err_emailAdd" class="text-danger"></label>
							</div>
						</div>
						
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="user.tel" text="!Telephone"/></label>
								<form:input path="telephoneAdd" type="text" cssClass="form-control textfield "/>
								<label id="err_telephoneAdd" class="text-danger"></label>
							</div>
						</div>
						
						
					</div>
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="user.airlinecode" text="!Airline code"/></label>
								<form:input path="airlineCodeAdd" type="text" cssClass="form-control textfield" readonly="true" value="${airlineParam.airlineCode }"/>
								<label id="err_airlineCodeAdd" class="text-danger"></label>
							</div>
						</div>
						
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="user.countrycode" text="!Country code"/></label>
								<form:select id="selectCountryCode" path="countryCodeAdd" cssClass="chosen-select" cssStyle="width:100%;">
									<c:forEach items="${lstCritIsoc }" var="elm">
										<option value="${elm.isoc }" <c:if test="${elm.isoc == airlineParam.isoc}"> selected="selected"</c:if> >${elm.isoc }</option>
									</c:forEach>
								</form:select>
							</div>
						</div>
						
						<div class="col-sm-4">
							<div class="form-group">
								<label><spring:message code="user.language" text="!Language"/></label>
								<form:select path="languageAdd" cssClass="chosen-select" cssStyle="width:100%;">
								<form:option value="en"><spring:message code="language.en" text="!English"/></form:option>
								<form:option value="fr"><spring:message code="language.fr" text="!French"/></form:option>
								</form:select>
							</div>
						</div>
					</div>
					</form:form>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<button id ="add" type="button" onclick="doCreateForm();" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-plus-square "></i> <spring:message code="button.create" text="!Create"/>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>