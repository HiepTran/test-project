<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script type="text/javascript">
	document.getElementById("manifest").className = "active";
	document.getElementById("listManifest").className = "active";

	var pageNo = ${flownFilter.page};
	$(function () {
		$('.input-group.date').datepicker({
			todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
			format: "dd/mm/yyyy"
	    });
	    
	    $.ajax({
	    	url : "<spring:url value='/secure/flown/ajax'/>",
	        data: { pageNo: pageNo },
	        cache : false,
	        success: function (response) {
	            $("#_results").html(response);
	        }
	    });
	});
</script>
	
<div id="body">
	<form:form name="form" id="form" method="post" commandName="flownFilter">
		<input type="hidden" name="action">
		<input type="hidden" name="id">
		
		<div class="row">
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="manifest.owner" text="!"/></label>
					<form:input path="tacn" type="text" cssClass="form-control textfield"/>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="manifest.no" text="!"/></label>
					<form:input path="manifestNo" type="text" cssClass="form-control textfield text-uppercase"/>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="manifest.orig" text="!"/></label>
					<form:input path="orig" maxlength="3" type="text" cssClass="form-control textfield text-uppercase"/>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="manifest.dest" text="!"/></label>
					<form:input path="dest" maxlength="3" type="text" cssClass="form-control textfield text-uppercase"/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="manifest.flight.from" text="!"/></label>
					<div class="input-group date">
	                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                      <form:input path="dateFrom" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="manifest.flight.to" text="!"/></label>
					<div class="input-group date">
	                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                      <form:input path="dateTo" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
				</div>
			</div>
		</div> 
		<div class="row">
			<div class="col-lg-12 text-right">
				<button onclick="javascript:doSubmit('RESET');" class="btn btn-w-m btn-default text-uppercase">
					<i class="fa fa-undo"></i> <spring:message code="button.reset" text="!"/>
				</button>
				<button onclick="javascript:doSubmit('GO');" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-check"></i> <spring:message code="button.go" text="!"/>
				</button>
			</div>
		</div>
					
		<div class="row">
			<div class="col-lg-12">
				<div id="_results"></div>
			</div>
		</div>
	</form:form>
</div>