<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
   
<style type="text/css">
	.hr-line-dashed {
	    background-color: #ffffff;
	    border-top: 1px dashed #e7eaec;
	    color: #ffffff;
	    height: 1px;
	    margin: 1px 5px 10px 10px;
	}
	
	.radio-info input[type="radio"]:checked + label::before {
	    border-color: #1c84c6;
	}
	.radio-info input[type="radio"]:checked + label::after {
	    background-color: #1c84c6;
	}
	.detail{
		background-color: #f9f9f9;
	}
	
	.chosen-container-single .chosen-single {
         background: none;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
     .datepicker { 
		z-index: 9999 !important;
	}
</style>

<script type="text/javascript">
	document.getElementById("lta").className = "active";
	document.getElementById("create").className = "active";

	$(document).ready(function() {

		$('.input-group.date').datepicker({
			todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
			format: "dd/mm/yyyy"
	    });
	    
	    $('.chosen-select').chosen({
	    	"disable_search": true
	    });

	    doShowPayment();

	   	$("form").keypress(function (e) {
	       if (e.which == 13) {
	    	   doSubmit('GO');
	       }
	   	});
	});

	function doShowPayment(){
		var mode = $('#modePayment').val();
		if(mode=='CC'){
			$('#payment_inf').css('display','block');
		}else{
			$('#payment_inf').css('display','none');
		}
	}
	
	function doShowCreateCurr(){
		var err = $('#err_cutp').html();
		if(err != null){
			$('#buttonCutp').removeClass("disabled");
		}
	}

	function doValidator(type){
		$.ajax({
			url: "<spring:url value='/secure/awb/error.json'/>",
	        data: $('#form').serialize(), 
	        type: "POST",
	        success: function(result){
	        	if(result.errCode > 0){
	        		 $('#err_tacn').html('');
	        		 $('#err_lta').html('');
	        		 $('#err_dateExecute').html('');
	        		 $('#err_shipName').html('');
	        		 $('#err_consigneeName').html('');
	        		 $('#err_orig').html('');
	        		 $('#err_dest').html('');
	        		 $('#err_flightNumber').html('');
	        		 $('#err_flightDate').html('');
	        		 
	        		 $('#err_quantity1').html('');
	        		 $('#err_weightGross1').html('');
	        		 $('#err_weightCharge1').html('');
	        		 $('#err_unit1').html('');

	        		 $('#err_quantity2').html('');
	        		 $('#err_weightGross2').html('');
	        		 $('#err_weightCharge2').html('');
	        		 $('#err_unit2').html('');

	        		 $('#err_quantity3').html('');
	        		 $('#err_weightGross3').html('');
	        		 $('#err_weightCharge3').html('');
	        		 $('#err_unit3').html('');

	        		 $('#err_quantity4').html('');
	        		 $('#err_weightGross4').html('');
	        		 $('#err_weightCharge4').html('');
	        		 $('#err_unit4').html('');

	        		 $('#err_quantity5').html('');
	        		 $('#err_weightGross5').html('');
	        		 $('#err_weightCharge5').html('');
	        		 $('#err_unit5').html('');
	        		 
	        		 $('#err_taxCode1').html('');
	        		 $('#err_taxValue1').html('');

	        		 $('#err_taxCode2').html('');
	        		 $('#err_taxValue2').html('');

	        		 $('#err_taxCode3').html('');
	        		 $('#err_taxValue3').html('');

	        		 $('#err_taxCode4').html('');
	        		 $('#err_taxValue4').html('');

	        		 $('#err_taxCode5').html('');
	        		 $('#err_taxValue5').html('');

	        		 $('#err_taxCode6').html('');
	        		 $('#err_taxValue6').html('');

	        		 $('#err_taxCode7').html('');
	        		 $('#err_taxValue7').html('');

	        		 $('#err_taxCode8').html('');
	        		 $('#err_taxValue8').html('');
	        		 
	        		 $('#err_commPercent').html('');
	        		 $('#err_vatPercent').html('');
	        		 $('#err_nbCart').html('');
	        		 $('#err_dateExp').html('');
	        		 $('#err_nameCart').html('');
	        		 
	        		 $('#err_cutp').html('');
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++) {
	                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
		           	doShowCreateCurr();
	        	}else{
		        		doSubmit(type);
	        	}
	        },
		 	error: function(XMLHttpRequest, textStatus, errorThrown){}
		});
	}
	
	function doCreateForm(){
		$.ajax({
			url: "<spring:url value='/secure/createAwb/curr/error.json'/>",
			type: "POST",
			cache: false,
			data: $('#form-add').serialize(),
			success: function(result){
				if(result.errCodeCreate > 0){
					$('#err_currFromAdd').html('');
					$('#err_currToAdd').html('');
					$('#err_rateAdd').html('');
					$('#err_dateStartAdd').html('');
					var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	for(var i=0;i<len;i++)
	               	{
	                   $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	}
				}
				else{
					$('#form-add').submit();
				}
			}
		});
	}
	
	function doShowModal(){
		$('#currFromAdd').val(document.getElementById('cutp').value);
		$('#dateStartAdd').val(document.getElementById('dateExecute').value);
	}
</script>

<div id="body">
	<form:form name="form" id="form" method="post" commandName="awbFilter">
		<input type="hidden" name="action">
		
		<spring:message code="awb.name" text="!Name" var="name"/>
		<spring:message code="awb.address" text="!Address" var="address"/>
		<spring:message code="awb.tel" text="!Phone" var="phone"/>
		
		<div class="row">
			<div class="col-lg-4">
				<a href="<c:url value="/secure/rdv"/>" class="btn btn-w-m btn-default text-uppercase"><spring:message code="button.back" text="!"/></a>
			</div>
			<div class="col-lg-8 text-right">
				<a onclick="doSubmit('NEW');" class="btn btn-w-m btn-success text-uppercase"><i class="fa fa-file-o"></i>&nbsp;<spring:message code="button.new" text="!"/></a>
				<a onclick="doValidator('GO');" class="btn btn-w-m btn-success text-uppercase"><i class="fa fa-check"></i>&nbsp;<spring:message code="button.valider" text="!"/></a>
			</div>
		</div>
		<div class="row">&nbsp;</div>
		
		<c:if test="${not empty message }">
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<span class="text-success">${message}</span>
					</div>
				</div>
			</div>
		</c:if>
		
		<div class="row">
			<div class="form-group">
				<div class="col-lg-1">
					<label><spring:message code="awb.awb" text="!"/></label>
					<form:input path="tacn" maxlength="3" class="form-control textfield"/>
					<label id="err_tacn" class="text-danger"></label>
				</div>
				<div class="col-lg-2">
					<label>&nbsp;</label>
					<form:input path="lta" maxlength="8" class="form-control textfield"/>
					<label id="err_lta" class="text-danger"></label>
				</div>
				<div class="col-lg-3">
					<label><spring:message code="awb.agent" text="!"/></label>
					<form:input path="agtn" value="${rdv.agtn}" readonly="true" maxlength="11" class="form-control textfield"/>
                    <label id="err_agtn" class="text-danger"></label>
				</div>
				<div class="col-lg-3">
					<label><spring:message code="awb.rdv" text="!"/></label>
					<form:input path="rdv" value="${rdv.rdv}" readonly="true" class="form-control textfield"/>
                    <label id="err_rdv" class="text-danger"></label>
				</div>
				<div class="col-lg-3 date">
					<label><spring:message code="awb.date.execution" text="!Date execution"/></label> 
					<div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <form:input id="dateExecute" path="dateExecute" type="text" cssClass="form-control textfield" placeholder="dd/MM/yyyy"/>
					</div>
					<label id="err_dateExecute" class="text-danger"></label>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="form-group">
				<div class="col-lg-6">
					<label><spring:message code="awb.shipper" text="!Shipper"/></label>
					<form:input path="shipName" placeholder="${name}" class="form-control textfield"/>
					<label id="err_shipName" class="text-danger"></label>
				</div>
				<div class="col-lg-6">
					<label><spring:message code="awb.consignee" text="!Consignee's"/></label> 
					<form:input path="consigneeName" placeholder="${name}" class="form-control textfield"/>
					<label id="err_consigneeName" class="text-danger"></label>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
					<form:input path="shipAddress1" placeholder="${phone}" class="form-control textfield"/>
				</div>
				<div class="col-lg-6">
					<form:input path="consigneeAddress1" placeholder="${phone}" class="form-control textfield"/>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
					<form:input path="shipAddress2" placeholder="${address}" class="form-control textfield"/>
				</div>
				<div class="col-lg-6">
					<form:input path="consigneeAddress2" placeholder="${address}" class="form-control textfield"/>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
					<form:input path="shipAddress3" placeholder="${address}" class="form-control textfield"/>
				</div>
				
				<div class="col-lg-6">
					<form:input path="consigneeAddress3" placeholder="${address}" class="form-control textfield"/>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-3">
					<label><spring:message code="awb.orig" text="!"/></label>
					<form:input path="orig" maxlength="3" class="form-control textfield text-uppercase"/>
				</div>
				<div class="col-lg-3">
					<label><spring:message code="awb.dest" text="!"/></label> 
				 	<form:input path="dest" maxlength="3" class="form-control textfield text-uppercase"/>
				</div>
				<div class="col-lg-1">
					<label><spring:message code="awb.flight.number" text="!"/></label> 
					<form:input path="carrier" maxlength="2" class="form-control textfield text-uppercase"/>
				</div>
				<div class="col-lg-2">
					<label>&nbsp;</label>
					<form:input path="flightNumber" class="form-control textfield"/>
				</div>
				<div class="col-lg-3">
					<label><spring:message code="awb.flight.date" text="!"/></label> 
					<div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <form:input path="flightDate" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-3">
					<label id="err_orig" class="text-danger"></label>
				</div>
				<div class="col-lg-3">
				 	<label id="err_dest" class="text-danger"></label>
				</div>
				<div class="col-lg-3">
					<label id="err_flightNumber" class="text-danger"></label>
				</div>
				<div class="col-lg-3">
					<label id="err_flightDate" class="text-danger"></label>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-3">
					<label><spring:message code="awb.cutp" text="!"/></label>
					<%-- <div class="input-group">
						<form:select id="cutp" path="cutp" class="chosen-select" cssStyle="width:100%;">
							<option value="CHF" <c:if test="${'CHF' == awbFilter.cutp}">selected="selected"</c:if>>CHF</option>
							<option value="EUR" <c:if test="${'EUR' == awbFilter.cutp}">selected="selected"</c:if>>EUR</option>
							<option value="NGN" <c:if test="${'NGN' == awbFilter.cutp}">selected="selected"</c:if>>NGN</option>
							<option value="USD" <c:if test="${'USD' == awbFilter.cutp}">selected="selected"</c:if>>USD</option>
							<option value="XAF" <c:if test="${'XAF' == awbFilter.cutp}">selected="selected"</c:if>>XAF</option>
							<option value="XOF" <c:if test="${'XOF' == awbFilter.cutp}">selected="selected"</c:if>>XOF</option>
						</form:select>
						<div id="showCurr" class="input-group-btn" hidden="true">
							<a class="btn btn-white text-uppercase" style="font-size: 10px" onclick="doShowModal()" data-toggle="modal" data-target="#formCreate">
								<i class="fa fa-plus-square "></i> <spring:message code="button.create" text="!Create"/>
							</a>
						</div>
					</div> --%>
                    <div class="input-group">
                    	<form:select id="cutp" path="cutp" class="chosen-select" cssStyle="width:100%;">
							<option value="CHF" <c:if test="${'CHF' == awbFilter.cutp}">selected="selected"</c:if>>CHF</option>
							<option value="EUR" <c:if test="${'EUR' == awbFilter.cutp}">selected="selected"</c:if>>EUR</option>
							<option value="NGN" <c:if test="${'NGN' == awbFilter.cutp}">selected="selected"</c:if>>NGN</option>
							<option value="USD" <c:if test="${'USD' == awbFilter.cutp}">selected="selected"</c:if>>USD</option>
							<option value="XAF" <c:if test="${'XAF' == awbFilter.cutp}">selected="selected"</c:if>>XAF</option>
							<option value="XOF" <c:if test="${'XOF' == awbFilter.cutp}">selected="selected"</c:if>>XOF</option>
						</form:select>
                    	<span class="input-group-btn"> 
                     		<a id="buttonCutp" onclick="doShowModal()" data-toggle="modal" data-target="#formCreate" class="btn btn-success text-uppercase disabled" style="height: 30px;padding-top: 4px;"><spring:message code="button.create" text="!Create"/></a> 
                    	</span>
                    </div>
                                        
					<label id="err_cutp" class="text-danger"></label>
				</div>
				<div class="col-lg-3">
					<div class="row">
						<div class="col-lg-6">
			               	<label><spring:message code="awb.fare" text="!"/></label>
			               	<div class="radio radio-primary">
	                            <input type="radio" name="chargeIndicator" id="radio1" value="P"
	                            <c:if test="${'P' == awbFilter.chargeIndicator}">checked</c:if>>
	                            <label for="radio1">
	                                <spring:message code="awb.fare.pp" text="!"/>
	                            </label>
	                        </div>
	                        <div class="radio radio-primary">
	                            <input type="radio" name="chargeIndicator" id="radio2" value="C"
	                            <c:if test="${'C' == awbFilter.chargeIndicator}">checked</c:if>>
	                            <label for="radio2">
	                                <spring:message code="awb.fare.cc" text="!"/>
	                            </label>
	                        </div>
						</div>
						<div class="col-lg-6">
			               	<label><spring:message code="awb.other" text="!"/></label>
			               	<div class="radio radio-primary">
	                            <input type="radio" name="otherIndicator" id="radio3" value="P" 
	                            <c:if test="${'P' == awbFilter.otherIndicator}">checked</c:if>>
	                            <label for="radio3">
	                                <spring:message code="awb.other.pp" text="!"/>
	                            </label>
	                        </div>
	                        <div class="radio radio-primary">
	                            <input type="radio" name="otherIndicator" id="radio4" value="C"
	                            <c:if test="${'C' == awbFilter.otherIndicator}">checked</c:if>>
	                            <label for="radio4">
	                                <spring:message code="awb.other.cc" text="!"/>
	                            </label>
	                        </div>
						</div>
					</div>
				</div>
				<div class="col-lg-3">
					<label><spring:message code="awb.declared.carrier" text="!"/></label> 
					<form:input path="declaredCarrige" class="form-control textfield"/>
				</div>
				<div class="col-lg-3">
					<label><spring:message code="awb.declared.custom" text="!"/></label> 
					<form:input path="declaredCustom" class="form-control textfield"/>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-12">
					<label><spring:message code="awb.handling" text="!"/></label>
					<form:textarea path="handlingInfor" rows="2" class="form-control textfield"/>
				</div>
			</div>
		</div>
		
		<!-- ---------------------------------------------------------DETAIL-------------------------------------------------------- -->
		<div class="row">
			<div class="col-lg-1">
				<label><spring:message code="awb.rcp" text="!"/></label>
			</div>
			<div class="col-lg-1">
				<label><spring:message code="awb.weight.gross" text="!"/></label> 
			</div>
			<div class="col-lg-1">
				<label><spring:message code="awb.weight.indicator" text="!"/></label> 
			</div>
			<div class="col-lg-1">
				<label><spring:message code="awb.rate" text="!"/></label> 
			</div>
			<div class="col-lg-1">
				<label><spring:message code="awb.weight.charge" text="!"/></label> 
			</div>
			<div class="col-lg-2">
				<label><spring:message code="awb.tarif" text="!"/></label> 
			</div>
			<div class="col-lg-2">
				<label><spring:message code="awb.total" text="!"/></label> 
			</div>
			<div class="col-lg-3">
				<label><spring:message code="awb.description" text="!"/></label> 
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-1">
				<form:input path="quantity1" class="form-control textfield text-right"/>
				<label id="err_quantity1" class="text-danger"></label>
			</div>
			<div class="col-lg-1">
				<form:input path="weightGross1" class="form-control textfield text-right"/>
				<label id="err_weightGross1" class="text-danger"></label>
			</div>
			<div class="col-lg-1">
				<form:select path="weightIndicator1" class="chosen-select" cssStyle="width:100%;">
					 <option value="K" <c:if test="${'K' == awbFilter.weightIndicator1}">selected="selected"</c:if>>K</option>
					 <option value="P" <c:if test="${'P' == awbFilter.weightIndicator1}">selected="selected"</c:if>>P</option>
				</form:select>
			</div>
			<div class="col-lg-1">
				<form:input path="rateClass1" class="form-control textfield text-uppercase"/>
			</div>
			<div class="col-lg-1">
				<form:input path="weightCharge1" class="form-control textfield text-right"/>
				<label id="err_weightCharge1" class="text-danger"></label>
			</div>
			<div class="col-lg-2">
				<form:input path="unit1" class="form-control textfield text-right"/>
				<label id="err_unit1" class="text-danger"></label>
			</div>
			<div class="col-lg-2">
				<form:input path="amount1" readonly="true" class="form-control textfield text-right"/>
			</div>
			<div class="col-lg-3">
				<form:input path="description1" class="form-control textfield"/>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-1">
				<form:input path="quantity2" class="form-control textfield detail text-right"/>
				<label id="err_quantity2" class="text-danger"></label>
			</div>
			<div class="col-lg-1">
				<form:input path="weightGross2" class="form-control textfield detail text-right"/>
				<label id="err_weightGross2" class="text-danger"></label>
			</div>
			<div class="col-lg-1">
				<form:select path="weightIndicator2" class="chosen-select detail" cssStyle="width:100%;">
					 <option value="K" <c:if test="${'K' == awbFilter.weightIndicator2}">selected="selected"</c:if>>K</option>
					 <option value="P" <c:if test="${'P' == awbFilter.weightIndicator2}">selected="selected"</c:if>>P</option>
				</form:select>
			</div>
			<div class="col-lg-1">
				<form:input path="rateClass2" class="form-control textfield detail text-uppercase"/>
			</div>
			<div class="col-lg-1">
				<form:input path="weightCharge2" class="form-control textfield detail text-right"/>
				<label id="err_weightCharge2" class="text-danger"></label>
			</div>
			<div class="col-lg-2">
				<form:input path="unit2" class="form-control textfield detail text-right"/>
				<label id="err_unit2" class="text-danger"></label>
			</div>
			<div class="col-lg-2">
				<form:input path="amount2" readonly="true" class="form-control textfield detail text-right"/>
			</div>
			<div class="col-lg-3">
				<form:input path="description2" class="form-control textfield detail"/>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-1">
				<form:input path="quantity3" class="form-control textfield text-right"/>
				<label id="err_quantity3" class="text-danger"></label>
			</div>
			<div class="col-lg-1">
				<form:input path="weightGross3" class="form-control textfield text-right"/>
				<label id="err_weightGross3" class="text-danger"></label>
			</div>
			<div class="col-lg-1">
				<form:select path="weightIndicator3" class="chosen-select detail" cssStyle="width:100%;">
					 <option value="K" <c:if test="${'K' == awbFilter.weightIndicator3}">selected="selected"</c:if>>K</option>
					 <option value="P" <c:if test="${'P' == awbFilter.weightIndicator3}">selected="selected"</c:if>>P</option>
				</form:select>
			</div>
			<div class="col-lg-1">
				<form:input path="rateClass3" class="form-control textfield text-uppercase"/>
			</div>
			<div class="col-lg-1">
				<form:input path="weightCharge3" class="form-control textfield text-right"/>
				<label id="err_weightCharge3" class="text-danger"></label>
			</div>
			<div class="col-lg-2">
				<form:input path="unit3" class="form-control textfield text-right"/>
				<label id="err_unit3" class="text-danger"></label>
			</div>
			<div class="col-lg-2">
				<form:input path="amount3" readonly="true" class="form-control textfield text-right"/>
			</div>
			<div class="col-lg-3">
				<form:input path="description3" class="form-control textfield"/>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-1">
				<form:input path="quantity4" class="form-control textfield detail text-right"/>
				<label id="err_quantity4" class="text-danger"></label>
			</div>
			<div class="col-lg-1">
				<form:input path="weightGross4" class="form-control textfield detail text-right"/>
				<label id="err_weightGross4" class="text-danger"></label>
			</div>
			<div class="col-lg-1">
				<form:select path="weightIndicator4" class="chosen-select detail" cssStyle="width:100%;">
					 <option value="K" <c:if test="${'K' == awbFilter.weightIndicator4}">selected="selected"</c:if>>K</option>
					 <option value="P" <c:if test="${'P' == awbFilter.weightIndicator4}">selected="selected"</c:if>>P</option>
				</form:select>
			</div>
			<div class="col-lg-1">
				<form:input path="rateClass4" class="form-control textfield detail text-uppercase"/>
			</div>
			<div class="col-lg-1">
				<form:input path="weightCharge4" class="form-control textfield detail text-right"/>
				<label id="err_weightCharge4" class="text-danger"></label>
			</div>
			<div class="col-lg-2">
				<form:input path="unit4" class="form-control textfield detail text-right"/>
				<label id="err_unit4" class="text-danger"></label>
			</div>
			<div class="col-lg-2">
				<form:input path="amount4" readonly="true" class="form-control textfield detail text-right"/>
			</div>
			<div class="col-lg-3">
				<form:input path="description4" class="form-control textfield detail"/>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-1">
				<form:input path="quantity5" class="form-control textfield text-right"/>
				<label id="err_quantity5" class="text-danger"></label>
			</div>
			<div class="col-lg-1">
				<form:input path="weightGross5" class="form-control textfield text-right"/>
				<label id="err_weightGross5" class="text-danger"></label>
			</div>
			<div class="col-lg-1">
				<form:select path="weightIndicator5" class="chosen-select detail" cssStyle="width:100%;">
					 <option value="K" <c:if test="${'K' == awbFilter.weightIndicator5}">selected="selected"</c:if>>K</option>
					 <option value="P" <c:if test="${'P' == awbFilter.weightIndicator5}">selected="selected"</c:if>>P</option>
				</form:select>
			</div>
			<div class="col-lg-1">
				<form:input path="rateClass5" class="form-control textfield text-uppercase"/>
			</div>
			<div class="col-lg-1">
				<form:input path="weightCharge5" class="form-control textfield text-right"/>
				<label id="err_weightCharge5" class="text-danger"></label>
			</div>
			<div class="col-lg-2">
				<form:input path="unit5" class="form-control textfield text-right"/>
				<label id="err_unit5" class="text-danger"></label>
			</div>
			<div class="col-lg-2">
				<form:input path="amount5" readonly="true" class="form-control textfield text-right"/>
			</div>
			<div class="col-lg-3">
				<form:input path="description5" class="form-control textfield"/>
			</div>
		</div>
		
		<!----------------------------------------------------------- TOTAL -------------------------------------------------- -->
		<div class="hr-line-dashed"></div>
		<div class="row">
			<div class="col-lg-7">
				<div class="form-group">
					<div class="row">
						<div class="col-lg-4" style="margin-top:30px">
							<label><spring:message code="awb.total.weight.charge" text="!Prepaid"/></label>
						</div>
						<div class="col-lg-4">
							<label><spring:message code="awb.prepaid" text="!PPD"/></label>
							<form:input path="weightPp" readonly="true" class="form-control textfield text-right"/>
						</div>
						<div class="col-lg-4">
							<label><spring:message code="awb.collect" text="!CCD"/></label> 
							<form:input path="weightCc" readonly="true" class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4" style="margin-top:7px">
						<label><spring:message code="awb.total.charge.agent" text="!"/></label>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="agentPp" readonly="true" class="form-control textfield text-right"/>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="agentCc" readonly="true" class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4" style="margin-top:7px">
						<label><spring:message code="awb.total.charge.carrier" text="!"/></label>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="carrierPp" readonly="true" class="form-control textfield text-right"/>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="carrierCc" readonly="true"  class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4" style="margin-top:7px">
						<label><spring:message code="awb.total" text="!Port paye"/></label>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="totalPp" readonly="true" class="form-control textfield text-right"/>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="totalCc" readonly="true" class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4" style="margin-top:7px">
						<label><spring:message code="awb.net" text="!Port paye"/></label>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="net" readonly="true" class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="hr-line-dashed"></div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4" style="margin-top:7px">
						<label><spring:message code="awb.com.percent" text="!"/>(%)</label>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="commPercent" class="form-control textfield text-right"/>
							<label id="err_commPercent" class="text-danger"></label>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="comm" readonly="true" class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4" style="margin-top:7px">
						<label><spring:message code="awb.vat" text="!"/>(%)</label>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="vatPercent" class="form-control textfield text-right"/>
							<label id="err_vatPercent" class="text-danger"></label>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="vat" readonly="true" class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4" style="margin-top:7px">
						<label><spring:message code="awb.mode.payment" text="!"/></label>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:select path="modePayment" onchange="doShowPayment()" class="chosen-select" cssStyle="width:100%;">
								<option value="CA" <c:if test="${'CA' == awbFilter.modePayment}">selected="selected"</c:if>>
									<spring:message code="awb.mode.payment.CA" text="!"/>
								</option>
								<option value="CH" <c:if test="${'CH' == awbFilter.modePayment}">selected="selected"</c:if>>
									<spring:message code="awb.mode.payment.CH" text="!"/>
								</option>
								<option value="EC" <c:if test="${'EC' == awbFilter.modePayment}">selected="selected"</c:if>>
									<spring:message code="awb.mode.payment.EC" text="!"/>
								</option>
								<option value="CC" <c:if test="${'CC' == awbFilter.modePayment}">selected="selected"</c:if>>
									<spring:message code="awb.mode.payment.CC" text="!"/>
								</option>
							</form:select>
						</div>
					</div>
				</div>
				<div id="payment_inf" style="display: none;">
					<div class="row">
						<div class="col-lg-4">
							<label><spring:message code="awb.nb.cart" text="!Port paye"/></label>
							<form:input path="nbCart" class="form-control textfield"/>
							<label id="err_nbCart" class="text-danger"></label>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label><spring:message code="awb.date.exp" text="!Port paye"/></label>
								<div class="input-group date">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			                        <form:input path="dateExp" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
								</div>
								<label id="err_dateExp" class="text-danger"></label>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label><spring:message code="awb.name.cart" text="!Port paye"/></label>
								<form:input path="nameCart" class="form-control textfield"/>
								<label id="err_nameCart" class="text-danger"></label>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label><spring:message code="awb.note" text="!Handling information"/></label>
							<form:textarea path="descriptUser" rows="3" class="form-control textfield"/>
						</div>
					</div>
				</div>
		
			</div>
			<!-- <div class="col-lg-1"></div> -->
			<div class="col-lg-5">
				<div class="row">
					<div class="col-lg-12">
							<label><spring:message code="awb.frais" text="!Taxe"/></label>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="taxCode1" maxlength="3" class="form-control textfield text-uppercase"/>
							<label id="err_taxCode1" class="text-danger"></label>
						</div>
					</div>
					<div class="col-lg-8">
						<div class="form-group">
							<form:input path="taxValue1" class="form-control textfield text-right"/>
							<label id="err_taxValue1" class="text-danger"></label>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="taxCode2" maxlength="3" class="form-control textfield text-uppercase"/>
							<label id="err_taxCode2" class="text-danger"></label>
						</div>
					</div>
					<div class="col-lg-8">
						<div class="form-group">
							<form:input path="taxValue2" class="form-control textfield text-right"/>
							<label id="err_taxValue2" class="text-danger"></label>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="taxCode3" maxlength="3" class="form-control textfield text-uppercase"/>
							<label id="err_taxCode3" class="text-danger"></label>
						</div>
					</div>
					<div class="col-lg-8">
						<div class="form-group">
							<form:input path="taxValue3" class="form-control textfield text-right"/>
							<label id="err_taxValue3" class="text-danger"></label>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="taxCode4" maxlength="3" class="form-control textfield text-uppercase"/>
							<label id="err_taxCode4" class="text-danger"></label>
						</div>
					</div>
					<div class="col-lg-8">
						<div class="form-group">
							<form:input path="taxValue4" class="form-control textfield text-right"/>
							<label id="err_taxValue4" class="text-danger"></label>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="taxCode5" maxlength="3" class="form-control textfield text-uppercase"/>
							<label id="err_taxCode5" class="text-danger"></label>
						</div>
					</div>
					<div class="col-lg-8">
						<div class="form-group">
							<form:input path="taxValue5" class="form-control textfield text-right"/>
							<label id="err_taxValue5" class="text-danger"></label>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="taxCode6" maxlength="3" class="form-control textfield text-uppercase"/>
							<label id="err_taxCode6" class="text-danger"></label>
						</div>
					</div>
					<div class="col-lg-8">
						<div class="form-group">
							<form:input path="taxValue6" class="form-control textfield text-right"/>
							<label id="err_taxValue6" class="text-danger"></label>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="taxCode7" maxlength="3" class="form-control textfield text-uppercase"/>
							<label id="err_taxCode7" class="text-danger"></label>
						</div>
					</div>
					<div class="col-lg-8">
						<div class="form-group">
							<form:input path="taxValue7" class="form-control textfield text-right"/>
							<label id="err_taxValue7" class="text-danger"></label>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="taxCode8" maxlength="3" class="form-control textfield text-uppercase"/>
							<label id="err_taxCode8" class="text-danger"></label>
						</div>
					</div>
					<div class="col-lg-8">
						<div class="form-group">
							<form:input path="taxValue8" class="form-control textfield text-right"/>
							<label id="err_taxValue8" class="text-danger"></label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 text-right">
				<div class="form-group">
					<a onclick="doSubmit('NEW');" class="btn btn-w-m btn-success text-uppercase"><i class="fa fa-file-o"></i>&nbsp;<spring:message code="button.new" text="!"/></a>
				 	<a onclick="doValidator('GO');" class="btn btn-w-m btn-success text-uppercase"><i class="fa fa-check"></i>&nbsp;<spring:message code="button.valider" text="!"/></a>
				</div>
			</div>
		</div>
	</form:form>
	
	<!----------------------------------------------------------- MODAL CURRENCY -------------------------------------------------- -->
	
	<div class="modal inmodal" id="formCreate" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" style="width:800px">
			<div class="modal-content animated bounceInRight">
				<div class="modal-body">
				<form:form id="form-add" name="form-add" method="post" commandName="bsrFilterCurr" action="createAwb/curr">
					<input type="hidden" name="action" value="CREATE">	
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label><spring:message code="bsr.convertfrom" text="!Convert from"/></label>
								<form:input id="currFromAdd" path="currFromAdd" cssClass="form-control textfield" maxlength="3" style='text-transform:uppercase' />
								<label id="err_currFromAdd" class="text-danger"></label>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label><spring:message code="bsr.convertto" text="!to"/></label>
								<form:input path="currToAdd" value="XAF" cssClass="form-control textfield" maxlength="3" style='text-transform:uppercase'/>
								<label id="err_currToAdd" class="text-danger"></label>
							</div>
							
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label><spring:message code="bsr.rate" text="!Roe"/></label>
								<form:input path="rateAdd" cssClass="form-control textfield text-right"/>
								<label id="err_rateAdd" class="text-danger"></label>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label><spring:message code="bsr.appticationdate" text="!Application date	"/></label>
								<div class="input-group date">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			                        <form:input id="dateStartAdd" path="dateStartAdd" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
								</div>
								<label id="err_dateStartAdd" class="text-danger"></label>
							</div>
						</div>
					</div>
					</form:form>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<button id ="add" type="button" onclick="doCreateForm();" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-plus-square "></i> <spring:message code="button.create" text="!Create"/>
					</button>
				</div>
			</div>
		</div>
	</div>
	
</div>
   