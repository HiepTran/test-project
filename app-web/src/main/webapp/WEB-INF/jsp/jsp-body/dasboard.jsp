<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="<c:url value="/static/inspinia/css/plugins/morris/morris-0.4.3.min.css"/>" rel="stylesheet">
   
<script src="<c:url value="/static/inspinia/js/plugins/morris/raphael-2.1.0.min.js"/>"></script>
<script src="<c:url value="/static/inspinia/js/plugins/morris/morris.js"/>"></script>
   
   
    
<script type="text/javascript">
	document.getElementById("dashboard").className = "active";

	function round(num,pre){
	 	return Math.round(num * Math.pow(10,pre)) / Math.pow(10,pre) ;
	}
	
	$(document).ready(function() {
		$('#page-wrapper').removeClass('white-bg');
		$('#page-wrapper').addClass('gray-bg');
		
		 Morris.Bar({
		        element: 'sale-chart',
		        data: [
					<c:forEach items="${listMonth}" step="1" varStatus="sttLm">
						<c:set value="0" var="nrtd"/>
						<c:set value="0" var="weightCharge"/>
						<c:set value="0" var="amount"/>
						
						<fmt:formatDate value="${listMonth[sttLm.index]}" pattern="MM/yyyy" var="month"/>
						<fmt:formatDate value="${listMonth[sttLm.index]}" pattern="MMM" var="month1"/>

						<c:forEach items="${listAwb}" var="elm" step="1" varStatus="stt1">
						  	<c:if test="${elm.month == month}">
						  		<c:set value="${elm.nrtd}" var="nrtd"/>
						  		<c:set value="${elm.weightCharge}" var="weightCharge"/>
						  		<c:set value="${elm.amount}" var="amount"/>
						  	</c:if>
						</c:forEach>
						{ y: '${month1}', a: ${nrtd}, b:round(${weightCharge},2), c: round(${amount/1000},2) },
					</c:forEach>
				        
				     ],
		        xkey: 'y',
		        ykeys: ['a', 'b', 'c'],
		        labels: ['<spring:message code="dashboard.lta" text="!"/>', '<spring:message code="dashboard.weight.charge" text="!"/>', '<spring:message code="dashboard.amount" text="!"/>'],
		        hideHover: 'auto',
		        resize: true,
		        barColors: ['#cacaca', '#1ab394', '#1c84c6'],
		    });

		 Morris.Bar({
		        element: 'flown-chart',
		        data: [
					<c:forEach items="${listMonth}" step="1" varStatus="sttLm">
						<c:set value="0" var="nrtd"/>
						<c:set value="0" var="weightCharge"/>
						<c:set value="0" var="amount"/>
						
						<fmt:formatDate value="${listMonth[sttLm.index]}" pattern="MM/yyyy" var="month"/>
						<fmt:formatDate value="${listMonth[sttLm.index]}" pattern="MMM" var="month1"/>

						<c:forEach items="${listFlown}" var="elm" step="1" varStatus="stt1">
						  	<c:if test="${elm.month == month}">
						  		<c:set value="${elm.nrtd}" var="nrtd"/>
						  		<c:set value="${elm.weightCharge}" var="weightCharge"/>
						  		<c:set value="${elm.amount}" var="amount"/>
						  	</c:if>
						</c:forEach>
						{ y: '${month1}', a: ${nrtd}, b:round(${weightCharge},2), c: round(${amount/1000},2) },
					</c:forEach>
				        
				     ],
		        xkey: 'y',
		        ykeys: ['a', 'b', 'c'],
		        labels: ['<spring:message code="dashboard.lta" text="!"/>', '<spring:message code="dashboard.weight.charge" text="!"/>', '<spring:message code="dashboard.amount" text="!"/>'],
		       	hideHover: 'auto',
		        resize: true,
		        barColors: ['#cacaca', '#1ab394', '#1c84c6'],
		    });
	});
</script>

<div id="body">
<form:form id="form" name="form" method="post" commandName="agentFilter">
	<c:set var="formatDouble" value="#,##0.00"/>
	<c:set var="formatInteger" value="#,##0"/>
		
	<div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right"><fmt:formatDate pattern="MMM yyyy" value="${currentDate}"/></span>
                    <h5><spring:message code="dashboard.sale" text="!"/></h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-3">
                            <p><spring:message code="dashboard.lta" text="!"/></p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatInteger}" value="${saleMonth.nrtd }"/></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-nowrap">
                            <p><spring:message code="dashboard.weight.gross" text="!"/></p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatDouble}" value="${saleMonth.weightGross}"/></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-nowrap">
                            <p><spring:message code="dashboard.weight.charge" text="!"/></p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatDouble}" value="${saleMonth.weightCharge}"/></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <p><spring:message code="dashboard.amount" text="!"/> (${cutp})</p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatDouble}" value="${saleMonth.amount}"/></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right"><fmt:formatDate pattern="yyyy" value="${currentDate}"/></span>
                    <h5><spring:message code="dashboard.sale" text="!"/></h5>
                </div>
                <div class="ibox-content">
                   <div class="row">
                        <div class="col-md-3">
                            <p><spring:message code="dashboard.lta" text="!"/></p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatInteger}" value="${saleYear.nrtd }"/></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-nowrap">
                            <p><spring:message code="dashboard.weight.gross" text="!"/></p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatDouble}" value="${saleYear.weightGross}"/></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-nowrap">
                            <p><spring:message code="dashboard.weight.charge" text="!"/></p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatDouble}" value="${saleYear.weightCharge}"/></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <p><spring:message code="dashboard.amount" text="!"/> (${cutp})</p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatDouble}" value="${saleYear.amount}"/></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right"><fmt:formatDate pattern="MMM yyyy" value="${currentDate}"/></span>
                    <h5><spring:message code="dashboard.flown" text="!"/></h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-3">
                            <p><spring:message code="dashboard.lta" text="!"/></p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatInteger}" value="${flownMonth.nrtd }"/></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-nowrap">
                            <p><spring:message code="dashboard.weight.gross" text="!"/></p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatDouble}" value="${flownMonth.weightGross}"/></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-nowrap">
                            <p><spring:message code="dashboard.weight.charge" text="!"/></p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatDouble}" value="${flownMonth.weightCharge}"/></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <p><spring:message code="dashboard.amount" text="!"/> (${cutp})</p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatDouble}" value="${flownMonth.amount}"/></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right"><fmt:formatDate pattern="yyyy" value="${currentDate}"/></span>
                    <h5><spring:message code="dashboard.flown" text="!"/></h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-3">
                            <p><spring:message code="dashboard.lta" text="!"/></p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatInteger}" value="${flownYear.nrtd }"/></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-nowrap">
                            <p><spring:message code="dashboard.weight.gross" text="!"/></p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatDouble}" value="${flownYear.weightGross}"/></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 text-nowrap">
                            <p><spring:message code="dashboard.weight.charge" text="!"/></p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatDouble}" value="${flownYear.weightCharge}"/></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <p><spring:message code="dashboard.amount" text="!"/> (${cutp})</p>
                        </div>
                        <div class="col-md-9 text-right">
                            <h4 class="no-margins"><fmt:formatNumber pattern="${formatDouble}" value="${flownYear.amount}"/></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                
	<div class="row">
		<div class="col-lg-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5><spring:message code="dashboard.sale" text="!"/></h5>
					<label class="label label-success">K ${cutp}</label>
				</div>
				<div class="ibox-content">
				 	<div id="sale-chart"></div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5><spring:message code="dashboard.flown" text="!"/></h5>
					<label class="label label-success">K ${cutp}</label>
				</div>
				<div class="ibox-content">
				 	<div id="flown-chart"></div>
				</div>
			</div>
		</div>
	</div>
</form:form>
</div>
   