<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>

<style type="text/css">
	.chosen-container-single .chosen-single {
         background: none;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
     .chosen-container {
        width: 100% !important;
    }
</style>

<script type="text/javascript">
	document.getElementById("param").className = "active";
	document.getElementById("param_agent").className = "active";

	$(document).ready(function() {
	    $('.chosen-select').chosen({disable_search_threshold: 10});
	});

 	function ConformDelete(id) {
        swal({
            title: "<spring:message code="message.delete" text="!"/>" + " [" + id + "] ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<spring:message code="button.delete" text="!"/>",
            cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
            closeOnConfirm: false
        }, function () {
       	 	document.forms[0].elements['action'].value='DELETE';
			 document.forms[0].elements['id'].value=id;
			 document.forms[0].submit();
        });
    };
    
    function doCreateAgent(){
    	$.ajax({
    		url: "<spring:url value='/secure/agent/create/error.json'/>",
    		type: "POST",
    		cache: false,
    		data: $('#form-add').serialize(),
    		success: function(result){
    			if(result.errCodeCreate > 0){
		           	 $('#err_agtnCode').html('');
		           	 $('#err_emission').html('');
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++)
	               	 {
	                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
	        	}else{
	        		$('#form-add').submit();
	        	}
    		},
    		error: function(XMLHttpRequest, textStatus, errorThrown){}
    	});
    }
    
	var pageNo = ${agentFilterPara.page};
	
	$(function(){
	    $.ajax({
	    	url : "<spring:url value='/secure/agent/loadpage'/>",
	        data: { pageNo: pageNo },
	        cache : false,
	        success: function (response) {
	            $("#_results").html(response);
	        }
	    });
	});
</script>

<div id="body">
<form:form id="form" name="form" method="post" commandName="agentFilterPara">
	<input name="action" type="hidden">
	<input name="id" type="hidden">
		
	<!-- modify -->
	<div id="edit-box" class="boxcss" style="display: block;">
		<div id="edit-result"></div>
    </div>
    
    <div class="row">
		<div class="col-lg-3">
			<div class="form-group">
				<label><spring:message code="agent.code" text="!"/></label>
				<form:input path="codeExt" maxlength="11" type="text" cssClass="form-control textfield"/>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				<label><spring:message code="agent.kind" text="!"/></label>
				<form:select path="kindExt" class="chosen-select" cssStyle="width:100%">
					<option value="" <c:if test="${'' == agentFilterPara.kind}">selected="selected"</c:if>><spring:message code="commom.all" text="!"/></option>
					<option value="TR" <c:if test="${'TR' == agentFilterPara.kindExt}">selected="selected"</c:if>>TR</option>
					<option value="CA" <c:if test="${'CA' == agentFilterPara.kindExt}">selected="selected"</c:if>>CA</option>
				</form:select>
			</div>
		</div>
		
		<div class="col-lg-3">
			<div class="form-group">
				<label><spring:message code="agent.country" text="!"/></label>
				<form:select path="isocExt" class="chosen-select" cssStyle="width:100%">
				<option value="" <c:if test="${'' == agentFilterPara.isocExt}">selected="selected"</c:if>><spring:message code="commom.all" text="!"/></option>
			 		<c:forEach items="${listIsoc}" var="elm">
						<option value="${elm}"
							<c:if test="${elm == agentFilterPara.isocExt}" >selected="selected"</c:if>>
							<spring:message code="isoc.${elm}" text="${elm}"/>
						</option>
					</c:forEach>
				</form:select>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				<label><spring:message code="agent.iata.ind" text="!"/></label>
				<form:select path="iataIndExt" class="chosen-select" cssStyle="width:100%">
					<option value="" <c:if test="${'' == agentFilterPara.iataIndExt}">selected="selected"</c:if>><spring:message code="commom.all" text="!"/></option>
					<option value="1" <c:if test="${'1' == agentFilterPara.iataIndExt}">selected="selected"</c:if>><spring:message code="agent.yes" text="!"/></option>
					<option value="0" <c:if test="${'0' == agentFilterPara.iataIndExt}">selected="selected"</c:if>><spring:message code="agent.no" text="!"/></option>
				</form:select>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-4">
			<button type="button" class="btn btn-w-m btn-success text-uppercase" data-toggle="modal" data-target="#formCreate">
				<i class="fa fa-plus-square"> <spring:message code="button.create" text="!"/></i>
			</button>
		</div>
		
		<div class="col-lg-8 text-right">
			<button onclick="javascript:doSubmit('RESET');" class="btn btn-w-m btn-default text-uppercase"><i class="fa fa-undo"></i> <spring:message code="button.reset" text="!"/></button>
			<button onclick="javascript:doSubmit('GO');" class="btn btn-w-m btn-success text-uppercase"><i class="fa fa-check"></i> <spring:message code="button.go" text="!"/></button>
			<a href="<c:url value="/secure/agent/excel"></c:url>" target="_blank" class="btn btn-w-m btn-success text-uppercase">
				<i class="fa fa-file-excel-o"></i> <spring:message code="button.excel" text="!"/>
			</a>	
		</div>
	</div>
			
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="col-lg-12">
			<div id="_results"></div>
		</div>
	</div>
</form:form>

<div class="modal inmodal" id="formCreate" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" style="margin-left: 200px;">
		<div class="modal-content animated bounceInRight" style="width:1050px">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
				<h5 class="modal-title"><spring:message code="agent.title" text="!"/></h5>
			</div>
			<div class="modal-body">
				<form:form id="form-add" name="form-add" method="post" commandName="agentFilterPara" action="agent/create">
					<input type="hidden" name="action" value="CREATE">	
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label><spring:message code="agent.code" text="!"/></label>
								<form:input path="agtnCode" maxlength="11" cssClass="form-control textfield"/>
								<label id="err_agtnCode" class="text-danger"></label>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label><spring:message code="agent.name" text="!"/></label>
								<form:input path="name" cssClass="form-control textfield"/>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label><spring:message code="agent.address" text="!"/></label>
								<form:input path="address1" class="form-control textfield"></form:input>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label><spring:message code="agent.kind" text="!"/></label>
							 	<form:select path="kind" cssClass="chosen-select" cssStyle="width:100%;">
									<option value="TR" <c:if test="${'TR' == agentFilterPara.kind}">selected="selected"</c:if>>TR</option>
									<option value="CA" <c:if test="${'CA' == agentFilterPara.kind}">selected="selected"</c:if>>CA</option>
								</form:select>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label><spring:message code="agent.city" text="!"/></label>
							 	<form:input path="city" class="form-control textfield"></form:input>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label><spring:message code="agent.zip" text="!"/></label>
							 	<form:input path="zip" class="form-control textfield"></form:input>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label><spring:message code="agent.country" text="!"/></label>
							 	<form:select path="isoc" cssClass="chosen-select" cssStyle="width:100%;">
							 		<c:forEach items="${lstCritIsoc}" var="elm">
										<option value="${elm.isoc}" <c:if test="${elm.isoc == agentFilterPara.isoc }">selected="selected"</c:if>>
											<spring:message code="isoc.${elm.isoc}" text=""/>
										</option>
									</c:forEach>
								</form:select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label><spring:message code="agent.region" text="!"/></label>
							 	<form:input path="region" class="form-control textfield"></form:input>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label><spring:message code="agent.phone" text="!"/></label>
							 	<form:input path="phone" class="form-control textfield"></form:input>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label><spring:message code="agent.fax" text="!"/></label>
							 	<form:input path="fax" class="form-control textfield"></form:input>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label><spring:message code="agent.email" text="!"/></label>
							 	<form:input path="email" class="form-control textfield"></form:input>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label><spring:message code="agent.iata.ind" text="!"/></label>
							 	<form:select path="iataInd" cssClass="chosen-select" cssStyle="width:100%;">
									<option value="1" <c:if test="${'1' == agentFilterPara.iataInd}">selected="selected"</c:if>><spring:message code="agent.yes" text="!"/></option>
									<option value="0" <c:if test="${'0' == agentFilterPara.iataInd}">selected="selected"</c:if>><spring:message code="agent.no" text="!"/></option>
								</form:select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label><spring:message code="agent.comm" text="!"/></label>
							 	<form:input path="comm" class="form-control textfield"></form:input>
							 	<label id="err_comm" class="text-danger"></label>
							</div>
						</div>
					</div>
				</form:form>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
					<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
				</button>
				<a onclick="doCreateAgent();" class="btn btn-w-m btn-success text-uppercase" type="submit">
	  				<i class="fa fa-check"></i> <spring:message code="button.go" text="!Go"/>
	  			</a>
			</div>
		</div>
	</div>
</div>
</div>
