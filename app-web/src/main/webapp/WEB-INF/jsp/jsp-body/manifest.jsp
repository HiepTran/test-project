<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>

<style type="text/css">
	.datepicker { 
		z-index: 9999 !important;
	}
</style>

<script type="text/javascript">
	document.getElementById("manifest").className = "active";
	document.getElementById("createManifest").className = "active";

	var pageNo = ${manifestFilter.page};
	$(function () {
	    $.ajax({
	    	url : "<spring:url value='/secure/manifest/loadPage'/>",
	        data: { pageNo: pageNo },
	        cache : false,
	        success: function (response) {
	            $("#_results").html(response);
	        }
	    });
	    
	    $('.input-group.date').datepicker({
			todayBtn: "linked",
	        keyboardNavigation: false,
	        forceParse: false,
	        calendarWeeks: true,
	        autoclose: true,
			format: "dd/mm/yyyy"
	    });
	 });  

	function doValidator(){
		$.ajax({
			url: "<spring:url value='/secure/manifest/error.json'/>",
	        data: $('#form-add').serialize(), 
	        type: "POST",
	        success: function(result){
	        	if(result.errCode > 0){
		           	$('#err_tacn').html('');
		           	$('#err_manifestNo').html('');
		           	$('#err_orig').html('');
           			$('#err_dest').html('');
   					$('#err_flightNumber').html('');
		           	$('#err_flightDate').html('');
		           	$('#err_entryNumber	').html('');
		           	
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++)
	               	 {
	                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
	        	}else{
	        		$('#form-add').submit();
	        	}
	        },
		 	error: function(XMLHttpRequest, textStatus, errorThrown){}
		});
	}
	
	function ConformDelete(id) {
        swal({
            title: "<spring:message code="message.delete" text="!"/>" + " [" + id + "] ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<spring:message code="button.delete" text="!"/>",
            cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
            closeOnConfirm: false
        }, function () {
       	 	document.forms[0].elements['action'].value='DELETE';
			 document.forms[0].elements['id'].value=id;
			 document.forms[0].submit();
        });
    };
</script>

<div id="body">
	<form:form id="form" name="form" method="post" commandName="manifestFilter">
		<input type="hidden" name="action">
		<input type="hidden" name="id">
		
		<div class="row">
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="manifest.no" text="!"/></label>
					<form:input path="manifestNo" type="text" cssClass="form-control textfield text-uppercase"/>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="manifest.orig" text="!"/></label>
					<form:input path="orig" type="text" cssClass="form-control textfield text-uppercase"/>
				</div>
			</div>
			
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="manifest.dest" text="!"/></label>
					<form:input path="dest" type="text" cssClass="form-control textfield text-uppercase"/>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="manifest.flight.number" text="!"/></label>
					<form:input path="flightNumber" type="text" cssClass="form-control textfield text-uppercase"/>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="manifest.flight.from" text="!"/></label>
					<div class="input-group date">
	                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                      <form:input path="dateFrom" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="manifest.flight.to" text="!"/></label>
					<div class="input-group date">
	                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                      <form:input path="dateTo" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
				</div>
			</div>
		</div> 
		
		<div class="row">
			<div class="col-lg-4">
				<button type="button" class="btn btn-w-m btn-success text-uppercase" data-toggle="modal" data-target="#fromManifest">
					<i class="fa fa-plus-square"></i> <spring:message code="button.create" text="!"/>
				</button>
			</div>
			<div class="col-lg-8 text-right">
				<button onclick="javascript:doSubmit('RESET');" class="btn btn-w-m btn-default text-uppercase">
					<i class="fa fa-undo"></i> <spring:message code="button.reset" text="!"/>
				</button>
				<button onclick="javascript:doSubmit('GO');" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-check"></i> <spring:message code="button.go" text="!"/>
				</button>
			</div>
		</div>
		
		&nbsp; 
		<div class="row">
			<div class="col-lg-12">
				<div id="_results"></div> 
			</div>
		</div>
	</form:form>
	
	<!-- Modal create Manifest -->
	<div class="modal inmodal" id="fromManifest" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" >
			<div class="modal-content animated bounceInRight" style="width: 650px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
					<h5 class="modal-title"><spring:message code="manifest.title" text="!"/></h5>
				</div>
				<div class="modal-body">
				<form:form class="form-horizontal" id="form-add" name="form-add" method="post" commandName="manifestFilter">
					<input type="hidden" name="action" value="ADD">
					
						<div class="form-group">
							<div class="row">
							 	<div class="col-lg-6">
								 	<label><spring:message code="manifest.owner" text="!"/></label>
								 	<form:input path="tacn" class="form-control textfield"/>
								 	<label id="err_tacn" class="text-danger"></label>
								 </div>
								<div class="col-lg-6">
								 	<label><spring:message code="manifest.no" text="!"/></label>
								 	<form:input path="manifestNo" class="form-control textfield text-uppercase"/>
								 	<label id="err_manifestNo" class="text-danger"></label>
								 </div>
							 </div>
						 </div>
						 
						 <div class="form-group">
							<div class="row">
							 	<div class="col-lg-6">
								 	<label><spring:message code="manifest.orig" text="!"/></label>
								 	<form:input path="orig" maxlength="3" class="form-control textfield text-uppercase"/>
								 	<label id="err_orig" class="text-danger"></label>
								 </div>
								<div class="col-lg-6">
								 	<label><spring:message code="manifest.dest" text="!"/></label>
								 	<form:input path="dest" maxlength="3" class="form-control textfield text-uppercase"/>
								 	<label id="err_dest" class="text-danger"></label>
								 </div>
							 </div>
						 </div>
						 
						 <div class="form-group">
							<div class="row">
						 		<div class="col-lg-2">
								 	<label><spring:message code="manifest.flight.number" text="!"/></label>
								 	<form:input path="carrier" maxlength="2" class="form-control textfield text-uppercase"/>
							 	</div>
							 	<div class="col-lg-4">
								 	<label>&nbsp;</label>
								 	<form:input path="flightNumber" class="form-control textfield text-uppercase"/>
							 	</div>
								<div class="col-lg-6">
								 	<label><spring:message code="manifest.flight.date" text="!"/></label>
								 	<div class="input-group date">
				                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				                        <form:input path="flightDate" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
									</div>
								 </div>
							 </div>
							 <div class="row">
							 	<div class="col-lg-6">
								 	<label id="err_flightNumber" class="text-danger"></label>
							 	</div>
								<div class="col-lg-6">
									<label id="err_flightDate" class="text-danger"></label>
								 </div>
							 </div>
						 </div>
						 
						 <div class="form-group">
							<div class="row">
								<div class="col-lg-6">
								 	<label><spring:message code="manifest.immat" text="!"/></label>
								 	<form:input path="immat" class="form-control textfield text-uppercase"/>
								 </div>
							 	<div class="col-lg-6">
								 	<label><spring:message code="manifest.entry.number" text="!"/></label>
								 	<form:input path="entryNumber" class="form-control textfield"/>
								 	<label id="err_entryNumber" class="text-danger"></label>
								 </div>
							 </div>
						 </div>
					</form:form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!"/>
					</button>
					<a onclick="doValidator();" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-plus-square "></i> <spring:message code="button.create" text="!"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>