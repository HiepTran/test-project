<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>

<script type="text/javascript">
	document.getElementById("param").className = "active";
	document.getElementById("param_line").className = "active";

	$(document).ready(function() {

		$('.table-result').dataTable({
 	 		"dom": 'T<"clear">lfrtip',
 	        "sDom": "<'row'<'col-sm-6 text-left'><'col-sm-6 text-right'>r>" + "t" + "<'row'<'col-sm-6'><'col-sm-6'p>>",
 			"bSort": false,
 			"paginate": false
 	   	}); 	
 	   	
	  //show error save template
	    <c:if test="${errorEdit == true}">
		    setTimeout(function() {
		        toastr.options = {
		            closeButton: true,
		            progressBar: true,
		            showMethod: 'slideDown',
		            timeOut: 20000
		        };
		        toastr.error('<spring:message code="line.edit.error" text="!"/>');	
		    }, 1300);
        </c:if>
        
	});
	
	function doAddButton(){
		$.ajax({
			url: "<spring:url value='/secure/line/create/error.json'/>",
			type: "POST",
			cache: false,
			data: $('#form-add').serialize(),
			success: function(result){
	        	if(result.errCodeCreate > 0){
		           	 $('#err_origExt').html('');
		           	 $('#err_destExt').html('');
		           	 $('#err_lineExt').html('');
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++)
	               	 {
	                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
		        	}else{
		        		$('#form-add').submit();
		        	}
		        },
			 	error: function(XMLHttpRequest, textStatus, errorThrown){}
		});
	}

	function ConformDelete(id) {
        swal({
            title: "<spring:message code="message.delete" text="!"/>" + " [" + id + "] ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<spring:message code="button.delete" text="!"/>",
            cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
            closeOnConfirm: false
        }, function () {
       	 	document.forms[0].elements['action'].value='DELETE';
			document.forms[0].elements['id'].value=id;
			document.forms[0].submit();
        });
    };
    
    
 	function showForm(divId, id) {
 	    $.ajax({
 	        url: "<spring:url value='/secure/line/model/'/>" + id,
 	        cache: false,
 	        success: function (reponse) {
 	            $("div#edit-result").html(reponse);
 	        }
 	    });
 	    show(divId);
 	}
</script>

<div id="body">
	<form:form id="form" name="form" method="post" commandName="lineFilter">
		<input name="action" type="hidden">
		<input name="id" type="hidden">

		<!-- modify -->
		<div id="edit-box" class="boxcss" style="display: block;">
			<div id="edit-result"></div>
	    </div>
    
		<div class="row">
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="line.orig" text="!"/></label>
					<form:input path="orig" maxlength="3" type="text" cssClass="form-control textfield text-uppercase"/>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="line.dest" text="!"/></label>
					<form:input path="dest" maxlength="3" type="text" cssClass="form-control textfield text-uppercase"/>
				</div>
			</div>
			
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="line.line" text="!"/></label>
					<form:input path="line" type="text" cssClass="form-control textfield text-uppercase"/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<button type="button" class="btn btn-w-m btn-success text-uppercase" data-toggle="modal" data-target="#formLine">
					<i class="fa fa-plus-square"></i> <spring:message code="button.create" text="!"/>
				</button>
			</div>
			<div class="col-lg-8 text-right">
				<button onclick="javascript:doSubmit('RESET');" class="btn btn-w-m btn-default text-uppercase">
					<i class="fa fa-undo"></i> <spring:message code="button.reset" text="!"/>
				</button>
				<button onclick="javascript:doSubmit('GO');" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-check"></i> <spring:message code="button.go" text="!"/>
				</button>
				<a href="<c:url value="/secure/line/excel"></c:url>" target="_blank" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-file-excel-o"></i> <spring:message code="button.excel" text="!"/>
				</a>
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">
		    <div class="col-lg-12">
		        <label class="label label-success" style="font-size:14px">${fn:length(listLine)} <spring:message code="message.result" text="!Result(s)"/></label>
		    </div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">
		    <div class="col-lg-12">
				<table class="table table-striped table-bordered table-hover table-result" style="width:100%;">
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th class="text-center"><spring:message code="commom.action" text="!"/></th>
							<th><spring:message code="line.orig" text="!"/></th>
							<th><spring:message code="line.dest" text="!"/></th>
							<th><spring:message code="line.line" text="!"/></th>
							<th><spring:message code="line.description" text="!"/></th>						
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${listLine}" var="elm" varStatus="stt">
							<tr id="${stt.index + 1}">
								<td class="text-center">${stt.index + 1}</td>
								<td class="text-center text-nowrap">
									<a id='a-edit-${stt.index + 1}' onclick='bfShow("edit-box","a-edit-${stt.index + 1}", "${stt.index + 1}"); showForm("edit-box", "${fn:trim(elm.descripbe)}")' 
										title="<spring:message code="message.modify" text="!Edit"/>"><i class="fa fa-2x fa-edit"></i>
									</a>
									<a onclick="ConformDelete('${fn:trim(elm.descripbe)}')" title="<spring:message code="button.delete" text="!Delete"/>"><i class="fa fa-2x fa-trash-o"></i></a>
								</td>
								<td>${elm.orac}</td>
								<td>${elm.dstc}</td>
								<td>${elm.line}</td>
								<td>${elm.descripbe}</td>
							</tr>
						</c:forEach>	
					</tbody>
				</table>
			</div>
		</div>
	
	</form:form>
	<div class="modal inmodal" id="formLine" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" >
			<div class="modal-content animated bounceInRight" style="width: 750px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
					<h5 class="modal-title"><spring:message code="line.title" text="!"/></h5>
				</div>
				<div class="modal-body">
				<form:form class="form-horizontal" id="form-add" name="form-add" method="post" commandName="lineFilter" action="line/create">
					<input type="hidden" name="action" value="ADD">
					
						<div class="form-group">
							<div class="row">
							 	<div class="col-lg-4">
								 	<label><spring:message code="line.orig" text="!"/></label>
								 	<form:input path="origExt" maxlength="3" class="form-control textfield text-uppercase"/>
								 	<label id="err_origExt" class="text-danger"></label>
								 </div>
								<div class="col-lg-4">
								 	<label><spring:message code="line.dest" text="!"/></label>
								 	<form:input path="destExt" maxlength="3" class="form-control textfield text-uppercase"/>
								 	<label id="err_destExt" class="text-danger"></label>
								 </div>
								 <div class="col-lg-4">
								 	<label><spring:message code="line.line" text="!"/></label>
								 	<form:input path="lineExt" class="form-control textfield text-uppercase"/>
								 	<label id="err_lineExt" class="text-danger"></label>
								 </div>
							 </div>
						 </div>
					</form:form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<button type="button" onclick="doAddButton();" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-plus-square "></i> <spring:message code="button.create" text="!Create"/>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>