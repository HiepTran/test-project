<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>

<style type="text/css">
	.chosen-container-single .chosen-single {
         background: none;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
</style>

<script type="text/javascript">
	document.getElementById("param").className = "active";
	document.getElementById("param_legkm").className = "active";
	
	var pageNo = ${legkmfilter.page};
	
	$(function(){
	    $.ajax({
	    	url : "<spring:url value='/secure/legkm/loadpage'/>",
	        data: { pageNo: pageNo },
	        cache : false,
	        success: function (response) {
	            $("#_results").html(response);
	        }
	    });
	});
	
	function doCreateForm(action){
		$.ajax({
			url: "<spring:url value='/secure/legkm/create/error.json'/>",
			type: "POST",
			cache: false,
			data: $('#form-add').serialize(),
			success: function(result){
	        	if(result.errCodeCreate > 0){
		           	 $('#err_isocFrom').html('');
		           	 $('#err_isocTo').html('');
		           	 $('#err_airportFrom').html('');
		           	 $('#err_airportTo').html('');
		           	 $('#err_km').html('');
		             $('#err_mile').html('');
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++)
	               	 {
	                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
		        	}else{
		        		document.forms.namedItem('form-add').elements['action'].value=action;
		        		$('#form-add').submit();
		        	}
		        },
			 	error: function(XMLHttpRequest, textStatus, errorThrown){}
		});
	}
	
	function ConformDelete(isocform, airportfrom, isocto, airportto) {
	    swal({
	        title: "<spring:message code="message.delete" text="!"/>" + " [" + airportfrom + "-" + airportto + "] ?",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#DD6B55",
	        confirmButtonText: "<spring:message code="button.delete" text="!"/>",
	        cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
	        closeOnConfirm: false
	    }, function () {
	   	 	 document.forms[0].elements['action'].value='DELETE';
			 document.forms[0].elements['isocform'].value=isocform;
			 document.forms[0].elements['airportfrom'].value=airportfrom;
			 document.forms[0].elements['isocto'].value=isocto;
			 document.forms[0].elements['airportto'].value=airportto;
			 $.ajax({
				 url: "<spring:url value='/secure/legkm/delete'/>",
				 type: "POST",
				 cache: false,
				 data: $('#form').serialize(),
				 success: function(){
					 document.forms[0].submit();
				 }
			 });
	    });
	};
	
	$(document).ready(function(){
		if(${error == true}){
			$('#formCreate').modal("show");
		}
	});
</script>

<div id="body">
	<form:form id="form" name="form" method="post" modelAttribute="legkmfilter">
	<input name="action" type="hidden">
	<input name="isocform" type="hidden">
	<input name="airportfrom" type="hidden">
	<input name="isocto" type="hidden">
	<input name="airportto" type="hidden">
	<div id="edit-box" class="boxcss" style="display: block;">
		<div id="edit-result"></div>
	</div>
		<div class="row">
			<div class="col-sm-3">
				<div class="form-group">
					<label><spring:message code="legkm.isocfrom" text="!Isoc from"></spring:message></label>
					<form:input path="isocFrom" style='text-transform:uppercase' maxlength="2" cssClass="form-control"/>
				</div>
			</div>
			
			<div class="col-sm-3">
				<div class="form-group">
					<label><spring:message code="legkm.isocto" text="!Isoc to"></spring:message></label>
					<form:input path="isocTo" style='text-transform:uppercase' maxlength="2" cssClass="form-control"/>
				</div>
			</div>
			
			<div class="col-sm-3">
				<div class="form-group">
					<label><spring:message code="legkm.airportfrom" text="!Airport form"></spring:message></label>
					<form:input path="airportFrom" style='text-transform:uppercase' maxlength="3" cssClass="form-control"/>
				</div>
			</div>
			
			<div class="col-sm-3">
				<div class="form-group">
					<label><spring:message code="legkm.airportto" text="!Airport to"></spring:message></label>
					<form:input path="airportTo" style='text-transform:uppercase' maxlength="3" cssClass="form-control"/>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class = "col-sm-4">
				<button type="button" class="btn btn-w-m btn-success text-uppercase" data-toggle="modal" data-target="#formCreate">
				<i class="fa fa-plus-square"> <spring:message code="button.create" text="!Create"></spring:message></i>
				</button>
			</div>
			<div class="col-sm-8 text-right">
				<button onclick="javascript:doSubmit('RESET');" class="btn btn-w-m btn-default text-uppercase"><i class="fa fa-undo"></i>
					<spring:message code="button.reset" text="!"></spring:message>
				</button>
				<button onclick="javascript:doSubmit('GO');" type="button" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-search"></i> <spring:message code="button.go" text="!"/>
				</button>	
				<a href="<c:url value="/secure/legkm/excel"></c:url>" target="_blank" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-file-excel-o"></i> <spring:message code="button.excel" text="!"/>
				</a>
			</div>
		</div>
		
		<div class="col-sm-12">&nbsp;</div>
		
		<div class="row">
			<div class="col-lg-12">
				<div id="_results"></div>
			</div>
		</div>
	</form:form>
	
	<div class="modal inmodal" id="formCreate" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" style="width:900px">
			<div class="modal-content animated bounceInRight">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
					<h4 class="modal-title"><spring:message code="legkm.modal" text="!Create LegKm"></spring:message></h4>
				</div>
				<div class="modal-body">
					<form:form id="form-add" name="form-add" method="post" commandName="legkmfilter" action="legkm/create">
						<input type="hidden" name="action">	
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<label><spring:message code="legkm.isocfrom" text="!Isoc from"></spring:message></label>
									<form:input path="legKmAdd.id.isocFrom" maxlength="2" style='text-transform:uppercase' cssClass="form-control textfield text-left"/>
									<label id="err_isocFrom" class="text-danger"></label>
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label><spring:message code="legkm.isocto" text="!Isoc to"></spring:message></label>
									<form:input path="legKmAdd.id.isocTo" maxlength="2" style='text-transform:uppercase' cssClass="form-control textfield text-left"/>
									<label id="err_isocTo" class="text-danger"></label>
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="form-group">
									<label><spring:message code="legkm.airportfrom" text="!Airport from"></spring:message></label>
									<form:input path="legKmAdd.id.airportFrom" maxlength="3" style='text-transform:uppercase' cssClass="form-control textfield text-left"/>
									<label id="err_airportFrom" class="text-danger"></label>
								</div>
							</div>
							
						</div>
						
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<label><spring:message code="legkm.airportto" text="!Airport to"></spring:message></label>
									<form:input path="legKmAdd.id.airportTo" maxlength="3" style='text-transform:uppercase' cssClass="form-control textfield text-left"/>
									<label id="err_airportTo" class="text-danger"></label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label><spring:message code="legkm.km" text="!Km"></spring:message></label>
									<form:input path="legKmAdd.km" cssClass="form-control textfield text-right"/>
									<label id="err_km" class="text-danger"></label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label><spring:message code="legkm.mile" text="!Mile"></spring:message></label>
									<form:input path="legKmAdd.mile" cssClass="form-control textfield text-right"/>
									<label id="err_mile" class="text-danger"></label>
								</div>
							</div>
						</div>
					</form:form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<button id ="add" type="button" onclick="doCreateForm('CREATE');" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-plus-square"></i> <spring:message code="button.create" text="!Create"/>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>