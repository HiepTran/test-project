<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<style>
	.chosen-container-single .chosen-single {
         background: none;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
</style>

<script type="text/javascript">

function doValidatorEdit(action, isocfrom, airportfrom, isocto, airportto){
	$.ajax({
		url: "<spring:url value='/secure/legkm/error.json'/>" ,    
        data: $('#form_modify').serialize(), 
        type: "POST",
        success: function(result){
        	if(result.errCodeEdit > 0){
	           	 $('#err_km').html('');
	           	 $('#err_mile').html('');
	           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
	           	 for(var i=0;i<len;i++)
               	 {
                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
              	 }
        	}else{
        		document.forms[1].elements['action'].value=action;
        		document.forms[1].elements['isocfrom'].value=isocfrom;
        		document.forms[1].elements['airportfrom'].value=airportfrom;
        		document.forms[1].elements['isocto'].value=isocto;
        		document.forms[1].elements['airportto'].value=airportto;
        		document.forms[1].submit();
        	}
        },
	 	error: function(XMLHttpRequest, textStatus, errorThrown){}
	});
}

</script>

<form:form name="form_modify" id="form_modify" method="post" modelAttribute="legkmfilter" action="legkm/modify">
<input name="action" type="hidden">
<input name="isocfrom" type="hidden">
<input name="airportfrom" type="hidden">
<input name="isocto" type="hidden">
<input name="airportto" type="hidden">
<div class="panel panel-success" style="margin-top:9px;">
	<div class="panel-heading" style="padding: 4px;">
             <div class="row">
                 <div class="col-lg-11">
                     <h3><spring:message code="message.modify" text="!Modify"/> ${legkm.id.isocFrom } - ${legkm.id.isocTo}</h3>
                 </div>
                 <div class="col-lg-1 text-right">
                     <i class="fa fa-2x fa-times-circle" onclick="javascript:hide('edit-box');" style="cursor:pointer;"></i>
                 </div>
             </div>
    </div>
    <div class="panel-body" style="padding:5px;">
    	<div class="form-group">
			<div class="row">
				<div class="col-lg-4">
					<label><spring:message code="legkm.isocfrom" text="!Isoc from"/></label>
					<form:input path="legKmEdit.id.isocFrom" readonly="true" style='text-transform:uppercase' maxlength="2" value="${fn:trim(legkm.id.isocFrom)}" class="form-control textfield text-left"></form:input>
				</div>
				
				<div class="col-lg-4">
					<label><spring:message code="legkm.isocto" text="!Isoc to"/></label>
					<form:input path="legKmEdit.id.isocTo" readonly="true" style='text-transform:uppercase' maxlength="2" value="${fn:trim(legkm.id.isocTo)}" class="form-control textfield text-left"></form:input>
				</div>
				
				<div class="col-lg-4">
					<label><spring:message code="legkm.airportfrom" text="!Airport from"/></label>
					<form:input path="legKmEdit.id.airportFrom" readonly="true" style='text-transform:uppercase' maxlength="3" value="${fn:trim(legkm.id.airportFrom)}" class="form-control textfield text-left"></form:input>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-4">
					<label><spring:message code="legkm.airportto" text="!Airport to"/></label>
					<form:input path="legKmEdit.id.airportTo" readonly="true" style='text-transform:uppercase' maxlength="3" value="${fn:trim(legkm.id.airportTo)}" class="form-control textfield text-left"></form:input>
				</div>
				<div class="col-lg-4">
					<label><spring:message code="legkm.km" text="!Km"/></label>
					<form:input path="legKmEdit.km" value="${fn:trim(legkm.km)}" class="form-control textfield text-right"></form:input>
					<label id="err_km" class="text-danger"></label>
				</div>
				<div class="col-lg-4">
					<label><spring:message code="legkm.mile" text="!Mile"/></label>
					<form:input path="legKmEdit.mile" value="${fn:trim(legkm.mile)}" class="form-control textfield text-right"></form:input>
					<label id="err_mile" class="text-danger"></label>
				</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-lg-12 text-right">
	  			<a onclick="doValidatorEdit('MODIFY','${legkm.id.isocFrom}', '${legkm.id.airportFrom }', '${legkm.id.isocTo }', '${legkm.id.airportTo }');" class="btn btn-w-m btn-success text-uppercase" type="submit">
	  				<i class="fa fa-check"></i> <spring:message code="button.go" text="!Go"/>
	  			</a>
			</div>
		</div>
    </div>
</div>
</form:form>