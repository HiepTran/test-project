<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style type="text/css">
	.chosen-container-single .chosen-single {
         background: #fff;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
     
     .hr-line-dashed {
	    background-color: #ffffff;
	    border-top: 2px dashed #808080;
	    color: #ffffff;
	    height: 1px;
	    margin: 1px 5px 10px 10px;
	}
	
	.chosen-container {
        width: 100% !important;
    }
    
    .breadcrumb{
    	background: #f5f5f5;
    }
    .breadcrumb>li+li:before{
    	color: #676a6c;
    	content: ","
    }
</style>

<script type="text/javascript">
	$(function () {
	    $(".chosen-select").chosen({
	    	  "disable_search": true
		});
	});

	function doEditForm(action, id){
		$.ajax({
			url: "<spring:url value='/secure/user/edit/error.json'/>",
			type: "POST",
			cache: false,
			data: $('#form_modify').serialize(),
			success: function(result){
				if(result.errCodeEdit > 0){
		           	 $('#err_usernameEdit').html('');
		           	 $('#err_passwordEdit').html('');
		           	 $('#err_retypePasswordEdit').html('');
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++)
	              	 {
	                   $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	             	 }
	       		} else {
	       			document.forms.namedItem('form_modify').elements['action'].value=action;
	       			document.forms.namedItem('form_modify').elements['id'].value = id;
	       			$('#form_modify').submit();	
	       		}
			}
		});
	}

	function doClickSelectRole(){
		var role = document.getElementById('roleChange').value;
		
		if(role != "ROLE_ADMIN" && role != "ROLE_DW" && role != ""){
			$('#roleCreate').prop('checked', false);
			$('#roleUpdate').prop('checked', false);	
			$('#roleDelete').prop('checked', false);	
			<c:forEach items="${lstAllRoleUser}" var="elm">
				//var r = "${fn:substring(elm.authority, 0, elm.authority.length() - 2)}";
				var r = "${elm.authority}";
				if(r == role + '_C'){
					$('#roleCreate').prop('checked', true);
				} else if(r == role + '_U'){
					$('#roleUpdate').prop('checked', true);
				} else if(r == role + '_D'){
					$('#roleDelete').prop('checked', true);
				}
				
			</c:forEach>
			$('#hiddenRole').show();
		}
		else{
			$('#hiddenRole').hide();
		}
	}
	
</script>

<form:form name="form_modify" id="form_modify" method="post" modelAttribute="userFilter" action="user/edit">
	<input name="action" type="hidden">
	<input name="id" type="hidden">
	<input name="idUser" type="hidden">
   		
	<div class="form-group">
		<div class="row">
			<div class="col-sm-4">
				<label><spring:message code="user.usename" text="!User name" /></label>
				<form:input path="usernameEdit" type="text" value="${fn:trim(user.username)}" cssClass="form-control textfield" readonly="true" />
				<label id="err_usernameEdit" class="text-danger"></label>
			</div>
			<div class="col-sm-4">
				<label><spring:message code="user.password" text="!Password" /></label>
				<form:password path="passwordEdit" cssClass="form-control textfield disablecopypaste" />
				<label id="err_passwordEdit" class="text-danger"></label>
			</div>
			<div class="col-sm-4">
				<label><spring:message code="user.retypepassword" text="!Confirm password" /></label>
				<form:password path="retypePasswordEdit" cssClass="form-control textfield disablecopypaste" />
				<label id="err_retypePasswordEdit" class="text-danger"></label>
			</div>
		</div>
	</div>
		
	<div class="form-group">
		<div class="row">
			<div class="col-sm-4">
				<label><spring:message code="user.name" text="!Name" /></label>
				<form:input path="nameEdit" value="${fn:trim(user.name)}" cssClass="form-control textfield " />
				<label id="err_nameEdit" class="text-danger"></label>
			</div>
			<div class="col-sm-4">
				<label><spring:message code="user.email" text="!Email" /></label>
				<form:input path="emailEdit" value="${fn:trim(user.email)}" type="text" cssClass="form-control textfield " />
				<label id="err_emailEdit" class="text-danger"></label>
			</div>
			<div class="col-sm-4">
				<label><spring:message code="user.tel" text="!Telephone" /></label>
				<form:input path="telephoneEdit" value="${fn:trim(user.telephone)}" type="text" cssClass="form-control textfield " />
				<label id="err_telephoneEdit" class="text-danger"></label>
			</div>
		</div>
	</div>
		
	<div class="form-group">
		<div class="row">
			<div class="col-sm-4">
				<label><spring:message code="user.airlinecode" text="!Airline code" /></label>
				<form:input path="airlineCodeEdit" value="${fn:trim(user.airlineCode) }" type="text" cssClass="form-control textfield " readonly="true"/>
			</div>
			<div class="col-sm-4">
				<label><spring:message code="user.countrycode" text="!Country code" /></label>
				<form:select path="countryCodeEdit" cssClass="chosen-select" cssStyle="width:100%;">
					<c:forEach items="${lstCritIsoc }" var="elm">
						<option value="${elm.isoc }" <c:if test="${elm.isoc == user.countryCode}"> selected="selected"</c:if>>${elm.isoc }</option>
					</c:forEach>
				</form:select>
			</div>
			<div class="col-sm-4">
				<label><spring:message code="user.language" text="!Language" /></label>
				<form:select path="languageEdit" cssClass="chosen-select" cssStyle="width:100%;">
					<option value="en" <c:if test="${user.language == 'en'}"> selected="selected"</c:if> >
						<spring:message code="language.en" text="!English" />
					</option>
					<option value="fr" <c:if test="${user.language == 'fr'}"> selected="selected"</c:if>>
						<spring:message code="language.fr" text="!French" />
					</option>
				</form:select>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<div class="row">
			<div class="col-lg-12 text-right">
	  			<a onclick="doEditForm('MODIFY','${user.sysUserId}')" class="btn btn-w-m btn-success text-uppercase" type="submit">
	  				<i class="fa fa-check"></i> <spring:message code="button.go" text="!Go"/>
	  			</a>
			</div>
		</div>
	</div>	
			
	<div class="form-group">
		<div class="col-sm-12">&nbsp;</div>
		<div class="hr-line-dashed"></div>
	</div>
</form:form>
			
<!-- ------------------------------------------------------------------------------------------------------------- -->
<form:form name="form_add_roleuser" id="form_add_roleuser" method="post" modelAttribute="userFilter">
	<input name="action" type="hidden">
	<input name="id" type="hidden">
	<input name="idUser" type="hidden">
	
	<div class="form-group">
		<div class="row">
			<div class="col-sm-4">
				<label><spring:message code="user.table.role" text="!ROLE"></spring:message></label>
		    </div>
		</div>
	</div>

	<div class="form-group">
		<div class="row">
			<div class="col-sm-4">
				<form:select id="roleChange" path="role" onChange="doClickSelectRole()" cssClass="chosen-select" cssStyle="width:100%;">
					<form:option value=""><spring:message code="select" text="-- SELECT --" /></form:option>
					<c:forEach items="${lstRole }" var="elm">
						<form:option value="${elm.authority }">${elm.authority }</form:option>
					</c:forEach>
				</form:select>
		    </div>
		    
		    <div id="hiddenRole" hidden="true">
			    <div class="col-sm-2">
		    		<label style="margin-top: 10px;" class="text-right"><spring:message code="user.label.role" text="!"/></label>
			    </div>
			    <div class="col-sm-2">
			    	<div class="checkbox checkbox-primary">
			    		 <input id="roleCreate" name="roleCreate" value="_C" type="checkbox"/>
                         <label for="roleCreate">
                             <spring:message code="user.checkbox.create" text="!"/>
                         </label>
                     </div>
			    </div>
			    <div class="col-sm-2">
			    	<div class="checkbox checkbox-primary">
                         <input id="roleUpdate" name="roleUpdate" value="_U" type="checkbox"/>
                         <label for="roleUpdate">
                             <spring:message code="user.checkbox.update" text="!"/>
                         </label>
                     </div>
			    </div>
			    <div class="col-sm-2">
			    	<div class="checkbox checkbox-primary">
                         <input id="roleDelete" name="roleDelete" value="_D" type="checkbox"/>
                         <label for="roleDelete">
                             <spring:message code="user.checkbox.delete" text="!"/>
                         </label>
                     </div>
			    </div>
		    </div>
	    </div>
	</div>
	    
 	<div class="form-group">
  		<div class="row">
  			<div class="col-sm-12 text-right">
				<a onclick="doAddButton('${userId}')"  class="btn btn-w-m btn-success text-uppercase" type="submit">
	  				<i class="fa fa-check"></i> <spring:message code="button.valider" text="!"/>
	 	 		</a>
		  	</div>
		 </div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<table class="table table-striped table-bordered table-hover table-manifest" style="width:100%;">
				<thead>
					<tr>
						<th class="text-center"><spring:message code="commom.no" text="!No"></spring:message></th>
						<th class="text-center"><spring:message code="commom.action" text="!Action"></spring:message></th>
						<th class="text-left"><spring:message code="role.table.authority" text="!Role name"></spring:message></th>
						<th><spring:message code="role.table.description" text="!Detail"></spring:message></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${lstRoleUser }" var="elm" varStatus="stt">
						<tr ${elm.sysRoleId }>
							<td class="text-center">${stt.index + 1 }</td>
							<td class="text-center">
								<a onClick="ConformDelete('${elm.sysRoleId }', '${userId }', '${elm.authority }');"
								title="<spring:message code="button.delete" text="!Delete"/>"><i class="fa fa-2x fa-trash-o"></i>
								</a>
							</td>
							<td class="text-left">${elm.authority } 
								<c:if test="${elm.authority != 'ROLE_DW' && elm.authority != 'ROLE_ADMIN' }">
									<ol class="breadcrumb">
										<c:if test="${checkCUD.get(elm.authority) == true }">( </c:if>
										<c:forEach items="${lstAllRoleUser}" var="elmRole" varStatus="stt">
											<c:choose>
												<c:when test="${elmRole.authority.endsWith('_C') && fn:substring(elmRole.authority, 0, elmRole.authority.length() - 2) == elm.authority}">
													<li><spring:message code="user.checkbox.create" text="!"/></li>
												</c:when>
												
												<c:when test="${elmRole.authority.endsWith('_U') && fn:substring(elmRole.authority, 0, elmRole.authority.length() - 2) == elm.authority }">
													 <li><spring:message code="user.checkbox.update" text="!"/></li>
												</c:when>
												
												<c:when test="${elmRole.authority.endsWith('_D') && fn:substring(elmRole.authority, 0, elmRole.authority.length() - 2) == elm.authority }">
													<li><spring:message code="user.checkbox.delete" text="!"/></li>
												</c:when>
											</c:choose>
										</c:forEach>
										<c:if test="${checkCUD.get(elm.authority) == true }">)</c:if>
									</ol>
								</c:if>
							</td>
							<td class="text-left">${elm.description }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</form:form>