<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>

<link href="<c:url value="/static/css/jquery-ui.css"/>" rel="stylesheet">

<style type="text/css">
     .ui-autocomplete { position: absolute; cursor: default;z-index:300000 !important;} 
	 .checkbox label, .radio label {
	     cursor: pointer;
	     font-weight: 400;
	     margin-bottom: 0;
	     min-height: 20px;
	     padding-left: 0px;
	 }
	 .hr-line-dashed {
	    background-color: #ffffff;
	    border-top: 2px dashed #808080;
	    color: #ffffff;
	    height: 1px;
	    margin: 1px 5px 10px 10px;
	}
</style>

<script type="text/javascript">
	document.getElementById("param").className = "active";
	document.getElementById("param_agentgsa").className = "active";

	$(function () {
	 	$('.table-result').dataTable({
	 		"dom": 'T<"clear">lfrtip',
	        "sDom": "<'row'<'col-sm-6 text-left'><'col-sm-6 text-right'>r>" + "t" + "<'row'<'col-sm-6'><'col-sm-6'p>>",
			"bSort": false,
			"paginate": false
	 	}); 
	});
	
	function doAddForm(code){
		$.ajax({
			url: "<spring:url value='/secure/agentGsa/"+ code + "/adddetail/error.json'/>", type: "POST",
			cache: false,
			data: $('#form-add-detail').serialize(),
			success: function(result){
				if(result.errCodeEdit > 0){
		           	 $('#err_agtnAddDetail').html('');
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++)
	               	 {
	                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
	        	}else{
					$('#form-add-detail').submit();
	        	}
			}
		});
	}
	
	$(document).ready(function(){
		$('#agtnAddDetail').autocomplete({
			 minLength: 1,
			 source: function(request, response) {
				 $.ajax({
					 url: "ajaxAgentGsa.json",
					 dataType: "json",
					 data: { agtnAddDetail: request.term },
					 success: function(data) {
						 response($.map(data, function(item) {
							 return {
								 label : item,
							     value : item
							 }
						 }));
					 }
				 });
			 }
		});
	});
	
	function doBackButton(action){
		document.forms.namedItem('formDetail').elements['action'].value = action;
		$('#formDetail').submit();
	}
	
	function ConformDeleteDetail(action, agtn, codeAgentGsa){
		swal({
            title: "<spring:message code="message.delete" text="!"/>" + " [" + agtn + "] ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<spring:message code="button.delete" text="!"/>",
            cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
            closeOnConfirm: false
        }, function () {
       	 	document.forms.namedItem('formDetail').elements['action'].value='DELETE';
			document.forms.namedItem('formDetail').elements['id'].value=agtn;
			document.forms.namedItem('formDetail').elements['codeAgentGsa'].value = codeAgentGsa
			$('#formDetail').submit();
        });
	}
</script>

<div id="body">
	<form:form id="formDetail" name="formDetail" method="get" commandName="agentGsaFilter">
		<input type="hidden" name="action">
		<input type="hidden" name="id">
		<input type="hidden" name="codeAgentGsa">
		<div class="row">
			<div class="col-sm-12">
				<button onclick="doBackButton('BACK');" type="button" class="btn btn-w-m btn-default text-uppercase">
					<spring:message code="button.back" text="!"/>
				</button>
			</div>
		</div>
		&nbsp;
		<div class="row">
			<div class="col-sm-12">
				<table class="table table-striped table-bordered table-hover table-result" style="width:100%;">
					<thead>
						<tr>
							<th><spring:message code="agentgsa.code" text="!Code"/></th>
							<th><spring:message code="agentgsa.name" text="!Name"/></th>
							<th class="text-right"><spring:message code="agentgsa.comm" text="!Comm"/></th>
							<th class="text-left"><spring:message code="agentgsa.datestart" text="!Date start"/></th>
							<th class="text-left"><spring:message code="agentgsa.dateend" text="!Date end"/></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>${fn:trim(agentGsa.code)}</td>
							<td>${agentGsa.gsaName}</td>
							<td class="text-right">${agentGsa.comm}</td>
							<td class="text-left"><fmt:formatDate pattern="dd/MM/yyyy" value="${agentGsa.dateStart}"/></td>
							<c:choose>
								<c:when test="${agentGsa.dateEnd.toString().contains('9999') == false}">
									<td class="text-left"><fmt:formatDate pattern="dd/MM/yyyy" value="${agentGsa.dateEnd}"/></td>
								</c:when>
								<c:otherwise>
									<td></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="row">
			<div class="col-sm-12">
				<button type="button" class="btn btn-success btn-w-m text-uppercase" data-toggle="modal" data-target="#formAddDetail">
					<i class="fa fa-plus-square"> <spring:message code="button.add" text="!Add"/></i>
				</button>
			</div>
		</div>
		&nbsp;
		<div class="row">
			<div class="col-sm-12">
				<table class="table table-striped table-bordered table-hover table-result" style="width:100%;">
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th class="text-center"><spring:message code="commom.action" text="!Action"/></th>
							<th><spring:message code="agentgsa.agtn" text="!Code"/></th>
							<th><spring:message code="agentgsa.name" text="!Name"/></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${listAgentGsaDetail }" var="elm" varStatus="stt">
							<tr>
								<td class="text-center">${stt.index + 1}</td>
								<td class="text-center">
									<a onclick="ConformDeleteDetail('DELETE','${elm.agtn}', '${code }')"
										title="<spring:message code="message.delete" text="!Delete"/>"><i class="fa fa-2x fa-trash-o"></i>
									</a>
								</td>
								<td>${elm.agtn}</td>
								<td>${elm.gsaName }</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</form:form>
<!-- 	----------------------------------------------------------------------------------------------------- -->
	<div class="modal inmodal" id="formAddDetail" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" style="width:800px">
			<div class="modal-content animated bounceInRight">
				<div class="modal-body">
				<form:form id="form-add-detail" name="form-add-detail" method="post" commandName="agentGsaFilter" action="${code}/adddetail">
					<input type="hidden" name="action" value="ADD">	
					<input type="hidden" name="agentgsa_name" value="${agentGsa.gsaName }">
					<input type="hidden" name="agentgsa_comm" value="${agentGsa.comm }">
					<input type="hidden" name="agentgsa_dateStart" value="${agentGsa.dateStart }">
					<input type="hidden" name="agentgsa_dateEnd" value="${agentGsa.dateEnd }">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label><spring:message code="agentgsa.code" text="!Code GSA"/></label>
								<form:input path="codeAddDetail" type="text" value="${agentGsa.code}" cssClass="form-control textfield" readonly="true"/>
								<label id="err_codeAddDetail" class="text-danger"></label>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label><spring:message code="agentgsa.agtn" text="!AGTN"/></label>
								<form:input path="agtnAddDetail" type="text" maxlength="10" cssClass="form-control textfield"/>
								<label id="err_agtnAddDetail" class="text-danger"></label>
							</div>
							
						</div>
					</div>
					</form:form>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<button id ="add" type="button" onclick="doAddForm('${code}');" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-plus-square "></i> <spring:message code="button.create" text="!Create"/>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>