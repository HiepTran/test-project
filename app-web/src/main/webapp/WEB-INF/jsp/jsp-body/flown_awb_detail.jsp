<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<style type="text/css">
	.hr-line-dashed {
	    background-color: #ffffff;
	    border-top: 1px dashed #e7eaec;
	    color: #ffffff;
	    height: 1px;
	    margin: 1px 5px 10px 10px;
	}
	
	.chosen-container-single .chosen-single {
         background: none;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
</style>

<script type="text/javascript">
	document.getElementById("manifest").className = "active";
	document.getElementById("listManifest").className = "active";

	$(document).ready(function() {

		$('.input-group.date').datepicker({
			todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
			format: "dd/mm/yyyy"
	    });
	    $('.chosen-select').chosen({disable_search_threshold: 10});
	});

	function doValidatorAwb(){
		$.ajax({
			url: "<spring:url value='/secure/awb/flown/detail/error.json'/>",
	        data: $('#form').serialize(), 
	        type: "POST",
	        success: function(result){
	        	if(result.errCode > 0){
	           	 	$('#err_isoc').html('');
	           	 	$('#err_dateExecute').html('');
   					$('#err_orig').html('');
					$('#err_dest').html('');
		           	$('#err_km').html('');
		           	$('#err_flightNumber').html('');
		           	$('#err_flightDate').html('');
		           	$('#err_errorTax').html('');
				           																							
	           	 	var len = $.map(result.lstErr, function(n, i) { return i; }).length;
	           	 	for(var i=0;i<len;i++) {
                   		$('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
	        	}else{
	        		document.forms[0].elements['action'].value='VALIDER';
	        		$('#form').submit();
	        	}
	        },
		 	error: function(XMLHttpRequest, textStatus, errorThrown){}
		});
	}	
</script>

<div id="body">
<form:form name="form" id="form" method="post" commandName="awbFilter">
	<input type="hidden" name="action">

	<div class="row">
		<div class="form-group">
			<div class="col-lg-6">
				<a onclick="doSubmit('BACK')" class="btn btn-w-m btn-default text-uppercase">
					<spring:message code="button.back" text="!"/>
				</a>
			</div>
			<div class="col-lg-6 text-right">
				<a onclick="javascript:doValidatorAwb()" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-check"></i> <spring:message code="button.valider" text="!"/>
				</a>
			</div>
		</div>
	</div>
	&nbsp;
	
	<div class="row">
		<div class="col-lg-1">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.awb" text="!"/></label>
				<form:input path="tacn" readonly="true" value="${awbFlown.tacn }" class="form-control textfield"/>
			</div>
		</div>
		<div class="col-lg-2">
			<div class="form-group">
				<label>&nbsp;</label>
				<form:input path="lta" readonly="true" value="${awbFlown.lta }" class="form-control textfield"/>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.agent" text="!"/></label>
				<form:input path="agtn" readonly="true" value="${awbFlown.agtn}" class="form-control textfield"/>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.agent.gsa" text="!"/></label>
				<input value="${awbFlown.agtnGsa}" readonly class="form-control textfield"/>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.rdv" text="!"/></label>
				<form:input path="rdv" readonly="true" value="${awbFlown.rdv}" class="form-control textfield"/>
			</div>
		</div>
	</div>
			
	<div class="row">
		<div class="col-lg-3">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.country" text="!"/></label>
				<form:input path="isoc" maxlength="2" value="${awbFlown.isoc}" class="form-control textfield text-uppercase"/>
				<label id="err_isoc" class="text-danger"></label>
			</div>
		</div>
		<div class="col-lg-6">
			<label class="control-label"><spring:message code="awb.agent.name" text="!"/></label>
			<input type="text" readonly="readonly" value="${awbFlown.agtnName }" class="form-control textfield">
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.date.execution" text="!Date execution"/></label>
				<fmt:formatDate pattern="dd/MM/yyyy" value="${awbFlown.dateExecute}" var="dateExecute"/>
				<div class="input-group date">
	               	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	               	<form:input path="dateExecute" value="${dateExecute }" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
				</div>
				<label id="err_dateExecute" class="text-danger"></label>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.shipper" text="!Shipper"/></label>
				<form:input path="shipName" value="${awbFlown.shipName}" class="form-control textfield"/>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.consignee" text="!Consignee's"/></label> 
				<form:input path="consigneeName" value="${awbFlown.consigneeName}" class="form-control textfield"/>
			</div>
		</div>
	</div>
				
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<form:input path="shipAddress1" value="${awbFlown.shipAddress1 }" class="form-control textfield"/>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<form:input path="consigneeAddress1" value="${awbFlown.consigneeAddress1 }" class="form-control textfield"/>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<form:input path="shipAddress2" value="${awbFlown.shipAddress2}" class="form-control textfield"/>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<form:input path="consigneeAddress2" value="${awbFlown.consigneeAddress2 }" class="form-control textfield"/>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<form:input path="shipAddress3" value="${awbFlown.shipAddress3 }" class="form-control textfield"/>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<form:input path="consigneeAddress3" value="${awbFlown.consigneeAddress3 }" class="form-control textfield"/>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-3">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.orig" text="!"/></label>
				<form:input path="orig" value="${awbFlown.orig }" maxlength="3" class="form-control textfield text-uppercase"/>
				<label id="err_orig" class="text-danger"></label>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.dest" text="!"/></label> 
			 	<form:input path="dest" value="${awbFlown.dest }" maxlength="3" class="form-control textfield text-uppercase"/>
			 	<label id="err_dest" class="text-danger"></label>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.km" text="!Km"/></label> 
				<form:input path="km" value="${awbFlown.km }" class="form-control textfield text-uppercase"/>
				<label id="err_km" class="text-danger"></label>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.cutp" text="!"/></label>
				<form:select path="cutp" class="chosen-select" cssStyle="width:100%;">
					<option value="CHF" <c:if test="${'CHF' == awbFlown.cutp}">selected="selected"</c:if>>CHF</option>
					<option value="EUR" <c:if test="${'EUR' == awbFlown.cutp}">selected="selected"</c:if>>EUR</option>
					<option value="NGN" <c:if test="${'NGN' == awbFlown.cutp}">selected="selected"</c:if>>NGN</option>
					<option value="USD" <c:if test="${'USD' == awbFlown.cutp}">selected="selected"</c:if>>USD</option>
					<option value="XAF" <c:if test="${'XAF' == awbFlown.cutp}">selected="selected"</c:if>>XAF</option>
					<option value="XOF" <c:if test="${'XOF' == awbFlown.cutp}">selected="selected"</c:if>>XOF</option>
				</form:select>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-3">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.flight.number" text="!"/></label> 
				<form:input path="flightNumber" value="${awbFlown.flightNumber }" class="form-control textfield text-uppercase"/>
				<label id="err_flightNumber" class="text-danger"></label>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.flight.date" text="!"/></label>
				<fmt:formatDate pattern="dd/MM/yyyy" value="${awbFlown.flightDate}" var="flightDate"/>
				<div class="input-group date">
                       <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                       <form:input path="flightDate" value="${flightDate }" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
				</div>
				<label id="err_flightDate" class="text-danger"></label>
			</div>
		</div>
		
		<div class="col-lg-3">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.declared.carrier" text="!"/></label> 
				<form:input path="declaredCarrige" value="${awbFlown.declaredCarrige}" class="form-control textfield text-uppercase"/>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.declared.custom" text="!"/></label> 
				<form:input path="declaredCustom" value="${awbFlown.declaredCustom}" class="form-control textfield text-uppercase"/>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-3">
			
			<div class="form-group">
              	<label class="control-label"><spring:message code="awb.fare" text="!"/></label>
              	<div class="radio radio-primary">
                    <input type="radio" name="chargeIndicator" id="radio1" value="P"
                    <c:if test="${fn:trim(awbFlown.chargeIndicator) eq 'P'}">checked</c:if>>
                    <label for="radio1">
                        <spring:message code="awb.fare.pp" text="!"/>
                    </label>
                </div>
                <div class="radio radio-primary">
                    <input type="radio" name="chargeIndicator" id="radio2" value="C"
                    <c:if test="${fn:trim(awbFlown.chargeIndicator) eq 'C'}">checked</c:if>>
                    <label for="radio2">
                        <spring:message code="awb.fare.cc" text="!"/>
                    </label>
                </div>
           	</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
               	<label class="control-label"><spring:message code="awb.other" text="!"/></label>
               	<div class="radio radio-primary">
                          <input type="radio" name="otherIndicator" id="radio3" value="P" 
                          <c:if test="${fn:trim(awbFlown.otherIndicator) eq 'P'}">checked</c:if>>
                          <label for="radio3">
                              <spring:message code="awb.other.pp" text="!"/>
                          </label>
                      </div>
                      <div class="radio radio-primary">
                          <input type="radio" name="otherIndicator" id="radio4" value="C"
                          <c:if test="${fn:trim(awbFlown.otherIndicator) eq 'C'}">checked</c:if>>
                          <label for="radio4">
                              <spring:message code="awb.other.cc" text="!"/>
                          </label>
                      </div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<label class="control-label"><spring:message code="awb.handling" text="!"/></label>
				<textarea name="handlingInfor" rows="2" class="form-control textfield">${awbFlown.handlingInfor }</textarea>
			</div>
		</div>
	</div>		
	
	<div class="hr-line-dashed"></div>
		
	<!-- DETAIL -->
	<div class="row">
		<div class="form-group">
			<label class="col-lg-1"><spring:message code="awb.rcp" text="!"/></label>
			<label class="col-lg-1 text-right"><spring:message code="awb.weight.gross" text="!"/></label> 
			<label class="col-lg-1"><spring:message code="awb.weight.indicator" text="!"/></label> 
			<label class="col-lg-1"><spring:message code="awb.rate" text="!"/></label> 
			<label class="col-lg-1 text-right"><spring:message code="awb.weight.charge" text="!"/></label> 
			<label class="col-lg-2 text-right"><spring:message code="awb.tarif" text="!"/></label> 
			<label class="col-lg-2 text-right"><spring:message code="awb.total" text="!"/></label> 
			<label class="col-lg-3"><spring:message code="awb.description" text="!"/></label> 
		</div>
	</div>
			
	<c:set value="0" var="_cnt" />
	<spring:message code="awb.tarif.cass" text="!" var="titleTarif"/>
	<c:forEach items="${awbFlown.awbFlownDetail}" var="elm">
		<div class="form-group">
			<div class="row">
				<div class="col-lg-1">
					<form:hidden path="awbDetail[${_cnt }].awbDetailId" value="${elm.awbFlownDetailId}" cssClass="form-control textfield text-uppercase"/>
					<form:input path="awbDetail[${_cnt }].quantity" readonly="true" value="${elm.quantity}" cssClass="form-control textfield text-uppercase"/>
				</div>
				<div class="col-lg-1">
					<form:input path="awbDetail[${_cnt }].weightGross" readonly="true" value="${elm.weightGross}" cssClass="form-control textfield text-right"/>
				</div>
				<div class="col-lg-1">
					<form:input path="awbDetail[${_cnt }].weightIndicator" readonly="true" value="${elm.weightIndicator}" cssClass="form-control textfield text-right"/>
				</div>
				<div class="col-lg-1">
					<form:input path="awbDetail[${_cnt }].rateClass" readonly="true" value="${elm.rateClass}" class="form-control textfield text-uppercase"/>
				</div>
				<div class="col-lg-1 text-right">
					<form:input path="awbDetail[${_cnt }].weightCharge" readonly="true" value="${elm.weightCharge}" class="form-control textfield text-right"/>
				</div>
				<div class="col-lg-2 text-right">
					<form:input path="awbDetail[${_cnt }].unit" readonly="true" value="${elm.unit}" title="${titleTarif}: ${elm.unitCass}" class="form-control textfield text-right"/>
				</div>
				<div class="col-lg-2 text-right">
					<form:input path="awbDetail[${_cnt }].amount" readonly="true" value="${elm.amount}" class="form-control textfield text-right"/>
				</div>
				<div class="col-lg-3">
					<form:input path="awbDetail[${_cnt }].description" readonly="true" value="${elm.description}" class="form-control textfield"/>
				</div>
			</div>
		</div>
		 <c:set value="${_cnt+1 }" var="_cnt" />
	</c:forEach>
					
	<div class="hr-line-dashed"></div>
	
	<div class="row">
		<div class="col-lg-7">
			<div class="row">
				<div class="form-group">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 text-right">
						<label class="control-label"><spring:message code="awb.prepaid" text="!"/></label>
					</div>
					<div class="col-lg-4 text-right">
						<label class="control-label"><spring:message code="awb.collect" text="!"/></label>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-4">
					<label class="control-label"><spring:message code="awb.total.weight.charge" text="!Prepaid"/></label>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<input value="${awbFlown.weightPp}" readonly="readonly" class="form-control textfield text-right"/>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<input value="${awbFlown.weightCc}" readonly="readonly" class="form-control textfield text-right"/>
					</div>
				</div>
			</div>
				
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label class="control-label"><spring:message code="awb.total.charge.agent" text="!"/></label>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<input value="${awbFlown.agentPp}" readonly="readonly" class="form-control textfield text-right"/>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<input value="${awbFlown.agentCc}" readonly="readonly" class="form-control textfield text-right"/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label class="control-label"><spring:message code="awb.total.charge.carrier" text="!"/></label>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<input value="${awbFlown.carrierPp}" readonly="readonly" class="form-control textfield text-right"/>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<input value="${awbFlown.carrierCc}" readonly="readonly"  class="form-control textfield text-right"/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label class="control-label"><spring:message code="awb.total" text="!"/></label>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<input value="${awbFlown.totalPp}" readonly="readonly" class="form-control textfield text-right"/>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<input value="${awbFlown.totalCc}" readonly="readonly" class="form-control textfield text-right"/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label class="control-label"><spring:message code="awb.net" text="!"/></label>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<input value="${awbFlown.net}" readonly="readonly" class="form-control textfield text-right"/>
					</div>
				</div>
			</div>
				
			<div class="row">
				<div class="col-lg-12">
					<div class="hr-line-dashed"></div>
				</div>
			</div>
			
			<!-- Commission -->
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label class="control-label"><spring:message code="awb.com.percent" text="!"/></label>
						<c:if test="${not empty awbFlown.agtnGsa }">(GSA)</c:if>
					</div>
				</div>
				<div class="col-lg-4 text-right">
					<div class="form-group">
						<form:input path="comm" readonly="true" type="text" value="${awbFlown.comm}" class="form-control textfield text-right"/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<label class="control-label"><spring:message code="awb.vat" text="!"/></label>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group text-right">
						<form:input path="vat" readonly="true" value="${awbFlown.vat}" class="form-control textfield text-right"/>
					</div>
				</div>
			</div>
				
			<div class="row">
				<div class="form-group">
					<div class="col-lg-12">
						<label class="control-label"><spring:message code="awb.note" text="!"/></label>
						<textarea name="descriptUser" rows="2" class="form-control textfield">${awb.descriptUser }</textarea>
					</div>
				</div>
			</div>
						
		</div>

		<div class="col-lg-1"></div>
		
		<div class="col-lg-4">
			<div class="row">
				<div class="form-group">
					<div class="col-lg-4">
						<label class="control-label"><spring:message code="awb.tax" text="!"/></label>
					</div>
					<div class="col-lg-8 text-right">
						<label><spring:message code="awb.tax.value" text="!"/></label>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-12">
					<label id="err_errorTax" class="text-danger"></label>
				</div>
			</div>
			
			<c:set value="0" var="_cnt" />
			<c:forEach items="${awbFlown.awbFlownTax}" var="elm">
				<div class="form-group">
					<div class="row">
						<div class="col-lg-4">
							<form:input path="awbTax[${_cnt }].code" maxlength="3" value="${elm.taxCode}" cssClass="form-control textfield text-uppercase"/>
						</div>
						<div class="col-lg-8">
							<form:input path="awbTax[${_cnt }].amount" value="${elm.taxAmount}" cssClass="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				 <c:set value="${_cnt+1 }" var="_cnt" />
			</c:forEach>
		</div>
	</div>
	&nbsp;
	<div class="row">
		<div class="form-group">
			<div class="col-lg-12 text-right">
				<a onclick="javascript:doValidatorAwb()" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-check"></i> <spring:message code="button.valider" text="!"/>
				</a>
			</div>
		</div>
	</div>
	&nbsp;
</form:form>
</div>