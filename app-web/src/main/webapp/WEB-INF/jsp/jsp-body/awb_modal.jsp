<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript">
	function doView(action, id, idFlown){	
		document.forms[1].elements['action'].value = action;	
		document.forms[1].elements['id'].value = id;
		document.forms[1].elements['idFlown'].value = idFlown;
		document.forms[1].submit();
	}
</script>

		
<div class="modal-dialog" >
	<div class="modal-content animated bounceInRight" style="width: 1250px;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
			<h5 class="modal-title"><spring:message code="awb.awb" text="!"/> <span>${awb.lta }</span></h5>
		</div>
		<div class="modal-body">
			<form:form id="form-view" name="form-view" method="post" action="awb">
				<input type="hidden" name="action">
				<input type="hidden" name="id">
				<input type="hidden" name="idFlown">
				
				<c:set var="formatPattern" value="#,##0.00"/>
				
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th rowspan="2" style="vertical-align:middle;" class="text-center"><spring:message code="commom.action" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.awb" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.agent" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;" class="text-center"><spring:message code="awb.country" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.rdv" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.orig" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.dest" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="manifest.line" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;" class="text-center">P/C</th>
							<th rowspan="2" class="text-right" style="vertical-align:middle;"><spring:message code="awb.list.weight.charge" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.currency" text="!"/></th>
							<th colspan="3" class="text-center"><spring:message code="awb.prepaid" text="!"/></th>
							<th colspan="3" class="text-center"><spring:message code="awb.collect" text="!"/></th>
							<th rowspan="2" class="text-right" style="vertical-align:middle;"><spring:message code="awb.list.com" text="!"/></th>
						</tr>
						
						<tr>
							<th class="text-right"><spring:message code="awb.list.charge" text="!"/></th>
							<th class="text-right text-nowrap"><spring:message code="awb.list.charge.agent" text="!"/></th>
							<th class="text-right text-nowrap"><spring:message code="awb.list.charge.carrier" text="!"/></th>
							<th class="text-right"><spring:message code="awb.list.charge" text="!"/></th>
							<th class="text-right text-nowrap"><spring:message code="awb.list.charge.agent" text="!"/></th>
							<th class="text-right text-nowrap"><spring:message code="awb.list.charge.carrier" text="!"/></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="text-center">
								<a onclick="doView('VIEW','${awb.lta}','')" title="<spring:message code="message.view" text=""/>"><i class="fa fa-2x fa-edit"></i></a>
							</td>
							<td>${awb.tacn}-${awb.lta}</td>
							<td>${awb.agtn}</td>
							<td class="text-center">${awb.isoc}</td>
							<td>${awb.rdv}</td>
							<td>${awb.orig}</td>
							<td>${awb.dest}</td>
							<td>${awb.line}</td>
							<td class="text-center">${awb.chargeIndicator}</td>
							<td class="text-right text-nowrap"><fmt:formatNumber pattern="${formatPattern}" value="${awb.weightGross}"/> ${awb.weightIndicator}</td>
							<td>${awb.cutp}</td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.weightPp}"/></td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.agentPp}"/></td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.carrierPp}"/></td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.weightCc}"/></td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.agentCc}"/></td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.carrierCc}"/></td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.comm}"/></td>
						</tr>
						<c:if test="${not empty awbFlowns }">
						<tr>
							<td colspan="17"></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td class="text-nowrap"><strong><spring:message code="manifest.title" text="!"/>-<spring:message code="awb.coupon" text="!coupon"/></strong></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						
						<c:set value="0.0" var ="weightCharge"></c:set>
						<c:set value="0.0" var ="weightPp"></c:set>
						<c:set value="0.0" var ="agentPp"></c:set>
						<c:set value="0.0" var ="carrierPp"></c:set>
						<c:set value="0.0" var ="weightCc"></c:set>
						<c:set value="0.0" var ="agentCc"></c:set>
						<c:set value="0.0" var ="carrierCc"></c:set>
						
						<c:forEach items="${awbFlowns}" var="elm" varStatus="stt">
							<c:choose>
								<c:when test="${elm.statusFlown eq 'F' }">
									<tr class="${cssColor }">
										<td class="text-center">
											<a onclick="doView('FLOWN', '${elm.manifestNo}', '${elm.awbFlownId}')" title="<spring:message code="message.view" text=""/>"><i class="fa fa-2x fa-edit"></i></a>
										</td>
										<td class="text-nowrap">${elm.tacn}-${elm.lta}</td>
										<td>${elm.agtn}</td>
										<td class="text-center">${elm.isoc}</td>
										<td>${elm.manifestNo}-${elm.coupon}</td>
										<td>${elm.orig}</td>
										<td>${elm.dest}</td>
										<td>${elm.line}</td>
										<td class="text-center">${elm.chargeIndicator}</td>
										<td class="text-right text-nowrap"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightCharge}"/> ${elm.weightIndicator}</td>
										<td>${elm.cutp}</td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightPp}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentPp}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierPp}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightCc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentCc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierCc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.comm}"/></td>
									</tr>
								</c:when>
								<c:otherwise>
									<tr class="text-success">
										<td></td>
										<td class="text-nowrap">${elm.tacn}-${elm.lta}</td>
										<td>${elm.agtn}</td>
										<td class="text-center">${elm.isoc}</td>
										<td class="text-uppercase"><spring:message code="awb.issued" text="!"/></td>
										<td>${elm.orig}</td>
										<td>${elm.dest}</td>
										<td>${elm.line}</td>
										<td class="text-center">${elm.chargeIndicator}</td>
										<td class="text-right text-nowrap"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightCharge}"/> ${elm.weightIndicator}</td>
										<td>${elm.cutp}</td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightPp}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentPp}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierPp}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightCc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentCc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierCc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.comm}"/></td>
									</tr>
								</c:otherwise>
							</c:choose>
							<c:set value="${weightCharge +  elm.weightCharge}" var ="weightCharge"></c:set>
							<c:set value="${weightPp +  elm.weightPp}" var ="weightPp"></c:set>
							<c:set value="${agentPp +  elm.agentPp}" var ="agentPp"></c:set>
							<c:set value="${carrierPp +  elm.carrierPp}" var ="carrierPp"></c:set>
							<c:set value="${weightCc +  elm.weightCc}" var ="weightCc"></c:set>
							<c:set value="${agentCc +  elm.agentCc}" var ="agentCc"></c:set>
							<c:set value="${carrierCc +  elm.carrierCc}" var ="carrierCc"></c:set>
							<c:set value="${comm +  elm.comm}" var ="comm"></c:set>
						</c:forEach>
						
						<tr>
							<th class="text-right text-uppercase" colspan="9"><spring:message code="awb.total.flown" text="!"/></th>
							<th class="text-right text-nowrap"><fmt:formatNumber pattern="${formatPattern}" value="${weightCharge}"/> ${awb.weightIndicator}</th>
							<th></th>
							<th class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${weightPp}"/></th>
							<th class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${agentPp}"/></th>
							<th class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${carrierPp}"/></th>
							<th class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${weightCc}"/></th>
							<th class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${agentCc}"/></th>
							<th class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${carrierCc}"/></th>
							<th class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${comm}"/></th>
						</tr>
					</c:if>
					</tbody>
			 	</table>
		 	</form:form>
		</div>
	</div>
</div>