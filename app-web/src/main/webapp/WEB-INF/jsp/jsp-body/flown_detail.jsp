<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script type="text/javascript">
	document.getElementById("manifest").className = "active";
	document.getElementById("listManifest").className = "active";
   	
</script>
	
<div id="body">
	<form:form name="form" id="form" method="post" commandName="flownFilter">
	<input type="hidden" name="action">
	<input type="hidden" name="id">
	
	<c:url value="/secure/flown/detail/excel"  var="linkExcel">
		<c:param name="id" value="${manifest.manifestNo}"/>
	</c:url>
							
	<div class="row">
		<div class="col-lg-6">
			<a href="<c:url value="/secure/flown"/>" class="btn btn-w-m btn-default text-uppercase">
        		<spring:message code="button.back" text="!"/>
        	</a>
		</div>	
    	<div class="col-lg-6 text-right">
        	<a href="${linkExcel}" target="_blank" class="btn btn-w-m btn-success text-uppercase">
				<i class="fa fa-file-excel-o"></i> <spring:message code="button.excel" text="!"/>
			</a>
	    </div>
	</div>
	
	<div class="row">&nbsp;</div>
	
		<div class="row">
			<div class="col-lg-12">
				<table class="table table-striped table-bordered table-hover" style="width:100%;">
					<thead>
						<tr>
							<th><spring:message code="manifest.owner" text="!"/></th>
							<th><spring:message code="manifest.no" text="!"/></th>
							<th><spring:message code="manifest.orig" text="!"/></th>
							<th><spring:message code="manifest.dest" text="!"/></th>
							<th><spring:message code="manifest.flight.number" text="!"/></th>
							<th><spring:message code="manifest.flight.date" text="!"/></th>
							<th class="text-right"><spring:message code="manifest.entry.number" text="!"/></th>
							<th class="text-right"><spring:message code="manifest.reality.number" text="!"/></th>
							<th class="text-right"><spring:message code="manifest.rest" text="!"/></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>${manifest.tacn}</td>
							<td class="text-nowrap">${manifest.manifestNo}</td>
							<td>${manifest.orig}</td>
							<td>${manifest.dest}</td>
							<td>${manifest.flightNumber}</td>
							<td><fmt:formatDate pattern="dd/MM/yyyy" value="${manifest.flightDate}" /></td>
							<td class="text-right">${manifest.entryNumber}</td>
							<td class="text-right">${manifest.awbFlownCount}</td>
							<c:choose>
								<c:when test="${manifest.entryNumber == 0}">
									<td class="text-right">N/A</td>
								</c:when>
								<c:when test="${manifest.entryNumber != 0 &&  (manifest.entryNumber - manifest.awbFlownCount) !=0}">
									<td class="text-right text-danger">${manifest.entryNumber - manifest.awbFlownCount}</td>
								</c:when>
								<c:otherwise>
									<td class="text-right">${manifest.entryNumber - manifest.awbFlownCount}</td>
								</c:otherwise>
							</c:choose>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="row">&nbsp;</div>
	
	<div class="row">
		<div class="col-lg-12">		
			<table class="table table-striped table-bordered table-hover table-result" style="width:100%;">
				<thead>
					<tr>
						<th rowspan="2" class="text-center" style="vertical-align:middle;">#</th>
						<th rowspan="2" class="text-center" style="vertical-align:middle;"><spring:message code="commom.action" text="!"/></th>
						<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.awb" text="!"/></th>
						<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.agent" text="!"/></th>
						<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.coupon" text="!coupon"/></th>
						<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.date.execution" text="!"/></th>
						<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.orig" text="!"/></th>
						<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.dest" text="!"/></th>
						<th rowspan="2" style="vertical-align:middle;"><spring:message code="manifest.line" text="!"/></th>
						<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.weight.charge" text="!"/></th>
						<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.currency" text="!"/></th>
						<th colspan="3" class="text-center"><spring:message code="awb.prepaid" text="!"/></th>
						<th colspan="3" class="text-center"><spring:message code="awb.collect" text="!"/></th>
						<th rowspan="2" class="text-right" style="vertical-align:middle;"><spring:message code="awb.list.com" text="!"/></th>
					</tr>
					
					<tr>
						<th class="text-right"><spring:message code="awb.list.charge" text="!"/></th>
						<th class="text-right text-nowrap"><spring:message code="awb.list.charge.agent" text="!"/></th>
						<th class="text-right text-nowrap"><spring:message code="awb.list.charge.carrier" text="!"/></th>
						<th class="text-right"><spring:message code="awb.list.charge" text="!"/></th>
						<th class="text-right text-nowrap"><spring:message code="awb.list.charge.agent" text="!"/></th>
						<th class="text-right text-nowrap"><spring:message code="awb.list.charge.carrier" text="!"/></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${awbFlowns}" var="elm" varStatus="stt">
						<c:url value="/secure/awb/flown/detail"  var="linkAwbDetail">
							<c:param name="id" value="${manifest.manifestNo}"/>
							<c:param name="idFlown" value="${elm.awbFlownId}"/>
						</c:url>
	
						<tr>
							<td class="text-center">${row + (stt.index + 1)}</td>
							<td class="text-center">
								<a href="${linkAwbDetail }" title="<spring:message code="message.view" text="!view"/>"><i class="fa fa-2x fa-search"></i></a>
							</td>
							<td class="text-nowrap">${elm.tacn}-${elm.lta}</td>
							<td>${elm.agtn}</td>
							<td><c:if test="${elm.coupon != 0}">${elm.coupon}</c:if></td>
							<td><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateExecute}" /></td>
							<td>${elm.orig}</td>
							<td>${elm.dest}</td>
							<td>${elm.line}</td>
							<td class="text-right text-nowrap">${elm.weightCharge} ${elm.weightIndicator}</td>
							<td>${elm.cutp}</td>
							<td class="text-right">${elm.weightPp}</td>
							<td class="text-right">${elm.agentPp}</td>
							<td class="text-right">${elm.carrierPp}</td>
							<td class="text-right">${elm.weightCc}</td>
							<td class="text-right">${elm.agentCc}</td>
							<td class="text-right">${elm.carrierCc}</td>
							<td class="text-right">${elm.comm}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	</form:form>
</div>

<script type="text/javascript">
	$(function () {
 	 	$('.table-result').dataTable({
 	 		"dom": 'T<"clear">lfrtip',
 	        "sDom": "<'row'<'col-sm-6 text-left'><'col-sm-6 text-right'>r>" + "t" + "<'row'<'col-sm-6'><'col-sm-6'p>>",
 			"bSort": false,
 			"paginate": false,
 			"scrollY": true,
 			"scrollY": '420px',
 	        "scrollCollapse": true,
 	        "scrollX": true
 	   	});
	});
   	
</script>