<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script>
	$(document).ready(function() {
		$('#add').click(function(){
			$('#form-add').submit();
		});
	});
</script>

<div class="modal-dialog">
	<div class="modal-content animated bounceInRight" style="width: 1150px;">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
			<h5 class="modal-title"><spring:message code="manifest.title" text="!"/>: <span class="text-danger">${manifest.manifestNo } ${manifest.orig } - ${manifest.dest } ${km } Km</span></h5>
		</div>
		<div class="modal-body">
			<form:form id="form-add" name="form-add" method="post" modelAttribute="awbAddFilter" action="add" >
				<input type="hidden" name="action" value="ADD_MODAL_AWB_KM">
				<input type="hidden" name="ltaAdd" value="${awb.lta }">
				<input type="hidden" name="manifestNo" value="${manifest.manifestNo }">
			
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.awb" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.agent" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.rdv" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.orig" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.dest" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="manifest.line" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;" class="text-center">P/C</th>
							<th rowspan="2" class="text-right" style="vertical-align:middle;"><spring:message code="awb.list.weight.charge" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.currency" text="!"/></th>
							<th colspan="3" class="text-center"><spring:message code="awb.prepaid" text="!"/></th>
							<th colspan="3" class="text-center"><spring:message code="awb.collect" text="!"/></th>
							<th rowspan="2" class="text-right" style="vertical-align:middle;"><spring:message code="awb.list.com" text="!"/></th>
						</tr>
						
						<tr>
							<th class="text-right"><spring:message code="awb.list.charge" text="!"/></th>
							<th class="text-right text-nowrap"><spring:message code="awb.list.charge.agent" text="!"/></th>
							<th class="text-right text-nowrap"><spring:message code="awb.list.charge.carrier" text="!"/></th>
							<th class="text-right"><spring:message code="awb.list.charge" text="!"/></th>
							<th class="text-right text-nowrap"><spring:message code="awb.list.charge.agent" text="!"/></th>
							<th class="text-right text-nowrap"><spring:message code="awb.list.charge.carrier" text="!"/></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>${awb.tacn}-${awb.lta}</td>
							<td>${awb.agtn}</td>
							<td>${awb.rdv}</td>
							<td>${awb.orig}</td>
							<td>${awb.dest}</td>
							<td>${awb.line}</td>
							<td class="text-center">${awb.chargeIndicator}</td>
							<td class="text-right text-nowrap">${awb.weightGross} ${awb.weightIndicator}</td>
							<td>${awb.cutp}</td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.weightPp}"/></td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.agentPp}"/></td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.carrierPp}"/></td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.weightCc}"/></td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.agentCc}"/></td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.carrierCc}"/></td>
							<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${awb.comm}"/></td>
						</tr>
					</tbody>
				</table>
				 
			 	<!-- DETAIL -->
				<div class="row">
					<div class="form-group">
						<label class="col-lg-1"><spring:message code="awb.rcp" text="!"/></label>
						<label class="col-lg-1 text-right"><spring:message code="awb.weight.gross" text="!"/></label> 
						<label class="col-lg-1"><spring:message code="awb.weight.indicator" text="!"/></label> 
						<label class="col-lg-1"><spring:message code="awb.rate" text="!"/></label> 
						<label class="col-lg-1 text-right"><spring:message code="awb.weight.charge" text="!"/></label> 
						<label class="col-lg-2 text-right"><spring:message code="awb.tarif" text="!"/></label> 
						<label class="col-lg-2 text-right"><spring:message code="awb.total" text="!"/></label> 
						<label class="col-lg-3"><spring:message code="awb.description" text="!"/></label> 
					</div>
				</div>
				 
			 	<spring:message code="awb.tarif.cass" text="!" var="titleTarif"/>
				<c:forEach items="${awb.awbDetail}" var="elm">
					<div class="form-group">
						<div class="row">
							<div class="col-lg-1">
								<input value="${elm.quantity}" readonly="readonly" class="form-control textfield"/>
							</div>
							<div class="col-lg-1">
								<input value="${elm.weightGross}" readonly="readonly" class="form-control textfield text-right"/>
							</div>
							<div class="col-lg-1">
								<input value="${elm.weightIndicator}" readonly="readonly" class="form-control textfield"/>
							</div>
							<div class="col-lg-1">
								<input value="${elm.rateClass}" readonly="readonly" class="form-control textfield"/>
							</div>
							<div class="col-lg-1 text-right">
								<input value="${elm.weightCharge}" readonly="readonly" class="form-control textfield text-right"/>
							</div>
							<div class="col-lg-2 text-right">
								<input value="${elm.unit}" readonly="readonly" title="${titleTarif}: ${elm.unitCass}" class="form-control textfield text-right"/>
							</div>
							<div class="col-lg-2 text-right">
								<input value="${elm.amount}" readonly="readonly" class="form-control textfield text-right"/>
							</div>
							<div class="col-lg-3">
								<input value="${elm.description}" readonly="readonly" class="form-control textfield"/>
							</div>
						</div>
					</div>
				</c:forEach>
				 
			 	<c:set value="0" var="_cnt" />
				<spring:message code="awb.tarif.cass" text="!" var="titleTarif"/>
				<c:forEach items="${setAwbDetail}" var="elm">
					<div class="form-group">
						<div class="row">
							<div class="col-lg-1">
								<form:hidden path="awbDetail[${_cnt }].sequence" value="${elm.quantity}"/>
								<form:input path="awbDetail[${_cnt }].quantity" readonly="true" value="${elm.quantity}" cssClass="form-control textfield text-uppercase"/>
							</div>
							<div class="col-lg-1">
								<form:input path="awbDetail[${_cnt }].weightGross" readonly="true" value="${elm.weightGross}" cssClass="form-control textfield text-right"/>
							</div>
							<div class="col-lg-1">
								<form:input path="awbDetail[${_cnt }].weightIndicator" readonly="true" value="${elm.weightIndicator}" cssClass="form-control textfield text-uppercase"/>
							</div>
							<div class="col-lg-1">
								<form:input path="awbDetail[${_cnt }].rateClass" readonly="true" value="${elm.rateClass}" class="form-control textfield text-uppercase"/>
							</div>
							<div class="col-lg-1 text-right">
								<form:input path="awbDetail[${_cnt }].weightCharge" readonly="true" value="${elm.weightCharge}" class="form-control textfield text-right"/>
							</div>
							<div class="col-lg-2 text-right">
								<form:input path="awbDetail[${_cnt }].unit" value="${elm.unit}" readonly="true" title="${titleTarif}: ${elm.unitCass}" class="form-control textfield text-right"/>
							</div>
							<div class="col-lg-2 text-right">
								<form:input path="awbDetail[${_cnt }].amount" value="${elm.amount}" class="form-control textfield text-right"/>
							</div>
							<div class="col-lg-3">
								<form:hidden path="awbDetail[${_cnt }].description" readonly="true" value="${elm.description}" class="form-control textfield"/>
							</div>
						</div>
					</div>
					 <c:set value="${_cnt+1 }" var="_cnt" />
				</c:forEach>
	
				<div class="row">
					<div class="col-lg-12">
						<div class="hr-line-dashed"></div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-8">
						<div class="row">
							<div class="form-group">
								<div class="col-lg-2">
									<label class="control-label"><spring:message code="awb.tax" text="!"/></label>
								</div>
								<div class="col-lg-4 text-right">
									<label><spring:message code="awb.tax.value" text="!"/></label>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
								<form:errors path="errorTax" cssClass="text-danger"/>
							</div>
						</div>
						
						<c:forEach items="${awb.awbTax}" var="elm">
							<div class="form-group">
								<div class="row">
									<div class="col-lg-2">
										<input readonly="readonly" value="${elm.id.taxCode}" class="form-control textfield text-uppercase"/>
									</div>
									<div class="col-lg-4">
										<input readonly="readonly" value="${elm.taxAmount}" class="form-control textfield text-right"/>
									</div>
								</div>
							</div>
						</c:forEach>
						
						<div class="form-group">
							<div class="row">
								<div class="col-lg-12">
									<label><spring:message code="awb.note" text="!Handling information"/></label>
									<form:textarea path="descriptUser" rows="3" class="form-control textfield"/>
								</div>
							</div>
						</div>
				
					</div>
					
					<div class="col-lg-4">
						<div class="row">
							<div class="form-group">
								<div class="col-lg-4">
									<label class="control-label"><spring:message code="awb.tax" text="!"/></label>
								</div>
								<div class="col-lg-8 text-right">
									<label><spring:message code="awb.tax.value" text="!"/></label>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12">
								<form:errors path="errorTax" cssClass="text-danger"/>
							</div>
						</div>
						
						<c:set value="0" var="_cnt" />
						<c:forEach items="${setTax}" var="elm">
							<div class="form-group">
								<div class="row">
									<div class="col-lg-4">
										<form:input path="awbTax[${_cnt }].code" maxlength="3" value="${elm.id.taxCode}" cssClass="form-control textfield text-uppercase"/>
									</div>
									<div class="col-lg-8">
										<form:input path="awbTax[${_cnt }].amount" value="${elm.taxAmount}" cssClass="form-control textfield text-right"/>
									</div>
								</div>
							</div>
							 <c:set value="${_cnt+1 }" var="_cnt" />
						</c:forEach>
					</div>
				</div>
			 </form:form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
				<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
			</button>
			<button id ="add" type="button" class="btn btn-w-m btn-success text-uppercase">
				<i class="fa fa-plus-square "></i> <spring:message code="button.flown" text="!Create"/>
			</button>
		</div>
	</div>
</div>
		
