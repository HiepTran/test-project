<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>

<style type="text/css">
	.sweet-alert {
	    background-color: white;
	    border-radius: 5px;
	    display: none;
	    font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
	    left: 50%;
	    margin-left: -256px;
	    margin-top: -200px;
	    overflow: hidden;
	    padding: 17px;
	    position: fixed;
	    text-align: center;
	    top: 50%;
	    width: 600px;
	    z-index: 99999;
	}
</style>

<script type="text/javascript">
	document.getElementById("report").className = "active";

	$(document).ready(function() {
		window.onload = actLoadDiv;
        function actLoadDiv() {
            window.location.hash = 'LST';
            window.setInterval(refresh, 20000);
        }

        function refresh() {
            var test = document.getElementById('wait');
            if (test == null) {
                return;
            }
            else {
                window.location.reload();
            }
        }

        $('.table-result').dataTable({
 	 		"dom": 'T<"clear">lfrtip',
 	        "sDom": "<'row'<'col-sm-6 text-left'><'col-sm-6 text-right'>r>" + "t" + "<'row'<'col-sm-6'><'col-sm-6'p>>",
 			"bSort": false,
 			"paginate": false
 	   	}); 
 	   	
	});
	
	function ConformDelete(id, fileName) {
        swal({
            title: "<spring:message code="message.delete" text="!"/>" + " [" + fileName + "] ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<spring:message code="button.delete" text="!"/>",
            cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
            closeOnConfirm: false
        }, function () {
       	 	document.forms[0].elements['action'].value='DELETE';
			 document.forms[0].elements['id'].value=id;
			 document.forms[0].submit();
        });
    };
    
</script>

<div id="body">
<form:form id="form" name="form" method="post" commandName="reportFilter">
<input name="action" type="hidden">
<input name="id" type="hidden">
	
	<div class="tabs-container">
        <ul class="nav nav-tabs">
            <li><a href="<c:url value="/secure/report"/>"><spring:message code="report.rapport" text="!"/></a></li>
            <li class="active" style="background:none;border:none;"><a><spring:message code="report.result" text="!"/></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active">
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover table-result">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center"><spring:message code="report.action" text="!"/></th>
                                <th><spring:message code="report.name" text="!"/></th>
                                <th><spring:message code="report.file.name" text="!"/></th>
                                 <th><spring:message code="report.filter" text="!"/></th>
                                <th><spring:message code="report.create" text="!"/></th>
                                <th><spring:message code="report.date" text="!"/></th>
                                <th class="text-center"><spring:message code="report.status" text="!"/></th>
                            </tr>
                        </thead>
                        <tbody>
                        	<c:forEach items="${reports}" var="elm" varStatus="stt">
	                        	<c:url value="/secure/reportResult/excel" var="linkExcel">
									<c:param name="fileName" value="${elm.fileName}"/>
								</c:url>
								<c:set var="arrayFilter" value="${fn:split(elm.filter,';')}"/>
							
	                            <tr>
	                                <td class="text-center">${stt.index + 1}</td>
	                                <td class="text-center text-nowrap">
	                                <c:if test="${elm.type eq 'SALE' }">
		                                <sec:authorize access="hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_REPORTS', 'ROLE_REPORT_LTA')">
			                                <c:choose>
												<c:when test="${elm.status == 'F' }">
													<a href="${linkExcel}" target="_blank" title="<spring:message code="button.excel" text="!Excel"/>"><i class="fa fa-2x fa-file-excel-o"></i></a>
												</c:when>
												<c:otherwise>
													<i class="fa fa-2x fa-file-excel-o"></i>
												</c:otherwise>
											</c:choose>
			                               <a onclick="ConformDelete('${elm.id}', '${elm.fileName}')" title="<spring:message code="button.delete" text="!Delete"/>"><i class="fa fa-2x fa-trash-o"></i></a>
		                                </sec:authorize>
	                                </c:if>
	                                
	                                <c:if test="${elm.type eq 'FLOWN' }">
		                                <sec:authorize access="hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_REPORTS', 'ROLE_REPORT_MANIFEST')">
			                                <c:choose>
												<c:when test="${elm.status == 'F' }">
													<a href="${linkExcel}" target="_blank" title="<spring:message code="button.excel" text="!Excel"/>"><i class="fa fa-2x fa-file-excel-o"></i></a>
												</c:when>
												<c:otherwise>
													<i class="fa fa-2x fa-file-excel-o"></i>
												</c:otherwise>
											</c:choose>
			                               <a onclick="ConformDelete('${elm.id}', '${elm.fileName}')" title="<spring:message code="button.delete" text="!Delete"/>"><i class="fa fa-2x fa-trash-o"></i></a>
		                                </sec:authorize>
	                                </c:if>
		                                
	                                </td>
	                                <td><spring:message code="report.${elm.name}" text="!"/></td>
	                                <td>${elm.fileName}</td>
	                                <td>
	                                	<c:forEach var="i" begin="0" end="${fn:length(arrayFilter)}">
										 	${arrayFilter[i]}<br>
										</c:forEach>
	                                </td>
	                                <td>${elm.createdBy}</td>
	                                <td><fmt:formatDate pattern="dd/MM/yyyy HH:mm" value="${elm.createdDate}"/></td>
	                                <td class="text-center">
										<c:choose>
											<c:when test="${elm.status == 'F' }">
												<span class="label label-success text-uppercase">
													<spring:message code="message.ok" text="!OK"/>
												</span>
											</c:when>
											
											<c:when test="${elm.status == 'W' }">
												<div class="sk-spinner sk-spinner-wave" id="wait">
			                                         <div class="sk-rect1"></div>
			                                         <div class="sk-rect2"></div>
			                                         <div class="sk-rect3"></div>
			                                         <div class="sk-rect4"></div>
			                                         <div class="sk-rect5"></div>
			                                     </div>
											</c:when>
											
											<c:when test="${elm.status == 'E' }">	
												<span class="label label-danger text-uppercase">
													<spring:message code="message.ko" text="!Error"/>
												</span>
											</c:when>
										</c:choose>	
									</td>
	                            </tr>
                            </c:forEach>
                        </tbody>
                    </table>    
                </div>
            </div>
        </div>
	</div>
</form:form>
</div>