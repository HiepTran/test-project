<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript">
	function doEdit(action, id){
		document.forms[1].elements['action'].value = action;
		document.forms[1].elements['id'].value = id;
		$.ajax({
			url: "<spring:url value='/secure/role/modify/error.json'/>",
			type: "POST",
			cache: false,
			data: $('#form_edit').serialize(),
			success: function(result){
				if(result.errCodeModify > 0){
		           	 $('#err_roleNameEdit').html('');
		           	 $('err_descriptionEdit').html('');
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++)
	              	 {
	                   $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	             	 }
       			}else{
					$('#form_edit').submit();
       			}
			}
		});
	}
</script>

<form:form name="form_edit" id="form_edit" method="post" modelAttribute="rolefilter" action="role/modify/${roleId }">
	<input name="action" type="hidden">
	<input name="id" type="hidden">
	
	<div class="panel panel-success" style="margin-top:9px;">
		<div class="panel-heading" style="padding: 4px;">
			<div class="row">
                 <div class="col-lg-11">
                     <h3><spring:message code="role.table.modify" text="!Edit"/></h3>
                 </div>
                 <div class="col-lg-1 text-right">
                     <i class="fa fa-2x fa-times-circle" onclick="javascript:hide('edit-box');" style="cursor:pointer;"></i>
                 </div>
             </div>
		</div>
		
		<div class="panel-body" style="padding:5px;">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-6">
						<label><spring:message code="role.name" text="!Role name"></spring:message></label>
						<form:input path="roleNameEdit" value="${fn:trim(role.authority)}" cssClass="form-control textfield"/>
						<label id="err_roleNameEdit" class="text-danger"></label>
					</div>
					<div class="col-sm-6">
						<label><spring:message code="role.description" text="!Detail"></spring:message></label>
						<form:input path="descriptionEdit" value="${fn:trim(role.description)}" cssClass="form-control textfield"/>
						<label id="err_descriptionEdit" class="text-danger"></label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-12 text-right">
						<button id="edit" type="button" onClick="doEdit('MODIFY', ${role.sysRoleId})" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-check"></i> <spring:message code="button.go" text="!Go"></spring:message></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</form:form>