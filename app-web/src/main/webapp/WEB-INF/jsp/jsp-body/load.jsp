<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<style type="text/css">
	.chosen-container-single .chosen-single {
         background: none;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
</style>

<script type="text/javascript">
	document.getElementById("lta").className = "active";
	document.getElementById("loadAwb").className = "active";
	
	var pageNo = ${fileFilter.page};
	
	$(function(){
	    $.ajax({
	    	url : "<spring:url value='/secure/awb/load/loadpage'/>",
	        data: { pageNo: pageNo },
	        cache : false,
	        success: function (response) {
	            $("#_results").html(response);
	        }
	    });
	});
	
	function doSubmitChanger(action){
		var file_data = $('#file').prop('files')[0];
		
		if(file_data != null){
			document.forms.namedItem('form').elements['fileName'].value=file_data.name;
			document.forms.namedItem('form').elements['fileSize'].value=file_data.size;
		}
		$.ajax({
			url: "<spring:url value='/secure/awb/load/error.json'/>",
			type: "POST",
			data: $('#form').serialize(),
	        success: function(result){
	        	if(result.errCode > 0){
		           	 $('#err_agtnGsa').html('');
		           	 $('#err_file').html('');
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++)
	               	 {
	                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
	        	}else{
	        		document.forms.namedItem('form').elements['action'].value=action;
	        		$('#form').submit();
	        	}
	        },
		 	error: function(XMLHttpRequest, textStatus, errorThrown){}
		});
	}
</script>
	
<div id="body">
	<form:form name="form" id="form" method="post" commandName="fileFilter" enctype="multipart/form-data">
		<input type="hidden" name="action">
		<input type="hidden" name="fileName"> 
		<input type="hidden" name="fileSize">
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-12">
					<label class="text-success">${msgSucess}</label>
					<label class="text-danger">${msgError}</label>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="log.isoc" text="!"/></label>
					<form:select path="isoc" class="chosen-select" cssStyle="width:100%;">
				 		<c:forEach items="${critIsoc}" var="elm">
							<option value="${elm.isoc}" <c:if test="${fileFilter.isoc == elm.isoc}" >selected="selected"</c:if>>
								<spring:message code="isoc.${elm.isoc}" text="notFound"/>
							</option>
						</c:forEach>
					</form:select>
				</div>
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="log.agtn.gsa" text="!"/></label>
					<form:input path="agtnGsa" class="form-control textfield"/>
					<label id="err_agtnGsa" class="text-danger"></label>
				</div>
				<div class="col-lg-3">
					<label>&nbsp;</label>
					<form:input type="file" path="file"/>
					<label id="err_file" class="text-danger"></label>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-12 text-right">
					<a onclick="doSubmitChanger('LOAD')" class="btn btn-w-m btn-success text-uppercase">
						<spring:message code="button.load" text="!"/>
					</a>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<div id="_results"></div>
			</div>
		</div>
	</form:form>
</div>