<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>

<style type="text/css">
	.chosen-container-single .chosen-single {
         background: none;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
     .chosen-container {
        width: 100% !important;
    }
</style>

<script type="text/javascript">
	document.getElementById("stock").className = "active";
	document.getElementById("stock_compagnie").className = "active";
	document.getElementById("stock_compagnie_agent").className = "active";
	
	$(document).ready(function() {
	    $('#move').click(function(){
	    	$('#form-move').submit();
	    });
	    
	    if(${errorMove == true}){
	    	modalMove(${id});
			$('#formMove').modal("show");
		}
	    $('.chosen-select').chosen({
	    	"disable_search": true
		    }
		);
	});
	function doShowAgent(){
		var agent = $('#typeMove').val();
		if(agent=='AGENT'){
			$('#agent_move').css('display','block');
		}else{
			$('#agent_move').css('display','none');
		}
	}
	
	function ConformDelete(id, agtn, serialFrom ,serialTo) {
        swal({
            title: "<spring:message code="message.delete" text="!"/>" + " ["+ agtn + ": " + serialFrom +"-" + serialTo +  "] ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<spring:message code="button.delete" text="!"/>",
            cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
            closeOnConfirm: false
        }, function () {
       	 	document.forms[0].elements['action'].value='CANCEL';
			 document.forms[0].elements['id'].value=id;
			 document.forms[0].submit();
        });
    };

    function modalMove(id){
		$.post('<c:url value="/secure/stock/compagnie/agent/model?id="/>' + id).done(function( data ) {
			$("#agtnAgent").val(data.agtn);
			$("#fromAgent").val(data.serialFrom);
			$("#toAgent").val(data.serialTo);
			$("#quantityAgent").val(data.quantity);
			$("#idAgent").val(data.idStockAgent);
		});
	}
	
</script>

<div id="body">
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 style="margin-top: 0px;"><spring:message code="menu.stock.compagnie" text="!"/></h2>
            <ol class="breadcrumb">
                <li>
                    <a><spring:message code="menu.stock" text="!"/></a>
                </li>
                <li>
                    <a><spring:message code="menu.stock.compagnie" text="!"/></a>
                </li>
                <li class="active">
                    <strong><spring:message code="menu.stock.agent" text="!"/></strong>
                </li>
            </ol>
        </div>
    </div>
    &nbsp;
    
	<form:form id="form" name="form" method="post" commandName="stockFilter">
			<input name="action" type="hidden">
			<input name="id" type="hidden">
		
		<div class="row">
		    <div class="col-lg-6">
		        <label class="label label-success" style="font-size:14px">${fn:length(stockAgent)} <spring:message code="message.result" text="!Result(s)"/></label>
		    </div> 
		    <div class="col-lg-6 text-right">
		    	<a href="<c:url value="/secure/stock/compagnie/agent/excel"></c:url>" target="_blank" class="btn btn-w-m btn-success text-uppercase">
		    		<i class="fa fa-file-excel-o"></i> <spring:message code="button.excel" text="!"/>
		    	</a>
		    </div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">
		    <div class="col-lg-12">
				<table class="table table-striped table-bordered table-hover table-lta" style="width:100%;">
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th class="text-center"><spring:message code="commom.action" text="!"/></th>
							<th><spring:message code="stock.compagnie.agent" text="!"/></th>
							<th><spring:message code="stock.compagnie.from" text="!"/></th>
							<th><spring:message code="stock.compagnie.to" text="!"/></th>
							<th><spring:message code="stock.compagnie.quantity" text="!"/></th>
							<th><spring:message code="stock.compagnie.date" text="!"/></th>
							<th><spring:message code="stock.compagnie.replenish" text="!"/></th>
							<th><spring:message code="stock.compagnie.agent.orig" text="!"/></th>
							<th><spring:message code="stock.compagnie.comment" text="!"/></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${stockAgent}" var="elm" varStatus="stt">
							<fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateUsed}" var="dateUsed"/>
						
							<tr id="${elm.idStockAgent}">
								<td class="text-center">${stt.index + 1}</td>
								<td class="text-center text-nowrap">
								 	<a onclick="modalMove('${elm.idStockAgent}')" data-toggle="modal" data-target="#formMove" title="<spring:message code="stock.compagnie.agent.replenish" text="!"/>"><i class="fa fa-2x fa-stack-overflow "></i></a>
								 	<a onclick="ConformDelete('${elm.idStockAgent}', '${elm.agtn}', '${elm.serialFrom}', '${elm.serialTo}')" title="<spring:message code="button.delete" text="!Delete"/>"><i class="fa fa-2x fa-trash-o"></i></a>
								</td>
								<td>${elm.agtn}</td>
								<td>${elm.serialFrom}</td>
								<td>${elm.serialTo}</td>
								<td>${elm.quantity}</td>
								<td><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateEntry}"/></td>
								<td>
									<c:if test="${dateUsed != '01/01/0001'}">
										<fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateUsed}"/>
									</c:if>
								</td>
								<td>${elm.agentOrig}</td>
								<td>${elm.comment}</td>
							</tr>
						</c:forEach>	
					</tbody>
				</table>
			</div>
		</div>
	</form:form>
	
	<!-- Modal move agent -->
	<div class="modal inmodal" id="formMove" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content animated bounceInRight" style="width: 850px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
					<h5 class="modal-title"><spring:message code="stock.compagnie.move" text="!"/></h5>
				</div>
				<div class="modal-body">
				<form:form class="form-horizontal" id="form-move" name="form-move" method="post" commandName="stockFilter">
					<input type="hidden" name="action" value="MOVE">
					<input type="hidden" name="idAgent" id="idAgent">
					
						<div class="form-group">
							<div class="row">
							 	<div class="col-lg-3">
								 	<label><spring:message code="stock.compagnie.agent" text="!"/></label>
								 	<input id="agtnAgent" readonly="readonly" type="text" class="form-control textfield"/>
							 	</div>
								<div class="col-lg-3">
								 	<label><spring:message code="stock.compagnie.from" text="!"/></label>
								 	<input id="fromAgent" readonly="readonly" type="text" class="form-control textfield"/>
								 </div>
								 <div class="col-lg-3">
								 	<label><spring:message code="stock.compagnie.to" text="!"/></label>
								 	<input id="toAgent" readonly="readonly" type="text" class="form-control textfield"/>
								 </div>
								 <div class="col-lg-3">
								 	<label><spring:message code="stock.compagnie.quantity" text="!"/></label>
								 	<input id="quantityAgent" readonly="readonly" type="text" class="form-control textfield"/>
								 </div>
							 </div>
						 </div>
						 
						 <div class="form-group">
							<div class="row">
								<div class="col-lg-3">
								 	<label><spring:message code="stock.compagnie.agent.type.move" text="!"/></label>
								 	<form:select path="typeMove" onchange="doShowAgent()" class="chosen-select" cssStyle="width:100%">
								 		<option value="AGENT" <c:if test="${'AGENT' == stockFilter.typeMove}">selected="selected"</c:if>><spring:message code="stock.compagnie.agent" text="!"/></option>
										<option value="CENTRAL" <c:if test="${'CENTRAL' == stockFilter.typeMove}">selected="selected"</c:if>><spring:message code="stock.compagnie.central" text="!"/></option>
									</form:select>
							 	</div>
							 	<div id="agent_move">
								 	<div class="col-lg-3" >
									 	<label><spring:message code="stock.compagnie.agent" text="!"/></label>
									 	<form:input path="agtn" maxlength="11" class="form-control textfield"/>
									 	<form:errors path="agtn" cssClass="text-danger"/>
									 </div>
									  <div class="col-lg-3">
									 	<label><spring:message code="stock.compagnie.from" text="!"/></label>
									 	<form:input path="serialFrom" class="form-control textfield"/>
									 	<form:errors path="serialFrom" cssClass="text-danger"/>
									 </div>
									  <div class="col-lg-3">
									 	<label><spring:message code="stock.compagnie.quantity" text="!"/></label>
									 	<form:input path="quantity" maxlength="11" class="form-control textfield"/>
									 	<form:errors path="quantity" cssClass="text-danger"/>
									 </div>
							 	</div>
							 </div>
						 </div>
						 
						 <div class="form-group">
							<div class="row">
							 	<div class="col-lg-12">
								 	<label><spring:message code="stock.compagnie.comment" text="!"/></label>
								 	<form:input path="comment" class="form-control textfield"/>
								 </div>
							 </div>
						 </div>
						 
					</form:form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!"/>
					</button>
					<button id ="move" type="button" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-check"></i> <spring:message code="button.go" text="!"/>
					</button>
				</div>
			</div>
		</div>
	</div>
	
</div>