<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>


<script type="text/javascript">
	document.getElementById("stock").className = "active";
	document.getElementById("stock_compagnie").className = "active";
	document.getElementById("stock_compagnie_control").className = "active";
	

	$(document).ready(function() {
	    $('#add').click(function(){
	    	$('#form-add').submit();
	    });

	    $('#move').click(function(){
	    	$('#form-move').submit();
	    });
	    
	    if(${errorAdd == true}){
			$('#formStock').modal("show");
		}

	    if(${errorMove == true}){
	    	modalMove(${id});
			$('#formMove').modal("show");
		}
	});
	
	function ConformDelete(id, serialFrom ,serialTo) {
        swal({
            title: "<spring:message code="message.delete" text="!"/>" + " [" + serialFrom +"-" + serialTo +  "] ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<spring:message code="button.delete" text="!"/>",
            cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
            closeOnConfirm: false
        }, function () {
       	 	document.forms[0].elements['action'].value='DELETE';
			 document.forms[0].elements['id'].value=id;
			 document.forms[0].submit();
        });
    };

    function modalMove(id){
		$.post('<c:url value="/secure/stock/compagnie/model?id="/>' + id).done(function( data ) {
			$("#fromCentral").val(data.serialFrom);
			$("#toCentral").val(data.serialTo);
			$("#quantityCentral").val(data.quantity);
			$("#idCentral").val(data.idStockCentral);
		});
	}
	
</script>

<div id="body">
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2 style="margin-top: 0px;"><spring:message code="menu.stock.compagnie" text="!"/></h2>
            <ol class="breadcrumb">
                <li>
                    <a><spring:message code="menu.stock" text="!"/></a>
                </li>
                <li>
                    <a><spring:message code="menu.stock.compagnie" text="!"/></a>
                </li>
                <li class="active">
                    <strong><spring:message code="menu.stock.replenishment" text="!"/></strong>
                </li>
            </ol>
        </div>
    </div>
    &nbsp; 
    
	<form:form id="form" name="form" method="post" commandName="stockFilter">
			<input name="action" type="hidden">
			<input name="id" type="hidden">
		
		<div class="row">
		    <div class="col-lg-6">
		        <label class="label label-success" style="font-size:14px">${fn:length(stockCentrals)} <spring:message code="message.result" text="!Result(s)"/></label>
		    </div>
		    <div class="col-lg-6 text-right">
			    <button type="button" class="btn btn-w-m btn-success text-uppercase" data-toggle="modal" data-target="#formStock">
				 	<i class="fa fa-plus-square"></i> <spring:message code="button.create" text="!Create"/>
				 </button>
				 
				 <a href='<c:url value="/secure/stock/compagnie/excel"></c:url>' target="_blank" class="btn btn-w-m btn-success text-uppercase">
				 	<i class="fa fa-file-excel-o"></i> <spring:message code="button.excel" text="!"/>
				 </a>
			</div> 
		</div>
		<div class="row">&nbsp;</div>
		<div class="row">
		    <div class="col-lg-12">
				<table class="table table-striped table-bordered table-hover table-lta" style="width:100%;">
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th class="text-center"><spring:message code="commom.action" text="!"/></th>
							<th><spring:message code="stock.compagnie.from" text="!"/></th>
							<th><spring:message code="stock.compagnie.to" text="!"/></th>
							<th><spring:message code="stock.compagnie.quantity" text="!"/></th>
							<th><spring:message code="stock.compagnie.date" text="!"/></th>
							<th><spring:message code="stock.compagnie.replenish" text="!"/></th>
							<th><spring:message code="stock.compagnie.comment" text="!"/></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${stockCentrals}" var="elm" varStatus="stt">
							<fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateReplenish}" var="dateReplenish"/>
						
							<tr id="${elm.idStockCentral}">
								<td class="text-center">${stt.index + 1}</td>
								<td class="text-center text-nowrap">
								 	<a onclick="modalMove('${elm.idStockCentral}')" data-toggle="modal" data-target="#formMove" title="<spring:message code="stock.compagnie.agent.replenish" text="!Delete"/>" >
									 		<i class="fa fa-2x fa-expand"></i>
	              				 	</a>		
								</td>
								<td>${elm.serialFrom}</td>
								<td>${elm.serialTo}</td>
								<td>${elm.quantity}</td>
								<td><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateEntry}"/></td>
								<td>
									<c:if test="${dateReplenish != '01/01/0001'}">
										<fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateReplenish}"/>
									</c:if>
								</td>
								<td>${elm.comment}</td>
							</tr>
						</c:forEach>	
					</tbody>
				</table>
			</div>
		</div>
	</form:form>
	
	<!-- Modal create stock -->
	<div class="modal inmodal" id="formStock" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content animated bounceInRight" style="width: 650px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
					<h5 class="modal-title"><spring:message code="stock.compagnie.title" text="!"/></h5>
				</div>
				<div class="modal-body">
				<form:form class="form-horizontal" id="form-add" name="form-add" method="post" commandName="stockFilter">
					<input type="hidden" name="action" value="ADD">
					
						<div class="form-group">
							<div class="row">
							 	<div class="col-lg-6">
								 	<label><spring:message code="stock.compagnie.from" text="!"/></label>
								 	<form:input path="serialFrom" maxlength="7" class="form-control textfield"/>
								 	<form:errors path="serialFrom" cssClass="text-danger"/>
								 </div>
								 <div class="col-lg-6">
								 	<label><spring:message code="stock.compagnie.quantity" text="!"/></label>
								 	<form:input path="quantity" class="form-control textfield"/>
								 	<form:errors path="quantity" cssClass="text-danger"/>
								 </div>
							 </div>
						 </div>
						 
						 <div class="form-group">
							<div class="row">
							 	<div class="col-lg-12">
								 	<label><spring:message code="stock.compagnie.comment" text="!"/></label>
								 	<form:input path="comment" class="form-control textfield"/>
								 </div>
							 </div>
						 </div>
						 
					</form:form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<button id ="add" type="button" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-plus-square "></i> <spring:message code="button.create" text="!Create"/>
					</button>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- Modal move stock -->
	<div class="modal inmodal" id="formMove" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content animated bounceInRight" style="width: 750px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
					<h5 class="modal-title"><spring:message code="stock.compagnie.move" text="!"/></h5>
				</div>
				<div class="modal-body">
				<form:form class="form-horizontal" id="form-move" name="form-move" method="post" commandName="stockFilter">
					<input type="hidden" name="action" value="MOVE">
					<input type="hidden" name="id" id="idCentral">
					
						<div class="form-group">
							<div class="row">
							 	<div class="col-lg-4">
								 	<label><spring:message code="stock.compagnie.from" text="!"/></label>
								 	<input id="fromCentral" readonly="readonly" type="text" class="form-control textfield"/>
							 	</div>
								<div class="col-lg-4">
								 	<label><spring:message code="stock.compagnie.to" text="!"/></label>
								 	<input id="toCentral" readonly="readonly" type="text" class="form-control textfield"/>
								 </div>
								 <div class="col-lg-4">
								 	<label><spring:message code="stock.compagnie.quantity" text="!"/></label>
								 	<input id="quantityCentral" readonly="readonly" type="text" class="form-control textfield"/>
								 </div>
							 </div>
						 </div>
						 
						 <div class="form-group">
							<div class="row">
								<div class="col-lg-4">
								 	<label><spring:message code="stock.compagnie.agent" text="!"/></label>
								 	<form:input path="agtn" maxlength="11" class="form-control textfield"/>
								 	<form:errors path="agtn" cssClass="text-danger"/>
							 	</div>
								 <div class="col-lg-4">
								 	<label><spring:message code="stock.compagnie.quantity" text="!"/></label>
								 	<form:input path="quantity" class="form-control textfield"/>
								 	<form:errors path="quantity" cssClass="text-danger"/>
								 </div>
							 </div>
						 </div>
						 
						 <div class="form-group">
							<div class="row">
							 	<div class="col-lg-12">
								 	<label><spring:message code="stock.compagnie.comment" text="!"/></label>
								 	<form:input path="comment" class="form-control textfield"/>
								 </div>
							 </div>
						 </div>
						 
					</form:form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<button id ="move" type="button" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-plus-square "></i> <spring:message code="button.create" text="!Create"/>
					</button>
				</div>
			</div>
		</div>
	</div>
	
</div>