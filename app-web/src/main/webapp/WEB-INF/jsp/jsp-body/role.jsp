<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>

<script type="text/javascript">
document.getElementById("userrole").className = "active";
document.getElementById("userrole_role").className = "active";

function showFormEdit(divId, roleID){
	$.ajax({
		url: "<spring:url value='/secure/role/showedit/" + roleID +"'/>", type: "GET",
		cache: false,
		success: function(reponse){
			$("div#edit-box").html(reponse);
		}
	});
	show(divId);
}

function ConformDelete(id, roleName) {
    swal({
        title: "<spring:message code="message.delete" text="!"/>" + " [" + roleName + "] ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "<spring:message code="button.delete" text="!"/>",
        cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
        closeOnConfirm: false
      },function () {
   	 	 document.forms[0].elements['action'].value='DELETE';
   	 	 document.forms[0].elements['roleName'].value=roleName;
		 doDelete(id);
    });
};

function doDelete(id){
	$.ajax({
		url: "<spring:url value='/secure/role/delete/" + id +"'/>",
		type: "POST",
		cache: false,
		data: $('#form').serialize(),
		success: function(reponse){
			window.location.reload();
		}
	});
}

function doCreateForm(action){
	$.ajax({
		url: "<spring:url value='/secure/role/create/error.json'/>",
		type: "POST",
		cache: false,
		data: $('#form-add').serialize(),
		success: function(result){
			if(result.errCodeCreate > 0){
	           	 $('#err_roleNameAdd').html('');
	           	 $('#err_descriptionAdd').html('');
	           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
	           	 for(var i=0;i<len;i++)
              	 {
                   $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
             	 }
       			}else{
       				document.forms.namedItem('form-add').elements['action'].value='CREATE';
					$('#form-add').submit();
       			}
		}
	});
	
}

</script>

<div id="body">
	<form:form id="form" method="post">
		<input name="action" type="hidden">
		<input name="id" type="hidden">
		<input name="roleName" type="hidden">
		
		<div id="edit-box" class="boxcss" style="display: block;">
			<div id="edit-result"></div>
		</div>
	
		<div class="row">
			<div class="col-sm-4">
				<button id="btnCreate" type="button" class="btn btn-w-m btn-success text-uppercase" data-toggle="modal" data-target="#formCreateRole">
					<i class="fa fa-plus-square"> <spring:message code="button.create" text="!Create Role"></spring:message></i>
				</button>
			</div>
			<div class="col-sm-8 text-right">
				<a href="<c:url value="/secure/role/excel"></c:url>" target="_blank" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-file-excel-o"></i> <spring:message code="button.excel" text="!"/>
				</a>
			</div>
		</div>
		&nbsp;
		<table class="table table-striped table-bordered table-hover table-result" style="width:100%;">
			<thead>
				<tr>
					<th class="text-center">#</th>
					<sec:authorize access="hasRole('ROLE_DW')">
						<th class="text-center"><spring:message code="commom.action" text="!Action"/></th>
					</sec:authorize>
					<th><spring:message code="role.table.authority" text="!Authority"/></th>
					<th><spring:message code="role.table.description" text="!Description"/></th>
					<th><spring:message code="role.table.create" text="!create"/></th>
					<th><spring:message code="role.table.modify" text="!"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lstRole}" var="elm" varStatus="stt">
				
					<tr id="${elm.sysRoleId }">
						<td class="text-center">${stt.index + 1 }</td>
						<sec:authorize access="hasRole('ROLE_DW')">
						<td class="text-center text-nowrap">
							<sec:authorize access="hasRole('ROLE_DW')">
							<a id='a-edit-${elm.sysRoleId }' onclick='bfShow("edit-box", "a-edit-${elm.sysRoleId }", "${elm.sysRoleId }"); showFormEdit("edit-box", "${elm.sysRoleId }")'
							title="<spring:message code="role.table.modify" text="!Edit"/>"><i class="fa fa-2x fa-edit"></i>
							</a> 
							</sec:authorize>
							<sec:authorize access="hasRole('ROLE_DW')">
							<a onClick="ConformDelete('${elm.sysRoleId}', '${elm.authority }')"
							title="<spring:message code="role.table.delete" text="!Delete role"/>"><i class="fa fa-2x fa-trash-o"></i>
							</a>
							</sec:authorize>
						</td>
						</sec:authorize>
						<td>${elm.authority }</td>
						<td>${elm.description }</td>
						<td>${elm.createdBy } - <fmt:formatDate pattern="dd/MM/yyyy" value="${elm.createdOn }"/></td>
						<td>${elm.modified }
							<c:if test="${not empty elm.modified }"> 
								- <fmt:formatDate pattern="dd/MM/yyyy" value="${elm.modifiedOn }"/>
							</c:if>
							</td>
						
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</form:form>

	<div class="modal inmodal" id="formCreateRole" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" style="width:800px">
			<div class="modal-content animated bounceInRight">
				<div class="modal-body">
					<form:form id="form-add" name="form-add" method="post" commandName="rolefilter" action="role/create">
						<input type="hidden" name="action">	
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label><spring:message code="role.name" text="!Role name"/></label>
									<form:input path="roleNameAdd" type="text" cssClass="form-control textfield text-left"/>
									<label id="err_roleNameAdd" class="text-danger"></label>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label><spring:message code="role.description" text="!Detail"/></label>
									<form:input path="descriptionAdd" type="text" cssClass="form-control textfield text-left"/>
									<label id="err_descriptionAdd" class="text-danger"></label>
								</div>
							</div>
						</div>
					</form:form>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<button id ="add" type="button" onclick="doCreateForm('CREATE');" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-plus-square "></i> <spring:message code="button.create" text="!Create"/>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>