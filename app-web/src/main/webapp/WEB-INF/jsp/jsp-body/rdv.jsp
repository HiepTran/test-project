<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>


<style type="text/css">
	.datepicker { 
		z-index: 9999 !important;
	}
	.chosen-container {
        width: 100% !important;
    }
</style>

<script type="text/javascript">
	document.getElementById("lta").className = "active";
	document.getElementById("create").className = "active";

	function doValidatorRdv(){
		$.ajax({
			url: "<spring:url value='/secure/rdv/error.json'/>",
	        data: $('#form-add').serialize(), 
	        type: "POST",
	        success: function(result){
	        	if(result.errCodeRdv > 0){
		           	 $('#err_agtn').html('');
		           	 $('#err_emission').html('');
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++)
	               	 {
	                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
	        	}else{
	        		$('#form-add').submit();
	        	}
	        },
		 	error: function(XMLHttpRequest, textStatus, errorThrown){}
		});
	}

	function doValidatorAgtn(){
		$.ajax({
			url: "<spring:url value='/secure/agent/error.json'/>",
	        data: $('#form-agent').serialize(), 
	        type: "POST",
	        success: function(result){
	        	if(result.errCodeAgent > 0){
		           	 $('#err_agtnCode').html('');
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++)
	               	 {
	                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
	        	}else{
	        		$('#form-agent').submit();
	        	}
	        },
		 	error: function(XMLHttpRequest, textStatus, errorThrown){}
		});
	};
	
	var pageNo = ${rdvFilter.page};
	$(function () {
		if(${error == true}){
			$('#formRdv').modal("show");
		}
		
	    $.ajax({
	    	url : "<spring:url value='/secure/rdv/loadPage'/>",
	        data: { pageNo: pageNo },
	        cache : false,
	        success: function (response) {
	            $("#_results").html(response);
	        }
	    });

	    $('.input-group.date').datepicker({
			todayBtn: "linked",
	        keyboardNavigation: false,
	        forceParse: false,
	        calendarWeeks: true,
	        autoclose: true,
			format: "dd/mm/yyyy"
	    });

	    $(".chosen-select").chosen({
	    	  "disable_search": true
    	});
    	
	});

	function ConformDelete(id, rdv) {
        swal({
            title: "<spring:message code="message.delete" text="!"/>" + " [" + rdv + "] ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<spring:message code="button.delete" text="!"/>",
            cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
            closeOnConfirm: false
        }, function () {
       	 	document.forms[0].elements['action'].value='DELETE';
			 document.forms[0].elements['id'].value=id;
			 document.forms[0].submit();
        });
    };
    
</script>
<div id="body">
	<form:form name="form" id="form" method="post" commandName="rdvFilter" action="rdv">
		<input type="hidden" name="action">
		<input type="hidden" name="id">
		
		<div class="row">
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="rdv.agtn" text="!"/></label>
					<form:input path="agtn" maxlength="11" type="text" cssClass="form-control textfield"/>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="rdv.rdv" text="!"/></label>
					<form:input path="rdv" type="text" cssClass="form-control textfield"/>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="form-group">
					<label><spring:message code="rdv.emission" text="!"/></label>
					<div class="input-group date">
                       	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    	<form:input path="emission" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
				</div>
			</div>
		</div>
	
		<div class="row">
			<div class="col-lg-4">
				<button type="button" class="btn btn-success text-uppercase" data-toggle="modal" data-target="#formRdv">
					<i class="fa fa-plus-square"></i> <spring:message code="button.create" text="!"/>
				</button>
		 	</div>
			<div class="col-lg-8 text-right">
				<button onclick="javascript:doSubmit('RESET');" class="btn btn-w-m btn-default text-uppercase">
					<i class="fa fa-undo"></i> <spring:message code="button.reset" text="!"/>
				</button>
				<button onclick="javascript:doSubmit('GO');" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-check"></i> <spring:message code="button.go" text="!"/>
				</button>
			</div>
		</div>
	
	 	&nbsp; 
		<div class="row">
			<div class="col-lg-12">
				<div id="_results"></div>
			</div>
		</div>
	</form:form>
	
	<!-- Modal create -->
	<div class="modal inmodal" id="formRdv" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" >
			<div class="modal-content animated bounceInRight" style="width: 650px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
					<h5 class="modal-title"><spring:message code="rdv.title" text="!"/></h5>
				</div>
				<div class="modal-body">
				<form:form id="form-add" name="form-add" method="post" commandName="rdvAddFilter" action="rdvAdd">
					<input type="hidden" name="action" value="ADD">
					
					<div class="form-group">
						<div class="row">
							<div class="col-lg-6">
							 	<label><spring:message code="rdv.agtn" text="!"/></label>
							 	<div class="input-group">
									<form:input path="agtn" maxlength="11" class="form-control textfield"/>
				                    <div class="input-group-btn">
				                        <button data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">Action <span class="caret"></span></button>
				                        <ul class="dropdown-menu pull-right">
				                            <li><a data-toggle="modal" data-target="#formAgent"><spring:message code="awb.add.agent" text="!"/></a></li>
				                        </ul>
				                    </div>
			                    </div>
			                    <label id="err_agtn" class="text-danger"></label>
						 	</div>
						 	<div class="col-lg-6">
							 	<label><spring:message code="rdv.emission" text="!"/></label>
							 	<div class="input-group date">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			                        <form:input path="emission" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
								</div>
								<label id="err_emission" class="text-danger"></label>
						 	</div>
						 </div>
					 </div>
						
					</form:form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<a onclick="doValidatorRdv();" class="btn btn-w-m btn-success text-uppercase" type="submit">
		  				<i class="fa fa-check"></i> <spring:message code="button.go" text="!Go"/>
		  			</a>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal inmodal" id="formAgent" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" style="margin-left: 200px;">
			<div class="modal-content animated bounceInRight" style="width: 1050px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
					<h5 class="modal-title"><spring:message code="agent.title" text="!"/></h5>
				</div>
				<div class="modal-body">
					<form:form id="form-agent" name="form-agent" method="post" modelAttribute="agentFilter" action="addAgent">
						<input type="hidden" name="action" value="ADD">
							<div class="form-group">
								<div class="row">
								 	<div class="col-lg-3">
									 	<label><spring:message code="agent.code" text="!"/></label>
									 	<form:input path="agtnCode" maxlength="11" class="form-control textfield"></form:input>
									 	<label id="err_agtnCode" class="text-danger"></label>
									 </div>
									 <div class="col-lg-3">
									 	<label><spring:message code="agent.name" text="!"/></label>
									 	<form:input path="name" class="form-control textfield"></form:input>
									 </div>
									 <div class="col-lg-3">
									 	<label><spring:message code="agent.address" text="!"/></label>
									 	<form:input path="address1" class="form-control textfield"></form:input>
									 </div>
									 <div class="col-lg-3">
									 	<label><spring:message code="agent.kind" text="!"/></label>
									 	<form:select path="kind" cssClass="chosen-select" cssStyle="width:100%;">
											<option value="TR" <c:if test="${'TR' == agentFilter.kind}">selected="selected"</c:if>>TR</option>
											<option value="CA" <c:if test="${'CA' == agentFilter.kind}">selected="selected"</c:if>>CA</option>
										</form:select>
									 </div>
								 </div>
							 </div>
							 <div class="form-group">
								<div class="row">
								 	<div class="col-lg-3">
									 	<label><spring:message code="agent.city" text="!"/></label>
									 	<form:input path="city" class="form-control textfield"></form:input>
									 </div>
									 <div class="col-lg-3">
									 	<label><spring:message code="agent.zip" text="!"/></label>
									 	<form:input path="zip" class="form-control textfield"></form:input>
									 </div>
									 <div class="col-lg-3">
									 	<label><spring:message code="agent.country" text="!"/></label>
									 	<form:select path="isoc" cssClass="chosen-select" cssStyle="width:100%;">
									 		<c:forEach items="${listIsoc}" var="elm">
												<option value="${elm.isoc}"
													<c:if test="${elm.isoc == agentFilter.isoc}" >selected="selected"</c:if>>
													<spring:message code="isoc.${elm.isoc}" text="notFound"/>
												</option>
											</c:forEach>
										</form:select>
									 </div>
									 <div class="col-lg-3">
									 	<label><spring:message code="agent.region" text="!"/></label>
									 	<form:input path="region" class="form-control textfield"></form:input>
									 </div>
									
								 </div>
							 </div>
							 <div class="form-group">
								<div class="row">
								 	<div class="col-lg-3">
									 	<label><spring:message code="agent.phone" text="!"/></label>
									 	<form:input path="phone" class="form-control textfield"></form:input>
									 </div>
									 <div class="col-lg-3">
									 	<label><spring:message code="agent.fax" text="!"/></label>
									 	<form:input path="fax" class="form-control textfield"></form:input>
									 </div>
									 <div class="col-lg-3">
									 	<label><spring:message code="agent.email" text="!"/></label>
									 	<form:input path="email" class="form-control textfield"></form:input>
									 </div>
									 <div class="col-lg-3">
									 	<label><spring:message code="agent.iata.ind" text="!"/></label>
									 	<form:select path="iataInd" cssClass="chosen-select" cssStyle="width:100%;">
											<option value="1" <c:if test="${'1' == agentFilter.iataInd}">selected="selected"</c:if>><spring:message code="agent.yes" text="!"/></option>
											<option value="0" <c:if test="${'0' == agentFilter.iataInd}">selected="selected"</c:if>><spring:message code="agent.no" text="!"/></option>
										</form:select>
									 </div>
									
								 </div>
							 </div>
							 <div class="form-group">
								<div class="row">
								 	<div class="col-lg-3">
									 	<label><spring:message code="agent.comm" text="!"/></label>
									 	<form:input path="comm" class="form-control textfield"></form:input>
									 </div>
								 </div>
							 </div>
					 </form:form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<a onclick="doValidatorAgtn();" class="btn btn-w-m btn-success text-uppercase" type="submit">
		  				<i class="fa fa-check"></i> <spring:message code="button.go" text="!Go"/>
		  			</a>
				</div>
			</div>
		</div>
	</div>
</div>