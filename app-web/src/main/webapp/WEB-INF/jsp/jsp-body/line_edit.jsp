<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<script type="text/javascript">
	function doEditForm(action, id){
		$.ajax({
			url: "<spring:url value='/secure/line/edit/error.json'/>",
			type: "POST",
			cache: false,
			data: $('#form_modify').serialize(),
			success: function(result){
	        	if(result.errCodeEdit > 0){
		           	 $('#err_lineEdit').html('');
		           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
		           	 for(var i=0;i<len;i++)
	               	 {
	                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
		        	}else{
		        		document.forms[1].elements['action'].value=action;
		        		document.forms[1].elements['id'].value=id;
		        		$('#form_modify').submit();
		        	}
		        },
			 	error: function(XMLHttpRequest, textStatus, errorThrown){}
		});
	}
</script>

<form:form name="form_modify" id="form_modify" method="post" commandName="lineFilter" action="line/edit">
	<input name="action" type="hidden">
	<input name="id" type="hidden">
	
	<div class="panel panel-success" style="margin-top:9px;">
         <div class="panel-heading" style="padding: 4px;">
             <div class="row">
                 <div class="col-lg-11">
                     <h3><spring:message code="message.modify" text="!Modify"/> ${line.descripbe }</h3>
                 </div>
                 <div class="col-lg-1 text-right">
                     <i class="fa fa-2x fa-times-circle" onclick="javascript:hide('edit-box');" style="cursor:pointer;"></i>
                 </div>
             </div>
         </div>
		
         <div class="panel-body" style="padding:5px;">
         	<div class="row">
				<div class="col-lg-3">
					<div class="form-group">
						<label><spring:message code="line.orig" text="!"/></label>
						<form:input path="origEdit" value="${line.orac }" readonly="true" maxlength="3" type="text" cssClass="form-control textfield text-uppercase"/>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group">
						<label><spring:message code="line.dest" text="!"/></label>
						<form:input path="destEdit" value="${line.dstc }" readonly="true" maxlength="3" type="text" cssClass="form-control textfield text-uppercase"/>
					</div>
				</div>
				
				<div class="col-lg-3">
					<div class="form-group">
						<label><spring:message code="line.line" text="!"/></label>
						<form:input path="lineEdit" value="${fn:trim(line.line) }" maxlength="5" type="text" cssClass="form-control textfield text-uppercase"/>
						<label id="err_lineEdit" class="text-danger"></label>
					</div>
				</div>
			</div>	 
			<div class="row">
				<div class="col-lg-12 text-right">
		  			<a onclick="doEditForm('UPDATE','${fn:trim(line.descripbe)}')" class="btn btn-w-m btn-success text-uppercase" type="submit">
		  				<i class="fa fa-check"></i> <spring:message code="button.go" text="!Go"/>
		  			</a>
				</div>
			</div>
         </div>
     </div>
</form:form>