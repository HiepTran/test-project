<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="row">
	<div class="col-lg-5">
		<label class="label label-success" style="font-size:14px">${agentFilterPara.rowCount} <spring:message code="message.result" text="!Result(s)"/></label>
	</div>
	<div class="col-lg-7 text-right">
		<c:if test="${agentFilterPara.totalPage > 1}">
			<div class="form-group">
				<label class="control-label" style="font-weight: 500; font-size: 14px;"><spring:message code="button.page" text="!page"></spring:message>&nbsp;</label>
				<input id="PageNoGo" type="text" style="width:50px; height:30px; vertical-align:middle;border-radius:3px; border: 1px solid #e5e6e7; font-size:14px;" value="${agentFilterPara.page + 1}">
		        <label class="control-label" style="font-weight: 500; font-size:15px"> /${agentFilterPara.totalPage}&nbsp;</label>
		        <button id="go" class="btn btn-success btn-sm text-uppercase" type="button">&nbsp;<spring:message code="button.go.page" text="!Go"/>&nbsp;</button>
		        <div class="btn-group">
			        <c:choose>
				        <c:when test="${agentFilterPara.page > 0}">
				        	<a id="prev" class="btn btn-sm btn-white text-uppercase" href="--pageNo"><spring:message code="button.prev" text="!Prev."/></a>
				        </c:when>
				        <c:otherwise>
				         	<a class="btn btn-sm btn-white text-uppercase disabled"><spring:message code="button.prev" text="!Prev."/></a>
				        </c:otherwise>
			        </c:choose>
		                        
		                <c:choose>
				            <c:when test="${agentFilterPara.page < (agentFilterPara.totalPage - 1)}">
				            	<a id="next" class="btn btn-sm btn-white text-uppercase" href="++pageNo"><spring:message code="button.next" text="!Next"/></a>
				                </c:when>
				                <c:otherwise>
				                	<a class="btn btn-sm btn-white text-uppercase disabled"><spring:message code="button.next" text="!Next"/></a>
				                </c:otherwise>
			            </c:choose>
		          </div>
			</div>
		</c:if>
	</div>
</div>

<table class="table table-striped table-bordered table-hover table-lta" style="width:100%;">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<th class="text-center"><spring:message code="commom.action" text="!Action"/></th>
			<th class="text-right"><spring:message code="agent.code" text="!"/></th>
			<th class="text-right"><spring:message code="agent.check.digit" text="!"/></th>
			<th><spring:message code="agent.name" text="!"/></th>
			<th><spring:message code="agent.kind" text="!"/></th>
			<th><spring:message code="agent.address" text="!"/></th>
			<th><spring:message code="agent.city" text="!"/></th>
			<th class="text-right"><spring:message code="agent.zip" text="!"/></th>
			<th><spring:message code="agent.country" text="!"/></th>
			<th><spring:message code="agent.region" text="!"/></th>
			<th><spring:message code="agent.comm" text="!"/></th>
			<th><spring:message code="agent.phone" text="!"/></th>
			<th><spring:message code="agent.fax" text="!"/></th>
			<th><spring:message code="agent.email" text="!"/></th>
			<th class="text-center"><spring:message code="agent.iata.ind" text="!"/></th>
		</tr>
	</thead>
	<tbody>
	  	<c:set var="formatPattern" value="#,##0.00"/>
		
		<c:forEach items="${agents}" var="elm" varStatus="stt">
			<tr id="${elm.code}">
				<td class="text-center">${row + stt.index + 1}</td>
				<td class="text-center text-nowrap">
					<a id='a-edit-${elm.code}' onclick='bfShow("edit-box","a-edit-${elm.code}", "${elm.code}"); showForm("edit-box", "${elm.code}")' 
						title="<spring:message code="message.modify" text="!Edit"/>"><i class="fa fa-2x fa-edit"></i>
					</a>
					<a onclick="ConformDelete('${elm.code}')" title="<spring:message code="button.delete" text="!Delete"/>"><i class="fa fa-2x fa-trash-o"></i></a>
								
				</td>
				<td>${fn:trim(elm.code)}</td>
				<td class="text-right">${elm.checkDigit}</td>
				<td>${elm.name}</td>
				<td>${elm.kind}</td>
				<td>${elm.address1}</td>
				<td>${elm.city}</td>
				<td class="text-right">${elm.zip}</td>
				<td><spring:message	code="isoc.${elm.isoc}" text="notFound" /></td>
				<td>${elm.region}</td>
				<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.comm}"/></td>
				<td class="text-nowrap">${elm.phone}</td>
				<td class="text-nowrap">${elm.fax}</td>
				<td>${elm.email}</td>
				<td class="text-center">
				<c:choose>
					<c:when test="${elm.iataInd == 1}">
						<spring:message code="agent.yes" text="!"/>
					</c:when>
					<c:otherwise>
						<spring:message code="agent.no" text="!"/>
					</c:otherwise>
				</c:choose>
				</td>
			</tr>
		</c:forEach>	
	</tbody>
</table>
<script type="text/javascript">

	var pageCount = ${agentFilterPara.totalPage};
	
	function showForm(divId, id) {
 	    $.ajax({
 	        url: "<spring:url value='/secure/agent/model/ajax/'/>" + id,
 	        cache: false,
 	        success: function (reponse) {
 	            $("div#edit-result").html(reponse);
 	        }
 	    });
 	    show(divId);
 	}
	
	$(function () {
		$("#prev").click(function () {
		    var href = $(this).attr("href");
		    eval("pageNo = (pageCount + " + href + " ) % pageCount");
		    fnLoadPage();
		    return false;
		});
		
		$("#next").click(function () {
		    var href = $(this).attr("href");
		    eval("pageNo = (pageCount + " + href + " ) % pageCount");
		    fnLoadPage();
		    return false;
		});
		$("#go").click(function () {
		    var href = $("#PageNoGo").val() - 1;
		
		    if (href < 1 || isNaN(href)) {
		        eval("pageNo = (pageCount + " + 0 + " ) % pageCount");
		    } else if (href > pageCount - 1) {
		        eval("pageNo = (pageCount + " + (pageCount - 1) + " ) % pageCount");
		    } else {
		        eval("pageNo = (pageCount + " + href + " ) % pageCount");
		    }
		    fnLoadPage();
		    return false;
		});
		
	});
		
	function fnLoadPage() {
		$.ajax({
			url : "<spring:url value='/secure/agent/loadpage'/>",
		    data: { pageNo: pageNo },
		    cache : false,
		    success: function (response) {
		        $("#_results").html(response);
		    }
		});
	};
	
	$(function () {
 	 	$('.table-lta').dataTable({
 	 		"dom": 'T<"clear">lfrtip',
 	        "sDom": "<'row'<'col-sm-6 text-left'><'col-sm-6 text-right'>r>" + "t" + "<'row'<'col-sm-6'><'col-sm-6'p>>",
 			"bSort": false,
 			"paginate": false,
 			"scrollY": true,
 			"scrollY": '500px',
 	        "scrollCollapse": true,
 	        "scrollX": true
 	   	}); 	 	
	});
</script>