<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>

<style type="text/css">
	.hr-line-dashed {
	    background-color: #ffffff;
	    border-top: 1px dashed #e7eaec;
	    color: #ffffff;
	    height: 1px;
	    margin: 1px 5px 10px 10px;
	}
	
	.chosen-container-single .chosen-single {
         background: none;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
     
     .sweet-alert {
	    background-color: white;
	    border-radius: 5px;
	    display: none;
	    font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
	    left: 50%;
	    margin-left: -256px;
	    margin-top: -200px;
	    overflow: hidden;
	    padding: 17px;
	    position: fixed;
	    text-align: center;
	    top: 50%;
	    width: 700px;
	    z-index: 99999;
	}
</style>

<script type="text/javascript">
	document.getElementById("lta").className = "active";
	document.getElementById("listLTA").className = "active";

	$(document).ready(function() {
		$('.input-group.date').datepicker({
			todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
			format: "dd/mm/yyyy"
	    });
	    
	    $('.chosen-select').chosen({
	    	"disable_search": true
	    });
	    
	    doShowPayment();
	});

	function doShowPayment(){
		var mode = $('#modePayment').val();
		if(mode=='CC'){
			$('#payment_inf').css('display','block');
		}else{
			$('#payment_inf').css('display','none');
		}
	}

	function doValidatorAwb(){
		$.ajax({
			url: "<spring:url value='/secure/awb/detail/error.json'/>",
	        data: $('#form').serialize(), 
	        type: "POST",
	        success: function(result){
	        	if(result.errCode > 0){
	           	 	$('#err_isoc').html('');
	           	 	$('#err_dateExecute').html('');
   					$('#err_orig').html('');
					$('#err_dest').html('');
		           	$('#err_km').html('');
		           	$('#err_flightNumber').html('');
		           	$('#err_flightDate').html('');
		           	$('#err_errorDetail').html('');
		           	$('#err_nbCart').html('');
		           	$('#err_dateExp').html('');
		           	$('#err_nameCart').html('');
		           	$('#err_errorTax').html('');
		           	$('#err_commPercent').html('');
		           	$('#err_vatPercent').html('');
				           																							
	           	 	var len = $.map(result.lstErr, function(n, i) { return i; }).length;
	           	 	for(var i=0;i<len;i++) {
                   		$('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
	        	}else{
	        		$('#form').submit();
	        	}
	        },
		 	error: function(XMLHttpRequest, textStatus, errorThrown){}
		});
	}

	function doValidatorTax(){
		$.ajax({
			url: "<spring:url value='/secure/awb/detail/errorTax.json'/>",
	        data: $('#form-tax').serialize(), 
	        type: "POST",
	        success: function(result){
	        	if(result.errCodeTax == 1){
	           	 	$('#err_errorTax').html('');
				           																							
	           	 	var len = $.map(result.lstErr, function(n, i) { return i; }).length;
	           	 	for(var i=0;i<len;i++) {
                   		$('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
	              	 }
	        	}else if(result.errCodeTax == 2){
		        	var title = '<spring:message code="awb.tax.empty" text="!"/>';
		        	title += '\n';
		        	title += '<spring:message code="awb.tax.zero" text="!"/>';
	        		swal({
	                    title: title,
	                    type: "warning",
	                    showCancelButton: true,
	                    confirmButtonColor: "#DD6B55",
	                    confirmButtonText: "<spring:message code="button.yes" text="!"/>",
	                    cancelButtonText: "<spring:message code="button.no" text="!"/>",
	                    closeOnConfirm: false
	                }, function () {
	                	$('#form-tax').submit();
	                });
	        	}else{
	        		$('#form-tax').submit();
	        	}
	        },
		 	error: function(XMLHttpRequest, textStatus, errorThrown){}
		});
	}
	
</script>

<div id="body">
	<c:set var="formatPattern" value="#,##0.00"/>

	<form:form name="form" id="form" method="post" commandName="awbDetailFilter">
		<input type="hidden" name="action" value="VALIDER">
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
					<button onclick="javascript:doSubmit('BACK');" class="btn btn-w-m btn-default text-uppercase">
						<spring:message code="button.back" text="!"/>
					</button>
				</div>
				<div class="col-lg-6 text-right">
					<a onclick="javascript:doValidatorAwb()" class="btn btn-w-m btn-success text-uppercase">
						<i class="fa fa-check"></i> <spring:message code="button.valider" text="!"/>
					</a>
				</div>
			</div>
		</div>
		&nbsp;
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-1">
					<label class="control-label"><spring:message code="awb.awb" text="!"/></label>
					<form:input path="tacn" readonly="true" value="${awb.tacn }" class="form-control textfield"/>
				</div>
				<div class="col-lg-2">
					<label>&nbsp;</label>
					<form:input path="lta" readonly="true" value="${awb.lta }" class="form-control textfield"/>
				</div>
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="awb.agent" text="!"/></label>
					<form:input path="agtn" readonly="true" value="${awb.agtn}" class="form-control textfield"/>
				</div>
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="awb.agent.gsa" text="!"/></label>
					<form:input path="agtnGsa" readonly="true" value="${awb.agtnGsa}" class="form-control textfield"/>
				</div>
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="awb.rdv" text="!"/></label>
					<form:input path="rdv" readonly="true" value="${awb.rdv}" class="form-control textfield"/>
				</div>
			</div>
		</div>
			
		<div class="form-group">	
			<div class="row">
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="awb.country" text="!"/></label>
					<form:input path="isoc" maxlength="2" value="${awb.isoc}" class="form-control textfield text-uppercase"/>
					<label id="err_isoc" class="text-danger"></label>
				</div>
				<div class="col-lg-6">
					<label class="control-label"><spring:message code="awb.agent.name" text="!"/></label>
					<input type="text" readonly="readonly" value="${awb.agtnName }" class="form-control textfield">
				</div>
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="awb.date.execution" text="!Date execution"/></label>
					<fmt:formatDate pattern="dd/MM/yyyy" value="${awb.dateExecute}" var="dateExecute"/>
					<div class="input-group date">
		               	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		               	<form:input path="dateExecute" value="${dateExecute }" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
					<label id="err_dateExecute" class="text-danger"></label>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
					<label class="control-label"><spring:message code="awb.shipper" text="!Shipper"/></label>
					<form:input path="shipName" value="${awb.shipName}" class="form-control textfield"/>
					<!-- <label id="err_shipName" class="text-danger"></label> -->
				</div>
				<div class="col-lg-6">
					<label class="control-label"><spring:message code="awb.consignee" text="!Consignee's"/></label> 
					<form:input path="consigneeName" value="${awb.consigneeName}" class="form-control textfield"/>
					<!-- <label id="err_consigneeName" class="text-danger"></label> -->
				</div>
			</div>
		</div>	
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
					<form:input path="shipAddress1" value="${awb.shipAddress1 }" class="form-control textfield"/>
				</div>
				<div class="col-lg-6">
					<form:input path="consigneeAddress1" value="${awb.consigneeAddress1 }" class="form-control textfield"/>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
					<form:input path="shipAddress2" value="${awb.shipAddress2}" class="form-control textfield"/>
				</div>
				<div class="col-lg-6">
					<form:input path="consigneeAddress2" value="${awb.consigneeAddress2 }" class="form-control textfield"/>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-6">
					<form:input path="shipAddress3" value="${awb.shipAddress3 }" class="form-control textfield"/>
				</div>
				<div class="col-lg-6">
					<form:input path="consigneeAddress3" value="${awb.consigneeAddress3 }" class="form-control textfield"/>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="awb.orig" text="!"/></label>
					<form:input path="orig" value="${awb.orig }" maxlength="3" class="form-control textfield text-uppercase"/>
					<label id="err_orig" class="text-danger"></label>
				</div>
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="awb.dest" text="!"/></label> 
				 	<form:input path="dest" value="${awb.dest }" maxlength="3" class="form-control textfield text-uppercase"/>
				 	<label id="err_dest" class="text-danger"></label>
				</div>
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="awb.km" text="!Km"/></label>
					<form:input path="km" value="${awb.km }" class="form-control textfield text-uppercase"/>
					<label id="err_km" class="text-danger"></label>
				</div>
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="awb.cutp" text="!"/></label>
					<form:select path="cutp" class="chosen-select" cssStyle="width:100%;">
						<option value="CHF" <c:if test="${'CHF' == awb.cutp}">selected="selected"</c:if>>CHF</option>
						<option value="EUR" <c:if test="${'EUR' == awb.cutp}">selected="selected"</c:if>>EUR</option>
						<option value="NGN" <c:if test="${'NGN' == awb.cutp}">selected="selected"</c:if>>NGN</option>
						<option value="USD" <c:if test="${'USD' == awb.cutp}">selected="selected"</c:if>>USD</option>
						<option value="XAF" <c:if test="${'XAF' == awb.cutp}">selected="selected"</c:if>>XAF</option>
						<option value="XOF" <c:if test="${'XOF' == awb.cutp}">selected="selected"</c:if>>XOF</option>
					</form:select>
				</div>
			</div>
		</div>
	
		<div class="form-group">
			<div class="row">
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="awb.flight.number" text="!"/></label> 
					<form:input path="flightNumber" value="${awb.flightNumber }" class="form-control textfield text-uppercase"/>
					<label id="err_flightNumber" class="text-danger"></label>
				</div>
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="awb.flight.date" text="!"/></label>
					<fmt:formatDate pattern="dd/MM/yyyy" value="${awb.flightDate}" var="flightDate"/>
					<div class="input-group date">
	                	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
	                	<form:input path="flightDate" value="${flightDate }" type="text" cssClass="form-control textfield" placeholder="dd/mm/yyyy"/>
					</div>
					<label id="err_flightDate" class="text-danger"></label>
				</div>
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="awb.declared.carrier" text="!"/></label> 
					<form:input path="declaredCarrige" value="${awb.declaredCarrige}" class="form-control textfield text-uppercase"/>
				</div>
				<div class="col-lg-3">
					<label class="control-label"><spring:message code="awb.declared.custom" text="!"/></label> 
					<form:input path="declaredCustom" value="${awb.declaredCustom}" class="form-control textfield text-uppercase"/>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-3">
	              	<label class="control-label"><spring:message code="awb.fare" text="!"/></label>
	              	<div class="radio radio-primary">
	                    <input type="radio" name="chargeIndicator" id="radio1" value="P"
	                    <c:if test="${fn:trim(awb.chargeIndicator) eq 'P'}">checked</c:if>>
	                    <label for="radio1">
	                        <spring:message code="awb.fare.pp" text="!"/>
	                    </label>
	                </div>
	                <div class="radio radio-primary">
	                    <input type="radio" name="chargeIndicator" id="radio2" value="C"
	                    <c:if test="${fn:trim(awb.chargeIndicator) eq 'C'}">checked</c:if>>
	                    <label for="radio2">
	                        <spring:message code="awb.fare.cc" text="!"/>
	                    </label>
	                </div>
				</div>
				<div class="col-lg-3">
	               	<label class="control-label"><spring:message code="awb.other" text="!"/></label>
	               	<div class="radio radio-primary">
	                    <input type="radio" name="otherIndicator" id="radio3" value="P" 
	                    <c:if test="${fn:trim(awb.otherIndicator) eq 'P'}">checked</c:if>>
	                    <label for="radio3">
	                        <spring:message code="awb.other.pp" text="!"/>
	                    </label>
	                </div>
		            <div class="radio radio-primary">
		                <input type="radio" name="otherIndicator" id="radio4" value="C"
		                <c:if test="${fn:trim(awb.otherIndicator) eq 'C'}">checked</c:if>>
		                <label for="radio4">
		                    <spring:message code="awb.other.cc" text="!"/>
		                </label>
		            </div>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-12">
					<label class="control-label"><spring:message code="awb.handling" text="!"/></label>
					<textarea name="handlingInfor" rows="2" class="form-control textfield">${awb.handlingInfor }</textarea>
				</div>
			</div>		
		</div>
		<div class="hr-line-dashed"></div>
			
		<!-- -----------------------------------------------DETAIL---------------------------------------------------------------------- -->
		<div class="row">
			<div class="form-group">
				<label class="col-lg-1"><spring:message code="awb.rcp" text="!"/></label>
				<label class="col-lg-1 text-right"><spring:message code="awb.weight.gross" text="!"/></label> 
				<label class="col-lg-1"><spring:message code="awb.weight.indicator" text="!"/></label> 
				<label class="col-lg-1"><spring:message code="awb.rate" text="!"/></label> 
				<label class="col-lg-1 text-right"><spring:message code="awb.weight.charge" text="!"/></label> 
				<label class="col-lg-2 text-right"><spring:message code="awb.tarif" text="!"/></label> 
				<label class="col-lg-2 text-right"><spring:message code="awb.total" text="!"/></label> 
				<label class="col-lg-3"><spring:message code="awb.description" text="!"/></label> 
			</div>
		</div>
		
		<div class="row">
			<div class="form-group">
				<div class="col-lg-12">
					<label id="err_errorDetail" class="text-danger"></label>
				</div>
			</div>
		</div>
				
		<c:set value="0" var="_cnt" />
		<spring:message code="awb.tarif.cass" text="!" var="titleTarif"/>
		<c:forEach items="${awb.awbDetail}" var="elm">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-1">
						<form:input path="awbDetail[${_cnt }].quantity" maxlength="3" value="${elm.quantity}" cssClass="form-control textfield text-uppercase"/>
					</div>
					<div class="col-lg-1">
						<form:input path="awbDetail[${_cnt }].weightGross" value="${elm.weightGross}" cssClass="form-control textfield text-right"/>
					</div>
					<div class="col-lg-1">
						<form:select path="awbDetail[${_cnt }].weightIndicator" class="chosen-select" cssStyle="width:100%;">
							 <option value="K" <c:if test="${'K' == elm.weightIndicator}">selected="selected"</c:if>>K</option>
							 <option value="P" <c:if test="${'P' == elm.weightIndicator}">selected="selected"</c:if>>P</option>
						</form:select>
					</div>
					<div class="col-lg-1">
						<form:input path="awbDetail[${_cnt }].rateClass" value="${elm.rateClass}" class="form-control textfield text-uppercase"/>
					</div>
					<div class="col-lg-1 text-right">
						<form:input path="awbDetail[${_cnt }].weightCharge" value="${elm.weightCharge}" class="form-control textfield text-right"/>
					</div>
					<div class="col-lg-2 text-right">
						<form:input path="awbDetail[${_cnt }].unit" value="${elm.unit}" title="${titleTarif}: ${elm.unitCass}" class="form-control textfield text-right"/>
					</div>
					<div class="col-lg-2 text-right">
						<form:input path="awbDetail[${_cnt }].amount" value="${elm.amount}" readonly="true" class="form-control textfield text-right"/>
					</div>
					<div class="col-lg-3">
						<form:input path="awbDetail[${_cnt }].description" value="${elm.description}" class="form-control textfield"/>
					</div>
				</div>
			</div>
			 <c:set value="${_cnt+1 }" var="_cnt" />
		</c:forEach>
					
		<div class="hr-line-dashed"></div>
		<div class="row">
		<!-- ------------------------------------------------ TOTAL ------------------------------------------------------- -->
			<div class="col-lg-7">
				<div class="row">
					<div class="col-lg-4"></div>
					<div class="col-lg-4 text-right">
						<label class="control-label"><spring:message code="awb.prepaid" text="!"/></label>
					</div>
					<div class="col-lg-4 text-right">
						<label class="control-label"><spring:message code="awb.collect" text="!"/></label>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4"><label></label></div>
				</div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-lg-4">
							<label class="control-label"><spring:message code="awb.total.weight.charge" text="!"/></label>
						</div>
						<div class="col-lg-4">
							<input value="${awb.weightPp}" readonly="readonly" class="form-control textfield text-right"/>
						</div>
						<div class="col-lg-4">
							<input value="${awb.weightCc}" readonly="readonly" class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-lg-4">
							<label class="control-label"><spring:message code="awb.total.charge.agent" text="!"/></label>
						</div>
						<div class="col-lg-4">
							<input value="${awb.agentPp}" readonly="readonly" class="form-control textfield text-right"/>
						</div>
						<div class="col-lg-4">
							<input value="${awb.agentCc}" readonly="readonly" class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-lg-4">
							<label class="control-label"><spring:message code="awb.total.charge.carrier" text="!"/></label>
						</div>
						<div class="col-lg-4">
							<input value="${awb.carrierPp}" readonly="readonly" class="form-control textfield text-right"/>
						</div>
						<div class="col-lg-4">
							<input value="${awb.carrierCc}" readonly="readonly"  class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-lg-4">
							<label class="control-label"><spring:message code="awb.total" text="!"/></label>
						</div>
						<div class="col-lg-4">
							<input value="${awb.totalPp}" readonly="readonly" class="form-control textfield text-right"/>
						</div>
						<div class="col-lg-4">
							<input value="${awb.totalCc}" readonly="readonly" class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-lg-4">
							<label class="control-label"><spring:message code="awb.net" text="!"/></label>
						</div>
						<div class="col-lg-4">
							<input value="${awb.net}" readonly="readonly" class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
					
				<div class="row">
					<div class="col-lg-12">
						<div class="hr-line-dashed"></div>
					</div>
				</div>
				
				<!-- Commission -->
				<div class="form-group">
					<div class="row">
						<div class="col-lg-4">
							<label class="control-label"><spring:message code="awb.com.percent" text="!"/></label>
							<c:if test="${not empty awb.agtnGsa }">(GSA)</c:if>
						</div>
						<div class="col-lg-4 text-right">
							<form:input path="commPercent" type="text" value="${awb.commPercent}" class="form-control textfield text-right"/>
							<label id="err_commPercent" class="text-danger"></label>
						</div>
						<div class="col-lg-4 text-right">
							<input type="text" value="${awb.comm}" readonly="readonly" class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-lg-4">
							<label class="control-label"><spring:message code="awb.vat" text="!"/>(%)</label>
						</div>
						<div class="col-lg-4">
							<form:input path="vatPercent" value="${awb.vatPercent}" class="form-control textfield text-right"/>
							<!-- <label id="err_vatPercent" class="text-danger"></label> -->
							<form:errors path="vatPercent"></form:errors>
						</div>
						<div class="col-lg-4">
							<input value="${awb.vat}" readonly="readonly" class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
					
				<div class="form-group">
					<div class="row">
						<div class="col-lg-4">
							<label class="control-label"><spring:message code="awb.mode.payment" text="!"/></label>
						</div>
						<div class="col-lg-4">
							<form:select path="modePayment" onchange="doShowPayment()" class="chosen-select" cssStyle="width:100%;">
								<option value="CA" <c:if test="${'CA' == awb.modePayment}">selected="selected"</c:if>>
									<spring:message code="awb.mode.payment.CA" text="!"/>
								</option>
								<option value="CH" <c:if test="${'CH' == awb.modePayment}">selected="selected"</c:if>>
									<spring:message code="awb.mode.payment.CH" text="!"/>
								</option>
								<option value="EC" <c:if test="${'EC' == awb.modePayment}">selected="selected"</c:if>>
									<spring:message code="awb.mode.payment.EC" text="!"/>
								</option>
								<option value="CC" <c:if test="${'CC' == awb.modePayment}">selected="selected"</c:if>>
									<spring:message code="awb.mode.payment.CC" text="!"/>
								</option>
							</form:select>
						</div>
					</div>
				</div>
					
				<div id="payment_inf" style="display: none;">
					<div class="form-group">
						<div class="row">
							<div class="col-lg-4">
								<label><spring:message code="awb.nb.cart" text="!Port paye"/></label>
								<form:input path="nbCart" value="${awb.nbCart }" class="form-control textfield"/>
								<label id="err_nbCart" class="text-danger"></label>
							</div>
							<div class="col-lg-4">
								<label><spring:message code="awb.date.exp" text="!Port paye"/></label>
								<div class="input-group date">
			                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			                        <form:input path="dateExp" value="${awb.dateExp }" type="text" cssClass="form-control textfield" placeholder="dd/MM/yyyy"/>
								</div>
								<label id="err_dateExp" class="text-danger"></label>
							</div>
							<div class="col-lg-4">
								<label><spring:message code="awb.name.cart" text="!Port paye"/></label>
								<form:input path="nameCart" value="${awb.nameCart }" class="form-control textfield"/>
								<label id="err_nameCart" class="text-danger"></label>
							</div>
						</div>
					</div>
				</div>
					
				<div class="form-group">
					<div class="row">
						<div class="col-lg-4">
							<label class="control-label"><spring:message code="awb.discount" text="!"/></label>
						</div>
						<div class="col-lg-4 text-right">
							<input type="text" value="${awb.discount}" disabled="disabled" class="form-control textfield text-right"/>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-lg-12">
							<label class="control-label"><spring:message code="awb.note" text="!"/></label>
							<textarea name="descriptUser" rows="2" class="form-control textfield">${awb.descriptUser }</textarea>
						</div>
					</div>
				</div>
			</div>
	
		<!-- -------------------------------------------------------- TAX ----------------------------------------------------------------------------- -->
			<div class="col-lg-1"></div>
			<div class="col-lg-4">
				<div class="row">
					<div class="col-lg-7">
						<label class="control-label"><spring:message code="awb.frais" text="!"/></label>
					</div>
					<div class="col-lg-5 text-right">
						<button type="button" style="margin-bottom: 5px;" class="btn btn-w-m btn-success text-uppercase" data-toggle="modal" data-target="#formTax">
							<i class="fa fa-pencil"></i> <spring:message code="button.modify" text="!Create"/>
						</button>
				 	</div>
				</div>
				<div class="row">
					<div class="col-lg-4"></div>
				</div>
				<c:forEach items="${awb.awbTax}" var="elm">
					<div class="form-group">
						<div class="row">
							<div class="col-lg-4">
								<input  value="${elm.id.taxCode}" readonly="readonly" class="form-control textfield"/>
							</div>
							<div class="col-lg-8">
								<input value="${elm.taxAmount}" readonly="readonly" class="form-control textfield text-right"/>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-lg-12 text-right">
					<a onclick="javascript:doValidatorAwb()" class="btn btn-w-m btn-success text-uppercase">
					<i class="fa fa-check"></i> <spring:message code="button.valider" text="!"/>
					</a>
				</div>
			</div>
		</div>
	</form:form>

	<div class="modal inmodal" id="formTax" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" >
			<div class="modal-content animated bounceInRight" style="width: 850px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
					<h5 class="modal-title"><spring:message code="awb.modify.tax" text="!"/></h5>
				</div>
				<div class="modal-body">
					<form:form id="form-tax" name="form-tax" method="post" commandName="awbDetailFilter" action="detail">
						<input type="hidden" name="action" value="TAX">
						<input type="hidden" name="id" value="${awb.lta }">
					
						<div class="row">
							<div class="col-lg-3">
								<label class="control-label"><spring:message code="awb.awb" text="!"/></label>
							</div>
							<div class="col-lg-2 text-right">
								<label>${awb.tacn}-${awb.lta}</label>
							</div>
							<div class="col-lg-2"></div>
							<div class="col-lg-3">
								<label class="control-label"><spring:message code="awb.total.charge.agent" text="!"/></label>
							</div>
							<div class="col-lg-2 text-right">
								<label>${awb.agentPp + awb.agentCc}</label>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-3">
								<label class="control-label"><spring:message code="awb.weight.charge" text="!"/></label>
							</div>
							<div class="col-lg-2 text-right">
								<label>${awb.weightCharge} ${awb.weightIndicator}</label>
							</div>
							<div class="col-lg-2"></div>
							<div class="col-lg-3">
								<label><spring:message code="awb.total.charge.carrier" text="!"/></label>
							</div>
							<div class="col-lg-2 text-right">
								<label>${awb.carrierPp + awb.carrierCc}</label>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-7"></div>
							<div class="col-lg-5">
								<label id="err_errorTax" class="text-danger"></label>
							</div>
						</div>
						
						<div class="row"><label></label></div>
						
						<div class="row">
							<div class="col-lg-5">
								<label class="control-label"><spring:message code="awb.frais" text="!"/></label>
							</div>
							<div class="col-lg-2"></div>
							<div class="col-lg-5">
								<label class="control-label"><spring:message code="awb.frais.modify" text="!"/></label>
							</div>
						</div>
						
						<c:set value="0" var="_cnt" />
						<c:forEach items="${awb.awbTax}" var="elm">
							<div class="form-group">
								<div class="row">
									<div class="col-lg-2">
										<input value="${elm.id.taxCode}" readonly="readonly" class="form-control textfield"/>
									</div>
									<div class="col-lg-3">
										<input value="${elm.taxAmount}" readonly="readonly" class="form-control textfield text-right"/>
									</div>
									<div class="col-lg-2"></div>
									<div class="col-lg-2">
										<form:input path="awbTax[${_cnt }].code" maxlength="3" value="${elm.id.taxCode}" cssClass="form-control textfield text-uppercase"/>
									</div>
									<div class="col-lg-3">
										<form:input path="awbTax[${_cnt }].amount" value="${elm.taxAmount}" cssClass="form-control textfield text-right"/>
									</div>
								</div>
							</div>
							 <c:set value="${_cnt+1 }" var="_cnt" />
						</c:forEach>
					</form:form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-w-m btn-default text-uppercase" data-dismiss="modal">
						<i class="fa fa-close"></i> <spring:message code="button.close" text="!Close"/>
					</button>
					<a onclick="doValidatorTax();" class="btn btn-w-m btn-success text-uppercase">
		  				<i class="fa fa-check"></i> <spring:message code="button.go" text="!Go"/>
		  			</a>
				</div>
			</div>
		</div>
	</div>
</div>