<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<link href="<c:url value="/static/inspinia/css/plugins/sweetalert/sweetalert.css"/>" rel="stylesheet">
<script src="<c:url value="/static/inspinia/js/plugins/sweetalert/sweetalert.min.js"/>"></script>

<script type="text/javascript">
	document.getElementById("lta").className = "active";
	document.getElementById("create").className = "active";

	function ConformDelete(id) {
        swal({
            title: "<spring:message code="message.delete" text="!"/>" + " [" + id + "] ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<spring:message code="button.delete" text="!"/>",
            cancelButtonText: "<spring:message code="button.cancel" text="!"/>",
            closeOnConfirm: false
        }, function () {
       	 	document.forms[0].elements['action'].value='DELETE';
			 document.forms[0].elements['id'].value=id;
			 document.forms[0].submit();
        });
    };
    
</script>

<div id="body">
	<form:form name="form" id="form" method="post" commandName="rdvFilter">
		<input type="hidden" name="action">
		<input type="hidden" name="id">
		
		<div class="row">
		    <div class="col-lg-6">
		        <label class="label label-success" style="font-size:14px">${fn:length(awbs)} <spring:message code="message.result" text="!Result(s)"/></label>
		    </div>
		    <div class="col-lg-6 text-right">
				<a href="<c:url value="/secure/rdv"/>" class="btn btn-w-m btn-default text-uppercase"><spring:message code="button.back" text="!"/></a>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<table class="table table-striped table-bordered table-hover table-lta" style="width:100%;">
					<thead>
						<tr>
							<th rowspan="2" class="text-center" style="vertical-align:middle;">#</th>
							<th rowspan="2" class="text-center" style="vertical-align:middle;"><spring:message code="commom.action" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.awb" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.agent" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.rdv" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.date.execution" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.orig" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.dest" text="!"/></th>
							<th rowspan="2" class="text-right" style="vertical-align:middle;"><spring:message code="awb.list.weight.charge" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.list.currency" text="!"/></th>
							<th colspan="3" class="text-center"><spring:message code="awb.prepaid" text="!"/></th>
							<th colspan="3" class="text-center"><spring:message code="awb.collect" text="!"/></th>
							<th rowspan="2" class="text-right" style="vertical-align:middle;"><spring:message code="awb.list.com" text="!"/></th>
							<th rowspan="2" style="vertical-align:middle;"><spring:message code="awb.channel" text="!"/></th>
						</tr>
						
						<tr>
							<th class="text-right"><spring:message code="awb.list.charge" text="!"/></th>
							<th class="text-right text-nowrap"><spring:message code="awb.list.charge.agent" text="!"/></th>
							<th class="text-right text-nowrap"><spring:message code="awb.list.charge.carrier" text="!"/></th>
							<th class="text-right"><spring:message code="awb.list.charge" text="!"/></th>
							<th class="text-right text-nowrap"><spring:message code="awb.list.charge.agent" text="!"/></th>
							<th class="text-right text-nowrap"><spring:message code="awb.list.charge.carrier" text="!"/></th>
						</tr>
					</thead>
					<tbody>
					  	<c:set var="formatPattern" value="#,##0.00"/>
						
						<c:forEach items="${awbs}" var="elm" varStatus="stt">
							<c:choose>
								<c:when test="${elm.cutp eq 'XAF'}">
									<tr>
										<td class="text-center">${stt.index + 1}</td>
										<td class="text-center text-nowrap">
											<a onclick="doEdit('DETAIL','${elm.lta}')" title="<spring:message code="message.view" text="!View"/>"><i class="fa fa-2x fa-edit"></i></a>
											<a onclick="ConformDelete('${elm.lta}')" title="<spring:message code="button.delete" text="!Delete"/>"><i class="fa fa-2x fa-trash-o"></i></a>
										</td>
										<td class="text-nowrap">${elm.tacn}</td>
										<td>${elm.agtn}</td>
										<td>${elm.rdv}</td>
										<td><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateExecute}" /></td>
										<td>${elm.orig}</td>
										<td>${elm.dest}</td>
										<td class="text-right text-nowrap">${elm.weightGross} ${elm.weightIndicator}</td>
										
										<td>${elm.cutp}</td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightPp}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentPp}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierPp}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightCc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentCc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierCc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.comm}"/></td>
										
										<td><spring:message code="awb.list.${elm.channel}" text="!"/></td>
									</tr>
								</c:when>
									
								<c:otherwise>
									<tr>
										<td rowspan="2" class="text-center">${row + (stt.index + 1)}</td>
										<td rowspan="2" class="text-center text-nowrap">
											<a onclick="doEdit('DETAIL','${elm.lta}')" title="<spring:message code="message.view" text="!View"/>"><i class="fa fa-2x fa-edit"></i></a>
											<a onclick="ConformDelete('${elm.lta}')" title="<spring:message code="button.delete" text="!Delete"/>"><i class="fa fa-2x fa-trash-o"></i></a>
										</td>
										<td rowspan="2" class="text-nowrap">${elm.tacn}-${elm.lta}</td>
										<td rowspan="2">${elm.agtn}</td>
										<td rowspan="2">${elm.rdv}</td>
										<td rowspan="2"><fmt:formatDate pattern="dd/MM/yyyy" value="${elm.dateExecute}" /></td>
										<td rowspan="2">${elm.orig}</td>
										<td rowspan="2">${elm.dest}</td>
										<td rowspan="2" class="text-right text-nowrap">${elm.weightGross} <spring:message code="commom.${elm.weightIndicator}" text="!"/></td>
										
										<td>${elm.cutp}</td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightPp}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentPp}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierPp}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightCc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentCc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierCc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.comm}"/></td>
										
										<td rowspan="2"><spring:message code="awb.list.${elm.channel}" text="!"/></td>
									</tr>
									
									<tr>
										<td style="display: none;"></td>
										<td style="display: none;"></td>
										<td style="display: none;"></td>
										<td style="display: none;"></td>
										<td style="display: none;"></td>
										<td style="display: none;"></td>
										<td style="display: none;"></td>
										<td style="display: none;"></td>
										<td style="display: none;"></td>
										<td>${elm.cutpLc}</td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightPpLc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentPpLc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierPpLc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.weightCcLc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.agentCcLc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.carrierCcLc}"/></td>
										<td class="text-right"><fmt:formatNumber pattern="${formatPattern}" value="${elm.commLc}"/></td>
										<td style="display: none;"></td>
									</tr>
								</c:otherwise>
							</c:choose>
							
							
						</c:forEach>
					</tbody>
				</table>
				
				<script type="text/javascript">
				 	$(function () {
				 	 	$('.table-lta').dataTable({
				 	 		"dom": 'T<"clear">lfrtip',
				 	        "sDom": "<'row'<'col-sm-6 text-left'><'col-sm-6 text-right'>r>" + "t" + "<'row'<'col-sm-6'><'col-sm-6'p>>",
				 			"bSort": false,
				 			"paginate": false,
				 			"scrollY": true,
				 			"scrollY": '420px',
				 	        "scrollCollapse": true,
				 	        "scrollX": true
				 	   	}); 
					});
				</script>
			</div>
		</div>
	</form:form>
</div>