<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
    
<script type="text/javascript">
	document.getElementById("report").className = "active";

	function doFilterCassSale() {
		$.ajax({
			url: "<spring:url value='/secure/report/cassSales'/>",
            cache : false,
            success: function (response) {
                $("#filter").html(response);
            }
        });
    };

    function doFilterRmk() {
		$.ajax({
			url: "<spring:url value='/secure/report/rmk'/>",
	        cache : false,
	        success: function (response) {
	            $("#filter").html(response);
	        }
	    });
	};

	function doFilterCaTransport() {
		$.ajax({
			url: "<spring:url value='/secure/report/caTransport'/>",
	        cache : false,
	        success: function (response) {
	            $("#filter").html(response);
	        }
	    });
	};
    
</script>

<div id="body">
    <div class="row">
		<div class="col-lg-12">
			<div class="tabs-container">
		        <ul class="nav nav-tabs">
		            <li class="active" style="background:none;border:none;"><a><spring:message code="report.rapport" text="!"/></a></li>
		            <li><a href="<c:url value="/secure/reportResult"/>"><spring:message code="report.result" text="!"/></a></li>
		        </ul>
		        <div class="tab-content">
		            <div class="tab-pane active">
		                <div class="panel-body">
		                    <table class="table table-striped table-bordered table-hover">
		                        <thead>
		                            <tr>
		                                <th class="text-center">#</th>
		                                <th><spring:message code="report.name" text="!"/></th>
		                                <th class="text-center"><spring:message code="report.filter" text="!"/></th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	<tr>
		                        		<td></td>
		                        		<td colspan="2" class="text-bold" style=" font-style: italic;">
		                        			<strong><spring:message code="menu.lta" text="!"/></strong>
		                        		</td>
		                        	</tr>
		                            <tr>
		                                <td class="text-center">1</td>
		                                <td><spring:message code="report.CASS_SALES" text="!"/></td>
		                                <td class="text-center">
		                                <sec:authorize access="hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_REPORTS', 'ROLE_REPORT_LTA')">
		                                 	<a onclick="javascript:doFilterCassSale()" title="Filter" data-toggle="modal" data-target="#formFilter">
												<i class="fa fa-bars"></i>
											</a>
		                                </sec:authorize>
			                               
		                                </td>
		                            </tr>
		                            <tr>
		                            	<td></td>
		                        		<td colspan="2" class="text-bold" style=" font-style: italic;">
		                        			<strong><spring:message code="menu.manifest" text="!"/></strong>
		                        		</td>
		                        	</tr>
		                            <tr>
		                                <td class="text-center">2</td>
		                                <td><spring:message code="report.RMK" text="!"/></td>
		                                <td class="text-center">
		                                	<sec:authorize access="hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_REPORTS', 'ROLE_REPORT_MANIFEST')">
			                                	<a onclick="javascript:doFilterRmk()" title="Filter" data-toggle="modal" data-target="#formFilter">
													<i class="fa fa-bars"></i>
												</a>
											</sec:authorize>
		                                </td>
		                            </tr>
		                            <tr>
		                                <td class="text-center">3</td>
		                                <td><spring:message code="report.CA_TRANSPORT" text="!"/></td>
		                                <td class="text-center">
			                                <sec:authorize access="hasAnyRole('ROLE_DW', 'ROLE_ADMIN', 'ROLE_REPORTS', 'ROLE_REPORT_MANIFEST')">
			                                	<a onclick="javascript:doFilterCaTransport()" title="Filter" data-toggle="modal" data-target="#formFilter">
													<i class="fa fa-bars"></i>
												</a>
											</sec:authorize>
		                                </td>
		                            </tr>
		                            <tr>
		                            <td></td>
		                        		<td colspan="2" class="text-bold" style=" font-style: italic;">
		                        			<strong><spring:message code="menu.stock" text="!"/></strong>
		                        		</td>
		                        	</tr>
		                        </tbody>
		                    </table>    
		                </div>
		            </div>
		        </div>
			</div>
		</div>
	</div>
	
	<!-- Modal create -->
	<div class="modal inmodal" id="formFilter" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" >
			<div class="modal-content animated bounceInRight" style="width: 850px;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x fa-times-circle"></i></span></button>
					<h5 class="modal-title"><spring:message code="report.filter" text="!"/></h5>
				</div>
				<div class="modal-body">
					<div id="filter"></div>
				</div>
			</div>
		</div>
	</div>
</div>