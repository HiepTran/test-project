<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style>
	.chosen-container-single .chosen-single {
         background: none;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
</style>

<script type="text/javascript">
	function doEditForm(action, id){
		$.ajax({
			url: "<spring:url value='/secure/agent/error'/>",
			type: "POST",
			data: $('#form_modify').serialize(),
			 success: function(result){
		        	if(result.errCodeEdit > 0){
			           	 $('#err_phone').html('');
			           	 $('#err_fax').html('');
			           	 $('#err_email').html('');
			           	 $('#err_comm').html('');
			           	 var len = $.map(result.lstErr, function(n, i) { return i; }).length;
			           	 for(var i=0;i<len;i++)
		               	 {
		                    $('#err_'+result.lstErr[i].propertyName).html(result.lstErr[i].message); 
		              	 }
		        	}else{
		        		document.forms[1].elements['action'].value=action;
		        		document.forms[1].elements['id'].value=id;
		        		document.forms[1].submit();
		        	}
		        },
			 	error: function(XMLHttpRequest, textStatus, errorThrown){}
		});
	}
</script>

<form:form name="form_modify" id="form_modify" method="post" commandName="agentFilterPara" action="agent">
	<input name="action" type="hidden">
	<input name="id" type="hidden">
	
	<div class="panel panel-success" style="margin-top:9px;">
         <div class="panel-heading" style="padding: 4px;">
             <div class="row">
                 <div class="col-lg-11">
                     <h3><spring:message code="message.modify" text="!Modify"/> ${agent.code }</h3>
                 </div>
                 <div class="col-lg-1 text-right">
                     <i class="fa fa-2x fa-times-circle" onclick="javascript:hide('edit-box');" style="cursor:pointer;"></i>
                 </div>
             </div>
         </div>
		
         <div class="panel-body" style="padding:5px;">
           	<div class="form-group">
				<div class="row">
					<div class="col-lg-3">
						<label><spring:message code="agent.code" text="!"/></label>
						<form:input path="agtnCode" value="${fn:trim(agent.code)}" readonly="true" class="form-control textfield"></form:input>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="agent.name" text="!"/></label>
						<form:input path="name" value="${fn:trim(agent.name)}" class="form-control textfield"></form:input>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="agent.address" text="!"/></label>
						<form:input path="address1" value="${fn:trim(agent.address1)}" class="form-control textfield"></form:input>
					</div>
					
					<div class="col-lg-3">
						<label><spring:message code="agent.kind" text="!"/></label>
					 	<form:select path="kind" class="form-control">
					 		<option value="CA" <c:if test="${'CA' == agent.kind}">selected="selected"</c:if>>CA</option>
							<option value="TR" <c:if test="${'TR' == agent.kind}">selected="selected"</c:if>>TR</option>
						</form:select>
					</div>	
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-lg-3">
						<label><spring:message code="agent.city" text="!"/></label>
						<form:input path="city" value="${fn:trim(agent.city)}" class="form-control textfield"></form:input>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="agent.zip" text="!"/></label>
						<form:input path="zip" value="${fn:trim(agent.zip)}" class="form-control textfield"></form:input>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="agent.country" text="!"/></label>
					 	<form:select path="isoc" class="form-control">
					 		<c:forEach items="${listIsoc}" var="elm">
								<option value="${elm.isoc}"
									<c:if test="${elm.isoc == fn:trim(agent.isoc)}" >selected="selected"</c:if>>
									<spring:message code="isoc.${elm.isoc}" text="notFound"/>
								</option>
							</c:forEach>
						</form:select>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="agent.region" text="!"/></label>
					 	<form:input path="region" value="${fn:trim(agent.region)}" class="form-control textfield"></form:input>
					</div>	
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-lg-3">
					 	<label><spring:message code="agent.phone" text="!"/></label>
					 	<form:input path="phone" value="${fn:trim(agent.phone)}" class="form-control textfield"></form:input>
					 	<label id="err_phone" class="text-danger"></label>
				 	</div>
					<div class="col-lg-3">
					 	<label><spring:message code="agent.fax" text="!"/></label>
					 	<form:input path="fax" value="${fn:trim(agent.fax)}" class="form-control textfield"></form:input>
					 	<label id="err_fax" class="text-danger"></label>
					</div>
					<div class="col-lg-3">
					 	<label><spring:message code="agent.email" text="!"/></label>
					 	<form:input path="email" value="${fn:trim(agent.email)}" class="form-control textfield"></form:input>
					 	<label id="err_email" class="text-danger"></label>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="agent.iata.ind" text="!"/></label>
					 	<form:select path="iataInd" class="form-control">
							<option value="1" <c:if test="${'1' == agent.iataInd}">selected="selected"</c:if>><spring:message code="agent.yes" text="!"/></option>
							<option value="0" <c:if test="${'0' == agent.iataInd}">selected="selected"</c:if>><spring:message code="agent.no" text="!"/></option>
						</form:select>
					</div>
				</div>
			</div>
				
			<div class="form-group">
				<div class="row">
				 	<div class="col-lg-3">
					 	<label><spring:message code="agent.comm" text="!"/></label>
					 	<form:input path="comm" value="${agent.comm}" class="form-control textfield"></form:input>
					 	<label id="err_comm" class="text-danger"></label>
					 </div>
				 </div>
			 </div>
							 
			<div class="row">
				<div class="col-lg-12 text-right">
		  			<a onclick="doEditForm('UPDATE','${agent.code}')" class="btn btn-w-m btn-success text-uppercase" type="submit">
		  				<i class="fa fa-check"></i> <spring:message code="button.go" text="!Go"/>
		  			</a>
				</div>
			</div>
         </div>
     </div>
</form:form>