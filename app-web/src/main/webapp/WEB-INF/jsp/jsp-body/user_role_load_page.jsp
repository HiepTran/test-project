<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<style type="text/css">
	.onoffswitch-label {
            border: 2px solid #1a7bb9;
            border-radius: 3px;
            cursor: pointer;
            display: block;
            overflow: hidden;
        }

        .onoffswitch-switch {
            background: #ffffff none repeat scroll 0 0;
            border: 2px solid #1a7bb9;
            border-radius: 3px;
            bottom: 0;
            display: block;
            margin: 0;
            position: absolute;
            right: 36px;
            top: 0;
            transition: all 0.3s ease-in 0s;
            width: 18px;
        }

        .onoffswitch-inner::before {
            background-color: #1a7bb9;
            color: #ffffff;
            content: "ON";
            padding-left: 10px;
            text-align: left;
        }

        .onoffswitch-inner::after {
            background-color: #ffffff;
            color: #1a7bb9;
            content: "OFF";
            padding-right: 6px;
            text-align: right;
        }
</style>

<table class="table table-striped table-bordered table-hover table-manifest" style="width:100%;">
	<thead>
		<tr>
			<th class="text-center">#</th>
			<sec:authorize access="hasAnyRole('ROLE_DW', 'ROLE_ADMIN')">
				<th class="text-center"><spring:message code="commom.action" text="!Action"/></th>
			</sec:authorize>
			<th><spring:message code="user.table.username" text="!Username"/></th>
			<sec:authorize access="hasAnyRole('ROLE_DW', 'ROLE_ADMIN')">
				<th><spring:message code="user.table.password" text="!Password"/></th>
			</sec:authorize>
			<th><spring:message code="user.table.role" text="!Role"></spring:message>
			<th><spring:message code="user.countrycode" text="!Role"></spring:message>
			<th><spring:message code="user.table.name" text="!Name"/></th>
			<th><spring:message code="user.table.telephone" text="!Telephone"/></th>
			<th><spring:message code="user.table.email" text="!Email"/></th>
			<th><spring:message code="user.table.create" text="!Create by"/></th>
			<th><spring:message code="user.table.active" text="!Active"/></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${lstUser}" var="elm" varStatus="stt">
			<tr id=${elm.sysUserId }>
				<td class="text-center">${stt.index + 1}</td>
				<sec:authorize access="hasAnyRole('ROLE_DW', 'ROLE_ADMIN')">
					<td class="text-center text-nowrap">
						<sec:authorize access="hasRole('ROLE_DW')">
							<a onclick='doEditUser("${elm.sysUserId}")' data-toggle="modal" data-target="#formEdit"
								title="<spring:message code="user.table.modify" text="!Modify user"/>"><i class="fa fa-2x fa-edit"></i>
							</a>
						</sec:authorize>
						
						<sec:authorize access="hasRole('ROLE_ADMIN')">
							<c:choose>
								<c:when test="${elm.checkAdmin == true }">
									<i class="fa fa-2x fa-edit" style="color: #e7eaec"></i>
								</c:when>
								<c:otherwise>
									<a onclick='doEditUser("${elm.sysUserId}")' data-toggle="modal" data-target="#formEdit"
										title="<spring:message code="user.table.modify" text="!Modify user"/>"><i class="fa fa-2x fa-edit"></i>
									</a>
								</c:otherwise>
							</c:choose>
						</sec:authorize>
					</td>
				</sec:authorize>
				
				<td>${elm.username }</td>
				<sec:authorize access="hasRole('ROLE_DW')">
					<td>${elm.password }</td>
				</sec:authorize>
				<sec:authorize access="hasRole('ROLE_ADMIN')">
					<td>**********</td>
				</sec:authorize>
				
				<td style="white-space: nowrap;">
					<c:forEach items="${elm.roles }" var="elmRole">
						<c:choose>
							<c:when test="${!elmRole.authority.endsWith('_C') && !elmRole.authority.endsWith('_U') && !elmRole.authority.endsWith('_D') }">
								${elmRole.authority }<br/>
							</c:when>
						</c:choose>
					</c:forEach>
				</td>
				<td>${elm.countryCode }</td>
				<td>${elm.name }</td>
				<td>${elm.telephone }</td>
				<td>${elm.email }</td>
				<td>${elm.modifiedBy } - <fmt:formatDate pattern="dd/MM/yyyy" value="${elm.modifiedOn }"/></td>
				<td class="text-center">
					<sec:authorize access="hasRole('ROLE_DW')">
						<c:if test="${elm.checkDW == false }">
							 <div class="switch">
								<div class="onoffswitch">
								    <input type="checkbox" onchange="doChangeStatus(${elm.sysUserId })" class="onoffswitch-checkbox" id="statusActive_${stt.index}">
								    <label class="onoffswitch-label" for="statusActive_${stt.index}">
								        <span class="onoffswitch-inner"></span>
								        <span class="onoffswitch-switch"></span>
								    </label>
								</div>
							</div>
						</c:if>
					</sec:authorize>
					<sec:authorize access="hasRole('ROLE_ADMIN')">
						<c:if test="${elm.checkAdmin == false }">
							<div class="switch">
								<div class="onoffswitch text-center">
								    <input type="checkbox" onchange="doChangeStatus(${elm.sysUserId })" class="onoffswitch-checkbox" id="statusActive_${stt.index}">
								    <label class="onoffswitch-label" for="statusActive_${stt.index}">
								        <span class="onoffswitch-inner"></span>
								        <span class="onoffswitch-switch"></span>
								    </label>
								</div>
							</div>
						</c:if>
					</sec:authorize>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready(function(){
	    
	    <c:forEach items="${lstUser}" var="elm" varStatus="stt">
		    var check = ${elm.isEnable};
		    if(check == true){
		    	$('#statusActive_${stt.index}').prop('checked', true);
		    }
	    </c:forEach>
	});

	function doChangeStatus(userId){
		$.ajax({
			url: "<spring:url value='/secure/user/changestatus/" + userId + "'/>", type: "POST",
			cache: false,
			success: function (response) {
				$("#_results").html(response);
		    }
		});
	}
</script>