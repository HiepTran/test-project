<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<style>
	.chosen-container-single .chosen-single {
         background: none;
         border-radius: 3px;
         color: #444;
         display: block;
         height: 23px;
         line-height: 24px;
         overflow: hidden;
         padding: 0 0 0 8px;
         position: relative;
         text-decoration: none;
         white-space: nowrap;
         box-shadow: none;
         border: 1px solid #e5e6e7;
     }
</style>

<script type="text/javascript">
	function doEditForm(action, id){
		document.forms[1].elements['action'].value=action;
		document.forms[1].elements['id'].value=id;
		document.forms[1].submit();
	}

	$(document).ready(function() {

		$('.input-group.date').datepicker({
			todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
			format: "dd/mm/yyyy"
	    });
	    $('.chosen-select').chosen({disable_search_threshold: 10});
	});
	
</script>

<form:form name="form_modify" id="form_modify" method="post" commandName="taxFilter" action="tax">
	<input name="action" type="hidden">
	<input name="id" type="hidden">
	
	<div class="panel panel-success" style="margin-top:9px;">
         <div class="panel-heading" style="padding: 4px;">
             <div class="row">
                 <div class="col-lg-11">
                     <h3><spring:message code="message.modify" text="!Modify"/> ${tax.code }</h3>
                 </div>
                 <div class="col-lg-1 text-right">
                     <i class="fa fa-2x fa-times-circle" onclick="javascript:hide('edit-box');" style="cursor:pointer;"></i>
                 </div>
             </div>
         </div>
		
         <div class="panel-body" style="padding:5px;">
           	<div class="form-group">
				<div class="row">
					<div class="col-lg-3">
						<label><spring:message code="tax.country" text="!"/></label>
						<input value="${tax.isoc}" readonly="readonly" class="form-control textfield"/>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="tax.code" text="!"/></label>
						<input value="${tax.code}" readonly="readonly" class="form-control textfield"/>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="tax.sub.code" text="!"/></label>
						<input value="${tax.subCode}" readonly="readonly" class="form-control textfield"/>
					</div>
					
					<div class="col-lg-3">
						<label><spring:message code="tax.item" text="!"/></label>
					 	<form:input path="itemEdit" value="${tax.item}" class="form-control textfield"/>
					</div>	
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-lg-3">
						<label><spring:message code="tax.currency" text="!"/></label>
						<form:select path="cutpEdit" class="form-control">
							<option value="CHF" <c:if test="${'CHF' == tax.cutp}">selected="selected"</c:if>>CHF</option>
							<option value="EUR" <c:if test="${'EUR' == tax.cutp}">selected="selected"</c:if>>EUR</option>
							<option value="NGN" <c:if test="${'NGN' == tax.cutp}">selected="selected"</c:if>>NGN</option>
							<option value="USD" <c:if test="${'USD' == tax.cutp}">selected="selected"</c:if>>USD</option>
							<option value="XAF" <c:if test="${'XAF' == tax.cutp}">selected="selected"</c:if>>XAF</option>
							<option value="XOF" <c:if test="${'XOF' == tax.cutp}">selected="selected"</c:if>>XOF</option>
						</form:select>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="tax.unit" text="!"/></label>
						<spring:message code="tax.${tax.unit}" text="!" var = "unitEdit"/>
						<form:select path="unitEdit" class="form-control">
							<option value="A" <c:if test="${'A' == tax.unit}">selected="selected"</c:if>>
								<spring:message code="tax.A" text="!"/>
							</option>
							<option value="K" <c:if test="${'K' == tax.unit}">selected="selected"</c:if>>
								<spring:message code="tax.K" text="!"/>
							</option>
							<option value="P" <c:if test="${'P' == tax.unit}">selected="selected"</c:if>>
								<spring:message code="tax.P" text="!"/>
							</option>
						</form:select>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="tax.amount" text="!"/></label>
						<form:input path="amountEdit" value="${tax.amount}" class="form-control textfield text-right"></form:input>
					</div>
					<div class="col-lg-3">
						<label><spring:message code="tax.description" text="!"/></label>
						<form:input path="descriptionEdit" value="${tax.description}" class="form-control textfield"></form:input>
					</div>
				</div>
			</div>
				
					 
			<div class="row">
				<div class="col-lg-12 text-right">
		  			<a onclick="doEditForm('UPDATE','${tax.id}')" class="btn btn-w-m btn-success text-uppercase" type="submit">
		  				<i class="fa fa-check"></i> <spring:message code="button.valider" text="!"/>
		  			</a>
				</div>
			</div>
         </div>
     </div>
</form:form>