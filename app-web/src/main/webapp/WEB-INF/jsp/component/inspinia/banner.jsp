<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="row border-bottom">
	<nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
		<div class="navbar-header">
		    <a class="navbar-minimalize minimalize-styl-2 btn btn-success" href="#"><i class="fa fa-bars"></i> </a>
		</div>
	    <ul class="nav navbar-top-links navbar-right">
	    	<li>
	            <span class="m-r-sm font-bold" style="font-size: 15px;"><spring:message code="title.platform" text="!OpenSky Platform"/></span>
	        </li>
	            
	        <li class="dropdown">
	        	<a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" style="font-weight:400;">
	          		<c:choose>
						<c:when test="${fn:trim(sysUser.language)=='en'}">
							<span><img alt="image" src="<c:url value="/static/images/lang_en.png"/>"/></span>&nbsp;&nbsp;English
						</c:when>
						<c:otherwise>
							 <span><img alt="image" src="<c:url value="/static/images/lang_fr.png"/>" /></span>&nbsp;&nbsp;Fran&ccedil;ais
						</c:otherwise>
					</c:choose>
	              </a>
	              <ul class="dropdown-menu dropdown-alerts" style="width:150px;">
	              	<c:choose>
						<c:when test="${fn:trim(sysUser.language)=='en'}">
							<li>
		                      <a href="?lang=fr">
		                      		<span><img alt="image" src="<c:url value="/static/images/lang_fr.png"/>"/></span>&nbsp;&nbsp;Fran&ccedil;ais
		                      </a>
		                  	</li> 
						</c:when>
						<c:otherwise>
						  	<li>
			                   <a href="?lang=en">
			                   		<span><img alt="image" src="<c:url value="/static/images/lang_en.png"/>"/></span>&nbsp;&nbsp;English
			                   </a>
			               </li> 
						</c:otherwise>
					</c:choose>                  
	             </ul>
	        </li>
	        <li class="dropdown">
	        	<a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" style="font-weight:400;">
	            	<i class="fa fa-user"></i> ${fn:trim(sysUser.username)}
	        	</a>
	            <ul class="dropdown-menu dropdown-alerts" style="width:250px;">
	                 <li>
		                 <a>
		                 	<i class="fa fa-clock-o fa-fw"></i> <spring:message code="menu.last.login" text="!Last login"/>
		                 	<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:formatDate pattern="dd/MM/yyyy hh:mm a" value="${sysUser.lastLoginDate }" />
		                 </a>                 
	                 </li>
	                 <li class="divider"></li>
	               	<li>
						<a style="text-align: right;" href="<c:url value="/logout"/>">
							<i class="fa fa-sign-out"></i> <spring:message code="menu.deconnexion" text="!Logout" />
						</a>
					</li>
	            </ul>
	       </li>
	    </ul>
	</nav>
</div>		