<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>


<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav" id="side-menu">
            <%-- <li class="nav-header">
				<div class="dropdown profile-element"> 
					<span><img alt="image" src="<c:url value="/static/images/logobig.png"/>"/></span>
				</div>
				<div class="logo-element">
					<span><img alt="image" src="<c:url value="/static/images/logosmall.png"/>"/></span>
				</div>
			</li> --%>
			<li class="nav-header">
                <div class="dropdown profile-element">
                 <span>
                     <img alt="image" class="img-circle" style="margin-left: 30%;" src="<c:url value='/static/images/profile_small_dw.jpg'></c:url>" />
                    </span>
                </div>
                <div class="logo-element">                    
                    <img alt="image" class="img-circle" src="<c:url value='/static/images/profile_small_48.jpg'></c:url>" />
                </div>
            </li>
            <li id="dashboard">
             	<a href="<c:url value="/secure/dashboard"/>">
             		<i class="fa fa-th-large"></i><span class="nav-label"><spring:message code="menu.dashboard" text="!"/></span>
             	</a>
			</li>
			<li id="lta">
				<a href="#">
					<i class="fa fa-files-o"></i><span class="nav-label"><spring:message code="menu.lta" text="!"/></span><span class="fa arrow"></span>
				</a>
				<ul class="nav nav-second-level">
					<li id="listLTA">
		             	<a href="<c:url value="/secure/awb"/>">
		             		<i class="fa fa-list-ul"></i><span class="nav-label"><spring:message code="menu.list.lta" text="!"/></span>
		             	</a>
					</li>
					<li id="create">
		             	<a href="<c:url value="/secure/rdv"/>">
		             		<i class="fa fa-edit"></i><span class="nav-label"><spring:message code="menu.create.lta" text="!"/></span>
		             	</a>
					</li>
					<li id="loadAwb">
		             	<a href="<c:url value="/secure/awb/load"/>">
		             		<i class="fa fa-database"></i><span class="nav-label"><spring:message code="menu.awb.load" text="!"/></span>
		             	</a>
					</li>
				</ul>
			</li>
			
			<li id="manifest">
				<a href="#">
					<i class="fa fa-plane"></i><span class="nav-label"><spring:message code="menu.manifest" text="!"/></span><span class="fa arrow"></span>
				</a>
				<ul class="nav nav-second-level">
					<li id="listManifest">
		             	<a href="<c:url value="/secure/flown"/>">
		             		<i class="fa fa-list-ul"></i><span class="nav-label"><spring:message code="menu.transport" text="!"/></span>
		             	</a>
					</li>
					<li id="createManifest">
		             	<a href="<c:url value="/secure/manifest"/>">
		             		<i class="fa fa-edit"></i><span class="nav-label"><spring:message code="menu.create.manifest" text="!"/></span>
		             	</a>
					</li>
				</ul>
			</li>
			
			<li id="report">
             	<a href="<c:url value="/secure/report"/>">
             		<i class="fa fa-file-excel-o"></i><span class="nav-label"><spring:message code="menu.report" text="!"/></span>
             	</a>
			</li>
			
			<li id="stock">
                <a href="#"><i class="fa fa-university"></i> <span class="nav-label"><spring:message code="menu.stock" text="!"/> </span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li id="stock_compagnie">
                        <a href="#"><i class="fa fa-user"></i><spring:message code="menu.stock.compagnie" text="!"/> <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li id="stock_compagnie_control">
                                <a href="<c:url value="/secure/stock/compagnie"/>"><i class="fa fa-caret-right "></i><spring:message code="menu.stock.replenishment" text="!"/></a>
                            </li>
                            <li id="stock_compagnie_agent">
                                <a href="<c:url value="/secure/stock/compagnie/agent"/>"><i class="fa fa-caret-right "></i><spring:message code="menu.stock.agent" text="!"/></a>
                            </li>

                        </ul>
                    </li>
                    <li id="stock_transitaire">
                        <a href="#"><i class="fa fa-user"></i><spring:message code="menu.stock.transitaire" text="!"/> <span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li id="stock_transitaire_control">
                                <a href="<c:url value="/secure/stock/transitaire"/>"><i class="fa fa-caret-right "></i><spring:message code="menu.stock.replenishment" text="!"/></a>
                            </li>
                            <li id="stock_transitaire_agent">
                                 <a href="<c:url value="/secure/stock/transitaire/agent"/>"><i class="fa fa-caret-right "></i><spring:message code="menu.stock.agent" text="!"/></a>
                            </li>

                        </ul>
                    </li>
                </ul>
            </li>
                
			<li id="param">
				<a href="#">
					<i class="fa fa-cog"></i><span class="nav-label"><spring:message code="menu.setting" text="!"/></span><span class="fa arrow"></span>
				</a>
				<ul class="nav nav-second-level">
					<li id="param_agent">
						<a href="<c:url value="/secure/agent"/>">
	                     	<i class="fa fa-user"></i> <spring:message code="menu.agent" text="!"/>
	                    </a>
					</li>
					<li id="param_agentgsa">
						<a href="<c:url value="/secure/agentGsa"/>">
	                     	<i class="fa fa-user-secret"></i> <spring:message code="menu.agentgsa" text="!Agent Gsa"/>
	                    </a>
					</li>
					<li id="param_tax">
						<a href="<c:url value="/secure/tax"/>">
	                     	<i class="fa fa-ticket"></i> <spring:message code="menu.tax" text="!"/>
                     	</a>
					</li>
					<li id="param_line">
						<a href="<c:url value="/secure/line"/>">
	                     	<i class="fa fa-road"></i> <spring:message code="menu.line" text="!"/>
                     	</a>
					</li>
					<li id="param_legkm">
						<a href="<c:url value="/secure/legkm"/>">
	                     	<i class="fa fa-fighter-jet"></i> <spring:message code="menu.legkm" text="!LegKm"/>
                     	</a>
					</li>
					<li id="param_bsr">
						<a href='<c:url value="/secure/bsr"/>'>
							<i class="fa fa-money"></i> <spring:message code="menu.bsr" text="!Bsr"></spring:message>
						</a>
					</li>
				</ul>
			</li>
			
			<sec:authorize access="hasAnyRole('ROLE_DW', 'ROLE_ADMIN')">
				<li id="userrole">
					<a href="#">
						<i class="fa fa-users"></i><span class="nav-label"><spring:message code="menu.userrole" text="!UserRole"/></span><span class="fa arrow"></span>
					</a>
					<ul class="nav nav-second-level">
						<li id="userrole_user">
							<a href="<c:url value="/secure/user"/>">
		                     	<i class="fa fa-user"></i> <spring:message code="menu.userrole.user" text="!User"/>
		                    </a>
						</li>
						<sec:authorize access="hasRole('ROLE_DW')">
							<li id="userrole_role">
								<a href="<c:url value="/secure/role"/>">
			                     	<i class="fa fa-ticket"></i> <spring:message code="menu.userrole.role" text="!Role"/>
		                     	</a>
							</li>
						</sec:authorize>
					</ul>
				</li>
			</sec:authorize>
         </ul>
	</div>
</nav>