<div id="footer" class="footer">                    
	<div>
		Powered by <strong>DataWings</strong> <span id="copyrightYear"></span> | <a href="mailto:sales@data-wings.com">Sales</a> | 
		<a href="mailto:support@data-wings.com">Support</a>
	</div>
</div>
<script type="text/javascript">
	function setCurrentDate() {
		var currentdate = new Date();
		var datetime = "� 2007 - " + currentdate.getFullYear();
		$('#copyrightYear').text(datetime);
	}

	 $(document).ready(function() {
        	setCurrentDate();
	 });  	
</script>