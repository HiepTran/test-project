(function($) {
	$.fn.dwselect = function(options) {
		//var opts = $.extend($.fn.dwselect.defaults, options);
		$(document).mousedown(checkExternalClick);
		$(document).keydown(checkOuterKeydown);
		return this.each(function(){
			var sl = $(this);
			if (!sl.is('select')){
				//sl.after('<span style="color: blue;">This is not select element</span>');
				return;
			}
			var _options = $('option', this);
			var _opSize = _options.length;

			var _dwSelect = $('<div class="nav-dw-select form-control">'+ sl.find('option:selected').text() +'</div>');
			//var _span = $('<span class="nav_select_bridge">xx</span>');
			var _divOuter = $('<div class="nav_select_outer nav_disable"></div>');
			if ($.fn.jquery >= $.fn.dwselect.jversion)
				/*_divOuter.disableSelection();*/

			var arr_size = 3;
			
			if (_opSize == 0) {
				return;
			} else if (_opSize > 0 && _opSize < 4) {
				// 1, 2, 3
				arr_size = 1;
			} else if (_opSize > 3 && _opSize < 11) {
				// 4, 5, 6, 7, 8, 9, 10
				arr_size = 2;
			}

			var col_length = Math.ceil(_opSize/arr_size);

			var _inner_div = $('<div class="nav_select_inner"></div>');
			for(var i = 0; i < arr_size ; i++){
				var col = $('<div class="nav_select_col"></div>');
				var col_ul = $('<ul></ul>');
				for (var j = 0; (j < col_length && (i*col_length + j)<_options.length); j++) {
					var crr_option_node = $(_options[i*col_length + j]);
					if (crr_option_node.is(':selected'))
						col_ul.append('<li id="'+crr_option_node.val()+'" class="item-selected"><a>'+crr_option_node.text()+'</a></li>');
					else
						col_ul.append('<li id="'+crr_option_node.val()+'"><a>'+crr_option_node.text()+'</a></li>');
				}
				col.append(col_ul);
				_inner_div.append(col);
				if(i == arr_size -1) {
					col.addClass('last-col');
				}
			}
			_inner_div.append("<div id='clear'></div>");
			_divOuter.append(_inner_div);
			sl.hide();
			sl.after(_dwSelect);
			if ($.fn.jquery >= $.fn.dwselect.jversion)
			/* _dwSelect.disableSelection();*/
			_dwSelect.after(_divOuter);
			_inner_div.mCustomScrollbar({scrollButtons:{enable:true},theme:"dark-thick"});

			_dwSelect.click(onClick);

			_divOuter.keydown(function(){
				console.log('hello...');
			});
			$('div.nav_select_col ul li', _inner_div).mouseenter(liMouseEnter);
			$('div.nav_select_col ul li', _inner_div).mouseleave(liMouseleave);
			$('div.nav_select_col', _inner_div).mouseleave(divMouseLeave);
			$('div.nav_select_col ul li', _inner_div).click(liClick);
		});
	};

	$.fn.dwselect._curInst = null;
	$.fn.dwselect.jversion = '1.4.4';
	//$.fn.dwselect.defaults = {height : 100, width : 100, cols : 2};

	function checkOuterKeydown(e){
	}

	function liClick(){
		var prt = $(this).parents('.nav_select_outer');
		prt.siblings('.nav-dw-select').text($(this).text());
		//select doing..
		var slct = prt.siblings('select');
		slct.val($(this).attr('id'));

		$('.item-selected2', $(this).parents('.nav_select_inner')).removeClass('item-selected2');
		$(this).addClass('item-selected2');
		prt.addClass('nav_disable');
		slct.change();
	}
	function liMouseEnter(){
		$('.item-selected', $(this).parents('.nav_select_inner')).addClass('item-selected2').removeClass('item-selected');
		$(this).addClass('li-over');
	}
	function liMouseleave(){
		$(this).removeClass('li-over');
	}
	function divMouseEnter(){
		$('li', $(this)).addClass('outer-enter');
	}
	function divMouseLeave(){
		$('.item-selected2', $(this).parents('.nav_select_inner')).addClass('item-selected').removeClass('item-selected2');
	}
	function hidedwselect(){
		$('.nav_select_outer').addClass('nav_disable');
	}
	function hidedwselectSub_curInst(){
		$('.nav_select_outer').each(function() {
			if(!$(this).hasClass('curent-select'))
				$(this).addClass('nav_disable');
		});
	}
	function onClick(){
		var _divOuter = $(this).siblings('.nav_select_outer');
		$('.nav_select_outer').removeClass('curent-select');
		_divOuter.addClass('curent-select');
		hidedwselectSub_curInst();
		var _dWidth = $(document).width();
		var _dHeight = $(document).height();
		//0
		var overflow_elements = ($(this).parents('div[style*="overflow"]'));
			if (overflow_elements.length > 0){
				for (var i = 0; i < overflow_elements.length; i++){
					var elm = overflow_elements[i];
					if ($(elm).css('overflow')=='hidden') {
						_dWidth = $(elm).width()+$(elm).offset().left + 20;
						break;
					} else {
						continue;
					}
				}
			}
		//
		_divOuter.css('left', 'auto');
		_divOuter.css('top', 'auto');
		_divOuter.toggleClass('nav_disable');
		if (!_divOuter.hasClass('nav_disable')){
			var innerDiv = $('div.nav_select_inner',_divOuter);
			var outerWidth=2;
			$('div.nav_select_col',innerDiv).each(function(){
				outerWidth += $(this).width();
			});
			var scroll = false;
			if($('div.nav_select_col:first-child',innerDiv).height()>innerDiv.height()) {
				scroll=true;
				outerWidth += 30;
			}
			if((navigator.userAgent).indexOf("MSIE") > 0 || (navigator.userAgent).indexOf(".NET") > 0)
				outerWidth += 5;
			innerDiv.css('width',outerWidth);
			if (_divOuter.width() + _divOuter.offset().left > _dWidth){
				if (_divOuter.width() < _dWidth) {
					_divOuter.css('left', (_dWidth - _divOuter.width()-_divOuter.offset().left - 10));
				}
			}
			if (_divOuter.height() + _divOuter.position().top > _dHeight){
				if (_divOuter.height() < (_divOuter.position().top - $(this).height())) {
					_divOuter.css('top', (_divOuter.position().top - $(this).height() - _divOuter.height() - 21));
				}
			}
			if (scroll)
				innerDiv.mCustomScrollbar('update');
		}
	}
	function checkExternalClick(event) {
		var $target = $(event.target);
		if (!$target.hasClass('nav-dw-select') && $target.parents('.nav_select_outer').length == 0 &&
				!$target.hasClass('nav_select_outer') &&
				!$target.hasClass('nav_select_bridge')) {
			hidedwselect();
		}
	}
/*	$.fn.select = function(options) {
		var settings = $.extend({}, {open: false}, options);
		return this.each(function() {
			var dts = $(this).children('dt');
			dts.click(onClick);
			dts.each(reset);
			if(settings.open) $(this).children('dt:first-child').next().show();
		});
		
		function onClick() {
			$(this).siblings('dt').each(hide);
			$(this).next().slideDown('fast');
			return false;
		}
		
		function hide() {
			$(this).next().slideUp('fast');
		}
		
		function reset() {
			$(this).next().hide();
		}
	};*/
})(jQuery);