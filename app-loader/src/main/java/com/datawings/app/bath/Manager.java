package com.datawings.app.bath;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.datawings.app.loader.AwbLoader;
import com.datawings.app.report.MainReport;

public class Manager {
private static final Log log = LogFactory.getLog(Manager.class);
	
	public Manager() {
	}

	public static void main(String[] args) throws Throwable {
		if (args.length != 1 && args.length != 2) {
			log.info("*********************ERROR**************************");
			System.exit(-1);
		}
		
		if(args.length == 1){
			AwbLoader loader = new AwbLoader();
			loader.main(args);
		}
		
		if(args.length == 2){
			MainReport report = new MainReport();
			report.main(args);
		}
	}
}
