package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.filter.Rmk;
import com.datawings.app.model.Awb;
import com.datawings.app.model.AwbFlown;
import com.datawings.app.model.Report;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.service.IAwbFlownService;
import com.datawings.app.service.IAwbService;
import com.datawings.app.service.IReportService;

public class MainReport {
	private static final Log log = LogFactory.getLog(MainReport.class);
	
	@Autowired
	private IReportService reportService;
	
	@Autowired
	private IAwbService awbService;
	
	@Autowired
	private IAwbFlownService manifestDetailService;
	
	public MainReport() {
		loadApplicationContext(new String[] {"classpath:/loader-context.xml"});
	}
	
	@SuppressWarnings("resource")
	public void loadApplicationContext(String[] context) {
		ConfigurableApplicationContext applicationContext = new ClassPathXmlApplicationContext(context);
		applicationContext.registerShutdownHook();
		applicationContext.getBeanFactory().autowireBeanProperties(this, AutowireCapableBeanFactory.AUTOWIRE_NO, false);
	}
	
	public void main(String[] args) throws IOException {
		Integer id = Integer.parseInt(args[1]);
		Report report = reportService.find(id);
		
		if(StringUtils.equals(report.getName(), CargoUtils.CASS_SALES)){
			try {
				ReportFilter filter = new ReportFilter();
				String isoc  = report.getFilter().split(";")[0].split(":")[1];
				String agtn  = report.getFilter().split(";")[1].split(":")[1];
				String dateFrom = report.getFilter().split(";")[2].split(":")[1];
				String dateTo = report.getFilter().split(";")[3].split(":")[1];
				String channel  = report.getFilter().split(";")[4].split(":")[1];
				
				filter.setIsoc(isoc);
				filter.setAgtn(agtn);
				filter.setDateFrom(dateFrom);
				filter.setDateTo(dateTo);
				filter.setChannel(channel);
				
				List<Awb> awbs = awbService.reportSalesCass(filter);
				
				SalesCassExcel salesCass = new SalesCassExcel();
				ByteArrayOutputStream baos = salesCass.makeExcel(awbs, filter);
				baos.writeTo(new FileOutputStream(report.getLocal() + report.getFileName()));
				baos.flush();
				baos.close();
				
				report.setStatus("F");
			} catch (Exception e) {
				log.info("********************* ERROR EXCEL SALES CASS**************************");
				report.setStatus("E");
			}
			reportService.merge(report);
		}else if(StringUtils.equals(report.getName(), CargoUtils.RMK)){
			try {
				ReportFilter filter = new ReportFilter();
				String month  = report.getFilter().split(";")[0].split(":")[1];
				filter.setDateFrom(month);
				
				Date current =  DateUtil.string2Date(month, "MM/yyyy");
				String year = StringUtils.substring(month, 3, 7);
				String beginYear = "01/01/" + year;
				String endYear =DateUtil.date2String(DateUtil.endMonth(current), "dd/MM/yyyy");
				
				
				List<Rmk> rmks = manifestDetailService.reportRmk(filter, beginYear, endYear);
				
				RmkExcel rmk = new RmkExcel();
				ByteArrayOutputStream baos = rmk.makeExcel(rmks, filter);
				baos.writeTo(new FileOutputStream(report.getLocal() + report.getFileName()));
				baos.flush();
				baos.close();
				
				report.setStatus("F");
			}catch (Exception e) {
				log.info("********************* ERROR EXCEL RMK**************************");
				report.setStatus("E");
			}
			reportService.merge(report);
			
		}else if(StringUtils.equals(report.getName(), CargoUtils.CA_TRANSPORT)){
			try {
				ReportFilter filter = new ReportFilter();
				
				String dateFrom  = report.getFilter().split(";")[0].split(":")[1];
				String dateTo  = report.getFilter().split(";")[1].split(":")[1];
				String flightNumber = report.getFilter().split(";")[2].split(":")[1];
				String immat = report.getFilter().split(";")[3].split(":")[1];
				String orig  = report.getFilter().split(";")[4].split(":")[1];
				String dest  = report.getFilter().split(";")[5].split(":")[1];
				
				filter.setDateFrom(dateFrom.trim());
				filter.setDateTo(dateTo.trim());
				filter.setFlightNumber(flightNumber.trim());
				filter.setImmat(immat.trim());
				filter.setOrig(orig.trim());
				filter.setDest(dest.trim());
				
				List<AwbFlown> caTransport = manifestDetailService.reportCaTransport(filter);
				
				CaTransportExcel caTransportExcel = new CaTransportExcel();
				ByteArrayOutputStream baos = caTransportExcel.makeExcel(caTransport, filter);
				baos.writeTo(new FileOutputStream(report.getLocal() + report.getFileName()));
				baos.flush();
				baos.close();
				
				report.setStatus("F");
			}catch (Exception e) {
				log.info("********************* ERROR EXCEL CA TRANSPORT **************************");
				report.setStatus("E");
			}
			reportService.merge(report);
			
			
		}
	}

}
