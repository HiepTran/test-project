package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.datawings.app.common.DoubleUtil;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.filter.Rmk;

public class RmkExcel extends AExcelPageEvent{

	Map<String, HSSFCellStyle> styles;
	public RmkExcel() {
		super();
		styles = new HashMap<String, HSSFCellStyle>();
	}
	
	public ByteArrayOutputStream makeExcel(List<Rmk> rmks, ReportFilter filter) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		
		HSSFSheet sheet = wb.createSheet("RTK");
		sheet.setDisplayGridlines(false);
		sheet.setFitToPage(true);
		sheet.setAutobreaks(true);
		createStyles(wb, styles);
		
		Integer row = 0;
		Integer col = 0;
		
		HSSFCellStyle filterStyle = styles.get("filterStyle");
		writeCell(sheet, row, col, "RMK PAX", filterStyle);
		
		row = row +1;
		col = 0;
		writeCell(sheet, ++row, col, "MOIS: " + filter.getDateFrom(), filterStyle);
		
		row = row +1;
		col = 10;
		writeCell(sheet, ++row, col, "CUMUL EXERCICE", styles.get("filterStyleCenter"), 7);
		
		
		col = 0;
		writeCell(sheet, ++row, col, "MOIS", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "PARCOURS", styles.get("titleCenter"));
		writeCell(sheet, row, ++col, "HT", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "MYC", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "TOTAL", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "POIDS KGS", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "RM KG", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "DISTANCE", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "TKT", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "RMK", styles.get("titleRight"));
		
		writeCell(sheet, row, ++col, "HT", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "MYC", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "TOTAL", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "POIDS KGS", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "RM KG", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "DISTANCE", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "TKT", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "RMK", styles.get("titleRight"));
		
		HSSFCellStyle detailLeft = styles.get("detailLeft");
		HSSFCellStyle detailRight = styles.get("detailRight");
		HSSFCellStyle detailColorLeft = styles.get("detailColorLeft");
		HSSFCellStyle detailColorRight = styles.get("detailColorRight");
		HSSFCellStyle styleLeft;
		HSSFCellStyle styleRight;
		
		for(int i = 0; i< rmks.size(); i++){
			if(i % 2 ==0){
				styleLeft = detailLeft;
				styleRight = detailRight;
			}else {
				styleLeft = detailColorLeft;
				styleRight = detailColorRight;
			}
			
			Rmk elm = rmks.get(i);
			col = 0;
			writeCell(sheet, ++row, col, elm.getMonth(), styleLeft);
			writeCell(sheet, row, ++col, elm.getOrig() +  "-" +  elm.getDest(), styleLeft);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getHt()) , styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getMyc()), styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getHt() + elm.getMyc()), styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getWeight()), styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel((elm.getHt() + elm.getMyc()) / elm.getWeight()), styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getDiscount()), styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getWeight()/1000 * elm.getDiscount()), styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel((elm.getHt() + elm.getMyc()) / (elm.getWeight()/1000 * elm.getDiscount())), styleRight);
			
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getHtYear()), styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getMycYear()), styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getHtYear() + elm.getMycYear()), styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getWeightYear()), styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel((elm.getHtYear() + elm.getMycYear()) / elm.getWeightYear()), styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getDiscountYear()), styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel(elm.getWeightYear()/1000 * elm.getDiscountYear()), styleRight);
			writeCell(sheet, row, ++col, DoubleUtil.roundExcel((elm.getHtYear() + elm.getMycYear()) / (elm.getWeightYear()/1000 * elm.getDiscountYear())), styleRight);
		}
		
		for (int k = 0; k <= col; k++) {
			sheet.autoSizeColumn((short) k);
		}
	
		wb.write(baos);
		return baos;
	}

}
