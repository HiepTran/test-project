package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.datawings.app.common.DateUtil;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.model.AwbFlown;

public class CaTransportExcel extends AExcelPageEvent{

	Map<String, HSSFCellStyle> styles;
	public CaTransportExcel() {
		super();
		styles = new HashMap<String, HSSFCellStyle>();
	}
	
	public ByteArrayOutputStream makeExcel(List<AwbFlown> caTransport, ReportFilter filter) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		
		HSSFSheet sheet = wb.createSheet("CA TRANSPORT");
		sheet.setDisplayGridlines(false);
		sheet.setFitToPage(true);
		sheet.setAutobreaks(true);
		createStyles(wb, styles);
		
		Integer row = 0;
		Integer col = 0;
		
		HSSFCellStyle filterStyle = styles.get("filterStyle");
		
		writeCell(sheet, row, col, "CA TRANSPORT PAR VOL", filterStyle, 6);
		
		row = row +1;
		col = 0;
		writeCell(sheet, ++row, col, "DATE VOL FROM", filterStyle);
		writeCell(sheet, row, ++col, filter.getDateFrom(), filterStyle);
		
		col = 0;
		writeCell(sheet, ++row, col, "TO", filterStyle);
		writeCell(sheet, row, ++col, filter.getDateTo(), filterStyle);
		
		col = 0;
		writeCell(sheet, ++row, col, "NOVOL", filterStyle);
		writeCell(sheet, row, ++col, filter.getFlightNumber(), filterStyle);
		
		col = 0;
		writeCell(sheet, ++row, col, "ORIG", filterStyle);
		writeCell(sheet, row, ++col, filter.getOrig(), filterStyle);
		
		col = 0;
		writeCell(sheet, ++row, col, "DEST", filterStyle);
		writeCell(sheet, row, ++col, filter.getDest(), filterStyle);
		
		col = 0;
		writeCell(sheet, ++row, col, "IMMAT", filterStyle);
		writeCell(sheet, row, ++col, filter.getImmat(), filterStyle);
		
		row = row + 1;
		col = 0;
		writeCell(sheet, ++row, col, "DATE VOL", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "NOVOL", styles.get("titleCenter"));
		writeCell(sheet, row, ++col, "ORIG", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "DEST", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "IMMAT", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "POIDS KGS", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "CA HT", styles.get("titleRight"));
		
		//Styles
		HSSFCellStyle detailLeft = styles.get("detailLeft");
		HSSFCellStyle detailRight = styles.get("detailRight");
		HSSFCellStyle detailColorLeft = styles.get("detailColorLeft");
		HSSFCellStyle detailColorRight = styles.get("detailColorRight");
		HSSFCellStyle styleLeft;
		HSSFCellStyle styleRight;
		
		for(int i = 0; i< caTransport.size(); i++){
			if(i % 2 ==0){
				styleLeft = detailLeft;
				styleRight = detailRight;
			}else {
				styleLeft = detailColorLeft;
				styleRight = detailColorRight;
			}
			
			AwbFlown elm = caTransport.get(i);
			
			col = 0;
			writeCell(sheet, ++row, col, DateUtil.date2String(elm.getFlightDate(), "dd/MM/yyyy"), styleLeft);
			writeCell(sheet, row, ++col, elm.getFlightNumber(), styleLeft);
			writeCell(sheet, row, ++col, elm.getOrig(), styleLeft);
			writeCell(sheet, row, ++col, elm.getDest(), styleLeft);
			writeCell(sheet, row, ++col, elm.getImmat(), styleLeft);
			writeCell(sheet, row, ++col, elm.getWeightGross(), styleRight);
			writeCell(sheet, row, ++col, elm.getTotalPp(), styleRight);			
		}
		
		for (int k = 0; k <= col; k++) {
			sheet.autoSizeColumn((short) k);
		}
	
		wb.write(baos);
		return baos;
	}

}
