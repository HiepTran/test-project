package com.datawings.app.report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.datawings.app.filter.ReportFilter;
import com.datawings.app.model.Awb;
import com.datawings.app.model.AwbTax;

public class SalesCassExcel extends AExcelPageEvent{

	Map<String, HSSFCellStyle> styles;
	public SalesCassExcel() {
		super();
		styles = new HashMap<String, HSSFCellStyle>();
	}
	
	public ByteArrayOutputStream makeExcel(List<Awb> awbs, ReportFilter filter) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		HSSFWorkbook wb = new HSSFWorkbook();
		
		HSSFSheet sheet = wb.createSheet("CSR QC");
		sheet.setDisplayGridlines(false);
		sheet.setFitToPage(true);
		sheet.setAutobreaks(true);
		createStyles(wb, styles);
		
		Integer row = 0;
		Integer col = 0;
		
		HSSFCellStyle filterStyle = styles.get("filterStyle");
		
		writeCell(sheet, row, col, "SALES REPORT FOR CAMAIR", filterStyle, 33);
		
		row = row +1;
		col = 0;
		String filterReport = "COUNTRY: " + filter.getIsoc() + "          " + "AGENT: " + filter.getAgtn()+ "          "  + "CHANNEL: " + filter.getChannel() +
				"          " + "DATE FROM: " + filter.getDateFrom() +  "          " + "TO: " + filter.getDateTo();
		writeCell(sheet, ++row, col, filterReport, filterStyle, 33);
		
		col = 0;
		row = row + 1;
		writeCell(sheet, ++row, col, "AIRLINE", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "COUNTRY", styles.get("titleCenter"));
		writeCell(sheet, row, ++col, "AWB", styles.get("titleCenter"));
		writeCell(sheet, row, ++col, "RDV", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "AGENT CODE", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "AGENT NAME", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "AGENT GSA", styles.get("titleLeft"));
		//writeCell(sheet, row, ++col, "FLIGHT DATE", styles.get("titleLeft"));
		//writeCell(sheet, row, ++col, "FLIGHT NBR.", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "ORIG", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "DEST", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "LINE", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "P/C", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "PCS.", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "ACTUAL WEIGHT", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "CHARG. WEIGHT", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "RATE", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "CUR.", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "ADDON", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "AMOUNT PP", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "AMOUNT CC", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "MY D.CARRIER", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "SC D.CARRIER", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "RA D.CARRIER", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "AW D.CARRIER", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "MC D.CARRIER", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "OTHERS D.CARRIER", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "TOTAL O/C DUE CARRIER", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "TOTAL DUE CARRIER", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "TOTAL O/C DUE AGENT", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "TOTAL DUE AGENT", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "GSA COM", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "TOTAL AIRLINE", styles.get("titleRight"));
		writeCell(sheet, row, ++col, "NATURE OF GOODS", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "HANDLING CODE", styles.get("titleLeft"));
		writeCell(sheet, row, ++col, "COMMENTS", styles.get("titleLeft"));

		// detail
		HSSFCellStyle detailLeft = styles.get("detailLeft");
		HSSFCellStyle detailRight = styles.get("detailRight");
		HSSFCellStyle detailColorLeft = styles.get("detailColorLeft");
		HSSFCellStyle detailColorRight = styles.get("detailColorRight");
		HSSFCellStyle styleLeft;
		HSSFCellStyle styleRight;
		
		for(int i = 0; i< awbs.size(); i++){
			if(i % 2 ==0){
				styleLeft = detailLeft;
				styleRight = detailRight;
			}else {
				styleLeft = detailColorLeft;
				styleRight = detailColorRight;
			}
			
			Awb awb = awbs.get(i);
			
			//Taxe
			AwbTax taxMyc = new AwbTax();
			AwbTax taxScc = new AwbTax();
			AwbTax taxRac = new AwbTax();
			AwbTax taxChc = new AwbTax();
			AwbTax taxIrc = new AwbTax();
			AwbTax taxOther = new AwbTax();
			
			AwbTax taxTotal = new AwbTax();
			
			for (AwbTax elm : awb.getAwbTax()) {
				if(StringUtils.equals(elm.getId().getTaxCode(), "MYC")){
					taxMyc = elm;
					taxTotal.setTaxAmount(taxTotal.getTaxAmount() + elm.getTaxAmount());
				}
				if(StringUtils.equals(elm.getId().getTaxCode(), "SCC")){
					taxScc = elm;
					taxTotal.setTaxAmount(taxTotal.getTaxAmount() + elm.getTaxAmount());
				}
				if(StringUtils.equals(elm.getId().getTaxCode(), "RAC")){
					taxRac = elm;
					taxTotal.setTaxAmount(taxTotal.getTaxAmount() + elm.getTaxAmount());
				}
				if(StringUtils.equals(elm.getId().getTaxCode(), "CHC")){
					taxChc = elm;
					taxTotal.setTaxAmount(taxTotal.getTaxAmount() + elm.getTaxAmount());
				}
				if(StringUtils.equals(elm.getId().getTaxCode(), "IRC")){
					taxIrc = elm;
					taxTotal.setTaxAmount(taxTotal.getTaxAmount() + elm.getTaxAmount());
				}
			}
			
			col = 0;
			writeCell(sheet, ++row, col, awb.getTacn(), styleLeft);
			writeCell(sheet, row, ++col, awb.getIsoc(), styleLeft);
			writeCell(sheet, row, ++col, awb.getLta(), styleLeft);
			writeCell(sheet, row, ++col, awb.getRdv(), styleLeft);
			writeCell(sheet, row, ++col, awb.getAgtn(), styleLeft);
			writeCell(sheet, row, ++col, awb.getAgtnName(), styleLeft);
			writeCell(sheet, row, ++col, awb.getAgtnGsa(), styleLeft);
			//writeCell(sheet, row, ++col, DateUtil.date2String(awb.getFlightDate(), "dd/MM/yyyy"), styleLeft);
			//writeCell(sheet, row, ++col, awb.getFlightNumber(), styleLeft);
			writeCell(sheet, row, ++col, awb.getOrig(), styleLeft);
			writeCell(sheet, row, ++col, awb.getDest(), styleLeft);
			writeCell(sheet, row, ++col, awb.getLine(), styleLeft);
			writeCell(sheet, row, ++col, awb.getChargeIndicator(), styleLeft);
			writeCell(sheet, row, ++col, 0.00, styleRight);
			writeCell(sheet, row, ++col, 0.00, styleRight);
			writeCell(sheet, row, ++col, awb.getWeightGross(), styleRight);
			writeCell(sheet, row, ++col, awb.getRate(), styleRight);
			writeCell(sheet, row, ++col, awb.getCutp(), styleLeft);
			writeCell(sheet, row, ++col, "", styleLeft);
			writeCell(sheet, row, ++col, awb.getWeightPp(), styleRight);
			writeCell(sheet, row, ++col, awb.getWeightCc(), styleRight);
			
			//tax
			writeCell(sheet, row, ++col, taxMyc.getTaxAmount(), styleRight);
			writeCell(sheet, row, ++col, taxScc.getTaxAmount(), styleRight);
			writeCell(sheet, row, ++col, taxRac.getTaxAmount(), styleRight);
			writeCell(sheet, row, ++col, taxChc.getTaxAmount(), styleRight);
			writeCell(sheet, row, ++col, taxIrc.getTaxAmount(), styleRight);
			writeCell(sheet, row, ++col, taxOther.getTaxAmount(), styleRight);
			
			writeCell(sheet, row, ++col, taxTotal.getTaxAmount(), styleRight);
			writeCell(sheet, row, ++col, awb.getWeightPp() + taxTotal.getTaxAmount(), styleRight);
			writeCell(sheet, row, ++col, 0.00, styleRight);
			writeCell(sheet, row, ++col, 0.00, styleRight);
			
			if(StringUtils.isNotBlank(awb.getAgtnGsa())){
				writeCell(sheet, row, ++col, awb.getComm(), styleRight);
			}else {
				writeCell(sheet, row, ++col, "", styleRight);
			}
			writeCell(sheet, row, ++col, awb.getWeightPp() + taxTotal.getTaxAmount() - awb.getComm(), styleRight);
			writeCell(sheet, row, ++col, "", styleLeft);
			writeCell(sheet, row, ++col, "", styleLeft);
			writeCell(sheet, row, ++col, awb.getDescriptUser(), styleLeft);
		}
		
		for (int k = 0; k <= col; k++) {
			sheet.autoSizeColumn((short) k);
		}
	
		wb.write(baos);
		return baos;
	}
}
