package com.datawings.app.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.model.Awb;
import com.datawings.app.model.AwbHeader;
import com.datawings.app.model.Cdo;
import com.datawings.app.model.Cdr;
import com.datawings.app.service.IAwbHeaderService;
import com.datawings.app.service.IAwbService;
import com.datawings.app.service.ICdoService;
import com.datawings.app.service.ICdrService;

@Service
public class LoadAwbManager {

	@Autowired
	private IAwbHeaderService awbHeaderService;
	
	@Autowired
	private IAwbService awbService;
	
	@Autowired
	private ICdoService cdoService;
	
	@Autowired
	private ICdrService cdrService;
	
	@Transactional
	public void saveAwbHeader(AwbHeader awbHeader) {
		awbHeaderService.merge(awbHeader);
	}

	@Transactional
	public void saveAwb(Awb awb) {
		awbService.merge(awb);
	}

	@Transactional
	public void saveCdo(Cdo cdo) {
		cdoService.merge(cdo);
	}

	@Transactional
	public void saveCdr(Cdr cdr) {
		cdrService.merge(cdr);
	}

}
