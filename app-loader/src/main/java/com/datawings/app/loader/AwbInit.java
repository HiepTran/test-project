package com.datawings.app.loader;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;

import com.datawings.app.common.BeanUtil;
import com.datawings.app.common.DateUtil;
import com.datawings.app.mapping.AAwb;

public class AwbInit {

	public static void loadBean(Object obj, String readLine) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		AAwb result = (AAwb) obj;
		Object[] values = extractInputValues(readLine, result.lengthFields());
		BeanUtil.loadSimpleValues(result, values );
	}

	public static Object[] extractInputValues(String readLine, Integer[] lengthFields) {
		String s = "";
		Integer pos = 0;
		for (int i = 0; i < lengthFields.length; i++) {
			s += StringUtils.mid(readLine, pos, lengthFields[i]) + ";";
			pos += lengthFields[i];
		}
		String[] result = s.split(";");

		return result;
	}

	@SuppressWarnings("rawtypes")
	public static void copySimplePropertyBean(Object dest, Object orig) {
		PropertyDescriptor descriptors[] = PropertyUtils.getPropertyDescriptors(dest);

		for (int i = 0; i < descriptors.length; i++) {
			String name = descriptors[i].getName();

			if (descriptors[i].getWriteMethod() != null) {
				try {
					Class d = PropertyUtils.getPropertyType(dest, name);
					Class o =  PropertyUtils.getPropertyType(orig, name);
					
					if(o != null){
						String value = (String) PropertyUtils.getProperty(orig, name);
						 
						if ("String".equalsIgnoreCase(d.getSimpleName())) {
							PropertyUtils.setSimpleProperty(dest, name, value);
						} else if ("Integer".equalsIgnoreCase(d.getSimpleName())) {
							PropertyUtils.setSimpleProperty(dest, name, Integer.parseInt(value));					
						} else if ("Date".equalsIgnoreCase(d.getSimpleName())) {
							PropertyUtils.setSimpleProperty(dest, name, DateUtil.string2Date(value, "yyMMdd"));
						}
					}
				} catch (IllegalAccessException e) {
					System.out.println("IllegalAccessException error ["+ e.getMessage() + "]");
				} catch (NoSuchMethodException e) {
					System.out.println("NoSuchMethodException error ["+ e.getMessage() + "]");
				} catch (InvocationTargetException e) {
					System.out.println("InvocationTargetException error ["+ e.getMessage() + "]");
				}
			}
		}
	}
}
