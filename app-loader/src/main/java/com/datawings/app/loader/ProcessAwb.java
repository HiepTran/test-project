package com.datawings.app.loader;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.DoubleUtil;
import com.datawings.app.manager.LoadAwbManager;
import com.datawings.app.mapping.AwbClass;
import com.datawings.app.mapping.AwbHeaderClass;
import com.datawings.app.mapping.CdoClass;
import com.datawings.app.mapping.CdrClass;
import com.datawings.app.model.Agent;
import com.datawings.app.model.AgentGsa;
import com.datawings.app.model.Awb;
import com.datawings.app.model.AwbDetail;
import com.datawings.app.model.AwbDetailId;
import com.datawings.app.model.AwbHeader;
import com.datawings.app.model.AwbTax;
import com.datawings.app.model.Cdo;
import com.datawings.app.model.Cdr;
import com.datawings.app.model.Line;
import com.datawings.app.model.Rdv;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.AwbUtil;
import com.datawings.app.process.CargoUtils;
import com.datawings.app.service.IAgentGsaService;
import com.datawings.app.service.IAgentService;
import com.datawings.app.service.IAirlineParamService;
import com.datawings.app.service.IAwbService;
import com.datawings.app.service.ILineService;

public class ProcessAwb {

	private static final Log log = LogFactory.getLog(ProcessAwb.class);
	
	@Autowired
	private LoadAwbManager loadAwbManager;
	
	@Autowired
	private IAirlineParamService airlineParamService;
	
	@Autowired
	private IAwbService awbService;
	
	@Autowired
	private IAgentService agentService;
	
	@Autowired
	private ProcessTax processTax;
	
	@Autowired
	private ILineService lineService;
	
	@Autowired
	private IAgentGsaService agentGsaService;
	
	@Autowired
	private AwbUtil awbUtil;
	
	public ProcessAwb() {
		loadApplicationContext(new String[] { "classpath:/loader-context.xml" });
	}

	@SuppressWarnings("resource")
	public void loadApplicationContext(String[] context) {
		ConfigurableApplicationContext applicationContext = new ClassPathXmlApplicationContext(context);
		applicationContext.registerShutdownHook();
		applicationContext.getBeanFactory().autowireBeanProperties(this, AutowireCapableBeanFactory.AUTOWIRE_NO, false);
	}

	public AwbHeader process(AwbHeaderClass awbHeaderClass, String readLine) {
		AwbHeader awbHeader = new AwbHeader();
		AwbInit.copySimplePropertyBean(awbHeader, awbHeaderClass);
		awbHeader.setIdHeader(readLine);
		//Create infor
		awbHeader.setCreatedBy(SysUser.USER_ADMIN);
		awbHeader.setCreatedDate(new Date());
		
		loadAwbManager.saveAwbHeader(awbHeader);
		return awbHeader;
	}

	public void processAwb(AwbClass awbClass, AwbHeader awbHeader) {
		log.info("********************* AWM LTA " + awbClass.getLta() + " **************************");
		
		Awb awbCheck = awbService.find(awbClass.getLta());
		
		if(awbCheck == null || StringUtils.isBlank(awbCheck.getStatusControl())){
			Awb awb = new Awb();
			AwbInit.copySimplePropertyBean(awb, awbClass);
			
			awb.setIsoc(awbHeader.getIsoc());
			awb.setIdHeader(awbHeader.getIdHeader());
			awb.setAgtnCode(StringUtils.substring(awb.getAgtn(), 0, 10));
			awb.setCheckDigit(StringUtils.substring(awb.getAgtn(), 10, 11));
			awb.setAgtnIata(StringUtils.substring(awb.getAgtn(), 0, 7));
			awb.setModePayment("CA");
			awb.setDeclaredCarrige(CargoUtils.NVD);
			awb.setDeclaredCustom(CargoUtils.NCV);
			
			Agent agent = agentService.find(awb.getAgtnCode());
			if(agent != null){
				awb.setAgtnName(agent.getName().trim());
			}else {
				log.info("********************* ERROR MISS AGENT " + awb.getAgtnCode() + "**************************");
				awb.setDescriptionAdmin("No agent");
			}
			
			//fotmat Double
			if(StringUtils.equals("K", awb.getWeightIndicator())){
				awb.setWeightGross(DoubleUtil.convertDouble(awbClass.getWeightCharge(), 1));
				awb.setWeightCharge(DoubleUtil.convertDouble(awbClass.getWeightCharge(), 1));
			}else{
				awb.setWeightGross(DoubleUtil.convertDouble(awbClass.getWeightCharge(), 0));
				awb.setWeightCharge(DoubleUtil.convertDouble(awbClass.getWeightCharge(), 0));
			}
			awb.setWeightPp(DoubleUtil.convertDouble(awbClass.getWeightPp(), 2));
			awb.setValuationPp(DoubleUtil.convertDouble(awbClass.getValuationPp(), 2));
			awb.setCarrierPp(DoubleUtil.convertDouble(awbClass.getCarrierPp(), 2));
			awb.setAgentPp(DoubleUtil.convertDouble(awbClass.getAgentPp(), 2));
			awb.setTotalPp(DoubleUtil.round(awb.getWeightPp() + awb.getCarrierPp() + awb.getAgentPp(), 2));
			
			awb.setWeightCc(DoubleUtil.convertDouble(awbClass.getWeightCc(), 2));
			awb.setValuationCc(DoubleUtil.convertDouble(awbClass.getValuationCc(), 2));
			awb.setCarrierCc(DoubleUtil.convertDouble(awbClass.getCarrierCc(), 2));
			awb.setAgentCc(DoubleUtil.convertDouble(awbClass.getAgentCc(), 2));
			awb.setTotalCc(DoubleUtil.round(awb.getWeightCc() + awb.getCarrierCc() + awb.getAgentCc(), 2));
			
			awb.setCommPercent(DoubleUtil.convertDouble(awbClass.getCommPercent(), 2));
			awb.setComm(DoubleUtil.round(DoubleUtil.convertDouble(awbClass.getComm(), 2),2));
			awb.setDiscount(DoubleUtil.convertDouble(awbClass.getDiscount(), 2));
			awb.setTaxeAgent(DoubleUtil.convertDouble(awbClass.getTaxeAgent(), 2));
			awb.setTaxAirline(DoubleUtil.convertDouble(awbClass.getTaxAirline(), 2));
			
			//Rate cass
			awb.setUnitCass(DoubleUtil.round((awb.getWeightPp() + awb.getWeightCc())/ awb.getWeightCharge(), 2) );
			
			String chargeIndicator = CargoUtils.CHARGE_P;
			String otherIndicator = CargoUtils.CHARGE_P;
			if(awb.getTotalCc() > 0.0){
				chargeIndicator = CargoUtils.CHARGE_C;
			}
			if((awb.getAgentCc() + awb.getCarrierCc()) > 0.0){
				otherIndicator = CargoUtils.CHARGE_C;
			}
			
			awb.setChargeIndicator(chargeIndicator);
			awb.setOtherIndicator(otherIndicator);
			
			awbUtil.convertLocal(awb);
			
			//Create info.
			awb.setChannel(CargoUtils.HOT);
			awb.setCreatedBy(SysUser.USER_ADMIN);
			awb.setCreatedDate(new Date());
			
			//KM
			Double km = awbService.getKm(awb.getOrig(), awb.getDest());
			awb.setKm(km);
			
			//RDV
			Rdv rdv = awbUtil.processRdv(awb.getAgtn(), DateUtil.date2String(new Date(), "dd/MM/yyyy"));
			awb.setRdv(rdv.getRdv());
			awb.setIdRdv(rdv.getCode());
			
			//LINE
			Line line = lineService.getLine(awb.getOrig(), awb.getDest());
			if(line != null) awb.setLine(line.getLine().trim());
			
			//DETAIL
			AwbDetail awbDetail = new AwbDetail();
			
			AwbDetailId awbDetailId = new AwbDetailId();
			awbDetailId.setLta(awb.getLta());
			awbDetailId.setSequence(1);
			
			awbDetail.setId(awbDetailId);
			awbDetail.setQuantity(1);
			awbDetail.setWeightIndicator(awb.getWeightIndicator());
			awbDetail.setWeightGross(awb.getWeightGross());
			awbDetail.setRateClass("");
			awbDetail.setWeightCharge(awb.getWeightCharge());
			awbDetail.setUnit(DoubleUtil.round((awb.getWeightPp() + awb.getWeightCc())/ awb.getWeightCharge(), 2) );
			awbDetail.setUnitCass(DoubleUtil.round((awb.getWeightPp() + awb.getWeightCc())/ awb.getWeightCharge(), 2) );
			awbDetail.setAmount(awb.getWeightPp() + awb.getWeightCc());
			awbDetail.setDescription("");
			awbDetail.setCreatedBy(CargoUtils.HOT);
			awbDetail.setCreatedDate(new Date());
			
			awbDetail.setAwb(awb);
			awb.getAwbDetail().add(awbDetail);
			
			//TAX
			List<AwbTax> listTaxs = new ArrayList<AwbTax>();
			listTaxs = processTax.setAwbTax(awb, agent);
			for (AwbTax elm : listTaxs) {
				elm.setAwb(awb);
				awb.getAwbTax().add(elm);
			}
			
			//GSA
			Double taxMyc = 0.0;
			for (AwbTax elmTax : listTaxs) {
				if(StringUtils.equals(elmTax.getId().getTaxCode(), "MYC")){
					taxMyc = elmTax.getTaxAmount();
					break;
				}
			}
			AgentGsa agentGsa = agentGsaService.getAgentGsa(awb.getAgtnIata(), awb.getDateExecute());
			if(agentGsa != null){
				awb.setAgtnGsa(agentGsa.getCode());
				awb.setCommPercent(agentGsa.getComm());
				awb.setComm(DoubleUtil.round(agentGsa.getComm() * (taxMyc + awb.getWeightPp() + awb.getWeightCc()) / 100, 2) );
			}
			
			awb.setNet(DoubleUtil.round(awb.getTotalPp() + awb.getTotalCc() + awb.getVat() - awb.getComm(), 2));
			
			//Delete tax and detail
			awbService.deleteAwbTax(awb.getLta());
			awbService.deleteAwbDetail(awb.getLta());
			
			//Save AWB
			loadAwbManager.saveAwb(awb);
		}
		
	}

	public void processCdo(CdoClass cdoClass, AwbHeader awbHeader) {
		
		log.info("*********************CDO LTA " + cdoClass.getLta() +  "**************************");
		
		Cdo cdo = new Cdo();
		AwbInit.copySimplePropertyBean(cdo, cdoClass);
		
		cdo.setIsoc(awbHeader.getIsoc());
		cdo.setIdHeader(awbHeader.getIdHeader());
		
		//Create infor
		cdo.setCreatedBy(SysUser.USER_ADMIN);
		cdo.setCreatedDate(new Date());
		
		loadAwbManager.saveCdo(cdo);
	}

	public void processCdr(CdrClass cdrClass, AwbHeader awbHeader) {
		log.info("*********************CDR LTA " + cdrClass.getLta() +  "**************************");
		
		Cdr cdr = new Cdr();
		AwbInit.copySimplePropertyBean(cdr, cdrClass);
		
		cdr.setIsoc(awbHeader.getIsoc());
		cdr.setIdHeader(awbHeader.getIdHeader());
		
		//Create infor
		cdr.setCreatedBy(SysUser.USER_ADMIN);
		cdr.setCreatedDate(new Date());
		
		loadAwbManager.saveCdr(cdr);
	}
	
}
