package com.datawings.app.loader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang.StringUtils;

import com.datawings.app.mapping.AwbClass;
import com.datawings.app.mapping.AwbHeaderClass;
import com.datawings.app.mapping.CdoClass;
import com.datawings.app.mapping.CdrClass;
import com.datawings.app.model.AwbHeader;

public class AwbLoader {

	public void main(String[] args) throws IOException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		String fileName = args[0];
		String readLine = "";
		
		BufferedReader reader = openFile(fileName);
		
		AwbHeader awbHeader = new AwbHeader();
		
		AwbHeaderClass awbHeaderClass = new AwbHeaderClass();
		AwbClass awbClass = new AwbClass();
		CdoClass cdoClass =new CdoClass();
		CdrClass cdrClass =new CdrClass();
		
		ProcessAwb process = new ProcessAwb();
		while ((readLine = reader.readLine()) != null) {
			String recordId = StringUtils.left(readLine, 3);
			
			if(StringUtils.equals(recordId, "AAA")){
				AwbInit.loadBean(awbHeaderClass, readLine);
				awbHeader = process.process(awbHeaderClass, readLine);
			}
			
			if(StringUtils.equals(recordId, "AWM")){
				AwbInit.loadBean(awbClass, readLine);
				process.processAwb(awbClass, awbHeader);
			}

			if(StringUtils.equals(recordId, "DCO")){
				AwbInit.loadBean(cdoClass, readLine);
				process.processCdo(cdoClass, awbHeader);
			}
			
			if(StringUtils.equals(recordId, "DCR")){
				AwbInit.loadBean(cdrClass, readLine);
				process.processCdr(cdrClass, awbHeader);
			}
		}
	}

	private BufferedReader openFile(String fileName) throws FileNotFoundException {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
		return bufferedReader;
	}
}
