package com.datawings.app.loader;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.DoubleUtil;
import com.datawings.app.model.Agent;
import com.datawings.app.model.Awb;
import com.datawings.app.model.AwbTax;
import com.datawings.app.model.AwbTaxId;
import com.datawings.app.model.SysUser;
import com.datawings.app.model.Tax;
import com.datawings.app.service.ITaxService;

@Component
public class ProcessTax {

	@Autowired 
	private ITaxService taxService;

	public List<AwbTax> setAwbTax(Awb awb, Agent agent) {
		List<AwbTax> rs = new ArrayList<AwbTax>();
		
		//AWA
		if(agent != null && StringUtils.equals(agent.getKind(), "TR")){
			Tax taxParam = new Tax();
			taxParam = taxService.getTax(awb.getIsoc(), "AWA", "", "", DateUtil.date2String(awb.getDateExecute(), "MM/dd/yyyy"), "GEN");
			
			if(taxParam != null){
				AwbTax awbTaxAwa = new AwbTax();
				awbTaxAwa = process(taxParam, awb);
				rs.add(awbTaxAwa);
			}
		}
		
		if(awb.getLine() != null){
			Tax taxParam = new Tax();
			
			//SCC
			taxParam = taxService.getTax(awb.getIsoc(), "SCC", awb.getLine(), "SPE", DateUtil.date2String(awb.getDateExecute(), "MM/dd/yyyy"), "GEN");
			if(taxParam != null){
				AwbTax elm = new AwbTax();
				elm = process(taxParam, awb);
				rs.add(elm);
			}
			
			//CHC
			taxParam = taxService.getTax(awb.getIsoc(), "CHC", awb.getLine(), "", DateUtil.date2String(awb.getDateExecute(), "MM/dd/yyyy"), "GEN");
			if(taxParam != null){
				AwbTax elm = new AwbTax();
				elm = process(taxParam, awb);
				rs.add(elm);
			}
			
			//MRC
			taxParam = taxService.getTax(awb.getIsoc(), "MRC", awb.getLine(), "", DateUtil.date2String(awb.getDateExecute(), "MM/dd/yyyy"), "GEN");
			if(taxParam != null){
				AwbTax elm = new AwbTax();
				elm = process(taxParam, awb);
				rs.add(elm);
			}
			
			//TXC
			if(StringUtils.equals(awb.getLine(), "REC") || StringUtils.equals(awb.getLine(), "DOM")){
				taxParam = taxService.getTax(awb.getIsoc(), "TXC", awb.getLine(), "", DateUtil.date2String(awb.getDateExecute(), "MM/dd/yyyy"), "GEN");
				if(taxParam != null){
					AwbTax elm = new AwbTax();
					elm = process(taxParam, awb);
					rs.add(elm);
				}
			}
		}		
		
		//DBC
		if(awb.getTotalCc() > 0.0){
			Tax taxParam = new Tax();
			taxParam = taxService.getTax(awb.getIsoc(), "DBC", awb.getLine(), "", DateUtil.date2String(awb.getDateExecute(), "MM/dd/yyyy"), "GEN");
			if(taxParam != null){
				AwbTax elm = new AwbTax();
				elm = process(taxParam, awb);
				rs.add(elm);
			}
		}
		
		//GSA
		if(StringUtils.isNotBlank(awb.getAgtnGsa())){
			Tax taxParam = new Tax();
			
			//MYC
			taxParam = taxService.getTax(awb.getIsoc(), "MYC", "", "SPE", DateUtil.date2String(awb.getDateExecute(), "MM/dd/yyyy"), "ATC");
			if(taxParam != null){
				AwbTax elm = new AwbTax();
				elm = process(taxParam, awb);
				rs.add(elm);
			}
			
			//IRC
			taxParam = taxService.getTax(awb.getIsoc(), "IRC", "", "SPE", DateUtil.date2String(awb.getDateExecute(), "MM/dd/yyyy"), "ATC");
			if(taxParam != null){
				AwbTax elm = new AwbTax();
				elm = process(taxParam, awb);
				rs.add(elm);
			}
		}else {
			if(StringUtils.isNotBlank(awb.getLine())){
				//MYC
				Tax taxParam = new Tax();
				taxParam = taxService.getTax(awb.getIsoc(), "MYC", "", "SPE", DateUtil.date2String(awb.getDateExecute(), "MM/dd/yyyy"), "GEN");
				
				if(taxParam != null){
					AwbTax elm = new AwbTax();
					elm = process(taxParam, awb);
					rs.add(elm);
				}
			}
		}	
		
		return rs;
	}

	
	private AwbTax process(Tax taxParam, Awb awb) {		
		AwbTax awbTax = new AwbTax();
		
		AwbTaxId awbTaxId = new AwbTaxId();
		awbTaxId.setLta(awb.getLta());
		awbTaxId.setTaxCode(taxParam.getCode());
		awbTax.setId(awbTaxId);
		awbTax.setCreatedBy(SysUser.USER_ADMIN);
		
		if(StringUtils.equals(taxParam.getUnit(), "A")){
			awbTax.setTaxAmount(taxParam.getAmount());
		}else if(StringUtils.equals(taxParam.getUnit(), "K")){
			awbTax.setTaxAmount(DoubleUtil.round(taxParam.getAmount() * awb.getWeightGross(), 2));
		}else if(StringUtils.equals(taxParam.getUnit(), "P")){
			awbTax.setTaxAmount(DoubleUtil.round(taxParam.getAmount() * awb.getWeightGross() / 100, 2));
		}
		
		return awbTax;
	}
}
