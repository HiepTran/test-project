package com.datawings.app.mapping;

import com.datawings.app.common.BeanUtil;

public class AwbHeaderClass extends AAwb{

	private String recordId;
	private String isoc;
	private String tacn;
	private String dateStart;
	private String dateEnd;
	private String dateBill;
	private String fileNumber;
	private String cutp;
	//private String branchOffice;

	public AwbHeaderClass() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public Integer[] lengthFields() {
		Integer[] result = { 3, 2, 3, 6, 6, 6, 2, 3 };
		return result;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getIsoc() {
		return isoc;
	}

	public void setIsoc(String isoc) {
		this.isoc = isoc;
	}

	public String getTacn() {
		return tacn;
	}

	public void setTacn(String tacn) {
		this.tacn = tacn;
	}

	public String getDateStart() {
		return dateStart;
	}

	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}

	public String getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getDateBill() {
		return dateBill;
	}

	public void setDateBill(String dateBill) {
		this.dateBill = dateBill;
	}

	public String getFileNumber() {
		return fileNumber;
	}

	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}

	public String getCutp() {
		return cutp;
	}

	public void setCutp(String cutp) {
		this.cutp = cutp;
	}
}
