package com.datawings.app.mapping;

import com.datawings.app.common.BeanUtil;

public class AwbClass extends AAwb {

	private String recordId;
	private String branchOffice;
	private String vatIndicator;
	private String tacn;
	private String lta;
	private String numberModular;
	private String filler1;
	private String orig;
	private String agtn;
	private String useIndicator;
	private String lateIndicator;
	private String filler2;
	private String dest;
	private String dateExecute;
	private String weightCharge;
	private String weightIndicator;
	private String cutp;
	private String weightPp;
	private String valuationPp;
	private String carrierPp;
	private String agentPp;
	private String weightCc;
	private String valuationCc;
	private String carrierCc;
	private String agentCc;
	private String commPercent;
	private String comm;
	private String filler3;
	private String agentRef;
	private String filler4;
	private String dateAccept;
	private String rate;
	private String discount;
	private String taxAirline;
	private String taxeAgent;

	public AwbClass() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public Integer[] lengthFields() {
		Integer[] result = { 3, 1, 1, 3, 8, 1, 1, 3, 11, 1, 1, 2, 3, 6, 7, 1,
				3, 12, 12, 12, 12, 12, 12, 12, 12, 4, 12, 1, 14, 10, 6, 11, 12,
				8, 8 };
		return result;
	}

	public String getRecordId() {
		return recordId;
	}

	public String getBranchOffice() {
		return branchOffice;
	}

	public String getVatIndicator() {
		return vatIndicator;
	}

	public String getTacn() {
		return tacn;
	}

	public String getLta() {
		return lta;
	}

	public String getNumberModular() {
		return numberModular;
	}

	public String getFiller1() {
		return filler1;
	}

	public String getOrig() {
		return orig;
	}

	public String getAgtn() {
		return agtn;
	}

	public String getUseIndicator() {
		return useIndicator;
	}

	public String getLateIndicator() {
		return lateIndicator;
	}

	public String getFiller2() {
		return filler2;
	}

	public String getDest() {
		return dest;
	}

	public String getWeightIndicator() {
		return weightIndicator;
	}

	public String getCutp() {
		return cutp;
	}

	public String getWeightPp() {
		return weightPp;
	}

	public String getValuationPp() {
		return valuationPp;
	}

	public String getCarrierPp() {
		return carrierPp;
	}

	public String getAgentPp() {
		return agentPp;
	}

	public String getWeightCc() {
		return weightCc;
	}

	public String getValuationCc() {
		return valuationCc;
	}

	public String getCarrierCc() {
		return carrierCc;
	}

	public String getAgentCc() {
		return agentCc;
	}

	public String getCommPercent() {
		return commPercent;
	}

	public String getComm() {
		return comm;
	}

	public String getFiller3() {
		return filler3;
	}

	public String getAgentRef() {
		return agentRef;
	}

	public String getFiller4() {
		return filler4;
	}

	public String getDateAccept() {
		return dateAccept;
	}

	public String getRate() {
		return rate;
	}

	public String getDiscount() {
		return discount;
	}

	public String getTaxAirline() {
		return taxAirline;
	}

	public String getTaxeAgent() {
		return taxeAgent;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public void setBranchOffice(String branchOffice) {
		this.branchOffice = branchOffice;
	}

	public void setVatIndicator(String vatIndicator) {
		this.vatIndicator = vatIndicator;
	}

	public void setTacn(String tacn) {
		this.tacn = tacn;
	}

	public void setLta(String lta) {
		this.lta = lta;
	}

	public void setNumberModular(String numberModular) {
		this.numberModular = numberModular;
	}

	public void setFiller1(String filler1) {
		this.filler1 = filler1;
	}

	public void setOrig(String orig) {
		this.orig = orig;
	}

	public void setAgtn(String agtn) {
		this.agtn = agtn;
	}

	public void setUseIndicator(String useIndicator) {
		this.useIndicator = useIndicator;
	}

	public void setLateIndicator(String lateIndicator) {
		this.lateIndicator = lateIndicator;
	}

	public void setFiller2(String filler2) {
		this.filler2 = filler2;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getDateExecute() {
		return dateExecute;
	}

	public void setDateExecute(String dateExecute) {
		this.dateExecute = dateExecute;
	}

	public String getWeightCharge() {
		return weightCharge;
	}

	public void setWeightCharge(String weightCharge) {
		this.weightCharge = weightCharge;
	}

	public void setWeightIndicator(String weightIndicator) {
		this.weightIndicator = weightIndicator;
	}

	public void setCutp(String cutp) {
		this.cutp = cutp;
	}

	public void setWeightPp(String weightPp) {
		this.weightPp = weightPp;
	}

	public void setValuationPp(String valuationPp) {
		this.valuationPp = valuationPp;
	}

	public void setCarrierPp(String carrierPp) {
		this.carrierPp = carrierPp;
	}

	public void setAgentPp(String agentPp) {
		this.agentPp = agentPp;
	}

	public void setWeightCc(String weightCc) {
		this.weightCc = weightCc;
	}

	public void setValuationCc(String valuationCc) {
		this.valuationCc = valuationCc;
	}

	public void setCarrierCc(String carrierCc) {
		this.carrierCc = carrierCc;
	}

	public void setAgentCc(String agentCc) {
		this.agentCc = agentCc;
	}

	public void setCommPercent(String commPercent) {
		this.commPercent = commPercent;
	}

	public void setComm(String comm) {
		this.comm = comm;
	}

	public void setFiller3(String filler3) {
		this.filler3 = filler3;
	}

	public void setAgentRef(String agentRef) {
		this.agentRef = agentRef;
	}

	public void setFiller4(String filler4) {
		this.filler4 = filler4;
	}

	public void setDateAccept(String dateAccept) {
		this.dateAccept = dateAccept;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public void setTaxAirline(String taxAirline) {
		this.taxAirline = taxAirline;
	}

	public void setTaxeAgent(String taxeAgent) {
		this.taxeAgent = taxeAgent;
	}

}
