package com.datawings.app.mapping;

import com.datawings.app.common.BeanUtil;

public class CdoClass extends AAwb {

	private String recordId;
	private String branchOffice;
	private String vatIndicator;
	private String tacn;
	private String lta;
	private String numberModular;
	private String filler1;
	private String orig;
	private String agtn;
	private String cdNumber;
	private String cutp;
	private String rate;
	private String dateExecute;
	private String weightType;
	private String weightCharge;
	private String valuation_type;
	private String valuationCharge;
	private String taxType;
	private String tax;
	private String taxAgentType;
	private String taxAgent;
	private String taxCarrierType;
	private String taxCarrier;
	private String vatCharge;
	private String comm;
	private String vatComm;
	private String discount;
	private String discountIndicator;
	private String weightIndicator;
	private String weight;
	private String dest;

	public CdoClass() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public Integer[] lengthFields() {
		Integer[] result = { 3, 1, 1, 3, 8, 1, 1, 3, 11, 6, 3, 11, 6, 1, 12, 1,
				12, 1, 12, 1, 12, 1, 12, 12, 12, 12, 12, 1, 1, 7, 3 };
		return result;
	}

	public String getRecordId() {
		return recordId;
	}

	public String getBranchOffice() {
		return branchOffice;
	}

	public String getVatIndicator() {
		return vatIndicator;
	}

	public String getTacn() {
		return tacn;
	}

	public String getLta() {
		return lta;
	}

	public String getNumberModular() {
		return numberModular;
	}

	public String getFiller1() {
		return filler1;
	}

	public String getOrig() {
		return orig;
	}

	public String getAgtn() {
		return agtn;
	}

	public String getCdNumber() {
		return cdNumber;
	}

	public String getCutp() {
		return cutp;
	}

	public String getRate() {
		return rate;
	}

	public String getWeightType() {
		return weightType;
	}

	public String getWeightCharge() {
		return weightCharge;
	}

	public String getValuation_type() {
		return valuation_type;
	}

	public String getValuationCharge() {
		return valuationCharge;
	}

	public String getTaxType() {
		return taxType;
	}

	public String getTax() {
		return tax;
	}

	public String getTaxAgentType() {
		return taxAgentType;
	}

	public String getTaxAgent() {
		return taxAgent;
	}

	public String getTaxCarrierType() {
		return taxCarrierType;
	}

	public String getTaxCarrier() {
		return taxCarrier;
	}

	public String getVatCharge() {
		return vatCharge;
	}

	public String getComm() {
		return comm;
	}

	public String getVatComm() {
		return vatComm;
	}

	public String getDiscount() {
		return discount;
	}

	public String getDiscountIndicator() {
		return discountIndicator;
	}

	public String getWeightIndicator() {
		return weightIndicator;
	}

	public String getWeight() {
		return weight;
	}

	public String getDest() {
		return dest;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public void setBranchOffice(String branchOffice) {
		this.branchOffice = branchOffice;
	}

	public void setVatIndicator(String vatIndicator) {
		this.vatIndicator = vatIndicator;
	}

	public void setTacn(String tacn) {
		this.tacn = tacn;
	}

	public void setLta(String lta) {
		this.lta = lta;
	}

	public void setNumberModular(String numberModular) {
		this.numberModular = numberModular;
	}

	public void setFiller1(String filler1) {
		this.filler1 = filler1;
	}

	public void setOrig(String orig) {
		this.orig = orig;
	}

	public void setAgtn(String agtn) {
		this.agtn = agtn;
	}

	public void setCdNumber(String cdNumber) {
		this.cdNumber = cdNumber;
	}

	public void setCutp(String cutp) {
		this.cutp = cutp;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getDateExecute() {
		return dateExecute;
	}

	public void setDateExecute(String dateExecute) {
		this.dateExecute = dateExecute;
	}

	public void setWeightType(String weightType) {
		this.weightType = weightType;
	}

	public void setWeightCharge(String weightCharge) {
		this.weightCharge = weightCharge;
	}

	public void setValuation_type(String valuation_type) {
		this.valuation_type = valuation_type;
	}

	public void setValuationCharge(String valuationCharge) {
		this.valuationCharge = valuationCharge;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public void setTaxAgentType(String taxAgentType) {
		this.taxAgentType = taxAgentType;
	}

	public void setTaxAgent(String taxAgent) {
		this.taxAgent = taxAgent;
	}

	public void setTaxCarrierType(String taxCarrierType) {
		this.taxCarrierType = taxCarrierType;
	}

	public void setTaxCarrier(String taxCarrier) {
		this.taxCarrier = taxCarrier;
	}

	public void setVatCharge(String vatCharge) {
		this.vatCharge = vatCharge;
	}

	public void setComm(String comm) {
		this.comm = comm;
	}

	public void setVatComm(String vatComm) {
		this.vatComm = vatComm;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public void setDiscountIndicator(String discountIndicator) {
		this.discountIndicator = discountIndicator;
	}

	public void setWeightIndicator(String weightIndicator) {
		this.weightIndicator = weightIndicator;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

}
