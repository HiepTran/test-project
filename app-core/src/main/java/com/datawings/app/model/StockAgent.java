package com.datawings.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "stock_agent")
@SequenceGenerator(name = "seq_stock_agent_id", sequenceName = "stock_agent_id_stock_agent_seq", allocationSize = 1)
public class StockAgent extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_stock_agent_id")
	@Column(name = "id_stock_agent", unique = true, nullable = false)
	private Integer idStockAgent;

	@Column(name = "agtn")
	private String agtn;

	@Column(name = "serial_from")
	private Integer serialFrom;

	@Column(name = "serial_to")
	private Integer serialTo;

	@Column(name = "quantity")
	private Integer quantity;

	@Column(name = "date_entry")
	private Date dateEntry;

	@Column(name = "date_used")
	private Date dateUsed;

	@Column(name = "type")
	private String type;

	@Column(name = "comment")
	private String comment;

	@Column(name = "id_stock_central_hist")
	private Integer idStockCentralHist;

	@Column(name = "agent_orig")
	private String agentOrig;

	@Column(name = "created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	public StockAgent() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getAgentOrig() {
		return agentOrig;
	}

	public void setAgentOrig(String agentOrig) {
		this.agentOrig = agentOrig;
	}

	public Integer getIdStockAgent() {
		return idStockAgent;
	}

	public String getAgtn() {
		return agtn;
	}

	public Integer getSerialFrom() {
		return serialFrom;
	}

	public Integer getSerialTo() {
		return serialTo;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public Date getDateEntry() {
		return dateEntry;
	}

	public Date getDateUsed() {
		return dateUsed;
	}

	public String getType() {
		return type;
	}

	public String getComment() {
		return comment;
	}

	public Integer getIdStockCentralHist() {
		return idStockCentralHist;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setIdStockAgent(Integer idStockAgent) {
		this.idStockAgent = idStockAgent;
	}

	public void setAgtn(String agtn) {
		this.agtn = agtn;
	}

	public void setSerialFrom(Integer serialFrom) {
		this.serialFrom = serialFrom;
	}

	public void setSerialTo(Integer serialTo) {
		this.serialTo = serialTo;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public void setDateEntry(Date dateEntry) {
		this.dateEntry = dateEntry;
	}

	public void setDateUsed(Date dateUsed) {
		this.dateUsed = dateUsed;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setIdStockCentralHist(Integer idStockCentralHist) {
		this.idStockCentralHist = idStockCentralHist;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
