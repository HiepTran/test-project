package com.datawings.app.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.datawings.app.common.BeanUtil;

@Embeddable
public class BsrId implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Column(name = "curr_from", length = 3)
	private String currFrom;
	
	@Column(name = "curr_to", length = 3)
	private String currTo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_start")
	private Date dateStart;

	public BsrId() {
		super();
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getCurrFrom() {
		return currFrom;
	}

	public void setCurrFrom(String currFrom) {
		this.currFrom = currFrom;
	}

	public String getCurrTo() {
		return currTo;
	}

	public void setCurrTo(String currTo) {
		this.currTo = currTo;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}
}
