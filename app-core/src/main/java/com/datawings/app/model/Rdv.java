package com.datawings.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "rdv")
public class Rdv extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "code", nullable = false)
	private String code;

	@Column(name = "rdv")
	private String rdv;

	@Column(name = "agtn")
	private String agtn;

	@Column(name = "emission")
	private Date emission;

	@Column(name = "created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	@Transient
	private Integer nbAwb;

	public Rdv() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getRdv() {
		return rdv;
	}

	public void setRdv(String rdv) {
		this.rdv = rdv;
	}

	public String getCode() {
		return code;
	}

	public String getAgtn() {
		return agtn;
	}

	public Date getEmission() {
		return emission;
	}

	public void setEmission(Date emission) {
		this.emission = emission;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setAgtn(String agtn) {
		this.agtn = agtn;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getNbAwb() {
		return nbAwb;
	}

	public void setNbAwb(Integer nbAwb) {
		this.nbAwb = nbAwb;
	}

}
