package com.datawings.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "agent")
public class Agent extends Base {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "code", length = 10, nullable = false)
	private String code;

	@Column(name = "check_digit", length = 1)
	private String checkDigit;

	@Column(name = "short_name")
	private String shortName;

	@Column(name = "name")
	private String name;

	@Column(name = "iata_ind", length = 1)
	private String iataInd;

	@Column(name = "kind")
	private String kind;

	@Column(name = "address1")
	private String address1;

	@Column(name = "address2")
	private String address2;

	@Column(name = "address3")
	private String address3;

	@Column(name = "zip")
	private String zip;

	@Column(name = "city")
	private String city;

	@Column(name = "isoc", length = 2)
	private String isoc;

	@Column(name = "region")
	private String region;

	@Column(name = "phone")
	private String phone;

	@Column(name = "fax")
	private String fax;

	@Column(name = "website")
	private String website;

	@Column(name = "email")
	private String email;
	
	@Column(name = "comm")
	private Double comm;

	@Column(name = "created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;
	
	public Agent() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public Double getComm() {
		return comm;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setComm(Double comm) {
		this.comm = comm;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCode() {
		return code;
	}

	public String getCheckDigit() {
		return checkDigit;
	}

	public String getShortName() {
		return shortName;
	}

	public String getName() {
		return name;
	}

	public String getIataInd() {
		return iataInd;
	}

	public String getKind() {
		return kind;
	}

	public String getAddress1() {
		return address1;
	}

	public String getAddress2() {
		return address2;
	}

	public String getAddress3() {
		return address3;
	}

	public String getZip() {
		return zip;
	}

	public String getCity() {
		return city;
	}

	public String getIsoc() {
		return isoc;
	}

	public String getRegion() {
		return region;
	}

	public String getPhone() {
		return phone;
	}

	public String getFax() {
		return fax;
	}

	public String getWebsite() {
		return website;
	}

	public String getEmail() {
		return email;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCheckDigit(String checkDigit) {
		this.checkDigit = checkDigit;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setIataInd(String iataInd) {
		this.iataInd = iataInd;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setIsoc(String isoc) {
		this.isoc = isoc;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
