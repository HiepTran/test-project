package com.datawings.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "line")
public class Line extends Base {
	private static final long serialVersionUID = 1L;

	@Column(name = "orac", length = 3)
	private String orac;

	@Column(name = "dstc", length = 3)
	private String dstc;

	@Column(name = "line", length = 5)
	private String line;

	@Id
	@Column(name = "descripbe", length = 30)
	private String descripbe;

	public Line() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getOrac() {
		return orac;
	}

	public String getDstc() {
		return dstc;
	}

	public String getLine() {
		return line;
	}

	public String getDescripbe() {
		return descripbe;
	}

	public void setOrac(String orac) {
		this.orac = orac;
	}

	public void setDstc(String dstc) {
		this.dstc = dstc;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public void setDescripbe(String descripbe) {
		this.descripbe = descripbe;
	}

}
