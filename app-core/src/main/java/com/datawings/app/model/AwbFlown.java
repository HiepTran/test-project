package com.datawings.app.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "awb_flown")
@SequenceGenerator(name = "seq_awb_flown", sequenceName = "awb_flown_awb_flown_id_seq", allocationSize = 1)
public class AwbFlown extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_awb_flown")
	@Column(name = "awb_flown_id", unique = true, nullable = false)
	private Integer awbFlownId;

	@Column(name = "manifest_no")
	private String manifestNo;

	@Column(name = "lta", length = 8)
	private String lta;

	@Column(name = "coupon")
	private Integer coupon;

	@Column(name = "item")
	private String item;

	@Column(name = "record_id", length = 3)
	private String recordId;

	@Column(name = "branch_office", length = 1)
	private String branchOffice;

	@Column(name = "vat_indicator", length = 1)
	private String vatIndicator;

	@Column(name = "tacn", length = 3)
	private String tacn;

	@Column(name = "isoc", length = 2)
	private String isoc;

	@Column(name = "cutp", length = 3)
	private String cutp;

	@Column(name = "number_modular", length = 1)
	private String numberModular;

	@Column(name = "ship_name")
	private String shipName;

	@Column(name = "ship_address1")
	private String shipAddress1;

	@Column(name = "ship_address2")
	private String shipAddress2;

	@Column(name = "ship_address3")
	private String shipAddress3;

	@Column(name = "consignee_name")
	private String consigneeName;

	@Column(name = "consignee_address1")
	private String consigneeAddress1;

	@Column(name = "consignee_address2")
	private String consigneeAddress2;

	@Column(name = "consignee_address3")
	private String consigneeAddress3;

	@Column(name = "agtn", length = 11)
	private String agtn;

	@Column(name = "agtn_name")
	private String agtnName;

	@Column(name = "agtn_code", length = 10)
	private String agtnCode;

	@Column(name = "agtn_iata", length = 7)
	private String agtnIata;

	@Column(name = "agtn_gsa")
	private String agtnGsa;

	@Column(name = "check_digit", length = 1)
	private String checkDigit;

	@Column(name = "orig", length = 3)
	private String orig;

	@Column(name = "dest", length = 3)
	private String dest;

	@Column(name = "use_indicator", length = 1)
	private String useIndicator;

	@Column(name = "late_indicator", length = 1)
	private String lateIndicator;

	@Column(name = "date_execute")
	private Date dateExecute;

	@Column(name = "weight_indicator", length = 1)
	private String weightIndicator;

	@Column(name = "weight_gross")
	private Double weightGross;

	@Column(name = "weight_charge")
	private Double weightCharge;

	@Column(name = "unit_cass")
	private Double unitCass;

	@Column(name = "charge_indicator", length = 1)
	private String chargeIndicator;

	@Column(name = "other_indicator", length = 1)
	private String otherIndicator;

	@Column(name = "weight_pp")
	private Double weightPp;

	@Column(name = "valuation_pp")
	private Double valuationPp;

	@Column(name = "carrier_pp")
	private Double carrierPp;

	@Column(name = "agent_pp")
	private Double agentPp;

	@Column(name = "weight_cc")
	private Double weightCc;

	@Column(name = "valuation_cc")
	private Double valuationCc;

	@Column(name = "carrier_cc")
	private Double carrierCc;

	@Column(name = "agent_cc")
	private Double agentCc;

	@Column(name = "comm_percent")
	private Double commPercent;

	@Column(name = "comm")
	private Double comm;

	@Column(name = "tax_airline_indicator", length = 1)
	private String taxAirlineIndicator;

	@Column(name = "agent_ref")
	private String agent_ref;

	@Column(name = "date_accept")
	private Date dateAccept;

	@Column(name = "rate")
	private Double rate;

	@Column(name = "discount")
	private Double discount;

	@Column(name = "tax_airline")
	private Double taxAirline;

	@Column(name = "taxe_agent")
	private Double taxeAgent;

	@Column(name = "handling_infor")
	private String handlingInfor;

	@Column(name = "description_admin")
	private String descriptionAdmin;

	@Column(name = "descript_user")
	private String descriptUser;

	@Column(name = "channel", length = 3)
	private String channel;

	@Column(name = "km")
	private Double km;

	@Column(name = "vat_percent")
	private Double vatPercent;

	@Column(name = "vat")
	private Double vat;

	@Column(name = "net")
	private Double net;

	@Column(name = "line")
	private String line;

	@Column(name = "mode_payment")
	private String modePayment;

	@Column(name = "nb_cart")
	private String nbCart;

	@Column(name = "date_exp")
	private String dateExp;

	@Column(name = "name_cart")
	private String nameCart;

	@Column(name = "rdv")
	private String rdv;

	@Column(name = "id_rdv")
	private String idRdv;

	@Column(name = "flight_number")
	private String flightNumber;

	@Column(name = "flight_date")
	private Date flightDate;

	@Column(name = "immat")
	private String immat;

	@Column(name = "declared_carrige")
	private String declaredCarrige;

	@Column(name = "declared_custom")
	private String declaredCustom;

	@Column(name = "total_pp")
	private Double totalPp;

	@Column(name = "total_cc")
	private Double totalCc;

	@Column(name = "roe")
	private Double roe;

	@Column(name = "status_flown")
	private String statusFlown;

	@Column(name = "content_manifest")
	private String contentManifest;

	@Column(name = "isoc_lc", length = 2)
	private String isocLc;

	@Column(name = "cutp_lc", length = 3)
	private String cutpLc;

	@Column(name = "weight_gross_lc")
	private Double weightGrossLc;

	@Column(name = "weight_indicator_lc", length = 1)
	private String weightIndicatorLc;

	@Column(name = "weight_pp_lc")
	private Double weightPpLc;

	@Column(name = "valuation_pp_lc")
	private Double valuationPpLc;

	@Column(name = "carrier_pp_lc")
	private Double carrierPpLc;

	@Column(name = "agent_pp_lc")
	private Double agentPpLc;

	@Column(name = "weight_cc_lc")
	private Double weightCcLc;

	@Column(name = "valuation_cc_lc")
	private Double valuationCcLc;

	@Column(name = "carrier_cc_lc")
	private Double carrierCcLc;

	@Column(name = "agent_cc_lc")
	private Double agentCcLc;

	@Column(name = "comm_lc")
	private Double commLc;

	@Column(name = "discount_lc")
	private Double discountLc;

	@Column(name = "tax_airline_lc")
	private Double taxAirlineLc;

	@Column(name = "taxe_agent_lc")
	private Double taxeAgentLc;

	@Column(name = "total_pp_lc")
	private Double totalPpLc;

	@Column(name = "total_cc_lc")
	private Double totalCcLc;

	@Column(name = "created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	@OneToMany(mappedBy = "awbFlown", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@OrderBy("coupon")
	private Set<AwbFlownDetail> awbFlownDetail = new HashSet<AwbFlownDetail>(0);

	@OneToMany(mappedBy = "awbFlown", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@OrderBy("taxCode")
	private Set<AwbFlownTax> awbFlownTax = new HashSet<AwbFlownTax>(0);

	public AwbFlown() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public Set<AwbFlownDetail> getAwbFlownDetail() {
		return awbFlownDetail;
	}

	public void setAwbFlownDetail(Set<AwbFlownDetail> awbFlownDetail) {
		this.awbFlownDetail = awbFlownDetail;
	}

	public Set<AwbFlownTax> getAwbFlownTax() {
		return awbFlownTax;
	}

	public void setAwbFlownTax(Set<AwbFlownTax> awbFlownTax) {
		this.awbFlownTax = awbFlownTax;
	}

	public String getStatusFlown() {
		return statusFlown;
	}

	public void setStatusFlown(String statusFlown) {
		this.statusFlown = statusFlown;
	}

	public String getContentManifest() {
		return contentManifest;
	}

	public void setContentManifest(String contentManifest) {
		this.contentManifest = contentManifest;
	}

	public Double getVatPercent() {
		return vatPercent;
	}

	public Double getVat() {
		return vat;
	}

	public String getModePayment() {
		return modePayment;
	}

	public String getNbCart() {
		return nbCart;
	}

	public String getNameCart() {
		return nameCart;
	}

	public void setModePayment(String modePayment) {
		this.modePayment = modePayment;
	}

	public void setNbCart(String nbCart) {
		this.nbCart = nbCart;
	}

	public String getDateExp() {
		return dateExp;
	}

	public void setDateExp(String dateExp) {
		this.dateExp = dateExp;
	}

	public void setNameCart(String nameCart) {
		this.nameCart = nameCart;
	}

	public Double getNet() {
		return net;
	}

	public void setVatPercent(Double vatPercent) {
		this.vatPercent = vatPercent;
	}

	public void setVat(Double vat) {
		this.vat = vat;
	}

	public void setNet(Double net) {
		this.net = net;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String getAgtnIata() {
		return agtnIata;
	}

	public String getAgtnGsa() {
		return agtnGsa;
	}

	public void setAgtnIata(String agtnIata) {
		this.agtnIata = agtnIata;
	}

	public void setAgtnGsa(String agtnGsa) {
		this.agtnGsa = agtnGsa;
	}

	public Double getKm() {
		return km;
	}

	public void setKm(Double km) {
		this.km = km;
	}

	public String getImmat() {
		return immat;
	}

	public void setImmat(String immat) {
		this.immat = immat;
	}

	public Double getTotalPpLc() {
		return totalPpLc;
	}

	public Double getTotalCcLc() {
		return totalCcLc;
	}

	public void setTotalPpLc(Double totalPpLc) {
		this.totalPpLc = totalPpLc;
	}

	public void setTotalCcLc(Double totalCcLc) {
		this.totalCcLc = totalCcLc;
	}

	public Double getTotalPp() {
		return totalPp;
	}

	public Double getTotalCc() {
		return totalCc;
	}

	public void setTotalPp(Double totalPp) {
		this.totalPp = totalPp;
	}

	public void setTotalCc(Double totalCc) {
		this.totalCc = totalCc;
	}

	public String getDeclaredCarrige() {
		return declaredCarrige;
	}

	public String getDeclaredCustom() {
		return declaredCustom;
	}

	public void setDeclaredCarrige(String declaredCarrige) {
		this.declaredCarrige = declaredCarrige;
	}

	public void setDeclaredCustom(String declaredCustom) {
		this.declaredCustom = declaredCustom;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(String branchOffice) {
		this.branchOffice = branchOffice;
	}

	public String getVatIndicator() {
		return vatIndicator;
	}

	public void setVatIndicator(String vatIndicator) {
		this.vatIndicator = vatIndicator;
	}

	public String getTacn() {
		return tacn;
	}

	public void setTacn(String tacn) {
		this.tacn = tacn;
	}

	public String getIsoc() {
		return isoc;
	}

	public void setIsoc(String isoc) {
		this.isoc = isoc;
	}

	public String getCutp() {
		return cutp;
	}

	public void setCutp(String cutp) {
		this.cutp = cutp;
	}

	public String getNumberModular() {
		return numberModular;
	}

	public void setNumberModular(String numberModular) {
		this.numberModular = numberModular;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public String getShipAddress1() {
		return shipAddress1;
	}

	public void setShipAddress1(String shipAddress1) {
		this.shipAddress1 = shipAddress1;
	}

	public String getShipAddress2() {
		return shipAddress2;
	}

	public void setShipAddress2(String shipAddress2) {
		this.shipAddress2 = shipAddress2;
	}

	public String getShipAddress3() {
		return shipAddress3;
	}

	public void setShipAddress3(String shipAddress3) {
		this.shipAddress3 = shipAddress3;
	}

	public String getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	public String getConsigneeAddress1() {
		return consigneeAddress1;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public void setConsigneeAddress1(String consigneeAddress1) {
		this.consigneeAddress1 = consigneeAddress1;
	}

	public String getConsigneeAddress2() {
		return consigneeAddress2;
	}

	public void setConsigneeAddress2(String consigneeAddress2) {
		this.consigneeAddress2 = consigneeAddress2;
	}

	public String getConsigneeAddress3() {
		return consigneeAddress3;
	}

	public void setConsigneeAddress3(String consigneeAddress3) {
		this.consigneeAddress3 = consigneeAddress3;
	}

	public String getAgtn() {
		return agtn;
	}

	public void setAgtn(String agtn) {
		this.agtn = agtn;
	}

	public String getAgtnName() {
		return agtnName;
	}

	public void setAgtnName(String agtnName) {
		this.agtnName = agtnName;
	}

	public String getOrig() {
		return orig;
	}

	public void setOrig(String orig) {
		this.orig = orig;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getUseIndicator() {
		return useIndicator;
	}

	public void setUseIndicator(String useIndicator) {
		this.useIndicator = useIndicator;
	}

	public String getLateIndicator() {
		return lateIndicator;
	}

	public void setLateIndicator(String lateIndicator) {
		this.lateIndicator = lateIndicator;
	}

	public Date getDateExecute() {
		return dateExecute;
	}

	public void setDateExecute(Date dateExecute) {
		this.dateExecute = dateExecute;
	}

	public String getWeightIndicator() {
		return weightIndicator;
	}

	public void setWeightIndicator(String weightIndicator) {
		this.weightIndicator = weightIndicator;
	}

	public Double getWeightGross() {
		return weightGross;
	}

	public void setWeightGross(Double weightGross) {
		this.weightGross = weightGross;
	}

	public String getChargeIndicator() {
		return chargeIndicator;
	}

	public String getOtherIndicator() {
		return otherIndicator;
	}

	public void setChargeIndicator(String chargeIndicator) {
		this.chargeIndicator = chargeIndicator;
	}

	public void setOtherIndicator(String otherIndicator) {
		this.otherIndicator = otherIndicator;
	}

	public Double getWeightPp() {
		return weightPp;
	}

	public void setWeightPp(Double weightPp) {
		this.weightPp = weightPp;
	}

	public Double getValuationPp() {
		return valuationPp;
	}

	public void setValuationPp(Double valuationPp) {
		this.valuationPp = valuationPp;
	}

	public Double getCarrierPp() {
		return carrierPp;
	}

	public void setCarrierPp(Double carrierPp) {
		this.carrierPp = carrierPp;
	}

	public Double getAgentPp() {
		return agentPp;
	}

	public void setAgentPp(Double agentPp) {
		this.agentPp = agentPp;
	}

	public Double getWeightCc() {
		return weightCc;
	}

	public void setWeightCc(Double weightCc) {
		this.weightCc = weightCc;
	}

	public Double getValuationCc() {
		return valuationCc;
	}

	public void setValuationCc(Double valuationCc) {
		this.valuationCc = valuationCc;
	}

	public Double getCarrierCc() {
		return carrierCc;
	}

	public void setCarrierCc(Double carrierCc) {
		this.carrierCc = carrierCc;
	}

	public Double getAgentCc() {
		return agentCc;
	}

	public void setAgentCc(Double agentCc) {
		this.agentCc = agentCc;
	}

	public Double getCommPercent() {
		return commPercent;
	}

	public void setCommPercent(Double commPercent) {
		this.commPercent = commPercent;
	}

	public Double getComm() {
		return comm;
	}

	public void setComm(Double comm) {
		this.comm = comm;
	}

	public String getTaxAirlineIndicator() {
		return taxAirlineIndicator;
	}

	public void setTaxAirlineIndicator(String taxAirlineIndicator) {
		this.taxAirlineIndicator = taxAirlineIndicator;
	}

	public String getAgent_ref() {
		return agent_ref;
	}

	public void setAgent_ref(String agent_ref) {
		this.agent_ref = agent_ref;
	}

	public Date getDateAccept() {
		return dateAccept;
	}

	public void setDateAccept(Date dateAccept) {
		this.dateAccept = dateAccept;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getTaxAirline() {
		return taxAirline;
	}

	public void setTaxAirline(Double taxAirline) {
		this.taxAirline = taxAirline;
	}

	public Double getTaxeAgent() {
		return taxeAgent;
	}

	public void setTaxeAgent(Double taxeAgent) {
		this.taxeAgent = taxeAgent;
	}

	public String getHandlingInfor() {
		return handlingInfor;
	}

	public void setHandlingInfor(String handlingInfor) {
		this.handlingInfor = handlingInfor;
	}

	public String getDescriptionAdmin() {
		return descriptionAdmin;
	}

	public void setDescriptionAdmin(String descriptionAdmin) {
		this.descriptionAdmin = descriptionAdmin;
	}

	public String getDescriptUser() {
		return descriptUser;
	}

	public void setDescriptUser(String descriptUser) {
		this.descriptUser = descriptUser;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public Double getRoe() {
		return roe;
	}

	public void setRoe(Double roe) {
		this.roe = roe;
	}

	public String getIsocLc() {
		return isocLc;
	}

	public void setIsocLc(String isocLc) {
		this.isocLc = isocLc;
	}

	public Double getUnitCass() {
		return unitCass;
	}

	public void setUnitCass(Double unitCass) {
		this.unitCass = unitCass;
	}

	public String getCutpLc() {
		return cutpLc;
	}

	public void setCutpLc(String cutpLc) {
		this.cutpLc = cutpLc;
	}

	public Double getWeightGrossLc() {
		return weightGrossLc;
	}

	public void setWeightGrossLc(Double weightGrossLc) {
		this.weightGrossLc = weightGrossLc;
	}

	public String getWeightIndicatorLc() {
		return weightIndicatorLc;
	}

	public void setWeightIndicatorLc(String weightIndicatorLc) {
		this.weightIndicatorLc = weightIndicatorLc;
	}

	public Double getWeightPpLc() {
		return weightPpLc;
	}

	public void setWeightPpLc(Double weightPpLc) {
		this.weightPpLc = weightPpLc;
	}

	public Double getValuationPpLc() {
		return valuationPpLc;
	}

	public void setValuationPpLc(Double valuationPpLc) {
		this.valuationPpLc = valuationPpLc;
	}

	public Double getCarrierPpLc() {
		return carrierPpLc;
	}

	public void setCarrierPpLc(Double carrierPpLc) {
		this.carrierPpLc = carrierPpLc;
	}

	public Double getAgentPpLc() {
		return agentPpLc;
	}

	public void setAgentPpLc(Double agentPpLc) {
		this.agentPpLc = agentPpLc;
	}

	public Double getWeightCcLc() {
		return weightCcLc;
	}

	public void setWeightCcLc(Double weightCcLc) {
		this.weightCcLc = weightCcLc;
	}

	public Double getValuationCcLc() {
		return valuationCcLc;
	}

	public void setValuationCcLc(Double valuationCcLc) {
		this.valuationCcLc = valuationCcLc;
	}

	public Double getCarrierCcLc() {
		return carrierCcLc;
	}

	public void setCarrierCcLc(Double carrierCcLc) {
		this.carrierCcLc = carrierCcLc;
	}

	public Double getCommLc() {
		return commLc;
	}

	public void setCommLc(Double commLc) {
		this.commLc = commLc;
	}

	public Double getDiscountLc() {
		return discountLc;
	}

	public void setDiscountLc(Double discountLc) {
		this.discountLc = discountLc;
	}

	public Double getTaxAirlineLc() {
		return taxAirlineLc;
	}

	public void setTaxAirlineLc(Double taxAirlineLc) {
		this.taxAirlineLc = taxAirlineLc;
	}

	public Double getTaxeAgentLc() {
		return taxeAgentLc;
	}

	public void setTaxeAgentLc(Double taxeAgentLc) {
		this.taxeAgentLc = taxeAgentLc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getAgtnCode() {
		return agtnCode;
	}

	public void setAgtnCode(String agtnCode) {
		this.agtnCode = agtnCode;
	}

	public String getCheckDigit() {
		return checkDigit;
	}

	public void setCheckDigit(String checkDigit) {
		this.checkDigit = checkDigit;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getRdv() {
		return rdv;
	}

	public void setRdv(String rdv) {
		this.rdv = rdv;
	}

	public Double getAgentCcLc() {
		return agentCcLc;
	}

	public void setAgentCcLc(Double agentCcLc) {
		this.agentCcLc = agentCcLc;
	}

	public String getIdRdv() {
		return idRdv;
	}

	public Integer getAwbFlownId() {
		return awbFlownId;
	}

	public void setAwbFlownId(Integer awbFlownId) {
		this.awbFlownId = awbFlownId;
	}

	public String getManifestNo() {
		return manifestNo;
	}

	public void setManifestNo(String manifestNo) {
		this.manifestNo = manifestNo;
	}

	public String getLta() {
		return lta;
	}

	public void setLta(String lta) {
		this.lta = lta;
	}

	public Integer getCoupon() {
		return coupon;
	}

	public void setCoupon(Integer coupon) {
		this.coupon = coupon;
	}

	public void setIdRdv(String idRdv) {
		this.idRdv = idRdv;
	}

	public Double getWeightCharge() {
		return weightCharge;
	}

	public void setWeightCharge(Double weightCharge) {
		this.weightCharge = weightCharge;
	}

}
