package com.datawings.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "cdo")
public class Cdo extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "lta", length = 8, nullable = false)
	private String lta;

	@Column(name = "record_id", length = 3)
	private String recordId;

	@Column(name = "branch_office", length = 1)
	private String branchOffice;

	@Column(name = "vat_indicator", length = 1)
	private String vatIndicator;

	@Column(name = "tacn", length = 3)
	private String tacn;

	@Column(name = "isoc", length = 2)
	private String isoc;

	@Column(name = "cutp", length = 3)
	private String cutp;

	@Column(name = "number_modular", length = 1)
	private String numberModular;
	@Column(name = "agtn", length = 11)
	private String agtn;

	@Column(name = "agtn_name")
	private String agtnName;

	@Column(name = "orig", length = 3)
	private String orig;

	@Column(name = "dest", length = 3)
	private String dest;

	@Column(name = "cd_number", length = 3)
	private String cdNumber;

	@Column(name = "rate")
	private Double rate;

	@Column(name = "date_execute")
	private Date dateExecute;

	@Column(name = "weight_type", length = 1)
	private String weightType;

	@Column(name = "weight_charge")
	private Double weightCharge;

	@Column(name = "valuation_type", length = 1)
	private String valuation_type;

	@Column(name = "valuation_charge")
	private Double valuationCharge;

	@Column(name = "tax_type", length = 1)
	private String taxType;

	@Column(name = "tax")
	private Double tax;

	@Column(name = "tax_agent_type", length = 1)
	private String taxAgentType;

	@Column(name = "tax_agent")
	private Double taxAgent;

	@Column(name = "tax_carrier_type", length = 1)
	private String taxCarrierType;

	@Column(name = "tax_carrier")
	private Double taxCarrier;

	@Column(name = "vat_charge")
	private Double vatCharge;

	@Column(name = "comm")
	private Double comm;

	@Column(name = "vat_comm")
	private Double vatComm;

	@Column(name = "discount")
	private Double discount;

	@Column(name = "discount_indicator", length = 1)
	private String discountIndicator;

	@Column(name = "weight_indicator", length = 1)
	private String weightIndicator;

	@Column(name = "weight")
	private Double weight;

	@Column(name = "description_admin")
	private String descriptionAdmin;

	@Column(name = "description_user", length = 1)
	private String descriptionUser;

	@Column(name = "id_header")
	private String idHeader;

	@Column(name = "roe")
	private Double roe;

	@Column(name = "cutp_lc", length = 3)
	private String cutpLc;

	@Column(name = "weight_charge_lc")
	private Double weightChargeLc;

	@Column(name = "valuation_charge_lc")
	private Double valuationChargeLc;

	@Column(name = "tax_lc")
	private Double taxLc;

	@Column(name = "tax_agent_lc")
	private Double taxAgentLc;

	@Column(name = "tax_carrier_lc")
	private Double taxCarrierLc;

	@Column(name = "vat_charge_lc")
	private Double vatChargeLc;

	@Column(name = "comm_lc")
	private Double commLc;

	@Column(name = "vat_comm_lc")
	private Double vatCommLc;

	@Column(name = "discount_lc")
	private Double discountLc;

	@Column(name = "weight_indicator_lc", length = 1)
	private String weightIndicatorLc;

	@Column(name = "weight_lc")
	private Double weightLc;

	@Column(name = "created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	public Cdo() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getLta() {
		return lta;
	}

	public void setLta(String lta) {
		this.lta = lta;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(String branchOffice) {
		this.branchOffice = branchOffice;
	}

	public String getVatIndicator() {
		return vatIndicator;
	}

	public void setVatIndicator(String vatIndicator) {
		this.vatIndicator = vatIndicator;
	}

	public String getTacn() {
		return tacn;
	}

	public void setTacn(String tacn) {
		this.tacn = tacn;
	}

	public String getIsoc() {
		return isoc;
	}

	public void setIsoc(String isoc) {
		this.isoc = isoc;
	}

	public String getCutp() {
		return cutp;
	}

	public void setCutp(String cutp) {
		this.cutp = cutp;
	}

	public String getNumberModular() {
		return numberModular;
	}

	public void setNumberModular(String numberModular) {
		this.numberModular = numberModular;
	}

	public String getAgtn() {
		return agtn;
	}

	public void setAgtn(String agtn) {
		this.agtn = agtn;
	}

	public String getAgtnName() {
		return agtnName;
	}

	public void setAgtnName(String agtnName) {
		this.agtnName = agtnName;
	}

	public String getOrig() {
		return orig;
	}

	public void setOrig(String orig) {
		this.orig = orig;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getCdNumber() {
		return cdNumber;
	}

	public void setCdNumber(String cdNumber) {
		this.cdNumber = cdNumber;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Date getDateExecute() {
		return dateExecute;
	}

	public void setDateExecute(Date dateExecute) {
		this.dateExecute = dateExecute;
	}

	public String getWeightType() {
		return weightType;
	}

	public void setWeightType(String weightType) {
		this.weightType = weightType;
	}

	public Double getWeightCharge() {
		return weightCharge;
	}

	public void setWeightCharge(Double weightCharge) {
		this.weightCharge = weightCharge;
	}

	public String getValuation_type() {
		return valuation_type;
	}

	public void setValuation_type(String valuation_type) {
		this.valuation_type = valuation_type;
	}

	public Double getValuationCharge() {
		return valuationCharge;
	}

	public void setValuationCharge(Double valuationCharge) {
		this.valuationCharge = valuationCharge;
	}

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public String getTaxAgentType() {
		return taxAgentType;
	}

	public void setTaxAgentType(String taxAgentType) {
		this.taxAgentType = taxAgentType;
	}

	public Double getTaxAgent() {
		return taxAgent;
	}

	public void setTaxAgent(Double taxAgent) {
		this.taxAgent = taxAgent;
	}

	public String getTaxCarrierType() {
		return taxCarrierType;
	}

	public void setTaxCarrierType(String taxCarrierType) {
		this.taxCarrierType = taxCarrierType;
	}

	public Double getTaxCarrier() {
		return taxCarrier;
	}

	public void setTaxCarrier(Double taxCarrier) {
		this.taxCarrier = taxCarrier;
	}

	public Double getVatCharge() {
		return vatCharge;
	}

	public void setVatCharge(Double vatCharge) {
		this.vatCharge = vatCharge;
	}

	public Double getComm() {
		return comm;
	}

	public void setComm(Double comm) {
		this.comm = comm;
	}

	public Double getVatComm() {
		return vatComm;
	}

	public void setVatComm(Double vatComm) {
		this.vatComm = vatComm;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public String getDiscountIndicator() {
		return discountIndicator;
	}

	public void setDiscountIndicator(String discountIndicator) {
		this.discountIndicator = discountIndicator;
	}

	public String getWeightIndicator() {
		return weightIndicator;
	}

	public void setWeightIndicator(String weightIndicator) {
		this.weightIndicator = weightIndicator;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getDescriptionAdmin() {
		return descriptionAdmin;
	}

	public void setDescriptionAdmin(String descriptionAdmin) {
		this.descriptionAdmin = descriptionAdmin;
	}

	public String getDescriptionUser() {
		return descriptionUser;
	}

	public void setDescriptionUser(String descriptionUser) {
		this.descriptionUser = descriptionUser;
	}


	public String getIdHeader() {
		return idHeader;
	}

	public void setIdHeader(String idHeader) {
		this.idHeader = idHeader;
	}

	public Double getRoe() {
		return roe;
	}

	public void setRoe(Double roe) {
		this.roe = roe;
	}

	public String getCutpLc() {
		return cutpLc;
	}

	public void setCutpLc(String cutpLc) {
		this.cutpLc = cutpLc;
	}

	public Double getWeightChargeLc() {
		return weightChargeLc;
	}

	public void setWeightChargeLc(Double weightChargeLc) {
		this.weightChargeLc = weightChargeLc;
	}

	public Double getValuationChargeLc() {
		return valuationChargeLc;
	}

	public void setValuationChargeLc(Double valuationChargeLc) {
		this.valuationChargeLc = valuationChargeLc;
	}

	public Double getTaxLc() {
		return taxLc;
	}

	public void setTaxLc(Double taxLc) {
		this.taxLc = taxLc;
	}

	public Double getTaxAgentLc() {
		return taxAgentLc;
	}

	public void setTaxAgentLc(Double taxAgentLc) {
		this.taxAgentLc = taxAgentLc;
	}

	public Double getTaxCarrierLc() {
		return taxCarrierLc;
	}

	public void setTaxCarrierLc(Double taxCarrierLc) {
		this.taxCarrierLc = taxCarrierLc;
	}

	public Double getVatChargeLc() {
		return vatChargeLc;
	}

	public void setVatChargeLc(Double vatChargeLc) {
		this.vatChargeLc = vatChargeLc;
	}

	public Double getCommLc() {
		return commLc;
	}

	public void setCommLc(Double commLc) {
		this.commLc = commLc;
	}

	public Double getVatCommLc() {
		return vatCommLc;
	}

	public void setVatCommLc(Double vatCommLc) {
		this.vatCommLc = vatCommLc;
	}

	public Double getDiscountLc() {
		return discountLc;
	}

	public void setDiscountLc(Double discountLc) {
		this.discountLc = discountLc;
	}

	public String getWeightIndicatorLc() {
		return weightIndicatorLc;
	}

	public void setWeightIndicatorLc(String weightIndicatorLc) {
		this.weightIndicatorLc = weightIndicatorLc;
	}

	public Double getWeightLc() {
		return weightLc;
	}

	public void setWeightLc(Double weightLc) {
		this.weightLc = weightLc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
