package com.datawings.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "bsr")
public class Bsr extends Base {
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private BsrId bsrId;
	
	@Column(name = "rate")
	private Double rate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_end")
	private Date dateEnd;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "timestamp_c")
	private Date timeStamp_C;

	public Bsr() {
		init();
	}
	
	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public BsrId getBsrId() {
		return bsrId;
	}

	public void setBsrId(BsrId bsrId) {
		this.bsrId = bsrId;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public Date getTimeStamp_C() {
		return timeStamp_C;
	}

	public void setTimeStamp_C(Date timeStamp_C) {
		this.timeStamp_C = timeStamp_C;
	}
}
