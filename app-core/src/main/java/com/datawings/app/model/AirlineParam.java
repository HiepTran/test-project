package com.datawings.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "airline_param")
public class AirlineParam extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "airline_code", length = 3, nullable = false)
	private String airlineCode;

	@Column(name = "isoc", length = 2)
	private String isoc;

	@Column(name = "currency", length = 3)
	private String currency;

	@Column(name = "isc")
	private Double isc;

	@Column(name = "vat")
	private Double vat;

	public AirlineParam() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getIsoc() {
		return isoc;
	}

	public void setIsoc(String isoc) {
		this.isoc = isoc;
	}

	public String getAirlineCode() {
		return airlineCode;
	}

	public String getCurrency() {
		return currency;
	}

	public Double getIsc() {
		return isc;
	}

	public Double getVat() {
		return vat;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setIsc(Double isc) {
		this.isc = isc;
	}

	public void setVat(Double vat) {
		this.vat = vat;
	}

}
