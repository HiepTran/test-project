package com.datawings.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "crit_isoc")
public class CritIsoc extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "isoc", length = 2)
	private String isoc;

	@Column(name = "tacn", length = 3)
	private String tacn;

	public CritIsoc() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getIsoc() {
		return isoc;
	}

	public String getTacn() {
		return tacn;
	}

	public void setIsoc(String isoc) {
		this.isoc = isoc;
	}

	public void setTacn(String tacn) {
		this.tacn = tacn;
	}

}
