package com.datawings.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "awb_detail")
public class AwbDetail extends Base {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AwbDetailId id;

	@Column(name = "quantity")
	private Integer quantity;

	@Column(name = "weight_indicator", length = 1)
	private String weightIndicator;

	@Column(name = "weight_gross")
	private Double weightGross;

	@Column(name = "rate_class", length = 1)
	private String rateClass;

	@Column(name = "weight_charge")
	private Double weightCharge;

	@Column(name = "unit")
	private Double unit;

	@Column(name = "unit_cass")
	private Double unitCass;

	@Column(name = "amount")
	private Double amount;

	@Column(name = "description")
	private String description;

	@Column(name = "weight_indicator_lc", length = 1)
	private String weightIndicatorLc;

	@Column(name = "weight_gross_lc")
	private Double weightGrossLc;

	@Column(name = "weight_charge_lc")
	private Double weightChargeLc;

	@Column(name = "unit_lc")
	private Double unitLc;

	@Column(name = "amount_lc")
	private Double amountLc;

	@Column(name = "created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "lta", nullable = false, insertable = false, updatable = false)
	private Awb awb;

	public AwbDetail() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
		this.id = new AwbDetailId();
	}

	public AwbDetailId getId() {
		return id;
	}

	public void setId(AwbDetailId id) {
		this.id = id;
	}

	@JsonIgnore
	public Awb getAwb() {
		return awb;
	}

	public void setAwb(Awb awb) {
		this.awb = awb;
	}

	public Double getUnitCass() {
		return unitCass;
	}

	public void setUnitCass(Double unitCass) {
		this.unitCass = unitCass;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getWeightIndicator() {
		return weightIndicator;
	}

	public void setWeightIndicator(String weightIndicator) {
		this.weightIndicator = weightIndicator;
	}

	public Double getWeightGross() {
		return weightGross;
	}

	public void setWeightGross(Double weightGross) {
		this.weightGross = weightGross;
	}

	public String getRateClass() {
		return rateClass;
	}

	public void setRateClass(String rateClass) {
		this.rateClass = rateClass;
	}

	public Double getWeightCharge() {
		return weightCharge;
	}

	public void setWeightCharge(Double weightCharge) {
		this.weightCharge = weightCharge;
	}

	public Double getUnit() {
		return unit;
	}

	public void setUnit(Double unit) {
		this.unit = unit;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWeightIndicatorLc() {
		return weightIndicatorLc;
	}

	public void setWeightIndicatorLc(String weightIndicatorLc) {
		this.weightIndicatorLc = weightIndicatorLc;
	}

	public Double getWeightGrossLc() {
		return weightGrossLc;
	}

	public void setWeightGrossLc(Double weightGrossLc) {
		this.weightGrossLc = weightGrossLc;
	}

	public Double getWeightChargeLc() {
		return weightChargeLc;
	}

	public void setWeightChargeLc(Double weightChargeLc) {
		this.weightChargeLc = weightChargeLc;
	}

	public Double getUnitLc() {
		return unitLc;
	}

	public void setUnitLc(Double unitLc) {
		this.unitLc = unitLc;
	}

	public Double getAmountLc() {
		return amountLc;
	}

	public void setAmountLc(Double amountLc) {
		this.amountLc = amountLc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
