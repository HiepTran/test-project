package com.datawings.app.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.datawings.app.common.BeanUtil;

@Embeddable
public class LegKmId implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "isoc_from")
	private String isocFrom;
	
	@Column(name = "airport_from")
	private String airportFrom;
	
	@Column(name = "isoc_to")
	private String isocTo;
	
	@Column(name = "airport_to")
	private String airportTo;

	public LegKmId() {
		super();
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getIsocFrom() {
		return isocFrom;
	}

	public void setIsocFrom(String isocFrom) {
		this.isocFrom = isocFrom;
	}

	public String getAirportFrom() {
		return airportFrom;
	}

	public void setAirportFrom(String airportFrom) {
		this.airportFrom = airportFrom;
	}

	public String getIsocTo() {
		return isocTo;
	}

	public void setIsocTo(String isocTo) {
		this.isocTo = isocTo;
	}

	public String getAirportTo() {
		return airportTo;
	}

	public void setAirportTo(String airportTo) {
		this.airportTo = airportTo;
	}
}
