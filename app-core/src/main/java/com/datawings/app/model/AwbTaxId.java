package com.datawings.app.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.datawings.app.common.BeanUtil;

@Embeddable
public class AwbTaxId implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "lta", length = 8)
	private String lta;

	@Column(name = "tax_code", length = 3)
	private String taxCode;

	public AwbTaxId() {
		super();
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getLta() {
		return lta;
	}

	public void setLta(String lta) {
		this.lta = lta;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

}
