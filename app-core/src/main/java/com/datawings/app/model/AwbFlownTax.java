package com.datawings.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "awb_flown_tax")
@SequenceGenerator(name = "seq_awb_flown_tax", sequenceName = "awb_flown_tax_awb_flown_tax_id_seq", allocationSize = 1)
public class AwbFlownTax extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_awb_flown_tax")
	@Column(name = "awb_flown_tax_id", unique = true, nullable = false)
	private Integer awbFlownTaxId;

	@Column(name = "awb_flown_id")
	private Integer awbFlownId;

	@Column(name = "manifest_no")
	private String manifestNo;

	@Column(name = "lta", length = 8)
	private String lta;

	@Column(name = "coupon")
	private Integer coupon;

	@Column(name = "tax_code", length = 3)
	private String taxCode;

	@Column(name = "tax_amount")
	private Double taxAmount;

	@Column(name = "tax_amount_lc")
	private Double taxAmountLc;

	@Column(name = "created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "awb_flown_id", nullable = false, insertable = false, updatable = false)
	private AwbFlown awbFlown;

	public AwbFlownTax() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	@JsonIgnore
	public AwbFlown getAwbFlown() {
		return awbFlown;
	}

	public Integer getAwbFlownTaxId() {
		return awbFlownTaxId;
	}

	public void setAwbFlownTaxId(Integer awbFlownTaxId) {
		this.awbFlownTaxId = awbFlownTaxId;
	}

	public Integer getAwbFlownId() {
		return awbFlownId;
	}

	public void setAwbFlownId(Integer awbFlownId) {
		this.awbFlownId = awbFlownId;
	}

	public String getManifestNo() {
		return manifestNo;
	}

	public void setManifestNo(String manifestNo) {
		this.manifestNo = manifestNo;
	}

	public String getLta() {
		return lta;
	}

	public void setLta(String lta) {
		this.lta = lta;
	}

	public Integer getCoupon() {
		return coupon;
	}

	public void setCoupon(Integer coupon) {
		this.coupon = coupon;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public void setAwbFlown(AwbFlown awbFlown) {
		this.awbFlown = awbFlown;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Double getTaxAmountLc() {
		return taxAmountLc;
	}

	public void setTaxAmountLc(Double taxAmountLc) {
		this.taxAmountLc = taxAmountLc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
