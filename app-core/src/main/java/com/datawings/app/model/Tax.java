package com.datawings.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "tax")
@SequenceGenerator(name = "seq_tax", sequenceName = "tax_id_seq", allocationSize = 1)
public class Tax extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tax")
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "isoc")
	private String isoc;

	@Column(name = "code")
	private String code;

	@Column(name = "sub_code")
	private String subCode;

	@Column(name = "item")
	private String item;

	@Column(name = "cutp", length = 3)
	private String cutp;

	@Column(name = "unit")
	private String unit;

	@Column(name = "amount")
	private Double amount;

	@Column(name = "description")
	private String description;

	@Column(name = "date_start")
	private Date dateStart;

	@Column(name = "date_end", length = 8)
	private Date dateEnd;

	@Column(name = "type")
	private String type;
	
	@Column(name = "created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	public Tax() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIsoc() {
		return isoc;
	}

	public String getCode() {
		return code;
	}

	public String getSubCode() {
		return subCode;
	}

	public String getItem() {
		return item;
	}

	public String getCutp() {
		return cutp;
	}

	public String getUnit() {
		return unit;
	}

	public Double getAmount() {
		return amount;
	}

	public String getDescription() {
		return description;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setIsoc(String isoc) {
		this.isoc = isoc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setSubCode(String subCode) {
		this.subCode = subCode;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public void setCutp(String cutp) {
		this.cutp = cutp;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
