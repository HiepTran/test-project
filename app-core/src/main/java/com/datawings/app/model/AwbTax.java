package com.datawings.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "awb_tax")
public class AwbTax extends Base {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AwbTaxId id;

	@Column(name = "tax_amount")
	private Double taxAmount;

	@Column(name = "tax_amount_lc")
	private Double taxAmountLc;

	@Column(name = "created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "lta", nullable = false, insertable = false, updatable = false)
	private Awb awb;

	public AwbTax() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
		this.id = new AwbTaxId();
	}

	@JsonIgnore
	public Awb getAwb() {
		return awb;
	}

	public void setAwb(Awb awb) {
		this.awb = awb;
	}

	public AwbTaxId getId() {
		return id;
	}

	public void setId(AwbTaxId id) {
		this.id = id;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Double getTaxAmountLc() {
		return taxAmountLc;
	}

	public void setTaxAmountLc(Double taxAmountLc) {
		this.taxAmountLc = taxAmountLc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
