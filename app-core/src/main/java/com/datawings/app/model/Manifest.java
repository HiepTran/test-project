package com.datawings.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "manifest")
public class Manifest extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "manifest_no", nullable = false)
	private String manifestNo;

	@Column(name = "tacn", length = 3)
	private String tacn;

	@Column(name = "orig", length = 3)
	private String orig;

	@Column(name = "dest", length = 3)
	private String dest;

	@Column(name = "flight_number")
	private String flightNumber;

	@Column(name = "flight_date")
	private Date flightDate;

	@Column(name = "immat")
	private String immat;

	@Column(name = "entry_number")
	private Integer entryNumber;

	@Column(name = "created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;

	public Manifest() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getImmat() {
		return immat;
	}

	public void setImmat(String immat) {
		this.immat = immat;
	}

	public String getManifestNo() {
		return manifestNo;
	}

	public String getOrig() {
		return orig;
	}

	public String getDest() {
		return dest;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public Integer getEntryNumber() {
		return entryNumber;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setManifestNo(String manifestNo) {
		this.manifestNo = manifestNo;
	}

	public String getTacn() {
		return tacn;
	}

	public void setTacn(String tacn) {
		this.tacn = tacn;
	}

	public void setOrig(String orig) {
		this.orig = orig;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public void setEntryNumber(Integer entryNumber) {
		this.entryNumber = entryNumber;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}
