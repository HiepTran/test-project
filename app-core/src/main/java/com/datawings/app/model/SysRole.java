package com.datawings.app.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "sys_role")
@SequenceGenerator(name = "seq_sys_role", sequenceName = "sys_role_id_seq", allocationSize = 1)
public class SysRole extends Base{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_sys_role")
	@Column(name = "sys_role_id", unique = true, nullable = false)
	private Integer sysRoleId;

	@Column(name = "authority", nullable = false)
	private String authority;

	@Column(name = "description", nullable = false)
	private String description;

	@Column(name = "created_on", nullable = false)
	private Date createdOn;

	@Column(name = "created_by", nullable = false)
	private String createdBy;

	@Column(name = "modified_on", nullable = false)
	private Date modifiedOn;

	@Column(name = "modified_by", nullable = false)
	private String modified;

	@ManyToMany(mappedBy = "roles")
	private Set<SysUser> users = new HashSet<SysUser>(0);
	
	public SysRole() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public Integer getSysRoleId() {
		return sysRoleId;
	}

	public void setSysRoleId(Integer sysRoleId) {
		this.sysRoleId = sysRoleId;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Set<SysUser> getUsers() {
		return users;
	}

	public void setUsers(Set<SysUser> users) {
		this.users = users;
	}
	
	
}
