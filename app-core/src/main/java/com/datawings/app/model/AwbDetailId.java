package com.datawings.app.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.datawings.app.common.BeanUtil;

@Embeddable
public class AwbDetailId implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name = "lta", length = 8)
	private String lta;

	@Column(name = "sequence")
	private Integer sequence;
	
	public AwbDetailId() {
		super();
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getLta() {
		return lta;
	}

	public void setLta(String lta) {
		this.lta = lta;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	
}
