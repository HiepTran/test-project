package com.datawings.app.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "leg_km")
public class LegKm extends Base {
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private LegKmId id;
	
	@Column(name = "km")
	private double km;
	
	@Column(name = "mile")
	private double mile;
	
	public LegKm(){
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}
	
	public LegKmId getId() {
		return id;
	}

	public void setId(LegKmId id) {
		this.id = id;
	}

	public double getKm() {
		return km;
	}

	public void setKm(double km) {
		this.km = km;
	}

	public double getMile() {
		return mile;
	}

	public void setMile(double mile) {
		this.mile = mile;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
