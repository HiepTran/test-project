package com.datawings.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.datawings.app.common.BeanUtil;

@Entity
@Table(name = "agent_gsa")
@SequenceGenerator(name = "seq_agent_gsa", sequenceName = "agent_gsa_id_seq", allocationSize = 1)
public class AgentGsa extends Base {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_agent_gsa")
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "code")
	private String code;

	@Column(name = "gsa_name")
	private String gsaName;

	@Column(name = "agtn")
	private String agtn;

	@Column(name = "comm")
	private Double comm;

	@Column(name = "date_start")
	private Date dateStart;

	@Column(name = "date_end")
	private Date dateEnd;

	@Column(name = "created_by")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date")
	private Date modifiedDate;
	
	@Transient
	private String numberAgtn;
	
	public AgentGsa() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public Integer getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getGsaName() {
		return gsaName;
	}

	public String getAgtn() {
		return agtn;
	}

	public Double getComm() {
		return comm;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setGsaName(String gsaName) {
		this.gsaName = gsaName;
	}

	public void setAgtn(String agtn) {
		this.agtn = agtn;
	}

	public void setComm(Double comm) {
		this.comm = comm;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getNumberAgtn() {
		return numberAgtn;
	}

	public void setNumberAgtn(String numberAgtn) {
		this.numberAgtn = numberAgtn;
	}

}
