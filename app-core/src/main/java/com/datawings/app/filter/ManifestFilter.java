package com.datawings.app.filter;

import java.util.Date;

import com.datawings.app.common.DateUtil;

public class ManifestFilter extends BaseFilter {

	private String manifestNo;
	private String tacn;
	private String orig;
	private String dest;
	private String carrier;
	private String flightNumber;
	private String flightDate;
	private String entryNumber;
	private String immat;
	private String dateFrom;
	private String dateTo;

	public ManifestFilter() {
		init();
	}

	public void init() {
		super.init();
		this.flightDate = DateUtil.date2String(new Date(), "dd/MM/yyyy");
	}

	public String getImmat() {
		return immat;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public void setImmat(String immat) {
		this.immat = immat;
	}

	public String getManifestNo() {
		return manifestNo;
	}

	public String getOrig() {
		return orig;
	}

	public String getDest() {
		return dest;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setManifestNo(String manifestNo) {
		this.manifestNo = manifestNo;
	}

	public void setOrig(String orig) {
		this.orig = orig;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	public String getTacn() {
		return tacn;
	}

	public void setTacn(String tacn) {
		this.tacn = tacn;
	}

	public String getEntryNumber() {
		return entryNumber;
	}

	public void setEntryNumber(String entryNumber) {
		this.entryNumber = entryNumber;
	}

}
