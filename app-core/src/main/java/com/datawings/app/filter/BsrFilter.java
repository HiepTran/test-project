package com.datawings.app.filter;

import com.datawings.app.common.BeanUtil;

public class BsrFilter extends BaseFilter {
	
	/*Filter*/
	private String currFrom;
	private String currTo;
	private String dateStart;
	private String dateEnd;
	private String table;
	
	/*Form add*/
	private String currFromAdd;
	private String currToAdd;
	private Double rateAdd;
	private String dateStartAdd;
	private String dateEndAdd;
	
	public BsrFilter() {
		super.init();
		init();
	}
	
	public void init(){
		BeanUtil.initSimplePropertyBean(this);
	}
	
	public void initFormAdd(){
		currFromAdd = "";
		currToAdd = "";
		rateAdd = 0.0;
		dateStartAdd = "";
		dateEndAdd = "";
	}

	public String getCurrFrom() {
		return currFrom;
	}

	public void setCurrFrom(String currFrom) {
		this.currFrom = currFrom;
	}

	public String getCurrTo() {
		return currTo;
	}

	public void setCurrTo(String currTo) {
		this.currTo = currTo;
	}

	public String getDateStart() {
		return dateStart;
	}

	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}

	public String getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getCurrFromAdd() {
		return currFromAdd;
	}

	public void setCurrFromAdd(String currFromAdd) {
		this.currFromAdd = currFromAdd;
	}

	public String getCurrToAdd() {
		return currToAdd;
	}

	public void setCurrToAdd(String currToAdd) {
		this.currToAdd = currToAdd;
	}

	public Double getRateAdd() {
		return rateAdd;
	}

	public void setRateAdd(Double rateAdd) {
		this.rateAdd = rateAdd;
	}

	public String getDateStartAdd() {
		return dateStartAdd;
	}

	public void setDateStartAdd(String dateStartAdd) {
		this.dateStartAdd = dateStartAdd;
	}

	public String getDateEndAdd() {
		return dateEndAdd;
	}

	public void setDateEndAdd(String dateEndAdd) {
		this.dateEndAdd = dateEndAdd;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}
	
}
