package com.datawings.app.filter;

import java.util.Date;

import com.datawings.app.common.BeanUtil;

public class ManifestBean {
	private String manifestNo;
	private String tacn;
	private String orig;
	private String dest;
	private String flightNumber;
	private Date flightDate;
	private String immat;
	private Integer entryNumber;
	private Integer awbFlownCount;

	public ManifestBean() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getManifestNo() {
		return manifestNo;
	}

	public void setManifestNo(String manifestNo) {
		this.manifestNo = manifestNo;
	}

	public String getTacn() {
		return tacn;
	}

	public void setTacn(String tacn) {
		this.tacn = tacn;
	}

	public String getOrig() {
		return orig;
	}

	public void setOrig(String orig) {
		this.orig = orig;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public String getImmat() {
		return immat;
	}

	public void setImmat(String immat) {
		this.immat = immat;
	}

	public Integer getEntryNumber() {
		return entryNumber;
	}

	public void setEntryNumber(Integer entryNumber) {
		this.entryNumber = entryNumber;
	}

	public Integer getAwbFlownCount() {
		return awbFlownCount;
	}

	public void setAwbFlownCount(Integer awbFlownCount) {
		this.awbFlownCount = awbFlownCount;
	}

}
