package com.datawings.app.filter;

import com.datawings.app.common.BeanUtil;

public class Rmk {
	private String month;
	private String orig;
	private String dest;
	private Double ht;
	private Double myc;
	private Double weight;
	private Double discount;

	private Double htYear;
	private Double mycYear;
	private Double weightYear;
	private Double discountYear;

	public Rmk() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getOrig() {
		return orig;
	}

	public String getDest() {
		return dest;
	}

	public Double getHt() {
		return ht;
	}

	public Double getMyc() {
		return myc;
	}

	public Double getWeight() {
		return weight;
	}

	public Double getDiscount() {
		return discount;
	}

	public Double getHtYear() {
		return htYear;
	}

	public Double getMycYear() {
		return mycYear;
	}

	public Double getWeightYear() {
		return weightYear;
	}

	public Double getDiscountYear() {
		return discountYear;
	}

	public void setOrig(String orig) {
		this.orig = orig;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public void setHt(Double ht) {
		this.ht = ht;
	}

	public void setMyc(Double myc) {
		this.myc = myc;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public void setHtYear(Double htYear) {
		this.htYear = htYear;
	}

	public void setMycYear(Double mycYear) {
		this.mycYear = mycYear;
	}

	public void setWeightYear(Double weightYear) {
		this.weightYear = weightYear;
	}

	public void setDiscountYear(Double discountYear) {
		this.discountYear = discountYear;
	}

}
