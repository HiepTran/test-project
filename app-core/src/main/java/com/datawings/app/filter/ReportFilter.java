package com.datawings.app.filter;

import com.datawings.app.common.BeanUtil;

public class ReportFilter extends BaseFilter {
	private String isoc;
	private String agtn;
	private String channel;
	private String dateFrom;
	private String dateTo;
	private String type;
	private String orig;
	private String dest;
	private String immat;
	private String flightNumber;

	public ReportFilter() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}
	
	public String getIsoc() {
		return isoc;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getOrig() {
		return orig;
	}

	public String getDest() {
		return dest;
	}

	public String getImmat() {
		return immat;
	}

	public void setOrig(String orig) {
		this.orig = orig;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public void setImmat(String immat) {
		this.immat = immat;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAgtn() {
		return agtn;
	}

	public String getChannel() {
		return channel;
	}

	public void setIsoc(String isoc) {
		this.isoc = isoc;
	}

	public void setAgtn(String agtn) {
		this.agtn = agtn;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

}
