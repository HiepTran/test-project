package com.datawings.app.filter;

public class RoleFilter extends BaseFilter {
	private String roleName;
	private String description;
	
	/* filter add role */
	private String roleNameAdd;
	private String descriptionAdd;
	
	/*filter edit role*/
	private String roleNameEdit;
	private String descriptionEdit;
	
	public void init() {
		super.init();
	}

	public RoleFilter() {
		init();
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRoleNameAdd() {
		return roleNameAdd;
	}

	public void setRoleNameAdd(String roleNameAdd) {
		this.roleNameAdd = roleNameAdd;
	}

	public String getDescriptionAdd() {
		return descriptionAdd;
	}

	public void setDescriptionAdd(String descriptionAdd) {
		this.descriptionAdd = descriptionAdd;
	}

	public String getRoleNameEdit() {
		return roleNameEdit;
	}

	public void setRoleNameEdit(String roleNameEdit) {
		this.roleNameEdit = roleNameEdit;
	}

	public String getDescriptionEdit() {
		return descriptionEdit;
	}

	public void setDescriptionEdit(String descriptionEdit) {
		this.descriptionEdit = descriptionEdit;
	}

}
