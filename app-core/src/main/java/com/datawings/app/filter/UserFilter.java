package com.datawings.app.filter;
import com.datawings.app.common.BeanUtil;

public class UserFilter {
	private String username;
	private String role;
	private String roleCreate;
	private String roleDelete;
	private String roleUpdate;
	
	/* filter add User*/
	private String usernameAdd;
	private String oldPasswordAdd;
	private String passwordAdd;
	private String retypePasswordAdd;
	private String nameAdd;
	private String addressAdd;
	private String telephoneAdd;
	private String oldEmailAdd;
	private String emailAdd;
	private String retypeEmailAdd;
	private String languageAdd;
	private String airlineCodeAdd;
	private String countryCodeAdd;
	
	/*filter edit user*/
	private String usernameEdit;
	private String oldPasswordEdit;
	private String passwordEdit;
	private String retypePasswordEdit;
	private String nameEdit;
	private String addressEdit;
	private String telephoneEdit;
	private String oldEmailEdit;
	private String emailEdit;
	private String retypeEmailEdit;
	private String languageEdit;
	private String airlineCodeEdit;
	private String countryCodeEdit;
	
	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}
	
	public UserFilter() {
		init();
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRoleCreate() {
		return roleCreate;
	}

	public void setRoleCreate(String roleCreate) {
		this.roleCreate = roleCreate;
	}

	public String getRoleDelete() {
		return roleDelete;
	}

	public void setRoleDelete(String roleDelete) {
		this.roleDelete = roleDelete;
	}

	public String getRoleUpdate() {
		return roleUpdate;
	}

	public void setRoleUpdate(String roleUpdate) {
		this.roleUpdate = roleUpdate;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUsernameAdd() {
		return usernameAdd;
	}

	public void setUsernameAdd(String usernameAdd) {
		this.usernameAdd = usernameAdd;
	}

	public String getOldPasswordAdd() {
		return oldPasswordAdd;
	}

	public void setOldPasswordAdd(String oldPasswordAdd) {
		this.oldPasswordAdd = oldPasswordAdd;
	}

	public String getPasswordAdd() {
		return passwordAdd;
	}

	public void setPasswordAdd(String passwordAdd) {
		this.passwordAdd = passwordAdd;
	}

	public String getRetypePasswordAdd() {
		return retypePasswordAdd;
	}

	public void setRetypePasswordAdd(String retypePasswordAdd) {
		this.retypePasswordAdd = retypePasswordAdd;
	}

	public String getNameAdd() {
		return nameAdd;
	}

	public void setNameAdd(String nameAdd) {
		this.nameAdd = nameAdd;
	}

	public String getAddressAdd() {
		return addressAdd;
	}

	public void setAddressAdd(String addressAdd) {
		this.addressAdd = addressAdd;
	}

	public String getTelephoneAdd() {
		return telephoneAdd;
	}

	public void setTelephoneAdd(String telephoneAdd) {
		this.telephoneAdd = telephoneAdd;
	}

	public String getOldEmailAdd() {
		return oldEmailAdd;
	}

	public void setOldEmailAdd(String oldEmailAdd) {
		this.oldEmailAdd = oldEmailAdd;
	}

	public String getEmailAdd() {
		return emailAdd;
	}

	public void setEmailAdd(String emailAdd) {
		this.emailAdd = emailAdd;
	}

	public String getRetypeEmailAdd() {
		return retypeEmailAdd;
	}

	public void setRetypeEmailAdd(String retypeEmailAdd) {
		this.retypeEmailAdd = retypeEmailAdd;
	}

	public String getLanguageAdd() {
		return languageAdd;
	}

	public void setLanguageAdd(String languageAdd) {
		this.languageAdd = languageAdd;
	}

	public String getAirlineCodeAdd() {
		return airlineCodeAdd;
	}

	public void setAirlineCodeAdd(String airlineCodeAdd) {
		this.airlineCodeAdd = airlineCodeAdd;
	}

	public String getCountryCodeAdd() {
		return countryCodeAdd;
	}

	public void setCountryCodeAdd(String countryCodeAdd) {
		this.countryCodeAdd = countryCodeAdd;
	}



	public String getUsernameEdit() {
		return usernameEdit;
	}

	public void setUsernameEdit(String usernameEdit) {
		this.usernameEdit = usernameEdit;
	}

	public String getOldPasswordEdit() {
		return oldPasswordEdit;
	}

	public void setOldPasswordEdit(String oldPasswordEdit) {
		this.oldPasswordEdit = oldPasswordEdit;
	}

	public String getPasswordEdit() {
		return passwordEdit;
	}

	public void setPasswordEdit(String passwordEdit) {
		this.passwordEdit = passwordEdit;
	}

	public String getRetypePasswordEdit() {
		return retypePasswordEdit;
	}

	public void setRetypePasswordEdit(String retypePasswordEdit) {
		this.retypePasswordEdit = retypePasswordEdit;
	}

	public String getNameEdit() {
		return nameEdit;
	}

	public void setNameEdit(String nameEdit) {
		this.nameEdit = nameEdit;
	}

	public String getAddressEdit() {
		return addressEdit;
	}

	public void setAddressEdit(String addressEdit) {
		this.addressEdit = addressEdit;
	}

	public String getTelephoneEdit() {
		return telephoneEdit;
	}

	public void setTelephoneEdit(String telephoneEdit) {
		this.telephoneEdit = telephoneEdit;
	}

	public String getOldEmailEdit() {
		return oldEmailEdit;
	}

	public void setOldEmailEdit(String oldEmailEdit) {
		this.oldEmailEdit = oldEmailEdit;
	}

	public String getEmailEdit() {
		return emailEdit;
	}

	public void setEmailEdit(String emailEdit) {
		this.emailEdit = emailEdit;
	}

	public String getRetypeEmailEdit() {
		return retypeEmailEdit;
	}

	public void setRetypeEmailEdit(String retypeEmailEdit) {
		this.retypeEmailEdit = retypeEmailEdit;
	}

	public String getLanguageEdit() {
		return languageEdit;
	}

	public void setLanguageEdit(String languageEdit) {
		this.languageEdit = languageEdit;
	}

	public String getAirlineCodeEdit() {
		return airlineCodeEdit;
	}

	public void setAirlineCodeEdit(String airlineCodeEdit) {
		this.airlineCodeEdit = airlineCodeEdit;
	}

	public String getCountryCodeEdit() {
		return countryCodeEdit;
	}

	public void setCountryCodeEdit(String countryCodeEdit) {
		this.countryCodeEdit = countryCodeEdit;
	}
	
}
