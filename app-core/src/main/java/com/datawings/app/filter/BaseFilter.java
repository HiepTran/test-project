package com.datawings.app.filter;

import com.datawings.app.common.BeanUtil;

public class BaseFilter {

	protected static int PAGE_SIZE_STANDARD = 40;

	protected Integer page;

	protected Integer rowCount;

	protected Integer pageSize;

	protected Integer pos;

	protected String col;

	protected Boolean sorting;

	public BaseFilter() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
		this.sorting = true;
		this.pos = -1;
		this.page = 0;
		this.pageSize = PAGE_SIZE_STANDARD;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getRowCount() {
		return rowCount;
	}

	public void setRowCount(Integer rowCount) {
		this.rowCount = rowCount;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPos() {
		return pos;
	}

	public void setPos(Integer pos) {
		this.pos = pos;
	}

	public String getCol() {
		return col;
	}

	public void setCol(String col) {
		this.col = col;
	}

	public Boolean getSorting() {
		return sorting;
	}

	public void setSorting(Boolean sorting) {
		this.sorting = sorting;
	}

	public Integer getOffset() {
		Integer offset = page * pageSize;
		return offset;
	}

	public Integer getTotalPage() {
		Integer tot = 0;
		Integer rs = 0;
		if (rowCount != 0 && pageSize != 0) {
			rs = (rowCount % pageSize);
			tot = rowCount / pageSize;
			tot = (rs > 0) ? (tot + 1) : tot;
		}
		return tot;
	}

}
