package com.datawings.app.filter;

import java.util.Date;

import com.datawings.app.common.BeanUtil;

public class AwbGSAMapping {
	private String tacn;
	private String lta;
	private String agtn;
	private String agtnName;
	private Date dateExecute;
	private Date flightDate;
	private String flightNumber;
	private String orig;
	private String flightDep;
	private String dest;
	private String chargeIndicator;
	private Integer quantity;
	private Double weightGross;
	private Double weightCharge;
	private Double unitCass;
	private String cutp;
	private Double addOn;
	private Double weightPp;
	private Double weightCc;

	private Double myc;
	private Double scc;
	private Double awc;
	private Double mcc;
	private Double rac;

	private Double carrier;
	private Double totalCarrier;
	private Double agent;
	private Double totalAgent;

	private Double otherGsa;
	private Double totalOther;

	private Double commGsa;
	private Double net;

	private String department;
	private String description;
	private String handlingInfor;
	private String descriptUser;

	public AwbGSAMapping() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getTacn() {
		return tacn;
	}

	public void setTacn(String tacn) {
		this.tacn = tacn;
	}

	public String getLta() {
		return lta;
	}

	public void setLta(String lta) {
		this.lta = lta;
	}

	public String getAgtn() {
		return agtn;
	}

	public void setAgtn(String agtn) {
		this.agtn = agtn;
	}

	public String getAgtnName() {
		return agtnName;
	}

	public void setAgtnName(String agtnName) {
		this.agtnName = agtnName;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getOrig() {
		return orig;
	}

	public void setOrig(String orig) {
		this.orig = orig;
	}

	public String getFlightDep() {
		return flightDep;
	}

	public void setFlightDep(String flightDep) {
		this.flightDep = flightDep;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getChargeIndicator() {
		return chargeIndicator;
	}

	public void setChargeIndicator(String chargeIndicator) {
		this.chargeIndicator = chargeIndicator;
	}

	public Date getDateExecute() {
		return dateExecute;
	}

	public void setDateExecute(Date dateExecute) {
		this.dateExecute = dateExecute;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getWeightGross() {
		return weightGross;
	}

	public void setWeightGross(Double weightGross) {
		this.weightGross = weightGross;
	}

	public Double getWeightCharge() {
		return weightCharge;
	}

	public void setWeightCharge(Double weightCharge) {
		this.weightCharge = weightCharge;
	}

	public Double getUnitCass() {
		return unitCass;
	}

	public void setUnitCass(Double unitCass) {
		this.unitCass = unitCass;
	}

	public String getCutp() {
		return cutp;
	}

	public void setCutp(String cutp) {
		this.cutp = cutp;
	}

	public Double getAddOn() {
		return addOn;
	}

	public void setAddOn(Double addOn) {
		this.addOn = addOn;
	}

	public Double getWeightPp() {
		return weightPp;
	}

	public void setWeightPp(Double weightPp) {
		this.weightPp = weightPp;
	}

	public Double getWeightCc() {
		return weightCc;
	}

	public void setWeightCc(Double weightCc) {
		this.weightCc = weightCc;
	}

	public Double getMyc() {
		return myc;
	}

	public void setMyc(Double myc) {
		this.myc = myc;
	}

	public Double getScc() {
		return scc;
	}

	public void setScc(Double scc) {
		this.scc = scc;
	}

	public Double getAwc() {
		return awc;
	}

	public void setAwc(Double awc) {
		this.awc = awc;
	}

	public Double getMcc() {
		return mcc;
	}

	public void setMcc(Double mcc) {
		this.mcc = mcc;
	}

	public Double getRac() {
		return rac;
	}

	public void setRac(Double rac) {
		this.rac = rac;
	}



	public Double getCarrier() {
		return carrier;
	}

	public void setCarrier(Double carrier) {
		this.carrier = carrier;
	}

	public Double getTotalCarrier() {
		return totalCarrier;
	}

	public void setTotalCarrier(Double totalCarrier) {
		this.totalCarrier = totalCarrier;
	}

	public Double getAgent() {
		return agent;
	}

	public void setAgent(Double agent) {
		this.agent = agent;
	}

	public Double getTotalAgent() {
		return totalAgent;
	}

	public void setTotalAgent(Double totalAgent) {
		this.totalAgent = totalAgent;
	}

	public Double getOtherGsa() {
		return otherGsa;
	}

	public void setOtherGsa(Double otherGsa) {
		this.otherGsa = otherGsa;
	}

	public Double getTotalOther() {
		return totalOther;
	}

	public void setTotalOther(Double totalOther) {
		this.totalOther = totalOther;
	}

	public Double getCommGsa() {
		return commGsa;
	}

	public void setCommGsa(Double commGsa) {
		this.commGsa = commGsa;
	}

	public Double getNet() {
		return net;
	}

	public void setNet(Double net) {
		this.net = net;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHandlingInfor() {
		return handlingInfor;
	}

	public void setHandlingInfor(String handlingInfor) {
		this.handlingInfor = handlingInfor;
	}

	public String getDescriptUser() {
		return descriptUser;
	}

	public void setDescriptUser(String descriptUser) {
		this.descriptUser = descriptUser;
	}

}
