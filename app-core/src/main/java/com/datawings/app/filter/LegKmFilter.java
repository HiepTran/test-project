package com.datawings.app.filter;

import com.datawings.app.model.LegKm;

public class LegKmFilter extends BaseFilter {
	private String isocFrom, airportFrom, isocTo, airportTo;
	private String km, mile;
	
	private LegKm legKmAdd;
	private LegKm legKmEdit;
	
	public LegKmFilter(){
		init();
	}

	public void init() {
		super.init();
		initLegKmAdd();
		initLegKmEdit();
	}
	
	public void initLegKmAdd(){
		legKmAdd = new LegKm();
	}
	
	public void initLegKmEdit(){
		legKmEdit = new LegKm();
	}
	
	public LegKm getLegKmAdd() {
		return legKmAdd;
	}

	public void setLegKmAdd(LegKm legKmAdd) {
		this.legKmAdd = legKmAdd;
	}

	public LegKm getLegKmEdit() {
		return legKmEdit;
	}

	public void setLegKmEdit(LegKm legKmEdit) {
		this.legKmEdit = legKmEdit;
	}

	public String getIsocFrom() {
		return isocFrom;
	}

	public void setIsocFrom(String isocFrom) {
		this.isocFrom = isocFrom;
	}

	public String getAirportFrom() {
		return airportFrom;
	}

	public void setAirportFrom(String airportFrom) {
		this.airportFrom = airportFrom;
	}

	public String getIsocTo() {
		return isocTo;
	}

	public void setIsocTo(String isocTo) {
		this.isocTo = isocTo;
	}

	public String getAirportTo() {
		return airportTo;
	}

	public void setAirportTo(String airportTo) {
		this.airportTo = airportTo;
	}

	public String getKm() {
		return km;
	}

	public void setKm(String km) {
		this.km = km;
	}

	public String getMile() {
		return mile;
	}

	public void setMile(String mile) {
		this.mile = mile;
	}

	
}
