package com.datawings.app.filter;

import com.datawings.app.common.BeanUtil;

public class AgentFilter extends BaseFilter {
	private String agtnCode;
	private String code;
	private String checkDigit;
	private String shortName;
	private String name;
	private String iataInd;
	private String kind;
	private String address1;
	private String address2;
	private String address3;
	private String zip;
	private String city;
	private String isoc;
	private String region;
	private String phone;
	private String fax;
	private String website;
	private String email;
	private String comm;
	
	private String codeExt;
	private String kindExt;
	private String isocExt;
	private String iataIndExt;
	
	public AgentFilter() {
		super.init();
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getAgtnCode() {
		return agtnCode;
	}

	public String getCodeExt() {
		return codeExt;
	}

	public String getKindExt() {
		return kindExt;
	}

	public String getIsocExt() {
		return isocExt;
	}

	public String getIataIndExt() {
		return iataIndExt;
	}

	public void setCodeExt(String codeExt) {
		this.codeExt = codeExt;
	}

	public void setKindExt(String kindExt) {
		this.kindExt = kindExt;
	}

	public void setIsocExt(String isocExt) {
		this.isocExt = isocExt;
	}

	public void setIataIndExt(String iataIndExt) {
		this.iataIndExt = iataIndExt;
	}

	public String getCode() {
		return code;
	}

	public String getCheckDigit() {
		return checkDigit;
	}

	public String getShortName() {
		return shortName;
	}

	public String getName() {
		return name;
	}

	public String getIataInd() {
		return iataInd;
	}

	public String getKind() {
		return kind;
	}

	public String getAddress1() {
		return address1;
	}

	public String getAddress2() {
		return address2;
	}

	public String getAddress3() {
		return address3;
	}

	public String getZip() {
		return zip;
	}

	public String getCity() {
		return city;
	}

	public String getIsoc() {
		return isoc;
	}

	public String getRegion() {
		return region;
	}

	public String getPhone() {
		return phone;
	}

	public String getFax() {
		return fax;
	}

	public String getWebsite() {
		return website;
	}

	public String getEmail() {
		return email;
	}

	public void setAgtnCode(String agtnCode) {
		this.agtnCode = agtnCode;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCheckDigit(String checkDigit) {
		this.checkDigit = checkDigit;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setIataInd(String iataInd) {
		this.iataInd = iataInd;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setIsoc(String isoc) {
		this.isoc = isoc;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getComm() {
		return comm;
	}

	public void setComm(String comm) {
		this.comm = comm;
	}

}
