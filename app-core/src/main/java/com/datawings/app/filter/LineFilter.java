package com.datawings.app.filter;

import com.datawings.app.common.BeanUtil;

public class LineFilter {
	private String orig;
	private String dest;
	private String line;
	
	private String origExt;
	private String destExt;
	private String lineExt;
	
	private String origEdit;
	private String destEdit;
	private String lineEdit;

	public LineFilter() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getOrigExt() {
		return origExt;
	}

	public String getDestExt() {
		return destExt;
	}

	public String getOrigEdit() {
		return origEdit;
	}

	public String getDestEdit() {
		return destEdit;
	}

	public String getLineEdit() {
		return lineEdit;
	}

	public void setOrigEdit(String origEdit) {
		this.origEdit = origEdit;
	}

	public void setDestEdit(String destEdit) {
		this.destEdit = destEdit;
	}

	public void setLineEdit(String lineEdit) {
		this.lineEdit = lineEdit;
	}

	public String getLineExt() {
		return lineExt;
	}

	public void setOrigExt(String origExt) {
		this.origExt = origExt;
	}

	public void setDestExt(String destExt) {
		this.destExt = destExt;
	}

	public void setLineExt(String lineExt) {
		this.lineExt = lineExt;
	}

	public String getOrig() {
		return orig;
	}

	public String getDest() {
		return dest;
	}

	public String getLine() {
		return line;
	}

	public void setOrig(String orig) {
		this.orig = orig;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public void setLine(String line) {
		this.line = line;
	}

}
