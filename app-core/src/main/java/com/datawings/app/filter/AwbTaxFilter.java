package com.datawings.app.filter;

import com.datawings.app.common.BeanUtil;

public class AwbTaxFilter {
	private Integer awbTaxId;
	private String code;
	private String amount;

	public AwbTaxFilter() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public Integer getAwbTaxId() {
		return awbTaxId;
	}

	public void setAwbTaxId(Integer awbTaxId) {
		this.awbTaxId = awbTaxId;
	}

	public String getCode() {
		return code;
	}

	public String getAmount() {
		return amount;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

}
