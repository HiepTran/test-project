package com.datawings.app.filter;

import com.datawings.app.common.BeanUtil;

public class TaxFilter {
	private String type;
	private String isoc;
	private String code;
	private String subCode;
	private String item;
	private String dateFrom;
	private String dateTo;

	private String dateFromAdd;
	private String amountAdd;
	private String descriptionAdd;

	private String cutpEdit;
	private String unitEdit;
	private String itemEdit;
	private String amountEdit;
	private String descriptionEdit;

	private String typeExt;
	private String isocExt;
	private String codeExt;
	private String subCodeExt;
	private String itemExt;
	private String dateFromExt;
	private String unitExt;
	private String amountExt;
	private String descriptionExt;
	private String cutpExt;

	public TaxFilter() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getDescriptionEdit() {
		return descriptionEdit;
	}

	public String getCutpEdit() {
		return cutpEdit;
	}

	public String getUnitEdit() {
		return unitEdit;
	}

	public String getItemEdit() {
		return itemEdit;
	}

	public void setCutpEdit(String cutpEdit) {
		this.cutpEdit = cutpEdit;
	}

	public void setUnitEdit(String unitEdit) {
		this.unitEdit = unitEdit;
	}

	public void setItemEdit(String itemEdit) {
		this.itemEdit = itemEdit;
	}

	public String getIsoc() {
		return isoc;
	}

	public String getDateFromAdd() {
		return dateFromAdd;
	}

	public String getAmountAdd() {
		return amountAdd;
	}

	public String getDescriptionAdd() {
		return descriptionAdd;
	}

	public String getTypeExt() {
		return typeExt;
	}

	public String getIsocExt() {
		return isocExt;
	}

	public String getCodeExt() {
		return codeExt;
	}

	public String getSubCodeExt() {
		return subCodeExt;
	}

	public String getItemExt() {
		return itemExt;
	}

	public String getDateFromExt() {
		return dateFromExt;
	}

	public String getUnitExt() {
		return unitExt;
	}

	public String getDescriptionExt() {
		return descriptionExt;
	}

	public String getCutpExt() {
		return cutpExt;
	}

	public void setTypeExt(String typeExt) {
		this.typeExt = typeExt;
	}

	public void setIsocExt(String isocExt) {
		this.isocExt = isocExt;
	}

	public void setCodeExt(String codeExt) {
		this.codeExt = codeExt;
	}

	public void setSubCodeExt(String subCodeExt) {
		this.subCodeExt = subCodeExt;
	}

	public void setItemExt(String itemExt) {
		this.itemExt = itemExt;
	}

	public void setDateFromExt(String dateFromExt) {
		this.dateFromExt = dateFromExt;
	}

	public void setUnitExt(String unitExt) {
		this.unitExt = unitExt;
	}

	public String getAmountExt() {
		return amountExt;
	}

	public void setAmountExt(String amountExt) {
		this.amountExt = amountExt;
	}

	public void setDescriptionExt(String descriptionExt) {
		this.descriptionExt = descriptionExt;
	}

	public void setCutpExt(String cutpExt) {
		this.cutpExt = cutpExt;
	}

	public String getAmountEdit() {
		return amountEdit;
	}

	public void setDateFromAdd(String dateFromAdd) {
		this.dateFromAdd = dateFromAdd;
	}

	public void setAmountAdd(String amountAdd) {
		this.amountAdd = amountAdd;
	}

	public void setDescriptionAdd(String descriptionAdd) {
		this.descriptionAdd = descriptionAdd;
	}

	public void setAmountEdit(String amountEdit) {
		this.amountEdit = amountEdit;
	}

	public void setDescriptionEdit(String descriptionEdit) {
		this.descriptionEdit = descriptionEdit;
	}

	public String getCode() {
		return code;
	}

	public String getSubCode() {
		return subCode;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getItem() {
		return item;
	}

	public void setIsoc(String isoc) {
		this.isoc = isoc;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setSubCode(String subCode) {
		this.subCode = subCode;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
