package com.datawings.app.filter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.datawings.app.common.DateUtil;

public class AwbFilter extends BaseFilter {
	private Integer id;
	private String isoc;
	private String tacn;
	private String lta;
	private String ltaAdd;
	private String agtn;
	private String agtnGsa;
	private String dateExecute;
	private String shipName;
	private String shipAddress1;
	private String shipAddress2;
	private String shipAddress3;
	private String shipAddress4;
	private String consigneeName;
	private String consigneeAddress1;
	private String consigneeAddress2;
	private String consigneeAddress3;
	private String consigneeAddress4;
	private String orig;
	private String dest;
	private String carrier;
	private String flightNumber;
	private String flightDate;
	private String cutp;
	private String declaredCarrige;
	private String declaredCustom;
	private String handlingInfor;
	private String rdv;
	private String dateStr;
	private String chargeIndicator;
	private String otherIndicator;
	private String descriptUser;
	private String km;
	private String manifestNo;

	// Detail
	/*--------------*/
	private String quantity1;
	private String weightGross1;
	private String weightIndicator1;
	private String rateClass1;
	private String weightCharge1;
	private String unit1;
	private String amount1;
	private String description1;
	/*--------------*/
	private String quantity2;
	private String weightGross2;
	private String weightIndicator2;
	private String rateClass2;
	private String weightCharge2;
	private String unit2;
	private String amount2;
	private String description2;
	/*--------------*/
	private String quantity3;
	private String weightGross3;
	private String weightIndicator3;
	private String rateClass3;
	private String weightCharge3;
	private String unit3;
	private String amount3;
	private String description3;
	/*--------------*/
	private String quantity4;
	private String weightGross4;
	private String weightIndicator4;
	private String rateClass4;
	private String weightCharge4;
	private String unit4;
	private String amount4;
	private String description4;
	/*--------------*/
	private String quantity5;
	private String weightGross5;
	private String weightIndicator5;
	private String rateClass5;
	private String weightCharge5;
	private String unit5;
	private String amount5;
	private String description5;

	// Total
	private String weightPp;
	private String carrierPp;
	private String agentPp;
	private String totalPp;
	private String weightCc;
	private String carrierCc;
	private String agentCc;
	private String totalCc;
	private String net;
	private String vatPercent;
	private String vat;

	// Tax
	private String taxCode1;
	private String taxValue1;
	private String taxCode2;
	private String taxValue2;
	private String taxCode3;
	private String taxValue3;
	private String taxCode4;
	private String taxValue4;
	private String taxCode5;
	private String taxValue5;
	private String taxCode6;
	private String taxValue6;
	private String taxCode7;
	private String taxValue7;
	private String taxCode8;
	private String taxValue8;

	private String commGsaPercent;
	private String commPercent;
	private String commGsa;
	private String comm;

	private String message;
	private String channel;
	private String dateFrom;
	private String dateTo;

	// credit cart
	private String modePayment;
	private String nbCart;
	private String dateExp;
	private String nameCart;

	private String errorTax;
	private String errorDetail;

	private List<AwbTaxFilter> awbTax;
	private List<AwbDetailFilter> awbDetail;

	public AwbFilter() {
		init();
	}

	public void init() {
		super.init();
		this.dateStr = DateUtil.date2String(new Date(), "dd/MM/yyyy");
		this.awbTax = new ArrayList<AwbTaxFilter>();
		this.awbDetail = new ArrayList<AwbDetailFilter>();
	}

	public String getVatPercent() {
		return vatPercent;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getAgtnGsa() {
		return agtnGsa;
	}

	public void setAgtnGsa(String agtnGsa) {
		this.agtnGsa = agtnGsa;
	}

	public String getManifestNo() {
		return manifestNo;
	}

	public void setManifestNo(String manifestNo) {
		this.manifestNo = manifestNo;
	}

	public String getErrorDetail() {
		return errorDetail;
	}

	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}

	public void setVatPercent(String vatPercent) {
		this.vatPercent = vatPercent;
	}

	public String getNet() {
		return net;
	}

	public String getKm() {
		return km;
	}

	public void setKm(String km) {
		this.km = km;
	}

	public String getModePayment() {
		return modePayment;
	}

	public String getNbCart() {
		return nbCart;
	}

	public String getDateExp() {
		return dateExp;
	}

	public List<AwbDetailFilter> getAwbDetail() {
		return awbDetail;
	}

	public void setAwbDetail(List<AwbDetailFilter> awbDetail) {
		this.awbDetail = awbDetail;
	}

	public String getNameCart() {
		return nameCart;
	}

	public void setNbCart(String nbCart) {
		this.nbCart = nbCart;
	}

	public void setDateExp(String dateExp) {
		this.dateExp = dateExp;
	}

	public void setNameCart(String nameCart) {
		this.nameCart = nameCart;
	}

	public void setModePayment(String modePayment) {
		this.modePayment = modePayment;
	}

	public void setNet(String net) {
		this.net = net;
	}

	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public String getChannel() {
		return channel;
	}

	public String getErrorTax() {
		return errorTax;
	}

	public void setErrorTax(String errorTax) {
		this.errorTax = errorTax;
	}

	public String getCommGsaPercent() {
		return commGsaPercent;
	}

	public String getCommGsa() {
		return commGsa;
	}

	public String getComm() {
		return comm;
	}

	public void setCommGsaPercent(String commGsaPercent) {
		this.commGsaPercent = commGsaPercent;
	}

	public void setCommGsa(String commGsa) {
		this.commGsa = commGsa;
	}

	public void setComm(String comm) {
		this.comm = comm;
	}

	public List<AwbTaxFilter> getAwbTax() {
		return awbTax;
	}

	public void setAwbTax(List<AwbTaxFilter> awbTax) {
		this.awbTax = awbTax;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getIsoc() {
		return isoc;
	}

	public void setIsoc(String isoc) {
		this.isoc = isoc;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLtaAdd() {
		return ltaAdd;
	}

	public void setLtaAdd(String ltaAdd) {
		this.ltaAdd = ltaAdd;
	}

	public String getDescriptUser() {
		return descriptUser;
	}

	public void setDescriptUser(String descriptUser) {
		this.descriptUser = descriptUser;
	}

	public String getWeightIndicator1() {
		return weightIndicator1;
	}

	public String getRateClass1() {
		return rateClass1;
	}

	public String getDateStr() {
		return dateStr;
	}

	public String getChargeIndicator() {
		return chargeIndicator;
	}

	public String getOtherIndicator() {
		return otherIndicator;
	}

	public String getQuantity1() {
		return quantity1;
	}

	public String getWeightGross1() {
		return weightGross1;
	}

	public String getWeightCharge1() {
		return weightCharge1;
	}

	public String getUnit1() {
		return unit1;
	}

	public String getAmount1() {
		return amount1;
	}

	public String getDescription1() {
		return description1;
	}

	public String getQuantity2() {
		return quantity2;
	}

	public String getWeightGross2() {
		return weightGross2;
	}

	public String getWeightIndicator2() {
		return weightIndicator2;
	}

	public String getRateClass2() {
		return rateClass2;
	}

	public String getWeightCharge2() {
		return weightCharge2;
	}

	public String getUnit2() {
		return unit2;
	}

	public String getAmount2() {
		return amount2;
	}

	public String getDescription2() {
		return description2;
	}

	public String getQuantity3() {
		return quantity3;
	}

	public String getWeightGross3() {
		return weightGross3;
	}

	public String getWeightIndicator3() {
		return weightIndicator3;
	}

	public String getRateClass3() {
		return rateClass3;
	}

	public String getWeightCharge3() {
		return weightCharge3;
	}

	public String getUnit3() {
		return unit3;
	}

	public String getAmount3() {
		return amount3;
	}

	public String getDescription3() {
		return description3;
	}

	public String getQuantity4() {
		return quantity4;
	}

	public String getWeightGross4() {
		return weightGross4;
	}

	public String getWeightIndicator4() {
		return weightIndicator4;
	}

	public String getRateClass4() {
		return rateClass4;
	}

	public String getWeightCharge4() {
		return weightCharge4;
	}

	public String getUnit4() {
		return unit4;
	}

	public String getAmount4() {
		return amount4;
	}

	public String getDescription4() {
		return description4;
	}

	public String getQuantity5() {
		return quantity5;
	}

	public String getWeightGross5() {
		return weightGross5;
	}

	public String getWeightIndicator5() {
		return weightIndicator5;
	}

	public String getRateClass5() {
		return rateClass5;
	}

	public String getWeightCharge5() {
		return weightCharge5;
	}

	public String getUnit5() {
		return unit5;
	}

	public String getAmount5() {
		return amount5;
	}

	public String getDescription5() {
		return description5;
	}

	public String getWeightPp() {
		return weightPp;
	}

	public String getCarrierPp() {
		return carrierPp;
	}

	public String getAgentPp() {
		return agentPp;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}

	public void setChargeIndicator(String chargeIndicator) {
		this.chargeIndicator = chargeIndicator;
	}

	public void setOtherIndicator(String otherIndicator) {
		this.otherIndicator = otherIndicator;
	}

	public void setQuantity1(String quantity1) {
		this.quantity1 = quantity1;
	}

	public void setWeightGross1(String weightGross1) {
		this.weightGross1 = weightGross1;
	}

	public void setWeightIndicator1(String weightIndicator1) {
		this.weightIndicator1 = weightIndicator1;
	}

	public void setRateClass1(String rateClass1) {
		this.rateClass1 = rateClass1;
	}

	public void setWeightCharge1(String weightCharge1) {
		this.weightCharge1 = weightCharge1;
	}

	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}

	public void setAmount1(String amount1) {
		this.amount1 = amount1;
	}

	public void setDescription1(String description1) {
		this.description1 = description1;
	}

	public void setQuantity2(String quantity2) {
		this.quantity2 = quantity2;
	}

	public void setWeightGross2(String weightGross2) {
		this.weightGross2 = weightGross2;
	}

	public void setWeightIndicator2(String weightIndicator2) {
		this.weightIndicator2 = weightIndicator2;
	}

	public void setRateClass2(String rateClass2) {
		this.rateClass2 = rateClass2;
	}

	public void setWeightCharge2(String weightCharge2) {
		this.weightCharge2 = weightCharge2;
	}

	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}

	public void setAmount2(String amount2) {
		this.amount2 = amount2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	public void setQuantity3(String quantity3) {
		this.quantity3 = quantity3;
	}

	public void setWeightGross3(String weightGross3) {
		this.weightGross3 = weightGross3;
	}

	public void setWeightIndicator3(String weightIndicator3) {
		this.weightIndicator3 = weightIndicator3;
	}

	public void setRateClass3(String rateClass3) {
		this.rateClass3 = rateClass3;
	}

	public void setWeightCharge3(String weightCharge3) {
		this.weightCharge3 = weightCharge3;
	}

	public void setUnit3(String unit3) {
		this.unit3 = unit3;
	}

	public void setAmount3(String amount3) {
		this.amount3 = amount3;
	}

	public void setDescription3(String description3) {
		this.description3 = description3;
	}

	public void setQuantity4(String quantity4) {
		this.quantity4 = quantity4;
	}

	public void setWeightGross4(String weightGross4) {
		this.weightGross4 = weightGross4;
	}

	public void setWeightIndicator4(String weightIndicator4) {
		this.weightIndicator4 = weightIndicator4;
	}

	public void setRateClass4(String rateClass4) {
		this.rateClass4 = rateClass4;
	}

	public void setWeightCharge4(String weightCharge4) {
		this.weightCharge4 = weightCharge4;
	}

	public void setUnit4(String unit4) {
		this.unit4 = unit4;
	}

	public void setAmount4(String amount4) {
		this.amount4 = amount4;
	}

	public void setDescription4(String description4) {
		this.description4 = description4;
	}

	public void setQuantity5(String quantity5) {
		this.quantity5 = quantity5;
	}

	public void setWeightGross5(String weightGross5) {
		this.weightGross5 = weightGross5;
	}

	public void setWeightIndicator5(String weightIndicator5) {
		this.weightIndicator5 = weightIndicator5;
	}

	public void setRateClass5(String rateClass5) {
		this.rateClass5 = rateClass5;
	}

	public void setWeightCharge5(String weightCharge5) {
		this.weightCharge5 = weightCharge5;
	}

	public void setUnit5(String unit5) {
		this.unit5 = unit5;
	}

	public void setAmount5(String amount5) {
		this.amount5 = amount5;
	}

	public void setDescription5(String description5) {
		this.description5 = description5;
	}

	public String getTotalPp() {
		return totalPp;
	}

	public String getWeightCc() {
		return weightCc;
	}

	public String getCarrierCc() {
		return carrierCc;
	}

	public String getAgentCc() {
		return agentCc;
	}

	public String getTotalCc() {
		return totalCc;
	}

	public void setWeightPp(String weightPp) {
		this.weightPp = weightPp;
	}

	public void setCarrierPp(String carrierPp) {
		this.carrierPp = carrierPp;
	}

	public void setAgentPp(String agentPp) {
		this.agentPp = agentPp;
	}

	public void setTotalPp(String totalPp) {
		this.totalPp = totalPp;
	}

	public void setWeightCc(String weightCc) {
		this.weightCc = weightCc;
	}

	public void setCarrierCc(String carrierCc) {
		this.carrierCc = carrierCc;
	}

	public void setAgentCc(String agentCc) {
		this.agentCc = agentCc;
	}

	public void setTotalCc(String totalCc) {
		this.totalCc = totalCc;
	}

	public String getHandlingInfor() {
		return handlingInfor;
	}

	public void setHandlingInfor(String handlingInfor) {
		this.handlingInfor = handlingInfor;
	}

	public String getTacn() {
		return tacn;
	}

	public String getLta() {
		return lta;
	}

	public String getAgtn() {
		return agtn;
	}

	public void setTacn(String tacn) {
		this.tacn = tacn;
	}

	public void setLta(String lta) {
		this.lta = lta;
	}

	public void setAgtn(String agtn) {
		this.agtn = agtn;
	}

	public String getDateExecute() {
		return dateExecute;
	}

	public void setDateExecute(String dateExecute) {
		this.dateExecute = dateExecute;
	}

	public String getShipName() {
		return shipName;
	}

	public String getShipAddress1() {
		return shipAddress1;
	}

	public String getShipAddress2() {
		return shipAddress2;
	}

	public String getShipAddress3() {
		return shipAddress3;
	}

	public String getShipAddress4() {
		return shipAddress4;
	}

	public String getConsigneeName() {
		return consigneeName;
	}

	public String getConsigneeAddress1() {
		return consigneeAddress1;
	}

	public String getConsigneeAddress2() {
		return consigneeAddress2;
	}

	public String getConsigneeAddress3() {
		return consigneeAddress3;
	}

	public String getConsigneeAddress4() {
		return consigneeAddress4;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public void setShipAddress1(String shipAddress1) {
		this.shipAddress1 = shipAddress1;
	}

	public void setShipAddress2(String shipAddress2) {
		this.shipAddress2 = shipAddress2;
	}

	public void setShipAddress3(String shipAddress3) {
		this.shipAddress3 = shipAddress3;
	}

	public void setShipAddress4(String shipAddress4) {
		this.shipAddress4 = shipAddress4;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	public void setConsigneeAddress1(String consigneeAddress1) {
		this.consigneeAddress1 = consigneeAddress1;
	}

	public void setConsigneeAddress2(String consigneeAddress2) {
		this.consigneeAddress2 = consigneeAddress2;
	}

	public void setConsigneeAddress3(String consigneeAddress3) {
		this.consigneeAddress3 = consigneeAddress3;
	}

	public void setConsigneeAddress4(String consigneeAddress4) {
		this.consigneeAddress4 = consigneeAddress4;
	}

	public String getOrig() {
		return orig;
	}

	public String getDest() {
		return dest;
	}

	public void setOrig(String orig) {
		this.orig = orig;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public String getFlightDate() {
		return flightDate;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public void setFlightDate(String flightDate) {
		this.flightDate = flightDate;
	}

	public String getCutp() {
		return cutp;
	}

	public void setCutp(String cutp) {
		this.cutp = cutp;
	}

	public String getDeclaredCarrige() {
		return declaredCarrige;
	}

	public String getDeclaredCustom() {
		return declaredCustom;
	}

	public void setDeclaredCarrige(String declaredCarrige) {
		this.declaredCarrige = declaredCarrige;
	}

	public void setDeclaredCustom(String declaredCustom) {
		this.declaredCustom = declaredCustom;
	}

	public String getTaxCode1() {
		return taxCode1;
	}

	public String getTaxValue1() {
		return taxValue1;
	}

	public String getTaxCode2() {
		return taxCode2;
	}

	public String getTaxValue2() {
		return taxValue2;
	}

	public String getTaxCode3() {
		return taxCode3;
	}

	public String getTaxValue3() {
		return taxValue3;
	}

	public String getTaxCode4() {
		return taxCode4;
	}

	public String getTaxValue4() {
		return taxValue4;
	}

	public String getTaxCode5() {
		return taxCode5;
	}

	public String getTaxValue5() {
		return taxValue5;
	}

	public String getTaxCode6() {
		return taxCode6;
	}

	public String getTaxValue6() {
		return taxValue6;
	}

	public String getTaxCode7() {
		return taxCode7;
	}

	public String getTaxValue7() {
		return taxValue7;
	}

	public String getTaxCode8() {
		return taxCode8;
	}

	public String getTaxValue8() {
		return taxValue8;
	}

	public void setTaxCode1(String taxCode1) {
		this.taxCode1 = taxCode1;
	}

	public void setTaxValue1(String taxValue1) {
		this.taxValue1 = taxValue1;
	}

	public void setTaxCode2(String taxCode2) {
		this.taxCode2 = taxCode2;
	}

	public void setTaxValue2(String taxValue2) {
		this.taxValue2 = taxValue2;
	}

	public void setTaxCode3(String taxCode3) {
		this.taxCode3 = taxCode3;
	}

	public void setTaxValue3(String taxValue3) {
		this.taxValue3 = taxValue3;
	}

	public void setTaxCode4(String taxCode4) {
		this.taxCode4 = taxCode4;
	}

	public void setTaxValue4(String taxValue4) {
		this.taxValue4 = taxValue4;
	}

	public void setTaxCode5(String taxCode5) {
		this.taxCode5 = taxCode5;
	}

	public void setTaxValue5(String taxValue5) {
		this.taxValue5 = taxValue5;
	}

	public void setTaxCode6(String taxCode6) {
		this.taxCode6 = taxCode6;
	}

	public void setTaxValue6(String taxValue6) {
		this.taxValue6 = taxValue6;
	}

	public void setTaxCode7(String taxCode7) {
		this.taxCode7 = taxCode7;
	}

	public void setTaxValue7(String taxValue7) {
		this.taxValue7 = taxValue7;
	}

	public void setTaxCode8(String taxCode8) {
		this.taxCode8 = taxCode8;
	}

	public void setTaxValue8(String taxValue8) {
		this.taxValue8 = taxValue8;
	}

	public String getRdv() {
		return rdv;
	}

	public void setRdv(String rdv) {
		this.rdv = rdv;
	}

	public String getCommPercent() {
		return commPercent;
	}

	public void setCommPercent(String commPercent) {
		this.commPercent = commPercent;
	}

}
