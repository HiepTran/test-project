package com.datawings.app.filter;

public class RdvFilter extends BaseFilter {
	private String type;
	private String rdv;
	private String agtn;
	private String emission;

	public RdvFilter() {
		init();
	}

	public void init() {
		super.init();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRdv() {
		return rdv;
	}

	public String getAgtn() {
		return agtn;
	}

	
	public void setRdv(String rdv) {
		this.rdv = rdv;
	}

	public void setAgtn(String agtn) {
		this.agtn = agtn;
	}

	public String getEmission() {
		return emission;
	}

	public void setEmission(String emission) {
		this.emission = emission;
	}

	

}
