package com.datawings.app.filter;

import com.datawings.app.common.BeanUtil;

public class StockFilter {

	private String serialFrom;
	private String serialTo;
	private String quantity;
	private String quantityUsed;
	private String quantityAvailable;
	private String dateEntry;
	private String type;
	private String comment;
	private String checkDigit;
	private String agtn;
	private String typeMove;

	public StockFilter() {
		init();
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getAgtn() {
		return agtn;
	}

	public String getTypeMove() {
		return typeMove;
	}

	public void setTypeMove(String typeMove) {
		this.typeMove = typeMove;
	}

	public void setAgtn(String agtn) {
		this.agtn = agtn;
	}

	public String getSerialFrom() {
		return serialFrom;
	}

	public String getSerialTo() {
		return serialTo;
	}

	public String getQuantity() {
		return quantity;
	}

	public String getQuantityUsed() {
		return quantityUsed;
	}

	public String getQuantityAvailable() {
		return quantityAvailable;
	}

	public String getDateEntry() {
		return dateEntry;
	}

	public String getType() {
		return type;
	}

	public String getComment() {
		return comment;
	}

	public String getCheckDigit() {
		return checkDigit;
	}

	public void setSerialFrom(String serialFrom) {
		this.serialFrom = serialFrom;
	}

	public void setSerialTo(String serialTo) {
		this.serialTo = serialTo;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public void setQuantityUsed(String quantityUsed) {
		this.quantityUsed = quantityUsed;
	}

	public void setQuantityAvailable(String quantityAvailable) {
		this.quantityAvailable = quantityAvailable;
	}

	public void setDateEntry(String dateEntry) {
		this.dateEntry = dateEntry;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setCheckDigit(String checkDigit) {
		this.checkDigit = checkDigit;
	}

}
