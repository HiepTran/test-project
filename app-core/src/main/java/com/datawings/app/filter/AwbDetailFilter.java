package com.datawings.app.filter;

import com.datawings.app.common.BeanUtil;

public class AwbDetailFilter {
	private Integer awbDetailId;
	private String sequence;
	private String quantity;
	private String weightIndicator;
	private String weightGross;
	private String rateClass;
	private String weightCharge;
	private String unit;
	private String amount;
	private String description;
	
	public AwbDetailFilter() {
		init();
	}

	public Integer getAwbDetailId() {
		return awbDetailId;
	}

	public void setAwbDetailId(Integer awbDetailId) {
		this.awbDetailId = awbDetailId;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public void init() {
		BeanUtil.initSimplePropertyBean(this);
	}

	public String getQuantity() {
		return quantity;
	}

	public String getWeightIndicator() {
		return weightIndicator;
	}

	public String getWeightGross() {
		return weightGross;
	}

	public String getRateClass() {
		return rateClass;
	}

	public String getWeightCharge() {
		return weightCharge;
	}

	public String getUnit() {
		return unit;
	}

	public String getAmount() {
		return amount;
	}

	public String getDescription() {
		return description;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public void setWeightIndicator(String weightIndicator) {
		this.weightIndicator = weightIndicator;
	}

	public void setWeightGross(String weightGross) {
		this.weightGross = weightGross;
	}

	public void setRateClass(String rateClass) {
		this.rateClass = rateClass;
	}

	public void setWeightCharge(String weightCharge) {
		this.weightCharge = weightCharge;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
