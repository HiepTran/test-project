package com.datawings.app.filter;

import com.datawings.app.common.BeanUtil;

public class AgentGsaFilter extends BaseFilter {
	
	//filter
	private String codeFilter;
	private String nameFilter;
	
	//add
	private String codeAdd;
	private String nameAdd;
	private Double commAdd;
	private String dateStartAdd;
	private String dateEndAdd;
	
	//addDetail
	private String codeAddDetail;
	private String agtnAddDetail;

	public AgentGsaFilter() {
		init();
	}
	
	public void init(){
		super.init();
		BeanUtil.initSimplePropertyBean(this);
	}


	public String getCodeFilter() {
		return codeFilter;
	}

	public void setCodeFilter(String codeFilter) {
		this.codeFilter = codeFilter;
	}

	public String getNameFilter() {
		return nameFilter;
	}

	public void setNameFilter(String nameFilter) {
		this.nameFilter = nameFilter;
	}

	public String getCodeAdd() {
		return codeAdd;
	}

	public void setCodeAdd(String codeAdd) {
		this.codeAdd = codeAdd;
	}

	public String getNameAdd() {
		return nameAdd;
	}

	public void setNameAdd(String nameAdd) {
		this.nameAdd = nameAdd;
	}


	public Double getCommAdd() {
		return commAdd;
	}

	public void setCommAdd(Double commAdd) {
		this.commAdd = commAdd;
	}

	public String getDateStartAdd() {
		return dateStartAdd;
	}

	public void setDateStartAdd(String dateStartAdd) {
		this.dateStartAdd = dateStartAdd;
	}

	public String getDateEndAdd() {
		return dateEndAdd;
	}

	public void setDateEndAdd(String dateEndAdd) {
		this.dateEndAdd = dateEndAdd;
	}

	public String getAgtnAddDetail() {
		return agtnAddDetail;
	}

	public void setAgtnAddDetail(String agtnAddDetail) {
		this.agtnAddDetail = agtnAddDetail;
	}

	public String getCodeAddDetail() {
		return codeAddDetail;
	}

	public void setCodeAddDetail(String codeAddDetail) {
		this.codeAddDetail = codeAddDetail;
	}
	
}
