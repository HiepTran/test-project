package com.datawings.app.filter;

import java.util.Date;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.datawings.app.common.DateUtil;

public class FileFilter extends BaseFilter {

	private CommonsMultipartFile file;
	private String type;
	private String dateStr;
	private String isoc;
	private String agtnGsa;

	public FileFilter() {
		init();
	}

	public void init() {
		super.init();
		this.dateStr = DateUtil.date2String(new Date(), "dd/MM/yyyy");
	}

	public String getIsoc() {
		return isoc;
	}

	public void setIsoc(String isoc) {
		this.isoc = isoc;
	}

	public String getAgtnGsa() {
		return agtnGsa;
	}

	public void setAgtnGsa(String agtnGsa) {
		this.agtnGsa = agtnGsa;
	}

	public CommonsMultipartFile getFile() {
		return file;
	}

	public void setFile(CommonsMultipartFile file) {
		this.file = file;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDateStr() {
		return dateStr;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}

}
