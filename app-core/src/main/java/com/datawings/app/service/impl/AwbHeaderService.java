package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IAwbHeaderDao;
import com.datawings.app.model.AwbHeader;
import com.datawings.app.service.IAwbHeaderService;

@Service
public class AwbHeaderService implements IAwbHeaderService {
	
	@Autowired
	private IAwbHeaderDao dao;
	
	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<AwbHeader> findAll() {
		return dao.findAll();
	}

	@Transactional
	public AwbHeader find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(AwbHeader model) {
		return dao.save(model);
	}

	@Transactional
	public void update(AwbHeader model) {
		dao.update(model);
	}

	@Transactional
	public void merge(AwbHeader model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(AwbHeader model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(AwbHeader model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}
}
