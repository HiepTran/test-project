package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.filter.LineFilter;
import com.datawings.app.model.Line;

public interface ILineService {
	
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<Line> findAll();

	public Line find(Serializable userId);

	public Serializable save(Line model);

	public void update(Line model);

	public void merge(Line model);

	public void saveOrUpdate(Line model);

	public void delete(Line model);

	public void deleteById(Serializable id);

	public Line getLine(String orig, String dest);

	public List<Line> getListLine(LineFilter filter);
}
