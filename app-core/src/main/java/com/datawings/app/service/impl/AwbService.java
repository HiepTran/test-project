package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IAwbDao;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.model.Awb;
import com.datawings.app.service.IAwbService;

@Service
public class AwbService implements IAwbService {
	@Autowired
	private IAwbDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<Awb> findAll() {
		return dao.findAll();
	}

	//@Transactional
	public Awb find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(Awb model) {
		return dao.save(model);
	}

	@Transactional
	public void update(Awb model) {
		dao.update(model);
	}

	@Transactional
	public void merge(Awb model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(Awb model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(Awb model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Transactional
	public Integer getLTARowsCount(AwbFilter filter) {
		return dao.getLTARowsCount(filter);
	}

	@Transactional
	public List<Awb> getListAwb(AwbFilter filter) {
		return dao.getListAwb(filter);
	}

	@Transactional
	public List<Awb> getListAwbManifest(AwbFilter filter) {
		return dao.getListAwbManifest(filter);
	}

	@Transactional
	public Double getRate(String cutp, String cutpAirline, String date) {
		return dao.getRate(cutp, cutpAirline, date);
	}

	@Transactional
	public Double getKm(String orig, String dest) {
		return dao.getKm(orig, dest);
	}

	public List<Awb> reportSalesCass(ReportFilter filter) {
		return dao.reportSalesCass(filter);
	}

	@Transactional
	public Integer getNbAwbByRdv(String rdv) {
		return dao.getNbAwbByRdv(rdv);
	}

	@Transactional
	public List<Awb> getAwbByRdvCode(String rdv) {
		return dao.getAwbByRdvCode(rdv);
	}

	@Transactional
	public void deleteAwbTax(String lta) {
		dao.deleteAwbTax(lta);
	}

	@Transactional
	public List<Awb> getExcelAwb(AwbFilter filter) {
		return dao.getExcelAwb(filter);
	}

	@Transactional
	public void deleteAwbDetail(String lta) {
		dao.deleteAwbDetail(lta);
	}
}
