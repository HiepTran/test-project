package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IAgentGsaDao;
import com.datawings.app.filter.AgentGsaFilter;
import com.datawings.app.model.AgentGsa;
import com.datawings.app.service.IAgentGsaService;

@Service
public class AgentGsaService implements IAgentGsaService{
	
	@Autowired
	private IAgentGsaDao dao;
	
	@Transactional
	public AgentGsa getAgentGsa(String agtnCode, Date date) {
		return dao.getAgentGsa(agtnCode, date);
	}

	@Override
	public Integer getAgentGsaRowCount(AgentGsaFilter filter) {
		return dao.getAgentGsaRowCount(filter);
	}

	@Transactional
	public List<AgentGsa> getListAgentGsa(AgentGsaFilter filter) {
		return dao.getListAgentGsa(filter);
	}

	@Transactional
	public Serializable save(AgentGsa model) {
		return dao.save(model);
	}

	@Transactional
	public void update(AgentGsa model) {
		dao.update(model);		
	}

	@Transactional
	public void merge(AgentGsa model) {
		dao.merge(model);
	}

	@Transactional
	public void delete(AgentGsa model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Transactional
	public AgentGsa find(Serializable userId) {
		return dao.find(userId);
	}

	@Override
	public List<AgentGsa> getDetailAgentGsaByCode(String code) {
		return dao.getDetailAgentGsaByCode(code);
	}

	@Transactional
	public List<AgentGsa> findAll() {
		return dao.findAll();
	}

	@Transactional
	public AgentGsa getAgentGsaByCode(String code) {
		return dao.getAgentGsaByCode(code);
	}

	@Override
	public String getCountAgtn(String code) {
		return dao.getCountAgtn(code);
	}

	@Transactional
	public void deleteAgentGsaByCode(String id) {
		dao.deleteAgentGsaByCode(id);
	}

	@Transactional
	public void deleteAgtnByAgtnCode(String id, String code) {
		dao.deleteAgtnByAgtnCode(id, code);
	}
}
