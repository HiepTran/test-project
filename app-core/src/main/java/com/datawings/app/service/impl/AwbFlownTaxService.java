package com.datawings.app.service.impl;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IAwbFlownTaxDao;
import com.datawings.app.model.AwbFlownTax;
import com.datawings.app.service.IAwbFlownTaxService;

@Service
public class AwbFlownTaxService implements IAwbFlownTaxService{

	@Autowired
	private IAwbFlownTaxDao dao;
	
	@Transactional
	public AwbFlownTax find(Serializable id) {
		return dao.find(id);
	}

	@Transactional
	public void merge(AwbFlownTax model) {
		dao.merge(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);		
	}

	@Transactional
	public void deleteId(Integer id) {
		dao.deleteId(id);
	}

}
