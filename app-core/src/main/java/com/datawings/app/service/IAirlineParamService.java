package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.model.AirlineParam;

public interface IAirlineParamService {

	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<AirlineParam> findAll();

	public AirlineParam find(Serializable userId);

	public Serializable save(AirlineParam model);

	public void update(AirlineParam model);

	public void merge(AirlineParam model);

	public void saveOrUpdate(AirlineParam model);

	public void delete(AirlineParam model);

	public void deleteById(Serializable id);

	public AirlineParam findOne();
}
