package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.filter.StockFilter;
import com.datawings.app.model.StockAgent;

public interface IStockAgentService {
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<StockAgent> findAll();

	public StockAgent find(Serializable userId);

	public Serializable save(StockAgent model);

	public void update(StockAgent model);

	public void merge(StockAgent model);

	public void saveOrUpdate(StockAgent model);

	public void delete(StockAgent model);

	public void deleteById(Serializable id);

	public List<StockAgent> getStockAgent(StockFilter filter);
}
