package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.model.CritIsoc;

public interface ICritIsocService {
	
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<CritIsoc> findAll();

	public CritIsoc find(Serializable userId);

	public Serializable save(CritIsoc model);

	public void update(CritIsoc model);

	public void merge(CritIsoc model);

	public void saveOrUpdate(CritIsoc model);

	public void delete(CritIsoc model);

	public void deleteById(Serializable id);

	public List<CritIsoc> getAll();
}
