package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IBsrDao;
import com.datawings.app.filter.BsrFilter;
import com.datawings.app.model.Bsr;
import com.datawings.app.service.IBsrService;

@Service
public class BsrService implements IBsrService {
	
	@Autowired
	IBsrDao dao;

	@Transactional
	public Serializable save(Bsr model) {
		return dao.save(model);
	}

	@Override
	public Bsr find(Serializable bsrId) {
		return dao.find(bsrId);
	}

	@Transactional
	public void update(Bsr model) {
		dao.update(model);
	}

	@Transactional
	public void merge(Bsr model) {
		dao.merge(model);
	}

	@Transactional
	public void delete(Bsr model) {
		dao.delete(model);
	}

	@Override
	public List<Bsr> findAll() {
		return dao.findAll();
	}

	@Override
	public Integer getRowCountBsr(BsrFilter filter) {
		return dao.getRowCountBsr(filter);
	}

	@Override
	public List<Bsr> getBsrs(BsrFilter filter) {
		return dao.getBsrs(filter);
	}

	@Transactional
	public List<Bsr> getBsrFilter(BsrFilter filter) {
		return dao.getBsrFilter(filter);
	}

	@Transactional
	public List<Bsr> getBsrFilterByFromTo(BsrFilter filter) {
		return dao.getBsrFilterByFromTo(filter);
	}

	@Override
	public Bsr findBsrByCutpAndDateExt(String cutp, String currency, String dateExecute) {
		return dao.findBsrByCutpAndDateExt(cutp, currency, dateExecute);
	}

}
