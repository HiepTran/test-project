package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.ILoadLogDao;
import com.datawings.app.filter.FileFilter;
import com.datawings.app.model.LoadLog;
import com.datawings.app.service.ILoadLogService;

@Service
public class LoadLogService implements ILoadLogService{

	@Autowired
	private ILoadLogDao dao;
	
	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<LoadLog> findAll() {
		return dao.findAll();
	}

	@Transactional
	public LoadLog find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(LoadLog model) {
		return dao.save(model);
	}

	@Transactional
	public void update(LoadLog model) {
		dao.update(model);
	}

	@Transactional
	public void merge(LoadLog model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(LoadLog model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(LoadLog model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Override
	public Integer getLoadLogRowCount(FileFilter filter) {
		return dao.getLoadLogRowCount(filter);
	}

	@Override
	public List<LoadLog> getAllLoadLog(FileFilter filter) {
		return dao.getAllLoadLog(filter);
	}
}
