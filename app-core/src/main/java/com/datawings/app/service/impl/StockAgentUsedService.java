package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IStockAgentUsedDao;
import com.datawings.app.model.StockAgentUsed;
import com.datawings.app.service.IStockAgentUsedService;

@Service
public class StockAgentUsedService implements IStockAgentUsedService {
	@Autowired
	private IStockAgentUsedDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<StockAgentUsed> findAll() {
		return dao.findAll();
	}

	@Transactional
	public StockAgentUsed find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(StockAgentUsed model) {
		return dao.save(model);
	}

	@Transactional
	public void update(StockAgentUsed model) {
		dao.update(model);
	}

	@Transactional
	public void merge(StockAgentUsed model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(StockAgentUsed model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(StockAgentUsed model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}
}
