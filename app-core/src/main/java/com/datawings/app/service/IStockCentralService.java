package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.filter.StockFilter;
import com.datawings.app.model.StockCentral;

public interface IStockCentralService {

	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<StockCentral> findAll();

	public StockCentral find(Serializable userId);

	public Serializable save(StockCentral model);

	public void update(StockCentral model);

	public void merge(StockCentral model);

	public void saveOrUpdate(StockCentral model);

	public void delete(StockCentral model);

	public void deleteById(Serializable id);

	public List<StockCentral> getStockCentral(StockFilter filter);
	
}
