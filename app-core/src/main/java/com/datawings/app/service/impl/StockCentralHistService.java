package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IStockCentralHistDao;
import com.datawings.app.model.StockCentralHist;
import com.datawings.app.service.IStockCentralHistService;

@Service
public class StockCentralHistService implements IStockCentralHistService {

	@Autowired
	private IStockCentralHistDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<StockCentralHist> findAll() {
		return dao.findAll();
	}

	@Transactional
	public StockCentralHist find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(StockCentralHist model) {
		return dao.save(model);
	}

	@Transactional
	public void update(StockCentralHist model) {
		dao.update(model);
	}

	@Transactional
	public void merge(StockCentralHist model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(StockCentralHist model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(StockCentralHist model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

}
