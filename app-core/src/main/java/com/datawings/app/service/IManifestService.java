package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.filter.ManifestBean;
import com.datawings.app.filter.ManifestFilter;
import com.datawings.app.model.Manifest;

public interface IManifestService {

	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<Manifest> findAll();

	public Manifest find(Serializable userId);

	public Serializable save(Manifest model);

	public void update(Manifest model);

	public void merge(Manifest model);

	public void saveOrUpdate(Manifest model);

	public void delete(Manifest model);

	public void deleteById(Serializable id);

	public List<ManifestBean> getManifest(ManifestFilter filter);

	public Integer getRowsCount(ManifestFilter filter);

	public ManifestBean findReturnBean(String id);
}
