package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IAwbFlownDao;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.filter.Rmk;
import com.datawings.app.model.AwbFlown;
import com.datawings.app.service.IAwbFlownService;

@Service
public class AwbFlownService implements IAwbFlownService{
	@Autowired
	private IAwbFlownDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<AwbFlown> findAll() {
		return dao.findAll();
	}

	@Transactional
	public AwbFlown find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(AwbFlown model) {
		return dao.save(model);
	}

	@Transactional
	public void update(AwbFlown model) {
		dao.update(model);
	}

	@Transactional
	public void merge(AwbFlown model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(AwbFlown model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(AwbFlown model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Transactional
	public List<Rmk> reportRmk(ReportFilter filter, String beginYear, String endYear) {
		return dao.reportRmk(filter, beginYear, endYear);
	}

	@Transactional
	public List<AwbFlown> reportCaTransport(ReportFilter filter) {
		return dao.reportCaTransport(filter);
	}

	public List<AwbFlown> getListAwbManifest(AwbFilter filter, String manifestNo) {
		return dao.getListAwbManifest(filter, manifestNo);
	}

	public List<AwbFlown> getListAwbFlown(String id) {
		return dao.getListAwbFlown(id);
	}

	@Transactional
	public List<AwbFlown> getByLta(String lta) {
		return dao.getByLta(lta);
	}
}
