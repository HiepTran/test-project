package com.datawings.app.service;

import java.io.Serializable;

import com.datawings.app.model.AwbFlownTax;

public interface IAwbFlownTaxService {
	
	public void deleteById(Serializable id);
	
	public AwbFlownTax find(Serializable id);
	
	public void merge(AwbFlownTax model);

	public void deleteId(Integer awbFlownTaxId);
}
