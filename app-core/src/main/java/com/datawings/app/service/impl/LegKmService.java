package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.ILegKmDao;
import com.datawings.app.filter.LegKmFilter;
import com.datawings.app.model.LegKm;
import com.datawings.app.model.LegKmId;
import com.datawings.app.service.ILegKmService;


@Service
public class LegKmService implements ILegKmService {
	
	@Autowired private ILegKmDao dao;
	
	@Override
	@Transactional
	public List<LegKm> fineAll() {
		return dao.findAll();
	}

	@Override
	@Transactional
	public Serializable save(LegKm legKm) {
		return dao.save(legKm);
	}

	@Override
	@Transactional
	public void delete(LegKm legKm) {
		dao.delete(legKm);
	}

	@Override
	@Transactional
	public List<LegKm> getLegKm(LegKmFilter filter) {
		return dao.getLegKm(filter);
	}

	@Override
	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Override
	@Transactional
	public LegKm find(Serializable id) {
		return dao.find(id);
	}

	@Override
	@Transactional
	public void update(LegKm legkm) {
		dao.update(legkm);
	}

	@Override
	@Transactional
	public void merge(LegKm legkm) {
		dao.merge(legkm);
	}

	@Override
	@Transactional
	public void updateLeg(LegKmFilter legKmFilter, LegKmId legkmID) {
		dao.updateLeg(legKmFilter, legkmID);
	}

	@Override
	@Transactional
	public Integer getLegKmRouCount(LegKmFilter filter) {
		return dao.getLegKmRouCount(filter);
	}

}
