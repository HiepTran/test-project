package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IAirlineParamDao;
import com.datawings.app.model.AirlineParam;
import com.datawings.app.service.IAirlineParamService;

@Service
public class AirlineParamService implements IAirlineParamService{
	@Autowired
	private IAirlineParamDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<AirlineParam> findAll() {
		return dao.findAll();
	}

	@Transactional
	public AirlineParam find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(AirlineParam model) {
		return dao.save(model);
	}

	@Transactional
	public void update(AirlineParam model) {
		dao.update(model);
	}

	@Transactional
	public void merge(AirlineParam model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(AirlineParam model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(AirlineParam model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Override
	public AirlineParam findOne() {
		return dao.findOne();
	}

}
