package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IRdvDao;
import com.datawings.app.filter.RdvFilter;
import com.datawings.app.model.Rdv;
import com.datawings.app.service.IRdvService;

@Service
public class RdvService implements IRdvService{

	@Autowired
	private IRdvDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<Rdv> findAll() {
		return dao.findAll();
	}

	@Transactional
	public Rdv find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(Rdv model) {
		return dao.save(model);
	}

	@Transactional
	public void update(Rdv model) {
		dao.update(model);
	}

	@Transactional
	public void merge(Rdv model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(Rdv model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(Rdv model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Transactional
	public Rdv getRdv(String agtn, String date) {
		return dao.getRdv(agtn, date);
	}

	@Transactional
	public String getMaxRdvCode(String date) {
		return dao.getMaxRdvCode(date);
	}

	@Transactional
	public Integer getRdvRowsCount(RdvFilter filter) {
		return dao.getRdvRowsCount(filter);
	}

	@Transactional
	public List<Rdv> getListRdv(RdvFilter filter) {
		return dao.getListRdv(filter);
	}

	@Transactional
	public Rdv getRdv(RdvFilter filter) {
		return dao.getRdv(filter);
	}
}
