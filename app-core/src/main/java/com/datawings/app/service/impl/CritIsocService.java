package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.ICritIsocDao;
import com.datawings.app.model.CritIsoc;
import com.datawings.app.service.ICritIsocService;

@Service
public class CritIsocService implements ICritIsocService{

	@Autowired
	private ICritIsocDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<CritIsoc> findAll() {
		return dao.findAll();
	}

	@Transactional
	public CritIsoc find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(CritIsoc model) {
		return dao.save(model);
	}

	@Transactional
	public void update(CritIsoc model) {
		dao.update(model);
	}

	@Transactional
	public void merge(CritIsoc model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(CritIsoc model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(CritIsoc model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Transactional
	public List<CritIsoc> getAll() {
		return dao.getAll();
	}
	
}
