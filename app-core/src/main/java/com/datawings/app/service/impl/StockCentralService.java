package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IStockCentralDao;
import com.datawings.app.filter.StockFilter;
import com.datawings.app.model.StockCentral;
import com.datawings.app.service.IStockCentralService;

@Service
public class StockCentralService implements IStockCentralService{
	@Autowired
	private IStockCentralDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<StockCentral> findAll() {
		return dao.findAll();
	}

	@Transactional
	public StockCentral find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(StockCentral model) {
		return dao.save(model);
	}

	@Transactional
	public void update(StockCentral model) {
		dao.update(model);
	}

	@Transactional
	public void merge(StockCentral model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(StockCentral model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(StockCentral model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Transactional
	public List<StockCentral> getStockCentral(StockFilter filter) {
		return dao.getStockCentral(filter);
	}

}
