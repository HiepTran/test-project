package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IReportDao;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.model.Report;
import com.datawings.app.service.IReportService;

@Service
public class ReportService implements IReportService {
	@Autowired
	private IReportDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<Report> findAll() {
		return dao.findAll();
	}

	@Transactional
	public Report find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(Report model) {
		return dao.save(model);
	}

	@Transactional
	public void update(Report model) {
		dao.update(model);
	}

	@Transactional
	public void merge(Report model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(Report model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(Report model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Transactional
	public List<Report> getReports(ReportFilter filter) {
		return dao.getReports(filter);
	}
}
