package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.filter.Rmk;
import com.datawings.app.model.AwbFlown;

public interface IAwbFlownService {
	
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<AwbFlown> findAll();

	public AwbFlown find(Serializable userId);

	public Serializable save(AwbFlown model);

	public void update(AwbFlown model);

	public void merge(AwbFlown model);

	public void saveOrUpdate(AwbFlown model);

	public void delete(AwbFlown model);

	public void deleteById(Serializable id);

	public List<Rmk> reportRmk(ReportFilter filter, String beginYear, String endYear);

	public List<AwbFlown> reportCaTransport(ReportFilter filter);

	public List<AwbFlown> getListAwbManifest(AwbFilter filter, String manifestNo);

	public List<AwbFlown> getListAwbFlown(String id);

	public List<AwbFlown> getByLta(String lta);
}
