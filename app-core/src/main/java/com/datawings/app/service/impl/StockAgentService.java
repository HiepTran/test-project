package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IStockAgentDao;
import com.datawings.app.filter.StockFilter;
import com.datawings.app.model.StockAgent;
import com.datawings.app.service.IStockAgentService;

@Service
public class StockAgentService implements IStockAgentService {
	@Autowired
	private IStockAgentDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<StockAgent> findAll() {
		return dao.findAll();
	}

	@Transactional
	public StockAgent find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(StockAgent model) {
		return dao.save(model);
	}

	@Transactional
	public void update(StockAgent model) {
		dao.update(model);
	}

	@Transactional
	public void merge(StockAgent model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(StockAgent model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(StockAgent model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Transactional
	public List<StockAgent> getStockAgent(StockFilter filter) {
		return dao.getStockAgent(filter);
	}
}
