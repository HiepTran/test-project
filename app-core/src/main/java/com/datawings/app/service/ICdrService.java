package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.model.Cdr;

public interface ICdrService {
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<Cdr> findAll();

	public Cdr find(Serializable userId);

	public Serializable save(Cdr model);

	public void update(Cdr model);

	public void merge(Cdr model);

	public void saveOrUpdate(Cdr model);

	public void delete(Cdr model);

	public void deleteById(Serializable id);
}
