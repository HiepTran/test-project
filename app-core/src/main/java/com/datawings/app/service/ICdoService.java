package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.model.Cdo;

public interface ICdoService {
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<Cdo> findAll();

	public Cdo find(Serializable userId);

	public Serializable save(Cdo model);

	public void update(Cdo model);

	public void merge(Cdo model);

	public void saveOrUpdate(Cdo model);

	public void delete(Cdo model);

	public void deleteById(Serializable id);
}
