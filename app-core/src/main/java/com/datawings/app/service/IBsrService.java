package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.filter.BsrFilter;
import com.datawings.app.model.Bsr;

public interface IBsrService {
	
	public Serializable save(Bsr model);
	
	public Bsr find(Serializable bsrId);

	public void update(Bsr model);

	public void merge(Bsr model);

	public void delete(Bsr model);
	
	public List<Bsr> findAll();

	public Integer getRowCountBsr(BsrFilter filter);

	public List<Bsr> getBsrs(BsrFilter filter);

	public List<Bsr> getBsrFilter(BsrFilter filter);

	public List<Bsr> getBsrFilterByFromTo(BsrFilter filter);

	public Bsr findBsrByCutpAndDateExt(String cutp, String currency, String dateExecute);

}
