package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.filter.FileFilter;
import com.datawings.app.model.LoadLog;

public interface ILoadLogService {
	
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<LoadLog> findAll();

	public LoadLog find(Serializable userId);

	public Serializable save(LoadLog model);

	public void update(LoadLog model);

	public void merge(LoadLog model);

	public void saveOrUpdate(LoadLog model);

	public void delete(LoadLog model);

	public void deleteById(Serializable id);

	public Integer getLoadLogRowCount(FileFilter filter);

	public List<LoadLog> getAllLoadLog(FileFilter filter);
}
