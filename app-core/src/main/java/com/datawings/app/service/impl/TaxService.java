package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.ITaxDao;
import com.datawings.app.filter.TaxFilter;
import com.datawings.app.model.Tax;
import com.datawings.app.service.ITaxService;

@Repository
public class TaxService implements ITaxService{
	@Autowired
	private ITaxDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<Tax> findAll() {
		return dao.findAll();
	}

	@Transactional
	public Tax find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(Tax model) {
		return dao.save(model);
	}

	@Transactional
	public void update(Tax model) {
		dao.update(model);
	}

	@Transactional
	public void merge(Tax model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(Tax model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(Tax model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Transactional
	public Tax getTax(String isoc, String code, String subCode, String item, String date, String type) {
		return dao.getTax(isoc, code, subCode, item, date, type);
	}

	@Transactional
	public List<Tax> getListtax(TaxFilter filter) {
		return dao.getListtax(filter);
	}

	@Transactional
	public Tax getTaxValidator(TaxFilter filter) {
		return dao.getTaxValidator(filter);
	}

}
