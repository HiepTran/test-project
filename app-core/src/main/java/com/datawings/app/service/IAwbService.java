package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.model.Awb;

public interface IAwbService {
	
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<Awb> findAll();

	public Awb find(Serializable userId);

	public Serializable save(Awb model);

	public void update(Awb model);

	public void merge(Awb model);

	public void saveOrUpdate(Awb model);

	public void delete(Awb model);

	public void deleteById(Serializable id);

	public Integer getLTARowsCount(AwbFilter filter);

	public List<Awb> getListAwb(AwbFilter filter);

	public List<Awb> getListAwbManifest(AwbFilter filter);

	public Double getRate(String cutp, String cutpAirline, String date2String);

	public Double getKm(String orig, String dest);

	public List<Awb> reportSalesCass(ReportFilter filter);

	public Integer getNbAwbByRdv(String code);

	public List<Awb> getAwbByRdvCode(String rdv);

	public void deleteAwbTax(String lta);

	public List<Awb> getExcelAwb(AwbFilter filter);

	public void deleteAwbDetail(String lta);
}
