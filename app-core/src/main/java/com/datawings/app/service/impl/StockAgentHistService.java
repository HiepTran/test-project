package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IStockAgentHistDao;
import com.datawings.app.model.StockAgentHist;
import com.datawings.app.service.IStockAgentHistService;

@Service
public class StockAgentHistService implements IStockAgentHistService{
	@Autowired
	private IStockAgentHistDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<StockAgentHist> findAll() {
		return dao.findAll();
	}

	@Transactional
	public StockAgentHist find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(StockAgentHist model) {
		return dao.save(model);
	}

	@Transactional
	public void update(StockAgentHist model) {
		dao.update(model);
	}

	@Transactional
	public void merge(StockAgentHist model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(StockAgentHist model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(StockAgentHist model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}
}
