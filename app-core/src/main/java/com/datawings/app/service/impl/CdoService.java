package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.ICdoDao;
import com.datawings.app.model.Cdo;
import com.datawings.app.service.ICdoService;

@Service
public class CdoService implements ICdoService{
	@Autowired
	private ICdoDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<Cdo> findAll() {
		return dao.findAll();
	}

	@Transactional
	public Cdo find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(Cdo model) {
		return dao.save(model);
	}

	@Transactional
	public void update(Cdo model) {
		dao.update(model);
	}

	@Transactional
	public void merge(Cdo model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(Cdo model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(Cdo model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}
}
