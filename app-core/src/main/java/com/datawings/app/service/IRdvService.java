package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.filter.RdvFilter;
import com.datawings.app.model.Rdv;

public interface IRdvService {

	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<Rdv> findAll();

	public Rdv find(Serializable userId);

	public Serializable save(Rdv model);

	public void update(Rdv model);

	public void merge(Rdv model);

	public void saveOrUpdate(Rdv model);

	public void delete(Rdv model);

	public void deleteById(Serializable id);

	public Rdv getRdv(String agtn, String date);

	public String getMaxRdvCode(String date);

	public Integer getRdvRowsCount(RdvFilter filter);

	public List<Rdv> getListRdv(RdvFilter filter);

	public Rdv getRdv(RdvFilter filter);
}
