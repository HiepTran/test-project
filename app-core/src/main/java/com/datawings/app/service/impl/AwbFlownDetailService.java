package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IAwbFlownDetailDao;
import com.datawings.app.model.AwbFlownDetail;
import com.datawings.app.service.IAwbFlownDetailService;

@Service
public class AwbFlownDetailService implements IAwbFlownDetailService{

	@Autowired
	private IAwbFlownDetailDao dao;
	
	@Transactional
	public Integer getRowsCount() {
		return null;
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<AwbFlownDetail> findAll() {
		return dao.findAll();
	}

	@Transactional
	public AwbFlownDetail find(Integer id) {
		return dao.find(id);
	}

	@Transactional
	public Serializable save(AwbFlownDetail model) {
		return dao.save(model);
	}

	@Transactional
	public void update(AwbFlownDetail model) {
		dao.update(model);
	}

	@Transactional
	public void merge(AwbFlownDetail model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(AwbFlownDetail model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(AwbFlownDetail model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(AwbFlownDetail id) {
		dao.deleteById(id);
	}

}
