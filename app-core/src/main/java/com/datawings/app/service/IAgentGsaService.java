package com.datawings.app.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.datawings.app.filter.AgentGsaFilter;
import com.datawings.app.model.AgentGsa;

public interface IAgentGsaService {
	
	public Serializable save(AgentGsa model);
	
	public AgentGsa find(Serializable userId);

	public void update(AgentGsa model);

	public void merge(AgentGsa model);

	public void delete(AgentGsa model);

	public void deleteById(Serializable id);
	
	public AgentGsa getAgentGsa(String agtnCode, Date date2String);

	public Integer getAgentGsaRowCount(AgentGsaFilter filter);

	public List<AgentGsa> getListAgentGsa(AgentGsaFilter filter);

	public List<AgentGsa> getDetailAgentGsaByCode(String code);

	public List<AgentGsa> findAll();

	public AgentGsa getAgentGsaByCode(String code);

	public String getCountAgtn(String code);

	public void deleteAgentGsaByCode(String id);

	public void deleteAgtnByAgtnCode(String id, String code);
}
