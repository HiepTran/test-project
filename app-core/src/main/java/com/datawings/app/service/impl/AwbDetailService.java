package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IAwbDetailDao;
import com.datawings.app.model.AwbDetail;
import com.datawings.app.model.AwbDetailId;
import com.datawings.app.service.IAwbDetailService;

@Service
public class AwbDetailService implements IAwbDetailService{

	@Autowired
	private IAwbDetailDao dao;
	
	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<AwbDetail> findAll() {
		return dao.findAll();
	}

	@Transactional
	public AwbDetail find(AwbDetailId id) {
		return dao.find(id);
	}

	@Transactional
	public Serializable save(AwbDetail model) {
		return dao.save(model);
	}

	@Transactional
	public void update(AwbDetail model) {
		dao.update(model);
	}

	@Transactional
	public void merge(AwbDetail model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(AwbDetail model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(AwbDetail model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(AwbDetailId id) {
		dao.deleteById(id);
	}

}
