package com.datawings.app.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IDashboardDao;
import com.datawings.app.filter.Dashboard;
import com.datawings.app.service.IDashboardService;

@Service
public class DashboardService implements IDashboardService{

	@Autowired
	private IDashboardDao dao;

	@Transactional
	public Date getCurrentDate() {
		return dao.getCurrentDate();
	}

	@Transactional
	public List<Dashboard> getListAwb(String year) {
		return dao.getListAwb(year);
	}

	@Transactional
	public List<Dashboard> getListTransport(String year) {
		return dao.getListTransport(year);
	}
	
}
