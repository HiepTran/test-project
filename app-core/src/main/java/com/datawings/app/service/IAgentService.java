package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.filter.AgentFilter;
import com.datawings.app.model.Agent;

public interface IAgentService {
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<Agent> findAll();

	public Agent find(Serializable userId);

	public Serializable save(Agent model);

	public void update(Agent model);

	public void merge(Agent model);

	public void saveOrUpdate(Agent model);

	public void delete(Agent model);

	public void deleteById(Serializable id);

	public List<Agent> getAgent(AgentFilter filter);

	public List<String> getIsoc();

	public Integer getAgentRowCount(AgentFilter filter);

	public List<Agent> getAgentEX(AgentFilter filter);

	public List<String> findListAgentCode(String agtnAddDetail);
}
