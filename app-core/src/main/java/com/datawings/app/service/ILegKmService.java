package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.filter.LegKmFilter;
import com.datawings.app.model.LegKm;
import com.datawings.app.model.LegKmId;

public interface ILegKmService {
	public List<LegKm> fineAll();
	
	public Serializable save(LegKm legKm);
	
	public void delete(LegKm legKm);
	
	public List<LegKm> getLegKm(LegKmFilter filter);
	
	public void deleteById(Serializable id);

	public LegKm find(Serializable id);
	
	public void update(LegKm legkm);
	
	public void merge(LegKm legkm);
	
	public void updateLeg(LegKmFilter legKmFilter, LegKmId legkmID);

	public Integer getLegKmRouCount(LegKmFilter legKmFilter);
	
}
