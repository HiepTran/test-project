package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.ILineDao;
import com.datawings.app.filter.LineFilter;
import com.datawings.app.model.Line;
import com.datawings.app.service.ILineService;

@Service
public class LineService implements ILineService {
	@Autowired
	private ILineDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<Line> findAll() {
		return dao.findAll();
	}

	@Transactional
	public Line find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(Line model) {
		return dao.save(model);
	}

	@Transactional
	public void update(Line model) {
		dao.update(model);
	}

	@Transactional
	public void merge(Line model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(Line model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(Line model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Transactional
	public Line getLine(String orig, String dest) {
		return dao.getLine(orig, dest);
	}

	@Transactional
	public List<Line> getListLine(LineFilter filter) {
		return dao.getListLine(filter);
	}
}
