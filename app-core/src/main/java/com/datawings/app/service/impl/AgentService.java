package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IAgentDao;
import com.datawings.app.filter.AgentFilter;
import com.datawings.app.model.Agent;
import com.datawings.app.service.IAgentService;

@Repository
public class AgentService implements IAgentService {
	@Autowired
	private IAgentDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<Agent> findAll() {
		return dao.findAll();
	}

	@Transactional
	public Agent find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(Agent model) {
		return dao.save(model);
	}

	@Transactional
	public void update(Agent model) {
		dao.update(model);
	}

	@Transactional
	public void merge(Agent model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(Agent model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(Agent model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Transactional
	public List<Agent> getAgent(AgentFilter filter) {
		return dao.getAgent(filter);
	}

	@Transactional
	public List<String> getIsoc() {
		return dao.getIsoc();
	}

	@Override
	@Transactional
	public Integer getAgentRowCount(AgentFilter filter) {
		return dao.getAgentRowCount(filter);
	}

	@Override
	@Transactional
	public List<Agent> getAgentEX(AgentFilter filter) {
		return dao.getAgentEX(filter);
	}

	@Override
	public List<String> findListAgentCode(String agtnAddDetail) {
		return dao.findListAgentCode(agtnAddDetail);
	}
}
