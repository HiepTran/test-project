package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.ICdrDao;
import com.datawings.app.model.Cdr;
import com.datawings.app.service.ICdrService;

@Service
public class CdrService implements ICdrService {

	@Autowired
	private ICdrDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<Cdr> findAll() {
		return dao.findAll();
	}

	@Transactional
	public Cdr find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(Cdr model) {
		return dao.save(model);
	}

	@Transactional
	public void update(Cdr model) {
		dao.update(model);
	}

	@Transactional
	public void merge(Cdr model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(Cdr model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(Cdr model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}
}
