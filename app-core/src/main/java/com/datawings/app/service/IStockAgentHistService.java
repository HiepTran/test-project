package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.model.StockAgentHist;

public interface IStockAgentHistService {
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<StockAgentHist> findAll();

	public StockAgentHist find(Serializable userId);

	public Serializable save(StockAgentHist model);

	public void update(StockAgentHist model);

	public void merge(StockAgentHist model);

	public void saveOrUpdate(StockAgentHist model);

	public void delete(StockAgentHist model);

	public void deleteById(Serializable id);
}
