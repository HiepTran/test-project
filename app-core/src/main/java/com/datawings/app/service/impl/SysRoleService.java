package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.ISysRoleDao;
import com.datawings.app.model.SysRole;
import com.datawings.app.model.SysUser;
import com.datawings.app.service.ISysRoleService;

/**
 * @author DW
 *
 */
@Service
public class SysRoleService implements ISysRoleService{

	@Autowired private ISysRoleDao dao;
	
	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<SysRole> findAll() {
		return dao.findAll();
	}

	@Transactional
	public SysRole find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(SysRole model) {
		return dao.save(model);
	}

	@Transactional
	public void update(SysRole model) {
		dao.update(model);
	}

	@Transactional
	public void merge(SysRole model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(SysRole model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(SysRole model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Transactional
	public List<SysRole> getListRoleNotIncludeUser(Integer id) {
		return dao.getListRoleNotIncludeUser(id);
	}

	@Transactional
	public SysRole getRoleByName(String role) {
		return dao.getRoleByName(role);
	}

	@Transactional
	public void deleteRoleUser(Integer roleId) {
		dao.deleteRoleUser(roleId);
	}

	@Transactional
	public void deleteByRoleName(String string) {
		dao.deleteByRoleName(string);
	}

	@Transactional
	public List<SysRole> getRoleOrderByRoleName() {
		return dao.getRoleOrderByRoleName();
	}

	@Transactional
	public List<SysRole> findAllRoleUser(SysUser prinUser) {
		return dao.findAllRoleUser(prinUser);
	}

	@Transactional
	public List<SysRole> findRoleOfUserByUserID(Integer id) {
		return dao.findRoleOfUserByUserID(id);
	}

	@Override
	public SysRole findRoleByName(String roleNameAdd) {
		return dao.findRoleByName(roleNameAdd);
	}

}
