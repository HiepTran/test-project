package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.model.SysRole;
import com.datawings.app.model.SysUser;

public interface ISysRoleService {
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<SysRole> findAll();

	public SysRole find(Serializable userId);

	public Serializable save(SysRole model);

	public void update(SysRole model);

	public void merge(SysRole model);

	public void saveOrUpdate(SysRole model);

	public void delete(SysRole model);

	public void deleteById(Serializable id);

	public List<SysRole> getListRoleNotIncludeUser(Integer id);

	public SysRole getRoleByName(String role);
	
	public void deleteRoleUser(Integer roleId);

	public void deleteByRoleName(String string);

	public List<SysRole> getRoleOrderByRoleName();

	public List<SysRole> findAllRoleUser(SysUser prinUser);

	public List<SysRole> findRoleOfUserByUserID(Integer id);

	public SysRole findRoleByName(String roleNameAdd);
	
}
