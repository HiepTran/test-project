package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.model.AwbDetail;
import com.datawings.app.model.AwbDetailId;

public interface IAwbDetailService {
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<AwbDetail> findAll();

	public AwbDetail find(AwbDetailId userId);

	public Serializable save(AwbDetail model);

	public void update(AwbDetail model);

	public void merge(AwbDetail model);

	public void saveOrUpdate(AwbDetail model);

	public void delete(AwbDetail model);

	public void deleteById(AwbDetailId id);
}
