package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.model.AwbHeader;

public interface IAwbHeaderService {
	
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<AwbHeader> findAll();

	public AwbHeader find(Serializable userId);

	public Serializable save(AwbHeader model);

	public void update(AwbHeader model);

	public void merge(AwbHeader model);

	public void saveOrUpdate(AwbHeader model);

	public void delete(AwbHeader model);

	public void deleteById(Serializable id);
}
