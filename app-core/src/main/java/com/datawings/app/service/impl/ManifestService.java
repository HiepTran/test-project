package com.datawings.app.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.dao.IManifestDao;
import com.datawings.app.filter.ManifestBean;
import com.datawings.app.filter.ManifestFilter;
import com.datawings.app.model.Manifest;
import com.datawings.app.service.IManifestService;

@Service
public class ManifestService implements IManifestService{
	@Autowired
	private IManifestDao dao;

	@Transactional
	public Integer getRowsCount() {
		return dao.getRowsCount();
	}

	@Transactional
	public Integer getIdMax(String propertyName) {
		return dao.getIdMax(propertyName);
	}

	@Transactional
	public List<Manifest> findAll() {
		return dao.findAll();
	}
	
	public Manifest find(Serializable userId) {
		return dao.find(userId);
	}

	@Transactional
	public Serializable save(Manifest model) {
		return dao.save(model);
	}

	@Transactional
	public void update(Manifest model) {
		dao.update(model);
	}

	@Transactional
	public void merge(Manifest model) {
		dao.merge(model);
	}

	@Transactional
	public void saveOrUpdate(Manifest model) {
		dao.saveOrUpdate(model);
	}

	@Transactional
	public void delete(Manifest model) {
		dao.delete(model);
	}

	@Transactional
	public void deleteById(Serializable id) {
		dao.deleteById(id);
	}

	@Transactional
	public List<ManifestBean> getManifest(ManifestFilter filter) {
		return dao.getManifest(filter);
	}

	@Transactional
	public Integer getRowsCount(ManifestFilter filter) {
		return dao.getRowsCount(filter);
	}

	@Transactional
	public ManifestBean findReturnBean(String id) {
		return dao.findReturnBean(id);
	}
}
