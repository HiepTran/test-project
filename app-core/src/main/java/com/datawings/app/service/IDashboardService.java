package com.datawings.app.service;

import java.util.Date;
import java.util.List;

import com.datawings.app.filter.Dashboard;

public interface IDashboardService {

	Date getCurrentDate();

	List<Dashboard> getListAwb(String year);

	List<Dashboard> getListTransport(String year);

}
