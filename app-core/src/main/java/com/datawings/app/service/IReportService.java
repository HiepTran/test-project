package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.filter.ReportFilter;
import com.datawings.app.model.Report;

public interface IReportService {

	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<Report> findAll();

	public Report find(Serializable userId);

	public Serializable save(Report model);

	public void update(Report model);

	public void merge(Report model);

	public void saveOrUpdate(Report model);

	public void delete(Report model);

	public void deleteById(Serializable id);

	public List<Report> getReports(ReportFilter filter);
	
}
