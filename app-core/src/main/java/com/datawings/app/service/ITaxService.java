package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.filter.TaxFilter;
import com.datawings.app.model.Tax;

public interface ITaxService {
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<Tax> findAll();

	public Tax find(Serializable userId);

	public Serializable save(Tax model);

	public void update(Tax model);

	public void merge(Tax model);

	public void saveOrUpdate(Tax model);

	public void delete(Tax model);

	public void deleteById(Serializable id);

	public Tax getTax(String isoc, String code, String subCode, String item, String date, String type);

	public List<Tax> getListtax(TaxFilter filter);

	public Tax getTaxValidator(TaxFilter filter);
}
