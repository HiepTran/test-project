package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.model.AwbFlownDetail;

public interface IAwbFlownDetailService {
	
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<AwbFlownDetail> findAll();

	public AwbFlownDetail find(Integer id);

	public Serializable save(AwbFlownDetail model);

	public void update(AwbFlownDetail model);

	public void merge(AwbFlownDetail model);

	public void saveOrUpdate(AwbFlownDetail model);

	public void delete(AwbFlownDetail model);

	public void deleteById(AwbFlownDetail id);
}
