package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.model.StockAgentUsed;

public interface IStockAgentUsedService {
	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<StockAgentUsed> findAll();

	public StockAgentUsed find(Serializable userId);

	public Serializable save(StockAgentUsed model);

	public void update(StockAgentUsed model);

	public void merge(StockAgentUsed model);

	public void saveOrUpdate(StockAgentUsed model);

	public void delete(StockAgentUsed model);

	public void deleteById(Serializable id);
}
