package com.datawings.app.service;

import java.io.Serializable;
import java.util.List;

import com.datawings.app.model.StockCentralHist;

public interface IStockCentralHistService {

	public Integer getRowsCount();

	public Integer getIdMax(String propertyName);

	public List<StockCentralHist> findAll();

	public StockCentralHist find(Serializable userId);

	public Serializable save(StockCentralHist model);

	public void update(StockCentralHist model);

	public void merge(StockCentralHist model);

	public void saveOrUpdate(StockCentralHist model);

	public void delete(StockCentralHist model);

	public void deleteById(Serializable id);

}
