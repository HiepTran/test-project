package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.AgentFilter;
import com.datawings.app.model.Agent;

public interface IAgentDao extends IBaseDao<Agent, String>{

	List<Agent> getAgent(AgentFilter filter);

	List<String> getIsoc();

	Integer getAgentRowCount(AgentFilter filter);

	List<Agent> getAgentEX(AgentFilter filter);

	List<String> findListAgentCode(String agtnAddDetail);

}
