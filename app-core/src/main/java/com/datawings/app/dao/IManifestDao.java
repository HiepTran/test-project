package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.ManifestBean;
import com.datawings.app.filter.ManifestFilter;
import com.datawings.app.model.Manifest;

public interface IManifestDao extends IBaseDao<Manifest, String>{

	List<ManifestBean> getManifest(ManifestFilter filter);

	Integer getRowsCount(ManifestFilter filter);

	ManifestBean findReturnBean(String id);

}
