package com.datawings.app.dao.impl;

import org.springframework.stereotype.Repository;

import com.datawings.app.dao.ICdrDao;
import com.datawings.app.model.Cdr;

@Repository
public class CdrDao extends BaseDao<Cdr, String> implements ICdrDao{

}
