package com.datawings.app.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.ILineDao;
import com.datawings.app.filter.LineFilter;
import com.datawings.app.model.Line;

@Repository
public class LineDao extends BaseDao<Line, String> implements ILineDao{

	public Line getLine(String orig, String dest) {
		StringBuffer hql = new StringBuffer();
		hql.append("select * from line");
		hql.append(" where (trim(orac) = '" +  orig.trim().toUpperCase() + "' or trim(orac) = '" + orig.trim().toUpperCase() + "')");
		hql.append(" and (trim(dstc) =  '" + dest.trim().toUpperCase() + "' or trim(dstc) = '" + dest.trim().toUpperCase() + "')" );
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		Line rs = (Line) query.addEntity(Line.class).uniqueResult();
		return rs;	
	}

	@SuppressWarnings("unchecked")
	public List<Line> getListLine(LineFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		
		if(StringUtils.isNotBlank(filter.getOrig())){
			crit.add(Restrictions.eq("orac", filter.getOrig().trim().toUpperCase()));
		}
		if(StringUtils.isNotBlank(filter.getDest())){
			crit.add(Restrictions.eq("dstc", filter.getDest().trim().toUpperCase()));
		}
		if(StringUtils.isNotBlank(filter.getLine())){
			crit.add(Restrictions.eq("line", filter.getLine().trim().toUpperCase()));
		}
		
		crit.addOrder(Order.asc("orac"));
		List<Line> result = crit.list();		
		return result;
	}

}
