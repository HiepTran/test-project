package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.filter.Rmk;
import com.datawings.app.model.AwbFlown;

public interface IAwbFlownDao extends IBaseDao<AwbFlown, Integer>{

	List<Rmk> reportRmk(ReportFilter filter, String beginYear, String endYear);

	List<AwbFlown> reportCaTransport(ReportFilter filter);

	List<AwbFlown> getListAwbManifest(AwbFilter filter, String manifestNo);

	List<AwbFlown> getListAwbFlown(String id);

	List<AwbFlown> getByLta(String lta);

}
