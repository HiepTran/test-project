package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.StockFilter;
import com.datawings.app.model.StockAgent;

public interface IStockAgentDao extends IBaseDao<StockAgent, Integer>{

	List<StockAgent> getStockAgent(StockFilter filter);

}
