package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.BsrFilter;
import com.datawings.app.model.Bsr;
import com.datawings.app.model.BsrId;

public interface IBsrDao extends IBaseDao<Bsr, BsrId> {

	List<Bsr> getBsrFilter(BsrFilter filter);

	Integer getRowCountBsr(BsrFilter filter);

	List<Bsr> getBsrs(BsrFilter filter);

	List<Bsr> getBsrFilterByFromTo(BsrFilter filter);

	Bsr findBsrByCutpAndDateExt(String cutp, String currency, String dateExecute);
	
}
