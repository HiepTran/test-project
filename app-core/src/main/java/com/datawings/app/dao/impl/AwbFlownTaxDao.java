package com.datawings.app.dao.impl;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IAwbFlownTaxDao;
import com.datawings.app.model.AwbFlownTax;

@Repository
public class AwbFlownTaxDao extends BaseDao<AwbFlownTax, Integer> implements IAwbFlownTaxDao{

	@SuppressWarnings("deprecation")
	public void deleteId(Integer id) {
		StringBuffer hql = new StringBuffer();
		hql.append("delete from awb_flown_tax");
		hql.append(" where awb_flown_tax_id ='" +  id + "'");
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		query.executeUpdate();		
	}

}
