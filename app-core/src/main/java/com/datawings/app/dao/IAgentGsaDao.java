package com.datawings.app.dao;

import java.util.Date;
import java.util.List;

import com.datawings.app.filter.AgentGsaFilter;
import com.datawings.app.model.AgentGsa;

public interface IAgentGsaDao extends IBaseDao<AgentGsa, Integer>{

	AgentGsa getAgentGsa(String agtnCode, Date date);

	Integer getAgentGsaRowCount(AgentGsaFilter filter);

	List<AgentGsa> getListAgentGsa(AgentGsaFilter filter);

	List<AgentGsa> getDetailAgentGsaByCode(String code);

	AgentGsa getAgentGsaByCode(String code);

	String getCountAgtn(String code);

	void deleteAgentGsaByCode(String id);

	void deleteAgtnByAgtnCode(String id, String code);

}
