package com.datawings.app.dao.impl;

import org.springframework.stereotype.Service;

import com.datawings.app.dao.IStockAgentHistDao;
import com.datawings.app.model.StockAgentHist;

@Service
public class StockAgentHistDao extends BaseDao<StockAgentHist, Integer> implements IStockAgentHistDao{

}
