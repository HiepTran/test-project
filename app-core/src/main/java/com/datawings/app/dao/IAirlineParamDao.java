package com.datawings.app.dao;

import com.datawings.app.model.AirlineParam;

public interface IAirlineParamDao extends IBaseDao<AirlineParam, String>{

	AirlineParam findOne();

}
