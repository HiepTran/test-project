package com.datawings.app.dao;

import com.datawings.app.model.StockCentralHist;

public interface IStockCentralHistDao extends IBaseDao<StockCentralHist, Integer>{

}
