package com.datawings.app.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.datawings.app.common.SqlUtil;
import com.datawings.app.dao.IAwbDao;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.model.Awb;
import com.datawings.app.process.CargoUtils;

@Repository
public class AwbDao extends BaseDao<Awb, String> implements IAwbDao{

	public Integer getLTARowsCount(AwbFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		if(StringUtils.isNotBlank(filter.getLta())){
			crit.add(Restrictions.like("lta", filter.getLta(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getChannel())){
			crit.add(Restrictions.eq("channel", filter.getChannel().trim()));
		}
		if(StringUtils.isNotBlank(filter.getOrig())){
			crit.add(Restrictions.like("orig", filter.getOrig().trim().toUpperCase(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getDest())){
			crit.add(Restrictions.like("dest", filter.getDest().trim().toUpperCase(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getAgtn())){
			crit.add(Restrictions.like("agtn", filter.getAgtn().trim(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getDateFrom().trim()) || StringUtils.isNotBlank(filter.getDateTo().trim()) ){
			SqlUtil.buildDatesBetweenLikeCrit("date_execute", filter.getDateFrom().trim(), filter.getDateTo().trim(), crit);			
		}
		if(StringUtils.isNotBlank(filter.getRdv())){
			crit.add(Restrictions.eq("rdv", filter.getRdv().trim().toUpperCase()));
		}
		return ((Number) crit.setProjection(Projections.rowCount()).uniqueResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	public List<Awb> getListAwb(AwbFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		if(StringUtils.isNotBlank(filter.getLta())){
			crit.add(Restrictions.like("lta", filter.getLta(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getChannel())){
			crit.add(Restrictions.eq("channel", filter.getChannel().trim()));
		}
		if(StringUtils.isNotBlank(filter.getOrig())){
			crit.add(Restrictions.like("orig", filter.getOrig().trim().toUpperCase(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getDest())){
			crit.add(Restrictions.like("dest", filter.getDest().trim().toUpperCase(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getAgtn())){
			crit.add(Restrictions.like("agtn", filter.getAgtn().trim(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getDateFrom().trim()) || StringUtils.isNotBlank(filter.getDateTo().trim()) ){
			SqlUtil.buildDatesBetweenLikeCrit("date_execute", filter.getDateFrom().trim(), filter.getDateTo().trim(), crit);			
		}
		if(StringUtils.isNotBlank(filter.getRdv())){
			crit.add(Restrictions.eq("rdv", filter.getRdv().trim().toUpperCase()));
		}
		
		crit.addOrder(Order.asc("lta"));	
		crit.setMaxResults(filter.getPageSize());
		crit.setFirstResult(filter.getOffset());
		
		List<Awb> result = crit.list();		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Awb> getExcelAwb(AwbFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		if(StringUtils.isNotBlank(filter.getLta())){
			crit.add(Restrictions.like("lta", filter.getLta(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getChannel())){
			crit.add(Restrictions.eq("channel", filter.getChannel().trim()));
		}
		if(StringUtils.isNotBlank(filter.getOrig())){
			crit.add(Restrictions.like("orig", filter.getOrig().trim().toUpperCase(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getDest())){
			crit.add(Restrictions.like("dest", filter.getDest().trim().toUpperCase(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getAgtn())){
			crit.add(Restrictions.like("agtn", filter.getAgtn().trim(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getDateFrom().trim()) || StringUtils.isNotBlank(filter.getDateTo().trim()) ){
			SqlUtil.buildDatesBetweenLikeCrit("date_execute", filter.getDateFrom().trim(), filter.getDateTo().trim(), crit);			
		}
		if(StringUtils.isNotBlank(filter.getRdv())){
			crit.add(Restrictions.eq("rdv", filter.getRdv().trim().toUpperCase()));
		}
		
		crit.addOrder(Order.asc("lta"));	
		List<Awb> result = crit.list();		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Awb> getListAwbManifest(AwbFilter filter) {
		StringBuffer hql = new StringBuffer();
		hql.append("select * from awb");
		hql.append(" where status_flown !='F'");
		//hql.append(" and (position(manifest in '"+ manifest + "') > 0)"); 
		
		if(StringUtils.isNotBlank(filter.getOrig())){
			hql.append(" and orig ='" + filter.getOrig() + "'");
		}
		if(StringUtils.isNotBlank(filter.getDest())){
			hql.append(" and dest ='" + filter.getDest() + "'");
		}
		if(StringUtils.isNotBlank(filter.getLta())){
			hql.append(" and lta like '%" + filter.getLta() + "%'");
		}
		if(StringUtils.isNotBlank(filter.getDateExecute())){
			hql.append(" and to_char(date_execute,'dd/MM/yyyy') ='" + filter.getDateExecute() + "'");
		}
		hql.append(" order by lta");
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		
		List<Awb> rs = query.addEntity(Awb.class).list();
		return rs;
		
	}

	public Double getRate(String cutp, String cutpAirline, String date) {
		StringBuffer hql = new StringBuffer();
		hql.append("select rate from bsr");
		hql.append(" where curr_from ='" + cutp + "'");
		hql.append(" and curr_to='" + cutpAirline + "'");
		hql.append(" and date_start <= '" + date + "'");
		hql.append(" and date_end >= '" + date + "'");
		hql.append(" order by date_start desc");
		hql.append(" limit 1");
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		Double rs = (Double) query.uniqueResult();
		
		return rs;
	}

	public Double getKm(String orig, String dest) {
		StringBuffer hql = new StringBuffer();
		hql.append("select km from leg_km");
		hql.append(" where airport_from ='" + orig + "'");
		hql.append(" and airport_to = '" + dest + "'");
		hql.append(" limit 1");
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		Double rs = (Double) query.uniqueResult();
		if(rs == null){
			rs = 0.0;
		}
		return rs;
	}

	@SuppressWarnings("unchecked")
	public List<Awb> reportSalesCass(ReportFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		if(!StringUtils.equals(filter.getIsoc(), CargoUtils.TOUT)){
			crit.add(Restrictions.eq("isoc", filter.getIsoc().trim()));
		}
		
		if(!StringUtils.equals(filter.getAgtn(), CargoUtils.TOUT)){
			crit.add(Restrictions.like("agtn", filter.getAgtn(), MatchMode.ANYWHERE));
		}
		
		if(!StringUtils.equals(filter.getChannel(), CargoUtils.TOUT)){
			crit.add(Restrictions.eq("channel", filter.getChannel().trim()));
		}
		
		if(!StringUtils.equals(filter.getDateFrom().trim(), CargoUtils.TOUT) || !StringUtils.equals(filter.getDateTo().trim(), CargoUtils.TOUT)){
			SqlUtil.buildDatesBetweenLikeCrit("date_execute", filter.getDateFrom().trim(), filter.getDateTo().trim(), crit);			
		}
		crit.addOrder(Order.asc("lta"));
		List<Awb> result = crit.list();		
		return result;
	}

	public Integer getNbAwbByRdv(String rdv) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("idRdv", rdv.trim()));
		
		return ((Number) crit.setProjection(Projections.rowCount()).uniqueResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	public List<Awb> getAwbByRdvCode(String rdv) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("idRdv", rdv.trim().toUpperCase()));				
		crit.addOrder(Order.asc("lta"));
		List<Awb> result = crit.list();		
		return result;
	}

	public void deleteAwbTax(String lta) {
		StringBuffer hql = new StringBuffer();
		hql.append("delete from awb_tax");
		hql.append(" where lta ='" + lta + "'");
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		query.executeUpdate();
	}

	public void deleteAwbDetail(String lta) {
		StringBuffer hql = new StringBuffer();
		hql.append("delete from awb_detail");
		hql.append(" where lta ='" + lta + "'");
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		query.executeUpdate();
	}

}
