package com.datawings.app.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.ILoadLogDao;
import com.datawings.app.filter.FileFilter;
import com.datawings.app.model.LoadLog;

@Repository
public class LoadLogDao extends BaseDao<LoadLog, Integer> implements ILoadLogDao{

	@Override
	public Integer getLoadLogRowCount(FileFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		return ((Number) crit.setProjection(Projections.rowCount()).uniqueResult()).intValue();
	}

	@Override
	public List<LoadLog> getAllLoadLog(FileFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.setMaxResults(filter.getPageSize());
		crit.setFirstResult(filter.getOffset());
		return crit.list();
	}

}
