package com.datawings.app.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.ISysUserDao;
import com.datawings.app.filter.UserFilter;
import com.datawings.app.model.SysUser;


@Repository
public class SysUserDao extends BaseDao<SysUser, Integer> implements ISysUserDao{
	
	public SysUser findByUsername(String username) {
		Criteria criteria = this.getSession().createCriteria(this.getPersistentClass());
		criteria.add(Restrictions.eq("username", username));
		return (SysUser) criteria.uniqueResult();	
	}

	/**
	 * method get distinct users order by username 
	 * @param userFilter the UserFilter of SysUser to filter conditions
	 * @return list all user
	 * @see com.datawings.app.dao.ISysUserDao#getUser(com.datawings.app.filter.UserFilter)
	 */
	@SuppressWarnings("unchecked")
	public List<SysUser> getUsers(UserFilter userFilter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		
		if(StringUtils.isNotBlank(userFilter.getUsername())){
			crit.add(Restrictions.like("username", "%"+userFilter.getUsername().trim()+"%").ignoreCase());
		}
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		crit.addOrder(Order.asc("username"));
		return crit.list();
	}

	@Override
	public String getUserRole(Integer id) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("sysUserId", id));
		
		return null;
	}

	@Override
	public void deleteRoleUserByRoleId(SysUser user, Integer id) {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from sys_role_user ");
		sql.append(" where sys_role_id='" + id + "'");
		sql.append(" and sys_user_id='" + user.getSysUserId() + "'");
		
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		query.executeUpdate();
	}

	/**
	 * method get distinct user for role admin not include ROLE_DW
	 * @param userFilter the UserFilter of SysUser to filter conditions
	 * @return list Users content with conditions
	 * @see com.datawings.app.dao.ISysUserDao#getUsersForRoleAdmin(com.datawings.app.filter.UserFilter)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SysUser> getUsersForRoleAdmin(UserFilter userFilter) {
		StringBuffer sql = new StringBuffer();
		sql.append("select distinct on (u.username) * from sys_user u");
		sql.append(" where u.username not in");
		sql.append(" (select distinct u1.username from sys_user u1 left join sys_role_user ur on u1.sys_user_id = ur.sys_user_id left join sys_role r on ur.sys_role_id = r.sys_role_id where r.authority = 'ROLE_DW')");
		if(StringUtils.isNotBlank(userFilter.getUsername())){
			sql.append(" and upper(u.username) like '%" + userFilter.getUsername().trim().toUpperCase() + "%'");
		}
		sql.append(" ORDER BY u.username asc");
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		
		return query.addEntity(SysUser.class).list();
	}
}
