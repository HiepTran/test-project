package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.UserFilter;
import com.datawings.app.model.SysUser;

public interface ISysUserDao extends IBaseDao<SysUser, Integer>{
	
	SysUser findByUsername(String username);

	List<SysUser> getUsers(UserFilter userFilter);

	String getUserRole(Integer id);

	void deleteRoleUserByRoleId(SysUser user, Integer id);

	List<SysUser> getUsersForRoleAdmin(UserFilter userFilter);

}
