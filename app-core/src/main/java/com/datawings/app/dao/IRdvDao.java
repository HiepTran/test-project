package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.RdvFilter;
import com.datawings.app.model.Rdv;

public interface IRdvDao extends IBaseDao<Rdv, String>{

	Rdv getRdv(String agtn, String date);

	String getMaxRdvCode(String date);

	Integer getRdvRowsCount(RdvFilter filter);

	List<Rdv> getListRdv(RdvFilter filter);

	Rdv getRdv(RdvFilter filter);

}
