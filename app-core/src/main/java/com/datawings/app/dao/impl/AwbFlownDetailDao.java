package com.datawings.app.dao.impl;

import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IAwbFlownDetailDao;
import com.datawings.app.model.AwbFlownDetail;

@Repository
public class AwbFlownDetailDao extends BaseDao<AwbFlownDetail, Integer> implements IAwbFlownDetailDao{

}
