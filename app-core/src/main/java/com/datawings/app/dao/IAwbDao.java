package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.model.Awb;

public interface IAwbDao extends IBaseDao<Awb, String>{

	Integer getLTARowsCount(AwbFilter filter);

	List<Awb> getListAwb(AwbFilter filter);

	List<Awb> getListAwbManifest(AwbFilter filter);

	Double getRate(String cutp, String cutpAirline, String date);

	Double getKm(String orig, String dest);

	List<Awb> reportSalesCass(ReportFilter filter);

	Integer getNbAwbByRdv(String rdv);

	List<Awb> getAwbByRdvCode(String rdv);

	void deleteAwbTax(String lta);

	List<Awb> getExcelAwb(AwbFilter filter);

	void deleteAwbDetail(String lta);

}
