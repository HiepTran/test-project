package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.model.SysRole;
import com.datawings.app.model.SysUser;


public interface ISysRoleDao extends IBaseDao<SysRole, Integer>  {

	List<SysRole> getListRoleNotIncludeUser(Integer id);

	SysRole getRoleByName(String role);

	void deleteRoleUser(Integer roleId);

	void deleteByRoleName(String string);

	List<SysRole> getRoleOrderByRoleName();

	List<SysRole> findAllRoleUser(SysUser prinUser);

	List<SysRole> findRoleOfUserByUserID(Integer id);

	SysRole findRoleByName(String roleNameAdd);

}
