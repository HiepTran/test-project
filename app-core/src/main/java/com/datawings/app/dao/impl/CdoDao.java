package com.datawings.app.dao.impl;

import org.springframework.stereotype.Repository;

import com.datawings.app.dao.ICdoDao;
import com.datawings.app.model.Cdo;

@Repository
public class CdoDao extends BaseDao<Cdo, String> implements ICdoDao {

}
