package com.datawings.app.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IRdvDao;
import com.datawings.app.filter.RdvFilter;
import com.datawings.app.model.Rdv;

@Repository
public class RdvDao extends BaseDao<Rdv, String> implements IRdvDao{

	@SuppressWarnings("deprecation")
	public Rdv getRdv(String agtn, String date) {
		Criteria crit = getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("agtn", agtn.trim()));
		crit.add(Restrictions.sqlRestriction("to_char(emission, 'dd/mm/yyyy') = '" + date + "'"));	
		
		Rdv rdv = (Rdv) crit.uniqueResult();
		return rdv;
	}

	@SuppressWarnings("deprecation")
	public String getMaxRdvCode(String date) {
		StringBuffer hql = new StringBuffer();
		hql.append("select max(rdv) from rdv");
		hql.append(" where to_char(emission, 'dd/mm/yyyy') = '" + date + "'");
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		String rs = "";
		rs = (String) query.uniqueResult();
		return rs;
	}

	@SuppressWarnings("deprecation")
	public Integer getRdvRowsCount(RdvFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		
		if(StringUtils.isNotBlank(filter.getRdv())){
			crit.add(Restrictions.like("rdv", filter.getRdv().trim().toUpperCase(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getAgtn())){
			crit.add(Restrictions.like("agtn", filter.getAgtn().trim(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getEmission())){
			crit.add(Restrictions.sqlRestriction("to_char(emission, 'dd/mm/yyyy') = '" + filter.getEmission() + "'"));
		}
		return ((Number) crit.setProjection(Projections.rowCount()).uniqueResult()).intValue();
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Rdv> getListRdv(RdvFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		if(StringUtils.isNotBlank(filter.getRdv())){
			crit.add(Restrictions.like("rdv", filter.getRdv().trim().toUpperCase(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getAgtn())){
			crit.add(Restrictions.like("agtn", filter.getAgtn().trim(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getEmission())){
			crit.add(Restrictions.sqlRestriction("to_char(emission, 'dd/mm/yyyy') = '" + filter.getEmission() + "'"));
		}
		
		crit.addOrder(Order.desc("emission"));
		crit.addOrder(Order.asc("agtn"));
		crit.setMaxResults(filter.getPageSize());
		crit.setFirstResult(filter.getOffset());
		
		List<Rdv> result = crit.list();		
		return result;
	}

	@SuppressWarnings("deprecation")
	public Rdv getRdv(RdvFilter filter) {
		Criteria crit = getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("agtn", filter.getAgtn().trim()));
		crit.add(Restrictions.sqlRestriction("to_char(emission, 'dd/mm/yyyy') = '" + filter.getEmission() + "'"));	
		
		Rdv rdv = (Rdv) crit.uniqueResult();
		return rdv;
	}

}
