package com.datawings.app.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.datawings.app.common.DoubleUtil;
import com.datawings.app.dao.ILegKmDao;
import com.datawings.app.filter.LegKmFilter;
import com.datawings.app.model.LegKm;
import com.datawings.app.model.LegKmId;

@Repository
public class LegKmDao extends BaseDao<LegKm, LegKmId> implements ILegKmDao {

	@Override
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<LegKm> getLegKm(LegKmFilter legKmFilter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		if (StringUtils.isNotBlank(legKmFilter.getIsocFrom()))
			crit.add(Restrictions.like("id.isocFrom", "%" + legKmFilter.getIsocFrom().toUpperCase().trim() + "%"));
		if (StringUtils.isNotBlank(legKmFilter.getAirportFrom()))
			crit.add(
					Restrictions.like("id.airportFrom", "%" + legKmFilter.getAirportFrom().toUpperCase().trim() + "%"));
		if (StringUtils.isNotBlank(legKmFilter.getIsocTo()))
			crit.add(Restrictions.like("id.isocTo", "%" + legKmFilter.getIsocTo().toUpperCase().trim() + "%"));
		if (StringUtils.isNotBlank(legKmFilter.getAirportTo()))
			crit.add(Restrictions.like("id.airportTo", "%" + legKmFilter.getAirportTo().toUpperCase().trim() + "%"));
		crit.addOrder(Order.asc("id.isocFrom"));
		crit.addOrder(Order.asc("id.airportFrom"));
		crit.addOrder(Order.asc("id.isocTo"));
		crit.addOrder(Order.asc("id.airportTo"));
		crit.setMaxResults(legKmFilter.getPageSize());
		crit.setFirstResult(legKmFilter.getOffset());
		List<LegKm> result = crit.list();
		return result;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void updateLeg(LegKmFilter legKmFilter, LegKmId legkmID) {
		StringBuffer sql = new StringBuffer();
		sql.append("update leg_km set");
		// if(StringUtils.isNotBlank(legKmFilter.getLegKmEdit().getId().getIsocFrom()))
		// sql.append(" isoc_from = '" +
		// legKmFilter.getLegKmEdit().getId().getIsocFrom().toUpperCase() +
		// "',");
		//
		// if(StringUtils.isNotBlank(legKmFilter.getLegKmEdit().getId().getAirportFrom()))
		// sql.append(" airport_from =
		// '"+legKmFilter.getLegKmEdit().getId().getAirportFrom().toUpperCase()+"',");
		//
		// if(StringUtils.isNotBlank(legKmFilter.getLegKmEdit().getId().getIsocTo()))
		// sql.append(" isoc_to =
		// '"+legKmFilter.getLegKmEdit().getId().getIsocTo().toUpperCase()+"',");
		//
		// if(StringUtils.isNotBlank(legKmFilter.getLegKmEdit().getId().getAirportTo()))
		// sql.append(" airport_to =
		// '"+legKmFilter.getLegKmEdit().getId().getAirportTo().toUpperCase()+"',");

		if (legKmFilter.getLegKmEdit().getKm() > 0)
			sql.append(" km = '" + DoubleUtil.round(legKmFilter.getLegKmEdit().getKm(), 2) + "',");

		if (legKmFilter.getLegKmEdit().getMile() > 0)
			sql.append(" mile = '" + DoubleUtil.round(legKmFilter.getLegKmEdit().getMile(), 2) + "'");
		sql.append(" where isoc_from = '" + legkmID.getIsocFrom() + "'");
		sql.append(" and airport_from = '" + legkmID.getAirportFrom() + "'");
		sql.append(" and isoc_to = '" + legkmID.getIsocTo() + "'");
		sql.append(" and airport_to = '" + legkmID.getAirportTo() + "'");

		SQLQuery hql = getSession().createSQLQuery(sql.toString());
		hql.executeUpdate();
	}

	@SuppressWarnings("deprecation")
	@Override
	public Integer getLegKmRouCount(LegKmFilter legKmFilter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		if (StringUtils.isNotBlank(legKmFilter.getIsocFrom()))
			crit.add(Restrictions.like("id.isocFrom", "%" + legKmFilter.getIsocFrom().toUpperCase().trim() + "%"));
		if (StringUtils.isNotBlank(legKmFilter.getAirportFrom()))
			crit.add(
					Restrictions.like("id.airportFrom", "%" + legKmFilter.getAirportFrom().toUpperCase().trim() + "%"));
		if (StringUtils.isNotBlank(legKmFilter.getIsocTo()))
			crit.add(Restrictions.like("id.isocTo", "%" + legKmFilter.getIsocTo().toUpperCase().trim() + "%"));
		if (StringUtils.isNotBlank(legKmFilter.getAirportTo()))
			crit.add(Restrictions.like("id.airportTo", "%" + legKmFilter.getAirportTo().toUpperCase().trim() + "%"));
		
		return ((Number) crit.setProjection(Projections.rowCount()).uniqueResult()).intValue();
	}

}
