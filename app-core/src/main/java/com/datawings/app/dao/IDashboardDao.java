package com.datawings.app.dao;

import java.util.Date;
import java.util.List;

import com.datawings.app.filter.Dashboard;

public interface IDashboardDao {

	Date getCurrentDate();

	List<Dashboard> getListAwb(String year);

	List<Dashboard> getListTransport(String year);

}
