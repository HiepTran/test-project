package com.datawings.app.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IAwbFlownDao;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.filter.Rmk;
import com.datawings.app.model.AwbFlown;
import com.datawings.app.process.CargoUtils;

@Repository
public class AwbFlownDao extends BaseDao<AwbFlown, Integer> implements IAwbFlownDao{

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Rmk> reportRmk(ReportFilter filter, String beginYear, String endYear) {
		StringBuffer hql = new StringBuffer();
		hql.append("select month, orig, dest, sum(ht) as ht, sum(myc) as myc, sum(weight) as weight, sum(discount) as discount,");
		hql.append(" sum(htYear) as htYear, sum(mycYear) as mycYear, sum(weightYear) as weightYear, sum(discountYear) as discountYear");
		hql.append(" from(");
		hql.append(" select '" + filter.getDateFrom() +  "' as month, orig, dest, sum(total_pp) as ht, 0.0 as myc, sum(weight_gross) as weight , sum(discount) as discount,");
		hql.append(" 0.0 as htYear, 0.0 as mycYear, 0.0 as weightYear , 0.0 as discountYear");
		hql.append(" from awb_flown");
		hql.append(" where to_char(flight_date,'mm/yyyy')='" + filter.getDateFrom() + "'");
		hql.append(" group by orig, dest");
		hql.append(" union all");
		hql.append(" select '" + filter.getDateFrom() +  "' as month, orig, dest, 0.0 as ht, 0.0 as myc, 0.0 as weight , 0.0 as discount,");
		hql.append(" sum(total_pp) as htYear, 0.0 as mycYear,  sum(weight_gross) as weightYear , sum(discount) as discountYear");
		hql.append(" from awb_flown");
		hql.append(" where flight_date between '" + beginYear + "' and '" + endYear + "'");
		hql.append(" group by orig, dest, flight_date");

		hql.append(" ) as dd");
		hql.append(" group by orig, dest, month");
		hql.append(" order by orig");
		
		SQLQuery query =  getSession().createSQLQuery(hql.toString());
		List<Rmk> result = query
				.addScalar("month", StringType.INSTANCE)
				.addScalar("orig", StringType.INSTANCE)
				.addScalar("dest", StringType.INSTANCE)
				.addScalar("ht", DoubleType.INSTANCE)
				.addScalar("myc", DoubleType.INSTANCE)
				.addScalar("weight", DoubleType.INSTANCE)
				.addScalar("discount", DoubleType.INSTANCE)
				.addScalar("htYear", DoubleType.INSTANCE)
				.addScalar("mycYear", DoubleType.INSTANCE)
				.addScalar("weightYear", DoubleType.INSTANCE)
				.addScalar("discountYear", DoubleType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(Rmk.class))
				.list();
		return result;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<AwbFlown> reportCaTransport(ReportFilter filter) {
		StringBuffer hql = new StringBuffer();
		hql.append("select flight_date as flightDate, flight_number as flightNumber, orig, dest, immat, sum(weight_gross) as weightGross, sum (total_pp + total_cc) as totalPp");
		hql.append(" from awb_flown");
		hql.append(" where 1 = 1");
		if(!StringUtils.equals(filter.getDateFrom(), CargoUtils.TOUT)){
			hql.append(" and flight_date >= '" + filter.getDateFrom() + "'");
		}
		
		if(!StringUtils.equals(filter.getDateTo(), CargoUtils.TOUT)){
			hql.append(" and flight_date <= '" + filter.getDateTo() + "'");
		}
		
		if(!StringUtils.equals(filter.getFlightNumber(), CargoUtils.TOUT)){
			hql.append(" and flight_number = '" + filter.getFlightNumber() + "'");
		}
		
		if(!StringUtils.equals(filter.getImmat(), CargoUtils.TOUT)){
			hql.append(" and immat = '" + filter.getImmat() + "'");
		}
		
		if(!StringUtils.equals(filter.getOrig(), CargoUtils.TOUT)){
			hql.append(" and orig = '" + filter.getOrig() + "'");
		}
		
		if(!StringUtils.equals(filter.getDest(), CargoUtils.TOUT)){
			hql.append(" and dest = '" + filter.getDest() + "'");
		}
		
		hql.append(" group by flight_date, flight_number, orig, dest, immat");
		hql.append(" order by flight_date");
		
		SQLQuery query =  getSession().createSQLQuery(hql.toString());
		List<AwbFlown> result = query
				.addScalar("flightDate", DateType.INSTANCE)
				.addScalar("flightNumber", StringType.INSTANCE)
				.addScalar("orig", StringType.INSTANCE)
				.addScalar("dest", StringType.INSTANCE)
				.addScalar("immat", StringType.INSTANCE)
				.addScalar("weightGross", DoubleType.INSTANCE)
				.addScalar("totalPp", DoubleType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(AwbFlown.class))
				.list();
		return result;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<AwbFlown> getListAwbManifest(AwbFilter filter, String manifestNo) {
		StringBuffer hql = new StringBuffer();
		hql.append("select * from awb_flown");
		hql.append(" where status_flown !='F'");
		hql.append(" and (position('" + manifestNo + "' in content_manifest) = 0)"); 
		
		if(StringUtils.isNotBlank(filter.getOrig())){
			hql.append(" and orig ='" + filter.getOrig() + "'");
		}
		if(StringUtils.isNotBlank(filter.getDest())){
			hql.append(" and dest ='" + filter.getDest() + "'");
		}
		if(StringUtils.isNotBlank(filter.getLta())){
			hql.append(" and lta like '%" + filter.getLta() + "%'");
		}
		if(StringUtils.isNotBlank(filter.getDateExecute())){
			hql.append(" and to_char(date_execute,'dd/MM/yyyy') ='" + filter.getDateExecute() + "'");
		}
		hql.append(" order by lta");
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		
		List<AwbFlown> rs = query.addEntity(AwbFlown.class).list();
		return rs;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<AwbFlown> getListAwbFlown(String id) {
		StringBuffer hql = new StringBuffer();
		hql.append("select * from awb_flown");
		hql.append(" where manifest_no ='" + id + "'");
		hql.append(" order by lta");
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		
		List<AwbFlown> rs = query.addEntity(AwbFlown.class).list();
		return rs;
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	public List<AwbFlown> getByLta(String lta) {
		StringBuffer hql = new StringBuffer();
		hql.append("select * from awb_flown");
		hql.append(" where lta ='" + lta + "'");
		hql.append(" order by coupon");
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		
		List<AwbFlown> rs = query.addEntity(AwbFlown.class).list();
		return rs;
	}

}
