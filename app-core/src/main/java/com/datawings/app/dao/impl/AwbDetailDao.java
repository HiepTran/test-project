package com.datawings.app.dao.impl;

import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IAwbDetailDao;
import com.datawings.app.model.AwbDetail;
import com.datawings.app.model.AwbDetailId;

@Repository
public class AwbDetailDao extends BaseDao<AwbDetail, AwbDetailId> implements IAwbDetailDao{

}
