package com.datawings.app.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.ICritIsocDao;
import com.datawings.app.model.CritIsoc;

@Repository
public class CritIsocDao extends BaseDao<CritIsoc, String> implements ICritIsocDao{

	@SuppressWarnings("unchecked")
	public List<CritIsoc> getAll() {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.addOrder(Order.asc("isoc"));
		List<CritIsoc> result = crit.list();		
		return result;
		
	}

}
