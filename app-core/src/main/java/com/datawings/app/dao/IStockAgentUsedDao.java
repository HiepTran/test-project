package com.datawings.app.dao;

import com.datawings.app.model.StockAgentUsed;

public interface IStockAgentUsedDao extends IBaseDao<StockAgentUsed, Integer>{

}
