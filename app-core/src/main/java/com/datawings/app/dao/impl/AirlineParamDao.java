package com.datawings.app.dao.impl;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IAirlineParamDao;
import com.datawings.app.model.AirlineParam;

@Repository
public class AirlineParamDao extends BaseDao<AirlineParam, String> implements IAirlineParamDao{

	
	/**
	 * method fine one AirlineParam
	 * @return one Object AirlineParam
	 * @see com.datawings.app.dao.IAirlineParamDao#findOne()
	 */
	@Override
	public AirlineParam findOne() {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.setFirstResult(0);
		crit.setMaxResults(1);
		return (AirlineParam) crit.uniqueResult();
	}

}
