package com.datawings.app.dao.impl;

import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IStockAgentUsedDao;
import com.datawings.app.model.StockAgentUsed;

@Repository
public class StockAgentUsedDao extends BaseDao<StockAgentUsed, Integer> implements IStockAgentUsedDao{

}
