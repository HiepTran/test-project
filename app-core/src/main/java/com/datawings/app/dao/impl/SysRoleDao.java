package com.datawings.app.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.ISysRoleDao;
import com.datawings.app.model.SysRole;
import com.datawings.app.model.SysUser;
import com.datawings.app.process.CargoUtils;


@Repository
public class SysRoleDao extends BaseDao<SysRole, Integer> implements ISysRoleDao {


	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<SysRole> getListRoleNotIncludeUser(Integer id) {
		StringBuffer sql = new StringBuffer();
		sql.append("select * ");
		sql.append("from sys_role r where r.authority != 'ROLE_DW' ");
		sql.append("and not exists (select * from sys_user u, sys_role_user ur where u.sys_user_id ='"+id+"' ");
		sql.append("and r.sys_role_id = ur.sys_role_id and u.sys_user_id = ur.sys_user_id) order by r.sys_role_id");
		
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		List<SysRole> lst = query.addEntity(SysRole.class).list();
		return lst;
	}

	@SuppressWarnings("deprecation")
	@Override
	public SysRole getRoleByName(String role) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("authority", role));
		
		return (SysRole) crit.uniqueResult();
	}
	
	/**
	 * delete role by roleID
	 * @param roleId the ID role want to delete
	 * @see com.datawings.app.dao.ISysRoleDao#deleteRoleUser(java.lang.Integer)
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void deleteRoleUser(Integer roleId) {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from sys_role_user");
		sql.append(" where sys_role_id = '" + roleId + "'");
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		query.executeUpdate();
	}

	
	@Override
	public void deleteByRoleName(String string) {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from sys_role where authority = '" +string+"'");
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		query.executeUpdate();
	}
	
	/**
	 * @return list role not include _C, _U, _D 
	 * @see com.datawings.app.dao.ISysRoleDao#getRoleOrderByRoleName()
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<SysRole> getRoleOrderByRoleName() {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from sys_role");
		sql.append(" where right(authority, 2) not in ( '" + CargoUtils.ROLE_C + "', '" + CargoUtils.ROLE_U + "','" + CargoUtils.ROLE_D + "')");
		sql.append(" order by 2");
		SQLQuery query = this.getSession().createSQLQuery(sql.toString());
		return query.addEntity(SysRole.class).list();
	}

	/**
	 * method get all role user not include _C, _U, _D order by sys_role_id
	 * @param prinUser the SysUser login
	 * @return list role content with conditions if ROLE_DW, then show all
	 * else ROLE_ADMIN, then show roles expect ROLE_DW and ROLE_ADMIN
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SysRole> findAllRoleUser(SysUser prinUser) {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from sys_role");
		sql.append(" where right(authority, 2) not in ( '" + CargoUtils.ROLE_C + "', '" + CargoUtils.ROLE_U + "','" + CargoUtils.ROLE_D + "')");
		for(SysRole role : prinUser.getRoles()){
			if(StringUtils.equals(role.getAuthority(), CargoUtils.ROLE_DW)){
				break;
			}else if(StringUtils.equals(role.getAuthority(), CargoUtils.ROLE_ADMIN))
				sql.append(" and authority != '" + CargoUtils.ROLE_ADMIN + "'");
				sql.append(" and authority != '" + CargoUtils.ROLE_DW + "'");
		}
		sql.append(" order by 2");
		SQLQuery query = this.getSession().createSQLQuery(sql.toString());
		return query.addEntity(SysRole.class).list();
	}

	/**
	 * method get role of user by user ID order by sys_role_id
	 * @param id the UserID 
	 * @return list SysRole of User content with conditions user ID
	 * @see com.datawings.app.dao.ISysRoleDao#findRoleOfUserByUserID(java.lang.Integer)
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<SysRole> findRoleOfUserByUserID(Integer id) {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from sys_role r, sys_role_user ru, sys_user u");
		sql.append(" where r.sys_role_id = ru.sys_role_id and ru.sys_user_id = u.sys_user_id and u.sys_user_id = '" + id + "'");
		sql.append(" order by r.sys_role_id");
		SQLQuery query = this.getSession().createSQLQuery(sql.toString());
		
		return query.addEntity(SysRole.class).list();
	}

	@Override
	public SysRole findRoleByName(String roleNameAdd) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("authority", roleNameAdd));
		return (SysRole) crit.uniqueResult();
	}
}
