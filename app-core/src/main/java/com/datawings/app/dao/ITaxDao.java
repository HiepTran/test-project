package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.TaxFilter;
import com.datawings.app.model.Tax;

public interface ITaxDao extends IBaseDao<Tax, Integer>{

	Tax getTax(String isoc, String code, String subCode, String item, String date, String type);

	List<Tax> getListtax(TaxFilter filter);

	Tax getTaxValidator(TaxFilter filter);

}
