package com.datawings.app.dao.impl;

import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IAwbHeaderDao;
import com.datawings.app.model.AwbHeader;

@Repository
public class AwbHeaderDao extends BaseDao<AwbHeader, Integer> implements IAwbHeaderDao{

}
