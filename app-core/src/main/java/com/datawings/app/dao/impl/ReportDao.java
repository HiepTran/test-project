package com.datawings.app.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IReportDao;
import com.datawings.app.filter.ReportFilter;
import com.datawings.app.model.Report;

@Repository
public class ReportDao extends BaseDao<Report, Integer> implements IReportDao {

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Report> getReports(ReportFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		//crit.add(Restrictions.eq("type", filter.getType().trim()));
		crit.addOrder(Order.desc("createdDate"));
		List<Report> result = crit.list();		
		return result;
	}

}
