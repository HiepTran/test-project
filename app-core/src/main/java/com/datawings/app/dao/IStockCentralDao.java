package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.StockFilter;
import com.datawings.app.model.StockCentral;

public interface IStockCentralDao extends IBaseDao<StockCentral, Integer>{

	List<StockCentral> getStockCentral(StockFilter filter);

}
