package com.datawings.app.dao.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IDashboardDao;
import com.datawings.app.filter.Dashboard;

@Repository
public class DashboardDao implements IDashboardDao {

	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;

	public Session getSession() {
	    return sessionFactory.openSession();
	}

	public Date getCurrentDate() {
		StringBuffer hql = new StringBuffer();
		hql.append("select max(date_execute) from awb");

		SQLQuery query =  getSession().createSQLQuery(hql.toString());
		Date rs = (Date) query.uniqueResult();
		return rs;
	}

	@SuppressWarnings("unchecked")
	public List<Dashboard> getListAwb(String year) {
		StringBuffer hql = new StringBuffer();
		hql.append("select to_char(date_execute, 'mm/yyyy') as month, count(lta) as nrtd, round(cast(SUM(weight_gross) AS NUMERIC), 2) as weightGross,");
		hql.append(" round(cast(SUM(weight_charge) AS NUMERIC), 2) as weightCharge, round(cast(SUM(total_pp_lc + total_cc_lc) AS NUMERIC), 2) as amount");
		hql.append(" from awb");
		hql.append(" where to_char(date_execute, 'yyyy') ='" +  year + "'");
		hql.append(" group by month");
		hql.append(" order by month");
		
		SQLQuery query =  getSession().createSQLQuery(hql.toString());
		List<Dashboard> result = query
				.addScalar("month", StringType.INSTANCE)
				.addScalar("nrtd", IntegerType.INSTANCE)
				.addScalar("weightGross", DoubleType.INSTANCE)
				.addScalar("weightCharge", DoubleType.INSTANCE)
				.addScalar("amount", DoubleType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(Dashboard.class))
				.list();
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Dashboard> getListTransport(String year) {
		StringBuffer hql = new StringBuffer();
		hql.append("select to_char(date_execute, 'mm/yyyy') as month, count(lta) as nrtd, round(cast(SUM(weight_gross) AS NUMERIC), 2) as weightGross, ");
		hql.append(" round(cast(SUM(weight_charge) AS NUMERIC), 2) as weightCharge, round(cast(SUM(total_pp + total_cc) AS NUMERIC), 2) as amount");
		hql.append(" from awb_flown");
		hql.append(" where to_char(flight_date, 'yyyy') ='" +  year + "'");
		hql.append(" group by month");
		hql.append(" order by month");
		
		SQLQuery query =  getSession().createSQLQuery(hql.toString());
		List<Dashboard> result = query
				.addScalar("month", StringType.INSTANCE)
				.addScalar("nrtd", IntegerType.INSTANCE)
				.addScalar("weightGross", DoubleType.INSTANCE)
				.addScalar("weightCharge", DoubleType.INSTANCE)
				.addScalar("amount", DoubleType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(Dashboard.class))
				.list();
		return result;
	}

}
