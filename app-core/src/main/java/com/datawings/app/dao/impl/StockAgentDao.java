package com.datawings.app.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IStockAgentDao;
import com.datawings.app.filter.StockFilter;
import com.datawings.app.model.StockAgent;

@Repository
public class StockAgentDao extends BaseDao<StockAgent, Integer> implements IStockAgentDao {

	@SuppressWarnings("unchecked")
	public List<StockAgent> getStockAgent(StockFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("type", filter.getType().trim()));
		
		crit.addOrder(Order.asc("agtn"));
		crit.addOrder(Order.asc("serialFrom"));
		List<StockAgent> result = crit.list();		
		return result;
	}

}
