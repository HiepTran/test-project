package com.datawings.app.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.datawings.app.common.DateUtil;
import com.datawings.app.dao.IBsrDao;
import com.datawings.app.filter.BsrFilter;
import com.datawings.app.model.Bsr;
import com.datawings.app.model.BsrId;

@Repository
public class BsrDao extends BaseDao<Bsr, BsrId> implements IBsrDao {

	@Override
	public Integer getRowCountBsr(BsrFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		if(StringUtils.isNotBlank(filter.getCurrFrom())){
			crit.add(Restrictions.eq("bsrId.currFrom", filter.getCurrFrom().trim().toUpperCase()));
		}
		if(StringUtils.isNotBlank(filter.getCurrTo())){
			crit.add(Restrictions.eq("bsrId.currTo", filter.getCurrTo().trim().toUpperCase()));
		}
		if(StringUtils.isNotBlank(filter.getDateStart()) && DateUtil.checkDateAsString(filter.getDateStart(), "dd/MM/yyyy")){
			crit.add(Restrictions.ge("dateEnd", DateUtil.string2Date(filter.getDateStart().trim(), "dd/MM/yyyy")));
		}
		if(StringUtils.isNotBlank(filter.getDateEnd()) && DateUtil.checkDateAsString(filter.getDateEnd(), "dd/MM/yyyy")){
			crit.add(Restrictions.le("bsrId.dateStart", DateUtil.string2Date(filter.getDateEnd().trim(), "dd/MM/yyyy")));
		}
		return ((Number) crit.setProjection(Projections.rowCount()).uniqueResult()).intValue();
	}

	@Override
	public List<Bsr> getBsrs(BsrFilter filter) {
		List<Bsr> rt = new ArrayList<Bsr>();
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		if(StringUtils.isNotBlank(filter.getCurrFrom())){
			crit.add(Restrictions.eq("bsrId.currFrom", filter.getCurrFrom().trim().toUpperCase()));
		}
		if(StringUtils.isNotBlank(filter.getCurrTo())){
			crit.add(Restrictions.eq("bsrId.currTo", filter.getCurrTo().trim().toUpperCase()));
		}
		if(StringUtils.isNotBlank(filter.getDateStart()) && DateUtil.checkDateAsString(filter.getDateStart(), "dd/MM/yyyy")){
			crit.add(Restrictions.ge("dateEnd", DateUtil.string2Date(filter.getDateStart().trim(), "dd/MM/yyyy")));
		}
		if(StringUtils.isNotBlank(filter.getDateEnd()) && DateUtil.checkDateAsString(filter.getDateEnd(), "dd/MM/yyyy")){
			crit.add(Restrictions.le("bsrId.dateStart", DateUtil.string2Date(filter.getDateEnd().trim(), "dd/MM/yyyy")));
		}
		
		//set sort
		if(StringUtils.equals(filter.getCol(), "COL_CURR_FROM")){
			if(filter.getSorting() == true){
				crit.addOrder(Order.asc("bsrId.currFrom"));
			}
			else{
				crit.addOrder(Order.desc("bsrId.currFrom"));
			}
		}
		if(StringUtils.equals(filter.getCol(), "COL_DAY_START")){
			if(filter.getSorting() == true){
				crit.addOrder(Order.asc("bsrId.dateStart"));
			}
			else{
				crit.addOrder(Order.desc("bsrId.dateStart"));
			}
		}
		
		if(StringUtils.equals(filter.getCol(), "DEFAULT")){
			crit.addOrder(Order.asc("bsrId.currFrom"));
			crit.addOrder(Order.desc("bsrId.dateStart"));
		}
		
		crit.setMaxResults(filter.getPageSize());
		crit.setFirstResult(filter.getOffset());
		rt = crit.list();
		return rt;
	}

	/**
	 * method get Bsr to do validator
	 * @param filter the BsrFilter
	 * @return list Bsr content with conditions
	 * @see com.datawings.app.dao.IBsrDao#getBsrFilter(com.datawings.app.filter.BsrFilter)
	 */
	@Override
	public List<Bsr> getBsrFilter(BsrFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("bsrId.currFrom", filter.getCurrFromAdd().trim().toUpperCase()));
		crit.add(Restrictions.eq("bsrId.currTo", filter.getCurrToAdd().trim().toUpperCase()));
		if(StringUtils.isNotBlank(filter.getDateStartAdd()) && DateUtil.checkDateAsString(filter.getDateStartAdd(), "dd/MM/yyyy")){
			crit.add(Restrictions.ge("dateEnd", DateUtil.string2Date(filter.getDateStartAdd(), "dd/MM/yyyy")));
		}
		crit.addOrder(Order.asc("bsrId.dateStart"));
		crit.addOrder(Order.asc("dateEnd"));
		return crit.list();
	}

	@Override
	public List<Bsr> getBsrFilterByFromTo(BsrFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("bsrId.currFrom", filter.getCurrFromAdd().trim().toUpperCase()));
		crit.add(Restrictions.eq("bsrId.currTo", filter.getCurrToAdd().trim().toUpperCase()));
		crit.addOrder(Order.asc("bsrId.dateStart"));
		crit.addOrder(Order.asc("dateEnd"));
		return crit.list();
	}

	@Override
	public Bsr findBsrByCutpAndDateExt(String cutp, String currency, String dateExecute) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("bsrId.currFrom", cutp));
		crit.add(Restrictions.eq("bsrId.currTo", currency));
		crit.add(Restrictions.le("bsrId.dateStart", DateUtil.string2Date(dateExecute, "dd/MM/yyyy")));
		crit.add(Restrictions.ge("dateEnd", DateUtil.string2Date(dateExecute, "dd/MM/yyyy")));
		crit.addOrder(Order.asc("bsrId.dateStart"));
		crit.addOrder(Order.asc("dateEnd"));
		return (Bsr) crit.uniqueResult();
	}

}
