package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.LineFilter;
import com.datawings.app.model.Line;

public interface ILineDao extends IBaseDao<Line, String>{

	Line getLine(String orig, String dest);

	List<Line> getListLine(LineFilter filter);

}
