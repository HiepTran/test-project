package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.ReportFilter;
import com.datawings.app.model.Report;

public interface IReportDao extends IBaseDao<Report, Integer>{

	List<Report> getReports(ReportFilter filter);

}
