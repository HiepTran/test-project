package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.FileFilter;
import com.datawings.app.model.LoadLog;

public interface ILoadLogDao extends IBaseDao<LoadLog, Integer>{

	Integer getLoadLogRowCount(FileFilter filter);

	List<LoadLog> getAllLoadLog(FileFilter filter);

}
