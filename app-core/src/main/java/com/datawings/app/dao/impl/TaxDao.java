package com.datawings.app.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.ITaxDao;
import com.datawings.app.filter.TaxFilter;
import com.datawings.app.model.Tax;

@Repository
public class TaxDao extends BaseDao<Tax, Integer> implements ITaxDao{

	public Tax getTax(String isoc, String code, String subCode, String item, String date, String type) {
		StringBuffer hql = new StringBuffer();
		hql.append("select * from tax");
		hql.append(" where isoc ='" +  isoc + "'");
		hql.append(" and code ='" + code + "'");
		hql.append(" and sub_code ='" + subCode + "'");
		hql.append(" and position('" + item+ "' in item)>0");
		hql.append(" and date_start <= '" + date + "'");
		hql.append(" and date_end >= '" + date + "'");
		hql.append(" and type = '" + type + "'");
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		Tax rs = (Tax) query.addEntity(Tax.class).uniqueResult();
		
		return rs;		
	}
	
	public Tax getTaxValidator(TaxFilter filter) {
		StringBuffer hql = new StringBuffer();
		hql.append("select * from tax");
		hql.append(" where isoc ='" +  filter.getIsocExt() + "'");
		hql.append(" and code ='" + filter.getCodeExt().toUpperCase() + "'");
		hql.append(" and sub_code ='" + filter.getSubCodeExt().toUpperCase() + "'");
		hql.append(" and type = 'GEN'");
		hql.append(" limit 1");
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		Tax rs = (Tax) query.addEntity(Tax.class).uniqueResult();
		
		return rs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tax> getListtax(TaxFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("type", filter.getType().trim()));
		
		if(StringUtils.isNotBlank(filter.getIsoc())){
			crit.add(Restrictions.eq("isoc", filter.getIsoc().trim().toUpperCase()));
		}
		
		if(StringUtils.isNotBlank(filter.getCode())){
			crit.add(Restrictions.eq("code", filter.getCode().trim().toUpperCase()));
		}
		
		if(StringUtils.isNotBlank(filter.getSubCode())){
			crit.add(Restrictions.eq("subCode", filter.getSubCode().trim().toUpperCase()));
		}
		
		if(StringUtils.isNotBlank(filter.getItem())){
			crit.add(Restrictions.like("item", filter.getItem().trim().toUpperCase(), MatchMode.ANYWHERE));
		}
		
		crit.addOrder(Order.asc("code"));
		crit.addOrder(Order.desc("dateEnd"));
		List<Tax> result = crit.list();		
		return result;
	}

	

}
