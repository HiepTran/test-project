package com.datawings.app.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IStockCentralDao;
import com.datawings.app.filter.StockFilter;
import com.datawings.app.model.StockCentral;

@Repository
public class StockCentralDao extends BaseDao<StockCentral, Integer> implements IStockCentralDao{
	
	@SuppressWarnings("unchecked")
	public List<StockCentral> getStockCentral(StockFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("type", filter.getType().trim()));
		
		crit.addOrder(Order.asc("serialFrom"));
		List<StockCentral> result = crit.list();		
		return result;
	}
}
