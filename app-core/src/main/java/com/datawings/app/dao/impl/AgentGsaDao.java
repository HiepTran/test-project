package com.datawings.app.dao.impl;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IAgentGsaDao;
import com.datawings.app.filter.AgentGsaFilter;
import com.datawings.app.model.AgentGsa;

@Repository
public class AgentGsaDao  extends BaseDao<AgentGsa, Integer> implements IAgentGsaDao{

	public AgentGsa getAgentGsa(String agtnCode, Date date) {
		StringBuffer hql = new StringBuffer();
		hql.append("select * from agent_gsa");
		hql.append(" where agtn ='" + agtnCode + "'");
		hql.append(" and date_start <= '" + date + "'");
		hql.append(" and date_end >= '" + date + "'");
		hql.append(" order by date_start desc");
		hql.append(" limit 1");
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		AgentGsa rs = (AgentGsa) query.addEntity(AgentGsa.class).uniqueResult();
		
		return rs;
	}

	/**
	 * method get row cout of agent gsa
	 * @param filter the AgentGsaFilter
	 * @return row count of agent gsa
	 * @see com.datawings.app.dao.IAgentGsaDao#getAgentGsaRowCount(com.datawings.app.filter.AgentGsaFilter)
	 */
	@Override
	public Integer getAgentGsaRowCount(AgentGsaFilter filter) {
		StringBuffer sql = new StringBuffer();
		sql.append("select count(distinct ag.code) from agent_gsa ag where id != '0'");
		if(StringUtils.isNotBlank(filter.getCodeFilter())){
			sql.append(" and ag.code like '%" + filter.getCodeFilter() + "%'");
		}
		if(StringUtils.isNotBlank(filter.getNameFilter())){
			sql.append(" and ag.gsa_name like '%" + filter.getNameFilter() + "%'");
		}
		SQLQuery query = this.getSession().createSQLQuery(sql.toString());
		
		BigInteger count = (BigInteger) query.uniqueResult();
		return count.intValue();
	}

	/**
	 * method get list agent gsa by conditions filter
	 * @param filter the AgentGsaFilter
	 * @return list agent gsa content with conditions
	 * @see com.datawings.app.dao.IAgentGsaDao#getListAgentGsa(com.datawings.app.filter.AgentGsaFilter)
	 */
	@Override
	public List<AgentGsa> getListAgentGsa(AgentGsaFilter filter) {
		StringBuffer sql = new StringBuffer();
		sql.append("select distinct on (code) * from agent_gsa where id != '0'");
		if(StringUtils.isNotBlank(filter.getCodeFilter())){
			sql.append(" and code like '%" + filter.getCodeFilter() + "%'");
		}
		if(StringUtils.isNotBlank(filter.getNameFilter())){
			sql.append(" and gsa_name like '%" + filter.getNameFilter() + "%'");
		}
		
		sql.append(" order by code");
		SQLQuery query = this.getSession().createSQLQuery(sql.toString());
		return query.addEntity(AgentGsa.class).list();
	}

	/**
	 * method get detail agent gsa by code
	 * @param code the Code agent gsa
	 * @return List agent gsa content with conditions
	 * @see com.datawings.app.dao.IAgentGsaDao#getDetailAgentGsaByCode(java.lang.Integer)
	 */
	@Override
	public List<AgentGsa> getDetailAgentGsaByCode(String code) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.eq("code", code));
		crit.add(Restrictions.not(Restrictions.eq("agtn", "")));
		return crit.list();
	}

	@Override
	public AgentGsa getAgentGsaByCode(String code) {
		StringBuffer sql = new StringBuffer();
		sql.append("select * from agent_gsa");
		sql.append(" where code = '" + code + "'");
		sql.append(" limit 1");
		SQLQuery query = this.getSession().createSQLQuery(sql.toString());
		return (AgentGsa) query.addEntity(AgentGsa.class).uniqueResult();
	}

	/**
	 * method get count agtn by code
	 * @param code the Code of AgentGsa
	 * @return count agtn by code
	 * @see com.datawings.app.dao.IAgentGsaDao#getCountAgtn(java.lang.String)
	 */
	@Override
	public String getCountAgtn(String code) {
		StringBuffer sql = new StringBuffer();
		sql.append("select count (agtn) from agent_gsa");
		sql.append(" where code = '" +code+ "'");
		sql.append(" and agtn != ''");
		SQLQuery query = this.getSession().createSQLQuery(sql.toString());
		BigInteger count =  (BigInteger) query.uniqueResult();
		return count.toString();
	}
	
	/**
	 * method delete AgentGsa by code
	 * @param id the code of AgentGsa
	 */
	@Override
	public void deleteAgentGsaByCode(String id) {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from agent_gsa where code = '" + id + "'");
		SQLQuery query = this.getSession().createSQLQuery(sql.toString());
		query.executeUpdate();
	}
	
	/**
	 * method delete Agtn by id
	 * @param id the agtn code of AgentGsa
	 * @param code the code of AgentGsa
	 */
	@Override
	public void deleteAgtnByAgtnCode(String id, String code) {
		StringBuffer sql = new StringBuffer();
		sql.append("delete from agent_gsa where code = '" + code + "'");
		sql.append(" and agtn = '" + id + "'");
		SQLQuery query = this.getSession().createSQLQuery(sql.toString());
		query.executeUpdate();
	}

}
