package com.datawings.app.dao;

import com.datawings.app.model.AwbFlownTax;

public interface IAwbFlownTaxDao extends IBaseDao<AwbFlownTax, Integer>{

	void deleteId(Integer id);

}
