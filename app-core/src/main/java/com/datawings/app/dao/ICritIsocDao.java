package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.model.CritIsoc;

public interface ICritIsocDao extends IBaseDao<CritIsoc, String>{

	List<CritIsoc> getAll();

}
