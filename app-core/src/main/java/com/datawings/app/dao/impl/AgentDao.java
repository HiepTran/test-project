package com.datawings.app.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.datawings.app.dao.IAgentDao;
import com.datawings.app.filter.AgentFilter;
import com.datawings.app.model.Agent;

@Repository
public class AgentDao extends BaseDao<Agent, String> implements IAgentDao {

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Agent> getAgent(AgentFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		if(StringUtils.isNotBlank(filter.getCodeExt())){
			crit.add(Restrictions.like("code", filter.getCodeExt().trim().toUpperCase(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getKindExt())){
			crit.add(Restrictions.eq("kind", filter.getKindExt().trim()));
		}
		if(StringUtils.isNotBlank(filter.getIataIndExt())){
			crit.add(Restrictions.eq("iataInd", filter.getIataIndExt().trim()));
		}
		if(StringUtils.isNotBlank(filter.getIsocExt())){
			crit.add(Restrictions.eq("isoc", filter.getIsocExt().trim()));
		}
		crit.addOrder(Order.asc("code"));
		crit.setMaxResults(filter.getPageSize());
		crit.setFirstResult(filter.getOffset());
		List<Agent> result = crit.list();		
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<String> getIsoc() {
		StringBuffer hql = new StringBuffer();
		hql.append("select distinct trim(isoc) from agent");
		hql.append(" order by trim(isoc)");
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		List<String> rs = query.list();
		return rs;
	}

	@Override
	public Integer getAgentRowCount(AgentFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		if(StringUtils.isNotBlank(filter.getCodeExt())){
			crit.add(Restrictions.like("code", filter.getCodeExt().trim().toUpperCase(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getKindExt())){
			crit.add(Restrictions.eq("kind", filter.getKindExt().trim()));
		}
		if(StringUtils.isNotBlank(filter.getIataIndExt())){
			crit.add(Restrictions.eq("iataInd", filter.getIataIndExt().trim()));
		}
		if(StringUtils.isNotBlank(filter.getIsocExt())){
			crit.add(Restrictions.eq("isoc", filter.getIsocExt().trim()));
		}
		return ((Number) crit.setProjection(Projections.rowCount()).uniqueResult()).intValue();
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<Agent> getAgentEX(AgentFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		if(StringUtils.isNotBlank(filter.getCodeExt())){
			crit.add(Restrictions.like("code", filter.getCodeExt().trim().toUpperCase(), MatchMode.ANYWHERE));
		}
		if(StringUtils.isNotBlank(filter.getKindExt())){
			crit.add(Restrictions.eq("kind", filter.getKindExt().trim()));
		}
		if(StringUtils.isNotBlank(filter.getIataIndExt())){
			crit.add(Restrictions.eq("iataInd", filter.getIataIndExt().trim()));
		}
		if(StringUtils.isNotBlank(filter.getIsocExt())){
			crit.add(Restrictions.eq("isoc", filter.getIsocExt().trim()));
		}
		crit.addOrder(Order.asc("code"));
		List<Agent> result = crit.list();		
		return result;
	}

	@Override
	public List<String> findListAgentCode(String agtnAddDetail) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		crit.add(Restrictions.like("code", "%" + agtnAddDetail + "%"));
		crit.setProjection(Projections.property("code"));
		crit.setMaxResults(10);
		List<String> rs = crit.list();
		return rs;
	}

}
