package com.datawings.app.dao;

import com.datawings.app.model.AwbDetail;
import com.datawings.app.model.AwbDetailId;

public interface IAwbDetailDao extends IBaseDao<AwbDetail, AwbDetailId>{

}
