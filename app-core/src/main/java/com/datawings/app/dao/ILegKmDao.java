package com.datawings.app.dao;

import java.util.List;

import com.datawings.app.filter.LegKmFilter;
import com.datawings.app.model.LegKm;
import com.datawings.app.model.LegKmId;

public interface ILegKmDao extends IBaseDao<LegKm, LegKmId> {
	List<LegKm> getLegKm(LegKmFilter legKmFilter);
	void updateLeg(LegKmFilter legKmFilter, LegKmId legkmID);
	Integer getLegKmRouCount(LegKmFilter legKmFilter);
}
