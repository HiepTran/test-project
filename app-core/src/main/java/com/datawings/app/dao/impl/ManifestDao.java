package com.datawings.app.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.SqlUtil;
import com.datawings.app.dao.IManifestDao;
import com.datawings.app.filter.ManifestBean;
import com.datawings.app.filter.ManifestFilter;
import com.datawings.app.model.Manifest;

@Repository
public class ManifestDao extends BaseDao<Manifest, String> implements IManifestDao{

	@SuppressWarnings({ "unchecked", "deprecation"})
	public List<ManifestBean> getManifest(ManifestFilter filter) {
		StringBuffer hql = new StringBuffer();
		hql.append("select manifest_no as manifestNo, tacn, orig, dest, flight_number as flightNumber, flight_date as flightDate, immat, entry_number as entryNumber");
		hql.append(" ,(select count(*) from awb_flown where manifest_no = manifest.manifest_no) as awbFlownCount");
		hql.append(" from manifest where manifest_no != '0'");
		if(StringUtils.isNotBlank(filter.getTacn())){
			hql.append(" and tacn like '%" + filter.getTacn() + "%'");
		}
		if(StringUtils.isNotBlank(filter.getManifestNo())){
			hql.append(" and manifest_no like '%" + filter.getManifestNo() + "%'");
		}
		if(StringUtils.isNotBlank(filter.getOrig())){
			hql.append(" and orig like '%" + filter.getOrig().trim().toUpperCase() + "%'");
		}
		if(StringUtils.isNotBlank(filter.getDest())){
			hql.append(" and dest like '%" + filter.getDest().trim().toUpperCase() + "%'");
		}
		if(StringUtils.isNotBlank(filter.getDateFrom().trim()) && StringUtils.isNotBlank(filter.getDateTo().trim()) ){
			if(DateUtil.checkDateAsString(filter.getDateFrom().trim(), "dd/MM/yyyy") 
					&& DateUtil.checkDateAsString(filter.getDateTo().trim(), "dd/MM/yyyy")){
					hql.append(" and flight_date between '" + filter.getDateFrom().trim() + "' and '" + filter.getDateTo().trim() + "'");
			}
		}
		else if(StringUtils.isNotBlank(filter.getDateFrom().trim())){
			if(DateUtil.checkDateAsString(filter.getDateFrom().trim(), "dd/MM/yyyy")){
				hql.append(" and flight_date > '" + filter.getDateFrom().trim() + "'");
			}
		}
		else if(StringUtils.isNotBlank(filter.getDateTo().trim())){
			if(DateUtil.checkDateAsString(filter.getDateFrom().trim(), "dd/MM/yyyy")){
				hql.append(" and flight_date < '" + filter.getDateTo().trim() + "'");
			}
		}
		hql.append(" order by manifest_no desc");
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		
		List<ManifestBean> result = query
			.addScalar("manifestNo", new StringType())
			.addScalar("tacn", new StringType())
			.addScalar("orig", new StringType())
			.addScalar("dest", new StringType())
			.addScalar("flightNumber", new StringType())
			.addScalar("flightDate", new DateType())
			.addScalar("immat", new StringType())
			.addScalar("entryNumber", new IntegerType())
			.addScalar("awbFlownCount", new IntegerType())
			.setResultTransformer(Transformers.aliasToBean(ManifestBean.class))
			.list();

		return result;
	}

	@SuppressWarnings("deprecation")
	public Integer getRowsCount(ManifestFilter filter) {
		Criteria crit = this.getSession().createCriteria(this.getPersistentClass());
		if(StringUtils.isNotBlank(filter.getTacn())){
			crit.add(Restrictions.like("tacn", "%" + filter.getTacn().trim() + "%"));
		}
		if(StringUtils.isNotBlank(filter.getManifestNo())){
			crit.add(Restrictions.like("manifestNo", "%" + filter.getManifestNo().trim().toUpperCase() + "%"));
		}
		if(StringUtils.isNotBlank(filter.getOrig())){
			crit.add(Restrictions.like("orig", "%" + filter.getOrig().trim().toUpperCase() + "%"));
		}
		if(StringUtils.isNotBlank(filter.getDest())){
			crit.add(Restrictions.like("dest", "%" + filter.getDest().trim().toUpperCase() + "%"));
		}
		if(StringUtils.isNotBlank(filter.getDateFrom().trim()) && StringUtils.isNotBlank(filter.getDateTo().trim()) ){
			if(DateUtil.checkDateAsString(filter.getDateFrom().trim(), "dd/MM/yyyy") && DateUtil.checkDateAsString(filter.getDateTo().trim(), "dd/MM/yyyy")){
				SqlUtil.buildDatesBetweenLikeCrit("flight_date", filter.getDateFrom().trim(), filter.getDateTo().trim(), crit);	
			}
		}
		else if(StringUtils.isNotBlank(filter.getDateFrom().trim())){
			if(DateUtil.checkDateAsString(filter.getDateFrom().trim(), "dd/MM/yyyy")){
				crit.add(Restrictions.sqlRestriction("flight_date > '" + filter.getDateFrom().trim() + "'"));
			}
		}
		else if(StringUtils.isNotBlank(filter.getDateTo().trim())){
			if(DateUtil.checkDateAsString(filter.getDateTo().trim(), "dd/MM/yyyy")){
				crit.add(Restrictions.sqlRestriction("flight_date < '" + filter.getDateTo().trim() +"'"));
			}
		}
		return ((Number) crit.setProjection(Projections.rowCount()).uniqueResult()).intValue();
	}

	@SuppressWarnings("deprecation")
	public ManifestBean findReturnBean(String id) {
		StringBuffer hql = new StringBuffer();
		hql.append("select manifest_no as manifestNo, tacn, orig, dest, flight_number as flightNumber, flight_date as flightDate, immat, entry_number as entryNumber");
		hql.append(" ,(select count(*) from awb_flown where manifest_no = manifest.manifest_no) as awbFlownCount");
		hql.append(" from manifest");
		hql.append(" where manifest_no ='" + id + "'");
		
		SQLQuery query = getSession().createSQLQuery(hql.toString());
		
		ManifestBean result = (ManifestBean) query
			.addScalar("manifestNo", new StringType())
			.addScalar("tacn", new StringType())
			.addScalar("orig", new StringType())
			.addScalar("dest", new StringType())
			.addScalar("flightNumber", new StringType())
			.addScalar("flightDate", new DateType())
			.addScalar("immat", new StringType())
			.addScalar("entryNumber", new IntegerType())
			.addScalar("awbFlownCount", new IntegerType())
			.setResultTransformer(Transformers.aliasToBean(ManifestBean.class))
			.uniqueResult();

		return result;
	}

}
