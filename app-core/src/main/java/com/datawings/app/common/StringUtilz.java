package com.datawings.app.common;

import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class StringUtilz {

	/**
	 * Class util des traitements des chaines
	 */

	/** Index du 1er caractere accentu� **/
	private static final int MIN = 192;
	/** Index du dernier caractere accentu� **/
	private static final int MAX = 255;
	/** Vecteur de correspondance entre accent / sans accent **/
	private static final List<String> map = initMap();

	/**
	 * Cette m�thode remplace toutes les occurences de �, �, �, �, �, �, �, �...
	 * par leurs �quivalents respectifs sans accents. Exemple :
	 * StringUtilz.traiterAccents("�, �, �, �, �, �, �, �... ") = e, e, e, i, i,
	 * o, o, a..
	 * 
	 * @param chaineATraiter
	 *            Cha�ne de caract�res � traiter.
	 * @return Cha�ne trait�e.
	 */
	public static String traiterAccents(String chaine) {
		StringBuffer result = new StringBuffer(chaine);

		for (int bcl = 0; bcl < result.length(); bcl++) {
			int carVal = chaine.charAt(bcl);
			if (carVal >= MIN && carVal <= MAX) { // Remplacement
				String newVal = map.get(carVal - MIN);
				result.replace(bcl, bcl + 1, newVal);
			}
		}

		return result.toString();
	}

	/**
	 * M�thode de normalisation d'une cha�ne de caract�res.<BR>
	 * Au cour de la normalisation, on enl�ve les accents, on passe tous les
	 * caract�res en majuscules et on supprime tous les caract�res qui ne sont
	 * pas alphanum�riques.
	 * 
	 * @param chaine
	 *            Cha�ne de caract�res � traiter.
	 * @return La cha�ne de caract�re trait�e.
	 */
	public static String normaliserString(String chaine) {
		String chaineSansAccent = traiterAccents(chaine);
		String chaineTraitee = chaineSansAccent.toUpperCase();
		chaineTraitee = chaineTraitee.replaceAll("[^a-zA-Z0-9]", "");
		chaineTraitee = chaineTraitee.replaceAll("^0*", "");
		return chaineTraitee;
	}

	/**
	 * @param chaineA
	 *            chaine A � comparer.
	 * @param chaineB
	 *            chaine B � comparer.
	 * @return true si les 2 chaines trimm�es sont �gales (case sensitive).
	 */
	public static boolean equalTrimmed(String chaineA, String chaineB) {
		return (chaineA.trim().equals(chaineB.trim())) ? true : false;
	}

	/**
	 * @param chaineA
	 *            chaine A � comparer.
	 * @param chaineB
	 *            chaine B � comparer.
	 * @return true si les 2 chaines trimm�es sont �gales (case non sensitive).
	 */
	public static boolean equalTrimmedIgnoreCase(String chaineA, String chaineB) {
		return (chaineA.trim().equalsIgnoreCase(chaineB.trim())) ? true : false;
	}

	/**
	 * Cette m�thode permet de remplacer toutes les occurences d'un tag dans un
	 * text donn� par une valeur.
	 * 
	 * @param text
	 *            Texte � modifier.
	 * @param tagName
	 *            Nom du tag � remplacer.
	 * @param value
	 *            valeur avec laquelle remplacer le tag.
	 */
	public static void replaceTagInText(StringBuffer text, String tagName,
			String value) {
		if ((text == null) || (tagName == null)) {
			return;
		}

		if (value == null) {
			value = " ";
		}

		if (value.length() == 0) {
			value = " ";
		}

		if ((text.length() > 0) && (tagName.length() > 0)
				&& (value.length() > 0)) {
			while (text.indexOf(tagName) > 0) {
				text.replace(text.indexOf(tagName), text.indexOf(tagName)
						+ tagName.length(), value);
			}
		}
	}

	// validate email valid???
	public static boolean isValidEmail(String email) {
		if (email.indexOf(";") != -1)
			return false;

		if (email.indexOf("@") == -1)
			return false;
		if (email.indexOf("/") != -1)
			return false;
		if (email.indexOf("\\") != -1)
			return false;
		if (email.indexOf("^") != -1)
			return false;
		if (email.indexOf(",") != -1)
			return false;
		if (email.indexOf("!") != -1)
			return false;
		if (email.indexOf("#") != -1)
			return false;
		if (email.indexOf("$") != -1)
			return false;
		if (email.indexOf("%") != -1)
			return false;
		if (email.indexOf("*") != -1)
			return false;
		if (email.indexOf("(") != -1)
			return false;
		if (email.indexOf(")") != -1)
			return false;
		if (email.indexOf(" ") != -1)
			return false;

		int lastDot = email.lastIndexOf(".");
		int at = email.indexOf("@");
		int dom = lastDot - at;
		int len = email.length();
		if ((len < 5) || (at == -1) || (lastDot == -1) || (at < 1) || (dom < 2)
				|| (lastDot == len - 1) || (at == len - 1))
			return false;
		return true;

	}

	/**
	 * @param input
	 *            cha�ne d'entr�e � encoder
	 * @param algorithme
	 *            Algorithme d'encodage � appliquer
	 * @return une correspondant � la cha�ne d'entr�e encod�e selon l'algorithme
	 *         indiquer
	 * @throws NoSuchAlgorithmException
	 */
	public static String encodeString(String input, String algorithme)
			throws NoSuchAlgorithmException {
		byte[] hash = MessageDigest.getInstance("MD5").digest(input.getBytes());
		StringBuffer hashString = new StringBuffer();

		for (int i = 0; i < hash.length; ++i) {
			String hex = Integer.toHexString(hash[i]);
			if (hex.length() == 1) {
				hashString.append('0');
				hashString.append(hex.charAt(hex.length() - 1));
			}

			else {
				hashString.append(hex.substring(hex.length() - 2));
			}
		}

		return hashString.toString();

	}

	/**
	 * @param value
	 *            Cha�ne de caract�res � tester.
	 * @return true si la cha�ne test�e peut �tre convertie en un nombre entier
	 *         positif ou n�gatif. Sinon, la m�thode retourne false.
	 */
	public static boolean isSignedNumeric(String value) {
		String valueToTest = null;
		if (value == null || value.equals(""))
			return false;
		if (value.charAt(0) == '-') {
			valueToTest = value.substring(1);
		} else {
			valueToTest = value;
		}
		return StringUtils.isNumeric(valueToTest);
	}

	public static boolean isNumbericString(String str) {
		// /Check input string is only numberic string
		String regex1 = "\\d+\\.\\d+";
		String regex2 = "\\d+";
		if ((str.matches(regex1) || str.matches(regex2))) {
			return true;
		}
		return false;
	}

	/**
	 * @param value
	 *            Cha�ne de caract�res � tester.
	 * @param nbDecimal
	 *            Nombre de chiffres apr�s la virgule souhait�.
	 * @return true si la cha�ne test�e peut �tre convertie en un nombre d�cimal
	 *         positif ou n�gatif. Sinon, la m�thode retourne false. <BR>
	 *         Si le nombre de chiffres apr�s la virgule est �gal � 0, alors on
	 *         vreifie que la cha�ne est convertible en entier. <BR>
	 *         Si ce nombre est n�gatif, la m�thode renvoie une
	 *         InvalidParameterException.
	 */
	public static boolean isSignedDecimal(String value, int nbDecimal) {
		String pattern = "^-?[0-9]+";
		if (nbDecimal < 0) {
			throw new InvalidParameterException();
		} else if (nbDecimal == 0) {
			pattern += "$";
		} else {
			pattern += "\\.[0-9]{" + nbDecimal + "}?";
		}
		if (!Pattern.matches(pattern, value)) {
			return false;
		}
		return true;
	}

	/**
	 * Initialisation du tableau de correspondance entre les caract�res
	 * accentu�s et leur homologues non accentu�s
	 **/
	private static List<String> initMap() {
		List<String> result = new ArrayList<String>();
		String car = null;

		car = new String("A");
		result.add(car); /* '\u00C0' � alt-0192 */
		result.add(car); /* '\u00C1' �? alt-0193 */
		result.add(car); /* '\u00C2' � alt-0194 */
		result.add(car); /* '\u00C3' � alt-0195 */
		result.add(car); /* '\u00C4' � alt-0196 */
		result.add(car); /* '\u00C5' � alt-0197 */
		car = new String("AE");
		result.add(car); /* '\u00C6' � alt-0198 */
		car = new String("C");
		result.add(car); /* '\u00C7' � alt-0199 */
		car = new String("E");
		result.add(car); /* '\u00C8' � alt-0200 */
		result.add(car); /* '\u00C9' � alt-0201 */
		result.add(car); /* '\u00CA' � alt-0202 */
		result.add(car); /* '\u00CB' � alt-0203 */
		car = new String("I");
		result.add(car); /* '\u00CC' � alt-0204 */
		result.add(car); /* '\u00CD' �? alt-0205 */
		result.add(car); /* '\u00CE' � alt-0206 */
		result.add(car); /* '\u00CF' �? alt-0207 */
		car = new String("D");
		result.add(car); /* '\u00D0' �? alt-0208 */
		car = new String("N");
		result.add(car); /* '\u00D1' � alt-0209 */
		car = new String("O");
		result.add(car); /* '\u00D2' � alt-0210 */
		result.add(car); /* '\u00D3' � alt-0211 */
		result.add(car); /* '\u00D4' � alt-0212 */
		result.add(car); /* '\u00D5' � alt-0213 */
		result.add(car); /* '\u00D6' � alt-0214 */
		car = new String("*");
		result.add(car); /* '\u00D7' � alt-0215 */
		car = new String("0");
		result.add(car); /* '\u00D8' � alt-0216 */
		car = new String("U");
		result.add(car); /* '\u00D9' � alt-0217 */
		result.add(car); /* '\u00DA' � alt-0218 */
		result.add(car); /* '\u00DB' � alt-0219 */
		result.add(car); /* '\u00DC' � alt-0220 */
		car = new String("Y");
		result.add(car); /* '\u00DD' �? alt-0221 */
		car = new String("�");
		result.add(car); /* '\u00DE' � alt-0222 */
		car = new String("B");
		result.add(car); /* '\u00DF' � alt-0223 */
		car = new String("a");
		result.add(car); /* '\u00E0' � alt-0224 */
		result.add(car); /* '\u00E1' � alt-0225 */
		result.add(car); /* '\u00E2' � alt-0226 */
		result.add(car); /* '\u00E3' � alt-0227 */
		result.add(car); /* '\u00E4' � alt-0228 */
		result.add(car); /* '\u00E5' � alt-0229 */
		car = new String("ae");
		result.add(car); /* '\u00E6' � alt-0230 */
		car = new String("c");
		result.add(car); /* '\u00E7' � alt-0231 */
		car = new String("e");
		result.add(car); /* '\u00E8' � alt-0232 */
		result.add(car); /* '\u00E9' � alt-0233 */
		result.add(car); /* '\u00EA' � alt-0234 */
		result.add(car); /* '\u00EB' � alt-0235 */
		car = new String("i");
		result.add(car); /* '\u00EC' � alt-0236 */
		result.add(car); /* '\u00ED' � alt-0237 */
		result.add(car); /* '\u00EE' � alt-0238 */
		result.add(car); /* '\u00EF' � alt-0239 */
		car = new String("d");
		result.add(car); /* '\u00F0' � alt-0240 */
		car = new String("n");
		result.add(car); /* '\u00F1' � alt-0241 */
		car = new String("o");
		result.add(car); /* '\u00F2' � alt-0242 */
		result.add(car); /* '\u00F3' � alt-0243 */
		result.add(car); /* '\u00F4' � alt-0244 */
		result.add(car); /* '\u00F5' � alt-0245 */
		result.add(car); /* '\u00F6' � alt-0246 */
		car = new String("/");
		result.add(car); /* '\u00F7' � alt-0247 */
		car = new String("0");
		result.add(car); /* '\u00F8' � alt-0248 */
		car = new String("u");
		result.add(car); /* '\u00F9' � alt-0249 */
		result.add(car); /* '\u00FA' � alt-0250 */
		result.add(car); /* '\u00FB' � alt-0251 */
		result.add(car); /* '\u00FC' � alt-0252 */
		car = new String("y");
		result.add(car); /* '\u00FD' � alt-0253 */
		car = new String("�");
		result.add(car); /* '\u00FE' � alt-0254 */
		car = new String("y");
		result.add(car); /* '\u00FF' � alt-0255 */
		result.add(car); /* '\u00FF' alt-0255 */

		return result;
	}

	public static String convertToNoSign(String org) {
		char arrChar[] = org.toCharArray();
		char result[] = new char[arrChar.length];
		for (int i = 0; i < arrChar.length; i++) {
			switch (arrChar[i]) {
			case '\u00E1':
			case '\u00E0':
			case '\u1EA3':
			case '\u00E3':
			case '\u1EA1':
			case '\u0103':
			case '\u1EAF':
			case '\u1EB1':
			case '\u1EB3':
			case '\u1EB5':
			case '\u1EB7':
			case '\u00E2':
			case '\u1EA5':
			case '\u1EA7':
			case '\u1EA9':
			case '\u1EAB':
			case '\u1EAD':
			case '\u0203':
			case '\u01CE': {
				result[i] = 'a';
				break;
			}
			case '\u00E9':
			case '\u00E8':
			case '\u1EBB':
			case '\u1EBD':
			case '\u1EB9':
			case '\u00EA':
			case '\u1EBF':
			case '\u1EC1':
			case '\u1EC3':
			case '\u1EC5':
			case '\u1EC7':
			case '\u0207': {
				result[i] = 'e';
				break;
			}
			case '\u00ED':
			case '\u00EC':
			case '\u1EC9':
			case '\u0129':
			case '\u1ECB': {
				result[i] = 'i';
				break;
			}
			case '\u00F3':
			case '\u00F2':
			case '\u1ECF':
			case '\u00F5':
			case '\u1ECD':
			case '\u00F4':
			case '\u1ED1':
			case '\u1ED3':
			case '\u1ED5':
			case '\u1ED7':
			case '\u1ED9':
			case '\u01A1':
			case '\u1EDB':
			case '\u1EDD':
			case '\u1EDF':
			case '\u1EE1':
			case '\u1EE3':
			case '\u020F': {
				result[i] = 'o';
				break;
			}
			case '\u00FA':
			case '\u00F9':
			case '\u1EE7':
			case '\u0169':
			case '\u1EE5':
			case '\u01B0':
			case '\u1EE9':
			case '\u1EEB':
			case '\u1EED':
			case '\u1EEF':
			case '\u1EF1': {
				result[i] = 'u';
				break;
			}
			case '\u00FD':
			case '\u1EF3':
			case '\u1EF7':
			case '\u1EF9':
			case '\u1EF5': {
				result[i] = 'y';
				break;
			}
			case '\u0111': {
				result[i] = 'd';
				break;
			}
			case '\u00C1':
			case '\u00C0':
			case '\u1EA2':
			case '\u00C3':
			case '\u1EA0':
			case '\u0102':
			case '\u1EAE':
			case '\u1EB0':
			case '\u1EB2':
			case '\u1EB4':
			case '\u1EB6':
			case '\u00C2':
			case '\u1EA4':
			case '\u1EA6':
			case '\u1EA8':
			case '\u1EAA':
			case '\u1EAC':
			case '\u0202':
			case '\u01CD': {
				result[i] = 'A';
				break;
			}
			case '\u00C9':
			case '\u00C8':
			case '\u1EBA':
			case '\u1EBC':
			case '\u1EB8':
			case '\u00CA':
			case '\u1EBE':
			case '\u1EC0':
			case '\u1EC2':
			case '\u1EC4':
			case '\u1EC6':
			case '\u0206': {
				result[i] = 'E';
				break;
			}
			case '\u00CD':
			case '\u00CC':
			case '\u1EC8':
			case '\u0128':
			case '\u1ECA': {
				result[i] = 'I';
				break;
			}
			case '\u00D3':
			case '\u00D2':
			case '\u1ECE':
			case '\u00D5':
			case '\u1ECC':
			case '\u00D4':
			case '\u1ED0':
			case '\u1ED2':
			case '\u1ED4':
			case '\u1ED6':
			case '\u1ED8':
			case '\u01A0':
			case '\u1EDA':
			case '\u1EDC':
			case '\u1EDE':
			case '\u1EE0':
			case '\u1EE2':
			case '\u020E': {
				result[i] = 'O';
				break;
			}
			case '\u00DA':
			case '\u00D9':
			case '\u1EE6':
			case '\u0168':
			case '\u1EE4':
			case '\u01AF':
			case '\u1EE8':
			case '\u1EEA':
			case '\u1EEC':
			case '\u1EEE':
			case '\u1EF0': {
				result[i] = 'U';
				break;
			}

			case '\u00DD':
			case '\u1EF2':
			case '\u1EF6':
			case '\u1EF8':
			case '\u1EF4': {
				result[i] = 'Y';
				break;
			}
			case '\u0110':
			case '\u00D0':
			case '\u0089': {
				result[i] = 'D';
				break;
			}
			default:
				result[i] = arrChar[i];
			}
		}
		return new String(result);
	}
	
	public static boolean isNumeric(String str){
		boolean rs = false;
		if(StringUtils.isNotBlank(str)){
			Pattern pattern = Pattern.compile("\\d*");
			Matcher matcher = pattern.matcher(str);
			if (matcher.matches()) {
				rs = true;
			} 
		}			
		return rs;
	}		
	
	public static String convertToBytes(long fsize) {
		// Set things up
		DecimalFormat df = new DecimalFormat("0.00");
		float bytes = 1024;
		String type = new String();
		String size = new String();
		// Calculate file sizes
		float fsize_kb = fsize / bytes;
		float fsize_mb = fsize / bytes / bytes;
		float fsize_gb = fsize / bytes / bytes / bytes;
		float fsize_tb = fsize / bytes / bytes / bytes / bytes;
		// Pick the right size i.e KB, MB or GB
		if (fsize_kb < 1500) {
			size = df.format(fsize_kb);
			type = "KB";
		}
		if (fsize_mb >= 1 && fsize_mb < 1000) {
			size = df.format(fsize_mb);
			type = "MB";
		}
		if (fsize_gb >= 1 && fsize_gb < 1000) {
			size = df.format(fsize_gb);
			type = "GB";
		}
		if (fsize_tb >= 1) {
			size = df.format(fsize_gb);
			type = "TB";
		}
		// Return the size of the file with the right units....
		return (size + " " + type);
	}

}
