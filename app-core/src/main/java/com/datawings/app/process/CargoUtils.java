package com.datawings.app.process;

public final class CargoUtils {
	
	public static String HOT = "HOT";
	public static String OWN = "OWN";
	
	public static String CASS_SALES = "CASS_SALES";
	public static String DATE_FROM = "DATE_FROM";
	public static String DATE_TO = "DATE_TO";
	public static String TOUT = "TOUT";
	public static String AGENT = "AGENT";
	public static String COUNTRY = "COUNTRY";
	public static String CHANNEL = "CHANNEL";
	public static String MONTH = "MONTH";
	public static String RMK = "RMK";
	
	public static String SALE = "SALE";
	public static String FLOWN = "FLOWN";
	public static String CA_TRANSPORT = "CA_TRANSPORT";
	public static String ORIG = "ORIG";
	public static String DEST = "DEST";
	public static String FLIGHT_NUMBER = "FLIGHT_NUMBER";
	public static String IMMAT = "IMMAT";
	
	public static String STOCK_OWN = "OWN";
	public static String STOCK_TR = "TR";
	
	public static Integer NB_TAX = 10;
	public static Integer NB_DETAIL = 5;
	
	public static String WEIGHT_K = "K";
	public static String WEIGHT_P = "P";
	
	public static String CHARGE_P = "P";
	public static String CHARGE_C = "C";
	
	public static String NVD = "NVD";
	public static String NCV = "NCV";
	
	public static String STATUS_F = "F";
	public static String STATUS_I = "I";
	
	public static String ROLE_C = "_C";
	public static String ROLE_U = "_U";
	public static String ROLE_D = "_D";
	public static String ROLE_DW = "ROLE_DW";
	public static String ROLE_ADMIN = "ROLE_ADMIN"; 
	
	public static String MAX_DATE = "31/12/9999";
	
	
}
