package com.datawings.app.process;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.datawings.app.common.DateUtil;
import com.datawings.app.common.DoubleUtil;
import com.datawings.app.common.IntegerUtil;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.model.Agent;
import com.datawings.app.model.AirlineParam;
import com.datawings.app.model.Awb;
import com.datawings.app.model.AwbDetail;
import com.datawings.app.model.AwbDetailId;
import com.datawings.app.model.AwbTax;
import com.datawings.app.model.AgentGsa;
import com.datawings.app.model.Line;
import com.datawings.app.model.Rdv;
import com.datawings.app.service.IAgentGsaService;
import com.datawings.app.service.IAgentService;
import com.datawings.app.service.IAirlineParamService;
import com.datawings.app.service.IAwbService;
import com.datawings.app.service.ILineService;
import com.datawings.app.service.IRdvService;

@Component
public class AwbUtil {

	private static Double ratePound = 0.45359237;
	
	@Autowired
	private IRdvService rdvService;
	
	@Autowired
	private IAirlineParamService airlineParamService;
	
	@Autowired
	private IAgentService agentService;
	
	@Autowired
	private IAwbService awbService;
	
	@Autowired
	private IAgentGsaService agentGsaService;
	
	@Autowired
	private ILineService lineService;
	
	public Rdv processRdv(String agtn, String date) {
		Rdv rdv = rdvService.getRdv(agtn, date);
		
		if(rdv == null){
			String rdvCode = "";
			String rdvCodeMax = rdvService.getMaxRdvCode(date);
			if(StringUtils.isBlank(rdvCodeMax)){
				String tmp = DateUtil.date2String(DateUtil.string2Date(date, "dd/MM/yyyy"), "yyyyMMdd");
				rdvCode = tmp + "001";
			}else{
				String temp1 = StringUtils.substring(rdvCodeMax, 0, 8);
				String temp2 = StringUtils.substring(rdvCodeMax, 8, 11);
				Integer intVal = Integer.parseInt("1" + temp2);
				intVal = intVal + 1;
				String intValTmp = intVal.toString();
				rdvCode = temp1 + StringUtils.substring(intValTmp, 1);
			}
			rdv = new Rdv();
			rdv.setAgtn(agtn);
			rdv.setEmission(DateUtil.string2Date(date, "dd/MM/yyyy"));
			rdv.setRdv(rdvCode);
			rdv.setCode(rdv.getRdv()+ "_" + rdv.getAgtn()+ "_" + DateUtil.date2String(rdv.getEmission(), "yyyyMMdd"));
			rdv.setCreatedDate(new Date());
			rdvService.save(rdv);
		}
		return rdv;
	}

	public void copyValue(Awb awb, AwbFilter filter) {
		awb.setTacn(filter.getTacn());
		awb.setLta(filter.getLta());
		awb.setAgtn(filter.getAgtn().toUpperCase());
		awb.setAgtnCode(StringUtils.substring(awb.getAgtn(), 0, 10));
		awb.setCheckDigit(StringUtils.substring(awb.getAgtn(), 10, 11));
		awb.setAgtnIata(StringUtils.substring(awb.getAgtn(), 0, 7));
		awb.setDateExecute(DateUtil.string2Date(filter.getDateExecute(), "dd/MM/yyyy"));
		awb.setDescriptUser(filter.getDescriptUser());
		
		awb.setShipName(filter.getShipName().toUpperCase());
		awb.setShipAddress1(filter.getShipAddress1().toUpperCase());
		awb.setShipAddress2(filter.getShipAddress2().toUpperCase());
		awb.setShipAddress3(filter.getShipAddress3().toUpperCase());
		awb.setConsigneeName(filter.getConsigneeName().toUpperCase());
		awb.setConsigneeAddress1(filter.getConsigneeAddress1().toUpperCase());
		awb.setConsigneeAddress2(filter.getConsigneeAddress2().toUpperCase());
		awb.setConsigneeAddress3(filter.getConsigneeAddress3().toUpperCase());
		awb.setOrig(filter.getOrig().toUpperCase());
		awb.setDest(filter.getDest().toUpperCase());
		awb.setFlightNumber(filter.getCarrier().toUpperCase() + filter.getFlightNumber().toUpperCase());
		awb.setFlightDate(DateUtil.string2Date(filter.getFlightDate(), "dd/MM/yyyy"));
		awb.setCutp(filter.getCutp());
		awb.setChargeIndicator(filter.getChargeIndicator());
		awb.setOtherIndicator(filter.getOtherIndicator());
		awb.setDeclaredCarrige(filter.getDeclaredCarrige().toUpperCase());
		awb.setDeclaredCustom(filter.getDeclaredCustom().toUpperCase());
		awb.setHandlingInfor(filter.getHandlingInfor().toUpperCase());
		awb.setWeightIndicator(filter.getWeightIndicator1());
		
		awb.setModePayment(filter.getModePayment());
		if(StringUtils.equals(awb.getModePayment(), "CC")){
			awb.setNbCart(filter.getNbCart());
			awb.setDateExp(filter.getDateExp());
			awb.setNameCart(filter.getNameCart().toUpperCase());
		}
	}

	public void convertLocal(Awb awb) {
		AirlineParam airlineParam = airlineParamService.find(awb.getTacn());
		
		if(airlineParam != null){
			String cutpAirline = airlineParam.getCurrency();
			
			//Convert curency to curency local
			if(!StringUtils.equals(cutpAirline, awb.getCutp())){
				Double rate = 0.0;
				rate = awbService.getRate(awb.getCutp(), cutpAirline, DateUtil.date2String(awb.getDateExecute(), "MM/dd/yyyy"));
				
				if(rate != null){
					awb.setRoe(rate);
					awb.setCutpLc(cutpAirline);
					awb.setWeightPpLc(DoubleUtil.round(awb.getWeightPp() * rate, 2));
					awb.setValuationPpLc(DoubleUtil.round(awb.getValuationPp() * rate, 2));
					awb.setCarrierPpLc(DoubleUtil.round(awb.getCarrierPp() * rate, 2));
					awb.setAgentPpLc(DoubleUtil.round(awb.getAgentPp() * rate, 2));
					awb.setTotalPpLc(awb.getWeightPpLc() + awb.getCarrierPpLc() + awb.getAgentPpLc());
					
					awb.setWeightCcLc(DoubleUtil.round(awb.getWeightCc() * rate, 2));
					awb.setValuationCcLc(DoubleUtil.round(awb.getValuationCc() * rate, 2));
					awb.setCarrierCcLc(DoubleUtil.round(awb.getCarrierCc() * rate, 2));
					awb.setAgentCcLc(DoubleUtil.round(awb.getAgentCc() * rate, 2));
					awb.setTotalCcLc(awb.getWeightCcLc() + awb.getCarrierCcLc() + awb.getAgentCcLc());
					
					awb.setCommLc(DoubleUtil.round(awb.getComm() * rate, 2));
					awb.setDiscountLc(DoubleUtil.round(awb.getDiscount() * rate, 2));
					awb.setTaxeAgentLc(DoubleUtil.round(awb.getTaxeAgent() * rate, 2));
					awb.setTaxAirlineLc(DoubleUtil.round(awb.getTaxAirline() * rate, 2));
				}
			}else {
				awb.setRoe(1.0);
				awb.setCutpLc(cutpAirline);
				awb.setWeightPpLc(awb.getWeightPp());
				awb.setValuationPpLc(awb.getValuationPp());
				awb.setCarrierPpLc(awb.getCarrierPp());
				awb.setAgentPpLc(awb.getAgentPp());
				awb.setTotalPpLc(awb.getWeightPpLc() + awb.getCarrierPpLc() + awb.getAgentPpLc());
				
				awb.setWeightCcLc(awb.getWeightCc());
				awb.setValuationCcLc(awb.getValuationCc());
				awb.setCarrierCcLc(awb.getCarrierCc());
				awb.setAgentCcLc(awb.getAgentCc());
				awb.setTotalCcLc(awb.getWeightCcLc() + awb.getCarrierCcLc() + awb.getAgentCcLc());
				
				awb.setCommLc(awb.getComm());
				awb.setDiscountLc(awb.getDiscount());
				awb.setTaxeAgentLc(awb.getTaxeAgent());
				awb.setTaxAirlineLc(awb.getTaxAirline());
			}
		}
		
		//Convert K to P
		if(StringUtils.equals("P", awb.getWeightIndicator())){
			awb.setWeightIndicatorLc("K");
			awb.setWeightGrossLc(DoubleUtil.round(awb.getWeightGross() * ratePound, 2));
		}
		
	}

	public void processAgent(Awb awb) {
		Agent agent = agentService.find(awb.getAgtnCode());
		awb.setAgtnName(agent.getName().trim().toUpperCase());
		awb.setIsoc(agent.getIsoc());
	}

	public void processDetail(Awb awb, AwbFilter filter) {
		Integer sequence = 0;
		
		if(StringUtils.isNotBlank(filter.getQuantity1())){
			sequence = sequence + 1;
			
			AwbDetail awbDetail1 = new AwbDetail();
			setDetail(awbDetail1, awb, filter.getQuantity1(), filter.getWeightIndicator1(), filter.getWeightGross1(), 
					filter.getRateClass1(), filter.getWeightCharge1(), filter.getUnit1(), filter.getAmount1(), filter.getDescription1(), sequence);
			filter.setAmount1(awbDetail1.getAmount().toString());
			awbDetail1.setAwb(awb);
			awb.getAwbDetail().add(awbDetail1);
		}
		
		if(StringUtils.isNotBlank(filter.getQuantity2())){
			sequence = sequence + 1;
			
			AwbDetail awbDetail2 = new AwbDetail();
			setDetail(awbDetail2, awb, filter.getQuantity2(), filter.getWeightIndicator2(), filter.getWeightGross2(), 
					filter.getRateClass2(), filter.getWeightCharge2(), filter.getUnit2(), filter.getAmount2(), filter.getDescription2(), sequence);
			filter.setAmount2(awbDetail2.getAmount().toString());
			awbDetail2.setAwb(awb);
			awb.getAwbDetail().add(awbDetail2);
		}
		
		if(StringUtils.isNotBlank(filter.getQuantity3())){
			sequence = sequence + 1;
			
			AwbDetail awbDetail3 = new AwbDetail();
			setDetail(awbDetail3, awb, filter.getQuantity3(), filter.getWeightIndicator3(), filter.getWeightGross3(), 
					filter.getRateClass3(), filter.getWeightCharge3(), filter.getUnit3(), filter.getAmount3(), filter.getDescription3(), sequence);
			awbDetail3.setAwb(awb);
			filter.setAmount3(awbDetail3.getAmount().toString());
			awb.getAwbDetail().add(awbDetail3);
		}
		
		if(StringUtils.isNotBlank(filter.getQuantity4())){
			sequence = sequence + 1;
			
			AwbDetail awbDetail4 = new AwbDetail();
			setDetail(awbDetail4, awb, filter.getQuantity4(), filter.getWeightIndicator4(), filter.getWeightGross4(), 
					filter.getRateClass4(), filter.getWeightCharge4(), filter.getUnit4(), filter.getAmount4(), filter.getDescription4(), sequence);
			filter.setAmount4(awbDetail4.getAmount().toString());
			awbDetail4.setAwb(awb);
			awb.getAwbDetail().add(awbDetail4);
		}
		
		if(StringUtils.isNotBlank(filter.getQuantity5())){
			sequence = sequence + 1;
			
			AwbDetail awbDetail5 = new AwbDetail();
			setDetail(awbDetail5, awb, filter.getQuantity5(), filter.getWeightIndicator5(), filter.getWeightGross5(), 
					filter.getRateClass5(), filter.getWeightCharge5(), filter.getUnit5(), filter.getAmount5(), filter.getDescription5(), sequence);
			filter.setAmount5(awbDetail5.getAmount().toString());
			awbDetail5.setAwb(awb);
			awb.getAwbDetail().add(awbDetail5);
		}
	}

	private void setDetail(AwbDetail awbDetail, Awb awb, String quantity, String weightIndicator, String weightGross, String rateClass,
			String weightCharge, String unit, String amount, String description, Integer sequence) {
		
		/*awbDetail.setIdAwbDetail(awb.getLta() + "_" + sequence );
		awbDetail.setLta(awb.getLta());
		awbDetail.setSequence(sequence);*/
		AwbDetailId awbDetailId = new AwbDetailId();
		awbDetailId.setLta(awb.getLta());
		awbDetailId.setSequence(sequence);
		
		awbDetail.setId(awbDetailId);
		
		awbDetail.setQuantity(IntegerUtil.convertInteger(quantity));
		awbDetail.setWeightIndicator(weightIndicator);
		awbDetail.setWeightGross(DoubleUtil.convertDouble(weightGross));
		awbDetail.setRateClass(rateClass);
		awbDetail.setWeightCharge(DoubleUtil.convertDouble(weightCharge));
		awbDetail.setUnit(DoubleUtil.convertDouble(unit));
		awbDetail.setUnitCass(DoubleUtil.convertDouble(unit));
		awbDetail.setAmount(DoubleUtil.round(awbDetail.getWeightCharge() * awbDetail.getUnit(), 2));
		awbDetail.setDescription(description.toUpperCase());
		awbDetail.setCreatedBy(awb.getCreatedBy());
		awbDetail.setCreatedDate(new Date());
	}

	public void processTax(Awb awb, AwbFilter filter) {
		
		if(StringUtils.isNotBlank(filter.getTaxCode1())){
			AwbTax awbTax1 = new AwbTax();
			setTax(awbTax1, awb, filter.getTaxCode1(), filter.getTaxValue1());
			awbTax1.setAwb(awb);
			awb.getAwbTax().add(awbTax1);
		}
		if(StringUtils.isNotBlank(filter.getTaxCode2())){
			AwbTax awbTax2 = new AwbTax();
			setTax(awbTax2, awb, filter.getTaxCode2(), filter.getTaxValue2());
			awbTax2.setAwb(awb);
			awb.getAwbTax().add(awbTax2);
		}
		if(StringUtils.isNotBlank(filter.getTaxCode3())){
			AwbTax awbTax3 = new AwbTax();
			setTax(awbTax3, awb, filter.getTaxCode3(), filter.getTaxValue3());
			awbTax3.setAwb(awb);
			awb.getAwbTax().add(awbTax3);
		}
		if(StringUtils.isNotBlank(filter.getTaxCode4())){
			AwbTax awbTax4 = new AwbTax();
			setTax(awbTax4, awb, filter.getTaxCode4(), filter.getTaxValue4());
			awbTax4.setAwb(awb);
			awb.getAwbTax().add(awbTax4);
		}
		if(StringUtils.isNotBlank(filter.getTaxCode5())){
			AwbTax awbTax5 = new AwbTax();
			setTax(awbTax5, awb, filter.getTaxCode5(), filter.getTaxValue5());
			awbTax5.setAwb(awb);
			awb.getAwbTax().add(awbTax5);
		}
		if(StringUtils.isNotBlank(filter.getTaxCode6())){
			AwbTax awbTax6 = new AwbTax();
			setTax(awbTax6, awb, filter.getTaxCode6(), filter.getTaxValue6());
			awbTax6.setAwb(awb);
			awb.getAwbTax().add(awbTax6);
		}
		if(StringUtils.isNotBlank(filter.getTaxCode7())){
			AwbTax awbTax7 = new AwbTax();
			setTax(awbTax7, awb, filter.getTaxCode7(), filter.getTaxValue7());
			awbTax7.setAwb(awb);
			awb.getAwbTax().add(awbTax7);
		}
		if(StringUtils.isNotBlank(filter.getTaxCode8())){
			AwbTax awbTax8 = new AwbTax();
			setTax(awbTax8, awb, filter.getTaxCode8(), filter.getTaxValue8());
			awbTax8.setAwb(awb);
			awb.getAwbTax().add(awbTax8);
		}
		
	}

	private void setTax(AwbTax awbTax, Awb awb, String taxCode, String taxValue) {
		awbTax.getId().setLta(awb.getLta());
		awbTax.getId().setTaxCode(taxCode.toUpperCase());
		awbTax.setTaxAmount(DoubleUtil.convertDouble(taxValue));
		awbTax.setCreatedBy(awb.getCreatedBy());
		awbTax.setCreatedDate(new Date());
	}

	public void processLine(Awb awb) {
		Double km = awbService.getKm(awb.getOrig(), awb.getDest());
		awb.setKm(km);
		
		Line line = lineService.getLine(awb.getOrig(), awb.getDest());
		if(line != null){
			awb.setLine(line.getLine().trim());
		}
	}

	public void processGsa(Awb awb) {
		AgentGsa agentGsa = agentGsaService.getAgentGsa(awb.getAgtnIata(), awb.getDateExecute());
		if(agentGsa != null){
			awb.setAgtnGsa(agentGsa.getCode());
		}
	}

	public void processTotal(Awb awb, AwbFilter filter) {
		Double weightGross = 0.0;
		Double weightCharge = 0.0;
		Double charge = 0.0;
		for (AwbDetail elm : awb.getAwbDetail()) {
			weightGross += elm.getWeightGross();
			weightCharge += elm.getWeightCharge();
			charge += elm.getAmount();
		}
		
		awb.setWeightGross(weightGross);
		awb.setWeightCharge(weightCharge);
		
		if(StringUtils.equals(awb.getChargeIndicator(), "P")){
			awb.setWeightPp(charge);
		}else {
			awb.setWeightCc(charge);
		}
		filter.setWeightPp(awb.getWeightPp().toString());
		filter.setWeightCc(awb.getWeightCc().toString());
		
		
		Double agentCharge = 0.0;
		Double carrierCharge = 0.0;
		
		for (AwbTax elm : awb.getAwbTax()) {
			if(StringUtils.equals(StringUtils.substring(elm.getId().getTaxCode(), 2), "A")){
				agentCharge += elm.getTaxAmount();
			}else{
				carrierCharge += elm.getTaxAmount();
			}
		}
		
		if(StringUtils.equals(awb.getOtherIndicator(), "P")){
			awb.setAgentPp(agentCharge);
			awb.setCarrierPp(carrierCharge);
			
			filter.setAgentPp(agentCharge.toString());
			filter.setCarrierPp(carrierCharge.toString());
			filter.setAgentCc("0.0");
			filter.setCarrierCc("0.0");
		}else {
			awb.setAgentCc(DoubleUtil.convertDouble(filter.getAgentCc()));
			awb.setCarrierCc(DoubleUtil.convertDouble(filter.getCarrierCc()));
			
			filter.setAgentPp("0.0");
			filter.setCarrierPp("0.0");
			filter.setAgentCc(agentCharge.toString());
			filter.setCarrierCc(carrierCharge.toString());
		}
		
		awb.setTotalPp(awb.getWeightPp() + awb.getAgentPp() + awb.getCarrierPp());
		awb.setTotalCc(awb.getWeightCc() + awb.getAgentCc() + awb.getCarrierCc());
		filter.setTotalPp(awb.getTotalPp().toString());
		filter.setTotalCc(awb.getTotalCc().toString());
		
		awb.setCommPercent(DoubleUtil.convertDouble(filter.getCommPercent()));
		awb.setComm(DoubleUtil.round((awb.getWeightPp() + awb.getWeightCc()) * awb.getCommPercent() / 100, 2)); 
		
		filter.setComm(awb.getComm().toString());
		
		awb.setVatPercent(DoubleUtil.convertDouble(filter.getVatPercent()));
		awb.setVat((awb.getTotalPp() + awb.getTotalCc()) * awb.getVatPercent() / 100);
		
		filter.setVat(awb.getVat().toString());
		
		awb.setNet(awb.getTotalPp() + awb.getTotalCc() + awb.getVat() - awb.getComm());
		filter.setNet(awb.getNet().toString());
	}
}
