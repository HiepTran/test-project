package com.datawings.app.process;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.datawings.app.common.BeanUtil;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.model.Awb;
import com.datawings.app.model.AwbDetail;
import com.datawings.app.model.AwbFlown;
import com.datawings.app.model.AwbFlownDetail;
import com.datawings.app.model.AwbFlownTax;
import com.datawings.app.model.AwbTax;
import com.datawings.app.model.Manifest;
import com.datawings.app.model.SysUser;
import com.datawings.app.service.IAwbDetailService;
import com.datawings.app.service.IAwbFlownDetailService;
import com.datawings.app.service.IAwbFlownService;
import com.datawings.app.service.IAwbFlownTaxService;
import com.datawings.app.service.IAwbService;

@Component
public class ManifestManager {

	@Autowired
	private IAwbService awbService;
	
	@Autowired
	private IAwbDetailService awbDetailService;
	
	@Autowired
	private IAwbFlownDetailService awbFlownDetailService;
	
	@Autowired
	private IAwbFlownTaxService awbFlownTaxService;
	
	@Autowired
	private IAwbFlownService awbFlownService;
	
	@Autowired
	private AwbFlownUtil awbFlownUtil;
	
	/**
	 * method create AwbFlown with choose checkbox
	 * @param AWB {number LTA}
	 * @param FLOWN {number No manifest}
	 */
	@Transactional
	public void flownGeneral(Manifest manifest, String[] codeAdd, SysUser sysUser) {

		for (String code: codeAdd) {
			String[] item = code.split(";");
			if(item[1].equals("AWB")){
				Awb awb = awbService.find(item[0]);
				
				AwbFlown awbFlown = new AwbFlown();
				awbFlownService.save(awbFlown);
				
				BeanUtil.copyProperties(awbFlown, awb);
				
				awbFlown.setManifestNo(manifest.getManifestNo());
				awbFlown.setFlightDate(manifest.getFlightDate());
				awbFlown.setFlightNumber(manifest.getFlightNumber());
				awbFlown.setImmat(manifest.getImmat());
				
				awbFlown.setStatusFlown(CargoUtils.STATUS_F);
				awbFlown.setCreatedDate(new Date());
				awbFlown.setCreatedBy(sysUser.getUsername());
									
				//Detail
				for(AwbDetail elm: awb.getAwbDetail()){
					AwbFlownDetail awbFlownDetail = new AwbFlownDetail();
					BeanUtil.copyProperties(awbFlownDetail, elm);
					awbFlownDetail.setLta(elm.getId().getLta());
					awbFlownDetail.setSequence(elm.getId().getSequence());
					awbFlownDetail.setManifestNo(manifest.getManifestNo());
					awbFlownDetail.setCreatedDate(new Date());
					awbFlownDetail.setCreatedBy(sysUser.getUsername());
					
					awbFlownDetail.setAwbFlownId(awbFlown.getAwbFlownId());
					awbFlownDetail.setAwbFlown(awbFlown);
					awbFlown.getAwbFlownDetail().add(awbFlownDetail);
				}
				
				//Tax
				for(AwbTax tax: awb.getAwbTax() ){
					AwbFlownTax awbFlownTax = new AwbFlownTax();
					BeanUtil.copyProperties(awbFlownTax, tax);
					awbFlownTax.setLta(tax.getId().getLta());
					awbFlownTax.setManifestNo(manifest.getManifestNo());
					awbFlownTax.setTaxCode(tax.getId().getTaxCode());
					awbFlownTax.setCreatedDate(new Date());
					awbFlownTax.setCreatedBy(sysUser.getUsername());
					
					awbFlownTax.setAwbFlownId(awbFlown.getAwbFlownId());
					awbFlownTax.setAwbFlown(awbFlown);
					awbFlown.getAwbFlownTax().add(awbFlownTax);
				}
				
				awbFlownService.merge(awbFlown);
				
				awb.setStatusControl(CargoUtils.STATUS_F);
				awb.setStatusFlown(CargoUtils.STATUS_F);
				awbService.merge(awb);
			}else {
				AwbFlown awbFlown =  awbFlownService.find(Integer.parseInt(item[0]));
				awbFlown.setManifestNo(manifest.getManifestNo());
				awbFlown.setStatusFlown(CargoUtils.STATUS_F);
				awbFlown.setCreatedDate(new Date());
				awbFlown.setCreatedBy(sysUser.getUsername());
				
				//Detail
				for(AwbFlownDetail elm: awbFlown.getAwbFlownDetail()){
					elm.setManifestNo(manifest.getManifestNo());
				}
				
				//Tax
				for(AwbFlownTax elm: awbFlown.getAwbFlownTax()){
					elm.setManifestNo(manifest.getManifestNo());
				}
				awbFlownService.merge(awbFlown);
			}
		}
	}

	/**
	 * method create AwbFlown with choose split leg with LTA sale
	 * return Awb flown, insert table awb_flown with status flown F
	 * return Awb sale, insert table awb_flown with status flown I
	 */
	@Transactional
	public void flownSplitAwbLeg(Manifest manifest, Awb awb, AwbFilter filter, SysUser sysUser) {
		AwbFlown awbFlown = new AwbFlown();
		awbFlown.setManifestNo(manifest.getManifestNo());
		awbFlown.setCoupon(1);
		awbFlown.setFlightDate(manifest.getFlightDate());
		awbFlown.setFlightNumber(manifest.getFlightNumber());
		awbFlown.setImmat(manifest.getImmat());
		
		awbFlownService.save(awbFlown);
		
		Double flownCharge = 0.0;
		BeanUtil.copyProperties(awbFlown, awb);
		
		flownCharge = awbFlownUtil.awbKmDetail(manifest, awb, awbFlown, filter, sysUser);
		awbFlownUtil.awbKmTax(manifest, awb, awbFlown, filter, sysUser);
		awbFlownUtil.awbKmTotal(manifest, awbFlown, sysUser);
		
		awbFlownService.merge(awbFlown);
		
		//AWB
		awb.setStatusControl(CargoUtils.STATUS_F);
		awb.setStatusFlown(CargoUtils.STATUS_F);
		awbService.merge(awb);
		
		//REST
		Double awbCharge = 0.0;
		if(StringUtils.equals(awb.getChargeIndicator(), "P")){
			awbCharge = awb.getWeightPp();
		}else {
			awbCharge = awb.getWeightCc();
		}
		
		if(flownCharge.equals(awbCharge) == false){
			AwbFlown awbFlownRest = new AwbFlown();
			awbFlownService.save(awbFlownRest);
			
			BeanUtil.copyProperties(awbFlownRest, awb);
			awbFlownUtil.awbKmDetailRest(awb, awbFlown, awbFlownRest, filter, sysUser);
			awbFlownUtil.awbKmTaxRest(awb, awbFlown, awbFlownRest, filter, sysUser);
			awbFlownUtil.awbKmTotalRest(manifest, awb, awbFlown, awbFlownRest, filter, sysUser);
			
			awbFlownService.merge(awbFlownRest);
		}
	}

	/**
	 * method create AwbFlown with choose split leg with LTA flown
	 * return Awb flown, insert table awb_flown with status flown F
	 * return Awb sale, insert table awb_flown with status flown I
	 */
	@Transactional
	public void flownSplitFlownLeg(Manifest manifest, AwbFlown awbFlown, AwbFilter filter, SysUser sysUser) {
		AwbFlown awbFlownOrig = new AwbFlown();
		BeanUtil.copyProperties(awbFlownOrig, awbFlown);
		
		List<AwbFlownDetail> awbFlownDetailOrig = new ArrayList<AwbFlownDetail>();
		for(AwbFlownDetail item: awbFlownOrig.getAwbFlownDetail()){
			AwbFlownDetail elm = new AwbFlownDetail();
			BeanUtil.copyProperties(elm, item);
			awbFlownDetailOrig.add(elm);
		}
		
		List<AwbFlownTax> awbFlownTaxOrig = new ArrayList<AwbFlownTax>();
		
		for(AwbFlownTax item: awbFlownOrig.getAwbFlownTax()){
			AwbFlownTax elm = new AwbFlownTax();
			BeanUtil.copyProperties(elm, item);
			awbFlownTaxOrig.add(elm);
		}
		
		awbFlown.setManifestNo(manifest.getManifestNo());
		
		Double flownCharge = 0.0;
		flownCharge = awbFlownUtil.awbFlownKmDetail(manifest, awbFlown, filter, sysUser);
		awbFlownUtil.awbFlownKmTax(manifest, awbFlown, filter, sysUser);
		awbFlownUtil.awbFlownKmTotal(manifest, awbFlown, sysUser);
		
		awbFlownService.merge(awbFlown);
		
		//REST
		Double awbCharge = 0.0;
		if(StringUtils.equals(awbFlown.getChargeIndicator(), "P")){
			awbCharge = awbFlownOrig.getWeightPp();
		}else {
			awbCharge = awbFlownOrig.getWeightCc();
		}
		
		if(flownCharge.equals(awbCharge) == false){
			AwbFlown awbFlownRest = new AwbFlown();
			awbFlownService.save(awbFlownRest);
			
			Integer id = new Integer(0);
			id = awbFlownRest.getAwbFlownId();
			
			BeanUtil.copyProperties(awbFlownRest, awbFlownOrig);
			awbFlownRest.setAwbFlownId(id);
			
			awbFlownUtil.awbFlownKmDetailRest(awbFlownOrig, awbFlownDetailOrig, awbFlown, awbFlownRest, filter, sysUser);
			awbFlownUtil.awbFlownKmTaxRest(awbFlownOrig, awbFlownTaxOrig, awbFlown, awbFlownRest, filter, sysUser);
			awbFlownUtil.awbFlownKmTotalRest(manifest, awbFlownOrig, awbFlown, awbFlownRest, filter, sysUser);
			
			awbFlownService.merge(awbFlownRest);
		}
		
	}

	/**
	 * method create AwbFlown with choose split weight with LTA sale
	 * return Awb flown, insert table awb_flown with status flown F
	 * return Awb sale, insert table awb_flown with status flown I
	 */
	@Transactional
	public void flownSplitAwbWeight(Manifest manifest, Awb awb, AwbFilter filter, SysUser sysUser) {
		AwbFlown awbFlown = new AwbFlown();
		awbFlown.setManifestNo(manifest.getManifestNo());
		awbFlown.setCoupon(1);
		awbFlown.setFlightDate(manifest.getFlightDate());
		awbFlown.setFlightNumber(manifest.getFlightNumber());
		awbFlown.setImmat(manifest.getImmat());
		
		awbFlownService.save(awbFlown);
		
		Double flownCharge = 0.0;
		BeanUtil.copyProperties(awbFlown, awb);
		
		flownCharge = awbFlownUtil.awbKgDetail(manifest, awb, awbFlown, filter, sysUser);
		awbFlownUtil.awbKgTax(manifest, awb, awbFlown, filter, sysUser);
		awbFlownUtil.awbKgTotal(manifest, awbFlown, sysUser);
		
		awbFlownService.merge(awbFlown);
		
		//AWB
		awb.setStatusControl(CargoUtils.STATUS_F);
		awb.setStatusFlown(CargoUtils.STATUS_F);
		awbService.merge(awb);
		
		//REST
		Double awbCharge = 0.0;
		if(StringUtils.equals(awb.getChargeIndicator(), "P")){
			awbCharge = awb.getWeightPp();
		}else {
			awbCharge = awb.getWeightCc();
		}
		
		if(flownCharge.equals(awbCharge) == false){
			AwbFlown awbFlownRest = new AwbFlown();
			awbFlownService.save(awbFlownRest);
			
			BeanUtil.copyProperties(awbFlownRest, awb);
			awbFlownUtil.awbKgDetailRest(awb, awbFlown, awbFlownRest, filter, sysUser);
			awbFlownUtil.awbKgTaxRest(awb, awbFlown, awbFlownRest, filter, sysUser);
			awbFlownUtil.awbKgTotalRest(manifest, awb, awbFlown, awbFlownRest, filter, sysUser);
			
			awbFlownService.merge(awbFlownRest);
		}
	}

	/**
	 * method create AwbFlown with choose split weight with LTA flown
	 * return Awb flown, insert table awb_flown with status flown F
	 * return Awb sale, insert table awb_flown with status flown I
	 */
	@Transactional
	public void flownSplitFlownWeight(Manifest manifest, AwbFlown awbFlown, AwbFilter filter, SysUser sysUser) {
		AwbFlown awbFlownOrig = new AwbFlown();
		BeanUtil.copyProperties(awbFlownOrig, awbFlown);
		
		List<AwbFlownDetail> awbFlownDetailOrig = new ArrayList<AwbFlownDetail>();
		for(AwbFlownDetail item: awbFlownOrig.getAwbFlownDetail()){
			AwbFlownDetail elm = new AwbFlownDetail();
			BeanUtil.copyProperties(elm, item);
			awbFlownDetailOrig.add(elm);
		}
		
		List<AwbFlownTax> awbFlownTaxOrig = new ArrayList<AwbFlownTax>();
		
		for(AwbFlownTax item: awbFlownOrig.getAwbFlownTax()){
			AwbFlownTax elm = new AwbFlownTax();
			BeanUtil.copyProperties(elm, item);
			awbFlownTaxOrig.add(elm);
		}
		
		awbFlown.setManifestNo(manifest.getManifestNo());
		
		Double flownCharge = 0.0;
		flownCharge = awbFlownUtil.awbFlownKgDetail(manifest, awbFlown, filter, sysUser);
		awbFlownUtil.awbFlownKgTax(manifest, awbFlown, filter, sysUser);
		awbFlownUtil.awbKgTotal(manifest, awbFlown, sysUser);
		
		awbFlownService.merge(awbFlown);
		
		//REST
		Double awbCharge = 0.0;
		if(StringUtils.equals(awbFlown.getChargeIndicator(), "P")){
			awbCharge = awbFlownOrig.getWeightPp();
		}else {
			awbCharge = awbFlownOrig.getWeightCc();
		}
		
		if(flownCharge.equals(awbCharge) == false){
			AwbFlown awbFlownRest = new AwbFlown();
			awbFlownService.save(awbFlownRest);
			
			Integer id = new Integer(0);
			id = awbFlownRest.getAwbFlownId();
			
			BeanUtil.copyProperties(awbFlownRest, awbFlownOrig);
			awbFlownRest.setAwbFlownId(id);
			
			awbFlownUtil.awbFlownKgDetailRest(awbFlownOrig, awbFlownDetailOrig, awbFlown, awbFlownRest, filter, sysUser);
			awbFlownUtil.awbFlownKgTaxRest(awbFlownOrig, awbFlownTaxOrig, awbFlown, awbFlownRest, filter, sysUser);
			awbFlownUtil.awbKgTotalRest(manifest, awbFlownOrig, awbFlown, awbFlownRest, filter, sysUser);
			
			awbFlownService.merge(awbFlownRest);
		}
	}
}
