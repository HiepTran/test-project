package com.datawings.app.process;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.datawings.app.common.DoubleUtil;
import com.datawings.app.common.IntegerUtil;
import com.datawings.app.filter.AwbDetailFilter;
import com.datawings.app.filter.AwbFilter;
import com.datawings.app.filter.AwbTaxFilter;
import com.datawings.app.model.Awb;
import com.datawings.app.model.AwbDetail;
import com.datawings.app.model.AwbDetailId;
import com.datawings.app.model.AwbFlown;
import com.datawings.app.model.AwbFlownDetail;
import com.datawings.app.model.AwbFlownTax;
import com.datawings.app.model.AwbTax;
import com.datawings.app.model.Manifest;
import com.datawings.app.model.SysUser;
import com.datawings.app.service.IAwbDetailService;

@Component
public class AwbFlownUtil {
	
	@Autowired
	private IAwbDetailService awbDetailService;
	
	public Double awbKmDetail(Manifest manifest, Awb awb, AwbFlown awbFlown, AwbFilter filter, SysUser sysUser) {
		Double flownCharge = 0.0;
		for (int i = 0; i < filter.getAwbDetail().size(); i++) {
			AwbDetailFilter elm = filter.getAwbDetail().get(i);
			
			AwbFlownDetail awbFlownDetail = new AwbFlownDetail();
			
			awbFlownDetail.setManifestNo(manifest.getManifestNo());
			awbFlownDetail.setLta(awb.getLta());
			awbFlownDetail.setSequence(i+1);
			awbFlownDetail.setCoupon(awbFlown.getCoupon());
			awbFlownDetail.setQuantity(IntegerUtil.convertInteger(elm.getQuantity()));
			awbFlownDetail.setWeightIndicator(elm.getWeightIndicator());
			awbFlownDetail.setWeightGross(DoubleUtil.convertDouble(elm.getWeightGross()));
			awbFlownDetail.setRateClass(elm.getRateClass());
			awbFlownDetail.setWeightCharge(DoubleUtil.convertDouble(elm.getWeightCharge()));
			awbFlownDetail.setUnit(DoubleUtil.convertDouble(elm.getUnit()));
			awbFlownDetail.setUnitCass(awb.getUnitCass());
			awbFlownDetail.setAmount(DoubleUtil.round(DoubleUtil.convertDouble(elm.getAmount()), 2));
			awbFlownDetail.setDescription(elm.getDescription());
			
			awbFlownDetail.setCreatedBy(sysUser.getUsername());
			awbFlownDetail.setCreatedDate(new Date());
			
			awbFlownDetail.setAwbFlownId(awbFlown.getAwbFlownId());
			awbFlownDetail.setAwbFlown(awbFlown);
			awbFlown.getAwbFlownDetail().add(awbFlownDetail);
			
			flownCharge += awbFlownDetail.getAmount();
		}
		return flownCharge;
	}

	public void awbKmTax(Manifest manifest, Awb awb, AwbFlown awbFlown, AwbFilter filter, SysUser sysUser) {
		for(AwbTaxFilter elm : filter.getAwbTax()) {
			int retval =  DoubleUtil.convertDouble(elm.getAmount()).compareTo(0.0);
			 
			if(elm.getCode().length() == 3 && retval != 0){
				AwbFlownTax awbFlownTax = new AwbFlownTax();
				awbFlownTax.setManifestNo(manifest.getManifestNo());
				awbFlownTax.setLta(awb.getLta());
				awbFlownTax.setCoupon(awbFlown.getCoupon());
				awbFlownTax.setTaxCode(elm.getCode());
				awbFlownTax.setTaxAmount(DoubleUtil.round(DoubleUtil.convertDouble(elm.getAmount()), 2));
				awbFlownTax.setCreatedBy(sysUser.getUsername());
				awbFlownTax.setCreatedDate(new Date());
				
				awbFlownTax.setAwbFlownId(awbFlown.getAwbFlownId());
				awbFlownTax.setAwbFlown(awbFlown);
				awbFlown.getAwbFlownTax().add(awbFlownTax);
			}
		}
	}

	public void awbKmTotal(Manifest manifest, AwbFlown awbFlown, SysUser sysUser) {
		
		//TOTAL
		Double weightGross = 0.0;
		Double weightCharge = 0.0;
		Double charge = 0.0;
		for (AwbFlownDetail elm : awbFlown.getAwbFlownDetail()) {
			weightGross += elm.getWeightGross();
			weightCharge += elm.getWeightCharge();
			charge += elm.getAmount();
		}
		
		awbFlown.setWeightGross(weightGross);
		awbFlown.setWeightCharge(weightCharge);
		
		if(StringUtils.equals(awbFlown.getChargeIndicator(), "P")){
			awbFlown.setWeightPp(charge);
			awbFlown.setWeightCc(0.0);
		}else {
			awbFlown.setWeightPp(0.0);
			awbFlown.setWeightCc(charge);
		}
		
		Double agentCharge = 0.0;
		Double carrierCharge = 0.0;
		
		for (AwbFlownTax elm : awbFlown.getAwbFlownTax()) {
			if(StringUtils.equals(StringUtils.substring(elm.getTaxCode(), 2), "A")){
				agentCharge += elm.getTaxAmount();
			}else{
				carrierCharge += elm.getTaxAmount();
			}
		}
		
		if(StringUtils.equals(awbFlown.getOtherIndicator(), "P")){
			awbFlown.setAgentPp(DoubleUtil.round(agentCharge, 2));
			awbFlown.setCarrierPp(DoubleUtil.round(carrierCharge, 2));
			awbFlown.setAgentCc(0.0);
			awbFlown.setCarrierCc(0.0);
		}else {
			awbFlown.setAgentPp(0.0);
			awbFlown.setCarrierPp(0.0);
			awbFlown.setAgentCc(DoubleUtil.round(agentCharge, 2));
			awbFlown.setCarrierCc(DoubleUtil.round(carrierCharge, 2));
		}
		
		awbFlown.setTotalPp(DoubleUtil.round(awbFlown.getWeightPp() + awbFlown.getAgentPp() + awbFlown.getCarrierPp(), 2));
		awbFlown.setTotalCc(DoubleUtil.round(awbFlown.getWeightCc() + awbFlown.getAgentCc() + awbFlown.getCarrierCc(), 2));
		
		awbFlown.setComm(DoubleUtil.round(awbFlown.getCommPercent() * (awbFlown.getWeightPp() + awbFlown.getWeightCc()) / 100, 2));
		
		//VAT
		awbFlown.setVat(DoubleUtil.round((awbFlown.getTotalPp() + awbFlown.getTotalCc()) * awbFlown.getVatPercent() / 100, 2));
		awbFlown.setNet(DoubleUtil.round(awbFlown.getTotalPp() + awbFlown.getTotalCc() + awbFlown.getVat() - awbFlown.getComm(), 2));
		
		awbFlown.setContentManifest(manifest.getManifestNo() + ";");
		awbFlown.setStatusFlown(CargoUtils.STATUS_F);
		awbFlown.setCreatedDate(new Date());
		awbFlown.setCreatedBy(sysUser.getUsername());
	}

	public void awbKmDetailRest(Awb awb, AwbFlown awbFlown, AwbFlown awbFlownRest, AwbFilter filter, SysUser sysUser) {
		
		for (int i = 0; i < filter.getAwbDetail().size(); i++) {
			AwbDetailFilter elm = filter.getAwbDetail().get(i);
			
			AwbDetailId id = new AwbDetailId();
			id.setLta(awb.getLta());
			id.setSequence(Integer.parseInt(elm.getSequence()));
			
			AwbDetail awbDetail = awbDetailService.find(id);
			
			AwbFlownDetail awbFlownDetail = new AwbFlownDetail();
			
			awbFlownDetail.setLta(awb.getLta());
			awbFlownDetail.setSequence(Integer.parseInt(elm.getSequence()));
			awbFlownDetail.setCoupon(awbFlown.getCoupon() + 1);
			awbFlownDetail.setQuantity(IntegerUtil.convertInteger(elm.getQuantity()));
			awbFlownDetail.setWeightIndicator(elm.getWeightIndicator());
			awbFlownDetail.setWeightGross(DoubleUtil.convertDouble(elm.getWeightGross()));
			awbFlownDetail.setRateClass(elm.getRateClass());
			awbFlownDetail.setWeightCharge(DoubleUtil.convertDouble(elm.getWeightCharge()));
			awbFlownDetail.setUnit(DoubleUtil.convertDouble(elm.getUnit()));
			awbFlownDetail.setUnitCass(awb.getUnitCass());
			awbFlownDetail.setAmount(DoubleUtil.round(awbDetail.getAmount() - DoubleUtil.convertDouble(elm.getAmount()), 2));
			awbFlownDetail.setDescription(elm.getDescription());
			
			awbFlownDetail.setCreatedBy(sysUser.getUsername());
			awbFlownDetail.setCreatedDate(new Date());
			
			awbFlownDetail.setAwbFlownId(awbFlownRest.getAwbFlownId());
			awbFlownDetail.setAwbFlown(awbFlownRest);
			awbFlownRest.getAwbFlownDetail().add(awbFlownDetail);
		}
	}

	public void awbKmTaxRest(Awb awb, AwbFlown awbFlown, AwbFlown awbFlownRest, AwbFilter filter, SysUser sysUser) {
		for(AwbTax elm : awb.getAwbTax()) {
			
			AwbTaxFilter taxFilter = new AwbTaxFilter();
			for(AwbTaxFilter item: filter.getAwbTax()){
				if(item.getCode().length() == 3 && StringUtils.equals(elm.getId().getTaxCode(), item.getCode())){
					taxFilter = item;
					break;
				}
			}
			
			AwbFlownTax awbFlownTax = new AwbFlownTax();
			awbFlownTax.setLta(awb.getLta());
			awbFlownTax.setCoupon(awbFlown.getCoupon() + 1);
			awbFlownTax.setTaxCode(elm.getId().getTaxCode());
			awbFlownTax.setTaxAmount(DoubleUtil.round(elm.getTaxAmount() - DoubleUtil.convertDouble(taxFilter.getAmount()), 2));
			awbFlownTax.setCreatedBy(sysUser.getUsername());
			awbFlownTax.setCreatedDate(new Date());
			
			if(awbFlownTax.getTaxAmount() > 0.0){
				awbFlownTax.setAwbFlownId(awbFlownRest.getAwbFlownId());
				awbFlownTax.setAwbFlown(awbFlownRest);
				awbFlownRest.getAwbFlownTax().add(awbFlownTax);
			}
		}
	}

	public void awbKmTotalRest(Manifest manifest, Awb awb, AwbFlown awbFlown, AwbFlown awbFlownRest, AwbFilter filter, SysUser sysUser) {
		awbFlownRest.setCoupon(awbFlown.getCoupon() + 1);
		
		awbFlownRest.setWeightGross(awb.getWeightGross());
		awbFlownRest.setWeightCharge(awb.getWeightCharge());
		
		if(StringUtils.equals(awbFlownRest.getChargeIndicator(), "P")){
			awbFlownRest.setWeightPp(DoubleUtil.round(awb.getWeightPp() - awbFlown.getWeightPp(), 2));
			awbFlownRest.setWeightCc(0.0);
		}else {
			awbFlownRest.setWeightPp(0.0);
			awbFlownRest.setWeightCc(DoubleUtil.round(awb.getWeightCc() - awbFlown.getWeightCc(), 2));
		}
		
		if(StringUtils.equals(awbFlownRest.getOtherIndicator(), "P")){
			awbFlownRest.setAgentPp(DoubleUtil.round(awb.getAgentPp() - awbFlown.getAgentPp(), 2));
			awbFlownRest.setCarrierPp(DoubleUtil.round(awb.getCarrierPp() - awbFlown.getCarrierPp(), 2));
			awbFlownRest.setAgentCc(0.0);
			awbFlownRest.setCarrierCc(0.0);
		}else {
			awbFlownRest.setAgentPp(0.0);
			awbFlownRest.setCarrierPp(0.0);
			awbFlownRest.setAgentCc(DoubleUtil.round(awb.getAgentCc() - awbFlown.getAgentCc(), 2));
			awbFlownRest.setCarrierCc(DoubleUtil.round(awb.getCarrierCc() - awbFlown.getCarrierCc(), 2));
		}
		
		awbFlownRest.setTotalPp(DoubleUtil.round(awbFlownRest.getWeightPp() + awbFlownRest.getAgentPp() + awbFlownRest.getCarrierPp(), 2));
		awbFlownRest.setTotalCc(DoubleUtil.round(awbFlownRest.getWeightCc() + awbFlownRest.getAgentCc() + awbFlownRest.getCarrierCc(), 2));
		
		//Comm.
		awbFlownRest.setComm(DoubleUtil.round(awb.getComm() - awbFlown.getComm(), 2));
		
		//VAT
		awbFlownRest.setVat(DoubleUtil.round(awb.getVat() - awbFlown.getVat(), 2));
		awbFlownRest.setNet(DoubleUtil.round(awb.getNet() - awbFlown.getNet(), 2));

		awbFlownRest.setContentManifest(manifest.getManifestNo() + ";");
		awbFlownRest.setStatusFlown(CargoUtils.STATUS_I);
		awbFlownRest.setCreatedDate(new Date());
		awbFlownRest.setCreatedBy(sysUser.getUsername());
	}

	public Double awbFlownKmDetail(Manifest manifest, AwbFlown awbFlown, AwbFilter filter, SysUser sysUser) {
		Double flownCharge = 0.0;
		for (AwbDetailFilter elm: filter.getAwbDetail()) {
			
			for(AwbFlownDetail awbFlownDetail : awbFlown.getAwbFlownDetail()){
				
				System.out.println(elm.getAwbDetailId());
				System.out.println(awbFlownDetail.getAwbFlownDetailId());
				
				System.out.println(elm.getAwbDetailId().compareTo(awbFlownDetail.getAwbFlownDetailId()));
				if(elm.getAwbDetailId().compareTo(awbFlownDetail.getAwbFlownDetailId()) == 0){
					
					awbFlownDetail.setManifestNo(manifest.getManifestNo());
					awbFlownDetail.setAmount(DoubleUtil.round(DoubleUtil.convertDouble(elm.getAmount()), 2));
					awbFlownDetail.setDescription(elm.getDescription());
					awbFlownDetail.setCreatedBy(sysUser.getUsername());
					awbFlownDetail.setCreatedDate(new Date());
					
					flownCharge += awbFlownDetail.getAmount();
					
					awbFlownDetail.setAwbFlown(awbFlown);
					break;
				}
			}
		}
		return flownCharge;
	}

	public void awbFlownKmTax(Manifest manifest, AwbFlown awbFlown, AwbFilter filter, SysUser sysUser) {
		
		for(AwbTaxFilter elm : filter.getAwbTax()) {
			int retval =  DoubleUtil.convertDouble(elm.getAmount()).compareTo(0.0);
			 
			if(elm.getCode().length() == 3 && retval != 0){
				
				for(AwbFlownTax awbFlownTax: awbFlown.getAwbFlownTax()){
					if(awbFlownTax.getAwbFlownTaxId().equals(elm.getAwbTaxId())){
						awbFlownTax.setManifestNo(manifest.getManifestNo());
						awbFlownTax.setTaxAmount(DoubleUtil.round(DoubleUtil.convertDouble(elm.getAmount()), 2));
						awbFlownTax.setCreatedBy(sysUser.getUsername());
						awbFlownTax.setCreatedDate(new Date());
						
						break;
					}
				}
			}
		}
	}

	public void awbFlownKmTotal(Manifest manifest, AwbFlown awbFlown, SysUser sysUser) {
		awbFlown.setManifestNo(manifest.getManifestNo());
		awbFlown.setFlightDate(manifest.getFlightDate());
		awbFlown.setFlightNumber(manifest.getFlightNumber());
		awbFlown.setImmat(manifest.getImmat());
		
		//TOTAL
		Double weightGross = 0.0;
		Double weightCharge = 0.0;
		Double charge = 0.0;
		for (AwbFlownDetail elm : awbFlown.getAwbFlownDetail()) {
			weightGross += elm.getWeightGross();
			weightCharge += elm.getWeightCharge();
			charge += elm.getAmount();
		}
		
		awbFlown.setWeightGross(DoubleUtil.round(weightGross,2));
		awbFlown.setWeightCharge(DoubleUtil.round(weightCharge,2));
		
		if(StringUtils.equals(awbFlown.getChargeIndicator().trim(), "P")){
			awbFlown.setWeightPp(DoubleUtil.round(charge,2));
			awbFlown.setWeightCc(0.0);
		}else {
			awbFlown.setWeightPp(0.0);
			awbFlown.setWeightCc(DoubleUtil.round(charge,2));
		}
		
		Double agentCharge = 0.0;
		Double carrierCharge = 0.0;
		
		for (AwbFlownTax elm : awbFlown.getAwbFlownTax()) {
			if(StringUtils.equals(StringUtils.substring(elm.getTaxCode(), 2), "A")){
				agentCharge += elm.getTaxAmount();
			}else{
				carrierCharge += elm.getTaxAmount();
			}
		}
		
		if(StringUtils.equals(awbFlown.getOtherIndicator(), "P")){
			awbFlown.setAgentPp(DoubleUtil.round(agentCharge,2));
			awbFlown.setCarrierPp(DoubleUtil.round(carrierCharge,2));
			awbFlown.setAgentCc(0.0);
			awbFlown.setCarrierCc(0.0);
		}else {
			awbFlown.setAgentPp(0.0);
			awbFlown.setCarrierPp(0.0);
			awbFlown.setAgentCc(DoubleUtil.round(agentCharge,2));
			awbFlown.setCarrierCc(DoubleUtil.round(carrierCharge,2));
		}
		
		awbFlown.setTotalPp(DoubleUtil.round(awbFlown.getWeightPp() + awbFlown.getAgentPp() + awbFlown.getCarrierPp(),2));
		awbFlown.setTotalCc(DoubleUtil.round(awbFlown.getWeightCc() + awbFlown.getAgentCc() + awbFlown.getCarrierCc(),2));
		
		//Comm.
		awbFlown.setComm(DoubleUtil.round(awbFlown.getCommPercent() * (awbFlown.getWeightPp() + awbFlown.getWeightCc()) / 100, 2));
		
		//VAT
		awbFlown.setVat(DoubleUtil.round((awbFlown.getTotalPp() + awbFlown.getTotalCc()) * awbFlown.getVatPercent() / 100, 2));
		awbFlown.setNet(DoubleUtil.round(awbFlown.getTotalPp() + awbFlown.getTotalCc() + awbFlown.getVat() - awbFlown.getComm(), 2));
		
		awbFlown.setStatusFlown(CargoUtils.STATUS_F);
		awbFlown.setCreatedDate(new Date());
		awbFlown.setCreatedBy(sysUser.getUsername());
	}

	public void awbFlownKmDetailRest(AwbFlown awbFlownOrig, List<AwbFlownDetail> awbFlownDetailOrig, AwbFlown awbFlown, AwbFlown awbFlownRest, AwbFilter filter, SysUser sysUser) {
		
		for (AwbDetailFilter elm: filter.getAwbDetail()) {
			AwbFlownDetail item = new AwbFlownDetail();
			for(AwbFlownDetail tmp: awbFlownDetailOrig){
				if(tmp.getSequence() ==  Integer.parseInt(elm.getSequence())){
					item = tmp;
					break;
				}
			}
			
			AwbFlownDetail awbFlownDetail = new AwbFlownDetail();
			awbFlownDetail.setLta(awbFlownOrig.getLta());
			awbFlownDetail.setSequence(Integer.parseInt(elm.getSequence()));
			awbFlownDetail.setCoupon(awbFlown.getCoupon() + 1);
			awbFlownDetail.setQuantity(IntegerUtil.convertInteger(elm.getQuantity()));
			awbFlownDetail.setWeightIndicator(elm.getWeightIndicator());
			awbFlownDetail.setWeightGross(DoubleUtil.convertDouble(elm.getWeightGross()));
			awbFlownDetail.setRateClass(elm.getRateClass());
			awbFlownDetail.setWeightCharge(DoubleUtil.convertDouble(elm.getWeightCharge()));
			awbFlownDetail.setUnit(DoubleUtil.convertDouble(elm.getUnit()));
			awbFlownDetail.setUnitCass(awbFlownOrig.getUnitCass());
			awbFlownDetail.setAmount(DoubleUtil.round(item.getAmount() - DoubleUtil.convertDouble(elm.getAmount()), 2));
			awbFlownDetail.setDescription(elm.getDescription());
			
			awbFlownDetail.setCreatedBy(sysUser.getUsername());
			awbFlownDetail.setCreatedDate(new Date());
			
			awbFlownDetail.setAwbFlownId(awbFlownRest.getAwbFlownId());
			awbFlownDetail.setAwbFlown(awbFlownRest);
			awbFlownRest.getAwbFlownDetail().add(awbFlownDetail);
		}
	}

	public void awbFlownKmTaxRest(AwbFlown awbFlownOrig, List<AwbFlownTax> awbFlownTaxOrig, AwbFlown awbFlown, AwbFlown awbFlownRest, AwbFilter filter, SysUser sysUser) {
		for(AwbFlownTax elm : awbFlownTaxOrig) {
			
			AwbTaxFilter taxFilter = new AwbTaxFilter();
			for(AwbTaxFilter item: filter.getAwbTax()){
				if(item.getCode().length() == 3 && StringUtils.equals(elm.getTaxCode(), item.getCode())){
					taxFilter = item;
					break;
				}
			}
			
			AwbFlownTax awbFlownTax = new AwbFlownTax();
			awbFlownTax.setLta(awbFlownOrig.getLta());
			awbFlownTax.setCoupon(awbFlown.getCoupon() + 1);
			awbFlownTax.setTaxCode(elm.getTaxCode());
			awbFlownTax.setTaxAmount(DoubleUtil.round(elm.getTaxAmount() - DoubleUtil.convertDouble(taxFilter.getAmount()), 2));
			awbFlownTax.setCreatedBy(sysUser.getUsername());
			awbFlownTax.setCreatedDate(new Date());
			
			if(awbFlownTax.getTaxAmount() > 0.0){
				awbFlownTax.setAwbFlownId(awbFlownRest.getAwbFlownId());
				awbFlownTax.setAwbFlown(awbFlownRest);
				awbFlownRest.getAwbFlownTax().add(awbFlownTax);
			}
		}
	}

	public void awbFlownKmTotalRest(Manifest manifest, AwbFlown awbFlownOrig, AwbFlown awbFlown, AwbFlown awbFlownRest, AwbFilter filter, SysUser sysUser) {
		awbFlownRest.setCoupon(awbFlown.getCoupon() + 1);
		
		awbFlownRest.setWeightGross(DoubleUtil.round(awbFlownOrig.getWeightGross(), 2));
		awbFlownRest.setWeightCharge(DoubleUtil.round(awbFlownOrig.getWeightCharge(), 2));
		
		if(StringUtils.equals(awbFlownRest.getChargeIndicator(), "P")){
			awbFlownRest.setWeightPp(DoubleUtil.round(awbFlownOrig.getWeightPp() - awbFlown.getWeightPp(), 2));
			awbFlownRest.setWeightCc(0.0);
		}else {
			awbFlownRest.setWeightPp(0.0);
			awbFlownRest.setWeightCc(DoubleUtil.round(awbFlownOrig.getWeightCc() - awbFlown.getWeightCc(), 2));
		}
		
		if(StringUtils.equals(awbFlownRest.getOtherIndicator(), "P")){
			awbFlownRest.setAgentPp(DoubleUtil.round(awbFlownOrig.getAgentPp() - awbFlown.getAgentPp(), 2));
			awbFlownRest.setCarrierPp(DoubleUtil.round(awbFlownOrig.getCarrierPp() - awbFlown.getCarrierPp(), 2));
			awbFlownRest.setAgentCc(0.0);
			awbFlownRest.setCarrierCc(0.0);
		}else {
			awbFlownRest.setAgentPp(0.0);
			awbFlownRest.setCarrierPp(0.0);
			awbFlownRest.setAgentCc(DoubleUtil.round(awbFlownOrig.getAgentCc() - awbFlown.getAgentCc(), 2));
			awbFlownRest.setCarrierCc(DoubleUtil.round(awbFlownOrig.getCarrierCc() - awbFlown.getCarrierCc(), 2));
		}
		
		awbFlownRest.setTotalPp(DoubleUtil.round(awbFlownRest.getWeightPp() + awbFlownRest.getAgentPp() + awbFlownRest.getCarrierPp(), 2));
		awbFlownRest.setTotalCc(DoubleUtil.round(awbFlownRest.getWeightCc() + awbFlownRest.getAgentCc() + awbFlownRest.getCarrierCc(), 2));
		
		//Comm.
		awbFlownRest.setComm(DoubleUtil.round(awbFlownOrig.getComm() - awbFlown.getComm(), 2));
		
		//VAT
		awbFlownRest.setVat(DoubleUtil.round(awbFlownOrig.getVat() - awbFlown.getVat(), 2));
		awbFlownRest.setNet(DoubleUtil.round(awbFlownOrig.getNet() - awbFlown.getNet(), 2));

		awbFlownRest.setContentManifest(awbFlown.getContentManifest() + manifest.getManifestNo() + ";");
		awbFlownRest.setStatusFlown(CargoUtils.STATUS_I);
		awbFlownRest.setCreatedDate(new Date());
		awbFlownRest.setCreatedBy(sysUser.getUsername());
		
	}

	public Double awbKgDetail(Manifest manifest, Awb awb, AwbFlown awbFlown, AwbFilter filter, SysUser sysUser) {
		Double flownCharge = 0.0;
		for (int i = 0; i < filter.getAwbDetail().size(); i++) {
			AwbDetailFilter elm = filter.getAwbDetail().get(i);
			
			AwbFlownDetail awbFlownDetail = new AwbFlownDetail();
			
			awbFlownDetail.setManifestNo(manifest.getManifestNo());
			awbFlownDetail.setLta(awb.getLta());
			awbFlownDetail.setSequence(i+1);
			awbFlownDetail.setCoupon(awbFlown.getCoupon());
			awbFlownDetail.setQuantity(IntegerUtil.convertInteger(elm.getQuantity()));
			awbFlownDetail.setWeightIndicator(elm.getWeightIndicator());
			awbFlownDetail.setWeightGross(DoubleUtil.convertDouble(elm.getWeightGross()));
			awbFlownDetail.setRateClass(elm.getRateClass());
			awbFlownDetail.setWeightCharge(DoubleUtil.convertDouble(elm.getWeightCharge()));
			awbFlownDetail.setUnit(DoubleUtil.convertDouble(elm.getUnit()));
			awbFlownDetail.setUnitCass(awb.getUnitCass());
			awbFlownDetail.setAmount(DoubleUtil.round(awbFlownDetail.getWeightCharge() *  awbFlownDetail.getUnit(), 2));
			awbFlownDetail.setDescription(elm.getDescription());
			
			awbFlownDetail.setCreatedBy(sysUser.getUsername());
			awbFlownDetail.setCreatedDate(new Date());
			
			awbFlownDetail.setAwbFlownId(awbFlown.getAwbFlownId());
			awbFlownDetail.setAwbFlown(awbFlown);
			awbFlown.getAwbFlownDetail().add(awbFlownDetail);
			
			flownCharge += awbFlownDetail.getAmount();
		}
		return flownCharge;
	}

	public void awbKgTax(Manifest manifest, Awb awb, AwbFlown awbFlown, AwbFilter filter, SysUser sysUser) {
		for(AwbTaxFilter elm : filter.getAwbTax()) {
			int retval =  DoubleUtil.convertDouble(elm.getAmount()).compareTo(0.0);
			 
			if(elm.getCode().length() == 3 && retval != 0){
				AwbFlownTax awbFlownTax = new AwbFlownTax();
				awbFlownTax.setManifestNo(manifest.getManifestNo());
				awbFlownTax.setLta(awb.getLta());
				awbFlownTax.setCoupon(awbFlown.getCoupon());
				awbFlownTax.setTaxCode(elm.getCode());
				awbFlownTax.setTaxAmount(DoubleUtil.round(DoubleUtil.convertDouble(elm.getAmount()), 2));
				awbFlownTax.setCreatedBy(sysUser.getUsername());
				awbFlownTax.setCreatedDate(new Date());
				
				awbFlownTax.setAwbFlownId(awbFlown.getAwbFlownId());
				awbFlownTax.setAwbFlown(awbFlown);
				awbFlown.getAwbFlownTax().add(awbFlownTax);
			}
		}
		
	}

	public void awbKgTotal(Manifest manifest, AwbFlown awbFlown, SysUser sysUser) {
		//TOTAL
		Double weightGross = 0.0;
		Double weightCharge = 0.0;
		Double charge = 0.0;
		for (AwbFlownDetail elm : awbFlown.getAwbFlownDetail()) {
			weightGross += elm.getWeightGross();
			weightCharge += elm.getWeightCharge();
			charge += elm.getAmount();
		}
		
		awbFlown.setWeightGross(weightGross);
		awbFlown.setWeightCharge(weightCharge);
		
		if(StringUtils.equals(awbFlown.getChargeIndicator(), "P")){
			awbFlown.setWeightPp(charge);
			awbFlown.setWeightCc(0.0);
		}else {
			awbFlown.setWeightPp(0.0);
			awbFlown.setWeightCc(charge);
		}
		
		Double agentCharge = 0.0;
		Double carrierCharge = 0.0;
		
		for (AwbFlownTax elm : awbFlown.getAwbFlownTax()) {
			if(StringUtils.equals(StringUtils.substring(elm.getTaxCode(), 2), "A")){
				agentCharge += elm.getTaxAmount();
			}else{
				carrierCharge += elm.getTaxAmount();
			}
		}
		
		if(StringUtils.equals(awbFlown.getOtherIndicator(), "P")){
			awbFlown.setAgentPp(DoubleUtil.round(agentCharge, 2));
			awbFlown.setCarrierPp(DoubleUtil.round(carrierCharge, 2));
			awbFlown.setAgentCc(0.0);
			awbFlown.setCarrierCc(0.0);
		}else {
			awbFlown.setAgentPp(0.0);
			awbFlown.setCarrierPp(0.0);
			awbFlown.setAgentCc(DoubleUtil.round(agentCharge, 2));
			awbFlown.setCarrierCc(DoubleUtil.round(carrierCharge, 2));
		}
		
		awbFlown.setTotalPp(DoubleUtil.round(awbFlown.getWeightPp() + awbFlown.getAgentPp() + awbFlown.getCarrierPp(), 2));
		awbFlown.setTotalCc(DoubleUtil.round(awbFlown.getWeightCc() + awbFlown.getAgentCc() + awbFlown.getCarrierCc(), 2));
		
		//Comm.
		awbFlown.setComm(DoubleUtil.round(awbFlown.getCommPercent() * (awbFlown.getWeightPp() + awbFlown.getWeightCc()) / 100, 2));
		
		//VAT
		awbFlown.setVat(DoubleUtil.round((awbFlown.getTotalPp() + awbFlown.getTotalCc()) * awbFlown.getVatPercent() / 100, 2));
		awbFlown.setNet(DoubleUtil.round(awbFlown.getTotalPp() + awbFlown.getTotalCc() + awbFlown.getVat() - awbFlown.getComm(), 2));
		
		awbFlown.setContentManifest(manifest.getManifestNo() + ";");
		awbFlown.setStatusFlown(CargoUtils.STATUS_F);
		awbFlown.setCreatedDate(new Date());
		awbFlown.setCreatedBy(sysUser.getUsername());
		
	}

	public void awbKgDetailRest(Awb awb, AwbFlown awbFlown, AwbFlown awbFlownRest, AwbFilter filter, SysUser sysUser) {
		
		for (int i = 0; i < filter.getAwbDetail().size(); i++) {
			AwbDetailFilter elm = filter.getAwbDetail().get(i);
			
			AwbDetailId id = new AwbDetailId();
			id.setLta(awb.getLta());
			id.setSequence(Integer.parseInt(elm.getSequence()));
			
			AwbDetail awbDetail = awbDetailService.find(id);
			
			AwbFlownDetail awbFlownDetail = new AwbFlownDetail();
			
			awbFlownDetail.setLta(awb.getLta());
			awbFlownDetail.setSequence(Integer.parseInt(elm.getSequence()));
			awbFlownDetail.setCoupon(awbFlown.getCoupon() + 1);
			awbFlownDetail.setQuantity(IntegerUtil.convertInteger(elm.getQuantity()));
			awbFlownDetail.setWeightIndicator(elm.getWeightIndicator());
			awbFlownDetail.setWeightGross(DoubleUtil.convertDouble(elm.getWeightGross()));
			awbFlownDetail.setRateClass(elm.getRateClass());
			awbFlownDetail.setWeightCharge(DoubleUtil.round(awbDetail.getWeightCharge() - DoubleUtil.convertDouble(elm.getWeightCharge()), 2));
			awbFlownDetail.setUnit(DoubleUtil.convertDouble(elm.getUnit()));
			awbFlownDetail.setUnitCass(awb.getUnitCass());
			awbFlownDetail.setAmount(DoubleUtil.round(awbFlownDetail.getWeightCharge() * awbFlownDetail.getUnit(), 2));
			awbFlownDetail.setDescription(elm.getDescription());
			
			awbFlownDetail.setCreatedBy(sysUser.getUsername());
			awbFlownDetail.setCreatedDate(new Date());
			
			awbFlownDetail.setAwbFlownId(awbFlownRest.getAwbFlownId());
			awbFlownDetail.setAwbFlown(awbFlownRest);
			awbFlownRest.getAwbFlownDetail().add(awbFlownDetail);
		}
		
	}

	public void awbKgTaxRest(Awb awb, AwbFlown awbFlown, AwbFlown awbFlownRest, AwbFilter filter, SysUser sysUser) {
		
		for(AwbTax elm : awb.getAwbTax()) {
			
			AwbTaxFilter taxFilter = new AwbTaxFilter();
			for(AwbTaxFilter item: filter.getAwbTax()){
				if(item.getCode().length() == 3 && StringUtils.equals(elm.getId().getTaxCode(), item.getCode())){
					taxFilter = item;
					break;
				}
			}
			
			AwbFlownTax awbFlownTax = new AwbFlownTax();
			awbFlownTax.setLta(awb.getLta());
			awbFlownTax.setCoupon(awbFlown.getCoupon() + 1);
			awbFlownTax.setTaxCode(elm.getId().getTaxCode());
			awbFlownTax.setTaxAmount(DoubleUtil.round(elm.getTaxAmount() - DoubleUtil.convertDouble(taxFilter.getAmount()), 2));
			awbFlownTax.setCreatedBy(sysUser.getUsername());
			awbFlownTax.setCreatedDate(new Date());
			
			if(awbFlownTax.getTaxAmount() > 0.0){
				awbFlownTax.setAwbFlownId(awbFlownRest.getAwbFlownId());
				awbFlownTax.setAwbFlown(awbFlownRest);
				awbFlownRest.getAwbFlownTax().add(awbFlownTax);
			}
		}
		
	}

	public void awbKgTotalRest(Manifest manifest, Awb awb, AwbFlown awbFlown, AwbFlown awbFlownRest, AwbFilter filter, SysUser sysUser) {
		awbFlownRest.setCoupon(awbFlown.getCoupon() + 1);
		
		awbFlownRest.setWeightGross(awb.getWeightGross());
		awbFlownRest.setWeightCharge(DoubleUtil.round(awb.getWeightCharge() - awbFlown.getWeightCharge(), 2));
		
		if(StringUtils.equals(awbFlownRest.getChargeIndicator(), "P")){
			awbFlownRest.setWeightPp(DoubleUtil.round(awb.getWeightPp() - awbFlown.getWeightPp(), 2));
			awbFlownRest.setWeightCc(0.0);
		}else {
			awbFlownRest.setWeightPp(0.0);
			awbFlownRest.setWeightCc(DoubleUtil.round(awb.getWeightCc() - awbFlown.getWeightCc(), 2));
		}
		
		if(StringUtils.equals(awbFlownRest.getOtherIndicator(), "P")){
			awbFlownRest.setAgentPp(DoubleUtil.round(awb.getAgentPp() - awbFlown.getAgentPp(), 2));
			awbFlownRest.setCarrierPp(DoubleUtil.round(awb.getCarrierPp() - awbFlown.getCarrierPp(), 2));
			awbFlownRest.setAgentCc(0.0);
			awbFlownRest.setCarrierCc(0.0);
		}else {
			awbFlownRest.setAgentPp(0.0);
			awbFlownRest.setCarrierPp(0.0);
			awbFlownRest.setAgentCc(DoubleUtil.round(awb.getAgentCc() - awbFlown.getAgentCc(), 2));
			awbFlownRest.setCarrierCc(DoubleUtil.round(awb.getCarrierCc() - awbFlown.getCarrierCc(), 2));
		}
		
		awbFlownRest.setTotalPp(DoubleUtil.round(awbFlownRest.getWeightPp() + awbFlownRest.getAgentPp() + awbFlownRest.getCarrierPp(), 2));
		awbFlownRest.setTotalCc(DoubleUtil.round(awbFlownRest.getWeightCc() + awbFlownRest.getAgentCc() + awbFlownRest.getCarrierCc(), 2));
		
		//Comm.
		awbFlownRest.setComm(DoubleUtil.round(awb.getComm() - awbFlown.getComm(), 2));
		
		//VAT
		awbFlownRest.setVat(DoubleUtil.round(awb.getVat() - awbFlown.getVat(), 2));
		awbFlownRest.setNet(DoubleUtil.round(awb.getNet() - awbFlown.getNet(), 2));

		awbFlownRest.setContentManifest(manifest.getManifestNo() + ";");
		awbFlownRest.setStatusFlown(CargoUtils.STATUS_I);
		awbFlownRest.setCreatedDate(new Date());
		awbFlownRest.setCreatedBy(sysUser.getUsername());
		
	}

	public Double awbFlownKgDetail(Manifest manifest, AwbFlown awbFlown, AwbFilter filter, SysUser sysUser) {
		Double flownCharge = 0.0;
		for (AwbDetailFilter elm: filter.getAwbDetail()) {
			
			for(AwbFlownDetail awbFlownDetail : awbFlown.getAwbFlownDetail()){
				if(elm.getAwbDetailId().compareTo(awbFlownDetail.getAwbFlownDetailId()) == 0){
					
					awbFlownDetail.setManifestNo(manifest.getManifestNo());
					awbFlownDetail.setWeightCharge(DoubleUtil.convertDouble(elm.getWeightCharge()));
					
					awbFlownDetail.setAmount(DoubleUtil.round(DoubleUtil.convertDouble(elm.getWeightCharge()) * awbFlownDetail.getUnit(), 2));
					awbFlownDetail.setDescription(elm.getDescription());
					awbFlownDetail.setCreatedBy(sysUser.getUsername());
					awbFlownDetail.setCreatedDate(new Date());
					
					flownCharge += awbFlownDetail.getAmount();
					
					awbFlownDetail.setAwbFlown(awbFlown);
					break;
				}
			}
		}
		return flownCharge;
	}

	public void awbFlownKgTax(Manifest manifest, AwbFlown awbFlown, AwbFilter filter, SysUser sysUser) {
		
		for(AwbTaxFilter elm : filter.getAwbTax()) {
			int retval =  DoubleUtil.convertDouble(elm.getAmount()).compareTo(0.0);
			 
			if(elm.getCode().length() == 3 && retval != 0){
				
				for(AwbFlownTax awbFlownTax: awbFlown.getAwbFlownTax()){
					if(awbFlownTax.getAwbFlownTaxId().equals(elm.getAwbTaxId())){
						awbFlownTax.setManifestNo(manifest.getManifestNo());
						awbFlownTax.setTaxAmount(DoubleUtil.round(DoubleUtil.convertDouble(elm.getAmount()), 2));
						awbFlownTax.setCreatedBy(sysUser.getUsername());
						awbFlownTax.setCreatedDate(new Date());
						
						break;
					}
				}
			}
		}
	}

	public void awbFlownKgDetailRest(AwbFlown awbFlownOrig, List<AwbFlownDetail> awbFlownDetailOrig, AwbFlown awbFlown, AwbFlown awbFlownRest, AwbFilter filter, SysUser sysUser) {
		for (AwbDetailFilter elm: filter.getAwbDetail()) {
			AwbFlownDetail item = new AwbFlownDetail();
			for(AwbFlownDetail tmp: awbFlownDetailOrig){
				if(tmp.getSequence() ==  Integer.parseInt(elm.getSequence())){
					item = tmp;
					break;
				}
			}
			
			AwbFlownDetail awbFlownDetail = new AwbFlownDetail();
			awbFlownDetail.setLta(awbFlownOrig.getLta());
			awbFlownDetail.setSequence(Integer.parseInt(elm.getSequence()));
			awbFlownDetail.setCoupon(awbFlown.getCoupon() + 1);
			awbFlownDetail.setQuantity(IntegerUtil.convertInteger(elm.getQuantity()));
			awbFlownDetail.setWeightIndicator(elm.getWeightIndicator());
			awbFlownDetail.setWeightGross(DoubleUtil.convertDouble(elm.getWeightGross()));
			awbFlownDetail.setRateClass(elm.getRateClass());
			awbFlownDetail.setWeightCharge(DoubleUtil.round(item.getWeightCharge() - DoubleUtil.convertDouble(elm.getWeightCharge()), 2));
			awbFlownDetail.setUnit(DoubleUtil.convertDouble(elm.getUnit()));
			awbFlownDetail.setUnitCass(awbFlownOrig.getUnitCass());
			awbFlownDetail.setAmount(DoubleUtil.round(awbFlownDetail.getWeightCharge() * awbFlownDetail.getUnit(), 2));
			awbFlownDetail.setDescription(elm.getDescription());
			
			awbFlownDetail.setCreatedBy(sysUser.getUsername());
			awbFlownDetail.setCreatedDate(new Date());
			
			awbFlownDetail.setAwbFlownId(awbFlownRest.getAwbFlownId());
			awbFlownDetail.setAwbFlown(awbFlownRest);
			awbFlownRest.getAwbFlownDetail().add(awbFlownDetail);
		}
		
	}

	public void awbFlownKgTaxRest(AwbFlown awbFlownOrig, List<AwbFlownTax> awbFlownTaxOrig, AwbFlown awbFlown, AwbFlown awbFlownRest, AwbFilter filter, SysUser sysUser) {
		for(AwbFlownTax elm : awbFlownTaxOrig) {
			
			AwbTaxFilter taxFilter = new AwbTaxFilter();
			for(AwbTaxFilter item: filter.getAwbTax()){
				if(item.getCode().length() == 3 && StringUtils.equals(elm.getTaxCode(), item.getCode())){
					taxFilter = item;
					break;
				}
			}
			
			AwbFlownTax awbFlownTax = new AwbFlownTax();
			awbFlownTax.setLta(awbFlownOrig.getLta());
			awbFlownTax.setCoupon(awbFlown.getCoupon() + 1);
			awbFlownTax.setTaxCode(elm.getTaxCode());
			awbFlownTax.setTaxAmount(DoubleUtil.round(elm.getTaxAmount() - DoubleUtil.convertDouble(taxFilter.getAmount()), 2));
			awbFlownTax.setCreatedBy(sysUser.getUsername());
			awbFlownTax.setCreatedDate(new Date());
			
			if(awbFlownTax.getTaxAmount() > 0.0){
				awbFlownTax.setAwbFlownId(awbFlownRest.getAwbFlownId());
				awbFlownTax.setAwbFlown(awbFlownRest);
				awbFlownRest.getAwbFlownTax().add(awbFlownTax);
			}
		}
	}

	public void awbKgTotalRest(Manifest manifest, AwbFlown awbFlownOrig, AwbFlown awbFlown, AwbFlown awbFlownRest, AwbFilter filter, SysUser sysUser) {
		awbFlownRest.setCoupon(awbFlown.getCoupon() + 1);
		
		awbFlownRest.setWeightGross(DoubleUtil.round(awbFlownOrig.getWeightGross(), 2));
		awbFlownRest.setWeightCharge(DoubleUtil.round(awbFlownOrig.getWeightCharge(), 2));
		
		if(StringUtils.equals(awbFlownRest.getChargeIndicator(), "P")){
			awbFlownRest.setWeightPp(DoubleUtil.round(awbFlownOrig.getWeightPp() - awbFlown.getWeightPp(), 2));
			awbFlownRest.setWeightCc(0.0);
		}else {
			awbFlownRest.setWeightPp(0.0);
			awbFlownRest.setWeightCc(DoubleUtil.round(awbFlownOrig.getWeightCc() - awbFlown.getWeightCc(), 2));
		}
		
		if(StringUtils.equals(awbFlownRest.getOtherIndicator(), "P")){
			awbFlownRest.setAgentPp(DoubleUtil.round(awbFlownOrig.getAgentPp() - awbFlown.getAgentPp(), 2));
			awbFlownRest.setCarrierPp(DoubleUtil.round(awbFlownOrig.getCarrierPp() - awbFlown.getCarrierPp(), 2));
			awbFlownRest.setAgentCc(0.0);
			awbFlownRest.setCarrierCc(0.0);
		}else {
			awbFlownRest.setAgentPp(0.0);
			awbFlownRest.setCarrierPp(0.0);
			awbFlownRest.setAgentCc(DoubleUtil.round(awbFlownOrig.getAgentCc() - awbFlown.getAgentCc(), 2));
			awbFlownRest.setCarrierCc(DoubleUtil.round(awbFlownOrig.getCarrierCc() - awbFlown.getCarrierCc(), 2));
		}
		
		awbFlownRest.setTotalPp(DoubleUtil.round(awbFlownRest.getWeightPp() + awbFlownRest.getAgentPp() + awbFlownRest.getCarrierPp(), 2));
		awbFlownRest.setTotalCc(DoubleUtil.round(awbFlownRest.getWeightCc() + awbFlownRest.getAgentCc() + awbFlownRest.getCarrierCc(), 2));
		
		//Comm.
		awbFlownRest.setComm(DoubleUtil.round(awbFlownOrig.getComm() - awbFlown.getComm(), 2));
		
		//VAT
		awbFlownRest.setVat(DoubleUtil.round(awbFlownOrig.getVat() - awbFlown.getVat(), 2));
		awbFlownRest.setNet(DoubleUtil.round(awbFlownOrig.getNet() - awbFlown.getNet(), 2));

		awbFlownRest.setContentManifest(awbFlown.getContentManifest() + manifest.getManifestNo() + ";");
		awbFlownRest.setStatusFlown(CargoUtils.STATUS_I);
		awbFlownRest.setCreatedDate(new Date());
		awbFlownRest.setCreatedBy(sysUser.getUsername());
		
	}

}
